"use strict";

$(document).ready(function(){

    //---------Main Page Diagram---------

    if ( $(".rates-diagram").length ) {

		// var dataInput = [
		// 	{
		// 		title: "Июль 2015",
		// 		value: 400
		// 	},{
		// 		title:  "Сент 2015",
		// 		value: 530
		// 	},{
		// 		title: "Нояб 2015",
		// 		value: 600
		// 	},{
		// 		title: "Янв 2016",
		// 		value: 900
		// 	},{
		// 		title: "Май 2016",
		// 		value: 900
		// 	},{
		// 		title: "Июль 2016",
		// 		value: 1500
		// 	},{
		// 		title:  "Сент 2016",
		// 		value: 1000
		// 	},{
		// 		title: "Нояб 2016",
		// 		value: 2000
		// 	},{
		// 		title: "Янв 2017",
		// 		value: 2550
		// 	},{
		// 		title: "Май 2017",
		// 		value: 2500
		// 	},{
		// 		title: "Июль 2017",
		// 		value: 3000
		// 	}
		// ];
        
        // var scaleStepSize = 250;
        
		// var scaleLabels = [];
        // var scaleData = [];
		// for (let elem of dataInput) {
        //     scaleLabels.push(elem.title);
		// }
		// for (let elem of dataInput) {
		// 	scaleData.push(elem.value);
        // }
        
        

        //---Диаграмма

        // let draw = Chart.controllers.line.prototype.draw;
        // Chart.controllers.line = Chart.controllers.line.extend({
        //     draw: function() {
        //         draw.apply(this, arguments);
        //         let ctx = this.chart.chart.ctx;
        //         let _stroke = ctx.stroke;
        //         ctx.stroke = function() {
        //             ctx.save();
        //             ctx.shadowColor = '#E56590';
        //             ctx.shadowBlur = 10;
        //             ctx.shadowOffsetX = 0;
        //             ctx.shadowOffsetY = 8;
        //             _stroke.apply(this, arguments)
        //             ctx.restore();
        //         }
        //     }
        // });

        // var ctx = $(".dollar-rate");
        // Chart.defaults.global.defaultFontColor = "#000";
        // Chart.defaults.global.defaultFontFamily = "Roboto";
        // Chart.defaults.global.defaultFontSize = 14;

        // var myChart = new Chart(ctx, {
        //     type: 'line',
        //     data: {
        //         labels:  scaleLabels,
        //         datasets: [{
        //             label: 'Course',
        //             data: scaleData,
        //             backgroundColor: 'transparent',
        //             borderColor: '#007FFF',
        //             borderWidth: 3,
        //             lineTension: 0.1,
        //             pointRadius: 0.5
        //         }]
        //     },
        //     options: {
        //         legend: {
        //             labels: {
        //                     fontSize: 17
        //                 }
        //         },
        //         elements: {
        //             line: {
        //                 tension: 0.3, // disables bezier curves
        //             }
        //         },
        //         scales: {
        //             xAxes: [{
        //                 gridLines: {
        //                     display: false,
        //                     color: "black"
        //                 }
        //             }],
        //             yAxes: [{
        //                 ticks: {
        //                     stepSize: scaleStepSize
        //                 }
        //             }]
        //         }
        //     }
        // });

        //--draw scaleY
        // let minData = scaleData[0];
        // let maxData = scaleData[0];
        // for (let i=0; i<scaleData.length; i++ ) {
        //     if(scaleData[i] > maxData){
        //         maxData = scaleData[i];
        //     }
        //     if(scaleData[i] < minData){
        //         minData = scaleData[i];
        //     } 
        // }

        //forced parameters
        // var scaleStepSize = 1400;
        // var minData = 1400;
        // var maxData = 14000;
        // //forced parameters

        // var numOfMin;
        // var numOfMax;
        // for (var i = 0; true; i++ ) {
        //     if (minData-scaleStepSize < i*scaleStepSize){
        //         numOfMin = i;
        //         break;
        //     }
        // }
        // for (var i = numOfMin; true; i++ ) {
        //     if (maxData <= i*scaleStepSize){
        //         numOfMax = i;
        //         break;
        //     }
        // }
        
        // var arrYScale = [];
        // for (var i = numOfMin; i <= numOfMax; i++) {
        //     arrYScale.push(i*scaleStepSize);
        // }
        // $(".rates-diagram .yScale").children("ul").append('<li class="units">USD</li>');
        // for(var i = arrYScale.length-1; i>=0; i--){
        //     $(".rates-diagram .yScale").children("ul").append("<li>"+arrYScale[i]+"</li>");
        // }
        
        //--/draw scaleY
    }

    //---------/Main Page Diagram---------


    //---------SoluteHome Swiper---------

    if ($(".solute-home__swiper").length) {

    var soluteHomeSwiper = new Swiper ('.solute-home__swiper', {
        // Optional parameters
        direction: 'horizontal',
        effect: 'fade',
        // If we need pagination
        pagination: {
          el: '.swiper-pagination',
          clickable: true
        },
    
        // Navigation arrows
        navigation: {
          nextEl: '.swiper-btn-next',
          prevEl: '.swiper-btn-prev',
        }
    })

    }

    //---------/SoluteHome Swiper---------


    //---------StartNow Swiper be here
    var StartNowSwiper = new MaxSwiper(".start-now", {
        wayBlock: ".container.gen-block__container>.slide-block",
        wayIcons: ".icons-block>.container"
    });

    //StartNowSwiper.switchSlide(3);

    //---------News Swiper---------
    
    if ( $(".news__swiper").length ){

        var newsSwiper = new Swiper ('.news__swiper', {
            // Optional parameters
            direction: 'horizontal',
            loop: true,
            effect: 'fade',
            // Navigation arrows
            navigation: {
            nextEl: '.arrow>.swiper-btn-next',
            prevEl: '.arrow>.swiper-btn-prev'
            }
        })

    }

    //---------Phone Swiper---------

    if( $(".phone-swiper").length ) {

    var phoneSwiper = new Swiper ('.phone-swiper', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,
        slidesPerView: 3,
        centeredSlides: true,
        spaceBetween: 120,
    
        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
    
        // Navigation arrows
        navigation: {
            nextEl: '.swiper-btn-next>div',
            prevEl: '.swiper-btn-prev>div',
        },
        breakpoints: {
            991:{
                spaceBetween: 0,
                slidesPerView: 2.5
            },
            767:{
                spaceBetween: 0,
                slidesPerView: 2.2
            },
            639: {
                spaceBetween: 0,
                slidesPerView: 2
            },
            479: {
                spaceBetween: 0,
                slidesPerView: 1.3
            }
            
        }
    })

    }


    //---------/Phone Swiper---------


    //---------Circle Swiper---------
    
    ;(function(){
        //--self-evocative function
    if( $(".circle-swiper").length ) {

    var circleIcons = $(".circle-swiper .big-circle").children(".icon");
    var circleItems = $(".circle-swiper>.sw-items").find("li");
    var circleSlides = $(".circle-swiper .big-circle").children(".slide");
    
    var startPeriod = 5;//Set-Up in sec
    var clickPeriod = 15;//Set-Up in sec
    var isSwiperStarted = false;
    
    //Set h3 for Slides & distribution data
    for(var i = 0; i < circleItems.length; i++) {
        circleSlides.eq(i).prepend("<h3>"+circleItems.eq(i).text()+"</h3>");
        circleSlides.eq(i).prepend('<div class="s-mob-pic">'+circleIcons.eq(i).find("div").html()+'</div>');
    
        circleIcons.eq(i).find("div").html()
        circleIcons.eq(i).data("numslide", i+1);
        circleItems.eq(i).data("numslide", i+1);
        circleSlides.eq(i).data("numslide", i+1);
    }
    
    
    // set active slide with other elements. give num of slide (value 1...5)
    function setActive(elemNum) {
        circleIcons.eq(elemNum-1).addClass("active");
        circleItems.eq(elemNum-1).addClass("active");
        circleSlides.eq(elemNum-1).addClass("active");
    }
    // do all slides not-active with other elements
    function removeActive() {
        circleIcons.filter(".active").removeClass("active");
        circleItems.filter(".active").removeClass("active");
        circleSlides.filter(".active").removeClass("active");
    }
    
    var intervalCircle;
    function startCircleSwiper(elemNum, myTime){
        clearInterval(intervalCircle);
        removeActive();
        setActive(elemNum);
        isSwiperStarted = true;
    
        var circle_growth = 5/myTime;
    
        var secLittleCircle = 0;
        intervalCircle = setInterval(function(){
            secLittleCircle = secLittleCircle+circle_growth;
            circleIcons.filter(".active").css("animation-delay","-"+secLittleCircle+"s");
            if(secLittleCircle > 100-circle_growth ){
                var i = circleIcons.filter(".active").data("numslide");
        
                if(i == circleIcons.length){
                    startCircleSwiper(1, startPeriod);
                }else{
                    startCircleSwiper(i+1, startPeriod);
                }
            }
        },50);
    }

    function stopCircleSwiper(){
        clearInterval(intervalCircle);
        removeActive();
        isSwiperStarted = false;
    }
    
    // start Swipe
    if($(window).width() > 767-17){
        startCircleSwiper(1, startPeriod);
    }
    var wind;
    $(window).resize(function(){
        wind = $(window).width();
        if(wind > 767-17) {
            if(!isSwiperStarted) {
                startCircleSwiper(1, startPeriod);
                console.log("start circle swiper");
            }	
        }else{
            if(isSwiperStarted) {
                stopCircleSwiper();
                console.log("stop circle swiper");
            }
        }
    })
    
    function onClickOfElem(elem) {
        startCircleSwiper(elem.data("numslide"), clickPeriod);
    }
    
    circleItems.on("click", function(event){
        event.preventDefault();
        onClickOfElem($(this));
    })
    circleIcons.on("click", function(event){
        onClickOfElem($(this));
    })

    }
    //--/self-evocative function close
    })();

    //---------/Circle Swiper---------


    //---------Perspective Subscribe---------

    ;(function(){   //--self-evocative function

        var perspForm = $(".persp-block .easy-form");
        var perspBtn = $(".persp-block .easy-form").children(".transp-btn");
        
        var beginingText = "";
        perspBtn.on("click", function(event){
            
            if( ! $(this).parent().hasClass("active") ){

                event.preventDefault();
                var otherBtn = perspForm.filter(".active").children("button");
                

                var _self = $(this);
                _self.prev().css("display", "block");
                _self.next().css("display", "block");
                setTimeout(function(){
                    _self.parent().addClass("active");
                    beginingText = _self.text();
                    _self.text("Подписаться");
                    _self.removeClass().addClass("easy-btn");
                },5)



                otherBtn.text(beginingText);
                otherBtn.removeClass().addClass("transp-btn");
                otherBtn.parent().removeClass("active");
                
                setTimeout(function(){
                    otherBtn.prev().css("display", "none");
                    otherBtn.next().css("display", "none");
                },100)

            }

        })
    
    })();  //--/self-evocative function close

    //---------/Perspective Subscribe---------

    //--popup

    var popUp = $(".gen-popup");
    
    var timerClose;

    function AddAppPopUp(button) {
        var buttonApp = $(button);
    
        buttonApp.on("click", function(event){
            popUp.css("display", "flex");
            setTimeout(function(){
                popUp.addClass("active");
            },5)
        })
    }

    function ClosePopUp() {
        clearTimeout(timerClose);
        popUp.removeClass("active");
        setTimeout(function(){
            popUp.css("display", "none");
        }, 400);
    }

    AddAppPopUp(".phone-app .mobileApp:nth-child(1) button");
    AddAppPopUp(".solute-start-mine .mobileApp:nth-child(1) .easy-btn");
    AddAppPopUp(".slider-miner-app .mobileApp:nth-child(1) .easy-btn");
    AddAppPopUp(".download-app-eco .mobileApp:nth-child(1) .easy-btn");
        
    // AddSubscribePopUp(".gen-popup .easy-form button");
    
    //--close popUp
    var closeBtn = popUp.find(".close-btn");
    closeBtn.on("click", function(event){
        ClosePopUp()
    })
    popUp.click(function(event) {
        if ($(event.target).closest(".container").length || $(event.target).closest("input").length){
            return;
        }
        ClosePopUp();
        event.stopPropagation();
    });
    //--/close popUp

    //--/popup


    //-----Browser Support------
    function get_name_browser(){
        // получаем данные userAgent
        var ua = navigator.userAgent;
        // с помощью регулярок проверяем наличие текста,
        // соответствующие тому или иному браузеру
        if(ua.search(/Edge/) >0) return 'Edge';
        if (ua.search(/Trident/) > 0) return 'Internet Explorer';
        if (ua.search(/Firefox/) > 0) return 'Firefox';
        if (ua.search(/Opera/) > 0) return 'Opera';
        if (ua.search(/Chrome/) > 0) return 'Google Chrome';
        if (ua.search(/Safari/) > 0) return 'Safari';
        
        // условий может быть и больше.
        // сейчас сделаны проверки только 
        // для популярных браузеров
        return 'Не определен';
    }
     
    var browser = get_name_browser();
    if (browser == 'Не определен' || browser == 'Internet Explorer' ||  browser == 'Edge') {
        
        $(".slider-miner-app .phone-swiper").prepend('<div class="gradient-bg"></div>');

        //----Resize to work of news__swiper on IE11 
        if ( $(".news__swiper").length ){
            $(".news .news__swiper").css("flex-basis", "450px");

            var resizeEvent = window.document.createEvent('UIEvents'); 
            resizeEvent .initUIEvent('resize', true, false, window, 0); 
            window.dispatchEvent(resizeEvent);
        }
        //----/Resize to work of news__swiper on IE11 
        
    }
    //-----/Browser Support------

})