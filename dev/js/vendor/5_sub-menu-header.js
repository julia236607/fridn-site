"use strict";
$(document).ready(function() {
    // menu
    $("#menu-item-17").addClass(' menu-item to-start');
    $("#menu-item-76").addClass(' menu-item to-know');
    $("#menu-item-45").addClass(' menu-item');
    $("<i class='fa fa-chevron-left hed-right' aria-hidden='true'></i> <i class='fa fa-chevron-right hed-left' aria-hidden='true'></i>").prependTo(".header-swiper");

    $("<div class='sec-lang-bl'></div>").appendTo('#qtranslate-2');

    //adaptation on load
    if ($(window).width() <= 744) { 
        $('#qtranslate-2').appendTo( $('#menu-topmenu') );
    }
    else if ($(window).width() >= 744) { 
        $('#qtranslate-2').appendTo( $('.header-sidebar') );
        $('.btn-background').appendTo( $('.header-sidebar') );
    }
    //adaptation on resize
    $(window).on('resize', function(event){
        $('.backghound-shadow').css('height', $('body').innerHeight() + 'px');
        var win = $(this);
        $(".content-wrapper").css('padding-top', ($('.header').innerHeight()) - 2 + 'px');
        if (win.width() <= 744) { 
            $('#qtranslate-2').appendTo( $('#menu-topmenu') );
        }
        else if (win.width() >= 744) { 
            $('#qtranslate-2').appendTo( $('.header-sidebar') );
            $('.btn-background').appendTo( $('.header-sidebar') );
        }
    });
    //i want to start drop-down on click
    $( "#menu-link-17" ).click(function(event) {
        if (event.target.id == 'menu-link-17'){
            $('#menu-topmenu .to-start .header-swiper').toggleClass('open-drop-down');
            $('#menu-link-17').toggleClass('link-active');
            //background-shadow
            $('.backghound-shadow').toggleClass('visible');
            $('.backghound-shadow').css('height', $('body').innerHeight() + 'px');
    }});
    $(document).click(function(event) {
        if ($(event.target).closest("#menu-topmenu .to-start").length) return;
        if ($('#menu-topmenu .to-start .header-swiper').hasClass('open-drop-down')) {
            $('#menu-topmenu .to-start .header-swiper').toggleClass('open-drop-down');
            $('#menu-link-17').toggleClass('link-active');
            $('.backghound-shadow').toggleClass('visible');
        }
    });

    //i want to know drop-down on click
    $( "#menu-link-76" ).click(function(event) {
        if (event.target.id == 'menu-link-76'){
            $('#menu-topmenu .to-know .header-swiper').toggleClass('open-drop-down');
            $('#menu-link-76').toggleClass('link-active');
            //background-shadow
            $('.backghound-shadow').toggleClass('visible');
            $('.backghound-shadow').css('height', $('body').innerHeight() + 'px');
    }});
    $(document).click(function(event) {
        if ($(event.target).closest("#menu-topmenu .to-know").length) return;
        if ( $('#menu-topmenu .to-know .header-swiper').hasClass('open-drop-down')) {
            $('#menu-topmenu .to-know .header-swiper').toggleClass('open-drop-down');
            $('#menu-link-76').toggleClass('link-active');
            $('.backghound-shadow').toggleClass('visible');
        }
    });
    //background-shadow
    $('.backghound-shadow').css('height', $('body').innerHeight() + 'px');
    


    // cut to three first letters of the languages
    var option = $("#qtranxs_select_qtranslate-2-chooser option");
    option.each(function(){
        $(this).html($(this).text().substring(0, 3));
    })
    
    //language drop-down row on click
    // option.css("display", "block");
    $("<div class='menu-dot'></div> <div class='select-wrap'><div class='lang-opener' id='lg-op'></div></div>").prependTo(".sec-lang-bl");
    $("<div class='lang-list-item'><div class='languages-menu-list'></div></div>").prependTo(".sec-lang-bl");
    $('#qtranslate-2-chooser').appendTo( $('.languages-menu-list') );
    $('#qtranxs_select_qtranslate-2-chooser').appendTo( $('.select-wrap') );
    
    $("<p>&nbsp</p>").appendTo('.lang-ru a');
    $("<p>&nbsp</p>").appendTo('.lang-en a');
    $('.lang-zh a').addClass('soon-lang');

    $("<div class='ring'><img src='/wp-content/uploads/2018/02/russia.svg'></div>").prependTo('.lang-ru a');
    $("<div class='ring'><img src='/wp-content/uploads/2018/02/united-kingdom.svg'></div>").prependTo('.lang-en a');
    $("<div class='ring'><img src='/wp-content/uploads/2018/02/germany.svg'></div>").prependTo('.lang-de a');
    $("<div class='ring'><img src='/wp-content/uploads/2018/02/china.svg'></div>").prependTo('.lang-zh a');
    $("<img src='/wp-content/uploads/2018/02/world-wide-web.svg' class='www' id='www'>").prependTo('#qtranslate-2');

    $(".lang-zh").remove();
    
    // $(".select-wrap").appendTo('.sec-lang-bl');

    
    
    $( "#qtranslate-2" ).click(function(event) {
        if (event.target.id == 'qtranslate-2' || event.target.id == 'lg-op' || event.target.id == 'www'){
        $( "#qtranslate-2" ).toggleClass('lang-open');
        //background-shadow
        $('.backghound-shadow').toggleClass('visible');
        $('.backghound-shadow').css('height', $('body').innerHeight() + 'px');
    }});
    
    $(document).click(function(event) {
        if ($(event.target).closest("#qtranslate-2").length) return;
        if ($( "#qtranslate-2" ).hasClass('lang-open')) {
            $( "#qtranslate-2" ).toggleClass('lang-open');
            $('.backghound-shadow').toggleClass('visible');
      }
    });
    
    //registration menu drop-down
    var btnWidth =  $('#header-btn').css('width'),
    reg = $('.header-sidebar .reg-menu');
    $( "#header-btn" ).click(function(event) {
        if (btnWidth=='120px') {
            reg.toggleClass('open-reg-menu');
            $('#header-btn').css('width', '30px');
            $('#header-btn span').fadeOut(0);
            $('#header-btn .close').fadeIn(500);
            btnWidth =  '30px';
        }
        else if (btnWidth=='30px') {
            reg.toggleClass('open-reg-menu');
            $('#header-btn').css('width', '120px');
            $('#header-btn .close').fadeOut(0);
            $('#header-btn span').fadeIn(500);
            btnWidth =  '120px';
        }
    });
    $(document).click(function(event){ 
        var regMenu = $(".header-sidebar .reg-menu"),
        regBtn = $("#header-btn");
        if (!regMenu.is(event.target) && regMenu.has(event.target).length === 0
        && !regBtn.is(event.target) && regBtn.has(event.target).length === 0) { 
            if ($(".header-sidebar .reg-menu").hasClass('open-reg-menu') && btnWidth=='30px') {
                $(".header-sidebar .reg-menu").removeClass('open-reg-menu');
                $('#header-btn').css('width', '120px');
                $('#header-btn .close').fadeOut(0);
                $('#header-btn span').fadeIn(500);
                btnWidth =  '120px';
            }
		}
    });
    //progressbar (i) opener
    $( ".time-info .info" ).click(function(event) {
        $( ".reg-box .progressbar" ).toggleClass('prog-bar-visible');
        $( ".reg-box .reg-separator").toggleClass('prog-bar-visible');
    });

    //header swiper to start
    // $(".menu-item .swiper-container").addClass(' header-swiper');
    $(".sub-menu li").addClass('swiper-slide sub-slide');

    var swiper = new Swiper('.header-swiper', {
        slidesPerView: 5,
        spaceBetween: 0,
        navigation: {
          nextEl: '.hed-left',
          prevEl: '.hed-right',
        },
        breakpoints: {
            825: {
                spaceBetween: 0,
                slidesPerView: 5
            },
            590: {
                spaceBetween: 0,
                slidesPerView: 4
            },
            569: {
                spaceBetween: 0,
                slidesPerView: 3
            },
            446: {
                spaceBetween: 0,
                slidesPerView: 2
            }
        }
    });




//--------------------------------------------------------------------------------

    //start now scroll & open link

    //StartNowSwiper.switchSlide(3);

    function startNow(link, item){
        $( link ).click(function(event) {
            var mapLocationArr = window.location.pathname.split("/");
            var mapPage;
             if (mapLocationArr[1].length == 2 && mapLocationArr[2] == "" || mapLocationArr[1] == "") {
                // $(link).on("click", function (event) {
                    event.preventDefault();
                    var id  = ".start-now",
                        top = $(".start-now").offset().top;
                    $('body,html').animate({scrollTop: top}, 1500);
                // });
                $(".start-now .icons-block__container .icon:nth-child(" + (item + 1) + ")").trigger('click');
                // StartNowSwiper.switchSlide(Number(sessionStorage.getItem("switchSlide")));
            }
            else{
                sessionStorage.setItem('switchSlide', item);
                window.location.href = "/";
            }
        })
    }
    startNow("#menu-link-79", 1);
    startNow("#menu-link-80", 2);
    startNow("#menu-link-81", 3);

   

//--------------------------------------------------------------------------------



    //active pages:
    //ecosystem
    if (window.location.pathname == "/ecosystem/" || window.location.pathname == "/en/ecosystem/" || window.location.pathname == "/de/ecosystem/") {
        $("#menu-link-77 .know-item").addClass('active-page');
        $("#menu-link-77 .know-item .ring img").addClass('active-img');
         }
    else{
        $("#menu-link-77 .know-item").removeClass('active-page');
        $("#menu-link-77 .know-item .ring img").removeClass('active-img');
          }
    //fa-step
    if (window.location.pathname == "/fa-step/" || window.location.pathname == "/en/fa-step/" || window.location.pathname == "/de/fa-step/") {
        $("#menu-link-78 .know-item").addClass('active-page');
        $("#menu-link-78 .know-item .ring img").addClass('active-img');
          }
    else{
        $("#menu-link-78 .know-item").removeClass('active-page');
        $("#menu-link-78 .know-item .ring img").removeClass('active-img');
    }
    //i want to know link
    if (window.location.pathname == "/fa-step/" || window.location.pathname == "/en/fa-step/" || window.location.pathname == "/de/fa-step/" || window.location.pathname == "/ecosystem/" || window.location.pathname == "/en/ecosystem/" || window.location.pathname == "/de/ecosystem/") {
        $("#menu-link-76").addClass('active-page');
    }
    else{
        $("#menu-link-76").removeClass('active-page');
    }
    //contacts
    if (window.location.pathname == "/contacts/" || window.location.pathname == "/en/contacts/" || window.location.pathname == "/de/contacts/") {
        $("#menu-link-45").addClass('active-page');
         }
    else{
        $("#menu-link-45").removeClass('active-page');
          }
    //start-miner & i want to start link
    if (window.location.pathname == "/start-miner/" || window.location.pathname == "/en/start-miner/" || window.location.pathname == "/de/start-miner/") {
        $("#menu-link-18 .start-item").addClass('active-page');
        $("#menu-link-18 .start-item .ring").css('border', '1px solid #D8AD4C');
        $("#menu-link-17").addClass('active-page');
         }
    else{
        $("#menu-link-18 .start-item").removeClass('active-page');
        $("#menu-link-18 .start-item .ring").css('border', '1px solid #dadada;');
        $("#menu-link-17").removeClass('active-page');
          }


    $(".content-wrapper").css('padding-top', ($('.header').innerHeight()) - 2 + 'px');
})
