// $(document).ready(function() {
// if ($("#map").length){
  var mapLocationArr = window.location.pathname.split("/");
  var mapPage;
   if (mapLocationArr[1].length == 2) {
    mapPage = mapLocationArr[2];
   }else{
    mapPage = mapLocationArr[1];
  }
  
  
  if (mapPage === "" || mapPage ==="contacts") {

    var simplemaps_worldmap_mapdata={
        main_settings: {
         //General settings
          // width: "655", //'700' or 'responsive'
          background_color: "#FFFFFF",
          background_transparent: "yes",
          border_color: "#7c8ac2",
         
          //Zoom settings
          zoom: "yes",
          state_description: "",
          state_color: "#fff",
          state_hover_color: "#8bae54",
          state_url: "",
          all_states_inactive: "no",
          location_type: "image",
          location_image_url: "/wp-content/uploads/2018/03/maps-and-flags.svg",
          
          //Location defaults
          location_description: "Regional Office",
          location_color: "#de6667",
          location_opacity: ".9",
          location_url: "",
          location_size: "25",
          all_locations_inactive: "no",
          url_new_tab: "yes",
          initial_zoom: "-1",
          initial_zoom_solo: "no",
          auto_load: "yes",
          hide_labels: "no",
          popups: "detect",
          state_image_url: "",
          state_image_position: "",
          all_states_zoomable: "",
          label_color: "",
          label_hover_color: "",
          label_size: "",
          label_font: "",
          border_size: "1",
          border_hover_size: "1",
          div: "map"
        },
        state_specific: {
          AE: {
            name: "United Arab Emirates",
            inactive: "no",
            hide: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          AF: {
            name: "Afghanistan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          AL: {
            name: "Albania",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          AM: {
            name: "Armenia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          AO: {
            name: "Angola",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          AR: {
            name: "Argentina",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          AT: {
            name: "Austria",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          AU: {
            name: "Australia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          AZ: {
            name: "Azerbaidjan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BA: {
            name: "Bosnia-Herzegovina",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BD: {
            name: "Bangladesh",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BE: {
            name: "Belgium",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BF: {
            name: "Burkina Faso",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BG: {
            name: "Bulgaria",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BH: {
            name: "Bahrain",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BI: {
            name: "Burundi",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BJ: {
            name: "Benin",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BN: {
            name: "Brunei Darussalam",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BO: {
            name: "Bolivia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BR: {
            name: "Brazil",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BS: {
            name: "Bahamas",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BT: {
            name: "Bhutan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BW: {
            name: "Botswana",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BY: {
            name: "Belarus",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BZ: {
            name: "Belize",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CA: {
            name: "Canada",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CD: {
            name: "Congo",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CF: {
            name: "Central African Republic",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CG: {
            name: "Congo",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CH: {
            name: "Switzerland",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CI: {
            name: "Ivory Coast",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CL: {
            name: "Chile",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CM: {
            name: "Cameroon",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CN: {
            name: "China",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CO: {
            name: "Colombia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CR: {
            name: "Costa Rica",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CU: {
            name: "Cuba",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CV: {
            name: "Cape Verde",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CY: {
            name: "Cyprus",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CZ: {
            name: "Czech Republic",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          DE: {
            name: "Germany",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          DJ: {
            name: "Djibouti",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          DK: {
            name: "Denmark",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          DO: {
            name: "Dominican Republic",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          DZ: {
            name: "Algeria",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          EC: {
            name: "Ecuador",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          EE: {
            name: "Estonia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          EG: {
            name: "Egypt",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          EH: {
            name: "Western Sahara",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          ER: {
            name: "Eritrea",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          ES: {
            name: "Spain",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          ET: {
            name: "Ethiopia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          FI: {
            name: "Finland",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          FJ: {
            name: "Fiji",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          FK: {
            name: "Falkland Islands",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          FR: {
            name: "France",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GA: {
            name: "Gabon",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GB: {
            name: "Great Britain",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GE: {
            name: "Georgia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GF: {
            name: "French Guyana",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GH: {
            name: "Ghana",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GL: {
            name: "Greenland",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GM: {
            name: "Gambia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GN: {
            name: "Guinea",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GQ: {
            name: "Equatorial Guinea",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GR: {
            name: "Greece",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GS: {
            name: "S. Georgia & S. Sandwich Isls.",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GT: {
            name: "Guatemala",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GW: {
            name: "Guinea Bissau",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GY: {
            name: "Guyana",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          HN: {
            name: "Honduras",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          HR: {
            name: "Croatia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          HT: {
            name: "Haiti",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          HU: {
            name: "Hungary",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          IC: {
            name: "Canary Islands",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          ID: {
            name: "Indonesia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          IE: {
            name: "Ireland",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          IL: {
            name: "Israel",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          IN: {
            name: "India",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          IQ: {
            name: "Iraq",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          IR: {
            name: "Iran",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          IS: {
            name: "Iceland",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          IT: {
            name: "Italy",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          JM: {
            name: "Jamaica",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          JO: {
            name: "Jordan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          JP: {
            name: "Japan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          KE: {
            name: "Kenya",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          KG: {
            name: "Kyrgyzstan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          KH: {
            name: "Cambodia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          KP: {
            name: "North Korea",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          KR: {
            name: "South Korea",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          KW: {
            name: "Kuwait",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          KZ: {
            name: "Kazakhstan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          LA: {
            name: "Laos",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          LK: {
            name: "Sri Lanka",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          LR: {
            name: "Liberia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          LS: {
            name: "Lesotho",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          LT: {
            name: "Lithuania",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          LU: {
            name: "Luxembourg",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          LV: {
            name: "Latvia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          LY: {
            name: "Libya",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MA: {
            name: "Morocco",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MD: {
            name: "Moldavia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          ME: {
            name: "Montenegro",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MG: {
            name: "Madagascar",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MK: {
            name: "Macedonia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          ML: {
            name: "Mali",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MM: {
            name: "Myanmar",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MN: {
            name: "Mongolia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MR: {
            name: "Mauritania",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MW: {
            name: "Malawi",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MX: {
            name: "Mexico",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MY: {
            name: "Malaysia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MZ: {
            name: "Mozambique",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          NA: {
            name: "Namibia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          NC: {
            name: "New Caledonia (French)",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          NE: {
            name: "Niger",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          NG: {
            name: "Nigeria",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          NI: {
            name: "Nicaragua",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          NL: {
            name: "Netherlands",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          NO: {
            name: "Norway",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          NP: {
            name: "Nepal",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          NZ: {
            name: "New Zealand",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          OM: {
            name: "Oman",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PA: {
            name: "Panama",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PE: {
            name: "Peru",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PG: {
            name: "Papua New Guinea",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PH: {
            name: "Philippines",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PK: {
            name: "Pakistan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PL: {
            name: "Poland",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PR: {
            name: "Puerto Rico",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PS: {
            name: "Palestine",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PT: {
            name: "Portugal",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PY: {
            name: "Paraguay",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          QA: {
            name: "Qatar",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          RO: {
            name: "Romania",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          RS: {
            name: "Serbia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          RU: {
            name: "Russia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          RW: {
            name: "Rwanda",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SA: {
            name: "Saudi Arabia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SB: {
            name: "Solomon Islands",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SD: {
            name: "Sudan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SE: {
            name: "Sweden",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SI: {
            name: "Slovenia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SK: {
            name: "Slovak Republic",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SL: {
            name: "Sierra Leone",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SN: {
            name: "Senegal",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SO: {
            name: "Somalia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SR: {
            name: "Suriname",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SS: {
            name: "South Sudan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SV: {
            name: "El Salvador",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SY: {
            name: "Syria",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SZ: {
            name: "Swaziland",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TD: {
            name: "Chad",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TG: {
            name: "Togo",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TH: {
            name: "Thailand",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TJ: {
            name: "Tadjikistan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TL: {
            name: "East Timor",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TM: {
            name: "Turkmenistan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TN: {
            name: "Tunisia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TR: {
            name: "Turkey",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TT: {
            name: "Trinidad and Tobago",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TW: {
            name: "Taiwan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TZ: {
            name: "Tanzania",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          UA: {
            name: "Ukraine",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          UG: {
            name: "Uganda",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          US: {
            name: "United States",
            inactive: "no",
            hide: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          UY: {
            name: "Uruguay",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          UZ: {
            name: "Uzbekistan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          VE: {
            name: "Venezuela",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          VN: {
            name: "Vietnam",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          VU: {
            name: "Vanuatu",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          YE: {
            name: "Yemen",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          ZA: {
            name: "South Africa",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          ZM: {
            name: "Zambia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          ZW: {
            name: "Zimbabwe",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          }
        },
        locations: {
          "0": {
            name: "New York",
            lat: "41.700000000",
            lng: "-75.900000000",
            type: "image",
            description: "Company Headquarters",
            opacity: "1",
            size: "30"
          },
          "1": {
            name: "Москва",
            lat: "57.666666670",
            lng: "37.750000000",
            type: "image"
          },
          "2": {
            name: "Tbilisi",
            lat: "42.69411",
            lng: "44.83368",
            type: "image"
          },
          "4": {
            name: "Singapur",
            lat: "4.28967",
            lng: "103.85007",
            type: "image"
          },
          "5": {
            name: "Dortmund",
            lat: "51.51494",
            lng: "7.466",
            type: "image"
          }
        },
        regions: {
          "0": {
            name: "North and South America",
            states: [
              "MX",
              "CA",
              "US",
              "GL",
              "EC",
              "AR",
              "VE",
              "BR",
              "CO",
              "BO",
              "PE",
              "BZ",
              "CL",
              "CR",
              "CU",
              "DO",
              "SV",
              "GT",
              "GY",
              "GF",
              "HN",
              "NI",
              "PA",
              "PY",
              "PR",
              "SR",
              "UY",
              "JM",
              "HT",
              "BS"
            ],
            hover_color: "#aab1cd",
            color: "#e5e8f1"
          },
          "1": {
            name: "Europe",
            states: [
              "IT",
              "NL",
              "NO",
              "DK",
              "IE",
              "GB",
              "RO",
              "DE",
              "FR",
              "AL",
              "AM",
              "AT",
              "BY",
              "BE",
              "LU",
              "BG",
              "CZ",
              "EE",
              "GE",
              "GR",
              "HU",
              "IS",
              "LV",
              "LT",
              "MD",
              "PL",
              "PT",
              "RS",
              "SI",
              "HR",
              "BA",
              "ME",
              "MK",
              "SK",
              "ES",
              "FI",
              "SE",
              "CH",
              "TR",
              "CY",
              "UA"
            ],
            hover_color: "#aab1cd",
            color: "#e5e8f1"
          },
          "2": {
            name: "Africa and the Middle East",
            states: [
              "QA",
              "SA",
              "AE",
              "SY",
              "OM",
              "KW",
              "PK",
              "AZ",
              "AF",
              "IR",
              "IQ",
              "IL",
              "PS",
              "JO",
              "LB",
              "YE",
              "TJ",
              "TM",
              "UZ",
              "KG",
              "NE",
              "AO",
              "EG",
              "TN",
              "GA",
              "DZ",
              "LY",
              "CG",
              "GQ",
              "BJ",
              "BW",
              "BF",
              "BI",
              "CM",
              "CF",
              "TD",
              "CI",
              "CD",
              "DJ",
              "ET",
              "GM",
              "GH",
              "GN",
              "GW",
              "KE",
              "LS",
              "LR",
              "MG",
              "MW",
              "ML",
              "MA",
              "MR",
              "MZ",
              "NA",
              "NG",
              "ER",
              "RW",
              "SN",
              "SL",
              "SO",
              "ZA",
              "SD",
              "SS",
              "SZ",
              "TZ",
              "TG",
              "UG",
              "EH",
              "ZM",
              "ZW"
            ],
            hover_color: "#aab1cd",
            color: "#e5e8f1"
          },
          "3": {
            name: "South Asia and Australia",
            states: [
              "TW",
              "IN",
              "AU",
              "MY",
              "IN",
              "NP",
              "NZ",
              "TH",
              "BN",
              "JP",
              "VN",
              "LK",
              "SB",
              "FJ",
              "BD",
              "BT",
              "KH",
              "LA",
              "MM",
              "NP",
              "KP",
              "PG",
              "PH",
              "KR",
              "ID",
              "CN"
            ],
            hover_color: "#aab1cd",
            color: "#e5e8f1"
          },
          "4": {
            name: "North Asia",
            states: [
              "MN",
              "RU",
              "KZ"
            ],
            hover_color: "#aab1cd",
            color: "#e5e8f1"
          }
        },
        labels: {}
      };
    
    
    };