"use strict";

function loadManager(){
    var count=0,
          load=0,
          me=this; 
              
    //add loading
    this.load=function(url, callback){
         count++;
         $.ajax({
             url:url,
             success:function(data){
                 load++;

                 //count %
                 me.procent=(100/count)*load;

                 callback(data);
             }
        })
    };
}

$(document).ready(function(){
    var ldr = new loadManager();
    ldr.load(location.href, function(){

        //--remove preloader (when site is loaded)
        setTimeout(function(){ 
            $(".preloader").animate({opacity: "0"},500, function(){
                $(this).css("display", "none");
                $("body").css("overflow-y", "auto"); 
            })
        }, 1500)
        //--/remove preloader

        //--email validation header
        $('#nf-field-41').keyup(function() {
            if($(this).val() != '') {
                var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
                if(pattern.test($(this).val())){
                    $("#nf-field-42").removeClass('transp-btn-not-active');
                    $("#nf-field-42").addClass('easy-btn');
                }
                else {
                    $("#nf-field-42").removeClass('easy-btn');
                    $("#nf-field-42").addClass('transp-btn-not-active');
                }
            }
            else {
                $("#nf-field-42").removeClass('easy-btn');
                $("#nf-field-42").addClass('transp-btn-not-active');
            }
        });
        // --/ email validation header


        //--list to StartNowSwiper (when was redirect from other page by "Soon-item menu")
        var mapLocationArr = window.location.pathname.split("/");
        if (mapLocationArr[1].length == 2 && mapLocationArr[2] == "" || mapLocationArr[1] == "") {
            if(sessionStorage.getItem("switchSlide")){
                // StartNowSwiper.switchSlide( Number(sessionStorage.getItem("switchSlide")) );
                $(".start-now .icons-block__container .icon:nth-child(" + (Number(sessionStorage.getItem("switchSlide")) + 1) + ")").trigger('click');
                var id  = ".start-now",
                top = $(id).offset().top;
                $('body,html').animate({scrollTop: top}, 1500);
                sessionStorage.removeItem('switchSlide');
            }
        }
        //--/list to StartNowSwiper

    })
})