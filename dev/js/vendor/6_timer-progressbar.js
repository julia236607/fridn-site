"use strict";
$(document).ready(function() {

    // ----------------TIMER------------------
        var countDownDate = new Date("May 31 2018 23:59:59").getTime();
        var x = setInterval(function() {
            var now = new Date().getTime();
            var distance = countDownDate - now;
            var days = Math.floor(distance / (1000 * 60 * 60 * 24)) + '';
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)) + '';
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))  + '';
            var seconds = Math.floor((distance % (1000 * 60)) / 1000)  + '';
            if(days.length < 2){
                days = '0' + days; 
            }
            if(hours.length < 2){
                hours = '0' + hours; 
            }
            if(minutes.length < 2){
                minutes = '0' + minutes; 
            }
            if(seconds.length < 2){
                seconds = '0' + seconds; 
            }
            if (seconds < 0){
                $(".days").html(0);
                $(".hours").html('00');
                $(".minutes").html('00');
                $(".seconds").html('00');
            }
            else{
            $(".days").html(days);
            $(".hours").html(hours);
            $(".minutes").html(minutes);
            $(".seconds").html(seconds);
            }
        }, 1000);
    
        //-------------PROGRESSBAR----------------
        var elem = $(".bar"),
        pointer = $(".pointer"),
        width = 0,
        id = setInterval(frame, 10);        
        function frame() {
        if (width >= 100) {
        clearInterval(id);
        } else {
        width=0; 
        // var marg = $(".bar").width() / $('.bar').parent().width() * 100;
        elem.css('width', width + '%');
        pointer.css('margin', 'calc(' + width + '% - 5px)');
        }
    }

    // email validation
    $('.timer .email').keyup(function() {
        if($(this).val() != '') {
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if(pattern.test($(this).val())){
                $(".timer .registration .reg-btn").removeClass('transp-btn-not-active');
                $(".timer .registration .reg-btn").addClass('easy-btn');
            }
            else {
                $(".timer .registration .reg-btn").removeClass('easy-btn');
                $(".timer .registration .reg-btn").addClass('transp-btn-not-active');
            }
        }
        else {
            $(".timer .registration .reg-btn").removeClass('easy-btn');
            $(".timer .registration .reg-btn").addClass('transp-btn-not-active');
        }
    });

    $('.timer .reg-btn').click(function() {
        if( $('.timer .reg-btn').attr('class') == "reg-btn easy-btn" ){
            $("#nf-field-41").val($(".timer .registration .email").val());
            $('#nf-field-42').trigger('click');
            $(".timer .registration .email").fadeOut(500);
            $(".timer .registration .reg-btn").fadeOut(500);
            var regMessage = setInterval(function() {
                if ($(".timer .registration p").length){
                    clearInterval(regMessage);
                }
                else{
                    $( ".timer .registration" ).html($(".header-sidebar .gen-form .nf-response-msg").html());
                }
            }, 500);
        }
    });

});