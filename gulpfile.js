
// Инициализируем плагины
var gulp        = require('gulp'),       // Собственно Gulp JS
    config      = require('./config.json'),   // Конфиг для проектов
    newer       = require('gulp-newer'),      // Passing through only those source files that are newer than corresponding destination files
    concat      = require('gulp-concat'),     // Склейка файлов
    jade        = require('gulp-jade-php'),       // Плагин для Jade
    //compass     = require('gulp-compass'),    // Плагин для Compass
    scss        = require('gulp-scss'),
    cssmin      = require('gulp-cssmin'),
    rename      = require('gulp-rename'),
    csso        = require('gulp-csso'),       // Минификация CSS
    path        = require('path'),
    imagemin    = require('gulp-imagemin'),   // Минификация изображений
    uglify      = require('gulp-uglify'),     // Минификация JS
    ftp         = require('gulp-ftp'),        // FTP-клиент
    del         = require('del'),             // Удаление файлов и папок
    browserSync = require('browser-sync'),    // Обновление без перезагрузки страницы
    reload      = browserSync.reload,
    order       = require('gulp-order'),      // Определение порядка файлов в потоке
    csscomb     = require('gulp-csscomb'),    // Форматирование стилей
    wrapper     = require('gulp-wrapper'),    // Добавляет к файлу текстовую шапку и/или подвал
    plumber     = require('gulp-plumber'),    // Перехватчик ошибок
    notify      = require("gulp-notify"),     // Нотификатор
    insert = require('gulp-insert'),
    minify = require('gulp-minify');


// Очистка результирующей папки
gulp.task('clean', function() {
  del('web/wp-content/themes/spst/**', function () {
    console.log('Files deleted');
  });
});


// Форматирование стилей

gulp.task('csscomb', function() {
  return gulp.src('./dev/scss/**/*.scss')
    .pipe(csscomb())
    .pipe(gulp.dest('./dev/scss'));
});


function scssTask() {
  return gulp.src(config.build.src.scss)
    .pipe(scss())
    .pipe(cssmin().on('error', function(err) {
        console.log(err);
    }))
    //.pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(config.build.dest.root));
}


// Собираем css из Compass
gulp.task('scss', function() {
  scssTask()
    .pipe(reload({ stream: true }));
});


gulp.task('css-other', function() {
  return gulp.src(config.build.src.css)
    .pipe(gulp.dest(config.build.dest.css))
    .pipe(reload({ stream: true }));
});

// Собираем html из Jade
gulp.task('jade', function() {
  return gulp.src(config.build.src.html)
    .pipe(newer(config.build.dest.html, '.html'))
    .pipe(plumber({ errorHandler: notify.onError("<%= error.message %>") }))
    .pipe(jade({
      pretty: '\t'  // Set to false to minify/uglify the PHP
    }))
    .pipe(gulp.dest(config.build.dest.html))
    .pipe(reload({ stream: true }));
});


// Собираем JS
gulp.task('js', function() {
  return gulp.src(config.build.src.js)
    .pipe(wrapper({
       header: '\n// ${filename}\n\n',
       footer: '\n'
    }))
    .pipe(order(config.build.src.js_order))
    .pipe(concat('main.js'))
    //.pipe(insert.wrap('window.onload = function() {', '}'))  
    .pipe(minify({
        ext:{
            src:'.js',
            min:'-min.js'
        },
        exclude: [],
        ignoreFiles: []
    }))    
    .pipe(gulp.dest(config.build.dest.js))
    .pipe(reload({ stream: true }));
});

gulp.task('js-vendor', function() {
  return gulp.src(config.build.src.js_vendor)
    .pipe(order(config.build.src.js_vendor_order))
    .pipe(concat('plugins.js'))
    .pipe(minify({
        ext:{
            src:'.js',
            min:'-min.js'
        },
        exclude: [],
        ignoreFiles: []
    }))    
    .pipe(gulp.dest(config.build.dest.js))
    .pipe(reload({ stream: true }));
});

gulp.task('js-other', function() {
  return gulp.src(config.build.src.js_other)
    .pipe(wrapper({
       header: '\n// ${filename}\n\n',
       footer: '\n'
    }))
    .pipe(gulp.dest(config.build.dest.js))
    .pipe(reload({ stream: true }));
});


// Копируем изображения
gulp.task('images', function() {
  return gulp.src(config.build.src.img)
    .pipe(newer(config.build.dest.img))
    .pipe(gulp.dest(config.build.dest.img))
    .pipe(reload({ stream: true }));
});

// Копируем php
gulp.task('php', function() {
  return gulp.src(config.build.src.php)
    .pipe(newer(config.build.dest.php))
    .pipe(gulp.dest(config.build.dest.php))
    .pipe(reload({ stream: true }));
});

gulp.task('wp-plugins', function() {
  return gulp.src(config.build.src.plugins)
    .pipe(newer(config.build.dest.plugins))
    .pipe(gulp.dest(config.build.dest.plugins))
    .pipe(reload({ stream: true }));
});

// Копируем шрифты
gulp.task('fonts', function() {
  return gulp.src(config.build.src.fonts)
    .pipe(newer(config.build.dest.fonts))
    .pipe(gulp.dest(config.build.dest.fonts))
    .pipe(reload({ stream: true }));
});





// Локальный сервер для разработки
// http://www.browsersync.io/docs/options/
gulp.task('server', function () {
  var files = [
    config.build.dest.html,
    config.build.dest.css,
    config.build.dest.img,
    config.build.dest.fonts,
    config.build.dest.js
  ];

  browserSync(files, {
    server: {
       baseDir: 'web/wp-content/themes/spst/'
    },
    port: 8888,
    notify: false,
    // browser: ["google chrome", "firefox", "safari"],
    open: false,
    ghostMode: {
      clicks: true,
      location: true,
      forms: true,
      scroll: true
    }
  });
});


// Запуск сервера разработки
gulp.task('watch', ['jade', 'php', 'wp-plugins', 'images', 'fonts', 'css-other',  'scss', 'js', 'js-other', 'js-vendor', 'server'], function() {
  gulp.watch(config.watch.jade, ['jade']);
  gulp.watch(config.watch.img, ['images']);
  gulp.watch(config.watch.php, ['php']);
  gulp.watch(config.watch.plugins, ['wp-plugins']);
  gulp.watch(config.watch.fonts, ['fonts']);
  gulp.watch(config.watch.scss, ['scss']);
  gulp.watch(config.watch.css, ['css-other']);
  gulp.watch(config.watch.js, ['js', 'js-other']);
  gulp.watch(config.watch.js_vendor, ['js-vendor']);
});


// Сборка неминимизированного проекта
gulp.task('build', ['jade', 'php', 'wp-plugins', 'images', 'fonts', 'css-other', 'scss', 'js', 'js-other', 'js-vendor'], function() {
});


// Сборка минимизированного проекта
gulp.task('production', ['clean'], function() {
  // css
  compassTask()
    .pipe(csso())
    .pipe(gulp.dest(config.build.dest.css));

  // jade
  gulp.src(config.build.src.html)
    .pipe(plumber({ errorHandler: notify.onError("<%= error.message %>") }))
    .pipe(jade())
    .pipe(gulp.dest(config.build.dest.html));

  // js
  gulp.src(config.build.src.js)
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest(config.build.dest.js));

  gulp.src(config.build.src.js_vendor)
    .pipe(order(config.build.src.js_order))
    .pipe(concat('plugins.js'))
    .pipe(gulp.dest(config.build.dest.js));

  gulp.src(config.build.src.js_other)
    .pipe(uglify())
    .pipe(gulp.dest(config.build.dest.js));

  // image
  gulp.src(config.build.src.img)
    .pipe(imagemin())
    .pipe(gulp.dest(config.build.dest.img));

  // fonts
  gulp.src(config.build.src.fonts)
    .pipe(gulp.dest(config.build.dest.fonts));
});


/**========= Загрузка на удаленный сервер =========*/

/** upload index */
gulp.task('upload-index', function () {
  gulp.src(config.build.dest.html + 'index.html')
    .pipe(ftp(config.server));
});
/** upload html */
gulp.task('upload-html', function () {
  var settings = JSON.parse(JSON.stringify(config.server));
  settings.remotePath += '/views';
  gulp.src(config.build.dest.html + 'views/*.html')
    .pipe(ftp(settings));
});

/** upload js */
gulp.task('upload-js', function () {
  var settings = JSON.parse(JSON.stringify(config.server));
  settings.remotePath += '/js';
  gulp.src(config.build.dest.js + '/**/*')
    .pipe(ftp(settings));
});

/** upload css */
gulp.task('upload-css', function () {
  var settings = JSON.parse(JSON.stringify(config.server));
  settings.remotePath += '/css';
  gulp.src(config.build.dest.css + '/**/*')
    .pipe(ftp(settings));
});

gulp.task('upload', ['upload-index', 'upload-html', 'upload-js', 'upload-css']);



gulp.task('default', ['watch']);


// PhoneGap build
// Копируем in app/www
gulp.task('cssApp', function() {
  return gulp.src(config.buildApp.src.css)
    .pipe(newer(config.buildApp.dest.css))
    .pipe(gulp.dest(config.buildApp.dest.css))
    .pipe(reload({ stream: true }));
});

gulp.task('jsApp', function() {
  return gulp.src(config.buildApp.src.js)
    .pipe(newer(config.buildApp.dest.js))
    .pipe(gulp.dest(config.buildApp.dest.js))
    .pipe(reload({ stream: true }));
});

gulp.task('imagesApp', function() {
  return gulp.src(config.buildApp.src.img)
    .pipe(newer(config.buildApp.dest.img))
    .pipe(gulp.dest(config.buildApp.dest.img))
    .pipe(reload({ stream: true }));
});

// Копируем шрифты
gulp.task('fontsApp', function() {
  return gulp.src(config.buildApp.src.fonts)
    .pipe(newer(config.buildApp.dest.fonts))
    .pipe(gulp.dest(config.buildApp.dest.fonts))
    .pipe(reload({ stream: true }));
});

gulp.task('htmlApp', function() {
  return gulp.src(config.buildApp.src.fonts)
    .pipe(newer(config.buildApp.dest.fonts))
    .pipe(gulp.dest(config.buildApp.dest.fonts))
    .pipe(reload({ stream: true }));
});

// Запуск buildApp
gulp.task('buildApp', ['clean'], function () {
  gulp.start(['cssApp', 'jsApp', 'imagesApp', 'fontsApp', 'htmlApp']);
});
