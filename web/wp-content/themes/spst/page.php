<!DOCTYPE html>
<html lang="ru"></html>
<head>
	<meta charset="UTF-8"><title><?php if ( is_front_page() && is_home() ) { echo 'Home'; } elseif (is_home () ){ echo 'Home';} elseif ( is_front_page() ){ echo 'Home';}  else {wp_title('',true);} ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1">
	<?php wp_head(); ?>
</head>
<body class="<?php is_front_page(); is_user_logged_in(); ?>"> 
	<noscript></noscript>
	<div class="preloader">
		<div class="container">
			<div class="icon-one-coin"><img src="/wp-content/uploads/2018/02/iqc.png"></div>
			<div class="loading-text"><span class="loading-text-words">L</span><span class="loading-text-words">O</span><span class="loading-text-words">A</span><span class="loading-text-words">D</span><span class="loading-text-words">I</span><span class="loading-text-words">N</span><span class="loading-text-words">G</span></div>
		</div>
	</div>
	<?php get_header(); ?>
	<div class="content-wrapper"><?php while ( have_posts() ) : the_post();
   the_content();
endwhile;
$blocks = get_field('blocks');
foreach ($blocks as $block) {
    setup_postdata( $block );
    echo "<script>";
    echo get_field('js_code',  $block->ID, false);
    echo "</script>";
    echo "<style>";
    echo get_field('css_code',  $block->ID, false);
    echo "</style>";
    echo "<div class='".get_field('block_type',  $block->ID)."'>";
    $content_post = get_post($block->ID);
    $content = $content_post->post_content;
    $content = do_shortcode($content);
    echo $content;
    echo "</div>";
}?>
	</div>
	<?php get_footer(); ?>
	<?php wp_footer(); ?>
</body>