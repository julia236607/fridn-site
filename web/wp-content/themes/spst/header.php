
<div class="header">
	<div class="wraper-container"><a href="<?php echo home_url()?>" class="custom-logo">
			<div class="logo"></div></a>
		<?php wp_nav_menu( array('menu' => 'main-nav', 'theme_location' => 'main-nav', 'walker' => new UIKIT_Walker, 'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>') ); ?>
		<div class="header-sidebar">
			<?php dynamic_sidebar( 'header' ); ?>
			<div class="btn-background">
				<button id="header-btn" class="transp-btn"> <img src="/wp-content/uploads/2018/01/close-1.png" class="close"/><span>Регистрация</span></button>
			</div>
			<div class="reg-box">
				<div class="reg-menu">
					<div class="timer-wrapper">
						<div class="time-info">
							<div class="time">
								<p class="small-text">Регистируйся и полуйчай скидку на приложение</p>
								<div class="timer-block">
									<div class="day item">
										<div class="numb days"></div>
										<div class="small-text">Дней</div>
									</div>
									<div class="separator"> </div>
									<div class="item">
										<div class="numb hours"></div>
										<div class="small-text">Часов</div>
									</div>
									<div class="colon">
										<div class="doto"></div>
										<div class="doto"></div>
									</div>
									<div class="item">
										<div class="numb minutes"></div>
										<div class="small-text">Минут</div>
									</div>
									<div class="colon">
										<div class="doto"></div>
										<div class="doto"></div>
									</div>
									<div class="item">
										<div class="numb seconds"></div>
										<div class="small-text">Секунд</div>
									</div>
								</div>
							</div><img src="/wp-content/uploads/2018/02/information.png" class="info"/>
						</div>
						<div class="gen-form"><?php if (get_locale() == "ru_RU"){
  echo do_shortcode('[ninja_form id=6]');
}
if (get_locale() == "en_US"){
  echo do_shortcode('[ninja_form id=9]');
}
if (get_locale() == "de_DE"){
  echo do_shortcode('[ninja_form id=12]');
}
if (get_locale() == "zh_CN"){
  echo do_shortcode('[ninja_form id=15]');
}?>
						</div>
					</div>
					<div class="reg-separator"></div>
					<div class="progressbar">
						<div class="progress">
							<div class="percent50">50%</div>
							<div class="percent30">30%</div>
							<div class="percent15">15%</div>
							<div class="bar">
								<div class="pointer"></div>
							</div>
						</div>
						<div class="members"><span class="first">0 чел</span><span>200 чел</span><span>500 чел</span><span class="last">1000 чел</span></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="backghound-shadow"></div>