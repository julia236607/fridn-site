<?php

remove_filter('the_content','wpautop');

//decide when you want to apply the auto paragraph

add_filter('the_content','my_custom_formatting');

function my_custom_formatting($content){
if(get_post_type()=='block') //if it does not work, you may want to pass the current post object to get_post_type
    return wpautop($content);//no autop
else
 return $content;
}


//--Show news n quotes

function add_show_news( $atts ){
  // параметры по умолчанию
  $args = array(
    'category'    => 4,
    'orderby'     => 'date',
    'order'       => 'DESC',
    'post_type'   => 'post'
  );

  $posts = get_posts( $args );

  $cont_quotes = "";

  foreach($posts as $post) {
    $cont_quotes .= "<div class='swiper-slide'>";
    $cont_quotes .= "<div class='img-new'>".get_the_post_thumbnail($post->ID)."</div>";
    $cont_quotes .= "<p class='title'><a href='".get_field("url", $post->ID)."' class='none-underline' target='_blank'>";
    $cont_quotes .= get_the_title($post->ID)."</a></p>";
    $cont_quotes .= "<h6>".$post->post_content."</h6>";
    // $cont_quotes .= "<div class='social-state'><div class='social-state__container'>";
    // $cont_quotes .= "<a href='".get_field("url_vk", $post->ID)."' class='none-underline'><i aria-hidden='true' class='fa fa-vk'></i></a>";
    // $cont_quotes .= "<a href='".get_field("url_tw", $post->ID)."' class='none-underline'><i aria-hidden='true' class='fa fa-twitter'></i></a>";
    // $cont_quotes .= "<a href='".get_field("url_fb", $post->ID)."' class='none-underline'><i aria-hidden='true' class='fa fa-facebook'></i></a>";
    // $cont_quotes .= "</div></div>";
    $cont_quotes .= "</div>";
  }
  return $cont_quotes;

  //return  var_dump($posts);
}
add_shortcode('show_news', 'add_show_news');

function add_show_quotes( $atts ){
  // параметры по умолчанию
  $args = array(
    'category'    => 5,
    'orderby'     => 'date',
    'order'       => 'DESC',
    'post_type'   => 'post'
  );

  $posts = get_posts( $args );

  $cont_quotes = "";

  foreach($posts as $post) {
    $cont_quotes .= "<div class='swiper-slide'>";
    $cont_quotes .= "<div class='img-new'>".get_the_post_thumbnail($post->ID)."</div>";
    $cont_quotes .= "<p class='author'>".get_the_title($post->ID)."</p>";
    $cont_quotes .= "<h6>".get_field("state_", $post->ID)."</h6>";
    $cont_quotes .= "<h6 class='statement'>".$post->post_content."</h6>";
    // $cont_quotes .= "<div class='social-state'><div class='social-state__container'>";
    // $cont_quotes .= "<a href='".get_field("url_vk", $post->ID)."' class='none-underline'><i aria-hidden='true' class='fa fa-vk'></i></a>";
    // $cont_quotes .= "<a href='".get_field("url_tw", $post->ID)."' class='none-underline'><i aria-hidden='true' class='fa fa-twitter'></i></a>";
    // $cont_quotes .= "<a href='".get_field("url_fb", $post->ID)."' class='none-underline'><i aria-hidden='true' class='fa fa-facebook'></i></a>";
    // $cont_quotes .= "</div></div>";
    $cont_quotes .= "</div>";
  }
  return $cont_quotes;
}
add_shortcode('show_quotes', 'add_show_quotes');

//--/Show news n quotes

class UIKIT_Walker extends Walker_Nav_Menu {

  function start_el(&$output, $item, $depth, $args) {
      $liAttributes = "";
      $aAttributes = "";
      $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

      $class_names = $value = '';

      $classes = empty( $item->classes ) ? array() : (array) $item->classes;
      $classes[] = 'menu-item-' . $item->ID;

      $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
      $class_names = in_array("current_page_item",$item->classes) ? ' uk-active' : '';
      //$class_names = in_array("current_menu_ancestor",$item->classes) ? ' uk-active' : '';

      if ( $args->walker->has_children ) {
          $class_names .= ' menu-item';
          if ($args->viewMode == "mobile") {
              $liAttributes .= ' aria-expanded="false" ';
              $liAttributes .= '';
          } else {
              $liAttributes .= ' data-uk-dropdown="{mode:\'click\'}" aria-haspopup="true" aria-expanded="false" ';
              $aAttributes .= ' <i class="uk-icon-caret-down"></i>';
          }
      }

      $class_names = ' class="' . esc_attr( $class_names ) . '"';

      $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
      $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
      $idA = " id=menu-link-". $item->ID."  ";
      $output .= $indent . '<li' . $id . $value . $class_names . $liAttributes . '>';

      $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
      $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
      $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
      $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

      $item_output = $args->before;
      $item_output .= '<a'. $idA . $attributes .'>';

      $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after . $aAttributes;

      /*GET THE EXCERPT*/
      //$q = new WP_Query(array('post__in'=>$item->object_id));
      //if($q->have_posts()) : while($q->have_posts()) : $q->the_post();
      //    $item_output .= '<span class="menu-excerpt">'.get_the_excerpt().'</span>';
      //endwhile;endif;
      /*****************/

      $item_output .= '</a>';
      $item_output .= $args->after;

      $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }


  function start_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    if ($args->viewMode == "mobile") {
        //$output .= "\n$indent<div style='overflow: hidden; position: relative; height: 0px;' class='uk-hidden'><ul class='uk-nav-sub' role='menu'>\n";
        $output .= "\n$indent<ul  class='uk-nav-sub' role='menu'>\n";
    } else {
        $output .= "\n$indent<div class='header-swiper swiper-container' aria-hidden='true'><ul class='sub-menu swiper-wrapper'>\n";
    }
  }
  function end_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    if ($args->viewMode == "mobile") {
      $output .= "$indent</ul>\n";
    } else {
      $output .= "$indent</ul></div>\n";
    }
  }
}


function spst_scripts() {
  wp_enqueue_style('font', get_template_directory_uri() . '/css/font.css');
  wp_enqueue_style('font-awesome', get_template_directory_uri() . '/fonts/font-awesome/css/font-awesome.min.css');
  wp_enqueue_style('swiper', get_template_directory_uri() . '/css/swiper.min.css');
  wp_enqueue_style( 'style', get_stylesheet_uri() );
  wp_enqueue_script('plugins-min.js', get_template_directory_uri() . '/js/plugins-min.js');
  wp_enqueue_script('main.js', get_template_directory_uri() . '/js/main.js');
}
add_action('wp_enqueue_scripts', 'spst_scripts');

function spst_widgets_init() {

  register_sidebar( array(
    'name'          => __( 'Top Header Sidebar', 'spst' ),
    'id'            => 'header',
    'description'   => __( 'Header sidebar (slogan, top-menu, search).', 'spst' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h1 class="widget-title">',
    'after_title'   => '</h1>',
  ) );


  register_sidebar( array(
    'name'          => __( 'Footer  Widget Area', 'spst' ),
    'id'            => 'footer',
    'description'   => __( 'Appears in the footer section of the site.', 'spst' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );  

}
add_action( 'widgets_init', 'spst_widgets_init' );


function flexible_widget_titles( $widget_title ) {
    $title = trim( $widget_title );
    if ( $title[0] == '[' && $title[strlen($title) - 1] == ']' ) $title = '';
    return $title;
}
 
add_filter( 'widget_title', 'flexible_widget_titles' );


add_filter( 'wp_default_scripts', 'dequeue_jquery_migrate' );

function dequeue_jquery_migrate( &$scripts){
    if(!is_admin()){
        //$scripts->remove( 'jquery');
    }
}

function register_menu() {
  if ( ! is_admin() ) {
    //wp_dequeue_script( 'jquery' );
    //wp_enqueue_script( 'plugins', get_template_directory_uri() . '/js/plugins.js ');
    //wp_enqueue_style( 'main', get_template_directory_uri().'/css/main.css');
    //wp_enqueue_style( 'paradigma', get_template_directory_uri().'/css/paradigma.css');
    //wp_enqueue_style( 'swiper', get_template_directory_uri().'/css/swiper.css');
    //wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js');
  }
}
add_action( 'init', 'register_menu' );


add_action( 'init', 'create_blocks_posttype' );
function create_blocks_posttype() {
  register_post_type( 'block',
    array(
      'labels' => array(
        'name' => __( 'Blocks' ),
        'singular_name' => __( 'Block' )
      ),
     'menu_icon' => 'dashicons-exerpt-view',
      'public' => true,
      'capability_type'       => 'page'
    )
  );
}

/**
 * Hide the admin bar outside page of administration
 */
add_action('after_setup_theme', function(){
  add_theme_support('post-thumbnails');
    if ( ! is_user_logged_in() ) show_admin_bar( false );
});


function register_my_menus() {
  register_nav_menus(
    array(
      'main-nav' => __( 'Main Menu' ),
      'footer-nav' => __( 'Footer Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

//Allow SVG through WordPress Media Uploader
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


/*
function lng_switcher_func( $atts ) {
  ob_start();
  the_widget('qTranslateXWidget', array('type' => 'both', 'hide-title' => true) );
  $w = ob_get_contents();
  ob_end_clean();
  $currentLang = translate("[:en]English[:es]Español[:]");
  $w = str_replace('language-chooser language-chooser-both qtranxs_language_chooser', 'uk-nav uk-nav-dropdown', $w);
  $start = '<div class="uk-button-dropdown" data-uk-dropdown="{mode:\'click\'}" aria-haspopup="false" aria-expanded="false" style="margin-top: -2px;"><button class="uk-button white-bg">'.$currentLang.'<i class="uk-icon-caret-down uk-margin-large-left"></i></button><div class="uk-dropdown uk-dropdown-bottom" aria-hidden="false" style="width:110px; top: 30px;left: 10px;padding-top: 0;
    padding-bottom: 0;" tabindex="">';
  $end = '</div></div>';
  return $start.$w.$end;
}
add_shortcode( 'lng_switcher', 'lng_switcher_func' );
*/

function mailchimp_subscribe() {
  $api_key = '2e6c1a63a6db2c2807557858e96b2512-us14';
  $dc = 'us14'; // дата-центр MailChimp
  $list_id = '81ffdd8e62';
  $email = 'ghost850@gmail.com';

  $url = "https://$dc.api.mailchimp.com/2.0/lists/subscribe.json";

  $request = wp_remote_post( $url, array(
      'body' => json_encode( array(
          'apikey' => $api_key,
          'id' => $list_id,
          'email' => array( 'email' => $email ),
      ) ),
  ) );

  $result = json_decode( wp_remote_retrieve_body( $request ) );

}

?>
