
// vendor/_max-swiper.js

"use strict";

//---------StartNow Swiper---------
function MaxSwiper(container, params) {

    if( $(container).length ) {

        this.params = params;

        if(this.params.wayBlock == undefined) {
            this.params.wayBlock = ".slide-block";
        }
        if(this.params.wayIcons == undefined) {
            this.params.wayIcons = ".icons-block .icon";
        }
        
        this.slideBlock = $(container+">"+this.params.wayBlock);
        this.slides =  this.slideBlock.children(".slide");
        this.itemIcons = $(container+">"+this.params.wayIcons).children("div");
        
        let _self = this;
        

        //--set num
        for(let i = 0; i < this.slides.length; i++) {
            this.itemIcons.eq(i).data("numslide", i);
            this.slides.eq(i).data("numslide", i);
        }
        //--/set num

        //--start after load page
        this.slides.css({
            "opacity": "0",
            "display": "none"
        });
        this.slides.eq(0).addClass("active");
        this.itemIcons.eq(0).addClass("active");
        this.fadeInCustom( this.slides.eq(0) );
        //--/start after load page

        //--switch by icon click
        this.itemIcons.on("click", function(event){
                let numSlide = $(this).data("numslide");
                _self.switchSlide(numSlide);
        })
        //--/switch by icon click

        //--Swipe & Pan switch

        // create a simple instance
        // by default, it only adds horizontal recognizers
        var hammer = new Hammer(this.slideBlock[0] );

        this.doSwipe = false;
        this.allowSwitch = true;

        let     isDragging = false,
                isLeftPan = false,
                isRightPan = false;

        let beginPosMouseX = 0,
            deviation = 0;

        let numShowedSlide = undefined;

        let distAbsSwitch = this.slideBlock.width() / 4 * 3;
        $(window).resize(function(){
            distAbsSwitch = _self.slideBlock.width()/4 * 3;
        })

        hammer.on('swipe', function(ev){
           if ( _self.allowSwitch ){
                _self.doSwipe = true;
            }
        });
        hammer.on("pan", function(ev){
            if ( _self.allowSwitch ){

            if ( ! isDragging ) {
                isDragging = true;
                beginPosMouseX = ev.deltaX;
            }
            deviation = beginPosMouseX - ev.deltaX;
            //setDinamic(deviation); //test

            if(0 < deviation && deviation <= distAbsSwitch){

                _self.slides.filter(".active").css("opacity", String( 1 - Math.abs(deviation)/distAbsSwitch ) );
                
                if( ! isLeftPan ){
                    if(numShowedSlide != undefined){
                        _self.slides.eq(numShowedSlide).css({
                            "opacity": "0",
                            "display": "none" 
                        });
                    }
                    numShowedSlide = _self.numOfNext();
                    isLeftPan = true;
                    isRightPan = false;
                    _self.slides.eq(numShowedSlide).css("display", "flex");
                }

                _self.slides.eq(numShowedSlide).css( "opacity", String( Math.abs(deviation)/distAbsSwitch ) );
            }
            
            if (0 > deviation && deviation >= -distAbsSwitch) {

                _self.slides.filter(".active").css("opacity", String( 1 - Math.abs(deviation)/distAbsSwitch ) );
                
                if( ! isRightPan ){
                    if(numShowedSlide != undefined){
                        _self.slides.eq(numShowedSlide).css({ 
                            "opacity": "0",
                            "display": "none" 
                        });
                    }
                    numShowedSlide = _self.numOfPrev();
                    isLeftPan = false;
                    isRightPan = true;
                    _self.slides.eq(numShowedSlide).css("display", "flex");
                }

                _self.slides.eq(numShowedSlide).css( "opacity", String( Math.abs(deviation)/distAbsSwitch ) );
            }

            if (ev.isFinal && numShowedSlide !=undefined) {
                isDragging = false;
                isLeftPan = false;
                isRightPan = false;
                if( Math.abs(deviation) > distAbsSwitch/2 || _self.doSwipe) {
                    _self.switchSlide(numShowedSlide);
                }else{
                    _self.fadeOutCustom( _self.slides.eq(numShowedSlide) );
                    _self.slides.filter(".active").animate({opacity: "1"}, 800);
                }
                numShowedSlide = undefined;
            }

            }
        });

        //--/Swipe & Pan switch
    }


}

//--fade function
MaxSwiper.prototype.fadeInCustom = function(elem) {
    let animSpeed = Math.floor( ( 1-elem.css("opacity") )*200 );
    elem.css("display", "flex");
    elem.animate({
            opacity: 1,
        },animSpeed, "linear")
}
MaxSwiper.prototype.fadeOutCustom = function(elem) {
    let _self = this;
    let animSpeed = Math.floor( elem.css("opacity") * 200 );
    elem.animate({
        opacity: 0,
    },
    animSpeed, "linear",
    function(){
        elem.css("display", "none");
        _self.doSwipe = false;
        _self.allowSwitch = true;
        if(elem.hasClass("active")){
            elem.css("display", "flex");
        }
    })
}
//--/fade function


//--numOfNext&prev
MaxSwiper.prototype.numOfNext = function(){
    let num = this.slides.filter(".active").data("numslide")+1;
    if( num >= this.slides.length ){
        num = 0;
    }
    return num;
}
MaxSwiper.prototype.numOfPrev = function(){
    let num = this.slides.filter(".active").data("numslide")-1;
    if( num <= -1 ){
        num = this.slides.length-1;
    }
    return num;
}
//--/numOfNext&prev


//--switch function
MaxSwiper.prototype.switchSlide = function(num) {
    let slideElem = this.slides.eq(num);
    if( ! slideElem.hasClass("active") ){
        this.allowSwitch = false;
        this.fadeOutCustom ( this.slides.filter(".active") );
        this.slides.filter(".active").removeClass("active");
        this.itemIcons.filter(".active").removeClass("active");
        slideElem.addClass("active");
        this.itemIcons.eq(num).addClass("active");
        this.fadeInCustom( slideElem );
    }
}
//--/switch function

//---------/StartNow Swiper---------
