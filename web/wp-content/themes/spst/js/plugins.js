
/*!
 * Chart.js
 * http://chartjs.org/
 * Version: 2.7.0
 *
 * Copyright 2017 Nick Downie
 * Released under the MIT license
 * https://github.com/chartjs/Chart.js/blob/master/LICENSE.md
 */
!function(t){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=t();else if("function"==typeof define&&define.amd)define([],t);else{("undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this).Chart=t()}}(function(){return function t(e,n,i){function a(r,l){if(!n[r]){if(!e[r]){var s="function"==typeof require&&require;if(!l&&s)return s(r,!0);if(o)return o(r,!0);var u=new Error("Cannot find module '"+r+"'");throw u.code="MODULE_NOT_FOUND",u}var d=n[r]={exports:{}};e[r][0].call(d.exports,function(t){var n=e[r][1][t];return a(n||t)},d,d.exports,t,e,n,i)}return n[r].exports}for(var o="function"==typeof require&&require,r=0;r<i.length;r++)a(i[r]);return a}({1:[function(t,e,n){},{}],2:[function(t,e,n){function i(t){if(t){var e=/^#([a-fA-F0-9]{3})$/i,n=/^#([a-fA-F0-9]{6})$/i,i=/^rgba?\(\s*([+-]?\d+)\s*,\s*([+-]?\d+)\s*,\s*([+-]?\d+)\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/i,a=/^rgba?\(\s*([+-]?[\d\.]+)\%\s*,\s*([+-]?[\d\.]+)\%\s*,\s*([+-]?[\d\.]+)\%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/i,o=/(\w+)/,r=[0,0,0],l=1,s=t.match(e);if(s){s=s[1];for(d=0;d<r.length;d++)r[d]=parseInt(s[d]+s[d],16)}else if(s=t.match(n)){s=s[1];for(d=0;d<r.length;d++)r[d]=parseInt(s.slice(2*d,2*d+2),16)}else if(s=t.match(i)){for(d=0;d<r.length;d++)r[d]=parseInt(s[d+1]);l=parseFloat(s[4])}else if(s=t.match(a)){for(d=0;d<r.length;d++)r[d]=Math.round(2.55*parseFloat(s[d+1]));l=parseFloat(s[4])}else if(s=t.match(o)){if("transparent"==s[1])return[0,0,0,0];if(!(r=c[s[1]]))return}for(var d=0;d<r.length;d++)r[d]=u(r[d],0,255);return l=l||0==l?u(l,0,1):1,r[3]=l,r}}function a(t){if(t){var e=/^hsla?\(\s*([+-]?\d+)(?:deg)?\s*,\s*([+-]?[\d\.]+)%\s*,\s*([+-]?[\d\.]+)%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)/,n=t.match(e);if(n){var i=parseFloat(n[4]);return[u(parseInt(n[1]),0,360),u(parseFloat(n[2]),0,100),u(parseFloat(n[3]),0,100),u(isNaN(i)?1:i,0,1)]}}}function o(t){if(t){var e=/^hwb\(\s*([+-]?\d+)(?:deg)?\s*,\s*([+-]?[\d\.]+)%\s*,\s*([+-]?[\d\.]+)%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)/,n=t.match(e);if(n){var i=parseFloat(n[4]);return[u(parseInt(n[1]),0,360),u(parseFloat(n[2]),0,100),u(parseFloat(n[3]),0,100),u(isNaN(i)?1:i,0,1)]}}}function r(t,e){return void 0===e&&(e=void 0!==t[3]?t[3]:1),"rgba("+t[0]+", "+t[1]+", "+t[2]+", "+e+")"}function l(t,e){return"rgba("+Math.round(t[0]/255*100)+"%, "+Math.round(t[1]/255*100)+"%, "+Math.round(t[2]/255*100)+"%, "+(e||t[3]||1)+")"}function s(t,e){return void 0===e&&(e=void 0!==t[3]?t[3]:1),"hsla("+t[0]+", "+t[1]+"%, "+t[2]+"%, "+e+")"}function u(t,e,n){return Math.min(Math.max(e,t),n)}function d(t){var e=t.toString(16).toUpperCase();return e.length<2?"0"+e:e}var c=t(6);e.exports={getRgba:i,getHsla:a,getRgb:function(t){var e=i(t);return e&&e.slice(0,3)},getHsl:function(t){var e=a(t);return e&&e.slice(0,3)},getHwb:o,getAlpha:function(t){var e=i(t);return e?e[3]:(e=a(t))?e[3]:(e=o(t))?e[3]:void 0},hexString:function(t){return"#"+d(t[0])+d(t[1])+d(t[2])},rgbString:function(t,e){return e<1||t[3]&&t[3]<1?r(t,e):"rgb("+t[0]+", "+t[1]+", "+t[2]+")"},rgbaString:r,percentString:function(t,e){return e<1||t[3]&&t[3]<1?l(t,e):"rgb("+Math.round(t[0]/255*100)+"%, "+Math.round(t[1]/255*100)+"%, "+Math.round(t[2]/255*100)+"%)"},percentaString:l,hslString:function(t,e){return e<1||t[3]&&t[3]<1?s(t,e):"hsl("+t[0]+", "+t[1]+"%, "+t[2]+"%)"},hslaString:s,hwbString:function(t,e){return void 0===e&&(e=void 0!==t[3]?t[3]:1),"hwb("+t[0]+", "+t[1]+"%, "+t[2]+"%"+(void 0!==e&&1!==e?", "+e:"")+")"},keyword:function(t){return h[t.slice(0,3)]}};var h={};for(var f in c)h[c[f]]=f},{6:6}],3:[function(t,e,n){var i=t(5),a=t(2),o=function(t){if(t instanceof o)return t;if(!(this instanceof o))return new o(t);this.valid=!1,this.values={rgb:[0,0,0],hsl:[0,0,0],hsv:[0,0,0],hwb:[0,0,0],cmyk:[0,0,0,0],alpha:1};var e;"string"==typeof t?(e=a.getRgba(t))?this.setValues("rgb",e):(e=a.getHsla(t))?this.setValues("hsl",e):(e=a.getHwb(t))&&this.setValues("hwb",e):"object"==typeof t&&(void 0!==(e=t).r||void 0!==e.red?this.setValues("rgb",e):void 0!==e.l||void 0!==e.lightness?this.setValues("hsl",e):void 0!==e.v||void 0!==e.value?this.setValues("hsv",e):void 0!==e.w||void 0!==e.whiteness?this.setValues("hwb",e):void 0===e.c&&void 0===e.cyan||this.setValues("cmyk",e))};o.prototype={isValid:function(){return this.valid},rgb:function(){return this.setSpace("rgb",arguments)},hsl:function(){return this.setSpace("hsl",arguments)},hsv:function(){return this.setSpace("hsv",arguments)},hwb:function(){return this.setSpace("hwb",arguments)},cmyk:function(){return this.setSpace("cmyk",arguments)},rgbArray:function(){return this.values.rgb},hslArray:function(){return this.values.hsl},hsvArray:function(){return this.values.hsv},hwbArray:function(){var t=this.values;return 1!==t.alpha?t.hwb.concat([t.alpha]):t.hwb},cmykArray:function(){return this.values.cmyk},rgbaArray:function(){var t=this.values;return t.rgb.concat([t.alpha])},hslaArray:function(){var t=this.values;return t.hsl.concat([t.alpha])},alpha:function(t){return void 0===t?this.values.alpha:(this.setValues("alpha",t),this)},red:function(t){return this.setChannel("rgb",0,t)},green:function(t){return this.setChannel("rgb",1,t)},blue:function(t){return this.setChannel("rgb",2,t)},hue:function(t){return t&&(t=(t%=360)<0?360+t:t),this.setChannel("hsl",0,t)},saturation:function(t){return this.setChannel("hsl",1,t)},lightness:function(t){return this.setChannel("hsl",2,t)},saturationv:function(t){return this.setChannel("hsv",1,t)},whiteness:function(t){return this.setChannel("hwb",1,t)},blackness:function(t){return this.setChannel("hwb",2,t)},value:function(t){return this.setChannel("hsv",2,t)},cyan:function(t){return this.setChannel("cmyk",0,t)},magenta:function(t){return this.setChannel("cmyk",1,t)},yellow:function(t){return this.setChannel("cmyk",2,t)},black:function(t){return this.setChannel("cmyk",3,t)},hexString:function(){return a.hexString(this.values.rgb)},rgbString:function(){return a.rgbString(this.values.rgb,this.values.alpha)},rgbaString:function(){return a.rgbaString(this.values.rgb,this.values.alpha)},percentString:function(){return a.percentString(this.values.rgb,this.values.alpha)},hslString:function(){return a.hslString(this.values.hsl,this.values.alpha)},hslaString:function(){return a.hslaString(this.values.hsl,this.values.alpha)},hwbString:function(){return a.hwbString(this.values.hwb,this.values.alpha)},keyword:function(){return a.keyword(this.values.rgb,this.values.alpha)},rgbNumber:function(){var t=this.values.rgb;return t[0]<<16|t[1]<<8|t[2]},luminosity:function(){for(var t=this.values.rgb,e=[],n=0;n<t.length;n++){var i=t[n]/255;e[n]=i<=.03928?i/12.92:Math.pow((i+.055)/1.055,2.4)}return.2126*e[0]+.7152*e[1]+.0722*e[2]},contrast:function(t){var e=this.luminosity(),n=t.luminosity();return e>n?(e+.05)/(n+.05):(n+.05)/(e+.05)},level:function(t){var e=this.contrast(t);return e>=7.1?"AAA":e>=4.5?"AA":""},dark:function(){var t=this.values.rgb;return(299*t[0]+587*t[1]+114*t[2])/1e3<128},light:function(){return!this.dark()},negate:function(){for(var t=[],e=0;e<3;e++)t[e]=255-this.values.rgb[e];return this.setValues("rgb",t),this},lighten:function(t){var e=this.values.hsl;return e[2]+=e[2]*t,this.setValues("hsl",e),this},darken:function(t){var e=this.values.hsl;return e[2]-=e[2]*t,this.setValues("hsl",e),this},saturate:function(t){var e=this.values.hsl;return e[1]+=e[1]*t,this.setValues("hsl",e),this},desaturate:function(t){var e=this.values.hsl;return e[1]-=e[1]*t,this.setValues("hsl",e),this},whiten:function(t){var e=this.values.hwb;return e[1]+=e[1]*t,this.setValues("hwb",e),this},blacken:function(t){var e=this.values.hwb;return e[2]+=e[2]*t,this.setValues("hwb",e),this},greyscale:function(){var t=this.values.rgb,e=.3*t[0]+.59*t[1]+.11*t[2];return this.setValues("rgb",[e,e,e]),this},clearer:function(t){var e=this.values.alpha;return this.setValues("alpha",e-e*t),this},opaquer:function(t){var e=this.values.alpha;return this.setValues("alpha",e+e*t),this},rotate:function(t){var e=this.values.hsl,n=(e[0]+t)%360;return e[0]=n<0?360+n:n,this.setValues("hsl",e),this},mix:function(t,e){var n=this,i=t,a=void 0===e?.5:e,o=2*a-1,r=n.alpha()-i.alpha(),l=((o*r==-1?o:(o+r)/(1+o*r))+1)/2,s=1-l;return this.rgb(l*n.red()+s*i.red(),l*n.green()+s*i.green(),l*n.blue()+s*i.blue()).alpha(n.alpha()*a+i.alpha()*(1-a))},toJSON:function(){return this.rgb()},clone:function(){var t,e,n=new o,i=this.values,a=n.values;for(var r in i)i.hasOwnProperty(r)&&(t=i[r],"[object Array]"===(e={}.toString.call(t))?a[r]=t.slice(0):"[object Number]"===e?a[r]=t:console.error("unexpected color value:",t));return n}},o.prototype.spaces={rgb:["red","green","blue"],hsl:["hue","saturation","lightness"],hsv:["hue","saturation","value"],hwb:["hue","whiteness","blackness"],cmyk:["cyan","magenta","yellow","black"]},o.prototype.maxes={rgb:[255,255,255],hsl:[360,100,100],hsv:[360,100,100],hwb:[360,100,100],cmyk:[100,100,100,100]},o.prototype.getValues=function(t){for(var e=this.values,n={},i=0;i<t.length;i++)n[t.charAt(i)]=e[t][i];return 1!==e.alpha&&(n.a=e.alpha),n},o.prototype.setValues=function(t,e){var n,a=this.values,o=this.spaces,r=this.maxes,l=1;if(this.valid=!0,"alpha"===t)l=e;else if(e.length)a[t]=e.slice(0,t.length),l=e[t.length];else if(void 0!==e[t.charAt(0)]){for(n=0;n<t.length;n++)a[t][n]=e[t.charAt(n)];l=e.a}else if(void 0!==e[o[t][0]]){var s=o[t];for(n=0;n<t.length;n++)a[t][n]=e[s[n]];l=e.alpha}if(a.alpha=Math.max(0,Math.min(1,void 0===l?a.alpha:l)),"alpha"===t)return!1;var u;for(n=0;n<t.length;n++)u=Math.max(0,Math.min(r[t][n],a[t][n])),a[t][n]=Math.round(u);for(var d in o)d!==t&&(a[d]=i[t][d](a[t]));return!0},o.prototype.setSpace=function(t,e){var n=e[0];return void 0===n?this.getValues(t):("number"==typeof n&&(n=Array.prototype.slice.call(e)),this.setValues(t,n),this)},o.prototype.setChannel=function(t,e,n){var i=this.values[t];return void 0===n?i[e]:n===i[e]?this:(i[e]=n,this.setValues(t,i),this)},"undefined"!=typeof window&&(window.Color=o),e.exports=o},{2:2,5:5}],4:[function(t,e,n){function i(t){var e,n,i,a=t[0]/255,o=t[1]/255,r=t[2]/255,l=Math.min(a,o,r),s=Math.max(a,o,r),u=s-l;return s==l?e=0:a==s?e=(o-r)/u:o==s?e=2+(r-a)/u:r==s&&(e=4+(a-o)/u),(e=Math.min(60*e,360))<0&&(e+=360),i=(l+s)/2,n=s==l?0:i<=.5?u/(s+l):u/(2-s-l),[e,100*n,100*i]}function a(t){var e,n,i,a=t[0],o=t[1],r=t[2],l=Math.min(a,o,r),s=Math.max(a,o,r),u=s-l;return n=0==s?0:u/s*1e3/10,s==l?e=0:a==s?e=(o-r)/u:o==s?e=2+(r-a)/u:r==s&&(e=4+(a-o)/u),(e=Math.min(60*e,360))<0&&(e+=360),i=s/255*1e3/10,[e,n,i]}function o(t){var e=t[0],n=t[1],a=t[2];return[i(t)[0],100*(1/255*Math.min(e,Math.min(n,a))),100*(a=1-1/255*Math.max(e,Math.max(n,a)))]}function l(t){var e,n,i,a,o=t[0]/255,r=t[1]/255,l=t[2]/255;return a=Math.min(1-o,1-r,1-l),e=(1-o-a)/(1-a)||0,n=(1-r-a)/(1-a)||0,i=(1-l-a)/(1-a)||0,[100*e,100*n,100*i,100*a]}function s(t){return C[JSON.stringify(t)]}function u(t){var e=t[0]/255,n=t[1]/255,i=t[2]/255;return[100*(.4124*(e=e>.04045?Math.pow((e+.055)/1.055,2.4):e/12.92)+.3576*(n=n>.04045?Math.pow((n+.055)/1.055,2.4):n/12.92)+.1805*(i=i>.04045?Math.pow((i+.055)/1.055,2.4):i/12.92)),100*(.2126*e+.7152*n+.0722*i),100*(.0193*e+.1192*n+.9505*i)]}function d(t){var e,n,i,a=u(t),o=a[0],r=a[1],l=a[2];return o/=95.047,r/=100,l/=108.883,o=o>.008856?Math.pow(o,1/3):7.787*o+16/116,r=r>.008856?Math.pow(r,1/3):7.787*r+16/116,l=l>.008856?Math.pow(l,1/3):7.787*l+16/116,e=116*r-16,n=500*(o-r),i=200*(r-l),[e,n,i]}function c(t){var e,n,i,a,o,r=t[0]/360,l=t[1]/100,s=t[2]/100;if(0==l)return o=255*s,[o,o,o];e=2*s-(n=s<.5?s*(1+l):s+l-s*l),a=[0,0,0];for(var u=0;u<3;u++)(i=r+1/3*-(u-1))<0&&i++,i>1&&i--,o=6*i<1?e+6*(n-e)*i:2*i<1?n:3*i<2?e+(n-e)*(2/3-i)*6:e,a[u]=255*o;return a}function h(t){var e=t[0]/60,n=t[1]/100,i=t[2]/100,a=Math.floor(e)%6,o=e-Math.floor(e),r=255*i*(1-n),l=255*i*(1-n*o),s=255*i*(1-n*(1-o)),i=255*i;switch(a){case 0:return[i,s,r];case 1:return[l,i,r];case 2:return[r,i,s];case 3:return[r,l,i];case 4:return[s,r,i];case 5:return[i,r,l]}}function f(t){var e,n,i,a,o=t[0]/360,l=t[1]/100,s=t[2]/100,u=l+s;switch(u>1&&(l/=u,s/=u),e=Math.floor(6*o),n=1-s,i=6*o-e,0!=(1&e)&&(i=1-i),a=l+i*(n-l),e){default:case 6:case 0:r=n,g=a,b=l;break;case 1:r=a,g=n,b=l;break;case 2:r=l,g=n,b=a;break;case 3:r=l,g=a,b=n;break;case 4:r=a,g=l,b=n;break;case 5:r=n,g=l,b=a}return[255*r,255*g,255*b]}function p(t){var e,n,i,a=t[0]/100,o=t[1]/100,r=t[2]/100,l=t[3]/100;return e=1-Math.min(1,a*(1-l)+l),n=1-Math.min(1,o*(1-l)+l),i=1-Math.min(1,r*(1-l)+l),[255*e,255*n,255*i]}function v(t){var e,n,i,a=t[0]/100,o=t[1]/100,r=t[2]/100;return e=3.2406*a+-1.5372*o+-.4986*r,n=-.9689*a+1.8758*o+.0415*r,i=.0557*a+-.204*o+1.057*r,e=e>.0031308?1.055*Math.pow(e,1/2.4)-.055:e*=12.92,n=n>.0031308?1.055*Math.pow(n,1/2.4)-.055:n*=12.92,i=i>.0031308?1.055*Math.pow(i,1/2.4)-.055:i*=12.92,e=Math.min(Math.max(0,e),1),n=Math.min(Math.max(0,n),1),i=Math.min(Math.max(0,i),1),[255*e,255*n,255*i]}function m(t){var e,n,i,a=t[0],o=t[1],r=t[2];return a/=95.047,o/=100,r/=108.883,a=a>.008856?Math.pow(a,1/3):7.787*a+16/116,o=o>.008856?Math.pow(o,1/3):7.787*o+16/116,r=r>.008856?Math.pow(r,1/3):7.787*r+16/116,e=116*o-16,n=500*(a-o),i=200*(o-r),[e,n,i]}function x(t){var e,n,i,a,o=t[0],r=t[1],l=t[2];return o<=8?a=(n=100*o/903.3)/100*7.787+16/116:(n=100*Math.pow((o+16)/116,3),a=Math.pow(n/100,1/3)),e=e/95.047<=.008856?e=95.047*(r/500+a-16/116)/7.787:95.047*Math.pow(r/500+a,3),i=i/108.883<=.008859?i=108.883*(a-l/200-16/116)/7.787:108.883*Math.pow(a-l/200,3),[e,n,i]}function y(t){var e,n,i,a=t[0],o=t[1],r=t[2];return e=Math.atan2(r,o),(n=360*e/2/Math.PI)<0&&(n+=360),i=Math.sqrt(o*o+r*r),[a,i,n]}function k(t){return v(x(t))}function w(t){var e,n,i,a=t[0],o=t[1];return i=t[2]/360*2*Math.PI,e=o*Math.cos(i),n=o*Math.sin(i),[a,e,n]}function M(t){return S[t]}e.exports={rgb2hsl:i,rgb2hsv:a,rgb2hwb:o,rgb2cmyk:l,rgb2keyword:s,rgb2xyz:u,rgb2lab:d,rgb2lch:function(t){return y(d(t))},hsl2rgb:c,hsl2hsv:function(t){var e,n,i=t[0],a=t[1]/100,o=t[2]/100;return 0===o?[0,0,0]:(o*=2,a*=o<=1?o:2-o,n=(o+a)/2,e=2*a/(o+a),[i,100*e,100*n])},hsl2hwb:function(t){return o(c(t))},hsl2cmyk:function(t){return l(c(t))},hsl2keyword:function(t){return s(c(t))},hsv2rgb:h,hsv2hsl:function(t){var e,n,i=t[0],a=t[1]/100,o=t[2]/100;return n=(2-a)*o,e=a*o,e/=n<=1?n:2-n,e=e||0,n/=2,[i,100*e,100*n]},hsv2hwb:function(t){return o(h(t))},hsv2cmyk:function(t){return l(h(t))},hsv2keyword:function(t){return s(h(t))},hwb2rgb:f,hwb2hsl:function(t){return i(f(t))},hwb2hsv:function(t){return a(f(t))},hwb2cmyk:function(t){return l(f(t))},hwb2keyword:function(t){return s(f(t))},cmyk2rgb:p,cmyk2hsl:function(t){return i(p(t))},cmyk2hsv:function(t){return a(p(t))},cmyk2hwb:function(t){return o(p(t))},cmyk2keyword:function(t){return s(p(t))},keyword2rgb:M,keyword2hsl:function(t){return i(M(t))},keyword2hsv:function(t){return a(M(t))},keyword2hwb:function(t){return o(M(t))},keyword2cmyk:function(t){return l(M(t))},keyword2lab:function(t){return d(M(t))},keyword2xyz:function(t){return u(M(t))},xyz2rgb:v,xyz2lab:m,xyz2lch:function(t){return y(m(t))},lab2xyz:x,lab2rgb:k,lab2lch:y,lch2lab:w,lch2xyz:function(t){return x(w(t))},lch2rgb:function(t){return k(w(t))}};var S={aliceblue:[240,248,255],antiquewhite:[250,235,215],aqua:[0,255,255],aquamarine:[127,255,212],azure:[240,255,255],beige:[245,245,220],bisque:[255,228,196],black:[0,0,0],blanchedalmond:[255,235,205],blue:[0,0,255],blueviolet:[138,43,226],brown:[165,42,42],burlywood:[222,184,135],cadetblue:[95,158,160],chartreuse:[127,255,0],chocolate:[210,105,30],coral:[255,127,80],cornflowerblue:[100,149,237],cornsilk:[255,248,220],crimson:[220,20,60],cyan:[0,255,255],darkblue:[0,0,139],darkcyan:[0,139,139],darkgoldenrod:[184,134,11],darkgray:[169,169,169],darkgreen:[0,100,0],darkgrey:[169,169,169],darkkhaki:[189,183,107],darkmagenta:[139,0,139],darkolivegreen:[85,107,47],darkorange:[255,140,0],darkorchid:[153,50,204],darkred:[139,0,0],darksalmon:[233,150,122],darkseagreen:[143,188,143],darkslateblue:[72,61,139],darkslategray:[47,79,79],darkslategrey:[47,79,79],darkturquoise:[0,206,209],darkviolet:[148,0,211],deeppink:[255,20,147],deepskyblue:[0,191,255],dimgray:[105,105,105],dimgrey:[105,105,105],dodgerblue:[30,144,255],firebrick:[178,34,34],floralwhite:[255,250,240],forestgreen:[34,139,34],fuchsia:[255,0,255],gainsboro:[220,220,220],ghostwhite:[248,248,255],gold:[255,215,0],goldenrod:[218,165,32],gray:[128,128,128],green:[0,128,0],greenyellow:[173,255,47],grey:[128,128,128],honeydew:[240,255,240],hotpink:[255,105,180],indianred:[205,92,92],indigo:[75,0,130],ivory:[255,255,240],khaki:[240,230,140],lavender:[230,230,250],lavenderblush:[255,240,245],lawngreen:[124,252,0],lemonchiffon:[255,250,205],lightblue:[173,216,230],lightcoral:[240,128,128],lightcyan:[224,255,255],lightgoldenrodyellow:[250,250,210],lightgray:[211,211,211],lightgreen:[144,238,144],lightgrey:[211,211,211],lightpink:[255,182,193],lightsalmon:[255,160,122],lightseagreen:[32,178,170],lightskyblue:[135,206,250],lightslategray:[119,136,153],lightslategrey:[119,136,153],lightsteelblue:[176,196,222],lightyellow:[255,255,224],lime:[0,255,0],limegreen:[50,205,50],linen:[250,240,230],magenta:[255,0,255],maroon:[128,0,0],mediumaquamarine:[102,205,170],mediumblue:[0,0,205],mediumorchid:[186,85,211],mediumpurple:[147,112,219],mediumseagreen:[60,179,113],mediumslateblue:[123,104,238],mediumspringgreen:[0,250,154],mediumturquoise:[72,209,204],mediumvioletred:[199,21,133],midnightblue:[25,25,112],mintcream:[245,255,250],mistyrose:[255,228,225],moccasin:[255,228,181],navajowhite:[255,222,173],navy:[0,0,128],oldlace:[253,245,230],olive:[128,128,0],olivedrab:[107,142,35],orange:[255,165,0],orangered:[255,69,0],orchid:[218,112,214],palegoldenrod:[238,232,170],palegreen:[152,251,152],paleturquoise:[175,238,238],palevioletred:[219,112,147],papayawhip:[255,239,213],peachpuff:[255,218,185],peru:[205,133,63],pink:[255,192,203],plum:[221,160,221],powderblue:[176,224,230],purple:[128,0,128],rebeccapurple:[102,51,153],red:[255,0,0],rosybrown:[188,143,143],royalblue:[65,105,225],saddlebrown:[139,69,19],salmon:[250,128,114],sandybrown:[244,164,96],seagreen:[46,139,87],seashell:[255,245,238],sienna:[160,82,45],silver:[192,192,192],skyblue:[135,206,235],slateblue:[106,90,205],slategray:[112,128,144],slategrey:[112,128,144],snow:[255,250,250],springgreen:[0,255,127],steelblue:[70,130,180],tan:[210,180,140],teal:[0,128,128],thistle:[216,191,216],tomato:[255,99,71],turquoise:[64,224,208],violet:[238,130,238],wheat:[245,222,179],white:[255,255,255],whitesmoke:[245,245,245],yellow:[255,255,0],yellowgreen:[154,205,50]},C={};for(var _ in S)C[JSON.stringify(S[_])]=_},{}],5:[function(t,e,n){var i=t(4),a=function(){return new u};for(var o in i){a[o+"Raw"]=function(t){return function(e){return"number"==typeof e&&(e=Array.prototype.slice.call(arguments)),i[t](e)}}(o);var r=/(\w+)2(\w+)/.exec(o),l=r[1],s=r[2];(a[l]=a[l]||{})[s]=a[o]=function(t){return function(e){"number"==typeof e&&(e=Array.prototype.slice.call(arguments));var n=i[t](e);if("string"==typeof n||void 0===n)return n;for(var a=0;a<n.length;a++)n[a]=Math.round(n[a]);return n}}(o)}var u=function(){this.convs={}};u.prototype.routeSpace=function(t,e){var n=e[0];return void 0===n?this.getValues(t):("number"==typeof n&&(n=Array.prototype.slice.call(e)),this.setValues(t,n))},u.prototype.setValues=function(t,e){return this.space=t,this.convs={},this.convs[t]=e,this},u.prototype.getValues=function(t){var e=this.convs[t];if(!e){var n=this.space,i=this.convs[n];e=a[n][t](i),this.convs[t]=e}return e},["rgb","hsl","hsv","cmyk","keyword"].forEach(function(t){u.prototype[t]=function(e){return this.routeSpace(t,arguments)}}),e.exports=a},{4:4}],6:[function(t,e,n){"use strict";e.exports={aliceblue:[240,248,255],antiquewhite:[250,235,215],aqua:[0,255,255],aquamarine:[127,255,212],azure:[240,255,255],beige:[245,245,220],bisque:[255,228,196],black:[0,0,0],blanchedalmond:[255,235,205],blue:[0,0,255],blueviolet:[138,43,226],brown:[165,42,42],burlywood:[222,184,135],cadetblue:[95,158,160],chartreuse:[127,255,0],chocolate:[210,105,30],coral:[255,127,80],cornflowerblue:[100,149,237],cornsilk:[255,248,220],crimson:[220,20,60],cyan:[0,255,255],darkblue:[0,0,139],darkcyan:[0,139,139],darkgoldenrod:[184,134,11],darkgray:[169,169,169],darkgreen:[0,100,0],darkgrey:[169,169,169],darkkhaki:[189,183,107],darkmagenta:[139,0,139],darkolivegreen:[85,107,47],darkorange:[255,140,0],darkorchid:[153,50,204],darkred:[139,0,0],darksalmon:[233,150,122],darkseagreen:[143,188,143],darkslateblue:[72,61,139],darkslategray:[47,79,79],darkslategrey:[47,79,79],darkturquoise:[0,206,209],darkviolet:[148,0,211],deeppink:[255,20,147],deepskyblue:[0,191,255],dimgray:[105,105,105],dimgrey:[105,105,105],dodgerblue:[30,144,255],firebrick:[178,34,34],floralwhite:[255,250,240],forestgreen:[34,139,34],fuchsia:[255,0,255],gainsboro:[220,220,220],ghostwhite:[248,248,255],gold:[255,215,0],goldenrod:[218,165,32],gray:[128,128,128],green:[0,128,0],greenyellow:[173,255,47],grey:[128,128,128],honeydew:[240,255,240],hotpink:[255,105,180],indianred:[205,92,92],indigo:[75,0,130],ivory:[255,255,240],khaki:[240,230,140],lavender:[230,230,250],lavenderblush:[255,240,245],lawngreen:[124,252,0],lemonchiffon:[255,250,205],lightblue:[173,216,230],lightcoral:[240,128,128],lightcyan:[224,255,255],lightgoldenrodyellow:[250,250,210],lightgray:[211,211,211],lightgreen:[144,238,144],lightgrey:[211,211,211],lightpink:[255,182,193],lightsalmon:[255,160,122],lightseagreen:[32,178,170],lightskyblue:[135,206,250],lightslategray:[119,136,153],lightslategrey:[119,136,153],lightsteelblue:[176,196,222],lightyellow:[255,255,224],lime:[0,255,0],limegreen:[50,205,50],linen:[250,240,230],magenta:[255,0,255],maroon:[128,0,0],mediumaquamarine:[102,205,170],mediumblue:[0,0,205],mediumorchid:[186,85,211],mediumpurple:[147,112,219],mediumseagreen:[60,179,113],mediumslateblue:[123,104,238],mediumspringgreen:[0,250,154],mediumturquoise:[72,209,204],mediumvioletred:[199,21,133],midnightblue:[25,25,112],mintcream:[245,255,250],mistyrose:[255,228,225],moccasin:[255,228,181],navajowhite:[255,222,173],navy:[0,0,128],oldlace:[253,245,230],olive:[128,128,0],olivedrab:[107,142,35],orange:[255,165,0],orangered:[255,69,0],orchid:[218,112,214],palegoldenrod:[238,232,170],palegreen:[152,251,152],paleturquoise:[175,238,238],palevioletred:[219,112,147],papayawhip:[255,239,213],peachpuff:[255,218,185],peru:[205,133,63],pink:[255,192,203],plum:[221,160,221],powderblue:[176,224,230],purple:[128,0,128],rebeccapurple:[102,51,153],red:[255,0,0],rosybrown:[188,143,143],royalblue:[65,105,225],saddlebrown:[139,69,19],salmon:[250,128,114],sandybrown:[244,164,96],seagreen:[46,139,87],seashell:[255,245,238],sienna:[160,82,45],silver:[192,192,192],skyblue:[135,206,235],slateblue:[106,90,205],slategray:[112,128,144],slategrey:[112,128,144],snow:[255,250,250],springgreen:[0,255,127],steelblue:[70,130,180],tan:[210,180,140],teal:[0,128,128],thistle:[216,191,216],tomato:[255,99,71],turquoise:[64,224,208],violet:[238,130,238],wheat:[245,222,179],white:[255,255,255],whitesmoke:[245,245,245],yellow:[255,255,0],yellowgreen:[154,205,50]}},{}],7:[function(t,e,n){var i=t(29)();i.helpers=t(45),t(27)(i),i.defaults=t(25),i.Element=t(26),i.elements=t(40),i.Interaction=t(28),i.platform=t(48),t(31)(i),t(22)(i),t(23)(i),t(24)(i),t(30)(i),t(33)(i),t(32)(i),t(35)(i),t(54)(i),t(52)(i),t(53)(i),t(55)(i),t(56)(i),t(57)(i),t(15)(i),t(16)(i),t(17)(i),t(18)(i),t(19)(i),t(20)(i),t(21)(i),t(8)(i),t(9)(i),t(10)(i),t(11)(i),t(12)(i),t(13)(i),t(14)(i);var a=[];a.push(t(49)(i),t(50)(i),t(51)(i)),i.plugins.register(a),i.platform.initialize(),e.exports=i,"undefined"!=typeof window&&(window.Chart=i),i.canvasHelpers=i.helpers.canvas},{10:10,11:11,12:12,13:13,14:14,15:15,16:16,17:17,18:18,19:19,20:20,21:21,22:22,23:23,24:24,25:25,26:26,27:27,28:28,29:29,30:30,31:31,32:32,33:33,35:35,40:40,45:45,48:48,49:49,50:50,51:51,52:52,53:53,54:54,55:55,56:56,57:57,8:8,9:9}],8:[function(t,e,n){"use strict";e.exports=function(t){t.Bar=function(e,n){return n.type="bar",new t(e,n)}}},{}],9:[function(t,e,n){"use strict";e.exports=function(t){t.Bubble=function(e,n){return n.type="bubble",new t(e,n)}}},{}],10:[function(t,e,n){"use strict";e.exports=function(t){t.Doughnut=function(e,n){return n.type="doughnut",new t(e,n)}}},{}],11:[function(t,e,n){"use strict";e.exports=function(t){t.Line=function(e,n){return n.type="line",new t(e,n)}}},{}],12:[function(t,e,n){"use strict";e.exports=function(t){t.PolarArea=function(e,n){return n.type="polarArea",new t(e,n)}}},{}],13:[function(t,e,n){"use strict";e.exports=function(t){t.Radar=function(e,n){return n.type="radar",new t(e,n)}}},{}],14:[function(t,e,n){"use strict";e.exports=function(t){t.Scatter=function(e,n){return n.type="scatter",new t(e,n)}}},{}],15:[function(t,e,n){"use strict";var i=t(25),a=t(40),o=t(45);i._set("bar",{hover:{mode:"label"},scales:{xAxes:[{type:"category",categoryPercentage:.8,barPercentage:.9,offset:!0,gridLines:{offsetGridLines:!0}}],yAxes:[{type:"linear"}]}}),i._set("horizontalBar",{hover:{mode:"index",axis:"y"},scales:{xAxes:[{type:"linear",position:"bottom"}],yAxes:[{position:"left",type:"category",categoryPercentage:.8,barPercentage:.9,offset:!0,gridLines:{offsetGridLines:!0}}]},elements:{rectangle:{borderSkipped:"left"}},tooltips:{callbacks:{title:function(t,e){var n="";return t.length>0&&(t[0].yLabel?n=t[0].yLabel:e.labels.length>0&&t[0].index<e.labels.length&&(n=e.labels[t[0].index])),n},label:function(t,e){return(e.datasets[t.datasetIndex].label||"")+": "+t.xLabel}},mode:"index",axis:"y"}}),e.exports=function(t){t.controllers.bar=t.DatasetController.extend({dataElementType:a.Rectangle,initialize:function(){var e,n=this;t.DatasetController.prototype.initialize.apply(n,arguments),(e=n.getMeta()).stack=n.getDataset().stack,e.bar=!0},update:function(t){var e,n,i=this,a=i.getMeta().data;for(i._ruler=i.getRuler(),e=0,n=a.length;e<n;++e)i.updateElement(a[e],e,t)},updateElement:function(t,e,n){var i=this,a=i.chart,r=i.getMeta(),l=i.getDataset(),s=t.custom||{},u=a.options.elements.rectangle;t._xScale=i.getScaleForId(r.xAxisID),t._yScale=i.getScaleForId(r.yAxisID),t._datasetIndex=i.index,t._index=e,t._model={datasetLabel:l.label,label:a.data.labels[e],borderSkipped:s.borderSkipped?s.borderSkipped:u.borderSkipped,backgroundColor:s.backgroundColor?s.backgroundColor:o.valueAtIndexOrDefault(l.backgroundColor,e,u.backgroundColor),borderColor:s.borderColor?s.borderColor:o.valueAtIndexOrDefault(l.borderColor,e,u.borderColor),borderWidth:s.borderWidth?s.borderWidth:o.valueAtIndexOrDefault(l.borderWidth,e,u.borderWidth)},i.updateElementGeometry(t,e,n),t.pivot()},updateElementGeometry:function(t,e,n){var i=this,a=t._model,o=i.getValueScale(),r=o.getBasePixel(),l=o.isHorizontal(),s=i._ruler||i.getRuler(),u=i.calculateBarValuePixels(i.index,e),d=i.calculateBarIndexPixels(i.index,e,s);a.horizontal=l,a.base=n?r:u.base,a.x=l?n?r:u.head:d.center,a.y=l?d.center:n?r:u.head,a.height=l?d.size:void 0,a.width=l?void 0:d.size},getValueScaleId:function(){return this.getMeta().yAxisID},getIndexScaleId:function(){return this.getMeta().xAxisID},getValueScale:function(){return this.getScaleForId(this.getValueScaleId())},getIndexScale:function(){return this.getScaleForId(this.getIndexScaleId())},getStackCount:function(t){var e,n,i=this,a=i.chart,o=i.getIndexScale().options.stacked,r=void 0===t?a.data.datasets.length:t+1,l=[];for(e=0;e<r;++e)(n=a.getDatasetMeta(e)).bar&&a.isDatasetVisible(e)&&(!1===o||!0===o&&-1===l.indexOf(n.stack)||void 0===o&&(void 0===n.stack||-1===l.indexOf(n.stack)))&&l.push(n.stack);return l.length},getStackIndex:function(t){return this.getStackCount(t)-1},getRuler:function(){var t,e,n=this,i=n.getIndexScale(),a=n.getStackCount(),o=n.index,r=[],l=i.isHorizontal(),s=l?i.left:i.top,u=s+(l?i.width:i.height);for(t=0,e=n.getMeta().data.length;t<e;++t)r.push(i.getPixelForValue(null,t,o));return{pixels:r,start:s,end:u,stackCount:a,scale:i}},calculateBarValuePixels:function(t,e){var n,i,a,o,r,l,s=this,u=s.chart,d=s.getMeta(),c=s.getValueScale(),h=u.data.datasets,f=c.getRightValue(h[t].data[e]),g=c.options.stacked,p=d.stack,v=0;if(g||void 0===g&&void 0!==p)for(n=0;n<t;++n)(i=u.getDatasetMeta(n)).bar&&i.stack===p&&i.controller.getValueScaleId()===c.id&&u.isDatasetVisible(n)&&(a=c.getRightValue(h[n].data[e]),(f<0&&a<0||f>=0&&a>0)&&(v+=a));return o=c.getPixelForValue(v),r=c.getPixelForValue(v+f),l=(r-o)/2,{size:l,base:o,head:r,center:r+l/2}},calculateBarIndexPixels:function(t,e,n){var i,a,r,l,s,u,d=this,c=n.scale.options,h=d.getStackIndex(t),f=n.pixels,g=f[e],p=f.length,v=n.start,m=n.end;return 1===p?(i=g>v?g-v:m-g,a=g<m?m-g:g-v):(e>0&&(i=(g-f[e-1])/2,e===p-1&&(a=i)),e<p-1&&(a=(f[e+1]-g)/2,0===e&&(i=a))),r=i*c.categoryPercentage,l=a*c.categoryPercentage,s=(r+l)/n.stackCount,u=s*c.barPercentage,u=Math.min(o.valueOrDefault(c.barThickness,u),o.valueOrDefault(c.maxBarThickness,1/0)),g-=r,g+=s*h,g+=(s-u)/2,{size:u,base:g,head:g+u,center:g+u/2}},draw:function(){var t=this,e=t.chart,n=t.getValueScale(),i=t.getMeta().data,a=t.getDataset(),r=i.length,l=0;for(o.canvas.clipArea(e.ctx,e.chartArea);l<r;++l)isNaN(n.getRightValue(a.data[l]))||i[l].draw();o.canvas.unclipArea(e.ctx)},setHoverStyle:function(t){var e=this.chart.data.datasets[t._datasetIndex],n=t._index,i=t.custom||{},a=t._model;a.backgroundColor=i.hoverBackgroundColor?i.hoverBackgroundColor:o.valueAtIndexOrDefault(e.hoverBackgroundColor,n,o.getHoverColor(a.backgroundColor)),a.borderColor=i.hoverBorderColor?i.hoverBorderColor:o.valueAtIndexOrDefault(e.hoverBorderColor,n,o.getHoverColor(a.borderColor)),a.borderWidth=i.hoverBorderWidth?i.hoverBorderWidth:o.valueAtIndexOrDefault(e.hoverBorderWidth,n,a.borderWidth)},removeHoverStyle:function(t){var e=this.chart.data.datasets[t._datasetIndex],n=t._index,i=t.custom||{},a=t._model,r=this.chart.options.elements.rectangle;a.backgroundColor=i.backgroundColor?i.backgroundColor:o.valueAtIndexOrDefault(e.backgroundColor,n,r.backgroundColor),a.borderColor=i.borderColor?i.borderColor:o.valueAtIndexOrDefault(e.borderColor,n,r.borderColor),a.borderWidth=i.borderWidth?i.borderWidth:o.valueAtIndexOrDefault(e.borderWidth,n,r.borderWidth)}}),t.controllers.horizontalBar=t.controllers.bar.extend({getValueScaleId:function(){return this.getMeta().xAxisID},getIndexScaleId:function(){return this.getMeta().yAxisID}})}},{25:25,40:40,45:45}],16:[function(t,e,n){"use strict";var i=t(25),a=t(40),o=t(45);i._set("bubble",{hover:{mode:"single"},scales:{xAxes:[{type:"linear",position:"bottom",id:"x-axis-0"}],yAxes:[{type:"linear",position:"left",id:"y-axis-0"}]},tooltips:{callbacks:{title:function(){return""},label:function(t,e){var n=e.datasets[t.datasetIndex].label||"",i=e.datasets[t.datasetIndex].data[t.index];return n+": ("+t.xLabel+", "+t.yLabel+", "+i.r+")"}}}}),e.exports=function(t){t.controllers.bubble=t.DatasetController.extend({dataElementType:a.Point,update:function(t){var e=this,n=e.getMeta().data;o.each(n,function(n,i){e.updateElement(n,i,t)})},updateElement:function(t,e,n){var i=this,a=i.getMeta(),o=t.custom||{},r=i.getScaleForId(a.xAxisID),l=i.getScaleForId(a.yAxisID),s=i._resolveElementOptions(t,e),u=i.getDataset().data[e],d=i.index,c=n?r.getPixelForDecimal(.5):r.getPixelForValue("object"==typeof u?u:NaN,e,d),h=n?l.getBasePixel():l.getPixelForValue(u,e,d);t._xScale=r,t._yScale=l,t._options=s,t._datasetIndex=d,t._index=e,t._model={backgroundColor:s.backgroundColor,borderColor:s.borderColor,borderWidth:s.borderWidth,hitRadius:s.hitRadius,pointStyle:s.pointStyle,radius:n?0:s.radius,skip:o.skip||isNaN(c)||isNaN(h),x:c,y:h},t.pivot()},setHoverStyle:function(t){var e=t._model,n=t._options;e.backgroundColor=o.valueOrDefault(n.hoverBackgroundColor,o.getHoverColor(n.backgroundColor)),e.borderColor=o.valueOrDefault(n.hoverBorderColor,o.getHoverColor(n.borderColor)),e.borderWidth=o.valueOrDefault(n.hoverBorderWidth,n.borderWidth),e.radius=n.radius+n.hoverRadius},removeHoverStyle:function(t){var e=t._model,n=t._options;e.backgroundColor=n.backgroundColor,e.borderColor=n.borderColor,e.borderWidth=n.borderWidth,e.radius=n.radius},_resolveElementOptions:function(t,e){var n,i,a,r=this,l=r.chart,s=l.data.datasets[r.index],u=t.custom||{},d=l.options.elements.point,c=o.options.resolve,h=s.data[e],f={},g={chart:l,dataIndex:e,dataset:s,datasetIndex:r.index},p=["backgroundColor","borderColor","borderWidth","hoverBackgroundColor","hoverBorderColor","hoverBorderWidth","hoverRadius","hitRadius","pointStyle"];for(n=0,i=p.length;n<i;++n)f[a=p[n]]=c([u[a],s[a],d[a]],g,e);return f.radius=c([u.radius,h?h.r:void 0,s.radius,d.radius],g,e),f}})}},{25:25,40:40,45:45}],17:[function(t,e,n){"use strict";var i=t(25),a=t(40),o=t(45);i._set("doughnut",{animation:{animateRotate:!0,animateScale:!1},hover:{mode:"single"},legendCallback:function(t){var e=[];e.push('<ul class="'+t.id+'-legend">');var n=t.data,i=n.datasets,a=n.labels;if(i.length)for(var o=0;o<i[0].data.length;++o)e.push('<li><span style="background-color:'+i[0].backgroundColor[o]+'"></span>'),a[o]&&e.push(a[o]),e.push("</li>");return e.push("</ul>"),e.join("")},legend:{labels:{generateLabels:function(t){var e=t.data;return e.labels.length&&e.datasets.length?e.labels.map(function(n,i){var a=t.getDatasetMeta(0),r=e.datasets[0],l=a.data[i],s=l&&l.custom||{},u=o.valueAtIndexOrDefault,d=t.options.elements.arc;return{text:n,fillStyle:s.backgroundColor?s.backgroundColor:u(r.backgroundColor,i,d.backgroundColor),strokeStyle:s.borderColor?s.borderColor:u(r.borderColor,i,d.borderColor),lineWidth:s.borderWidth?s.borderWidth:u(r.borderWidth,i,d.borderWidth),hidden:isNaN(r.data[i])||a.data[i].hidden,index:i}}):[]}},onClick:function(t,e){var n,i,a,o=e.index,r=this.chart;for(n=0,i=(r.data.datasets||[]).length;n<i;++n)(a=r.getDatasetMeta(n)).data[o]&&(a.data[o].hidden=!a.data[o].hidden);r.update()}},cutoutPercentage:50,rotation:-.5*Math.PI,circumference:2*Math.PI,tooltips:{callbacks:{title:function(){return""},label:function(t,e){var n=e.labels[t.index],i=": "+e.datasets[t.datasetIndex].data[t.index];return o.isArray(n)?(n=n.slice())[0]+=i:n+=i,n}}}}),i._set("pie",o.clone(i.doughnut)),i._set("pie",{cutoutPercentage:0}),e.exports=function(t){t.controllers.doughnut=t.controllers.pie=t.DatasetController.extend({dataElementType:a.Arc,linkScales:o.noop,getRingIndex:function(t){for(var e=0,n=0;n<t;++n)this.chart.isDatasetVisible(n)&&++e;return e},update:function(t){var e=this,n=e.chart,i=n.chartArea,a=n.options,r=a.elements.arc,l=i.right-i.left-r.borderWidth,s=i.bottom-i.top-r.borderWidth,u=Math.min(l,s),d={x:0,y:0},c=e.getMeta(),h=a.cutoutPercentage,f=a.circumference;if(f<2*Math.PI){var g=a.rotation%(2*Math.PI),p=(g+=2*Math.PI*(g>=Math.PI?-1:g<-Math.PI?1:0))+f,v={x:Math.cos(g),y:Math.sin(g)},m={x:Math.cos(p),y:Math.sin(p)},b=g<=0&&p>=0||g<=2*Math.PI&&2*Math.PI<=p,x=g<=.5*Math.PI&&.5*Math.PI<=p||g<=2.5*Math.PI&&2.5*Math.PI<=p,y=g<=-Math.PI&&-Math.PI<=p||g<=Math.PI&&Math.PI<=p,k=g<=.5*-Math.PI&&.5*-Math.PI<=p||g<=1.5*Math.PI&&1.5*Math.PI<=p,w=h/100,M={x:y?-1:Math.min(v.x*(v.x<0?1:w),m.x*(m.x<0?1:w)),y:k?-1:Math.min(v.y*(v.y<0?1:w),m.y*(m.y<0?1:w))},S={x:b?1:Math.max(v.x*(v.x>0?1:w),m.x*(m.x>0?1:w)),y:x?1:Math.max(v.y*(v.y>0?1:w),m.y*(m.y>0?1:w))},C={width:.5*(S.x-M.x),height:.5*(S.y-M.y)};u=Math.min(l/C.width,s/C.height),d={x:-.5*(S.x+M.x),y:-.5*(S.y+M.y)}}n.borderWidth=e.getMaxBorderWidth(c.data),n.outerRadius=Math.max((u-n.borderWidth)/2,0),n.innerRadius=Math.max(h?n.outerRadius/100*h:0,0),n.radiusLength=(n.outerRadius-n.innerRadius)/n.getVisibleDatasetCount(),n.offsetX=d.x*n.outerRadius,n.offsetY=d.y*n.outerRadius,c.total=e.calculateTotal(),e.outerRadius=n.outerRadius-n.radiusLength*e.getRingIndex(e.index),e.innerRadius=Math.max(e.outerRadius-n.radiusLength,0),o.each(c.data,function(n,i){e.updateElement(n,i,t)})},updateElement:function(t,e,n){var i=this,a=i.chart,r=a.chartArea,l=a.options,s=l.animation,u=(r.left+r.right)/2,d=(r.top+r.bottom)/2,c=l.rotation,h=l.rotation,f=i.getDataset(),g=n&&s.animateRotate?0:t.hidden?0:i.calculateCircumference(f.data[e])*(l.circumference/(2*Math.PI)),p=n&&s.animateScale?0:i.innerRadius,v=n&&s.animateScale?0:i.outerRadius,m=o.valueAtIndexOrDefault;o.extend(t,{_datasetIndex:i.index,_index:e,_model:{x:u+a.offsetX,y:d+a.offsetY,startAngle:c,endAngle:h,circumference:g,outerRadius:v,innerRadius:p,label:m(f.label,e,a.data.labels[e])}});var b=t._model;this.removeHoverStyle(t),n&&s.animateRotate||(b.startAngle=0===e?l.rotation:i.getMeta().data[e-1]._model.endAngle,b.endAngle=b.startAngle+b.circumference),t.pivot()},removeHoverStyle:function(e){t.DatasetController.prototype.removeHoverStyle.call(this,e,this.chart.options.elements.arc)},calculateTotal:function(){var t,e=this.getDataset(),n=this.getMeta(),i=0;return o.each(n.data,function(n,a){t=e.data[a],isNaN(t)||n.hidden||(i+=Math.abs(t))}),i},calculateCircumference:function(t){var e=this.getMeta().total;return e>0&&!isNaN(t)?2*Math.PI*(t/e):0},getMaxBorderWidth:function(t){for(var e,n,i=0,a=this.index,o=t.length,r=0;r<o;r++)e=t[r]._model?t[r]._model.borderWidth:0,i=(n=t[r]._chart?t[r]._chart.config.data.datasets[a].hoverBorderWidth:0)>(i=e>i?e:i)?n:i;return i}})}},{25:25,40:40,45:45}],18:[function(t,e,n){"use strict";var i=t(25),a=t(40),o=t(45);i._set("line",{showLines:!0,spanGaps:!1,hover:{mode:"label"},scales:{xAxes:[{type:"category",id:"x-axis-0"}],yAxes:[{type:"linear",id:"y-axis-0"}]}}),e.exports=function(t){function e(t,e){return o.valueOrDefault(t.showLine,e.showLines)}t.controllers.line=t.DatasetController.extend({datasetElementType:a.Line,dataElementType:a.Point,update:function(t){var n,i,a,r=this,l=r.getMeta(),s=l.dataset,u=l.data||[],d=r.chart.options,c=d.elements.line,h=r.getScaleForId(l.yAxisID),f=r.getDataset(),g=e(f,d);for(g&&(a=s.custom||{},void 0!==f.tension&&void 0===f.lineTension&&(f.lineTension=f.tension),s._scale=h,s._datasetIndex=r.index,s._children=u,s._model={spanGaps:f.spanGaps?f.spanGaps:d.spanGaps,tension:a.tension?a.tension:o.valueOrDefault(f.lineTension,c.tension),backgroundColor:a.backgroundColor?a.backgroundColor:f.backgroundColor||c.backgroundColor,borderWidth:a.borderWidth?a.borderWidth:f.borderWidth||c.borderWidth,borderColor:a.borderColor?a.borderColor:f.borderColor||c.borderColor,borderCapStyle:a.borderCapStyle?a.borderCapStyle:f.borderCapStyle||c.borderCapStyle,borderDash:a.borderDash?a.borderDash:f.borderDash||c.borderDash,borderDashOffset:a.borderDashOffset?a.borderDashOffset:f.borderDashOffset||c.borderDashOffset,borderJoinStyle:a.borderJoinStyle?a.borderJoinStyle:f.borderJoinStyle||c.borderJoinStyle,fill:a.fill?a.fill:void 0!==f.fill?f.fill:c.fill,steppedLine:a.steppedLine?a.steppedLine:o.valueOrDefault(f.steppedLine,c.stepped),cubicInterpolationMode:a.cubicInterpolationMode?a.cubicInterpolationMode:o.valueOrDefault(f.cubicInterpolationMode,c.cubicInterpolationMode)},s.pivot()),n=0,i=u.length;n<i;++n)r.updateElement(u[n],n,t);for(g&&0!==s._model.tension&&r.updateBezierControlPoints(),n=0,i=u.length;n<i;++n)u[n].pivot()},getPointBackgroundColor:function(t,e){var n=this.chart.options.elements.point.backgroundColor,i=this.getDataset(),a=t.custom||{};return a.backgroundColor?n=a.backgroundColor:i.pointBackgroundColor?n=o.valueAtIndexOrDefault(i.pointBackgroundColor,e,n):i.backgroundColor&&(n=i.backgroundColor),n},getPointBorderColor:function(t,e){var n=this.chart.options.elements.point.borderColor,i=this.getDataset(),a=t.custom||{};return a.borderColor?n=a.borderColor:i.pointBorderColor?n=o.valueAtIndexOrDefault(i.pointBorderColor,e,n):i.borderColor&&(n=i.borderColor),n},getPointBorderWidth:function(t,e){var n=this.chart.options.elements.point.borderWidth,i=this.getDataset(),a=t.custom||{};return isNaN(a.borderWidth)?!isNaN(i.pointBorderWidth)||o.isArray(i.pointBorderWidth)?n=o.valueAtIndexOrDefault(i.pointBorderWidth,e,n):isNaN(i.borderWidth)||(n=i.borderWidth):n=a.borderWidth,n},updateElement:function(t,e,n){var i,a,r=this,l=r.getMeta(),s=t.custom||{},u=r.getDataset(),d=r.index,c=u.data[e],h=r.getScaleForId(l.yAxisID),f=r.getScaleForId(l.xAxisID),g=r.chart.options.elements.point;void 0!==u.radius&&void 0===u.pointRadius&&(u.pointRadius=u.radius),void 0!==u.hitRadius&&void 0===u.pointHitRadius&&(u.pointHitRadius=u.hitRadius),i=f.getPixelForValue("object"==typeof c?c:NaN,e,d),a=n?h.getBasePixel():r.calculatePointY(c,e,d),t._xScale=f,t._yScale=h,t._datasetIndex=d,t._index=e,t._model={x:i,y:a,skip:s.skip||isNaN(i)||isNaN(a),radius:s.radius||o.valueAtIndexOrDefault(u.pointRadius,e,g.radius),pointStyle:s.pointStyle||o.valueAtIndexOrDefault(u.pointStyle,e,g.pointStyle),backgroundColor:r.getPointBackgroundColor(t,e),borderColor:r.getPointBorderColor(t,e),borderWidth:r.getPointBorderWidth(t,e),tension:l.dataset._model?l.dataset._model.tension:0,steppedLine:!!l.dataset._model&&l.dataset._model.steppedLine,hitRadius:s.hitRadius||o.valueAtIndexOrDefault(u.pointHitRadius,e,g.hitRadius)}},calculatePointY:function(t,e,n){var i,a,o,r=this,l=r.chart,s=r.getMeta(),u=r.getScaleForId(s.yAxisID),d=0,c=0;if(u.options.stacked){for(i=0;i<n;i++)if(a=l.data.datasets[i],"line"===(o=l.getDatasetMeta(i)).type&&o.yAxisID===u.id&&l.isDatasetVisible(i)){var h=Number(u.getRightValue(a.data[e]));h<0?c+=h||0:d+=h||0}var f=Number(u.getRightValue(t));return f<0?u.getPixelForValue(c+f):u.getPixelForValue(d+f)}return u.getPixelForValue(t)},updateBezierControlPoints:function(){function t(t,e,n){return Math.max(Math.min(t,n),e)}var e,n,i,a,r=this,l=r.getMeta(),s=r.chart.chartArea,u=l.data||[];if(l.dataset._model.spanGaps&&(u=u.filter(function(t){return!t._model.skip})),"monotone"===l.dataset._model.cubicInterpolationMode)o.splineCurveMonotone(u);else for(e=0,n=u.length;e<n;++e)i=u[e]._model,a=o.splineCurve(o.previousItem(u,e)._model,i,o.nextItem(u,e)._model,l.dataset._model.tension),i.controlPointPreviousX=a.previous.x,i.controlPointPreviousY=a.previous.y,i.controlPointNextX=a.next.x,i.controlPointNextY=a.next.y;if(r.chart.options.elements.line.capBezierPoints)for(e=0,n=u.length;e<n;++e)(i=u[e]._model).controlPointPreviousX=t(i.controlPointPreviousX,s.left,s.right),i.controlPointPreviousY=t(i.controlPointPreviousY,s.top,s.bottom),i.controlPointNextX=t(i.controlPointNextX,s.left,s.right),i.controlPointNextY=t(i.controlPointNextY,s.top,s.bottom)},draw:function(){var t=this,n=t.chart,i=t.getMeta(),a=i.data||[],r=n.chartArea,l=a.length,s=0;for(o.canvas.clipArea(n.ctx,r),e(t.getDataset(),n.options)&&i.dataset.draw(),o.canvas.unclipArea(n.ctx);s<l;++s)a[s].draw(r)},setHoverStyle:function(t){var e=this.chart.data.datasets[t._datasetIndex],n=t._index,i=t.custom||{},a=t._model;a.radius=i.hoverRadius||o.valueAtIndexOrDefault(e.pointHoverRadius,n,this.chart.options.elements.point.hoverRadius),a.backgroundColor=i.hoverBackgroundColor||o.valueAtIndexOrDefault(e.pointHoverBackgroundColor,n,o.getHoverColor(a.backgroundColor)),a.borderColor=i.hoverBorderColor||o.valueAtIndexOrDefault(e.pointHoverBorderColor,n,o.getHoverColor(a.borderColor)),a.borderWidth=i.hoverBorderWidth||o.valueAtIndexOrDefault(e.pointHoverBorderWidth,n,a.borderWidth)},removeHoverStyle:function(t){var e=this,n=e.chart.data.datasets[t._datasetIndex],i=t._index,a=t.custom||{},r=t._model;void 0!==n.radius&&void 0===n.pointRadius&&(n.pointRadius=n.radius),r.radius=a.radius||o.valueAtIndexOrDefault(n.pointRadius,i,e.chart.options.elements.point.radius),r.backgroundColor=e.getPointBackgroundColor(t,i),r.borderColor=e.getPointBorderColor(t,i),r.borderWidth=e.getPointBorderWidth(t,i)}})}},{25:25,40:40,45:45}],19:[function(t,e,n){"use strict";var i=t(25),a=t(40),o=t(45);i._set("polarArea",{scale:{type:"radialLinear",angleLines:{display:!1},gridLines:{circular:!0},pointLabels:{display:!1},ticks:{beginAtZero:!0}},animation:{animateRotate:!0,animateScale:!0},startAngle:-.5*Math.PI,legendCallback:function(t){var e=[];e.push('<ul class="'+t.id+'-legend">');var n=t.data,i=n.datasets,a=n.labels;if(i.length)for(var o=0;o<i[0].data.length;++o)e.push('<li><span style="background-color:'+i[0].backgroundColor[o]+'"></span>'),a[o]&&e.push(a[o]),e.push("</li>");return e.push("</ul>"),e.join("")},legend:{labels:{generateLabels:function(t){var e=t.data;return e.labels.length&&e.datasets.length?e.labels.map(function(n,i){var a=t.getDatasetMeta(0),r=e.datasets[0],l=a.data[i].custom||{},s=o.valueAtIndexOrDefault,u=t.options.elements.arc;return{text:n,fillStyle:l.backgroundColor?l.backgroundColor:s(r.backgroundColor,i,u.backgroundColor),strokeStyle:l.borderColor?l.borderColor:s(r.borderColor,i,u.borderColor),lineWidth:l.borderWidth?l.borderWidth:s(r.borderWidth,i,u.borderWidth),hidden:isNaN(r.data[i])||a.data[i].hidden,index:i}}):[]}},onClick:function(t,e){var n,i,a,o=e.index,r=this.chart;for(n=0,i=(r.data.datasets||[]).length;n<i;++n)(a=r.getDatasetMeta(n)).data[o].hidden=!a.data[o].hidden;r.update()}},tooltips:{callbacks:{title:function(){return""},label:function(t,e){return e.labels[t.index]+": "+t.yLabel}}}}),e.exports=function(t){t.controllers.polarArea=t.DatasetController.extend({dataElementType:a.Arc,linkScales:o.noop,update:function(t){var e=this,n=e.chart,i=n.chartArea,a=e.getMeta(),r=n.options,l=r.elements.arc,s=Math.min(i.right-i.left,i.bottom-i.top);n.outerRadius=Math.max((s-l.borderWidth/2)/2,0),n.innerRadius=Math.max(r.cutoutPercentage?n.outerRadius/100*r.cutoutPercentage:1,0),n.radiusLength=(n.outerRadius-n.innerRadius)/n.getVisibleDatasetCount(),e.outerRadius=n.outerRadius-n.radiusLength*e.index,e.innerRadius=e.outerRadius-n.radiusLength,a.count=e.countVisibleElements(),o.each(a.data,function(n,i){e.updateElement(n,i,t)})},updateElement:function(t,e,n){for(var i=this,a=i.chart,r=i.getDataset(),l=a.options,s=l.animation,u=a.scale,d=a.data.labels,c=i.calculateCircumference(r.data[e]),h=u.xCenter,f=u.yCenter,g=0,p=i.getMeta(),v=0;v<e;++v)isNaN(r.data[v])||p.data[v].hidden||++g;var m=l.startAngle,b=t.hidden?0:u.getDistanceFromCenterForValue(r.data[e]),x=m+c*g,y=x+(t.hidden?0:c),k=s.animateScale?0:u.getDistanceFromCenterForValue(r.data[e]);o.extend(t,{_datasetIndex:i.index,_index:e,_scale:u,_model:{x:h,y:f,innerRadius:0,outerRadius:n?k:b,startAngle:n&&s.animateRotate?m:x,endAngle:n&&s.animateRotate?m:y,label:o.valueAtIndexOrDefault(d,e,d[e])}}),i.removeHoverStyle(t),t.pivot()},removeHoverStyle:function(e){t.DatasetController.prototype.removeHoverStyle.call(this,e,this.chart.options.elements.arc)},countVisibleElements:function(){var t=this.getDataset(),e=this.getMeta(),n=0;return o.each(e.data,function(e,i){isNaN(t.data[i])||e.hidden||n++}),n},calculateCircumference:function(t){var e=this.getMeta().count;return e>0&&!isNaN(t)?2*Math.PI/e:0}})}},{25:25,40:40,45:45}],20:[function(t,e,n){"use strict";var i=t(25),a=t(40),o=t(45);i._set("radar",{scale:{type:"radialLinear"},elements:{line:{tension:0}}}),e.exports=function(t){t.controllers.radar=t.DatasetController.extend({datasetElementType:a.Line,dataElementType:a.Point,linkScales:o.noop,update:function(t){var e=this,n=e.getMeta(),i=n.dataset,a=n.data,r=i.custom||{},l=e.getDataset(),s=e.chart.options.elements.line,u=e.chart.scale;void 0!==l.tension&&void 0===l.lineTension&&(l.lineTension=l.tension),o.extend(n.dataset,{_datasetIndex:e.index,_scale:u,_children:a,_loop:!0,_model:{tension:r.tension?r.tension:o.valueOrDefault(l.lineTension,s.tension),backgroundColor:r.backgroundColor?r.backgroundColor:l.backgroundColor||s.backgroundColor,borderWidth:r.borderWidth?r.borderWidth:l.borderWidth||s.borderWidth,borderColor:r.borderColor?r.borderColor:l.borderColor||s.borderColor,fill:r.fill?r.fill:void 0!==l.fill?l.fill:s.fill,borderCapStyle:r.borderCapStyle?r.borderCapStyle:l.borderCapStyle||s.borderCapStyle,borderDash:r.borderDash?r.borderDash:l.borderDash||s.borderDash,borderDashOffset:r.borderDashOffset?r.borderDashOffset:l.borderDashOffset||s.borderDashOffset,borderJoinStyle:r.borderJoinStyle?r.borderJoinStyle:l.borderJoinStyle||s.borderJoinStyle}}),n.dataset.pivot(),o.each(a,function(n,i){e.updateElement(n,i,t)},e),e.updateBezierControlPoints()},updateElement:function(t,e,n){var i=this,a=t.custom||{},r=i.getDataset(),l=i.chart.scale,s=i.chart.options.elements.point,u=l.getPointPositionForValue(e,r.data[e]);void 0!==r.radius&&void 0===r.pointRadius&&(r.pointRadius=r.radius),void 0!==r.hitRadius&&void 0===r.pointHitRadius&&(r.pointHitRadius=r.hitRadius),o.extend(t,{_datasetIndex:i.index,_index:e,_scale:l,_model:{x:n?l.xCenter:u.x,y:n?l.yCenter:u.y,tension:a.tension?a.tension:o.valueOrDefault(r.lineTension,i.chart.options.elements.line.tension),radius:a.radius?a.radius:o.valueAtIndexOrDefault(r.pointRadius,e,s.radius),backgroundColor:a.backgroundColor?a.backgroundColor:o.valueAtIndexOrDefault(r.pointBackgroundColor,e,s.backgroundColor),borderColor:a.borderColor?a.borderColor:o.valueAtIndexOrDefault(r.pointBorderColor,e,s.borderColor),borderWidth:a.borderWidth?a.borderWidth:o.valueAtIndexOrDefault(r.pointBorderWidth,e,s.borderWidth),pointStyle:a.pointStyle?a.pointStyle:o.valueAtIndexOrDefault(r.pointStyle,e,s.pointStyle),hitRadius:a.hitRadius?a.hitRadius:o.valueAtIndexOrDefault(r.pointHitRadius,e,s.hitRadius)}}),t._model.skip=a.skip?a.skip:isNaN(t._model.x)||isNaN(t._model.y)},updateBezierControlPoints:function(){var t=this.chart.chartArea,e=this.getMeta();o.each(e.data,function(n,i){var a=n._model,r=o.splineCurve(o.previousItem(e.data,i,!0)._model,a,o.nextItem(e.data,i,!0)._model,a.tension);a.controlPointPreviousX=Math.max(Math.min(r.previous.x,t.right),t.left),a.controlPointPreviousY=Math.max(Math.min(r.previous.y,t.bottom),t.top),a.controlPointNextX=Math.max(Math.min(r.next.x,t.right),t.left),a.controlPointNextY=Math.max(Math.min(r.next.y,t.bottom),t.top),n.pivot()})},setHoverStyle:function(t){var e=this.chart.data.datasets[t._datasetIndex],n=t.custom||{},i=t._index,a=t._model;a.radius=n.hoverRadius?n.hoverRadius:o.valueAtIndexOrDefault(e.pointHoverRadius,i,this.chart.options.elements.point.hoverRadius),a.backgroundColor=n.hoverBackgroundColor?n.hoverBackgroundColor:o.valueAtIndexOrDefault(e.pointHoverBackgroundColor,i,o.getHoverColor(a.backgroundColor)),a.borderColor=n.hoverBorderColor?n.hoverBorderColor:o.valueAtIndexOrDefault(e.pointHoverBorderColor,i,o.getHoverColor(a.borderColor)),a.borderWidth=n.hoverBorderWidth?n.hoverBorderWidth:o.valueAtIndexOrDefault(e.pointHoverBorderWidth,i,a.borderWidth)},removeHoverStyle:function(t){var e=this.chart.data.datasets[t._datasetIndex],n=t.custom||{},i=t._index,a=t._model,r=this.chart.options.elements.point;a.radius=n.radius?n.radius:o.valueAtIndexOrDefault(e.pointRadius,i,r.radius),a.backgroundColor=n.backgroundColor?n.backgroundColor:o.valueAtIndexOrDefault(e.pointBackgroundColor,i,r.backgroundColor),a.borderColor=n.borderColor?n.borderColor:o.valueAtIndexOrDefault(e.pointBorderColor,i,r.borderColor),a.borderWidth=n.borderWidth?n.borderWidth:o.valueAtIndexOrDefault(e.pointBorderWidth,i,r.borderWidth)}})}},{25:25,40:40,45:45}],21:[function(t,e,n){"use strict";t(25)._set("scatter",{hover:{mode:"single"},scales:{xAxes:[{id:"x-axis-1",type:"linear",position:"bottom"}],yAxes:[{id:"y-axis-1",type:"linear",position:"left"}]},showLines:!1,tooltips:{callbacks:{title:function(){return""},label:function(t){return"("+t.xLabel+", "+t.yLabel+")"}}}}),e.exports=function(t){t.controllers.scatter=t.controllers.line}},{25:25}],22:[function(t,e,n){"use strict";var i=t(25),a=t(26),o=t(45);i._set("global",{animation:{duration:1e3,easing:"easeOutQuart",onProgress:o.noop,onComplete:o.noop}}),e.exports=function(t){t.Animation=a.extend({chart:null,currentStep:0,numSteps:60,easing:"",render:null,onAnimationProgress:null,onAnimationComplete:null}),t.animationService={frameDuration:17,animations:[],dropFrames:0,request:null,addAnimation:function(t,e,n,i){var a,o,r=this.animations;for(e.chart=t,i||(t.animating=!0),a=0,o=r.length;a<o;++a)if(r[a].chart===t)return void(r[a]=e);r.push(e),1===r.length&&this.requestAnimationFrame()},cancelAnimation:function(t){var e=o.findIndex(this.animations,function(e){return e.chart===t});-1!==e&&(this.animations.splice(e,1),t.animating=!1)},requestAnimationFrame:function(){var t=this;null===t.request&&(t.request=o.requestAnimFrame.call(window,function(){t.request=null,t.startDigest()}))},startDigest:function(){var t=this,e=Date.now(),n=0;t.dropFrames>1&&(n=Math.floor(t.dropFrames),t.dropFrames=t.dropFrames%1),t.advance(1+n);var i=Date.now();t.dropFrames+=(i-e)/t.frameDuration,t.animations.length>0&&t.requestAnimationFrame()},advance:function(t){for(var e,n,i=this.animations,a=0;a<i.length;)n=(e=i[a]).chart,e.currentStep=(e.currentStep||0)+t,e.currentStep=Math.min(e.currentStep,e.numSteps),o.callback(e.render,[n,e],n),o.callback(e.onAnimationProgress,[e],n),e.currentStep>=e.numSteps?(o.callback(e.onAnimationComplete,[e],n),n.animating=!1,i.splice(a,1)):++a}},Object.defineProperty(t.Animation.prototype,"animationObject",{get:function(){return this}}),Object.defineProperty(t.Animation.prototype,"chartInstance",{get:function(){return this.chart},set:function(t){this.chart=t}})}},{25:25,26:26,45:45}],23:[function(t,e,n){"use strict";var i=t(25),a=t(45),o=t(28),r=t(48);e.exports=function(t){function e(t){var e=(t=t||{}).data=t.data||{};return e.datasets=e.datasets||[],e.labels=e.labels||[],t.options=a.configMerge(i.global,i[t.type],t.options||{}),t}function n(t){var e=t.options;e.scale?t.scale.options=e.scale:e.scales&&e.scales.xAxes.concat(e.scales.yAxes).forEach(function(e){t.scales[e.id].options=e}),t.tooltip._options=e.tooltips}function l(t){return"top"===t||"bottom"===t}var s=t.plugins;t.types={},t.instances={},t.controllers={},a.extend(t.prototype,{construct:function(n,i){var o=this;i=e(i);var l=r.acquireContext(n,i),s=l&&l.canvas,u=s&&s.height,d=s&&s.width;o.id=a.uid(),o.ctx=l,o.canvas=s,o.config=i,o.width=d,o.height=u,o.aspectRatio=u?d/u:null,o.options=i.options,o._bufferedRender=!1,o.chart=o,o.controller=o,t.instances[o.id]=o,Object.defineProperty(o,"data",{get:function(){return o.config.data},set:function(t){o.config.data=t}}),l&&s?(o.initialize(),o.update()):console.error("Failed to create chart: can't acquire context from the given item")},initialize:function(){var t=this;return s.notify(t,"beforeInit"),a.retinaScale(t,t.options.devicePixelRatio),t.bindEvents(),t.options.responsive&&t.resize(!0),t.ensureScalesHaveIDs(),t.buildScales(),t.initToolTip(),s.notify(t,"afterInit"),t},clear:function(){return a.canvas.clear(this),this},stop:function(){return t.animationService.cancelAnimation(this),this},resize:function(t){var e=this,n=e.options,i=e.canvas,o=n.maintainAspectRatio&&e.aspectRatio||null,r=Math.max(0,Math.floor(a.getMaximumWidth(i))),l=Math.max(0,Math.floor(o?r/o:a.getMaximumHeight(i)));if((e.width!==r||e.height!==l)&&(i.width=e.width=r,i.height=e.height=l,i.style.width=r+"px",i.style.height=l+"px",a.retinaScale(e,n.devicePixelRatio),!t)){var u={width:r,height:l};s.notify(e,"resize",[u]),e.options.onResize&&e.options.onResize(e,u),e.stop(),e.update(e.options.responsiveAnimationDuration)}},ensureScalesHaveIDs:function(){var t=this.options,e=t.scales||{},n=t.scale;a.each(e.xAxes,function(t,e){t.id=t.id||"x-axis-"+e}),a.each(e.yAxes,function(t,e){t.id=t.id||"y-axis-"+e}),n&&(n.id=n.id||"scale")},buildScales:function(){var e=this,n=e.options,i=e.scales={},o=[];n.scales&&(o=o.concat((n.scales.xAxes||[]).map(function(t){return{options:t,dtype:"category",dposition:"bottom"}}),(n.scales.yAxes||[]).map(function(t){return{options:t,dtype:"linear",dposition:"left"}}))),n.scale&&o.push({options:n.scale,dtype:"radialLinear",isDefault:!0,dposition:"chartArea"}),a.each(o,function(n){var o=n.options,r=a.valueOrDefault(o.type,n.dtype),s=t.scaleService.getScaleConstructor(r);if(s){l(o.position)!==l(n.dposition)&&(o.position=n.dposition);var u=new s({id:o.id,options:o,ctx:e.ctx,chart:e});i[u.id]=u,u.mergeTicksOptions(),n.isDefault&&(e.scale=u)}}),t.scaleService.addScalesToLayout(this)},buildOrUpdateControllers:function(){var e=this,n=[],i=[];return a.each(e.data.datasets,function(a,o){var r=e.getDatasetMeta(o),l=a.type||e.config.type;if(r.type&&r.type!==l&&(e.destroyDatasetMeta(o),r=e.getDatasetMeta(o)),r.type=l,n.push(r.type),r.controller)r.controller.updateIndex(o);else{var s=t.controllers[r.type];if(void 0===s)throw new Error('"'+r.type+'" is not a chart type.');r.controller=new s(e,o),i.push(r.controller)}},e),i},resetElements:function(){var t=this;a.each(t.data.datasets,function(e,n){t.getDatasetMeta(n).controller.reset()},t)},reset:function(){this.resetElements(),this.tooltip.initialize()},update:function(t){var e=this;if(t&&"object"==typeof t||(t={duration:t,lazy:arguments[1]}),n(e),!1!==s.notify(e,"beforeUpdate")){e.tooltip._data=e.data;var i=e.buildOrUpdateControllers();a.each(e.data.datasets,function(t,n){e.getDatasetMeta(n).controller.buildOrUpdateElements()},e),e.updateLayout(),a.each(i,function(t){t.reset()}),e.updateDatasets(),s.notify(e,"afterUpdate"),e._bufferedRender?e._bufferedRequest={duration:t.duration,easing:t.easing,lazy:t.lazy}:e.render(t)}},updateLayout:function(){var e=this;!1!==s.notify(e,"beforeLayout")&&(t.layoutService.update(this,this.width,this.height),s.notify(e,"afterScaleUpdate"),s.notify(e,"afterLayout"))},updateDatasets:function(){var t=this;if(!1!==s.notify(t,"beforeDatasetsUpdate")){for(var e=0,n=t.data.datasets.length;e<n;++e)t.updateDataset(e);s.notify(t,"afterDatasetsUpdate")}},updateDataset:function(t){var e=this,n=e.getDatasetMeta(t),i={meta:n,index:t};!1!==s.notify(e,"beforeDatasetUpdate",[i])&&(n.controller.update(),s.notify(e,"afterDatasetUpdate",[i]))},render:function(e){var n=this;e&&"object"==typeof e||(e={duration:e,lazy:arguments[1]});var i=e.duration,o=e.lazy;if(!1!==s.notify(n,"beforeRender")){var r=n.options.animation,l=function(t){s.notify(n,"afterRender"),a.callback(r&&r.onComplete,[t],n)};if(r&&(void 0!==i&&0!==i||void 0===i&&0!==r.duration)){var u=new t.Animation({numSteps:(i||r.duration)/16.66,easing:e.easing||r.easing,render:function(t,e){var n=a.easing.effects[e.easing],i=e.currentStep,o=i/e.numSteps;t.draw(n(o),o,i)},onAnimationProgress:r.onProgress,onAnimationComplete:l});t.animationService.addAnimation(n,u,i,o)}else n.draw(),l(new t.Animation({numSteps:0,chart:n}));return n}},draw:function(t){var e=this;e.clear(),a.isNullOrUndef(t)&&(t=1),e.transition(t),!1!==s.notify(e,"beforeDraw",[t])&&(a.each(e.boxes,function(t){t.draw(e.chartArea)},e),e.scale&&e.scale.draw(),e.drawDatasets(t),e.tooltip.draw(),s.notify(e,"afterDraw",[t]))},transition:function(t){for(var e=this,n=0,i=(e.data.datasets||[]).length;n<i;++n)e.isDatasetVisible(n)&&e.getDatasetMeta(n).controller.transition(t);e.tooltip.transition(t)},drawDatasets:function(t){var e=this;if(!1!==s.notify(e,"beforeDatasetsDraw",[t])){for(var n=(e.data.datasets||[]).length-1;n>=0;--n)e.isDatasetVisible(n)&&e.drawDataset(n,t);s.notify(e,"afterDatasetsDraw",[t])}},drawDataset:function(t,e){var n=this,i=n.getDatasetMeta(t),a={meta:i,index:t,easingValue:e};!1!==s.notify(n,"beforeDatasetDraw",[a])&&(i.controller.draw(e),s.notify(n,"afterDatasetDraw",[a]))},getElementAtEvent:function(t){return o.modes.single(this,t)},getElementsAtEvent:function(t){return o.modes.label(this,t,{intersect:!0})},getElementsAtXAxis:function(t){return o.modes["x-axis"](this,t,{intersect:!0})},getElementsAtEventForMode:function(t,e,n){var i=o.modes[e];return"function"==typeof i?i(this,t,n):[]},getDatasetAtEvent:function(t){return o.modes.dataset(this,t,{intersect:!0})},getDatasetMeta:function(t){var e=this,n=e.data.datasets[t];n._meta||(n._meta={});var i=n._meta[e.id];return i||(i=n._meta[e.id]={type:null,data:[],dataset:null,controller:null,hidden:null,xAxisID:null,yAxisID:null}),i},getVisibleDatasetCount:function(){for(var t=0,e=0,n=this.data.datasets.length;e<n;++e)this.isDatasetVisible(e)&&t++;return t},isDatasetVisible:function(t){var e=this.getDatasetMeta(t);return"boolean"==typeof e.hidden?!e.hidden:!this.data.datasets[t].hidden},generateLegend:function(){return this.options.legendCallback(this)},destroyDatasetMeta:function(t){var e=this.id,n=this.data.datasets[t],i=n._meta&&n._meta[e];i&&(i.controller.destroy(),delete n._meta[e])},destroy:function(){var e,n,i=this,o=i.canvas;for(i.stop(),e=0,n=i.data.datasets.length;e<n;++e)i.destroyDatasetMeta(e);o&&(i.unbindEvents(),a.canvas.clear(i),r.releaseContext(i.ctx),i.canvas=null,i.ctx=null),s.notify(i,"destroy"),delete t.instances[i.id]},toBase64Image:function(){return this.canvas.toDataURL.apply(this.canvas,arguments)},initToolTip:function(){var e=this;e.tooltip=new t.Tooltip({_chart:e,_chartInstance:e,_data:e.data,_options:e.options.tooltips},e)},bindEvents:function(){var t=this,e=t._listeners={},n=function(){t.eventHandler.apply(t,arguments)};a.each(t.options.events,function(i){r.addEventListener(t,i,n),e[i]=n}),t.options.responsive&&(n=function(){t.resize()},r.addEventListener(t,"resize",n),e.resize=n)},unbindEvents:function(){var t=this,e=t._listeners;e&&(delete t._listeners,a.each(e,function(e,n){r.removeEventListener(t,n,e)}))},updateHoverStyle:function(t,e,n){var i,a,o,r=n?"setHoverStyle":"removeHoverStyle";for(a=0,o=t.length;a<o;++a)(i=t[a])&&this.getDatasetMeta(i._datasetIndex).controller[r](i)},eventHandler:function(t){var e=this,n=e.tooltip;if(!1!==s.notify(e,"beforeEvent",[t])){e._bufferedRender=!0,e._bufferedRequest=null;var i=e.handleEvent(t);i|=n&&n.handleEvent(t),s.notify(e,"afterEvent",[t]);var a=e._bufferedRequest;return a?e.render(a):i&&!e.animating&&(e.stop(),e.render(e.options.hover.animationDuration,!0)),e._bufferedRender=!1,e._bufferedRequest=null,e}},handleEvent:function(t){var e=this,n=e.options||{},i=n.hover,o=!1;return e.lastActive=e.lastActive||[],"mouseout"===t.type?e.active=[]:e.active=e.getElementsAtEventForMode(t,i.mode,i),a.callback(n.onHover||n.hover.onHover,[t.native,e.active],e),"mouseup"!==t.type&&"click"!==t.type||n.onClick&&n.onClick.call(e,t.native,e.active),e.lastActive.length&&e.updateHoverStyle(e.lastActive,i.mode,!1),e.active.length&&i.mode&&e.updateHoverStyle(e.active,i.mode,!0),o=!a.arrayEquals(e.active,e.lastActive),e.lastActive=e.active,o}}),t.Controller=t}},{25:25,28:28,45:45,48:48}],24:[function(t,e,n){"use strict";var i=t(45);e.exports=function(t){function e(t,e){t._chartjs?t._chartjs.listeners.push(e):(Object.defineProperty(t,"_chartjs",{configurable:!0,enumerable:!1,value:{listeners:[e]}}),a.forEach(function(e){var n="onData"+e.charAt(0).toUpperCase()+e.slice(1),a=t[e];Object.defineProperty(t,e,{configurable:!0,enumerable:!1,value:function(){var e=Array.prototype.slice.call(arguments),o=a.apply(this,e);return i.each(t._chartjs.listeners,function(t){"function"==typeof t[n]&&t[n].apply(t,e)}),o}})}))}function n(t,e){var n=t._chartjs;if(n){var i=n.listeners,o=i.indexOf(e);-1!==o&&i.splice(o,1),i.length>0||(a.forEach(function(e){delete t[e]}),delete t._chartjs)}}var a=["push","pop","shift","splice","unshift"];t.DatasetController=function(t,e){this.initialize(t,e)},i.extend(t.DatasetController.prototype,{datasetElementType:null,dataElementType:null,initialize:function(t,e){var n=this;n.chart=t,n.index=e,n.linkScales(),n.addElements()},updateIndex:function(t){this.index=t},linkScales:function(){var t=this,e=t.getMeta(),n=t.getDataset();null===e.xAxisID&&(e.xAxisID=n.xAxisID||t.chart.options.scales.xAxes[0].id),null===e.yAxisID&&(e.yAxisID=n.yAxisID||t.chart.options.scales.yAxes[0].id)},getDataset:function(){return this.chart.data.datasets[this.index]},getMeta:function(){return this.chart.getDatasetMeta(this.index)},getScaleForId:function(t){return this.chart.scales[t]},reset:function(){this.update(!0)},destroy:function(){this._data&&n(this._data,this)},createMetaDataset:function(){var t=this,e=t.datasetElementType;return e&&new e({_chart:t.chart,_datasetIndex:t.index})},createMetaData:function(t){var e=this,n=e.dataElementType;return n&&new n({_chart:e.chart,_datasetIndex:e.index,_index:t})},addElements:function(){var t,e,n=this,i=n.getMeta(),a=n.getDataset().data||[],o=i.data;for(t=0,e=a.length;t<e;++t)o[t]=o[t]||n.createMetaData(t);i.dataset=i.dataset||n.createMetaDataset()},addElementAndReset:function(t){var e=this.createMetaData(t);this.getMeta().data.splice(t,0,e),this.updateElement(e,t,!0)},buildOrUpdateElements:function(){var t=this,i=t.getDataset(),a=i.data||(i.data=[]);t._data!==a&&(t._data&&n(t._data,t),e(a,t),t._data=a),t.resyncElements()},update:i.noop,transition:function(t){for(var e=this.getMeta(),n=e.data||[],i=n.length,a=0;a<i;++a)n[a].transition(t);e.dataset&&e.dataset.transition(t)},draw:function(){var t=this.getMeta(),e=t.data||[],n=e.length,i=0;for(t.dataset&&t.dataset.draw();i<n;++i)e[i].draw()},removeHoverStyle:function(t,e){var n=this.chart.data.datasets[t._datasetIndex],a=t._index,o=t.custom||{},r=i.valueAtIndexOrDefault,l=t._model;l.backgroundColor=o.backgroundColor?o.backgroundColor:r(n.backgroundColor,a,e.backgroundColor),l.borderColor=o.borderColor?o.borderColor:r(n.borderColor,a,e.borderColor),l.borderWidth=o.borderWidth?o.borderWidth:r(n.borderWidth,a,e.borderWidth)},setHoverStyle:function(t){var e=this.chart.data.datasets[t._datasetIndex],n=t._index,a=t.custom||{},o=i.valueAtIndexOrDefault,r=i.getHoverColor,l=t._model;l.backgroundColor=a.hoverBackgroundColor?a.hoverBackgroundColor:o(e.hoverBackgroundColor,n,r(l.backgroundColor)),l.borderColor=a.hoverBorderColor?a.hoverBorderColor:o(e.hoverBorderColor,n,r(l.borderColor)),l.borderWidth=a.hoverBorderWidth?a.hoverBorderWidth:o(e.hoverBorderWidth,n,l.borderWidth)},resyncElements:function(){var t=this,e=t.getMeta(),n=t.getDataset().data,i=e.data.length,a=n.length;a<i?e.data.splice(a,i-a):a>i&&t.insertElements(i,a-i)},insertElements:function(t,e){for(var n=0;n<e;++n)this.addElementAndReset(t+n)},onDataPush:function(){this.insertElements(this.getDataset().data.length-1,arguments.length)},onDataPop:function(){this.getMeta().data.pop()},onDataShift:function(){this.getMeta().data.shift()},onDataSplice:function(t,e){this.getMeta().data.splice(t,e),this.insertElements(t,arguments.length-2)},onDataUnshift:function(){this.insertElements(0,arguments.length)}}),t.DatasetController.extend=i.inherits}},{45:45}],25:[function(t,e,n){"use strict";var i=t(45);e.exports={_set:function(t,e){return i.merge(this[t]||(this[t]={}),e)}}},{45:45}],26:[function(t,e,n){"use strict";function i(t,e,n,i){var o,r,l,s,u,d,c,h,f,g=Object.keys(n);for(o=0,r=g.length;o<r;++o)if(l=g[o],d=n[l],e.hasOwnProperty(l)||(e[l]=d),(s=e[l])!==d&&"_"!==l[0]){if(t.hasOwnProperty(l)||(t[l]=s),u=t[l],(c=typeof d)===typeof u)if("string"===c){if((h=a(u)).valid&&(f=a(d)).valid){e[l]=f.mix(h,i).rgbString();continue}}else if("number"===c&&isFinite(u)&&isFinite(d)){e[l]=u+(d-u)*i;continue}e[l]=d}}var a=t(3),o=t(45),r=function(t){o.extend(this,t),this.initialize.apply(this,arguments)};o.extend(r.prototype,{initialize:function(){this.hidden=!1},pivot:function(){var t=this;return t._view||(t._view=o.clone(t._model)),t._start={},t},transition:function(t){var e=this,n=e._model,a=e._start,o=e._view;return n&&1!==t?(o||(o=e._view={}),a||(a=e._start={}),i(a,o,n,t),e):(e._view=n,e._start=null,e)},tooltipPosition:function(){return{x:this._model.x,y:this._model.y}},hasValue:function(){return o.isNumber(this._model.x)&&o.isNumber(this._model.y)}}),r.extend=o.inherits,e.exports=r},{3:3,45:45}],27:[function(t,e,n){"use strict";var i=t(3),a=t(25),o=t(45);e.exports=function(t){function e(t,e,n){var i;return"string"==typeof t?(i=parseInt(t,10),-1!==t.indexOf("%")&&(i=i/100*e.parentNode[n])):i=t,i}function n(t){return void 0!==t&&null!==t&&"none"!==t}function r(t,i,a){var o=document.defaultView,r=t.parentNode,l=o.getComputedStyle(t)[i],s=o.getComputedStyle(r)[i],u=n(l),d=n(s),c=Number.POSITIVE_INFINITY;return u||d?Math.min(u?e(l,t,a):c,d?e(s,r,a):c):"none"}o.extend=function(t){for(var e=1,n=arguments.length;e<n;e++)o.each(arguments[e],function(e,n){t[n]=e});return t},o.configMerge=function(){return o.merge(o.clone(arguments[0]),[].slice.call(arguments,1),{merger:function(e,n,i,a){var r=n[e]||{},l=i[e];"scales"===e?n[e]=o.scaleMerge(r,l):"scale"===e?n[e]=o.merge(r,[t.scaleService.getScaleDefaults(l.type),l]):o._merger(e,n,i,a)}})},o.scaleMerge=function(){return o.merge(o.clone(arguments[0]),[].slice.call(arguments,1),{merger:function(e,n,i,a){if("xAxes"===e||"yAxes"===e){var r,l,s,u=i[e].length;for(n[e]||(n[e]=[]),r=0;r<u;++r)s=i[e][r],l=o.valueOrDefault(s.type,"xAxes"===e?"category":"linear"),r>=n[e].length&&n[e].push({}),!n[e][r].type||s.type&&s.type!==n[e][r].type?o.merge(n[e][r],[t.scaleService.getScaleDefaults(l),s]):o.merge(n[e][r],s)}else o._merger(e,n,i,a)}})},o.where=function(t,e){if(o.isArray(t)&&Array.prototype.filter)return t.filter(e);var n=[];return o.each(t,function(t){e(t)&&n.push(t)}),n},o.findIndex=Array.prototype.findIndex?function(t,e,n){return t.findIndex(e,n)}:function(t,e,n){n=void 0===n?t:n;for(var i=0,a=t.length;i<a;++i)if(e.call(n,t[i],i,t))return i;return-1},o.findNextWhere=function(t,e,n){o.isNullOrUndef(n)&&(n=-1);for(var i=n+1;i<t.length;i++){var a=t[i];if(e(a))return a}},o.findPreviousWhere=function(t,e,n){o.isNullOrUndef(n)&&(n=t.length);for(var i=n-1;i>=0;i--){var a=t[i];if(e(a))return a}},o.inherits=function(t){var e=this,n=t&&t.hasOwnProperty("constructor")?t.constructor:function(){return e.apply(this,arguments)},i=function(){this.constructor=n};return i.prototype=e.prototype,n.prototype=new i,n.extend=o.inherits,t&&o.extend(n.prototype,t),n.__super__=e.prototype,n},o.isNumber=function(t){return!isNaN(parseFloat(t))&&isFinite(t)},o.almostEquals=function(t,e,n){return Math.abs(t-e)<n},o.almostWhole=function(t,e){var n=Math.round(t);return n-e<t&&n+e>t},o.max=function(t){return t.reduce(function(t,e){return isNaN(e)?t:Math.max(t,e)},Number.NEGATIVE_INFINITY)},o.min=function(t){return t.reduce(function(t,e){return isNaN(e)?t:Math.min(t,e)},Number.POSITIVE_INFINITY)},o.sign=Math.sign?function(t){return Math.sign(t)}:function(t){return 0==(t=+t)||isNaN(t)?t:t>0?1:-1},o.log10=Math.log10?function(t){return Math.log10(t)}:function(t){return Math.log(t)/Math.LN10},o.toRadians=function(t){return t*(Math.PI/180)},o.toDegrees=function(t){return t*(180/Math.PI)},o.getAngleFromPoint=function(t,e){var n=e.x-t.x,i=e.y-t.y,a=Math.sqrt(n*n+i*i),o=Math.atan2(i,n);return o<-.5*Math.PI&&(o+=2*Math.PI),{angle:o,distance:a}},o.distanceBetweenPoints=function(t,e){return Math.sqrt(Math.pow(e.x-t.x,2)+Math.pow(e.y-t.y,2))},o.aliasPixel=function(t){return t%2==0?0:.5},o.splineCurve=function(t,e,n,i){var a=t.skip?e:t,o=e,r=n.skip?e:n,l=Math.sqrt(Math.pow(o.x-a.x,2)+Math.pow(o.y-a.y,2)),s=Math.sqrt(Math.pow(r.x-o.x,2)+Math.pow(r.y-o.y,2)),u=l/(l+s),d=s/(l+s),c=i*(u=isNaN(u)?0:u),h=i*(d=isNaN(d)?0:d);return{previous:{x:o.x-c*(r.x-a.x),y:o.y-c*(r.y-a.y)},next:{x:o.x+h*(r.x-a.x),y:o.y+h*(r.y-a.y)}}},o.EPSILON=Number.EPSILON||1e-14,o.splineCurveMonotone=function(t){var e,n,i,a,r=(t||[]).map(function(t){return{model:t._model,deltaK:0,mK:0}}),l=r.length;for(e=0;e<l;++e)if(!(i=r[e]).model.skip){if(n=e>0?r[e-1]:null,(a=e<l-1?r[e+1]:null)&&!a.model.skip){var s=a.model.x-i.model.x;i.deltaK=0!==s?(a.model.y-i.model.y)/s:0}!n||n.model.skip?i.mK=i.deltaK:!a||a.model.skip?i.mK=n.deltaK:this.sign(n.deltaK)!==this.sign(i.deltaK)?i.mK=0:i.mK=(n.deltaK+i.deltaK)/2}var u,d,c,h;for(e=0;e<l-1;++e)i=r[e],a=r[e+1],i.model.skip||a.model.skip||(o.almostEquals(i.deltaK,0,this.EPSILON)?i.mK=a.mK=0:(u=i.mK/i.deltaK,d=a.mK/i.deltaK,(h=Math.pow(u,2)+Math.pow(d,2))<=9||(c=3/Math.sqrt(h),i.mK=u*c*i.deltaK,a.mK=d*c*i.deltaK)));var f;for(e=0;e<l;++e)(i=r[e]).model.skip||(n=e>0?r[e-1]:null,a=e<l-1?r[e+1]:null,n&&!n.model.skip&&(f=(i.model.x-n.model.x)/3,i.model.controlPointPreviousX=i.model.x-f,i.model.controlPointPreviousY=i.model.y-f*i.mK),a&&!a.model.skip&&(f=(a.model.x-i.model.x)/3,i.model.controlPointNextX=i.model.x+f,i.model.controlPointNextY=i.model.y+f*i.mK))},o.nextItem=function(t,e,n){return n?e>=t.length-1?t[0]:t[e+1]:e>=t.length-1?t[t.length-1]:t[e+1]},o.previousItem=function(t,e,n){return n?e<=0?t[t.length-1]:t[e-1]:e<=0?t[0]:t[e-1]},o.niceNum=function(t,e){var n=Math.floor(o.log10(t)),i=t/Math.pow(10,n);return(e?i<1.5?1:i<3?2:i<7?5:10:i<=1?1:i<=2?2:i<=5?5:10)*Math.pow(10,n)},o.requestAnimFrame="undefined"==typeof window?function(t){t()}:window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||function(t){return window.setTimeout(t,1e3/60)},o.getRelativePosition=function(t,e){var n,i,a=t.originalEvent||t,r=t.currentTarget||t.srcElement,l=r.getBoundingClientRect(),s=a.touches;s&&s.length>0?(n=s[0].clientX,i=s[0].clientY):(n=a.clientX,i=a.clientY);var u=parseFloat(o.getStyle(r,"padding-left")),d=parseFloat(o.getStyle(r,"padding-top")),c=parseFloat(o.getStyle(r,"padding-right")),h=parseFloat(o.getStyle(r,"padding-bottom")),f=l.right-l.left-u-c,g=l.bottom-l.top-d-h;return n=Math.round((n-l.left-u)/f*r.width/e.currentDevicePixelRatio),i=Math.round((i-l.top-d)/g*r.height/e.currentDevicePixelRatio),{x:n,y:i}},o.getConstraintWidth=function(t){return r(t,"max-width","clientWidth")},o.getConstraintHeight=function(t){return r(t,"max-height","clientHeight")},o.getMaximumWidth=function(t){var e=t.parentNode;if(!e)return t.clientWidth;var n=parseInt(o.getStyle(e,"padding-left"),10),i=parseInt(o.getStyle(e,"padding-right"),10),a=e.clientWidth-n-i,r=o.getConstraintWidth(t);return isNaN(r)?a:Math.min(a,r)},o.getMaximumHeight=function(t){var e=t.parentNode;if(!e)return t.clientHeight;var n=parseInt(o.getStyle(e,"padding-top"),10),i=parseInt(o.getStyle(e,"padding-bottom"),10),a=e.clientHeight-n-i,r=o.getConstraintHeight(t);return isNaN(r)?a:Math.min(a,r)},o.getStyle=function(t,e){return t.currentStyle?t.currentStyle[e]:document.defaultView.getComputedStyle(t,null).getPropertyValue(e)},o.retinaScale=function(t,e){var n=t.currentDevicePixelRatio=e||window.devicePixelRatio||1;if(1!==n){var i=t.canvas,a=t.height,o=t.width;i.height=a*n,i.width=o*n,t.ctx.scale(n,n),i.style.height=a+"px",i.style.width=o+"px"}},o.fontString=function(t,e,n){return e+" "+t+"px "+n},o.longestText=function(t,e,n,i){var a=(i=i||{}).data=i.data||{},r=i.garbageCollect=i.garbageCollect||[];i.font!==e&&(a=i.data={},r=i.garbageCollect=[],i.font=e),t.font=e;var l=0;o.each(n,function(e){void 0!==e&&null!==e&&!0!==o.isArray(e)?l=o.measureText(t,a,r,l,e):o.isArray(e)&&o.each(e,function(e){void 0===e||null===e||o.isArray(e)||(l=o.measureText(t,a,r,l,e))})});var s=r.length/2;if(s>n.length){for(var u=0;u<s;u++)delete a[r[u]];r.splice(0,s)}return l},o.measureText=function(t,e,n,i,a){var o=e[a];return o||(o=e[a]=t.measureText(a).width,n.push(a)),o>i&&(i=o),i},o.numberOfLabelLines=function(t){var e=1;return o.each(t,function(t){o.isArray(t)&&t.length>e&&(e=t.length)}),e},o.color=i?function(t){return t instanceof CanvasGradient&&(t=a.global.defaultColor),i(t)}:function(t){return console.error("Color.js not found!"),t},o.getHoverColor=function(t){return t instanceof CanvasPattern?t:o.color(t).saturate(.5).darken(.1).rgbString()}}},{25:25,3:3,45:45}],28:[function(t,e,n){"use strict";function i(t,e){return t.native?{x:t.x,y:t.y}:u.getRelativePosition(t,e)}function a(t,e){var n,i,a,o,r;for(i=0,o=t.data.datasets.length;i<o;++i)if(t.isDatasetVisible(i))for(a=0,r=(n=t.getDatasetMeta(i)).data.length;a<r;++a){var l=n.data[a];l._view.skip||e(l)}}function o(t,e){var n=[];return a(t,function(t){t.inRange(e.x,e.y)&&n.push(t)}),n}function r(t,e,n,i){var o=Number.POSITIVE_INFINITY,r=[];return a(t,function(t){if(!n||t.inRange(e.x,e.y)){var a=t.getCenterPoint(),l=i(e,a);l<o?(r=[t],o=l):l===o&&r.push(t)}}),r}function l(t){var e=-1!==t.indexOf("x"),n=-1!==t.indexOf("y");return function(t,i){var a=e?Math.abs(t.x-i.x):0,o=n?Math.abs(t.y-i.y):0;return Math.sqrt(Math.pow(a,2)+Math.pow(o,2))}}function s(t,e,n){var a=i(e,t);n.axis=n.axis||"x";var s=l(n.axis),u=n.intersect?o(t,a):r(t,a,!1,s),d=[];return u.length?(t.data.datasets.forEach(function(e,n){if(t.isDatasetVisible(n)){var i=t.getDatasetMeta(n).data[u[0]._index];i&&!i._view.skip&&d.push(i)}}),d):[]}var u=t(45);e.exports={modes:{single:function(t,e){var n=i(e,t),o=[];return a(t,function(t){if(t.inRange(n.x,n.y))return o.push(t),o}),o.slice(0,1)},label:s,index:s,dataset:function(t,e,n){var a=i(e,t);n.axis=n.axis||"xy";var s=l(n.axis),u=n.intersect?o(t,a):r(t,a,!1,s);return u.length>0&&(u=t.getDatasetMeta(u[0]._datasetIndex).data),u},"x-axis":function(t,e){return s(t,e,{intersect:!0})},point:function(t,e){return o(t,i(e,t))},nearest:function(t,e,n){var a=i(e,t);n.axis=n.axis||"xy";var o=l(n.axis),s=r(t,a,n.intersect,o);return s.length>1&&s.sort(function(t,e){var n=t.getArea()-e.getArea();return 0===n&&(n=t._datasetIndex-e._datasetIndex),n}),s.slice(0,1)},x:function(t,e,n){var o=i(e,t),r=[],l=!1;return a(t,function(t){t.inXRange(o.x)&&r.push(t),t.inRange(o.x,o.y)&&(l=!0)}),n.intersect&&!l&&(r=[]),r},y:function(t,e,n){var o=i(e,t),r=[],l=!1;return a(t,function(t){t.inYRange(o.y)&&r.push(t),t.inRange(o.x,o.y)&&(l=!0)}),n.intersect&&!l&&(r=[]),r}}}},{45:45}],29:[function(t,e,n){"use strict";t(25)._set("global",{responsive:!0,responsiveAnimationDuration:0,maintainAspectRatio:!0,events:["mousemove","mouseout","click","touchstart","touchmove"],hover:{onHover:null,mode:"nearest",intersect:!0,animationDuration:400},onClick:null,defaultColor:"rgba(0,0,0,0.1)",defaultFontColor:"#666",defaultFontFamily:"'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",defaultFontSize:12,defaultFontStyle:"normal",showLines:!0,elements:{},layout:{padding:{top:0,right:0,bottom:0,left:0}}}),e.exports=function(){var t=function(t,e){return this.construct(t,e),this};return t.Chart=t,t}},{25:25}],30:[function(t,e,n){"use strict";var i=t(45);e.exports=function(t){function e(t,e){return i.where(t,function(t){return t.position===e})}function n(t,e){t.forEach(function(t,e){return t._tmpIndex_=e,t}),t.sort(function(t,n){var i=e?n:t,a=e?t:n;return i.weight===a.weight?i._tmpIndex_-a._tmpIndex_:i.weight-a.weight}),t.forEach(function(t){delete t._tmpIndex_})}t.layoutService={defaults:{},addBox:function(t,e){t.boxes||(t.boxes=[]),e.fullWidth=e.fullWidth||!1,e.position=e.position||"top",e.weight=e.weight||0,t.boxes.push(e)},removeBox:function(t,e){var n=t.boxes?t.boxes.indexOf(e):-1;-1!==n&&t.boxes.splice(n,1)},configure:function(t,e,n){for(var i,a=["fullWidth","position","weight"],o=a.length,r=0;r<o;++r)i=a[r],n.hasOwnProperty(i)&&(e[i]=n[i])},update:function(t,a,o){function r(t){var e=i.findNextWhere(_,function(e){return e.box===t});if(e)if(t.isHorizontal()){var n={left:Math.max(T,D),right:Math.max(F,I),top:0,bottom:0};t.update(t.fullWidth?x:S,y/2,n)}else t.update(e.minSize.width,C)}function l(t){t.isHorizontal()?(t.left=t.fullWidth?d:T,t.right=t.fullWidth?a-c:T+S,t.top=V,t.bottom=V+t.height,V=t.bottom):(t.left=N,t.right=N+t.width,t.top=O,t.bottom=O+C,N=t.right)}if(t){var s=t.options.layout||{},u=i.options.toPadding(s.padding),d=u.left,c=u.right,h=u.top,f=u.bottom,g=e(t.boxes,"left"),p=e(t.boxes,"right"),v=e(t.boxes,"top"),m=e(t.boxes,"bottom"),b=e(t.boxes,"chartArea");n(g,!0),n(p,!1),n(v,!0),n(m,!1);var x=a-d-c,y=o-h-f,k=y/2,w=(a-x/2)/(g.length+p.length),M=(o-k)/(v.length+m.length),S=x,C=y,_=[];i.each(g.concat(p,v,m),function(t){var e,n=t.isHorizontal();n?(e=t.update(t.fullWidth?x:S,M),C-=e.height):(e=t.update(w,k),S-=e.width),_.push({horizontal:n,minSize:e,box:t})});var D=0,I=0,P=0,A=0;i.each(v.concat(m),function(t){if(t.getPadding){var e=t.getPadding();D=Math.max(D,e.left),I=Math.max(I,e.right)}}),i.each(g.concat(p),function(t){if(t.getPadding){var e=t.getPadding();P=Math.max(P,e.top),A=Math.max(A,e.bottom)}});var T=d,F=c,O=h,R=f;i.each(g.concat(p),r),i.each(g,function(t){T+=t.width}),i.each(p,function(t){F+=t.width}),i.each(v.concat(m),r),i.each(v,function(t){O+=t.height}),i.each(m,function(t){R+=t.height}),i.each(g.concat(p),function(t){var e=i.findNextWhere(_,function(e){return e.box===t}),n={left:0,right:0,top:O,bottom:R};e&&t.update(e.minSize.width,C,n)}),T=d,F=c,O=h,R=f,i.each(g,function(t){T+=t.width}),i.each(p,function(t){F+=t.width}),i.each(v,function(t){O+=t.height}),i.each(m,function(t){R+=t.height});var L=Math.max(D-T,0);T+=L,F+=Math.max(I-F,0);var z=Math.max(P-O,0);O+=z,R+=Math.max(A-R,0);var B=o-O-R,W=a-T-F;W===S&&B===C||(i.each(g,function(t){t.height=B}),i.each(p,function(t){t.height=B}),i.each(v,function(t){t.fullWidth||(t.width=W)}),i.each(m,function(t){t.fullWidth||(t.width=W)}),C=B,S=W);var N=d+L,V=h+z;i.each(g.concat(v),l),N+=S,V+=C,i.each(p,l),i.each(m,l),t.chartArea={left:T,top:O,right:T+S,bottom:O+C},i.each(b,function(e){e.left=t.chartArea.left,e.top=t.chartArea.top,e.right=t.chartArea.right,e.bottom=t.chartArea.bottom,e.update(S,C)})}}}}},{45:45}],31:[function(t,e,n){"use strict";var i=t(25),a=t(26),o=t(45);i._set("global",{plugins:{}}),e.exports=function(t){t.plugins={_plugins:[],_cacheId:0,register:function(t){var e=this._plugins;[].concat(t).forEach(function(t){-1===e.indexOf(t)&&e.push(t)}),this._cacheId++},unregister:function(t){var e=this._plugins;[].concat(t).forEach(function(t){var n=e.indexOf(t);-1!==n&&e.splice(n,1)}),this._cacheId++},clear:function(){this._plugins=[],this._cacheId++},count:function(){return this._plugins.length},getAll:function(){return this._plugins},notify:function(t,e,n){var i,a,o,r,l,s=this.descriptors(t),u=s.length;for(i=0;i<u;++i)if(a=s[i],o=a.plugin,"function"==typeof(l=o[e])&&((r=[t].concat(n||[])).push(a.options),!1===l.apply(o,r)))return!1;return!0},descriptors:function(t){var e=t._plugins||(t._plugins={});if(e.id===this._cacheId)return e.descriptors;var n=[],a=[],r=t&&t.config||{},l=r.options&&r.options.plugins||{};return this._plugins.concat(r.plugins||[]).forEach(function(t){if(-1===n.indexOf(t)){var e=t.id,r=l[e];!1!==r&&(!0===r&&(r=o.clone(i.global.plugins[e])),n.push(t),a.push({plugin:t,options:r||{}}))}}),e.descriptors=a,e.id=this._cacheId,a}},t.pluginService=t.plugins,t.PluginBase=a.extend({})}},{25:25,26:26,45:45}],32:[function(t,e,n){"use strict";function i(t){var e,n,i=[];for(e=0,n=t.length;e<n;++e)i.push(t[e].label);return i}function a(t,e,n){var i=t.getPixelForTick(e);return n&&(i-=0===e?(t.getPixelForTick(1)-i)/2:(i-t.getPixelForTick(e-1))/2),i}var o=t(25),r=t(26),l=t(45),s=t(34);o._set("scale",{display:!0,position:"left",offset:!1,gridLines:{display:!0,color:"rgba(0, 0, 0, 0.1)",lineWidth:1,drawBorder:!0,drawOnChartArea:!0,drawTicks:!0,tickMarkLength:10,zeroLineWidth:1,zeroLineColor:"rgba(0,0,0,0.25)",zeroLineBorderDash:[],zeroLineBorderDashOffset:0,offsetGridLines:!1,borderDash:[],borderDashOffset:0},scaleLabel:{display:!1,labelString:"",lineHeight:1.2,padding:{top:4,bottom:4}},ticks:{beginAtZero:!1,minRotation:0,maxRotation:50,mirror:!1,padding:0,reverse:!1,display:!0,autoSkip:!0,autoSkipPadding:0,labelOffset:0,callback:s.formatters.values,minor:{},major:{}}}),e.exports=function(t){function e(t,e,n){return l.isArray(e)?l.longestText(t,n,e):t.measureText(e).width}function n(t){var e=l.valueOrDefault,n=o.global,i=e(t.fontSize,n.defaultFontSize),a=e(t.fontStyle,n.defaultFontStyle),r=e(t.fontFamily,n.defaultFontFamily);return{size:i,style:a,family:r,font:l.fontString(i,a,r)}}function s(t){return l.options.toLineHeight(l.valueOrDefault(t.lineHeight,1.2),l.valueOrDefault(t.fontSize,o.global.defaultFontSize))}t.Scale=r.extend({getPadding:function(){var t=this;return{left:t.paddingLeft||0,top:t.paddingTop||0,right:t.paddingRight||0,bottom:t.paddingBottom||0}},getTicks:function(){return this._ticks},mergeTicksOptions:function(){var t=this.options.ticks;!1===t.minor&&(t.minor={display:!1}),!1===t.major&&(t.major={display:!1});for(var e in t)"major"!==e&&"minor"!==e&&(void 0===t.minor[e]&&(t.minor[e]=t[e]),void 0===t.major[e]&&(t.major[e]=t[e]))},beforeUpdate:function(){l.callback(this.options.beforeUpdate,[this])},update:function(t,e,n){var i,a,o,r,s,u,d=this;for(d.beforeUpdate(),d.maxWidth=t,d.maxHeight=e,d.margins=l.extend({left:0,right:0,top:0,bottom:0},n),d.longestTextCache=d.longestTextCache||{},d.beforeSetDimensions(),d.setDimensions(),d.afterSetDimensions(),d.beforeDataLimits(),d.determineDataLimits(),d.afterDataLimits(),d.beforeBuildTicks(),s=d.buildTicks()||[],d.afterBuildTicks(),d.beforeTickToLabelConversion(),o=d.convertTicksToLabels(s)||d.ticks,d.afterTickToLabelConversion(),d.ticks=o,i=0,a=o.length;i<a;++i)r=o[i],(u=s[i])?u.label=r:s.push(u={label:r,major:!1});return d._ticks=s,d.beforeCalculateTickRotation(),d.calculateTickRotation(),d.afterCalculateTickRotation(),d.beforeFit(),d.fit(),d.afterFit(),d.afterUpdate(),d.minSize},afterUpdate:function(){l.callback(this.options.afterUpdate,[this])},beforeSetDimensions:function(){l.callback(this.options.beforeSetDimensions,[this])},setDimensions:function(){var t=this;t.isHorizontal()?(t.width=t.maxWidth,t.left=0,t.right=t.width):(t.height=t.maxHeight,t.top=0,t.bottom=t.height),t.paddingLeft=0,t.paddingTop=0,t.paddingRight=0,t.paddingBottom=0},afterSetDimensions:function(){l.callback(this.options.afterSetDimensions,[this])},beforeDataLimits:function(){l.callback(this.options.beforeDataLimits,[this])},determineDataLimits:l.noop,afterDataLimits:function(){l.callback(this.options.afterDataLimits,[this])},beforeBuildTicks:function(){l.callback(this.options.beforeBuildTicks,[this])},buildTicks:l.noop,afterBuildTicks:function(){l.callback(this.options.afterBuildTicks,[this])},beforeTickToLabelConversion:function(){l.callback(this.options.beforeTickToLabelConversion,[this])},convertTicksToLabels:function(){var t=this,e=t.options.ticks;t.ticks=t.ticks.map(e.userCallback||e.callback,this)},afterTickToLabelConversion:function(){l.callback(this.options.afterTickToLabelConversion,[this])},beforeCalculateTickRotation:function(){l.callback(this.options.beforeCalculateTickRotation,[this])},calculateTickRotation:function(){var t=this,e=t.ctx,a=t.options.ticks,o=i(t._ticks),r=n(a);e.font=r.font;var s=a.minRotation||0;if(o.length&&t.options.display&&t.isHorizontal())for(var u,d=l.longestText(e,r.font,o,t.longestTextCache),c=d,h=t.getPixelForTick(1)-t.getPixelForTick(0)-6;c>h&&s<a.maxRotation;){var f=l.toRadians(s);if(u=Math.cos(f),Math.sin(f)*d>t.maxHeight){s--;break}s++,c=u*d}t.labelRotation=s},afterCalculateTickRotation:function(){l.callback(this.options.afterCalculateTickRotation,[this])},beforeFit:function(){l.callback(this.options.beforeFit,[this])},fit:function(){var t=this,a=t.minSize={width:0,height:0},o=i(t._ticks),r=t.options,u=r.ticks,d=r.scaleLabel,c=r.gridLines,h=r.display,f=t.isHorizontal(),g=n(u),p=r.gridLines.tickMarkLength;if(a.width=f?t.isFullWidth()?t.maxWidth-t.margins.left-t.margins.right:t.maxWidth:h&&c.drawTicks?p:0,a.height=f?h&&c.drawTicks?p:0:t.maxHeight,d.display&&h){var v=s(d)+l.options.toPadding(d.padding).height;f?a.height+=v:a.width+=v}if(u.display&&h){var m=l.longestText(t.ctx,g.font,o,t.longestTextCache),b=l.numberOfLabelLines(o),x=.5*g.size,y=t.options.ticks.padding;if(f){t.longestLabelWidth=m;var k=l.toRadians(t.labelRotation),w=Math.cos(k),M=Math.sin(k)*m+g.size*b+x*(b-1)+x;a.height=Math.min(t.maxHeight,a.height+M+y),t.ctx.font=g.font;var S=e(t.ctx,o[0],g.font),C=e(t.ctx,o[o.length-1],g.font);0!==t.labelRotation?(t.paddingLeft="bottom"===r.position?w*S+3:w*x+3,t.paddingRight="bottom"===r.position?w*x+3:w*C+3):(t.paddingLeft=S/2+3,t.paddingRight=C/2+3)}else u.mirror?m=0:m+=y+x,a.width=Math.min(t.maxWidth,a.width+m),t.paddingTop=g.size/2,t.paddingBottom=g.size/2}t.handleMargins(),t.width=a.width,t.height=a.height},handleMargins:function(){var t=this;t.margins&&(t.paddingLeft=Math.max(t.paddingLeft-t.margins.left,0),t.paddingTop=Math.max(t.paddingTop-t.margins.top,0),t.paddingRight=Math.max(t.paddingRight-t.margins.right,0),t.paddingBottom=Math.max(t.paddingBottom-t.margins.bottom,0))},afterFit:function(){l.callback(this.options.afterFit,[this])},isHorizontal:function(){return"top"===this.options.position||"bottom"===this.options.position},isFullWidth:function(){return this.options.fullWidth},getRightValue:function(t){if(l.isNullOrUndef(t))return NaN;if("number"==typeof t&&!isFinite(t))return NaN;if(t)if(this.isHorizontal()){if(void 0!==t.x)return this.getRightValue(t.x)}else if(void 0!==t.y)return this.getRightValue(t.y);return t},getLabelForIndex:l.noop,getPixelForValue:l.noop,getValueForPixel:l.noop,getPixelForTick:function(t){var e=this,n=e.options.offset;if(e.isHorizontal()){var i=(e.width-(e.paddingLeft+e.paddingRight))/Math.max(e._ticks.length-(n?0:1),1),a=i*t+e.paddingLeft;n&&(a+=i/2);var o=e.left+Math.round(a);return o+=e.isFullWidth()?e.margins.left:0}var r=e.height-(e.paddingTop+e.paddingBottom);return e.top+t*(r/(e._ticks.length-1))},getPixelForDecimal:function(t){var e=this;if(e.isHorizontal()){var n=(e.width-(e.paddingLeft+e.paddingRight))*t+e.paddingLeft,i=e.left+Math.round(n);return i+=e.isFullWidth()?e.margins.left:0}return e.top+t*e.height},getBasePixel:function(){return this.getPixelForValue(this.getBaseValue())},getBaseValue:function(){var t=this,e=t.min,n=t.max;return t.beginAtZero?0:e<0&&n<0?n:e>0&&n>0?e:0},_autoSkip:function(t){var e,n,i,a,o=this,r=o.isHorizontal(),s=o.options.ticks.minor,u=t.length,d=l.toRadians(o.labelRotation),c=Math.cos(d),h=o.longestLabelWidth*c,f=[];for(s.maxTicksLimit&&(a=s.maxTicksLimit),r&&(e=!1,(h+s.autoSkipPadding)*u>o.width-(o.paddingLeft+o.paddingRight)&&(e=1+Math.floor((h+s.autoSkipPadding)*u/(o.width-(o.paddingLeft+o.paddingRight)))),a&&u>a&&(e=Math.max(e,Math.floor(u/a)))),n=0;n<u;n++)i=t[n],((e>1&&n%e>0||n%e==0&&n+e>=u)&&n!==u-1||l.isNullOrUndef(i.label))&&delete i.label,f.push(i);return f},draw:function(t){var e=this,i=e.options;if(i.display){var r=e.ctx,u=o.global,d=i.ticks.minor,c=i.ticks.major||d,h=i.gridLines,f=i.scaleLabel,g=0!==e.labelRotation,p=e.isHorizontal(),v=d.autoSkip?e._autoSkip(e.getTicks()):e.getTicks(),m=l.valueOrDefault(d.fontColor,u.defaultFontColor),b=n(d),x=l.valueOrDefault(c.fontColor,u.defaultFontColor),y=n(c),k=h.drawTicks?h.tickMarkLength:0,w=l.valueOrDefault(f.fontColor,u.defaultFontColor),M=n(f),S=l.options.toPadding(f.padding),C=l.toRadians(e.labelRotation),_=[],D="right"===i.position?e.left:e.right-k,I="right"===i.position?e.left+k:e.right,P="bottom"===i.position?e.top:e.bottom-k,A="bottom"===i.position?e.top+k:e.bottom;if(l.each(v,function(n,o){if(void 0!==n.label){var r,s,c,f,m=n.label;o===e.zeroLineIndex&&i.offset===h.offsetGridLines?(r=h.zeroLineWidth,s=h.zeroLineColor,c=h.zeroLineBorderDash,f=h.zeroLineBorderDashOffset):(r=l.valueAtIndexOrDefault(h.lineWidth,o),s=l.valueAtIndexOrDefault(h.color,o),c=l.valueOrDefault(h.borderDash,u.borderDash),f=l.valueOrDefault(h.borderDashOffset,u.borderDashOffset));var b,x,y,w,M,S,T,F,O,R,L="middle",z="middle",B=d.padding;if(p){var W=k+B;"bottom"===i.position?(z=g?"middle":"top",L=g?"right":"center",R=e.top+W):(z=g?"middle":"bottom",L=g?"left":"center",R=e.bottom-W);var N=a(e,o,h.offsetGridLines&&v.length>1);N<e.left&&(s="rgba(0,0,0,0)"),N+=l.aliasPixel(r),O=e.getPixelForTick(o)+d.labelOffset,b=y=M=T=N,x=P,w=A,S=t.top,F=t.bottom}else{var V,E="left"===i.position;d.mirror?(L=E?"left":"right",V=B):(L=E?"right":"left",V=k+B),O=E?e.right-V:e.left+V;var H=a(e,o,h.offsetGridLines&&v.length>1);H<e.top&&(s="rgba(0,0,0,0)"),H+=l.aliasPixel(r),R=e.getPixelForTick(o)+d.labelOffset,b=D,y=I,M=t.left,T=t.right,x=w=S=F=H}_.push({tx1:b,ty1:x,tx2:y,ty2:w,x1:M,y1:S,x2:T,y2:F,labelX:O,labelY:R,glWidth:r,glColor:s,glBorderDash:c,glBorderDashOffset:f,rotation:-1*C,label:m,major:n.major,textBaseline:z,textAlign:L})}}),l.each(_,function(t){if(h.display&&(r.save(),r.lineWidth=t.glWidth,r.strokeStyle=t.glColor,r.setLineDash&&(r.setLineDash(t.glBorderDash),r.lineDashOffset=t.glBorderDashOffset),r.beginPath(),h.drawTicks&&(r.moveTo(t.tx1,t.ty1),r.lineTo(t.tx2,t.ty2)),h.drawOnChartArea&&(r.moveTo(t.x1,t.y1),r.lineTo(t.x2,t.y2)),r.stroke(),r.restore()),d.display){r.save(),r.translate(t.labelX,t.labelY),r.rotate(t.rotation),r.font=t.major?y.font:b.font,r.fillStyle=t.major?x:m,r.textBaseline=t.textBaseline,r.textAlign=t.textAlign;var e=t.label;if(l.isArray(e))for(var n=0,i=0;n<e.length;++n)r.fillText(""+e[n],0,i),i+=1.5*b.size;else r.fillText(e,0,0);r.restore()}}),f.display){var T,F,O=0,R=s(f)/2;if(p)T=e.left+(e.right-e.left)/2,F="bottom"===i.position?e.bottom-R-S.bottom:e.top+R+S.top;else{var L="left"===i.position;T=L?e.left+R+S.top:e.right-R-S.top,F=e.top+(e.bottom-e.top)/2,O=L?-.5*Math.PI:.5*Math.PI}r.save(),r.translate(T,F),r.rotate(O),r.textAlign="center",r.textBaseline="middle",r.fillStyle=w,r.font=M.font,r.fillText(f.labelString,0,0),r.restore()}if(h.drawBorder){r.lineWidth=l.valueAtIndexOrDefault(h.lineWidth,0),r.strokeStyle=l.valueAtIndexOrDefault(h.color,0);var z=e.left,B=e.right,W=e.top,N=e.bottom,V=l.aliasPixel(r.lineWidth);p?(W=N="top"===i.position?e.bottom:e.top,W+=V,N+=V):(z=B="left"===i.position?e.right:e.left,z+=V,B+=V),r.beginPath(),r.moveTo(z,W),r.lineTo(B,N),r.stroke()}}}})}},{25:25,26:26,34:34,45:45}],33:[function(t,e,n){"use strict";var i=t(25),a=t(45);e.exports=function(t){t.scaleService={constructors:{},defaults:{},registerScaleType:function(t,e,n){this.constructors[t]=e,this.defaults[t]=a.clone(n)},getScaleConstructor:function(t){return this.constructors.hasOwnProperty(t)?this.constructors[t]:void 0},getScaleDefaults:function(t){return this.defaults.hasOwnProperty(t)?a.merge({},[i.scale,this.defaults[t]]):{}},updateScaleDefaults:function(t,e){var n=this;n.defaults.hasOwnProperty(t)&&(n.defaults[t]=a.extend(n.defaults[t],e))},addScalesToLayout:function(e){a.each(e.scales,function(n){n.fullWidth=n.options.fullWidth,n.position=n.options.position,n.weight=n.options.weight,t.layoutService.addBox(e,n)})}}}},{25:25,45:45}],34:[function(t,e,n){"use strict";var i=t(45);e.exports={generators:{linear:function(t,e){var n,a=[];if(t.stepSize&&t.stepSize>0)n=t.stepSize;else{var o=i.niceNum(e.max-e.min,!1);n=i.niceNum(o/(t.maxTicks-1),!0)}var r=Math.floor(e.min/n)*n,l=Math.ceil(e.max/n)*n;t.min&&t.max&&t.stepSize&&i.almostWhole((t.max-t.min)/t.stepSize,n/1e3)&&(r=t.min,l=t.max);var s=(l-r)/n;s=i.almostEquals(s,Math.round(s),n/1e3)?Math.round(s):Math.ceil(s),a.push(void 0!==t.min?t.min:r);for(var u=1;u<s;++u)a.push(r+u*n);return a.push(void 0!==t.max?t.max:l),a},logarithmic:function(t,e){var n,a,o=[],r=i.valueOrDefault,l=r(t.min,Math.pow(10,Math.floor(i.log10(e.min)))),s=Math.floor(i.log10(e.max)),u=Math.ceil(e.max/Math.pow(10,s));0===l?(n=Math.floor(i.log10(e.minNotZero)),a=Math.floor(e.minNotZero/Math.pow(10,n)),o.push(l),l=a*Math.pow(10,n)):(n=Math.floor(i.log10(l)),a=Math.floor(l/Math.pow(10,n)));do{o.push(l),10===++a&&(a=1,++n),l=a*Math.pow(10,n)}while(n<s||n===s&&a<u);var d=r(t.max,l);return o.push(d),o}},formatters:{values:function(t){return i.isArray(t)?t:""+t},linear:function(t,e,n){var a=n.length>3?n[2]-n[1]:n[1]-n[0];Math.abs(a)>1&&t!==Math.floor(t)&&(a=t-Math.floor(t));var o=i.log10(Math.abs(a)),r="";if(0!==t){var l=-1*Math.floor(o);l=Math.max(Math.min(l,20),0),r=t.toFixed(l)}else r="0";return r},logarithmic:function(t,e,n){var a=t/Math.pow(10,Math.floor(i.log10(t)));return 0===t?"0":1===a||2===a||5===a||0===e||e===n.length-1?t.toExponential():""}}}},{45:45}],35:[function(t,e,n){"use strict";var i=t(25),a=t(26),o=t(45);i._set("global",{tooltips:{enabled:!0,custom:null,mode:"nearest",position:"average",intersect:!0,backgroundColor:"rgba(0,0,0,0.8)",titleFontStyle:"bold",titleSpacing:2,titleMarginBottom:6,titleFontColor:"#fff",titleAlign:"left",bodySpacing:2,bodyFontColor:"#fff",bodyAlign:"left",footerFontStyle:"bold",footerSpacing:2,footerMarginTop:6,footerFontColor:"#fff",footerAlign:"left",yPadding:6,xPadding:6,caretPadding:2,caretSize:5,cornerRadius:6,multiKeyBackground:"#fff",displayColors:!0,borderColor:"rgba(0,0,0,0)",borderWidth:0,callbacks:{beforeTitle:o.noop,title:function(t,e){var n="",i=e.labels,a=i?i.length:0;if(t.length>0){var o=t[0];o.xLabel?n=o.xLabel:a>0&&o.index<a&&(n=i[o.index])}return n},afterTitle:o.noop,beforeBody:o.noop,beforeLabel:o.noop,label:function(t,e){var n=e.datasets[t.datasetIndex].label||"";return n&&(n+=": "),n+=t.yLabel},labelColor:function(t,e){var n=e.getDatasetMeta(t.datasetIndex).data[t.index]._view;return{borderColor:n.borderColor,backgroundColor:n.backgroundColor}},labelTextColor:function(){return this._options.bodyFontColor},afterLabel:o.noop,afterBody:o.noop,beforeFooter:o.noop,footer:o.noop,afterFooter:o.noop}}}),e.exports=function(t){function e(t,e){var n=o.color(t);return n.alpha(e*n.alpha()).rgbaString()}function n(t,e){return e&&(o.isArray(e)?Array.prototype.push.apply(t,e):t.push(e)),t}function r(t){var e=t._xScale,n=t._yScale||t._scale,i=t._index,a=t._datasetIndex;return{xLabel:e?e.getLabelForIndex(i,a):"",yLabel:n?n.getLabelForIndex(i,a):"",index:i,datasetIndex:a,x:t._model.x,y:t._model.y}}function l(t){var e=i.global,n=o.valueOrDefault;return{xPadding:t.xPadding,yPadding:t.yPadding,xAlign:t.xAlign,yAlign:t.yAlign,bodyFontColor:t.bodyFontColor,_bodyFontFamily:n(t.bodyFontFamily,e.defaultFontFamily),_bodyFontStyle:n(t.bodyFontStyle,e.defaultFontStyle),_bodyAlign:t.bodyAlign,bodyFontSize:n(t.bodyFontSize,e.defaultFontSize),bodySpacing:t.bodySpacing,titleFontColor:t.titleFontColor,_titleFontFamily:n(t.titleFontFamily,e.defaultFontFamily),_titleFontStyle:n(t.titleFontStyle,e.defaultFontStyle),titleFontSize:n(t.titleFontSize,e.defaultFontSize),_titleAlign:t.titleAlign,titleSpacing:t.titleSpacing,titleMarginBottom:t.titleMarginBottom,footerFontColor:t.footerFontColor,_footerFontFamily:n(t.footerFontFamily,e.defaultFontFamily),_footerFontStyle:n(t.footerFontStyle,e.defaultFontStyle),footerFontSize:n(t.footerFontSize,e.defaultFontSize),_footerAlign:t.footerAlign,footerSpacing:t.footerSpacing,footerMarginTop:t.footerMarginTop,caretSize:t.caretSize,cornerRadius:t.cornerRadius,backgroundColor:t.backgroundColor,opacity:0,legendColorBackground:t.multiKeyBackground,displayColors:t.displayColors,borderColor:t.borderColor,borderWidth:t.borderWidth}}function s(t,e){var n=t._chart.ctx,i=2*e.yPadding,a=0,r=e.body,l=r.reduce(function(t,e){return t+e.before.length+e.lines.length+e.after.length},0);l+=e.beforeBody.length+e.afterBody.length;var s=e.title.length,u=e.footer.length,d=e.titleFontSize,c=e.bodyFontSize,h=e.footerFontSize;i+=s*d,i+=s?(s-1)*e.titleSpacing:0,i+=s?e.titleMarginBottom:0,i+=l*c,i+=l?(l-1)*e.bodySpacing:0,i+=u?e.footerMarginTop:0,i+=u*h,i+=u?(u-1)*e.footerSpacing:0;var f=0,g=function(t){a=Math.max(a,n.measureText(t).width+f)};return n.font=o.fontString(d,e._titleFontStyle,e._titleFontFamily),o.each(e.title,g),n.font=o.fontString(c,e._bodyFontStyle,e._bodyFontFamily),o.each(e.beforeBody.concat(e.afterBody),g),f=e.displayColors?c+2:0,o.each(r,function(t){o.each(t.before,g),o.each(t.lines,g),o.each(t.after,g)}),f=0,n.font=o.fontString(h,e._footerFontStyle,e._footerFontFamily),o.each(e.footer,g),a+=2*e.xPadding,{width:a,height:i}}function u(t,e){var n=t._model,i=t._chart,a=t._chart.chartArea,o="center",r="center";n.y<e.height?r="top":n.y>i.height-e.height&&(r="bottom");var l,s,u,d,c,h=(a.left+a.right)/2,f=(a.top+a.bottom)/2;"center"===r?(l=function(t){return t<=h},s=function(t){return t>h}):(l=function(t){return t<=e.width/2},s=function(t){return t>=i.width-e.width/2}),u=function(t){return t+e.width>i.width},d=function(t){return t-e.width<0},c=function(t){return t<=f?"top":"bottom"},l(n.x)?(o="left",u(n.x)&&(o="center",r=c(n.y))):s(n.x)&&(o="right",d(n.x)&&(o="center",r=c(n.y)));var g=t._options;return{xAlign:g.xAlign?g.xAlign:o,yAlign:g.yAlign?g.yAlign:r}}function d(t,e,n){var i=t.x,a=t.y,o=t.caretSize,r=t.caretPadding,l=t.cornerRadius,s=n.xAlign,u=n.yAlign,d=o+r,c=l+r;return"right"===s?i-=e.width:"center"===s&&(i-=e.width/2),"top"===u?a+=d:a-="bottom"===u?e.height+d:e.height/2,"center"===u?"left"===s?i+=d:"right"===s&&(i-=d):"left"===s?i-=c:"right"===s&&(i+=c),{x:i,y:a}}t.Tooltip=a.extend({initialize:function(){this._model=l(this._options)},getTitle:function(){var t=this,e=t._options.callbacks,i=e.beforeTitle.apply(t,arguments),a=e.title.apply(t,arguments),o=e.afterTitle.apply(t,arguments),r=[];return r=n(r,i),r=n(r,a),r=n(r,o)},getBeforeBody:function(){var t=this._options.callbacks.beforeBody.apply(this,arguments);return o.isArray(t)?t:void 0!==t?[t]:[]},getBody:function(t,e){var i=this,a=i._options.callbacks,r=[];return o.each(t,function(t){var o={before:[],lines:[],after:[]};n(o.before,a.beforeLabel.call(i,t,e)),n(o.lines,a.label.call(i,t,e)),n(o.after,a.afterLabel.call(i,t,e)),r.push(o)}),r},getAfterBody:function(){var t=this._options.callbacks.afterBody.apply(this,arguments);return o.isArray(t)?t:void 0!==t?[t]:[]},getFooter:function(){var t=this,e=t._options.callbacks,i=e.beforeFooter.apply(t,arguments),a=e.footer.apply(t,arguments),o=e.afterFooter.apply(t,arguments),r=[];return r=n(r,i),r=n(r,a),r=n(r,o)},update:function(e){var n,i,a=this,c=a._options,h=a._model,f=a._model=l(c),g=a._active,p=a._data,v={xAlign:h.xAlign,yAlign:h.yAlign},m={x:h.x,y:h.y},b={width:h.width,height:h.height},x={x:h.caretX,y:h.caretY};if(g.length){f.opacity=1;var y=[],k=[];x=t.Tooltip.positioners[c.position](g,a._eventPosition);var w=[];for(n=0,i=g.length;n<i;++n)w.push(r(g[n]));c.filter&&(w=w.filter(function(t){return c.filter(t,p)})),c.itemSort&&(w=w.sort(function(t,e){return c.itemSort(t,e,p)})),o.each(w,function(t){y.push(c.callbacks.labelColor.call(a,t,a._chart)),k.push(c.callbacks.labelTextColor.call(a,t,a._chart))}),f.title=a.getTitle(w,p),f.beforeBody=a.getBeforeBody(w,p),f.body=a.getBody(w,p),f.afterBody=a.getAfterBody(w,p),f.footer=a.getFooter(w,p),f.x=Math.round(x.x),f.y=Math.round(x.y),f.caretPadding=c.caretPadding,f.labelColors=y,f.labelTextColors=k,f.dataPoints=w,m=d(f,b=s(this,f),v=u(this,b))}else f.opacity=0;return f.xAlign=v.xAlign,f.yAlign=v.yAlign,f.x=m.x,f.y=m.y,f.width=b.width,f.height=b.height,f.caretX=x.x,f.caretY=x.y,a._model=f,e&&c.custom&&c.custom.call(a,f),a},drawCaret:function(t,e){var n=this._chart.ctx,i=this._view,a=this.getCaretPosition(t,e,i);n.lineTo(a.x1,a.y1),n.lineTo(a.x2,a.y2),n.lineTo(a.x3,a.y3)},getCaretPosition:function(t,e,n){var i,a,o,r,l,s,u=n.caretSize,d=n.cornerRadius,c=n.xAlign,h=n.yAlign,f=t.x,g=t.y,p=e.width,v=e.height;if("center"===h)l=g+v/2,"left"===c?(a=(i=f)-u,o=i,r=l+u,s=l-u):(a=(i=f+p)+u,o=i,r=l-u,s=l+u);else if("left"===c?(i=(a=f+d+u)-u,o=a+u):"right"===c?(i=(a=f+p-d-u)-u,o=a+u):(i=(a=f+p/2)-u,o=a+u),"top"===h)l=(r=g)-u,s=r;else{l=(r=g+v)+u,s=r;var m=o;o=i,i=m}return{x1:i,x2:a,x3:o,y1:r,y2:l,y3:s}},drawTitle:function(t,n,i,a){var r=n.title;if(r.length){i.textAlign=n._titleAlign,i.textBaseline="top";var l=n.titleFontSize,s=n.titleSpacing;i.fillStyle=e(n.titleFontColor,a),i.font=o.fontString(l,n._titleFontStyle,n._titleFontFamily);var u,d;for(u=0,d=r.length;u<d;++u)i.fillText(r[u],t.x,t.y),t.y+=l+s,u+1===r.length&&(t.y+=n.titleMarginBottom-s)}},drawBody:function(t,n,i,a){var r=n.bodyFontSize,l=n.bodySpacing,s=n.body;i.textAlign=n._bodyAlign,i.textBaseline="top",i.font=o.fontString(r,n._bodyFontStyle,n._bodyFontFamily);var u=0,d=function(e){i.fillText(e,t.x+u,t.y),t.y+=r+l};o.each(n.beforeBody,d);var c=n.displayColors;u=c?r+2:0,o.each(s,function(l,s){o.each(l.before,d),o.each(l.lines,function(o){if(c){i.fillStyle=e(n.legendColorBackground,a),i.fillRect(t.x,t.y,r,r),i.lineWidth=1,i.strokeStyle=e(n.labelColors[s].borderColor,a),i.strokeRect(t.x,t.y,r,r),i.fillStyle=e(n.labelColors[s].backgroundColor,a),i.fillRect(t.x+1,t.y+1,r-2,r-2);var l=e(n.labelTextColors[s],a);i.fillStyle=l}d(o)}),o.each(l.after,d)}),u=0,o.each(n.afterBody,d),t.y-=l},drawFooter:function(t,n,i,a){var r=n.footer;r.length&&(t.y+=n.footerMarginTop,i.textAlign=n._footerAlign,i.textBaseline="top",i.fillStyle=e(n.footerFontColor,a),i.font=o.fontString(n.footerFontSize,n._footerFontStyle,n._footerFontFamily),o.each(r,function(e){i.fillText(e,t.x,t.y),t.y+=n.footerFontSize+n.footerSpacing}))},drawBackground:function(t,n,i,a,o){i.fillStyle=e(n.backgroundColor,o),i.strokeStyle=e(n.borderColor,o),i.lineWidth=n.borderWidth;var r=n.xAlign,l=n.yAlign,s=t.x,u=t.y,d=a.width,c=a.height,h=n.cornerRadius;i.beginPath(),i.moveTo(s+h,u),"top"===l&&this.drawCaret(t,a),i.lineTo(s+d-h,u),i.quadraticCurveTo(s+d,u,s+d,u+h),"center"===l&&"right"===r&&this.drawCaret(t,a),i.lineTo(s+d,u+c-h),i.quadraticCurveTo(s+d,u+c,s+d-h,u+c),"bottom"===l&&this.drawCaret(t,a),i.lineTo(s+h,u+c),i.quadraticCurveTo(s,u+c,s,u+c-h),"center"===l&&"left"===r&&this.drawCaret(t,a),i.lineTo(s,u+h),i.quadraticCurveTo(s,u,s+h,u),i.closePath(),i.fill(),n.borderWidth>0&&i.stroke()},draw:function(){var t=this._chart.ctx,e=this._view;if(0!==e.opacity){var n={width:e.width,height:e.height},i={x:e.x,y:e.y},a=Math.abs(e.opacity<.001)?0:e.opacity,o=e.title.length||e.beforeBody.length||e.body.length||e.afterBody.length||e.footer.length;this._options.enabled&&o&&(this.drawBackground(i,e,t,n,a),i.x+=e.xPadding,i.y+=e.yPadding,this.drawTitle(i,e,t,a),this.drawBody(i,e,t,a),this.drawFooter(i,e,t,a))}},handleEvent:function(t){var e=this,n=e._options,i=!1;if(e._lastActive=e._lastActive||[],"mouseout"===t.type?e._active=[]:e._active=e._chart.getElementsAtEventForMode(t,n.mode,n),!(i=!o.arrayEquals(e._active,e._lastActive)))return!1;if(e._lastActive=e._active,n.enabled||n.custom){e._eventPosition={x:t.x,y:t.y};var a=e._model;e.update(!0),e.pivot(),i|=a.x!==e._model.x||a.y!==e._model.y}return i}}),t.Tooltip.positioners={average:function(t){if(!t.length)return!1;var e,n,i=0,a=0,o=0;for(e=0,n=t.length;e<n;++e){var r=t[e];if(r&&r.hasValue()){var l=r.tooltipPosition();i+=l.x,a+=l.y,++o}}return{x:Math.round(i/o),y:Math.round(a/o)}},nearest:function(t,e){var n,i,a,r=e.x,l=e.y,s=Number.POSITIVE_INFINITY;for(n=0,i=t.length;n<i;++n){var u=t[n];if(u&&u.hasValue()){var d=u.getCenterPoint(),c=o.distanceBetweenPoints(e,d);c<s&&(s=c,a=u)}}if(a){var h=a.tooltipPosition();r=h.x,l=h.y}return{x:r,y:l}}}}},{25:25,26:26,45:45}],36:[function(t,e,n){"use strict";var i=t(25),a=t(26),o=t(45);i._set("global",{elements:{arc:{backgroundColor:i.global.defaultColor,borderColor:"#fff",borderWidth:2}}}),e.exports=a.extend({inLabelRange:function(t){var e=this._view;return!!e&&Math.pow(t-e.x,2)<Math.pow(e.radius+e.hoverRadius,2)},inRange:function(t,e){var n=this._view;if(n){for(var i=o.getAngleFromPoint(n,{x:t,y:e}),a=i.angle,r=i.distance,l=n.startAngle,s=n.endAngle;s<l;)s+=2*Math.PI;for(;a>s;)a-=2*Math.PI;for(;a<l;)a+=2*Math.PI;var u=a>=l&&a<=s,d=r>=n.innerRadius&&r<=n.outerRadius;return u&&d}return!1},getCenterPoint:function(){var t=this._view,e=(t.startAngle+t.endAngle)/2,n=(t.innerRadius+t.outerRadius)/2;return{x:t.x+Math.cos(e)*n,y:t.y+Math.sin(e)*n}},getArea:function(){var t=this._view;return Math.PI*((t.endAngle-t.startAngle)/(2*Math.PI))*(Math.pow(t.outerRadius,2)-Math.pow(t.innerRadius,2))},tooltipPosition:function(){var t=this._view,e=t.startAngle+(t.endAngle-t.startAngle)/2,n=(t.outerRadius-t.innerRadius)/2+t.innerRadius;return{x:t.x+Math.cos(e)*n,y:t.y+Math.sin(e)*n}},draw:function(){var t=this._chart.ctx,e=this._view,n=e.startAngle,i=e.endAngle;t.beginPath(),t.arc(e.x,e.y,e.outerRadius,n,i),t.arc(e.x,e.y,e.innerRadius,i,n,!0),t.closePath(),t.strokeStyle=e.borderColor,t.lineWidth=e.borderWidth,t.fillStyle=e.backgroundColor,t.fill(),t.lineJoin="bevel",e.borderWidth&&t.stroke()}})},{25:25,26:26,45:45}],37:[function(t,e,n){"use strict";var i=t(25),a=t(26),o=t(45),r=i.global;i._set("global",{elements:{line:{tension:.4,backgroundColor:r.defaultColor,borderWidth:3,borderColor:r.defaultColor,borderCapStyle:"butt",borderDash:[],borderDashOffset:0,borderJoinStyle:"miter",capBezierPoints:!0,fill:!0}}}),e.exports=a.extend({draw:function(){var t,e,n,i,a=this,l=a._view,s=a._chart.ctx,u=l.spanGaps,d=a._children.slice(),c=r.elements.line,h=-1;for(a._loop&&d.length&&d.push(d[0]),s.save(),s.lineCap=l.borderCapStyle||c.borderCapStyle,s.setLineDash&&s.setLineDash(l.borderDash||c.borderDash),s.lineDashOffset=l.borderDashOffset||c.borderDashOffset,s.lineJoin=l.borderJoinStyle||c.borderJoinStyle,s.lineWidth=l.borderWidth||c.borderWidth,s.strokeStyle=l.borderColor||r.defaultColor,s.beginPath(),h=-1,t=0;t<d.length;++t)e=d[t],n=o.previousItem(d,t),i=e._view,0===t?i.skip||(s.moveTo(i.x,i.y),h=t):(n=-1===h?n:d[h],i.skip||(h!==t-1&&!u||-1===h?s.moveTo(i.x,i.y):o.canvas.lineTo(s,n._view,e._view),h=t));s.stroke(),s.restore()}})},{25:25,26:26,45:45}],38:[function(t,e,n){"use strict";function i(t){var e=this._view;return!!e&&Math.pow(t-e.x,2)<Math.pow(e.radius+e.hitRadius,2)}var a=t(25),o=t(26),r=t(45),l=a.global.defaultColor;a._set("global",{elements:{point:{radius:3,pointStyle:"circle",backgroundColor:l,borderColor:l,borderWidth:1,hitRadius:1,hoverRadius:4,hoverBorderWidth:1}}}),e.exports=o.extend({inRange:function(t,e){var n=this._view;return!!n&&Math.pow(t-n.x,2)+Math.pow(e-n.y,2)<Math.pow(n.hitRadius+n.radius,2)},inLabelRange:i,inXRange:i,inYRange:function(t){var e=this._view;return!!e&&Math.pow(t-e.y,2)<Math.pow(e.radius+e.hitRadius,2)},getCenterPoint:function(){var t=this._view;return{x:t.x,y:t.y}},getArea:function(){return Math.PI*Math.pow(this._view.radius,2)},tooltipPosition:function(){var t=this._view;return{x:t.x,y:t.y,padding:t.radius+t.borderWidth}},draw:function(t){var e=this._view,n=this._model,i=this._chart.ctx,o=e.pointStyle,s=e.radius,u=e.x,d=e.y,c=r.color,h=0;e.skip||(i.strokeStyle=e.borderColor||l,i.lineWidth=r.valueOrDefault(e.borderWidth,a.global.elements.point.borderWidth),i.fillStyle=e.backgroundColor||l,void 0!==t&&(n.x<t.left||1.01*t.right<n.x||n.y<t.top||1.01*t.bottom<n.y)&&(n.x<t.left?h=(u-n.x)/(t.left-n.x):1.01*t.right<n.x?h=(n.x-u)/(n.x-t.right):n.y<t.top?h=(d-n.y)/(t.top-n.y):1.01*t.bottom<n.y&&(h=(n.y-d)/(n.y-t.bottom)),h=Math.round(100*h)/100,i.strokeStyle=c(i.strokeStyle).alpha(h).rgbString(),i.fillStyle=c(i.fillStyle).alpha(h).rgbString()),r.canvas.drawPoint(i,o,s,u,d))}})},{25:25,26:26,45:45}],39:[function(t,e,n){"use strict";function i(t){return void 0!==t._view.width}function a(t){var e,n,a,o,r=t._view;if(i(t)){var l=r.width/2;e=r.x-l,n=r.x+l,a=Math.min(r.y,r.base),o=Math.max(r.y,r.base)}else{var s=r.height/2;e=Math.min(r.x,r.base),n=Math.max(r.x,r.base),a=r.y-s,o=r.y+s}return{left:e,top:a,right:n,bottom:o}}var o=t(25),r=t(26);o._set("global",{elements:{rectangle:{backgroundColor:o.global.defaultColor,borderColor:o.global.defaultColor,borderSkipped:"bottom",borderWidth:0}}}),e.exports=r.extend({draw:function(){function t(t){return m[(b+t)%4]}var e,n,i,a,o,r,l,s=this._chart.ctx,u=this._view,d=u.borderWidth;if(u.horizontal?(e=u.base,n=u.x,i=u.y-u.height/2,a=u.y+u.height/2,o=n>e?1:-1,r=1,l=u.borderSkipped||"left"):(e=u.x-u.width/2,n=u.x+u.width/2,i=u.y,o=1,r=(a=u.base)>i?1:-1,l=u.borderSkipped||"bottom"),d){var c=Math.min(Math.abs(e-n),Math.abs(i-a)),h=(d=d>c?c:d)/2,f=e+("left"!==l?h*o:0),g=n+("right"!==l?-h*o:0),p=i+("top"!==l?h*r:0),v=a+("bottom"!==l?-h*r:0);f!==g&&(i=p,a=v),p!==v&&(e=f,n=g)}s.beginPath(),s.fillStyle=u.backgroundColor,s.strokeStyle=u.borderColor,s.lineWidth=d;var m=[[e,a],[e,i],[n,i],[n,a]],b=["bottom","left","top","right"].indexOf(l,0);-1===b&&(b=0);var x=t(0);s.moveTo(x[0],x[1]);for(var y=1;y<4;y++)x=t(y),s.lineTo(x[0],x[1]);s.fill(),d&&s.stroke()},height:function(){var t=this._view;return t.base-t.y},inRange:function(t,e){var n=!1;if(this._view){var i=a(this);n=t>=i.left&&t<=i.right&&e>=i.top&&e<=i.bottom}return n},inLabelRange:function(t,e){var n=this;if(!n._view)return!1;var o=a(n);return i(n)?t>=o.left&&t<=o.right:e>=o.top&&e<=o.bottom},inXRange:function(t){var e=a(this);return t>=e.left&&t<=e.right},inYRange:function(t){var e=a(this);return t>=e.top&&t<=e.bottom},getCenterPoint:function(){var t,e,n=this._view;return i(this)?(t=n.x,e=(n.y+n.base)/2):(t=(n.x+n.base)/2,e=n.y),{x:t,y:e}},getArea:function(){var t=this._view;return t.width*Math.abs(t.y-t.base)},tooltipPosition:function(){var t=this._view;return{x:t.x,y:t.y}}})},{25:25,26:26}],40:[function(t,e,n){"use strict";e.exports={},e.exports.Arc=t(36),e.exports.Line=t(37),e.exports.Point=t(38),e.exports.Rectangle=t(39)},{36:36,37:37,38:38,39:39}],41:[function(t,e,n){"use strict";var i=t(42),n=e.exports={clear:function(t){t.ctx.clearRect(0,0,t.width,t.height)},roundedRect:function(t,e,n,i,a,o){if(o){var r=Math.min(o,i/2),l=Math.min(o,a/2);t.moveTo(e+r,n),t.lineTo(e+i-r,n),t.quadraticCurveTo(e+i,n,e+i,n+l),t.lineTo(e+i,n+a-l),t.quadraticCurveTo(e+i,n+a,e+i-r,n+a),t.lineTo(e+r,n+a),t.quadraticCurveTo(e,n+a,e,n+a-l),t.lineTo(e,n+l),t.quadraticCurveTo(e,n,e+r,n)}else t.rect(e,n,i,a)},drawPoint:function(t,e,n,i,a){var o,r,l,s,u,d;if("object"!=typeof e||"[object HTMLImageElement]"!==(o=e.toString())&&"[object HTMLCanvasElement]"!==o){if(!(isNaN(n)||n<=0)){switch(e){default:t.beginPath(),t.arc(i,a,n,0,2*Math.PI),t.closePath(),t.fill();break;case"triangle":t.beginPath(),u=(r=3*n/Math.sqrt(3))*Math.sqrt(3)/2,t.moveTo(i-r/2,a+u/3),t.lineTo(i+r/2,a+u/3),t.lineTo(i,a-2*u/3),t.closePath(),t.fill();break;case"rect":d=1/Math.SQRT2*n,t.beginPath(),t.fillRect(i-d,a-d,2*d,2*d),t.strokeRect(i-d,a-d,2*d,2*d);break;case"rectRounded":var c=n/Math.SQRT2,h=i-c,f=a-c,g=Math.SQRT2*n;t.beginPath(),this.roundedRect(t,h,f,g,g,n/2),t.closePath(),t.fill();break;case"rectRot":d=1/Math.SQRT2*n,t.beginPath(),t.moveTo(i-d,a),t.lineTo(i,a+d),t.lineTo(i+d,a),t.lineTo(i,a-d),t.closePath(),t.fill();break;case"cross":t.beginPath(),t.moveTo(i,a+n),t.lineTo(i,a-n),t.moveTo(i-n,a),t.lineTo(i+n,a),t.closePath();break;case"crossRot":t.beginPath(),l=Math.cos(Math.PI/4)*n,s=Math.sin(Math.PI/4)*n,t.moveTo(i-l,a-s),t.lineTo(i+l,a+s),t.moveTo(i-l,a+s),t.lineTo(i+l,a-s),t.closePath();break;case"star":t.beginPath(),t.moveTo(i,a+n),t.lineTo(i,a-n),t.moveTo(i-n,a),t.lineTo(i+n,a),l=Math.cos(Math.PI/4)*n,s=Math.sin(Math.PI/4)*n,t.moveTo(i-l,a-s),t.lineTo(i+l,a+s),t.moveTo(i-l,a+s),t.lineTo(i+l,a-s),t.closePath();break;case"line":t.beginPath(),t.moveTo(i-n,a),t.lineTo(i+n,a),t.closePath();break;case"dash":t.beginPath(),t.moveTo(i,a),t.lineTo(i+n,a),t.closePath()}t.stroke()}}else t.drawImage(e,i-e.width/2,a-e.height/2,e.width,e.height)},clipArea:function(t,e){t.save(),t.beginPath(),t.rect(e.left,e.top,e.right-e.left,e.bottom-e.top),t.clip()},unclipArea:function(t){t.restore()},lineTo:function(t,e,n,i){if(n.steppedLine)return"after"===n.steppedLine&&!i||"after"!==n.steppedLine&&i?t.lineTo(e.x,n.y):t.lineTo(n.x,e.y),void t.lineTo(n.x,n.y);n.tension?t.bezierCurveTo(i?e.controlPointPreviousX:e.controlPointNextX,i?e.controlPointPreviousY:e.controlPointNextY,i?n.controlPointNextX:n.controlPointPreviousX,i?n.controlPointNextY:n.controlPointPreviousY,n.x,n.y):t.lineTo(n.x,n.y)}};i.clear=n.clear,i.drawRoundedRectangle=function(t){t.beginPath(),n.roundedRect.apply(n,arguments),t.closePath()}},{42:42}],42:[function(t,e,n){"use strict";var i={noop:function(){},uid:function(){var t=0;return function(){return t++}}(),isNullOrUndef:function(t){return null===t||void 0===t},isArray:Array.isArray?Array.isArray:function(t){return"[object Array]"===Object.prototype.toString.call(t)},isObject:function(t){return null!==t&&"[object Object]"===Object.prototype.toString.call(t)},valueOrDefault:function(t,e){return void 0===t?e:t},valueAtIndexOrDefault:function(t,e,n){return i.valueOrDefault(i.isArray(t)?t[e]:t,n)},callback:function(t,e,n){if(t&&"function"==typeof t.call)return t.apply(n,e)},each:function(t,e,n,a){var o,r,l;if(i.isArray(t))if(r=t.length,a)for(o=r-1;o>=0;o--)e.call(n,t[o],o);else for(o=0;o<r;o++)e.call(n,t[o],o);else if(i.isObject(t))for(r=(l=Object.keys(t)).length,o=0;o<r;o++)e.call(n,t[l[o]],l[o])},arrayEquals:function(t,e){var n,a,o,r;if(!t||!e||t.length!==e.length)return!1;for(n=0,a=t.length;n<a;++n)if(o=t[n],r=e[n],o instanceof Array&&r instanceof Array){if(!i.arrayEquals(o,r))return!1}else if(o!==r)return!1;return!0},clone:function(t){if(i.isArray(t))return t.map(i.clone);if(i.isObject(t)){for(var e={},n=Object.keys(t),a=n.length,o=0;o<a;++o)e[n[o]]=i.clone(t[n[o]]);return e}return t},_merger:function(t,e,n,a){var o=e[t],r=n[t];i.isObject(o)&&i.isObject(r)?i.merge(o,r,a):e[t]=i.clone(r)},_mergerIf:function(t,e,n){var a=e[t],o=n[t];i.isObject(a)&&i.isObject(o)?i.mergeIf(a,o):e.hasOwnProperty(t)||(e[t]=i.clone(o))},merge:function(t,e,n){var a,o,r,l,s,u=i.isArray(e)?e:[e],d=u.length;if(!i.isObject(t))return t;for(a=(n=n||{}).merger||i._merger,o=0;o<d;++o)if(e=u[o],i.isObject(e))for(s=0,l=(r=Object.keys(e)).length;s<l;++s)a(r[s],t,e,n);return t},mergeIf:function(t,e){return i.merge(t,e,{merger:i._mergerIf})}};e.exports=i,i.callCallback=i.callback,i.indexOf=function(t,e,n){return Array.prototype.indexOf.call(t,e,n)},i.getValueOrDefault=i.valueOrDefault,i.getValueAtIndexOrDefault=i.valueAtIndexOrDefault},{}],43:[function(t,e,n){"use strict";var i=t(42),a={linear:function(t){return t},easeInQuad:function(t){return t*t},easeOutQuad:function(t){return-t*(t-2)},easeInOutQuad:function(t){return(t/=.5)<1?.5*t*t:-.5*(--t*(t-2)-1)},easeInCubic:function(t){return t*t*t},easeOutCubic:function(t){return(t-=1)*t*t+1},easeInOutCubic:function(t){return(t/=.5)<1?.5*t*t*t:.5*((t-=2)*t*t+2)},easeInQuart:function(t){return t*t*t*t},easeOutQuart:function(t){return-((t-=1)*t*t*t-1)},easeInOutQuart:function(t){return(t/=.5)<1?.5*t*t*t*t:-.5*((t-=2)*t*t*t-2)},easeInQuint:function(t){return t*t*t*t*t},easeOutQuint:function(t){return(t-=1)*t*t*t*t+1},easeInOutQuint:function(t){return(t/=.5)<1?.5*t*t*t*t*t:.5*((t-=2)*t*t*t*t+2)},easeInSine:function(t){return 1-Math.cos(t*(Math.PI/2))},easeOutSine:function(t){return Math.sin(t*(Math.PI/2))},easeInOutSine:function(t){return-.5*(Math.cos(Math.PI*t)-1)},easeInExpo:function(t){return 0===t?0:Math.pow(2,10*(t-1))},easeOutExpo:function(t){return 1===t?1:1-Math.pow(2,-10*t)},easeInOutExpo:function(t){return 0===t?0:1===t?1:(t/=.5)<1?.5*Math.pow(2,10*(t-1)):.5*(2-Math.pow(2,-10*--t))},easeInCirc:function(t){return t>=1?t:-(Math.sqrt(1-t*t)-1)},easeOutCirc:function(t){return Math.sqrt(1-(t-=1)*t)},easeInOutCirc:function(t){return(t/=.5)<1?-.5*(Math.sqrt(1-t*t)-1):.5*(Math.sqrt(1-(t-=2)*t)+1)},easeInElastic:function(t){var e=1.70158,n=0,i=1;return 0===t?0:1===t?1:(n||(n=.3),i<1?(i=1,e=n/4):e=n/(2*Math.PI)*Math.asin(1/i),-i*Math.pow(2,10*(t-=1))*Math.sin((t-e)*(2*Math.PI)/n))},easeOutElastic:function(t){var e=1.70158,n=0,i=1;return 0===t?0:1===t?1:(n||(n=.3),i<1?(i=1,e=n/4):e=n/(2*Math.PI)*Math.asin(1/i),i*Math.pow(2,-10*t)*Math.sin((t-e)*(2*Math.PI)/n)+1)},easeInOutElastic:function(t){var e=1.70158,n=0,i=1;return 0===t?0:2==(t/=.5)?1:(n||(n=.45),i<1?(i=1,e=n/4):e=n/(2*Math.PI)*Math.asin(1/i),t<1?i*Math.pow(2,10*(t-=1))*Math.sin((t-e)*(2*Math.PI)/n)*-.5:i*Math.pow(2,-10*(t-=1))*Math.sin((t-e)*(2*Math.PI)/n)*.5+1)},easeInBack:function(t){var e=1.70158;return t*t*((e+1)*t-e)},easeOutBack:function(t){var e=1.70158;return(t-=1)*t*((e+1)*t+e)+1},easeInOutBack:function(t){var e=1.70158;return(t/=.5)<1?t*t*((1+(e*=1.525))*t-e)*.5:.5*((t-=2)*t*((1+(e*=1.525))*t+e)+2)},easeInBounce:function(t){return 1-a.easeOutBounce(1-t)},easeOutBounce:function(t){return t<1/2.75?7.5625*t*t:t<2/2.75?7.5625*(t-=1.5/2.75)*t+.75:t<2.5/2.75?7.5625*(t-=2.25/2.75)*t+.9375:7.5625*(t-=2.625/2.75)*t+.984375},easeInOutBounce:function(t){return t<.5?.5*a.easeInBounce(2*t):.5*a.easeOutBounce(2*t-1)+.5}};e.exports={effects:a},i.easingEffects=a},{42:42}],44:[function(t,e,n){"use strict";var i=t(42);e.exports={toLineHeight:function(t,e){var n=(""+t).match(/^(normal|(\d+(?:\.\d+)?)(px|em|%)?)$/);if(!n||"normal"===n[1])return 1.2*e;switch(t=+n[2],n[3]){case"px":return t;case"%":t/=100}return e*t},toPadding:function(t){var e,n,a,o;return i.isObject(t)?(e=+t.top||0,n=+t.right||0,a=+t.bottom||0,o=+t.left||0):e=n=a=o=+t||0,{top:e,right:n,bottom:a,left:o,height:e+a,width:o+n}},resolve:function(t,e,n){var a,o,r;for(a=0,o=t.length;a<o;++a)if(void 0!==(r=t[a])&&(void 0!==e&&"function"==typeof r&&(r=r(e)),void 0!==n&&i.isArray(r)&&(r=r[n]),void 0!==r))return r}}},{42:42}],45:[function(t,e,n){"use strict";e.exports=t(42),e.exports.easing=t(43),e.exports.canvas=t(41),e.exports.options=t(44)},{41:41,42:42,43:43,44:44}],46:[function(t,e,n){e.exports={acquireContext:function(t){return t&&t.canvas&&(t=t.canvas),t&&t.getContext("2d")||null}}},{}],47:[function(t,e,n){"use strict";function i(t,e){var n=v.getStyle(t,e),i=n&&n.match(/^(\d+)(\.\d+)?px$/);return i?Number(i[1]):void 0}function a(t,e){var n=t.style,a=t.getAttribute("height"),o=t.getAttribute("width");if(t[m]={initial:{height:a,width:o,style:{display:n.display,height:n.height,width:n.width}}},n.display=n.display||"block",null===o||""===o){var r=i(t,"width");void 0!==r&&(t.width=r)}if(null===a||""===a)if(""===t.style.height)t.height=t.width/(e.options.aspectRatio||2);else{var l=i(t,"height");void 0!==r&&(t.height=l)}return t}function o(t,e,n){t.addEventListener(e,n,M)}function r(t,e,n){t.removeEventListener(e,n,M)}function l(t,e,n,i,a){return{type:t,chart:e,native:a||null,x:void 0!==n?n:null,y:void 0!==i?i:null}}function s(t,e){var n=w[t.type]||t.type,i=v.getRelativePosition(t,e);return l(n,e,i.x,i.y,t)}function u(t,e){var n=!1,i=[];return function(){i=Array.prototype.slice.call(arguments),e=e||this,n||(n=!0,v.requestAnimFrame.call(window,function(){n=!1,t.apply(e,i)}))}}function d(t){var e=document.createElement("div"),n=b+"size-monitor",i="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;";e.style.cssText=i,e.className=n,e.innerHTML='<div class="'+n+'-expand" style="'+i+'"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="'+n+'-shrink" style="'+i+'"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div>';var a=e.childNodes[0],r=e.childNodes[1];e._reset=function(){a.scrollLeft=1e6,a.scrollTop=1e6,r.scrollLeft=1e6,r.scrollTop=1e6};var l=function(){e._reset(),t()};return o(a,"scroll",l.bind(a,"expand")),o(r,"scroll",l.bind(r,"shrink")),e}function c(t,e){var n=(t[m]||(t[m]={})).renderProxy=function(t){t.animationName===y&&e()};v.each(k,function(e){o(t,e,n)}),t.classList.add(x)}function h(t){var e=t[m]||{},n=e.renderProxy;n&&(v.each(k,function(e){r(t,e,n)}),delete e.renderProxy),t.classList.remove(x)}function f(t,e,n){var i=t[m]||(t[m]={}),a=i.resizer=d(u(function(){if(i.resizer)return e(l("resize",n))}));c(t,function(){if(i.resizer){var e=t.parentNode;e&&e!==a.parentNode&&e.insertBefore(a,e.firstChild),a._reset()}})}function g(t){var e=t[m]||{},n=e.resizer;delete e.resizer,h(t),n&&n.parentNode&&n.parentNode.removeChild(n)}function p(t,e){var n=t._style||document.createElement("style");t._style||(t._style=n,e="/* Chart.js */\n"+e,n.setAttribute("type","text/css"),document.getElementsByTagName("head")[0].appendChild(n)),n.appendChild(document.createTextNode(e))}var v=t(45),m="$chartjs",b="chartjs-",x=b+"render-monitor",y=b+"render-animation",k=["animationstart","webkitAnimationStart"],w={touchstart:"mousedown",touchmove:"mousemove",touchend:"mouseup",pointerenter:"mouseenter",pointerdown:"mousedown",pointermove:"mousemove",pointerup:"mouseup",pointerleave:"mouseout",pointerout:"mouseout"},M=!!function(){var t=!1;try{var e=Object.defineProperty({},"passive",{get:function(){t=!0}});window.addEventListener("e",null,e)}catch(t){}return t}()&&{passive:!0};e.exports={_enabled:"undefined"!=typeof window&&"undefined"!=typeof document,initialize:function(){var t="from{opacity:0.99}to{opacity:1}";p(this,"@-webkit-keyframes "+y+"{"+t+"}@keyframes "+y+"{"+t+"}."+x+"{-webkit-animation:"+y+" 0.001s;animation:"+y+" 0.001s;}")},acquireContext:function(t,e){"string"==typeof t?t=document.getElementById(t):t.length&&(t=t[0]),t&&t.canvas&&(t=t.canvas);var n=t&&t.getContext&&t.getContext("2d");return n&&n.canvas===t?(a(t,e),n):null},releaseContext:function(t){var e=t.canvas;if(e[m]){var n=e[m].initial;["height","width"].forEach(function(t){var i=n[t];v.isNullOrUndef(i)?e.removeAttribute(t):e.setAttribute(t,i)}),v.each(n.style||{},function(t,n){e.style[n]=t}),e.width=e.width,delete e[m]}},addEventListener:function(t,e,n){var i=t.canvas;if("resize"!==e){var a=n[m]||(n[m]={});o(i,e,(a.proxies||(a.proxies={}))[t.id+"_"+e]=function(e){n(s(e,t))})}else f(i,n,t)},removeEventListener:function(t,e,n){var i=t.canvas;if("resize"!==e){var a=((n[m]||{}).proxies||{})[t.id+"_"+e];a&&r(i,e,a)}else g(i)}},v.addEvent=o,v.removeEvent=r},{45:45}],48:[function(t,e,n){"use strict";var i=t(45),a=t(46),o=t(47),r=o._enabled?o:a;e.exports=i.extend({initialize:function(){},acquireContext:function(){},releaseContext:function(){},addEventListener:function(){},removeEventListener:function(){}},r)},{45:45,46:46,47:47}],49:[function(t,e,n){"use strict";var i=t(25),a=t(40),o=t(45);i._set("global",{plugins:{filler:{propagate:!0}}}),e.exports=function(){function t(t,e,n){var i,a=t._model||{},o=a.fill;if(void 0===o&&(o=!!a.backgroundColor),!1===o||null===o)return!1;if(!0===o)return"origin";if(i=parseFloat(o,10),isFinite(i)&&Math.floor(i)===i)return"-"!==o[0]&&"+"!==o[0]||(i=e+i),!(i===e||i<0||i>=n)&&i;switch(o){case"bottom":return"start";case"top":return"end";case"zero":return"origin";case"origin":case"start":case"end":return o;default:return!1}}function e(t){var e,n=t.el._model||{},i=t.el._scale||{},a=t.fill,o=null;if(isFinite(a))return null;if("start"===a?o=void 0===n.scaleBottom?i.bottom:n.scaleBottom:"end"===a?o=void 0===n.scaleTop?i.top:n.scaleTop:void 0!==n.scaleZero?o=n.scaleZero:i.getBasePosition?o=i.getBasePosition():i.getBasePixel&&(o=i.getBasePixel()),void 0!==o&&null!==o){if(void 0!==o.x&&void 0!==o.y)return o;if("number"==typeof o&&isFinite(o))return e=i.isHorizontal(),{x:e?o:null,y:e?null:o}}return null}function n(t,e,n){var i,a=t[e].fill,o=[e];if(!n)return a;for(;!1!==a&&-1===o.indexOf(a);){if(!isFinite(a))return a;if(!(i=t[a]))return!1;if(i.visible)return a;o.push(a),a=i.fill}return!1}function r(t){var e=t.fill,n="dataset";return!1===e?null:(isFinite(e)||(n="boundary"),d[n](t))}function l(t){return t&&!t.skip}function s(t,e,n,i,a){var r;if(i&&a){for(t.moveTo(e[0].x,e[0].y),r=1;r<i;++r)o.canvas.lineTo(t,e[r-1],e[r]);for(t.lineTo(n[a-1].x,n[a-1].y),r=a-1;r>0;--r)o.canvas.lineTo(t,n[r],n[r-1],!0)}}function u(t,e,n,i,a,o){var r,u,d,c,h,f,g,p=e.length,v=i.spanGaps,m=[],b=[],x=0,y=0;for(t.beginPath(),r=0,u=p+!!o;r<u;++r)h=n(c=e[d=r%p]._view,d,i),f=l(c),g=l(h),f&&g?(x=m.push(c),y=b.push(h)):x&&y&&(v?(f&&m.push(c),g&&b.push(h)):(s(t,m,b,x,y),x=y=0,m=[],b=[]));s(t,m,b,x,y),t.closePath(),t.fillStyle=a,t.fill()}var d={dataset:function(t){var e=t.fill,n=t.chart,i=n.getDatasetMeta(e),a=i&&n.isDatasetVisible(e)&&i.dataset._children||[],o=a.length||0;return o?function(t,e){return e<o&&a[e]._view||null}:null},boundary:function(t){var e=t.boundary,n=e?e.x:null,i=e?e.y:null;return function(t){return{x:null===n?t.x:n,y:null===i?t.y:i}}}};return{id:"filler",afterDatasetsUpdate:function(i,o){var l,s,u,d,c=(i.data.datasets||[]).length,h=o.propagate,f=[];for(s=0;s<c;++s)d=null,(u=(l=i.getDatasetMeta(s)).dataset)&&u._model&&u instanceof a.Line&&(d={visible:i.isDatasetVisible(s),fill:t(u,s,c),chart:i,el:u}),l.$filler=d,f.push(d);for(s=0;s<c;++s)(d=f[s])&&(d.fill=n(f,s,h),d.boundary=e(d),d.mapper=r(d))},beforeDatasetDraw:function(t,e){var n=e.meta.$filler;if(n){var a=t.ctx,r=n.el,l=r._view,s=r._children||[],d=n.mapper,c=l.backgroundColor||i.global.defaultColor;d&&c&&s.length&&(o.canvas.clipArea(a,t.chartArea),u(a,s,d,l,c,r._loop),o.canvas.unclipArea(a))}}}}},{25:25,40:40,45:45}],50:[function(t,e,n){"use strict";var i=t(25),a=t(26),o=t(45);i._set("global",{legend:{display:!0,position:"top",fullWidth:!0,reverse:!1,weight:1e3,onClick:function(t,e){var n=e.datasetIndex,i=this.chart,a=i.getDatasetMeta(n);a.hidden=null===a.hidden?!i.data.datasets[n].hidden:null,i.update()},onHover:null,labels:{boxWidth:40,padding:10,generateLabels:function(t){var e=t.data;return o.isArray(e.datasets)?e.datasets.map(function(e,n){return{text:e.label,fillStyle:o.isArray(e.backgroundColor)?e.backgroundColor[0]:e.backgroundColor,hidden:!t.isDatasetVisible(n),lineCap:e.borderCapStyle,lineDash:e.borderDash,lineDashOffset:e.borderDashOffset,lineJoin:e.borderJoinStyle,lineWidth:e.borderWidth,strokeStyle:e.borderColor,pointStyle:e.pointStyle,datasetIndex:n}},this):[]}}},legendCallback:function(t){var e=[];e.push('<ul class="'+t.id+'-legend">');for(var n=0;n<t.data.datasets.length;n++)e.push('<li><span style="background-color:'+t.data.datasets[n].backgroundColor+'"></span>'),t.data.datasets[n].label&&e.push(t.data.datasets[n].label),e.push("</li>");return e.push("</ul>"),e.join("")}}),e.exports=function(t){function e(t,e){return t.usePointStyle?e*Math.SQRT2:t.boxWidth}function n(e,n){var i=new t.Legend({ctx:e.ctx,options:n,chart:e});r.configure(e,i,n),r.addBox(e,i),e.legend=i}var r=t.layoutService,l=o.noop;return t.Legend=a.extend({initialize:function(t){o.extend(this,t),this.legendHitBoxes=[],this.doughnutMode=!1},beforeUpdate:l,update:function(t,e,n){var i=this;return i.beforeUpdate(),i.maxWidth=t,i.maxHeight=e,i.margins=n,i.beforeSetDimensions(),i.setDimensions(),i.afterSetDimensions(),i.beforeBuildLabels(),i.buildLabels(),i.afterBuildLabels(),i.beforeFit(),i.fit(),i.afterFit(),i.afterUpdate(),i.minSize},afterUpdate:l,beforeSetDimensions:l,setDimensions:function(){var t=this;t.isHorizontal()?(t.width=t.maxWidth,t.left=0,t.right=t.width):(t.height=t.maxHeight,t.top=0,t.bottom=t.height),t.paddingLeft=0,t.paddingTop=0,t.paddingRight=0,t.paddingBottom=0,t.minSize={width:0,height:0}},afterSetDimensions:l,beforeBuildLabels:l,buildLabels:function(){var t=this,e=t.options.labels||{},n=o.callback(e.generateLabels,[t.chart],t)||[];e.filter&&(n=n.filter(function(n){return e.filter(n,t.chart.data)})),t.options.reverse&&n.reverse(),t.legendItems=n},afterBuildLabels:l,beforeFit:l,fit:function(){var t=this,n=t.options,a=n.labels,r=n.display,l=t.ctx,s=i.global,u=o.valueOrDefault,d=u(a.fontSize,s.defaultFontSize),c=u(a.fontStyle,s.defaultFontStyle),h=u(a.fontFamily,s.defaultFontFamily),f=o.fontString(d,c,h),g=t.legendHitBoxes=[],p=t.minSize,v=t.isHorizontal();if(v?(p.width=t.maxWidth,p.height=r?10:0):(p.width=r?10:0,p.height=t.maxHeight),r)if(l.font=f,v){var m=t.lineWidths=[0],b=t.legendItems.length?d+a.padding:0;l.textAlign="left",l.textBaseline="top",o.each(t.legendItems,function(n,i){var o=e(a,d)+d/2+l.measureText(n.text).width;m[m.length-1]+o+a.padding>=t.width&&(b+=d+a.padding,m[m.length]=t.left),g[i]={left:0,top:0,width:o,height:d},m[m.length-1]+=o+a.padding}),p.height+=b}else{var x=a.padding,y=t.columnWidths=[],k=a.padding,w=0,M=0,S=d+x;o.each(t.legendItems,function(t,n){var i=e(a,d)+d/2+l.measureText(t.text).width;M+S>p.height&&(k+=w+a.padding,y.push(w),w=0,M=0),w=Math.max(w,i),M+=S,g[n]={left:0,top:0,width:i,height:d}}),k+=w,y.push(w),p.width+=k}t.width=p.width,t.height=p.height},afterFit:l,isHorizontal:function(){return"top"===this.options.position||"bottom"===this.options.position},draw:function(){var t=this,n=t.options,a=n.labels,r=i.global,l=r.elements.line,s=t.width,u=t.lineWidths;if(n.display){var d,c=t.ctx,h=o.valueOrDefault,f=h(a.fontColor,r.defaultFontColor),g=h(a.fontSize,r.defaultFontSize),p=h(a.fontStyle,r.defaultFontStyle),v=h(a.fontFamily,r.defaultFontFamily),m=o.fontString(g,p,v);c.textAlign="left",c.textBaseline="middle",c.lineWidth=.5,c.strokeStyle=f,c.fillStyle=f,c.font=m;var b=e(a,g),x=t.legendHitBoxes,y=function(t,e,i){if(!(isNaN(b)||b<=0)){c.save(),c.fillStyle=h(i.fillStyle,r.defaultColor),c.lineCap=h(i.lineCap,l.borderCapStyle),c.lineDashOffset=h(i.lineDashOffset,l.borderDashOffset),c.lineJoin=h(i.lineJoin,l.borderJoinStyle),c.lineWidth=h(i.lineWidth,l.borderWidth),c.strokeStyle=h(i.strokeStyle,r.defaultColor);var a=0===h(i.lineWidth,l.borderWidth);if(c.setLineDash&&c.setLineDash(h(i.lineDash,l.borderDash)),n.labels&&n.labels.usePointStyle){var s=g*Math.SQRT2/2,u=s/Math.SQRT2,d=t+u,f=e+u;o.canvas.drawPoint(c,i.pointStyle,s,d,f)}else a||c.strokeRect(t,e,b,g),c.fillRect(t,e,b,g);c.restore()}},k=function(t,e,n,i){var a=g/2,o=b+a+t,r=e+a;c.fillText(n.text,o,r),n.hidden&&(c.beginPath(),c.lineWidth=2,c.moveTo(o,r),c.lineTo(o+i,r),c.stroke())},w=t.isHorizontal();d=w?{x:t.left+(s-u[0])/2,y:t.top+a.padding,line:0}:{x:t.left+a.padding,y:t.top+a.padding,line:0};var M=g+a.padding;o.each(t.legendItems,function(e,n){var i=c.measureText(e.text).width,o=b+g/2+i,r=d.x,l=d.y;w?r+o>=s&&(l=d.y+=M,d.line++,r=d.x=t.left+(s-u[d.line])/2):l+M>t.bottom&&(r=d.x=r+t.columnWidths[d.line]+a.padding,l=d.y=t.top+a.padding,d.line++),y(r,l,e),x[n].left=r,x[n].top=l,k(r,l,e,i),w?d.x+=o+a.padding:d.y+=M})}},handleEvent:function(t){var e=this,n=e.options,i="mouseup"===t.type?"click":t.type,a=!1;if("mousemove"===i){if(!n.onHover)return}else{if("click"!==i)return;if(!n.onClick)return}var o=t.x,r=t.y;if(o>=e.left&&o<=e.right&&r>=e.top&&r<=e.bottom)for(var l=e.legendHitBoxes,s=0;s<l.length;++s){var u=l[s];if(o>=u.left&&o<=u.left+u.width&&r>=u.top&&r<=u.top+u.height){if("click"===i){n.onClick.call(e,t.native,e.legendItems[s]),a=!0;break}if("mousemove"===i){n.onHover.call(e,t.native,e.legendItems[s]),a=!0;break}}}return a}}),{id:"legend",beforeInit:function(t){var e=t.options.legend;e&&n(t,e)},beforeUpdate:function(t){var e=t.options.legend,a=t.legend;e?(o.mergeIf(e,i.global.legend),a?(r.configure(t,a,e),a.options=e):n(t,e)):a&&(r.removeBox(t,a),delete t.legend)},afterEvent:function(t,e){var n=t.legend;n&&n.handleEvent(e)}}}},{25:25,26:26,45:45}],51:[function(t,e,n){"use strict";var i=t(25),a=t(26),o=t(45);i._set("global",{title:{display:!1,fontStyle:"bold",fullWidth:!0,lineHeight:1.2,padding:10,position:"top",text:"",weight:2e3}}),e.exports=function(t){function e(e,i){var a=new t.Title({ctx:e.ctx,options:i,chart:e});n.configure(e,a,i),n.addBox(e,a),e.titleBlock=a}var n=t.layoutService,r=o.noop;return t.Title=a.extend({initialize:function(t){var e=this;o.extend(e,t),e.legendHitBoxes=[]},beforeUpdate:r,update:function(t,e,n){var i=this;return i.beforeUpdate(),i.maxWidth=t,i.maxHeight=e,i.margins=n,i.beforeSetDimensions(),i.setDimensions(),i.afterSetDimensions(),i.beforeBuildLabels(),i.buildLabels(),i.afterBuildLabels(),i.beforeFit(),i.fit(),i.afterFit(),i.afterUpdate(),i.minSize},afterUpdate:r,beforeSetDimensions:r,setDimensions:function(){var t=this;t.isHorizontal()?(t.width=t.maxWidth,t.left=0,t.right=t.width):(t.height=t.maxHeight,t.top=0,t.bottom=t.height),t.paddingLeft=0,t.paddingTop=0,t.paddingRight=0,t.paddingBottom=0,t.minSize={width:0,height:0}},afterSetDimensions:r,beforeBuildLabels:r,buildLabels:r,afterBuildLabels:r,beforeFit:r,fit:function(){var t=this,e=o.valueOrDefault,n=t.options,a=n.display,r=e(n.fontSize,i.global.defaultFontSize),l=t.minSize,s=o.isArray(n.text)?n.text.length:1,u=o.options.toLineHeight(n.lineHeight,r),d=a?s*u+2*n.padding:0;t.isHorizontal()?(l.width=t.maxWidth,l.height=d):(l.width=d,l.height=t.maxHeight),t.width=l.width,t.height=l.height},afterFit:r,isHorizontal:function(){var t=this.options.position;return"top"===t||"bottom"===t},draw:function(){var t=this,e=t.ctx,n=o.valueOrDefault,a=t.options,r=i.global;if(a.display){var l,s,u,d=n(a.fontSize,r.defaultFontSize),c=n(a.fontStyle,r.defaultFontStyle),h=n(a.fontFamily,r.defaultFontFamily),f=o.fontString(d,c,h),g=o.options.toLineHeight(a.lineHeight,d),p=g/2+a.padding,v=0,m=t.top,b=t.left,x=t.bottom,y=t.right;e.fillStyle=n(a.fontColor,r.defaultFontColor),e.font=f,t.isHorizontal()?(s=b+(y-b)/2,u=m+p,l=y-b):(s="left"===a.position?b+p:y-p,u=m+(x-m)/2,l=x-m,v=Math.PI*("left"===a.position?-.5:.5)),e.save(),e.translate(s,u),e.rotate(v),e.textAlign="center",e.textBaseline="middle";var k=a.text;if(o.isArray(k))for(var w=0,M=0;M<k.length;++M)e.fillText(k[M],0,w,l),w+=g;else e.fillText(k,0,0,l);e.restore()}}}),{id:"title",beforeInit:function(t){var n=t.options.title;n&&e(t,n)},beforeUpdate:function(a){var r=a.options.title,l=a.titleBlock;r?(o.mergeIf(r,i.global.title),l?(n.configure(a,l,r),l.options=r):e(a,r)):l&&(t.layoutService.removeBox(a,l),delete a.titleBlock)}}}},{25:25,26:26,45:45}],52:[function(t,e,n){"use strict";e.exports=function(t){var e={position:"bottom"},n=t.Scale.extend({getLabels:function(){var t=this.chart.data;return this.options.labels||(this.isHorizontal()?t.xLabels:t.yLabels)||t.labels},determineDataLimits:function(){var t=this,e=t.getLabels();t.minIndex=0,t.maxIndex=e.length-1;var n;void 0!==t.options.ticks.min&&(n=e.indexOf(t.options.ticks.min),t.minIndex=-1!==n?n:t.minIndex),void 0!==t.options.ticks.max&&(n=e.indexOf(t.options.ticks.max),t.maxIndex=-1!==n?n:t.maxIndex),t.min=e[t.minIndex],t.max=e[t.maxIndex]},buildTicks:function(){var t=this,e=t.getLabels();t.ticks=0===t.minIndex&&t.maxIndex===e.length-1?e:e.slice(t.minIndex,t.maxIndex+1)},getLabelForIndex:function(t,e){var n=this,i=n.chart.data,a=n.isHorizontal();return i.yLabels&&!a?n.getRightValue(i.datasets[e].data[t]):n.ticks[t-n.minIndex]},getPixelForValue:function(t,e){var n,i=this,a=i.options.offset,o=Math.max(i.maxIndex+1-i.minIndex-(a?0:1),1);if(void 0!==t&&null!==t&&(n=i.isHorizontal()?t.x:t.y),void 0!==n||void 0!==t&&isNaN(e)){var r=i.getLabels();t=n||t;var l=r.indexOf(t);e=-1!==l?l:e}if(i.isHorizontal()){var s=i.width/o,u=s*(e-i.minIndex);return a&&(u+=s/2),i.left+Math.round(u)}var d=i.height/o,c=d*(e-i.minIndex);return a&&(c+=d/2),i.top+Math.round(c)},getPixelForTick:function(t){return this.getPixelForValue(this.ticks[t],t+this.minIndex,null)},getValueForPixel:function(t){var e=this,n=e.options.offset,i=Math.max(e._ticks.length-(n?0:1),1),a=e.isHorizontal(),o=(a?e.width:e.height)/i;return t-=a?e.left:e.top,n&&(t-=o/2),(t<=0?0:Math.round(t/o))+e.minIndex},getBasePixel:function(){return this.bottom}});t.scaleService.registerScaleType("category",n,e)}},{}],53:[function(t,e,n){"use strict";var i=t(25),a=t(45),o=t(34);e.exports=function(t){var e={position:"left",ticks:{callback:o.formatters.linear}},n=t.LinearScaleBase.extend({determineDataLimits:function(){function t(t){return r?t.xAxisID===e.id:t.yAxisID===e.id}var e=this,n=e.options,i=e.chart,o=i.data.datasets,r=e.isHorizontal();e.min=null,e.max=null;var l=n.stacked;if(void 0===l&&a.each(o,function(e,n){if(!l){var a=i.getDatasetMeta(n);i.isDatasetVisible(n)&&t(a)&&void 0!==a.stack&&(l=!0)}}),n.stacked||l){var s={};a.each(o,function(o,r){var l=i.getDatasetMeta(r),u=[l.type,void 0===n.stacked&&void 0===l.stack?r:"",l.stack].join(".");void 0===s[u]&&(s[u]={positiveValues:[],negativeValues:[]});var d=s[u].positiveValues,c=s[u].negativeValues;i.isDatasetVisible(r)&&t(l)&&a.each(o.data,function(t,i){var a=+e.getRightValue(t);isNaN(a)||l.data[i].hidden||(d[i]=d[i]||0,c[i]=c[i]||0,n.relativePoints?d[i]=100:a<0?c[i]+=a:d[i]+=a)})}),a.each(s,function(t){var n=t.positiveValues.concat(t.negativeValues),i=a.min(n),o=a.max(n);e.min=null===e.min?i:Math.min(e.min,i),e.max=null===e.max?o:Math.max(e.max,o)})}else a.each(o,function(n,o){var r=i.getDatasetMeta(o);i.isDatasetVisible(o)&&t(r)&&a.each(n.data,function(t,n){var i=+e.getRightValue(t);isNaN(i)||r.data[n].hidden||(null===e.min?e.min=i:i<e.min&&(e.min=i),null===e.max?e.max=i:i>e.max&&(e.max=i))})});e.min=isFinite(e.min)&&!isNaN(e.min)?e.min:0,e.max=isFinite(e.max)&&!isNaN(e.max)?e.max:1,this.handleTickRangeOptions()},getTickLimit:function(){var t,e=this,n=e.options.ticks;if(e.isHorizontal())t=Math.min(n.maxTicksLimit?n.maxTicksLimit:11,Math.ceil(e.width/50));else{var o=a.valueOrDefault(n.fontSize,i.global.defaultFontSize);t=Math.min(n.maxTicksLimit?n.maxTicksLimit:11,Math.ceil(e.height/(2*o)))}return t},handleDirectionalChanges:function(){this.isHorizontal()||this.ticks.reverse()},getLabelForIndex:function(t,e){return+this.getRightValue(this.chart.data.datasets[e].data[t])},getPixelForValue:function(t){var e,n=this,i=n.start,a=+n.getRightValue(t),o=n.end-i;return n.isHorizontal()?(e=n.left+n.width/o*(a-i),Math.round(e)):(e=n.bottom-n.height/o*(a-i),Math.round(e))},getValueForPixel:function(t){var e=this,n=e.isHorizontal(),i=n?e.width:e.height,a=(n?t-e.left:e.bottom-t)/i;return e.start+(e.end-e.start)*a},getPixelForTick:function(t){return this.getPixelForValue(this.ticksAsNumbers[t])}});t.scaleService.registerScaleType("linear",n,e)}},{25:25,34:34,45:45}],54:[function(t,e,n){"use strict";var i=t(45),a=t(34);e.exports=function(t){var e=i.noop;t.LinearScaleBase=t.Scale.extend({getRightValue:function(e){return"string"==typeof e?+e:t.Scale.prototype.getRightValue.call(this,e)},handleTickRangeOptions:function(){var t=this,e=t.options.ticks;if(e.beginAtZero){var n=i.sign(t.min),a=i.sign(t.max);n<0&&a<0?t.max=0:n>0&&a>0&&(t.min=0)}var o=void 0!==e.min||void 0!==e.suggestedMin,r=void 0!==e.max||void 0!==e.suggestedMax;void 0!==e.min?t.min=e.min:void 0!==e.suggestedMin&&(null===t.min?t.min=e.suggestedMin:t.min=Math.min(t.min,e.suggestedMin)),void 0!==e.max?t.max=e.max:void 0!==e.suggestedMax&&(null===t.max?t.max=e.suggestedMax:t.max=Math.max(t.max,e.suggestedMax)),o!==r&&t.min>=t.max&&(o?t.max=t.min+1:t.min=t.max-1),t.min===t.max&&(t.max++,e.beginAtZero||t.min--)},getTickLimit:e,handleDirectionalChanges:e,buildTicks:function(){var t=this,e=t.options.ticks,n=t.getTickLimit(),o={maxTicks:n=Math.max(2,n),min:e.min,max:e.max,stepSize:i.valueOrDefault(e.fixedStepSize,e.stepSize)},r=t.ticks=a.generators.linear(o,t);t.handleDirectionalChanges(),t.max=i.max(r),t.min=i.min(r),e.reverse?(r.reverse(),t.start=t.max,t.end=t.min):(t.start=t.min,t.end=t.max)},convertTicksToLabels:function(){var e=this;e.ticksAsNumbers=e.ticks.slice(),e.zeroLineIndex=e.ticks.indexOf(0),t.Scale.prototype.convertTicksToLabels.call(e)}})}},{34:34,45:45}],55:[function(t,e,n){"use strict";var i=t(45),a=t(34);e.exports=function(t){var e={position:"left",ticks:{callback:a.formatters.logarithmic}},n=t.Scale.extend({determineDataLimits:function(){function t(t){return s?t.xAxisID===e.id:t.yAxisID===e.id}var e=this,n=e.options,a=n.ticks,o=e.chart,r=o.data.datasets,l=i.valueOrDefault,s=e.isHorizontal();e.min=null,e.max=null,e.minNotZero=null;var u=n.stacked;if(void 0===u&&i.each(r,function(e,n){if(!u){var i=o.getDatasetMeta(n);o.isDatasetVisible(n)&&t(i)&&void 0!==i.stack&&(u=!0)}}),n.stacked||u){var d={};i.each(r,function(a,r){var l=o.getDatasetMeta(r),s=[l.type,void 0===n.stacked&&void 0===l.stack?r:"",l.stack].join(".");o.isDatasetVisible(r)&&t(l)&&(void 0===d[s]&&(d[s]=[]),i.each(a.data,function(t,i){var a=d[s],o=+e.getRightValue(t);isNaN(o)||l.data[i].hidden||(a[i]=a[i]||0,n.relativePoints?a[i]=100:a[i]+=o)}))}),i.each(d,function(t){var n=i.min(t),a=i.max(t);e.min=null===e.min?n:Math.min(e.min,n),e.max=null===e.max?a:Math.max(e.max,a)})}else i.each(r,function(n,a){var r=o.getDatasetMeta(a);o.isDatasetVisible(a)&&t(r)&&i.each(n.data,function(t,n){var i=+e.getRightValue(t);isNaN(i)||r.data[n].hidden||(null===e.min?e.min=i:i<e.min&&(e.min=i),null===e.max?e.max=i:i>e.max&&(e.max=i),0!==i&&(null===e.minNotZero||i<e.minNotZero)&&(e.minNotZero=i))})});e.min=l(a.min,e.min),e.max=l(a.max,e.max),e.min===e.max&&(0!==e.min&&null!==e.min?(e.min=Math.pow(10,Math.floor(i.log10(e.min))-1),e.max=Math.pow(10,Math.floor(i.log10(e.max))+1)):(e.min=1,e.max=10))},buildTicks:function(){var t=this,e=t.options.ticks,n={min:e.min,max:e.max},o=t.ticks=a.generators.logarithmic(n,t);t.isHorizontal()||o.reverse(),t.max=i.max(o),t.min=i.min(o),e.reverse?(o.reverse(),t.start=t.max,t.end=t.min):(t.start=t.min,t.end=t.max)},convertTicksToLabels:function(){this.tickValues=this.ticks.slice(),t.Scale.prototype.convertTicksToLabels.call(this)},getLabelForIndex:function(t,e){return+this.getRightValue(this.chart.data.datasets[e].data[t])},getPixelForTick:function(t){return this.getPixelForValue(this.tickValues[t])},getPixelForValue:function(t){var e,n,a,o=this,r=o.start,l=+o.getRightValue(t),s=o.options.ticks;return o.isHorizontal()?(a=i.log10(o.end)-i.log10(r),0===l?n=o.left:(e=o.width,n=o.left+e/a*(i.log10(l)-i.log10(r)))):(e=o.height,0!==r||s.reverse?0===o.end&&s.reverse?(a=i.log10(o.start)-i.log10(o.minNotZero),n=l===o.end?o.top:l===o.minNotZero?o.top+.02*e:o.top+.02*e+.98*e/a*(i.log10(l)-i.log10(o.minNotZero))):0===l?n=s.reverse?o.top:o.bottom:(a=i.log10(o.end)-i.log10(r),e=o.height,n=o.bottom-e/a*(i.log10(l)-i.log10(r))):(a=i.log10(o.end)-i.log10(o.minNotZero),n=l===r?o.bottom:l===o.minNotZero?o.bottom-.02*e:o.bottom-.02*e-.98*e/a*(i.log10(l)-i.log10(o.minNotZero)))),n},getValueForPixel:function(t){var e,n,a=this,o=i.log10(a.end)-i.log10(a.start);return a.isHorizontal()?(n=a.width,e=a.start*Math.pow(10,(t-a.left)*o/n)):(n=a.height,e=Math.pow(10,(a.bottom-t)*o/n)/a.start),e}});t.scaleService.registerScaleType("logarithmic",n,e)}},{34:34,45:45}],56:[function(t,e,n){"use strict";var i=t(25),a=t(45),o=t(34);e.exports=function(t){function e(t){var e=t.options;return e.angleLines.display||e.pointLabels.display?t.chart.data.labels.length:0}function n(t){var e=t.options.pointLabels,n=a.valueOrDefault(e.fontSize,v.defaultFontSize),i=a.valueOrDefault(e.fontStyle,v.defaultFontStyle),o=a.valueOrDefault(e.fontFamily,v.defaultFontFamily);return{size:n,style:i,family:o,font:a.fontString(n,i,o)}}function r(t,e,n){return a.isArray(n)?{w:a.longestText(t,t.font,n),h:n.length*e+1.5*(n.length-1)*e}:{w:t.measureText(n).width,h:e}}function l(t,e,n,i,a){return t===i||t===a?{start:e-n/2,end:e+n/2}:t<i||t>a?{start:e-n-5,end:e}:{start:e,end:e+n+5}}function s(t){var i,o,s,u=n(t),d=Math.min(t.height/2,t.width/2),c={r:t.width,l:0,t:t.height,b:0},h={};t.ctx.font=u.font,t._pointLabelSizes=[];var f=e(t);for(i=0;i<f;i++){s=t.getPointPosition(i,d),o=r(t.ctx,u.size,t.pointLabels[i]||""),t._pointLabelSizes[i]=o;var g=t.getIndexAngle(i),p=a.toDegrees(g)%360,v=l(p,s.x,o.w,0,180),m=l(p,s.y,o.h,90,270);v.start<c.l&&(c.l=v.start,h.l=g),v.end>c.r&&(c.r=v.end,h.r=g),m.start<c.t&&(c.t=m.start,h.t=g),m.end>c.b&&(c.b=m.end,h.b=g)}t.setReductions(d,c,h)}function u(t){var e=Math.min(t.height/2,t.width/2);t.drawingArea=Math.round(e),t.setCenterPoint(0,0,0,0)}function d(t){return 0===t||180===t?"center":t<180?"left":"right"}function c(t,e,n,i){if(a.isArray(e))for(var o=n.y,r=1.5*i,l=0;l<e.length;++l)t.fillText(e[l],n.x,o),o+=r;else t.fillText(e,n.x,n.y)}function h(t,e,n){90===t||270===t?n.y-=e.h/2:(t>270||t<90)&&(n.y-=e.h)}function f(t){var i=t.ctx,o=a.valueOrDefault,r=t.options,l=r.angleLines,s=r.pointLabels;i.lineWidth=l.lineWidth,i.strokeStyle=l.color;var u=t.getDistanceFromCenterForValue(r.ticks.reverse?t.min:t.max),f=n(t);i.textBaseline="top";for(var g=e(t)-1;g>=0;g--){if(l.display){var p=t.getPointPosition(g,u);i.beginPath(),i.moveTo(t.xCenter,t.yCenter),i.lineTo(p.x,p.y),i.stroke(),i.closePath()}if(s.display){var m=t.getPointPosition(g,u+5),b=o(s.fontColor,v.defaultFontColor);i.font=f.font,i.fillStyle=b;var x=t.getIndexAngle(g),y=a.toDegrees(x);i.textAlign=d(y),h(y,t._pointLabelSizes[g],m),c(i,t.pointLabels[g]||"",m,f.size)}}}function g(t,n,i,o){var r=t.ctx;if(r.strokeStyle=a.valueAtIndexOrDefault(n.color,o-1),r.lineWidth=a.valueAtIndexOrDefault(n.lineWidth,o-1),t.options.gridLines.circular)r.beginPath(),r.arc(t.xCenter,t.yCenter,i,0,2*Math.PI),r.closePath(),r.stroke();else{var l=e(t);if(0===l)return;r.beginPath();var s=t.getPointPosition(0,i);r.moveTo(s.x,s.y);for(var u=1;u<l;u++)s=t.getPointPosition(u,i),r.lineTo(s.x,s.y);r.closePath(),r.stroke()}}function p(t){return a.isNumber(t)?t:0}var v=i.global,m={display:!0,animate:!0,position:"chartArea",angleLines:{display:!0,color:"rgba(0, 0, 0, 0.1)",lineWidth:1},gridLines:{circular:!1},ticks:{showLabelBackdrop:!0,backdropColor:"rgba(255,255,255,0.75)",backdropPaddingY:2,backdropPaddingX:2,callback:o.formatters.linear},pointLabels:{display:!0,fontSize:10,callback:function(t){return t}}},b=t.LinearScaleBase.extend({setDimensions:function(){var t=this,e=t.options,n=e.ticks;t.width=t.maxWidth,t.height=t.maxHeight,t.xCenter=Math.round(t.width/2),t.yCenter=Math.round(t.height/2);var i=a.min([t.height,t.width]),o=a.valueOrDefault(n.fontSize,v.defaultFontSize);t.drawingArea=e.display?i/2-(o/2+n.backdropPaddingY):i/2},determineDataLimits:function(){var t=this,e=t.chart,n=Number.POSITIVE_INFINITY,i=Number.NEGATIVE_INFINITY;a.each(e.data.datasets,function(o,r){if(e.isDatasetVisible(r)){var l=e.getDatasetMeta(r);a.each(o.data,function(e,a){var o=+t.getRightValue(e);isNaN(o)||l.data[a].hidden||(n=Math.min(o,n),i=Math.max(o,i))})}}),t.min=n===Number.POSITIVE_INFINITY?0:n,t.max=i===Number.NEGATIVE_INFINITY?0:i,t.handleTickRangeOptions()},getTickLimit:function(){var t=this.options.ticks,e=a.valueOrDefault(t.fontSize,v.defaultFontSize);return Math.min(t.maxTicksLimit?t.maxTicksLimit:11,Math.ceil(this.drawingArea/(1.5*e)))},convertTicksToLabels:function(){var e=this;t.LinearScaleBase.prototype.convertTicksToLabels.call(e),e.pointLabels=e.chart.data.labels.map(e.options.pointLabels.callback,e)},getLabelForIndex:function(t,e){return+this.getRightValue(this.chart.data.datasets[e].data[t])},fit:function(){this.options.pointLabels.display?s(this):u(this)},setReductions:function(t,e,n){var i=this,a=e.l/Math.sin(n.l),o=Math.max(e.r-i.width,0)/Math.sin(n.r),r=-e.t/Math.cos(n.t),l=-Math.max(e.b-i.height,0)/Math.cos(n.b);a=p(a),o=p(o),r=p(r),l=p(l),i.drawingArea=Math.min(Math.round(t-(a+o)/2),Math.round(t-(r+l)/2)),i.setCenterPoint(a,o,r,l)},setCenterPoint:function(t,e,n,i){var a=this,o=a.width-e-a.drawingArea,r=t+a.drawingArea,l=n+a.drawingArea,s=a.height-i-a.drawingArea;a.xCenter=Math.round((r+o)/2+a.left),a.yCenter=Math.round((l+s)/2+a.top)},getIndexAngle:function(t){return t*(2*Math.PI/e(this))+(this.chart.options&&this.chart.options.startAngle?this.chart.options.startAngle:0)*Math.PI*2/360},getDistanceFromCenterForValue:function(t){var e=this;if(null===t)return 0;var n=e.drawingArea/(e.max-e.min);return e.options.ticks.reverse?(e.max-t)*n:(t-e.min)*n},getPointPosition:function(t,e){var n=this,i=n.getIndexAngle(t)-Math.PI/2;return{x:Math.round(Math.cos(i)*e)+n.xCenter,y:Math.round(Math.sin(i)*e)+n.yCenter}},getPointPositionForValue:function(t,e){return this.getPointPosition(t,this.getDistanceFromCenterForValue(e))},getBasePosition:function(){var t=this,e=t.min,n=t.max;return t.getPointPositionForValue(0,t.beginAtZero?0:e<0&&n<0?n:e>0&&n>0?e:0)},draw:function(){var t=this,e=t.options,n=e.gridLines,i=e.ticks,o=a.valueOrDefault;if(e.display){var r=t.ctx,l=this.getIndexAngle(0),s=o(i.fontSize,v.defaultFontSize),u=o(i.fontStyle,v.defaultFontStyle),d=o(i.fontFamily,v.defaultFontFamily),c=a.fontString(s,u,d);a.each(t.ticks,function(e,a){if(a>0||i.reverse){var u=t.getDistanceFromCenterForValue(t.ticksAsNumbers[a]);if(n.display&&0!==a&&g(t,n,u,a),i.display){var d=o(i.fontColor,v.defaultFontColor);if(r.font=c,r.save(),r.translate(t.xCenter,t.yCenter),r.rotate(l),i.showLabelBackdrop){var h=r.measureText(e).width;r.fillStyle=i.backdropColor,r.fillRect(-h/2-i.backdropPaddingX,-u-s/2-i.backdropPaddingY,h+2*i.backdropPaddingX,s+2*i.backdropPaddingY)}r.textAlign="center",r.textBaseline="middle",r.fillStyle=d,r.fillText(e,0,-u),r.restore()}}}),(e.angleLines.display||e.pointLabels.display)&&f(t)}}});t.scaleService.registerScaleType("radialLinear",b,m)}},{25:25,34:34,45:45}],57:[function(t,e,n){"use strict";function i(t,e){return t-e}function a(t){var e,n,i,a={},o=[];for(e=0,n=t.length;e<n;++e)a[i=t[e]]||(a[i]=!0,o.push(i));return o}function o(t,e,n,i){if("linear"===i||!t.length)return[{time:e,pos:0},{time:n,pos:1}];var a,o,r,l,s,u=[],d=[e];for(a=0,o=t.length;a<o;++a)(l=t[a])>e&&l<n&&d.push(l);for(d.push(n),a=0,o=d.length;a<o;++a)s=d[a+1],r=d[a-1],l=d[a],void 0!==r&&void 0!==s&&Math.round((s+r)/2)===l||u.push({time:l,pos:a/(o-1)});return u}function r(t,e,n){for(var i,a,o,r=0,l=t.length-1;r>=0&&r<=l;){if(i=r+l>>1,a=t[i-1]||null,o=t[i],!a)return{lo:null,hi:o};if(o[e]<n)r=i+1;else{if(!(a[e]>n))return{lo:a,hi:o};l=i-1}}return{lo:o,hi:null}}function l(t,e,n,i){var a=r(t,e,n),o=a.lo?a.hi?a.lo:t[t.length-2]:t[0],l=a.lo?a.hi?a.hi:t[t.length-1]:t[1],s=l[e]-o[e],u=s?(n-o[e])/s:0,d=(l[i]-o[i])*u;return o[i]+d}function s(t,e){var n=e.parser,i=e.parser||e.format;return"function"==typeof n?n(t):"string"==typeof t&&"string"==typeof i?v(t,i):(t instanceof v||(t=v(t)),t.isValid()?t:"function"==typeof i?i(t):t)}function u(t,e){if(b.isNullOrUndef(t))return null;var n=e.options.time,i=s(e.getRightValue(t),n);return i.isValid()?(n.round&&i.startOf(n.round),i.valueOf()):null}function d(t,e,n,i){var a,o,r,l=e-t,s=k[n],u=s.size,d=s.steps;if(!d)return Math.ceil(l/((i||1)*u));for(a=0,o=d.length;a<o&&(r=d[a],!(Math.ceil(l/(u*r))<=i));++a);return r}function c(t,e,n,i){var a,o,r,l=w.length;for(a=w.indexOf(t);a<l-1;++a)if(o=k[w[a]],r=o.steps?o.steps[o.steps.length-1]:y,Math.ceil((n-e)/(r*o.size))<=i)return w[a];return w[l-1]}function h(t){for(var e=w.indexOf(t)+1,n=w.length;e<n;++e)if(k[w[e]].major)return w[e]}function f(t,e,n,i,a,o){var r,l=o.time,s=b.valueOrDefault(l.stepSize,l.unitStepSize),u="week"===n&&l.isoWeekday,c=o.ticks.major.enabled,h=k[n],f=v(t),g=v(e),p=[];for(s||(s=d(t,e,n,a)),u&&(f=f.isoWeekday(u),g=g.isoWeekday(u)),f=f.startOf(u?"day":n),(g=g.startOf(u?"day":n))<e&&g.add(1,n),r=v(f),c&&i&&!u&&!l.round&&(r.startOf(i),r.add(~~((f-r)/(h.size*s))*s,n));r<g;r.add(s,n))p.push(+r);return p.push(+r),p}function g(t,e,n,i,a){var o,r,s=0,u=0;return a.offset&&e.length&&(a.time.min||(o=e.length>1?e[1]:i,r=e[0],s=(l(t,"time",o,"pos")-l(t,"time",r,"pos"))/2),a.time.max||(o=e[e.length-1],r=e.length>1?e[e.length-2]:n,u=(l(t,"time",o,"pos")-l(t,"time",r,"pos"))/2)),{left:s,right:u}}function p(t,e){var n,i,a,o,r=[];for(n=0,i=t.length;n<i;++n)a=t[n],o=!!e&&a===+v(a).startOf(e),r.push({value:a,major:o});return r}var v=t(1);v="function"==typeof v?v:window.moment;var m=t(25),b=t(45),x=Number.MIN_SAFE_INTEGER||-9007199254740991,y=Number.MAX_SAFE_INTEGER||9007199254740991,k={millisecond:{major:!0,size:1,steps:[1,2,5,10,20,50,100,250,500]},second:{major:!0,size:1e3,steps:[1,2,5,10,30]},minute:{major:!0,size:6e4,steps:[1,2,5,10,30]},hour:{major:!0,size:36e5,steps:[1,2,3,6,12]},day:{major:!0,size:864e5,steps:[1,2,5]},week:{major:!1,size:6048e5,steps:[1,2,3,4]},month:{major:!0,size:2628e6,steps:[1,2,3]},quarter:{major:!1,size:7884e6,steps:[1,2,3,4]},year:{major:!0,size:3154e7}},w=Object.keys(k);e.exports=function(t){var e={position:"bottom",distribution:"linear",bounds:"data",time:{parser:!1,format:!1,unit:!1,round:!1,displayFormat:!1,isoWeekday:!1,minUnit:"millisecond",displayFormats:{millisecond:"h:mm:ss.SSS a",second:"h:mm:ss a",minute:"h:mm a",hour:"hA",day:"MMM D",week:"ll",month:"MMM YYYY",quarter:"[Q]Q - YYYY",year:"YYYY"}},ticks:{autoSkip:!1,source:"auto",major:{enabled:!1}}},n=t.Scale.extend({initialize:function(){if(!v)throw new Error("Chart.js - Moment.js could not be found! You must include it before Chart.js to use the time scale. Download at https://momentjs.com");this.mergeTicksOptions(),t.Scale.prototype.initialize.call(this)},update:function(){var e=this,n=e.options;return n.time&&n.time.format&&console.warn("options.time.format is deprecated and replaced by options.time.parser."),t.Scale.prototype.update.apply(e,arguments)},getRightValue:function(e){return e&&void 0!==e.t&&(e=e.t),t.Scale.prototype.getRightValue.call(this,e)},determineDataLimits:function(){var t,e,n,o,r,l,s=this,d=s.chart,c=s.options.time,h=u(c.min,s)||y,f=u(c.max,s)||x,g=[],p=[],m=[];for(t=0,n=d.data.labels.length;t<n;++t)m.push(u(d.data.labels[t],s));for(t=0,n=(d.data.datasets||[]).length;t<n;++t)if(d.isDatasetVisible(t))if(r=d.data.datasets[t].data,b.isObject(r[0]))for(p[t]=[],e=0,o=r.length;e<o;++e)l=u(r[e],s),g.push(l),p[t][e]=l;else g.push.apply(g,m),p[t]=m.slice(0);else p[t]=[];m.length&&(m=a(m).sort(i),h=Math.min(h,m[0]),f=Math.max(f,m[m.length-1])),g.length&&(g=a(g).sort(i),h=Math.min(h,g[0]),f=Math.max(f,g[g.length-1])),h=h===y?+v().startOf("day"):h,f=f===x?+v().endOf("day")+1:f,s.min=Math.min(h,f),s.max=Math.max(h+1,f),s._horizontal=s.isHorizontal(),s._table=[],s._timestamps={data:g,datasets:p,labels:m}},buildTicks:function(){var t,e,n,i=this,a=i.min,r=i.max,l=i.options,s=l.time,d=s.displayFormats,v=i.getLabelCapacity(a),m=s.unit||c(s.minUnit,a,r,v),b=h(m),x=[],y=[];switch(l.ticks.source){case"data":x=i._timestamps.data;break;case"labels":x=i._timestamps.labels;break;case"auto":default:x=f(a,r,m,b,v,l)}for("ticks"===l.bounds&&x.length&&(a=x[0],r=x[x.length-1]),a=u(s.min,i)||a,r=u(s.max,i)||r,t=0,e=x.length;t<e;++t)(n=x[t])>=a&&n<=r&&y.push(n);return i.min=a,i.max=r,i._unit=m,i._majorUnit=b,i._minorFormat=d[m],i._majorFormat=d[b],i._table=o(i._timestamps.data,a,r,l.distribution),i._offsets=g(i._table,y,a,r,l),p(y,b)},getLabelForIndex:function(t,e){var n=this,i=n.chart.data,a=n.options.time,o=i.labels&&t<i.labels.length?i.labels[t]:"",r=i.datasets[e].data[t];return b.isObject(r)&&(o=n.getRightValue(r)),a.tooltipFormat&&(o=s(o,a).format(a.tooltipFormat)),o},tickFormatFunction:function(t,e,n){var i=this,a=i.options,o=t.valueOf(),r=i._majorUnit,l=i._majorFormat,s=t.clone().startOf(i._majorUnit).valueOf(),u=a.ticks.major,d=u.enabled&&r&&l&&o===s,c=t.format(d?l:i._minorFormat),h=d?u:a.ticks.minor,f=b.valueOrDefault(h.callback,h.userCallback);return f?f(c,e,n):c},convertTicksToLabels:function(t){var e,n,i=[];for(e=0,n=t.length;e<n;++e)i.push(this.tickFormatFunction(v(t[e].value),e,t));return i},getPixelForOffset:function(t){var e=this,n=e._horizontal?e.width:e.height,i=e._horizontal?e.left:e.top,a=l(e._table,"time",t,"pos");return i+n*(e._offsets.left+a)/(e._offsets.left+1+e._offsets.right)},getPixelForValue:function(t,e,n){var i=this,a=null;if(void 0!==e&&void 0!==n&&(a=i._timestamps.datasets[n][e]),null===a&&(a=u(t,i)),null!==a)return i.getPixelForOffset(a)},getPixelForTick:function(t){var e=this.getTicks();return t>=0&&t<e.length?this.getPixelForOffset(e[t].value):null},getValueForPixel:function(t){var e=this,n=e._horizontal?e.width:e.height,i=e._horizontal?e.left:e.top,a=(n?(t-i)/n:0)*(e._offsets.left+1+e._offsets.left)-e._offsets.right,o=l(e._table,"pos",a,"time");return v(o)},getLabelWidth:function(t){var e=this,n=e.options.ticks,i=e.ctx.measureText(t).width,a=b.toRadians(n.maxRotation),o=Math.cos(a),r=Math.sin(a);return i*o+b.valueOrDefault(n.fontSize,m.global.defaultFontSize)*r},getLabelCapacity:function(t){var e=this;e._minorFormat=e.options.time.displayFormats.millisecond;var n=e.tickFormatFunction(v(t),0,[]),i=e.getLabelWidth(n),a=e.isHorizontal()?e.width:e.height;return Math.floor(a/i)}});t.scaleService.registerScaleType("time",n,e)}},{1:1,25:25,45:45}]},{},[7])(7)});
/*! Hammer.JS - v2.0.8 - 2016-04-23
 * http://hammerjs.github.io/
 *
 * Copyright (c) 2016 Jorik Tangelder;
 * Licensed under the MIT license */
(function(window, document, exportName, undefined) {
    'use strict';
  
  var VENDOR_PREFIXES = ['', 'webkit', 'Moz', 'MS', 'ms', 'o'];
  var TEST_ELEMENT = document.createElement('div');
  
  var TYPE_FUNCTION = 'function';
  
  var round = Math.round;
  var abs = Math.abs;
  var now = Date.now;
  
  /**
   * set a timeout with a given scope
   * @param {Function} fn
   * @param {Number} timeout
   * @param {Object} context
   * @returns {number}
   */
  function setTimeoutContext(fn, timeout, context) {
      return setTimeout(bindFn(fn, context), timeout);
  }
  
  /**
   * if the argument is an array, we want to execute the fn on each entry
   * if it aint an array we don't want to do a thing.
   * this is used by all the methods that accept a single and array argument.
   * @param {*|Array} arg
   * @param {String} fn
   * @param {Object} [context]
   * @returns {Boolean}
   */
  function invokeArrayArg(arg, fn, context) {
      if (Array.isArray(arg)) {
          each(arg, context[fn], context);
          return true;
      }
      return false;
  }
  
  /**
   * walk objects and arrays
   * @param {Object} obj
   * @param {Function} iterator
   * @param {Object} context
   */
  function each(obj, iterator, context) {
      var i;
  
      if (!obj) {
          return;
      }
  
      if (obj.forEach) {
          obj.forEach(iterator, context);
      } else if (obj.length !== undefined) {
          i = 0;
          while (i < obj.length) {
              iterator.call(context, obj[i], i, obj);
              i++;
          }
      } else {
          for (i in obj) {
              obj.hasOwnProperty(i) && iterator.call(context, obj[i], i, obj);
          }
      }
  }
  
  /**
   * wrap a method with a deprecation warning and stack trace
   * @param {Function} method
   * @param {String} name
   * @param {String} message
   * @returns {Function} A new function wrapping the supplied method.
   */
  function deprecate(method, name, message) {
      var deprecationMessage = 'DEPRECATED METHOD: ' + name + '\n' + message + ' AT \n';
      return function() {
          var e = new Error('get-stack-trace');
          var stack = e && e.stack ? e.stack.replace(/^[^\(]+?[\n$]/gm, '')
              .replace(/^\s+at\s+/gm, '')
              .replace(/^Object.<anonymous>\s*\(/gm, '{anonymous}()@') : 'Unknown Stack Trace';
  
          var log = window.console && (window.console.warn || window.console.log);
          if (log) {
              log.call(window.console, deprecationMessage, stack);
          }
          return method.apply(this, arguments);
      };
  }
  
  /**
   * extend object.
   * means that properties in dest will be overwritten by the ones in src.
   * @param {Object} target
   * @param {...Object} objects_to_assign
   * @returns {Object} target
   */
  var assign;
  if (typeof Object.assign !== 'function') {
      assign = function assign(target) {
          if (target === undefined || target === null) {
              throw new TypeError('Cannot convert undefined or null to object');
          }
  
          var output = Object(target);
          for (var index = 1; index < arguments.length; index++) {
              var source = arguments[index];
              if (source !== undefined && source !== null) {
                  for (var nextKey in source) {
                      if (source.hasOwnProperty(nextKey)) {
                          output[nextKey] = source[nextKey];
                      }
                  }
              }
          }
          return output;
      };
  } else {
      assign = Object.assign;
  }
  
  /**
   * extend object.
   * means that properties in dest will be overwritten by the ones in src.
   * @param {Object} dest
   * @param {Object} src
   * @param {Boolean} [merge=false]
   * @returns {Object} dest
   */
  var extend = deprecate(function extend(dest, src, merge) {
      var keys = Object.keys(src);
      var i = 0;
      while (i < keys.length) {
          if (!merge || (merge && dest[keys[i]] === undefined)) {
              dest[keys[i]] = src[keys[i]];
          }
          i++;
      }
      return dest;
  }, 'extend', 'Use `assign`.');
  
  /**
   * merge the values from src in the dest.
   * means that properties that exist in dest will not be overwritten by src
   * @param {Object} dest
   * @param {Object} src
   * @returns {Object} dest
   */
  var merge = deprecate(function merge(dest, src) {
      return extend(dest, src, true);
  }, 'merge', 'Use `assign`.');
  
  /**
   * simple class inheritance
   * @param {Function} child
   * @param {Function} base
   * @param {Object} [properties]
   */
  function inherit(child, base, properties) {
      var baseP = base.prototype,
          childP;
  
      childP = child.prototype = Object.create(baseP);
      childP.constructor = child;
      childP._super = baseP;
  
      if (properties) {
          assign(childP, properties);
      }
  }
  
  /**
   * simple function bind
   * @param {Function} fn
   * @param {Object} context
   * @returns {Function}
   */
  function bindFn(fn, context) {
      return function boundFn() {
          return fn.apply(context, arguments);
      };
  }
  
  /**
   * let a boolean value also be a function that must return a boolean
   * this first item in args will be used as the context
   * @param {Boolean|Function} val
   * @param {Array} [args]
   * @returns {Boolean}
   */
  function boolOrFn(val, args) {
      if (typeof val == TYPE_FUNCTION) {
          return val.apply(args ? args[0] || undefined : undefined, args);
      }
      return val;
  }
  
  /**
   * use the val2 when val1 is undefined
   * @param {*} val1
   * @param {*} val2
   * @returns {*}
   */
  function ifUndefined(val1, val2) {
      return (val1 === undefined) ? val2 : val1;
  }
  
  /**
   * addEventListener with multiple events at once
   * @param {EventTarget} target
   * @param {String} types
   * @param {Function} handler
   */
  function addEventListeners(target, types, handler) {
      each(splitStr(types), function(type) {
          target.addEventListener(type, handler, false);
      });
  }
  
  /**
   * removeEventListener with multiple events at once
   * @param {EventTarget} target
   * @param {String} types
   * @param {Function} handler
   */
  function removeEventListeners(target, types, handler) {
      each(splitStr(types), function(type) {
          target.removeEventListener(type, handler, false);
      });
  }
  
  /**
   * find if a node is in the given parent
   * @method hasParent
   * @param {HTMLElement} node
   * @param {HTMLElement} parent
   * @return {Boolean} found
   */
  function hasParent(node, parent) {
      while (node) {
          if (node == parent) {
              return true;
          }
          node = node.parentNode;
      }
      return false;
  }
  
  /**
   * small indexOf wrapper
   * @param {String} str
   * @param {String} find
   * @returns {Boolean} found
   */
  function inStr(str, find) {
      return str.indexOf(find) > -1;
  }
  
  /**
   * split string on whitespace
   * @param {String} str
   * @returns {Array} words
   */
  function splitStr(str) {
      return str.trim().split(/\s+/g);
  }
  
  /**
   * find if a array contains the object using indexOf or a simple polyFill
   * @param {Array} src
   * @param {String} find
   * @param {String} [findByKey]
   * @return {Boolean|Number} false when not found, or the index
   */
  function inArray(src, find, findByKey) {
      if (src.indexOf && !findByKey) {
          return src.indexOf(find);
      } else {
          var i = 0;
          while (i < src.length) {
              if ((findByKey && src[i][findByKey] == find) || (!findByKey && src[i] === find)) {
                  return i;
              }
              i++;
          }
          return -1;
      }
  }
  
  /**
   * convert array-like objects to real arrays
   * @param {Object} obj
   * @returns {Array}
   */
  function toArray(obj) {
      return Array.prototype.slice.call(obj, 0);
  }
  
  /**
   * unique array with objects based on a key (like 'id') or just by the array's value
   * @param {Array} src [{id:1},{id:2},{id:1}]
   * @param {String} [key]
   * @param {Boolean} [sort=False]
   * @returns {Array} [{id:1},{id:2}]
   */
  function uniqueArray(src, key, sort) {
      var results = [];
      var values = [];
      var i = 0;
  
      while (i < src.length) {
          var val = key ? src[i][key] : src[i];
          if (inArray(values, val) < 0) {
              results.push(src[i]);
          }
          values[i] = val;
          i++;
      }
  
      if (sort) {
          if (!key) {
              results = results.sort();
          } else {
              results = results.sort(function sortUniqueArray(a, b) {
                  return a[key] > b[key];
              });
          }
      }
  
      return results;
  }
  
  /**
   * get the prefixed property
   * @param {Object} obj
   * @param {String} property
   * @returns {String|Undefined} prefixed
   */
  function prefixed(obj, property) {
      var prefix, prop;
      var camelProp = property[0].toUpperCase() + property.slice(1);
  
      var i = 0;
      while (i < VENDOR_PREFIXES.length) {
          prefix = VENDOR_PREFIXES[i];
          prop = (prefix) ? prefix + camelProp : property;
  
          if (prop in obj) {
              return prop;
          }
          i++;
      }
      return undefined;
  }
  
  /**
   * get a unique id
   * @returns {number} uniqueId
   */
  var _uniqueId = 1;
  function uniqueId() {
      return _uniqueId++;
  }
  
  /**
   * get the window object of an element
   * @param {HTMLElement} element
   * @returns {DocumentView|Window}
   */
  function getWindowForElement(element) {
      var doc = element.ownerDocument || element;
      return (doc.defaultView || doc.parentWindow || window);
  }
  
  var MOBILE_REGEX = /mobile|tablet|ip(ad|hone|od)|android/i;
  
  var SUPPORT_TOUCH = ('ontouchstart' in window);
  var SUPPORT_POINTER_EVENTS = prefixed(window, 'PointerEvent') !== undefined;
  var SUPPORT_ONLY_TOUCH = SUPPORT_TOUCH && MOBILE_REGEX.test(navigator.userAgent);
  
  var INPUT_TYPE_TOUCH = 'touch';
  var INPUT_TYPE_PEN = 'pen';
  var INPUT_TYPE_MOUSE = 'mouse';
  var INPUT_TYPE_KINECT = 'kinect';
  
  var COMPUTE_INTERVAL = 25;
  
  var INPUT_START = 1;
  var INPUT_MOVE = 2;
  var INPUT_END = 4;
  var INPUT_CANCEL = 8;
  
  var DIRECTION_NONE = 1;
  var DIRECTION_LEFT = 2;
  var DIRECTION_RIGHT = 4;
  var DIRECTION_UP = 8;
  var DIRECTION_DOWN = 16;
  
  var DIRECTION_HORIZONTAL = DIRECTION_LEFT | DIRECTION_RIGHT;
  var DIRECTION_VERTICAL = DIRECTION_UP | DIRECTION_DOWN;
  var DIRECTION_ALL = DIRECTION_HORIZONTAL | DIRECTION_VERTICAL;
  
  var PROPS_XY = ['x', 'y'];
  var PROPS_CLIENT_XY = ['clientX', 'clientY'];
  
  /**
   * create new input type manager
   * @param {Manager} manager
   * @param {Function} callback
   * @returns {Input}
   * @constructor
   */
  function Input(manager, callback) {
      var self = this;
      this.manager = manager;
      this.callback = callback;
      this.element = manager.element;
      this.target = manager.options.inputTarget;
  
      // smaller wrapper around the handler, for the scope and the enabled state of the manager,
      // so when disabled the input events are completely bypassed.
      this.domHandler = function(ev) {
          if (boolOrFn(manager.options.enable, [manager])) {
              self.handler(ev);
          }
      };
  
      this.init();
  
  }
  
  Input.prototype = {
      /**
       * should handle the inputEvent data and trigger the callback
       * @virtual
       */
      handler: function() { },
  
      /**
       * bind the events
       */
      init: function() {
          this.evEl && addEventListeners(this.element, this.evEl, this.domHandler);
          this.evTarget && addEventListeners(this.target, this.evTarget, this.domHandler);
          this.evWin && addEventListeners(getWindowForElement(this.element), this.evWin, this.domHandler);
      },
  
      /**
       * unbind the events
       */
      destroy: function() {
          this.evEl && removeEventListeners(this.element, this.evEl, this.domHandler);
          this.evTarget && removeEventListeners(this.target, this.evTarget, this.domHandler);
          this.evWin && removeEventListeners(getWindowForElement(this.element), this.evWin, this.domHandler);
      }
  };
  
  /**
   * create new input type manager
   * called by the Manager constructor
   * @param {Hammer} manager
   * @returns {Input}
   */
  function createInputInstance(manager) {
      var Type;
      var inputClass = manager.options.inputClass;
  
      if (inputClass) {
          Type = inputClass;
      } else if (SUPPORT_POINTER_EVENTS) {
          Type = PointerEventInput;
      } else if (SUPPORT_ONLY_TOUCH) {
          Type = TouchInput;
      } else if (!SUPPORT_TOUCH) {
          Type = MouseInput;
      } else {
          Type = TouchMouseInput;
      }
      return new (Type)(manager, inputHandler);
  }
  
  /**
   * handle input events
   * @param {Manager} manager
   * @param {String} eventType
   * @param {Object} input
   */
  function inputHandler(manager, eventType, input) {
      var pointersLen = input.pointers.length;
      var changedPointersLen = input.changedPointers.length;
      var isFirst = (eventType & INPUT_START && (pointersLen - changedPointersLen === 0));
      var isFinal = (eventType & (INPUT_END | INPUT_CANCEL) && (pointersLen - changedPointersLen === 0));
  
      input.isFirst = !!isFirst;
      input.isFinal = !!isFinal;
  
      if (isFirst) {
          manager.session = {};
      }
  
      // source event is the normalized value of the domEvents
      // like 'touchstart, mouseup, pointerdown'
      input.eventType = eventType;
  
      // compute scale, rotation etc
      computeInputData(manager, input);
  
      // emit secret event
      manager.emit('hammer.input', input);
  
      manager.recognize(input);
      manager.session.prevInput = input;
  }
  
  /**
   * extend the data with some usable properties like scale, rotate, velocity etc
   * @param {Object} manager
   * @param {Object} input
   */
  function computeInputData(manager, input) {
      var session = manager.session;
      var pointers = input.pointers;
      var pointersLength = pointers.length;
  
      // store the first input to calculate the distance and direction
      if (!session.firstInput) {
          session.firstInput = simpleCloneInputData(input);
      }
  
      // to compute scale and rotation we need to store the multiple touches
      if (pointersLength > 1 && !session.firstMultiple) {
          session.firstMultiple = simpleCloneInputData(input);
      } else if (pointersLength === 1) {
          session.firstMultiple = false;
      }
  
      var firstInput = session.firstInput;
      var firstMultiple = session.firstMultiple;
      var offsetCenter = firstMultiple ? firstMultiple.center : firstInput.center;
  
      var center = input.center = getCenter(pointers);
      input.timeStamp = now();
      input.deltaTime = input.timeStamp - firstInput.timeStamp;
  
      input.angle = getAngle(offsetCenter, center);
      input.distance = getDistance(offsetCenter, center);
  
      computeDeltaXY(session, input);
      input.offsetDirection = getDirection(input.deltaX, input.deltaY);
  
      var overallVelocity = getVelocity(input.deltaTime, input.deltaX, input.deltaY);
      input.overallVelocityX = overallVelocity.x;
      input.overallVelocityY = overallVelocity.y;
      input.overallVelocity = (abs(overallVelocity.x) > abs(overallVelocity.y)) ? overallVelocity.x : overallVelocity.y;
  
      input.scale = firstMultiple ? getScale(firstMultiple.pointers, pointers) : 1;
      input.rotation = firstMultiple ? getRotation(firstMultiple.pointers, pointers) : 0;
  
      input.maxPointers = !session.prevInput ? input.pointers.length : ((input.pointers.length >
          session.prevInput.maxPointers) ? input.pointers.length : session.prevInput.maxPointers);
  
      computeIntervalInputData(session, input);
  
      // find the correct target
      var target = manager.element;
      if (hasParent(input.srcEvent.target, target)) {
          target = input.srcEvent.target;
      }
      input.target = target;
  }
  
  function computeDeltaXY(session, input) {
      var center = input.center;
      var offset = session.offsetDelta || {};
      var prevDelta = session.prevDelta || {};
      var prevInput = session.prevInput || {};
  
      if (input.eventType === INPUT_START || prevInput.eventType === INPUT_END) {
          prevDelta = session.prevDelta = {
              x: prevInput.deltaX || 0,
              y: prevInput.deltaY || 0
          };
  
          offset = session.offsetDelta = {
              x: center.x,
              y: center.y
          };
      }
  
      input.deltaX = prevDelta.x + (center.x - offset.x);
      input.deltaY = prevDelta.y + (center.y - offset.y);
  }
  
  /**
   * velocity is calculated every x ms
   * @param {Object} session
   * @param {Object} input
   */
  function computeIntervalInputData(session, input) {
      var last = session.lastInterval || input,
          deltaTime = input.timeStamp - last.timeStamp,
          velocity, velocityX, velocityY, direction;
  
      if (input.eventType != INPUT_CANCEL && (deltaTime > COMPUTE_INTERVAL || last.velocity === undefined)) {
          var deltaX = input.deltaX - last.deltaX;
          var deltaY = input.deltaY - last.deltaY;
  
          var v = getVelocity(deltaTime, deltaX, deltaY);
          velocityX = v.x;
          velocityY = v.y;
          velocity = (abs(v.x) > abs(v.y)) ? v.x : v.y;
          direction = getDirection(deltaX, deltaY);
  
          session.lastInterval = input;
      } else {
          // use latest velocity info if it doesn't overtake a minimum period
          velocity = last.velocity;
          velocityX = last.velocityX;
          velocityY = last.velocityY;
          direction = last.direction;
      }
  
      input.velocity = velocity;
      input.velocityX = velocityX;
      input.velocityY = velocityY;
      input.direction = direction;
  }
  
  /**
   * create a simple clone from the input used for storage of firstInput and firstMultiple
   * @param {Object} input
   * @returns {Object} clonedInputData
   */
  function simpleCloneInputData(input) {
      // make a simple copy of the pointers because we will get a reference if we don't
      // we only need clientXY for the calculations
      var pointers = [];
      var i = 0;
      while (i < input.pointers.length) {
          pointers[i] = {
              clientX: round(input.pointers[i].clientX),
              clientY: round(input.pointers[i].clientY)
          };
          i++;
      }
  
      return {
          timeStamp: now(),
          pointers: pointers,
          center: getCenter(pointers),
          deltaX: input.deltaX,
          deltaY: input.deltaY
      };
  }
  
  /**
   * get the center of all the pointers
   * @param {Array} pointers
   * @return {Object} center contains `x` and `y` properties
   */
  function getCenter(pointers) {
      var pointersLength = pointers.length;
  
      // no need to loop when only one touch
      if (pointersLength === 1) {
          return {
              x: round(pointers[0].clientX),
              y: round(pointers[0].clientY)
          };
      }
  
      var x = 0, y = 0, i = 0;
      while (i < pointersLength) {
          x += pointers[i].clientX;
          y += pointers[i].clientY;
          i++;
      }
  
      return {
          x: round(x / pointersLength),
          y: round(y / pointersLength)
      };
  }
  
  /**
   * calculate the velocity between two points. unit is in px per ms.
   * @param {Number} deltaTime
   * @param {Number} x
   * @param {Number} y
   * @return {Object} velocity `x` and `y`
   */
  function getVelocity(deltaTime, x, y) {
      return {
          x: x / deltaTime || 0,
          y: y / deltaTime || 0
      };
  }
  
  /**
   * get the direction between two points
   * @param {Number} x
   * @param {Number} y
   * @return {Number} direction
   */
  function getDirection(x, y) {
      if (x === y) {
          return DIRECTION_NONE;
      }
  
      if (abs(x) >= abs(y)) {
          return x < 0 ? DIRECTION_LEFT : DIRECTION_RIGHT;
      }
      return y < 0 ? DIRECTION_UP : DIRECTION_DOWN;
  }
  
  /**
   * calculate the absolute distance between two points
   * @param {Object} p1 {x, y}
   * @param {Object} p2 {x, y}
   * @param {Array} [props] containing x and y keys
   * @return {Number} distance
   */
  function getDistance(p1, p2, props) {
      if (!props) {
          props = PROPS_XY;
      }
      var x = p2[props[0]] - p1[props[0]],
          y = p2[props[1]] - p1[props[1]];
  
      return Math.sqrt((x * x) + (y * y));
  }
  
  /**
   * calculate the angle between two coordinates
   * @param {Object} p1
   * @param {Object} p2
   * @param {Array} [props] containing x and y keys
   * @return {Number} angle
   */
  function getAngle(p1, p2, props) {
      if (!props) {
          props = PROPS_XY;
      }
      var x = p2[props[0]] - p1[props[0]],
          y = p2[props[1]] - p1[props[1]];
      return Math.atan2(y, x) * 180 / Math.PI;
  }
  
  /**
   * calculate the rotation degrees between two pointersets
   * @param {Array} start array of pointers
   * @param {Array} end array of pointers
   * @return {Number} rotation
   */
  function getRotation(start, end) {
      return getAngle(end[1], end[0], PROPS_CLIENT_XY) + getAngle(start[1], start[0], PROPS_CLIENT_XY);
  }
  
  /**
   * calculate the scale factor between two pointersets
   * no scale is 1, and goes down to 0 when pinched together, and bigger when pinched out
   * @param {Array} start array of pointers
   * @param {Array} end array of pointers
   * @return {Number} scale
   */
  function getScale(start, end) {
      return getDistance(end[0], end[1], PROPS_CLIENT_XY) / getDistance(start[0], start[1], PROPS_CLIENT_XY);
  }
  
  var MOUSE_INPUT_MAP = {
      mousedown: INPUT_START,
      mousemove: INPUT_MOVE,
      mouseup: INPUT_END
  };
  
  var MOUSE_ELEMENT_EVENTS = 'mousedown';
  var MOUSE_WINDOW_EVENTS = 'mousemove mouseup';
  
  /**
   * Mouse events input
   * @constructor
   * @extends Input
   */
  function MouseInput() {
      this.evEl = MOUSE_ELEMENT_EVENTS;
      this.evWin = MOUSE_WINDOW_EVENTS;
  
      this.pressed = false; // mousedown state
  
      Input.apply(this, arguments);
  }
  
  inherit(MouseInput, Input, {
      /**
       * handle mouse events
       * @param {Object} ev
       */
      handler: function MEhandler(ev) {
          var eventType = MOUSE_INPUT_MAP[ev.type];
  
          // on start we want to have the left mouse button down
          if (eventType & INPUT_START && ev.button === 0) {
              this.pressed = true;
          }
  
          if (eventType & INPUT_MOVE && ev.which !== 1) {
              eventType = INPUT_END;
          }
  
          // mouse must be down
          if (!this.pressed) {
              return;
          }
  
          if (eventType & INPUT_END) {
              this.pressed = false;
          }
  
          this.callback(this.manager, eventType, {
              pointers: [ev],
              changedPointers: [ev],
              pointerType: INPUT_TYPE_MOUSE,
              srcEvent: ev
          });
      }
  });
  
  var POINTER_INPUT_MAP = {
      pointerdown: INPUT_START,
      pointermove: INPUT_MOVE,
      pointerup: INPUT_END,
      pointercancel: INPUT_CANCEL,
      pointerout: INPUT_CANCEL
  };
  
  // in IE10 the pointer types is defined as an enum
  var IE10_POINTER_TYPE_ENUM = {
      2: INPUT_TYPE_TOUCH,
      3: INPUT_TYPE_PEN,
      4: INPUT_TYPE_MOUSE,
      5: INPUT_TYPE_KINECT // see https://twitter.com/jacobrossi/status/480596438489890816
  };
  
  var POINTER_ELEMENT_EVENTS = 'pointerdown';
  var POINTER_WINDOW_EVENTS = 'pointermove pointerup pointercancel';
  
  // IE10 has prefixed support, and case-sensitive
  if (window.MSPointerEvent && !window.PointerEvent) {
      POINTER_ELEMENT_EVENTS = 'MSPointerDown';
      POINTER_WINDOW_EVENTS = 'MSPointerMove MSPointerUp MSPointerCancel';
  }
  
  /**
   * Pointer events input
   * @constructor
   * @extends Input
   */
  function PointerEventInput() {
      this.evEl = POINTER_ELEMENT_EVENTS;
      this.evWin = POINTER_WINDOW_EVENTS;
  
      Input.apply(this, arguments);
  
      this.store = (this.manager.session.pointerEvents = []);
  }
  
  inherit(PointerEventInput, Input, {
      /**
       * handle mouse events
       * @param {Object} ev
       */
      handler: function PEhandler(ev) {
          var store = this.store;
          var removePointer = false;
  
          var eventTypeNormalized = ev.type.toLowerCase().replace('ms', '');
          var eventType = POINTER_INPUT_MAP[eventTypeNormalized];
          var pointerType = IE10_POINTER_TYPE_ENUM[ev.pointerType] || ev.pointerType;
  
          var isTouch = (pointerType == INPUT_TYPE_TOUCH);
  
          // get index of the event in the store
          var storeIndex = inArray(store, ev.pointerId, 'pointerId');
  
          // start and mouse must be down
          if (eventType & INPUT_START && (ev.button === 0 || isTouch)) {
              if (storeIndex < 0) {
                  store.push(ev);
                  storeIndex = store.length - 1;
              }
          } else if (eventType & (INPUT_END | INPUT_CANCEL)) {
              removePointer = true;
          }
  
          // it not found, so the pointer hasn't been down (so it's probably a hover)
          if (storeIndex < 0) {
              return;
          }
  
          // update the event in the store
          store[storeIndex] = ev;
  
          this.callback(this.manager, eventType, {
              pointers: store,
              changedPointers: [ev],
              pointerType: pointerType,
              srcEvent: ev
          });
  
          if (removePointer) {
              // remove from the store
              store.splice(storeIndex, 1);
          }
      }
  });
  
  var SINGLE_TOUCH_INPUT_MAP = {
      touchstart: INPUT_START,
      touchmove: INPUT_MOVE,
      touchend: INPUT_END,
      touchcancel: INPUT_CANCEL
  };
  
  var SINGLE_TOUCH_TARGET_EVENTS = 'touchstart';
  var SINGLE_TOUCH_WINDOW_EVENTS = 'touchstart touchmove touchend touchcancel';
  
  /**
   * Touch events input
   * @constructor
   * @extends Input
   */
  function SingleTouchInput() {
      this.evTarget = SINGLE_TOUCH_TARGET_EVENTS;
      this.evWin = SINGLE_TOUCH_WINDOW_EVENTS;
      this.started = false;
  
      Input.apply(this, arguments);
  }
  
  inherit(SingleTouchInput, Input, {
      handler: function TEhandler(ev) {
          var type = SINGLE_TOUCH_INPUT_MAP[ev.type];
  
          // should we handle the touch events?
          if (type === INPUT_START) {
              this.started = true;
          }
  
          if (!this.started) {
              return;
          }
  
          var touches = normalizeSingleTouches.call(this, ev, type);
  
          // when done, reset the started state
          if (type & (INPUT_END | INPUT_CANCEL) && touches[0].length - touches[1].length === 0) {
              this.started = false;
          }
  
          this.callback(this.manager, type, {
              pointers: touches[0],
              changedPointers: touches[1],
              pointerType: INPUT_TYPE_TOUCH,
              srcEvent: ev
          });
      }
  });
  
  /**
   * @this {TouchInput}
   * @param {Object} ev
   * @param {Number} type flag
   * @returns {undefined|Array} [all, changed]
   */
  function normalizeSingleTouches(ev, type) {
      var all = toArray(ev.touches);
      var changed = toArray(ev.changedTouches);
  
      if (type & (INPUT_END | INPUT_CANCEL)) {
          all = uniqueArray(all.concat(changed), 'identifier', true);
      }
  
      return [all, changed];
  }
  
  var TOUCH_INPUT_MAP = {
      touchstart: INPUT_START,
      touchmove: INPUT_MOVE,
      touchend: INPUT_END,
      touchcancel: INPUT_CANCEL
  };
  
  var TOUCH_TARGET_EVENTS = 'touchstart touchmove touchend touchcancel';
  
  /**
   * Multi-user touch events input
   * @constructor
   * @extends Input
   */
  function TouchInput() {
      this.evTarget = TOUCH_TARGET_EVENTS;
      this.targetIds = {};
  
      Input.apply(this, arguments);
  }
  
  inherit(TouchInput, Input, {
      handler: function MTEhandler(ev) {
          var type = TOUCH_INPUT_MAP[ev.type];
          var touches = getTouches.call(this, ev, type);
          if (!touches) {
              return;
          }
  
          this.callback(this.manager, type, {
              pointers: touches[0],
              changedPointers: touches[1],
              pointerType: INPUT_TYPE_TOUCH,
              srcEvent: ev
          });
      }
  });
  
  /**
   * @this {TouchInput}
   * @param {Object} ev
   * @param {Number} type flag
   * @returns {undefined|Array} [all, changed]
   */
  function getTouches(ev, type) {
      var allTouches = toArray(ev.touches);
      var targetIds = this.targetIds;
  
      // when there is only one touch, the process can be simplified
      if (type & (INPUT_START | INPUT_MOVE) && allTouches.length === 1) {
          targetIds[allTouches[0].identifier] = true;
          return [allTouches, allTouches];
      }
  
      var i,
          targetTouches,
          changedTouches = toArray(ev.changedTouches),
          changedTargetTouches = [],
          target = this.target;
  
      // get target touches from touches
      targetTouches = allTouches.filter(function(touch) {
          return hasParent(touch.target, target);
      });
  
      // collect touches
      if (type === INPUT_START) {
          i = 0;
          while (i < targetTouches.length) {
              targetIds[targetTouches[i].identifier] = true;
              i++;
          }
      }
  
      // filter changed touches to only contain touches that exist in the collected target ids
      i = 0;
      while (i < changedTouches.length) {
          if (targetIds[changedTouches[i].identifier]) {
              changedTargetTouches.push(changedTouches[i]);
          }
  
          // cleanup removed touches
          if (type & (INPUT_END | INPUT_CANCEL)) {
              delete targetIds[changedTouches[i].identifier];
          }
          i++;
      }
  
      if (!changedTargetTouches.length) {
          return;
      }
  
      return [
          // merge targetTouches with changedTargetTouches so it contains ALL touches, including 'end' and 'cancel'
          uniqueArray(targetTouches.concat(changedTargetTouches), 'identifier', true),
          changedTargetTouches
      ];
  }
  
  /**
   * Combined touch and mouse input
   *
   * Touch has a higher priority then mouse, and while touching no mouse events are allowed.
   * This because touch devices also emit mouse events while doing a touch.
   *
   * @constructor
   * @extends Input
   */
  
  var DEDUP_TIMEOUT = 2500;
  var DEDUP_DISTANCE = 25;
  
  function TouchMouseInput() {
      Input.apply(this, arguments);
  
      var handler = bindFn(this.handler, this);
      this.touch = new TouchInput(this.manager, handler);
      this.mouse = new MouseInput(this.manager, handler);
  
      this.primaryTouch = null;
      this.lastTouches = [];
  }
  
  inherit(TouchMouseInput, Input, {
      /**
       * handle mouse and touch events
       * @param {Hammer} manager
       * @param {String} inputEvent
       * @param {Object} inputData
       */
      handler: function TMEhandler(manager, inputEvent, inputData) {
          var isTouch = (inputData.pointerType == INPUT_TYPE_TOUCH),
              isMouse = (inputData.pointerType == INPUT_TYPE_MOUSE);
  
          if (isMouse && inputData.sourceCapabilities && inputData.sourceCapabilities.firesTouchEvents) {
              return;
          }
  
          // when we're in a touch event, record touches to  de-dupe synthetic mouse event
          if (isTouch) {
              recordTouches.call(this, inputEvent, inputData);
          } else if (isMouse && isSyntheticEvent.call(this, inputData)) {
              return;
          }
  
          this.callback(manager, inputEvent, inputData);
      },
  
      /**
       * remove the event listeners
       */
      destroy: function destroy() {
          this.touch.destroy();
          this.mouse.destroy();
      }
  });
  
  function recordTouches(eventType, eventData) {
      if (eventType & INPUT_START) {
          this.primaryTouch = eventData.changedPointers[0].identifier;
          setLastTouch.call(this, eventData);
      } else if (eventType & (INPUT_END | INPUT_CANCEL)) {
          setLastTouch.call(this, eventData);
      }
  }
  
  function setLastTouch(eventData) {
      var touch = eventData.changedPointers[0];
  
      if (touch.identifier === this.primaryTouch) {
          var lastTouch = {x: touch.clientX, y: touch.clientY};
          this.lastTouches.push(lastTouch);
          var lts = this.lastTouches;
          var removeLastTouch = function() {
              var i = lts.indexOf(lastTouch);
              if (i > -1) {
                  lts.splice(i, 1);
              }
          };
          setTimeout(removeLastTouch, DEDUP_TIMEOUT);
      }
  }
  
  function isSyntheticEvent(eventData) {
      var x = eventData.srcEvent.clientX, y = eventData.srcEvent.clientY;
      for (var i = 0; i < this.lastTouches.length; i++) {
          var t = this.lastTouches[i];
          var dx = Math.abs(x - t.x), dy = Math.abs(y - t.y);
          if (dx <= DEDUP_DISTANCE && dy <= DEDUP_DISTANCE) {
              return true;
          }
      }
      return false;
  }
  
  var PREFIXED_TOUCH_ACTION = prefixed(TEST_ELEMENT.style, 'touchAction');
  var NATIVE_TOUCH_ACTION = PREFIXED_TOUCH_ACTION !== undefined;
  
  // magical touchAction value
  var TOUCH_ACTION_COMPUTE = 'compute';
  var TOUCH_ACTION_AUTO = 'auto';
  var TOUCH_ACTION_MANIPULATION = 'manipulation'; // not implemented
  var TOUCH_ACTION_NONE = 'none';
  var TOUCH_ACTION_PAN_X = 'pan-x';
  var TOUCH_ACTION_PAN_Y = 'pan-y';
  var TOUCH_ACTION_MAP = getTouchActionProps();
  
  /**
   * Touch Action
   * sets the touchAction property or uses the js alternative
   * @param {Manager} manager
   * @param {String} value
   * @constructor
   */
  function TouchAction(manager, value) {
      this.manager = manager;
      this.set(value);
  }
  
  TouchAction.prototype = {
      /**
       * set the touchAction value on the element or enable the polyfill
       * @param {String} value
       */
      set: function(value) {
          // find out the touch-action by the event handlers
          if (value == TOUCH_ACTION_COMPUTE) {
              value = this.compute();
          }
  
          if (NATIVE_TOUCH_ACTION && this.manager.element.style && TOUCH_ACTION_MAP[value]) {
              this.manager.element.style[PREFIXED_TOUCH_ACTION] = value;
          }
          this.actions = value.toLowerCase().trim();
      },
  
      /**
       * just re-set the touchAction value
       */
      update: function() {
          this.set(this.manager.options.touchAction);
      },
  
      /**
       * compute the value for the touchAction property based on the recognizer's settings
       * @returns {String} value
       */
      compute: function() {
          var actions = [];
          each(this.manager.recognizers, function(recognizer) {
              if (boolOrFn(recognizer.options.enable, [recognizer])) {
                  actions = actions.concat(recognizer.getTouchAction());
              }
          });
          return cleanTouchActions(actions.join(' '));
      },
  
      /**
       * this method is called on each input cycle and provides the preventing of the browser behavior
       * @param {Object} input
       */
      preventDefaults: function(input) {
          var srcEvent = input.srcEvent;
          var direction = input.offsetDirection;
  
          // if the touch action did prevented once this session
          if (this.manager.session.prevented) {
              srcEvent.preventDefault();
              return;
          }
  
          var actions = this.actions;
          var hasNone = inStr(actions, TOUCH_ACTION_NONE) && !TOUCH_ACTION_MAP[TOUCH_ACTION_NONE];
          var hasPanY = inStr(actions, TOUCH_ACTION_PAN_Y) && !TOUCH_ACTION_MAP[TOUCH_ACTION_PAN_Y];
          var hasPanX = inStr(actions, TOUCH_ACTION_PAN_X) && !TOUCH_ACTION_MAP[TOUCH_ACTION_PAN_X];
  
          if (hasNone) {
              //do not prevent defaults if this is a tap gesture
  
              var isTapPointer = input.pointers.length === 1;
              var isTapMovement = input.distance < 2;
              var isTapTouchTime = input.deltaTime < 250;
  
              if (isTapPointer && isTapMovement && isTapTouchTime) {
                  return;
              }
          }
  
          if (hasPanX && hasPanY) {
              // `pan-x pan-y` means browser handles all scrolling/panning, do not prevent
              return;
          }
  
          if (hasNone ||
              (hasPanY && direction & DIRECTION_HORIZONTAL) ||
              (hasPanX && direction & DIRECTION_VERTICAL)) {
              return this.preventSrc(srcEvent);
          }
      },
  
      /**
       * call preventDefault to prevent the browser's default behavior (scrolling in most cases)
       * @param {Object} srcEvent
       */
      preventSrc: function(srcEvent) {
          this.manager.session.prevented = true;
          srcEvent.preventDefault();
      }
  };
  
  /**
   * when the touchActions are collected they are not a valid value, so we need to clean things up. *
   * @param {String} actions
   * @returns {*}
   */
  function cleanTouchActions(actions) {
      // none
      if (inStr(actions, TOUCH_ACTION_NONE)) {
          return TOUCH_ACTION_NONE;
      }
  
      var hasPanX = inStr(actions, TOUCH_ACTION_PAN_X);
      var hasPanY = inStr(actions, TOUCH_ACTION_PAN_Y);
  
      // if both pan-x and pan-y are set (different recognizers
      // for different directions, e.g. horizontal pan but vertical swipe?)
      // we need none (as otherwise with pan-x pan-y combined none of these
      // recognizers will work, since the browser would handle all panning
      if (hasPanX && hasPanY) {
          return TOUCH_ACTION_NONE;
      }
  
      // pan-x OR pan-y
      if (hasPanX || hasPanY) {
          return hasPanX ? TOUCH_ACTION_PAN_X : TOUCH_ACTION_PAN_Y;
      }
  
      // manipulation
      if (inStr(actions, TOUCH_ACTION_MANIPULATION)) {
          return TOUCH_ACTION_MANIPULATION;
      }
  
      return TOUCH_ACTION_AUTO;
  }
  
  function getTouchActionProps() {
      if (!NATIVE_TOUCH_ACTION) {
          return false;
      }
      var touchMap = {};
      var cssSupports = window.CSS && window.CSS.supports;
      ['auto', 'manipulation', 'pan-y', 'pan-x', 'pan-x pan-y', 'none'].forEach(function(val) {
  
          // If css.supports is not supported but there is native touch-action assume it supports
          // all values. This is the case for IE 10 and 11.
          touchMap[val] = cssSupports ? window.CSS.supports('touch-action', val) : true;
      });
      return touchMap;
  }
  
  /**
   * Recognizer flow explained; *
   * All recognizers have the initial state of POSSIBLE when a input session starts.
   * The definition of a input session is from the first input until the last input, with all it's movement in it. *
   * Example session for mouse-input: mousedown -> mousemove -> mouseup
   *
   * On each recognizing cycle (see Manager.recognize) the .recognize() method is executed
   * which determines with state it should be.
   *
   * If the recognizer has the state FAILED, CANCELLED or RECOGNIZED (equals ENDED), it is reset to
   * POSSIBLE to give it another change on the next cycle.
   *
   *               Possible
   *                  |
   *            +-----+---------------+
   *            |                     |
   *      +-----+-----+               |
   *      |           |               |
   *   Failed      Cancelled          |
   *                          +-------+------+
   *                          |              |
   *                      Recognized       Began
   *                                         |
   *                                      Changed
   *                                         |
   *                                  Ended/Recognized
   */
  var STATE_POSSIBLE = 1;
  var STATE_BEGAN = 2;
  var STATE_CHANGED = 4;
  var STATE_ENDED = 8;
  var STATE_RECOGNIZED = STATE_ENDED;
  var STATE_CANCELLED = 16;
  var STATE_FAILED = 32;
  
  /**
   * Recognizer
   * Every recognizer needs to extend from this class.
   * @constructor
   * @param {Object} options
   */
  function Recognizer(options) {
      this.options = assign({}, this.defaults, options || {});
  
      this.id = uniqueId();
  
      this.manager = null;
  
      // default is enable true
      this.options.enable = ifUndefined(this.options.enable, true);
  
      this.state = STATE_POSSIBLE;
  
      this.simultaneous = {};
      this.requireFail = [];
  }
  
  Recognizer.prototype = {
      /**
       * @virtual
       * @type {Object}
       */
      defaults: {},
  
      /**
       * set options
       * @param {Object} options
       * @return {Recognizer}
       */
      set: function(options) {
          assign(this.options, options);
  
          // also update the touchAction, in case something changed about the directions/enabled state
          this.manager && this.manager.touchAction.update();
          return this;
      },
  
      /**
       * recognize simultaneous with an other recognizer.
       * @param {Recognizer} otherRecognizer
       * @returns {Recognizer} this
       */
      recognizeWith: function(otherRecognizer) {
          if (invokeArrayArg(otherRecognizer, 'recognizeWith', this)) {
              return this;
          }
  
          var simultaneous = this.simultaneous;
          otherRecognizer = getRecognizerByNameIfManager(otherRecognizer, this);
          if (!simultaneous[otherRecognizer.id]) {
              simultaneous[otherRecognizer.id] = otherRecognizer;
              otherRecognizer.recognizeWith(this);
          }
          return this;
      },
  
      /**
       * drop the simultaneous link. it doesnt remove the link on the other recognizer.
       * @param {Recognizer} otherRecognizer
       * @returns {Recognizer} this
       */
      dropRecognizeWith: function(otherRecognizer) {
          if (invokeArrayArg(otherRecognizer, 'dropRecognizeWith', this)) {
              return this;
          }
  
          otherRecognizer = getRecognizerByNameIfManager(otherRecognizer, this);
          delete this.simultaneous[otherRecognizer.id];
          return this;
      },
  
      /**
       * recognizer can only run when an other is failing
       * @param {Recognizer} otherRecognizer
       * @returns {Recognizer} this
       */
      requireFailure: function(otherRecognizer) {
          if (invokeArrayArg(otherRecognizer, 'requireFailure', this)) {
              return this;
          }
  
          var requireFail = this.requireFail;
          otherRecognizer = getRecognizerByNameIfManager(otherRecognizer, this);
          if (inArray(requireFail, otherRecognizer) === -1) {
              requireFail.push(otherRecognizer);
              otherRecognizer.requireFailure(this);
          }
          return this;
      },
  
      /**
       * drop the requireFailure link. it does not remove the link on the other recognizer.
       * @param {Recognizer} otherRecognizer
       * @returns {Recognizer} this
       */
      dropRequireFailure: function(otherRecognizer) {
          if (invokeArrayArg(otherRecognizer, 'dropRequireFailure', this)) {
              return this;
          }
  
          otherRecognizer = getRecognizerByNameIfManager(otherRecognizer, this);
          var index = inArray(this.requireFail, otherRecognizer);
          if (index > -1) {
              this.requireFail.splice(index, 1);
          }
          return this;
      },
  
      /**
       * has require failures boolean
       * @returns {boolean}
       */
      hasRequireFailures: function() {
          return this.requireFail.length > 0;
      },
  
      /**
       * if the recognizer can recognize simultaneous with an other recognizer
       * @param {Recognizer} otherRecognizer
       * @returns {Boolean}
       */
      canRecognizeWith: function(otherRecognizer) {
          return !!this.simultaneous[otherRecognizer.id];
      },
  
      /**
       * You should use `tryEmit` instead of `emit` directly to check
       * that all the needed recognizers has failed before emitting.
       * @param {Object} input
       */
      emit: function(input) {
          var self = this;
          var state = this.state;
  
          function emit(event) {
              self.manager.emit(event, input);
          }
  
          // 'panstart' and 'panmove'
          if (state < STATE_ENDED) {
              emit(self.options.event + stateStr(state));
          }
  
          emit(self.options.event); // simple 'eventName' events
  
          if (input.additionalEvent) { // additional event(panleft, panright, pinchin, pinchout...)
              emit(input.additionalEvent);
          }
  
          // panend and pancancel
          if (state >= STATE_ENDED) {
              emit(self.options.event + stateStr(state));
          }
      },
  
      /**
       * Check that all the require failure recognizers has failed,
       * if true, it emits a gesture event,
       * otherwise, setup the state to FAILED.
       * @param {Object} input
       */
      tryEmit: function(input) {
          if (this.canEmit()) {
              return this.emit(input);
          }
          // it's failing anyway
          this.state = STATE_FAILED;
      },
  
      /**
       * can we emit?
       * @returns {boolean}
       */
      canEmit: function() {
          var i = 0;
          while (i < this.requireFail.length) {
              if (!(this.requireFail[i].state & (STATE_FAILED | STATE_POSSIBLE))) {
                  return false;
              }
              i++;
          }
          return true;
      },
  
      /**
       * update the recognizer
       * @param {Object} inputData
       */
      recognize: function(inputData) {
          // make a new copy of the inputData
          // so we can change the inputData without messing up the other recognizers
          var inputDataClone = assign({}, inputData);
  
          // is is enabled and allow recognizing?
          if (!boolOrFn(this.options.enable, [this, inputDataClone])) {
              this.reset();
              this.state = STATE_FAILED;
              return;
          }
  
          // reset when we've reached the end
          if (this.state & (STATE_RECOGNIZED | STATE_CANCELLED | STATE_FAILED)) {
              this.state = STATE_POSSIBLE;
          }
  
          this.state = this.process(inputDataClone);
  
          // the recognizer has recognized a gesture
          // so trigger an event
          if (this.state & (STATE_BEGAN | STATE_CHANGED | STATE_ENDED | STATE_CANCELLED)) {
              this.tryEmit(inputDataClone);
          }
      },
  
      /**
       * return the state of the recognizer
       * the actual recognizing happens in this method
       * @virtual
       * @param {Object} inputData
       * @returns {Const} STATE
       */
      process: function(inputData) { }, // jshint ignore:line
  
      /**
       * return the preferred touch-action
       * @virtual
       * @returns {Array}
       */
      getTouchAction: function() { },
  
      /**
       * called when the gesture isn't allowed to recognize
       * like when another is being recognized or it is disabled
       * @virtual
       */
      reset: function() { }
  };
  
  /**
   * get a usable string, used as event postfix
   * @param {Const} state
   * @returns {String} state
   */
  function stateStr(state) {
      if (state & STATE_CANCELLED) {
          return 'cancel';
      } else if (state & STATE_ENDED) {
          return 'end';
      } else if (state & STATE_CHANGED) {
          return 'move';
      } else if (state & STATE_BEGAN) {
          return 'start';
      }
      return '';
  }
  
  /**
   * direction cons to string
   * @param {Const} direction
   * @returns {String}
   */
  function directionStr(direction) {
      if (direction == DIRECTION_DOWN) {
          return 'down';
      } else if (direction == DIRECTION_UP) {
          return 'up';
      } else if (direction == DIRECTION_LEFT) {
          return 'left';
      } else if (direction == DIRECTION_RIGHT) {
          return 'right';
      }
      return '';
  }
  
  /**
   * get a recognizer by name if it is bound to a manager
   * @param {Recognizer|String} otherRecognizer
   * @param {Recognizer} recognizer
   * @returns {Recognizer}
   */
  function getRecognizerByNameIfManager(otherRecognizer, recognizer) {
      var manager = recognizer.manager;
      if (manager) {
          return manager.get(otherRecognizer);
      }
      return otherRecognizer;
  }
  
  /**
   * This recognizer is just used as a base for the simple attribute recognizers.
   * @constructor
   * @extends Recognizer
   */
  function AttrRecognizer() {
      Recognizer.apply(this, arguments);
  }
  
  inherit(AttrRecognizer, Recognizer, {
      /**
       * @namespace
       * @memberof AttrRecognizer
       */
      defaults: {
          /**
           * @type {Number}
           * @default 1
           */
          pointers: 1
      },
  
      /**
       * Used to check if it the recognizer receives valid input, like input.distance > 10.
       * @memberof AttrRecognizer
       * @param {Object} input
       * @returns {Boolean} recognized
       */
      attrTest: function(input) {
          var optionPointers = this.options.pointers;
          return optionPointers === 0 || input.pointers.length === optionPointers;
      },
  
      /**
       * Process the input and return the state for the recognizer
       * @memberof AttrRecognizer
       * @param {Object} input
       * @returns {*} State
       */
      process: function(input) {
          var state = this.state;
          var eventType = input.eventType;
  
          var isRecognized = state & (STATE_BEGAN | STATE_CHANGED);
          var isValid = this.attrTest(input);
  
          // on cancel input and we've recognized before, return STATE_CANCELLED
          if (isRecognized && (eventType & INPUT_CANCEL || !isValid)) {
              return state | STATE_CANCELLED;
          } else if (isRecognized || isValid) {
              if (eventType & INPUT_END) {
                  return state | STATE_ENDED;
              } else if (!(state & STATE_BEGAN)) {
                  return STATE_BEGAN;
              }
              return state | STATE_CHANGED;
          }
          return STATE_FAILED;
      }
  });
  
  /**
   * Pan
   * Recognized when the pointer is down and moved in the allowed direction.
   * @constructor
   * @extends AttrRecognizer
   */
  function PanRecognizer() {
      AttrRecognizer.apply(this, arguments);
  
      this.pX = null;
      this.pY = null;
  }
  
  inherit(PanRecognizer, AttrRecognizer, {
      /**
       * @namespace
       * @memberof PanRecognizer
       */
      defaults: {
          event: 'pan',
          threshold: 10,
          pointers: 1,
          direction: DIRECTION_ALL
      },
  
      getTouchAction: function() {
          var direction = this.options.direction;
          var actions = [];
          if (direction & DIRECTION_HORIZONTAL) {
              actions.push(TOUCH_ACTION_PAN_Y);
          }
          if (direction & DIRECTION_VERTICAL) {
              actions.push(TOUCH_ACTION_PAN_X);
          }
          return actions;
      },
  
      directionTest: function(input) {
          var options = this.options;
          var hasMoved = true;
          var distance = input.distance;
          var direction = input.direction;
          var x = input.deltaX;
          var y = input.deltaY;
  
          // lock to axis?
          if (!(direction & options.direction)) {
              if (options.direction & DIRECTION_HORIZONTAL) {
                  direction = (x === 0) ? DIRECTION_NONE : (x < 0) ? DIRECTION_LEFT : DIRECTION_RIGHT;
                  hasMoved = x != this.pX;
                  distance = Math.abs(input.deltaX);
              } else {
                  direction = (y === 0) ? DIRECTION_NONE : (y < 0) ? DIRECTION_UP : DIRECTION_DOWN;
                  hasMoved = y != this.pY;
                  distance = Math.abs(input.deltaY);
              }
          }
          input.direction = direction;
          return hasMoved && distance > options.threshold && direction & options.direction;
      },
  
      attrTest: function(input) {
          return AttrRecognizer.prototype.attrTest.call(this, input) &&
              (this.state & STATE_BEGAN || (!(this.state & STATE_BEGAN) && this.directionTest(input)));
      },
  
      emit: function(input) {
  
          this.pX = input.deltaX;
          this.pY = input.deltaY;
  
          var direction = directionStr(input.direction);
  
          if (direction) {
              input.additionalEvent = this.options.event + direction;
          }
          this._super.emit.call(this, input);
      }
  });
  
  /**
   * Pinch
   * Recognized when two or more pointers are moving toward (zoom-in) or away from each other (zoom-out).
   * @constructor
   * @extends AttrRecognizer
   */
  function PinchRecognizer() {
      AttrRecognizer.apply(this, arguments);
  }
  
  inherit(PinchRecognizer, AttrRecognizer, {
      /**
       * @namespace
       * @memberof PinchRecognizer
       */
      defaults: {
          event: 'pinch',
          threshold: 0,
          pointers: 2
      },
  
      getTouchAction: function() {
          return [TOUCH_ACTION_NONE];
      },
  
      attrTest: function(input) {
          return this._super.attrTest.call(this, input) &&
              (Math.abs(input.scale - 1) > this.options.threshold || this.state & STATE_BEGAN);
      },
  
      emit: function(input) {
          if (input.scale !== 1) {
              var inOut = input.scale < 1 ? 'in' : 'out';
              input.additionalEvent = this.options.event + inOut;
          }
          this._super.emit.call(this, input);
      }
  });
  
  /**
   * Press
   * Recognized when the pointer is down for x ms without any movement.
   * @constructor
   * @extends Recognizer
   */
  function PressRecognizer() {
      Recognizer.apply(this, arguments);
  
      this._timer = null;
      this._input = null;
  }
  
  inherit(PressRecognizer, Recognizer, {
      /**
       * @namespace
       * @memberof PressRecognizer
       */
      defaults: {
          event: 'press',
          pointers: 1,
          time: 251, // minimal time of the pointer to be pressed
          threshold: 9 // a minimal movement is ok, but keep it low
      },
  
      getTouchAction: function() {
          return [TOUCH_ACTION_AUTO];
      },
  
      process: function(input) {
          var options = this.options;
          var validPointers = input.pointers.length === options.pointers;
          var validMovement = input.distance < options.threshold;
          var validTime = input.deltaTime > options.time;
  
          this._input = input;
  
          // we only allow little movement
          // and we've reached an end event, so a tap is possible
          if (!validMovement || !validPointers || (input.eventType & (INPUT_END | INPUT_CANCEL) && !validTime)) {
              this.reset();
          } else if (input.eventType & INPUT_START) {
              this.reset();
              this._timer = setTimeoutContext(function() {
                  this.state = STATE_RECOGNIZED;
                  this.tryEmit();
              }, options.time, this);
          } else if (input.eventType & INPUT_END) {
              return STATE_RECOGNIZED;
          }
          return STATE_FAILED;
      },
  
      reset: function() {
          clearTimeout(this._timer);
      },
  
      emit: function(input) {
          if (this.state !== STATE_RECOGNIZED) {
              return;
          }
  
          if (input && (input.eventType & INPUT_END)) {
              this.manager.emit(this.options.event + 'up', input);
          } else {
              this._input.timeStamp = now();
              this.manager.emit(this.options.event, this._input);
          }
      }
  });
  
  /**
   * Rotate
   * Recognized when two or more pointer are moving in a circular motion.
   * @constructor
   * @extends AttrRecognizer
   */
  function RotateRecognizer() {
      AttrRecognizer.apply(this, arguments);
  }
  
  inherit(RotateRecognizer, AttrRecognizer, {
      /**
       * @namespace
       * @memberof RotateRecognizer
       */
      defaults: {
          event: 'rotate',
          threshold: 0,
          pointers: 2
      },
  
      getTouchAction: function() {
          return [TOUCH_ACTION_NONE];
      },
  
      attrTest: function(input) {
          return this._super.attrTest.call(this, input) &&
              (Math.abs(input.rotation) > this.options.threshold || this.state & STATE_BEGAN);
      }
  });
  
  /**
   * Swipe
   * Recognized when the pointer is moving fast (velocity), with enough distance in the allowed direction.
   * @constructor
   * @extends AttrRecognizer
   */
  function SwipeRecognizer() {
      AttrRecognizer.apply(this, arguments);
  }
  
  inherit(SwipeRecognizer, AttrRecognizer, {
      /**
       * @namespace
       * @memberof SwipeRecognizer
       */
      defaults: {
          event: 'swipe',
          threshold: 10,
          velocity: 0.3,
          direction: DIRECTION_HORIZONTAL | DIRECTION_VERTICAL,
          pointers: 1
      },
  
      getTouchAction: function() {
          return PanRecognizer.prototype.getTouchAction.call(this);
      },
  
      attrTest: function(input) {
          var direction = this.options.direction;
          var velocity;
  
          if (direction & (DIRECTION_HORIZONTAL | DIRECTION_VERTICAL)) {
              velocity = input.overallVelocity;
          } else if (direction & DIRECTION_HORIZONTAL) {
              velocity = input.overallVelocityX;
          } else if (direction & DIRECTION_VERTICAL) {
              velocity = input.overallVelocityY;
          }
  
          return this._super.attrTest.call(this, input) &&
              direction & input.offsetDirection &&
              input.distance > this.options.threshold &&
              input.maxPointers == this.options.pointers &&
              abs(velocity) > this.options.velocity && input.eventType & INPUT_END;
      },
  
      emit: function(input) {
          var direction = directionStr(input.offsetDirection);
          if (direction) {
              this.manager.emit(this.options.event + direction, input);
          }
  
          this.manager.emit(this.options.event, input);
      }
  });
  
  /**
   * A tap is ecognized when the pointer is doing a small tap/click. Multiple taps are recognized if they occur
   * between the given interval and position. The delay option can be used to recognize multi-taps without firing
   * a single tap.
   *
   * The eventData from the emitted event contains the property `tapCount`, which contains the amount of
   * multi-taps being recognized.
   * @constructor
   * @extends Recognizer
   */
  function TapRecognizer() {
      Recognizer.apply(this, arguments);
  
      // previous time and center,
      // used for tap counting
      this.pTime = false;
      this.pCenter = false;
  
      this._timer = null;
      this._input = null;
      this.count = 0;
  }
  
  inherit(TapRecognizer, Recognizer, {
      /**
       * @namespace
       * @memberof PinchRecognizer
       */
      defaults: {
          event: 'tap',
          pointers: 1,
          taps: 1,
          interval: 300, // max time between the multi-tap taps
          time: 250, // max time of the pointer to be down (like finger on the screen)
          threshold: 9, // a minimal movement is ok, but keep it low
          posThreshold: 10 // a multi-tap can be a bit off the initial position
      },
  
      getTouchAction: function() {
          return [TOUCH_ACTION_MANIPULATION];
      },
  
      process: function(input) {
          var options = this.options;
  
          var validPointers = input.pointers.length === options.pointers;
          var validMovement = input.distance < options.threshold;
          var validTouchTime = input.deltaTime < options.time;
  
          this.reset();
  
          if ((input.eventType & INPUT_START) && (this.count === 0)) {
              return this.failTimeout();
          }
  
          // we only allow little movement
          // and we've reached an end event, so a tap is possible
          if (validMovement && validTouchTime && validPointers) {
              if (input.eventType != INPUT_END) {
                  return this.failTimeout();
              }
  
              var validInterval = this.pTime ? (input.timeStamp - this.pTime < options.interval) : true;
              var validMultiTap = !this.pCenter || getDistance(this.pCenter, input.center) < options.posThreshold;
  
              this.pTime = input.timeStamp;
              this.pCenter = input.center;
  
              if (!validMultiTap || !validInterval) {
                  this.count = 1;
              } else {
                  this.count += 1;
              }
  
              this._input = input;
  
              // if tap count matches we have recognized it,
              // else it has began recognizing...
              var tapCount = this.count % options.taps;
              if (tapCount === 0) {
                  // no failing requirements, immediately trigger the tap event
                  // or wait as long as the multitap interval to trigger
                  if (!this.hasRequireFailures()) {
                      return STATE_RECOGNIZED;
                  } else {
                      this._timer = setTimeoutContext(function() {
                          this.state = STATE_RECOGNIZED;
                          this.tryEmit();
                      }, options.interval, this);
                      return STATE_BEGAN;
                  }
              }
          }
          return STATE_FAILED;
      },
  
      failTimeout: function() {
          this._timer = setTimeoutContext(function() {
              this.state = STATE_FAILED;
          }, this.options.interval, this);
          return STATE_FAILED;
      },
  
      reset: function() {
          clearTimeout(this._timer);
      },
  
      emit: function() {
          if (this.state == STATE_RECOGNIZED) {
              this._input.tapCount = this.count;
              this.manager.emit(this.options.event, this._input);
          }
      }
  });
  
  /**
   * Simple way to create a manager with a default set of recognizers.
   * @param {HTMLElement} element
   * @param {Object} [options]
   * @constructor
   */
  function Hammer(element, options) {
      options = options || {};
      options.recognizers = ifUndefined(options.recognizers, Hammer.defaults.preset);
      return new Manager(element, options);
  }
  
  /**
   * @const {string}
   */
  Hammer.VERSION = '2.0.8';
  
  /**
   * default settings
   * @namespace
   */
  Hammer.defaults = {
      /**
       * set if DOM events are being triggered.
       * But this is slower and unused by simple implementations, so disabled by default.
       * @type {Boolean}
       * @default false
       */
      domEvents: false,
  
      /**
       * The value for the touchAction property/fallback.
       * When set to `compute` it will magically set the correct value based on the added recognizers.
       * @type {String}
       * @default compute
       */
      touchAction: TOUCH_ACTION_COMPUTE,
  
      /**
       * @type {Boolean}
       * @default true
       */
      enable: true,
  
      /**
       * EXPERIMENTAL FEATURE -- can be removed/changed
       * Change the parent input target element.
       * If Null, then it is being set the to main element.
       * @type {Null|EventTarget}
       * @default null
       */
      inputTarget: null,
  
      /**
       * force an input class
       * @type {Null|Function}
       * @default null
       */
      inputClass: null,
  
      /**
       * Default recognizer setup when calling `Hammer()`
       * When creating a new Manager these will be skipped.
       * @type {Array}
       */
      preset: [
          // RecognizerClass, options, [recognizeWith, ...], [requireFailure, ...]
          [RotateRecognizer, {enable: false}],
          [PinchRecognizer, {enable: false}, ['rotate']],
          [SwipeRecognizer, {direction: DIRECTION_HORIZONTAL}],
          [PanRecognizer, {direction: DIRECTION_HORIZONTAL}, ['swipe']],
          [TapRecognizer],
          [TapRecognizer, {event: 'doubletap', taps: 2}, ['tap']],
          [PressRecognizer]
      ],
  
      /**
       * Some CSS properties can be used to improve the working of Hammer.
       * Add them to this method and they will be set when creating a new Manager.
       * @namespace
       */
      cssProps: {
          /**
           * Disables text selection to improve the dragging gesture. Mainly for desktop browsers.
           * @type {String}
           * @default 'none'
           */
          userSelect: 'none',
  
          /**
           * Disable the Windows Phone grippers when pressing an element.
           * @type {String}
           * @default 'none'
           */
          touchSelect: 'none',
  
          /**
           * Disables the default callout shown when you touch and hold a touch target.
           * On iOS, when you touch and hold a touch target such as a link, Safari displays
           * a callout containing information about the link. This property allows you to disable that callout.
           * @type {String}
           * @default 'none'
           */
          touchCallout: 'none',
  
          /**
           * Specifies whether zooming is enabled. Used by IE10>
           * @type {String}
           * @default 'none'
           */
          contentZooming: 'none',
  
          /**
           * Specifies that an entire element should be draggable instead of its contents. Mainly for desktop browsers.
           * @type {String}
           * @default 'none'
           */
          userDrag: 'none',
  
          /**
           * Overrides the highlight color shown when the user taps a link or a JavaScript
           * clickable element in iOS. This property obeys the alpha value, if specified.
           * @type {String}
           * @default 'rgba(0,0,0,0)'
           */
          tapHighlightColor: 'rgba(0,0,0,0)'
      }
  };
  
  var STOP = 1;
  var FORCED_STOP = 2;
  
  /**
   * Manager
   * @param {HTMLElement} element
   * @param {Object} [options]
   * @constructor
   */
  function Manager(element, options) {
      this.options = assign({}, Hammer.defaults, options || {});
  
      this.options.inputTarget = this.options.inputTarget || element;
  
      this.handlers = {};
      this.session = {};
      this.recognizers = [];
      this.oldCssProps = {};
  
      this.element = element;
      this.input = createInputInstance(this);
      this.touchAction = new TouchAction(this, this.options.touchAction);
  
      toggleCssProps(this, true);
  
      each(this.options.recognizers, function(item) {
          var recognizer = this.add(new (item[0])(item[1]));
          item[2] && recognizer.recognizeWith(item[2]);
          item[3] && recognizer.requireFailure(item[3]);
      }, this);
  }
  
  Manager.prototype = {
      /**
       * set options
       * @param {Object} options
       * @returns {Manager}
       */
      set: function(options) {
          assign(this.options, options);
  
          // Options that need a little more setup
          if (options.touchAction) {
              this.touchAction.update();
          }
          if (options.inputTarget) {
              // Clean up existing event listeners and reinitialize
              this.input.destroy();
              this.input.target = options.inputTarget;
              this.input.init();
          }
          return this;
      },
  
      /**
       * stop recognizing for this session.
       * This session will be discarded, when a new [input]start event is fired.
       * When forced, the recognizer cycle is stopped immediately.
       * @param {Boolean} [force]
       */
      stop: function(force) {
          this.session.stopped = force ? FORCED_STOP : STOP;
      },
  
      /**
       * run the recognizers!
       * called by the inputHandler function on every movement of the pointers (touches)
       * it walks through all the recognizers and tries to detect the gesture that is being made
       * @param {Object} inputData
       */
      recognize: function(inputData) {
          var session = this.session;
          if (session.stopped) {
              return;
          }
  
          // run the touch-action polyfill
          this.touchAction.preventDefaults(inputData);
  
          var recognizer;
          var recognizers = this.recognizers;
  
          // this holds the recognizer that is being recognized.
          // so the recognizer's state needs to be BEGAN, CHANGED, ENDED or RECOGNIZED
          // if no recognizer is detecting a thing, it is set to `null`
          var curRecognizer = session.curRecognizer;
  
          // reset when the last recognizer is recognized
          // or when we're in a new session
          if (!curRecognizer || (curRecognizer && curRecognizer.state & STATE_RECOGNIZED)) {
              curRecognizer = session.curRecognizer = null;
          }
  
          var i = 0;
          while (i < recognizers.length) {
              recognizer = recognizers[i];
  
              // find out if we are allowed try to recognize the input for this one.
              // 1.   allow if the session is NOT forced stopped (see the .stop() method)
              // 2.   allow if we still haven't recognized a gesture in this session, or the this recognizer is the one
              //      that is being recognized.
              // 3.   allow if the recognizer is allowed to run simultaneous with the current recognized recognizer.
              //      this can be setup with the `recognizeWith()` method on the recognizer.
              if (session.stopped !== FORCED_STOP && ( // 1
                      !curRecognizer || recognizer == curRecognizer || // 2
                      recognizer.canRecognizeWith(curRecognizer))) { // 3
                  recognizer.recognize(inputData);
              } else {
                  recognizer.reset();
              }
  
              // if the recognizer has been recognizing the input as a valid gesture, we want to store this one as the
              // current active recognizer. but only if we don't already have an active recognizer
              if (!curRecognizer && recognizer.state & (STATE_BEGAN | STATE_CHANGED | STATE_ENDED)) {
                  curRecognizer = session.curRecognizer = recognizer;
              }
              i++;
          }
      },
  
      /**
       * get a recognizer by its event name.
       * @param {Recognizer|String} recognizer
       * @returns {Recognizer|Null}
       */
      get: function(recognizer) {
          if (recognizer instanceof Recognizer) {
              return recognizer;
          }
  
          var recognizers = this.recognizers;
          for (var i = 0; i < recognizers.length; i++) {
              if (recognizers[i].options.event == recognizer) {
                  return recognizers[i];
              }
          }
          return null;
      },
  
      /**
       * add a recognizer to the manager
       * existing recognizers with the same event name will be removed
       * @param {Recognizer} recognizer
       * @returns {Recognizer|Manager}
       */
      add: function(recognizer) {
          if (invokeArrayArg(recognizer, 'add', this)) {
              return this;
          }
  
          // remove existing
          var existing = this.get(recognizer.options.event);
          if (existing) {
              this.remove(existing);
          }
  
          this.recognizers.push(recognizer);
          recognizer.manager = this;
  
          this.touchAction.update();
          return recognizer;
      },
  
      /**
       * remove a recognizer by name or instance
       * @param {Recognizer|String} recognizer
       * @returns {Manager}
       */
      remove: function(recognizer) {
          if (invokeArrayArg(recognizer, 'remove', this)) {
              return this;
          }
  
          recognizer = this.get(recognizer);
  
          // let's make sure this recognizer exists
          if (recognizer) {
              var recognizers = this.recognizers;
              var index = inArray(recognizers, recognizer);
  
              if (index !== -1) {
                  recognizers.splice(index, 1);
                  this.touchAction.update();
              }
          }
  
          return this;
      },
  
      /**
       * bind event
       * @param {String} events
       * @param {Function} handler
       * @returns {EventEmitter} this
       */
      on: function(events, handler) {
          if (events === undefined) {
              return;
          }
          if (handler === undefined) {
              return;
          }
  
          var handlers = this.handlers;
          each(splitStr(events), function(event) {
              handlers[event] = handlers[event] || [];
              handlers[event].push(handler);
          });
          return this;
      },
  
      /**
       * unbind event, leave emit blank to remove all handlers
       * @param {String} events
       * @param {Function} [handler]
       * @returns {EventEmitter} this
       */
      off: function(events, handler) {
          if (events === undefined) {
              return;
          }
  
          var handlers = this.handlers;
          each(splitStr(events), function(event) {
              if (!handler) {
                  delete handlers[event];
              } else {
                  handlers[event] && handlers[event].splice(inArray(handlers[event], handler), 1);
              }
          });
          return this;
      },
  
      /**
       * emit event to the listeners
       * @param {String} event
       * @param {Object} data
       */
      emit: function(event, data) {
          // we also want to trigger dom events
          if (this.options.domEvents) {
              triggerDomEvent(event, data);
          }
  
          // no handlers, so skip it all
          var handlers = this.handlers[event] && this.handlers[event].slice();
          if (!handlers || !handlers.length) {
              return;
          }
  
          data.type = event;
          data.preventDefault = function() {
              data.srcEvent.preventDefault();
          };
  
          var i = 0;
          while (i < handlers.length) {
              handlers[i](data);
              i++;
          }
      },
  
      /**
       * destroy the manager and unbinds all events
       * it doesn't unbind dom events, that is the user own responsibility
       */
      destroy: function() {
          this.element && toggleCssProps(this, false);
  
          this.handlers = {};
          this.session = {};
          this.input.destroy();
          this.element = null;
      }
  };
  
  /**
   * add/remove the css properties as defined in manager.options.cssProps
   * @param {Manager} manager
   * @param {Boolean} add
   */
  function toggleCssProps(manager, add) {
      var element = manager.element;
      if (!element.style) {
          return;
      }
      var prop;
      each(manager.options.cssProps, function(value, name) {
          prop = prefixed(element.style, name);
          if (add) {
              manager.oldCssProps[prop] = element.style[prop];
              element.style[prop] = value;
          } else {
              element.style[prop] = manager.oldCssProps[prop] || '';
          }
      });
      if (!add) {
          manager.oldCssProps = {};
      }
  }
  
  /**
   * trigger dom event
   * @param {String} event
   * @param {Object} data
   */
  function triggerDomEvent(event, data) {
      var gestureEvent = document.createEvent('Event');
      gestureEvent.initEvent(event, true, true);
      gestureEvent.gesture = data;
      data.target.dispatchEvent(gestureEvent);
  }
  
  assign(Hammer, {
      INPUT_START: INPUT_START,
      INPUT_MOVE: INPUT_MOVE,
      INPUT_END: INPUT_END,
      INPUT_CANCEL: INPUT_CANCEL,
  
      STATE_POSSIBLE: STATE_POSSIBLE,
      STATE_BEGAN: STATE_BEGAN,
      STATE_CHANGED: STATE_CHANGED,
      STATE_ENDED: STATE_ENDED,
      STATE_RECOGNIZED: STATE_RECOGNIZED,
      STATE_CANCELLED: STATE_CANCELLED,
      STATE_FAILED: STATE_FAILED,
  
      DIRECTION_NONE: DIRECTION_NONE,
      DIRECTION_LEFT: DIRECTION_LEFT,
      DIRECTION_RIGHT: DIRECTION_RIGHT,
      DIRECTION_UP: DIRECTION_UP,
      DIRECTION_DOWN: DIRECTION_DOWN,
      DIRECTION_HORIZONTAL: DIRECTION_HORIZONTAL,
      DIRECTION_VERTICAL: DIRECTION_VERTICAL,
      DIRECTION_ALL: DIRECTION_ALL,
  
      Manager: Manager,
      Input: Input,
      TouchAction: TouchAction,
  
      TouchInput: TouchInput,
      MouseInput: MouseInput,
      PointerEventInput: PointerEventInput,
      TouchMouseInput: TouchMouseInput,
      SingleTouchInput: SingleTouchInput,
  
      Recognizer: Recognizer,
      AttrRecognizer: AttrRecognizer,
      Tap: TapRecognizer,
      Pan: PanRecognizer,
      Swipe: SwipeRecognizer,
      Pinch: PinchRecognizer,
      Rotate: RotateRecognizer,
      Press: PressRecognizer,
  
      on: addEventListeners,
      off: removeEventListeners,
      each: each,
      merge: merge,
      extend: extend,
      assign: assign,
      inherit: inherit,
      bindFn: bindFn,
      prefixed: prefixed
  });
  
  // this prevents errors when Hammer is loaded in the presence of an AMD
  //  style loader but by script tag, not by the loader.
  var freeGlobal = (typeof window !== 'undefined' ? window : (typeof self !== 'undefined' ? self : {})); // jshint ignore:line
  freeGlobal.Hammer = Hammer;
  
  if (typeof define === 'function' && define.amd) {
      define(function() {
          return Hammer;
      });
  } else if (typeof module != 'undefined' && module.exports) {
      module.exports = Hammer;
  } else {
      window[exportName] = Hammer;
  }
  
  })(window, document, 'Hammer');
/*! jQuery v2.2.4 | (c) jQuery Foundation | jquery.org/license */
!function(a,b){"object"==typeof module&&"object"==typeof module.exports?module.exports=a.document?b(a,!0):function(a){if(!a.document)throw new Error("jQuery requires a window with a document");return b(a)}:b(a)}("undefined"!=typeof window?window:this,function(a,b){var c=[],d=a.document,e=c.slice,f=c.concat,g=c.push,h=c.indexOf,i={},j=i.toString,k=i.hasOwnProperty,l={},m="2.2.4",n=function(a,b){return new n.fn.init(a,b)},o=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,p=/^-ms-/,q=/-([\da-z])/gi,r=function(a,b){return b.toUpperCase()};n.fn=n.prototype={jquery:m,constructor:n,selector:"",length:0,toArray:function(){return e.call(this)},get:function(a){return null!=a?0>a?this[a+this.length]:this[a]:e.call(this)},pushStack:function(a){var b=n.merge(this.constructor(),a);return b.prevObject=this,b.context=this.context,b},each:function(a){return n.each(this,a)},map:function(a){return this.pushStack(n.map(this,function(b,c){return a.call(b,c,b)}))},slice:function(){return this.pushStack(e.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(a){var b=this.length,c=+a+(0>a?b:0);return this.pushStack(c>=0&&b>c?[this[c]]:[])},end:function(){return this.prevObject||this.constructor()},push:g,sort:c.sort,splice:c.splice},n.extend=n.fn.extend=function(){var a,b,c,d,e,f,g=arguments[0]||{},h=1,i=arguments.length,j=!1;for("boolean"==typeof g&&(j=g,g=arguments[h]||{},h++),"object"==typeof g||n.isFunction(g)||(g={}),h===i&&(g=this,h--);i>h;h++)if(null!=(a=arguments[h]))for(b in a)c=g[b],d=a[b],g!==d&&(j&&d&&(n.isPlainObject(d)||(e=n.isArray(d)))?(e?(e=!1,f=c&&n.isArray(c)?c:[]):f=c&&n.isPlainObject(c)?c:{},g[b]=n.extend(j,f,d)):void 0!==d&&(g[b]=d));return g},n.extend({expando:"jQuery"+(m+Math.random()).replace(/\D/g,""),isReady:!0,error:function(a){throw new Error(a)},noop:function(){},isFunction:function(a){return"function"===n.type(a)},isArray:Array.isArray,isWindow:function(a){return null!=a&&a===a.window},isNumeric:function(a){var b=a&&a.toString();return!n.isArray(a)&&b-parseFloat(b)+1>=0},isPlainObject:function(a){var b;if("object"!==n.type(a)||a.nodeType||n.isWindow(a))return!1;if(a.constructor&&!k.call(a,"constructor")&&!k.call(a.constructor.prototype||{},"isPrototypeOf"))return!1;for(b in a);return void 0===b||k.call(a,b)},isEmptyObject:function(a){var b;for(b in a)return!1;return!0},type:function(a){return null==a?a+"":"object"==typeof a||"function"==typeof a?i[j.call(a)]||"object":typeof a},globalEval:function(a){var b,c=eval;a=n.trim(a),a&&(1===a.indexOf("use strict")?(b=d.createElement("script"),b.text=a,d.head.appendChild(b).parentNode.removeChild(b)):c(a))},camelCase:function(a){return a.replace(p,"ms-").replace(q,r)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toLowerCase()===b.toLowerCase()},each:function(a,b){var c,d=0;if(s(a)){for(c=a.length;c>d;d++)if(b.call(a[d],d,a[d])===!1)break}else for(d in a)if(b.call(a[d],d,a[d])===!1)break;return a},trim:function(a){return null==a?"":(a+"").replace(o,"")},makeArray:function(a,b){var c=b||[];return null!=a&&(s(Object(a))?n.merge(c,"string"==typeof a?[a]:a):g.call(c,a)),c},inArray:function(a,b,c){return null==b?-1:h.call(b,a,c)},merge:function(a,b){for(var c=+b.length,d=0,e=a.length;c>d;d++)a[e++]=b[d];return a.length=e,a},grep:function(a,b,c){for(var d,e=[],f=0,g=a.length,h=!c;g>f;f++)d=!b(a[f],f),d!==h&&e.push(a[f]);return e},map:function(a,b,c){var d,e,g=0,h=[];if(s(a))for(d=a.length;d>g;g++)e=b(a[g],g,c),null!=e&&h.push(e);else for(g in a)e=b(a[g],g,c),null!=e&&h.push(e);return f.apply([],h)},guid:1,proxy:function(a,b){var c,d,f;return"string"==typeof b&&(c=a[b],b=a,a=c),n.isFunction(a)?(d=e.call(arguments,2),f=function(){return a.apply(b||this,d.concat(e.call(arguments)))},f.guid=a.guid=a.guid||n.guid++,f):void 0},now:Date.now,support:l}),"function"==typeof Symbol&&(n.fn[Symbol.iterator]=c[Symbol.iterator]),n.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(a,b){i["[object "+b+"]"]=b.toLowerCase()});function s(a){var b=!!a&&"length"in a&&a.length,c=n.type(a);return"function"===c||n.isWindow(a)?!1:"array"===c||0===b||"number"==typeof b&&b>0&&b-1 in a}var t=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u="sizzle"+1*new Date,v=a.document,w=0,x=0,y=ga(),z=ga(),A=ga(),B=function(a,b){return a===b&&(l=!0),0},C=1<<31,D={}.hasOwnProperty,E=[],F=E.pop,G=E.push,H=E.push,I=E.slice,J=function(a,b){for(var c=0,d=a.length;d>c;c++)if(a[c]===b)return c;return-1},K="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",L="[\\x20\\t\\r\\n\\f]",M="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",N="\\["+L+"*("+M+")(?:"+L+"*([*^$|!~]?=)"+L+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+M+"))|)"+L+"*\\]",O=":("+M+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+N+")*)|.*)\\)|)",P=new RegExp(L+"+","g"),Q=new RegExp("^"+L+"+|((?:^|[^\\\\])(?:\\\\.)*)"+L+"+$","g"),R=new RegExp("^"+L+"*,"+L+"*"),S=new RegExp("^"+L+"*([>+~]|"+L+")"+L+"*"),T=new RegExp("="+L+"*([^\\]'\"]*?)"+L+"*\\]","g"),U=new RegExp(O),V=new RegExp("^"+M+"$"),W={ID:new RegExp("^#("+M+")"),CLASS:new RegExp("^\\.("+M+")"),TAG:new RegExp("^("+M+"|[*])"),ATTR:new RegExp("^"+N),PSEUDO:new RegExp("^"+O),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+L+"*(even|odd|(([+-]|)(\\d*)n|)"+L+"*(?:([+-]|)"+L+"*(\\d+)|))"+L+"*\\)|)","i"),bool:new RegExp("^(?:"+K+")$","i"),needsContext:new RegExp("^"+L+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+L+"*((?:-\\d)?\\d*)"+L+"*\\)|)(?=[^-]|$)","i")},X=/^(?:input|select|textarea|button)$/i,Y=/^h\d$/i,Z=/^[^{]+\{\s*\[native \w/,$=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,_=/[+~]/,aa=/'|\\/g,ba=new RegExp("\\\\([\\da-f]{1,6}"+L+"?|("+L+")|.)","ig"),ca=function(a,b,c){var d="0x"+b-65536;return d!==d||c?b:0>d?String.fromCharCode(d+65536):String.fromCharCode(d>>10|55296,1023&d|56320)},da=function(){m()};try{H.apply(E=I.call(v.childNodes),v.childNodes),E[v.childNodes.length].nodeType}catch(ea){H={apply:E.length?function(a,b){G.apply(a,I.call(b))}:function(a,b){var c=a.length,d=0;while(a[c++]=b[d++]);a.length=c-1}}}function fa(a,b,d,e){var f,h,j,k,l,o,r,s,w=b&&b.ownerDocument,x=b?b.nodeType:9;if(d=d||[],"string"!=typeof a||!a||1!==x&&9!==x&&11!==x)return d;if(!e&&((b?b.ownerDocument||b:v)!==n&&m(b),b=b||n,p)){if(11!==x&&(o=$.exec(a)))if(f=o[1]){if(9===x){if(!(j=b.getElementById(f)))return d;if(j.id===f)return d.push(j),d}else if(w&&(j=w.getElementById(f))&&t(b,j)&&j.id===f)return d.push(j),d}else{if(o[2])return H.apply(d,b.getElementsByTagName(a)),d;if((f=o[3])&&c.getElementsByClassName&&b.getElementsByClassName)return H.apply(d,b.getElementsByClassName(f)),d}if(c.qsa&&!A[a+" "]&&(!q||!q.test(a))){if(1!==x)w=b,s=a;else if("object"!==b.nodeName.toLowerCase()){(k=b.getAttribute("id"))?k=k.replace(aa,"\\$&"):b.setAttribute("id",k=u),r=g(a),h=r.length,l=V.test(k)?"#"+k:"[id='"+k+"']";while(h--)r[h]=l+" "+qa(r[h]);s=r.join(","),w=_.test(a)&&oa(b.parentNode)||b}if(s)try{return H.apply(d,w.querySelectorAll(s)),d}catch(y){}finally{k===u&&b.removeAttribute("id")}}}return i(a.replace(Q,"$1"),b,d,e)}function ga(){var a=[];function b(c,e){return a.push(c+" ")>d.cacheLength&&delete b[a.shift()],b[c+" "]=e}return b}function ha(a){return a[u]=!0,a}function ia(a){var b=n.createElement("div");try{return!!a(b)}catch(c){return!1}finally{b.parentNode&&b.parentNode.removeChild(b),b=null}}function ja(a,b){var c=a.split("|"),e=c.length;while(e--)d.attrHandle[c[e]]=b}function ka(a,b){var c=b&&a,d=c&&1===a.nodeType&&1===b.nodeType&&(~b.sourceIndex||C)-(~a.sourceIndex||C);if(d)return d;if(c)while(c=c.nextSibling)if(c===b)return-1;return a?1:-1}function la(a){return function(b){var c=b.nodeName.toLowerCase();return"input"===c&&b.type===a}}function ma(a){return function(b){var c=b.nodeName.toLowerCase();return("input"===c||"button"===c)&&b.type===a}}function na(a){return ha(function(b){return b=+b,ha(function(c,d){var e,f=a([],c.length,b),g=f.length;while(g--)c[e=f[g]]&&(c[e]=!(d[e]=c[e]))})})}function oa(a){return a&&"undefined"!=typeof a.getElementsByTagName&&a}c=fa.support={},f=fa.isXML=function(a){var b=a&&(a.ownerDocument||a).documentElement;return b?"HTML"!==b.nodeName:!1},m=fa.setDocument=function(a){var b,e,g=a?a.ownerDocument||a:v;return g!==n&&9===g.nodeType&&g.documentElement?(n=g,o=n.documentElement,p=!f(n),(e=n.defaultView)&&e.top!==e&&(e.addEventListener?e.addEventListener("unload",da,!1):e.attachEvent&&e.attachEvent("onunload",da)),c.attributes=ia(function(a){return a.className="i",!a.getAttribute("className")}),c.getElementsByTagName=ia(function(a){return a.appendChild(n.createComment("")),!a.getElementsByTagName("*").length}),c.getElementsByClassName=Z.test(n.getElementsByClassName),c.getById=ia(function(a){return o.appendChild(a).id=u,!n.getElementsByName||!n.getElementsByName(u).length}),c.getById?(d.find.ID=function(a,b){if("undefined"!=typeof b.getElementById&&p){var c=b.getElementById(a);return c?[c]:[]}},d.filter.ID=function(a){var b=a.replace(ba,ca);return function(a){return a.getAttribute("id")===b}}):(delete d.find.ID,d.filter.ID=function(a){var b=a.replace(ba,ca);return function(a){var c="undefined"!=typeof a.getAttributeNode&&a.getAttributeNode("id");return c&&c.value===b}}),d.find.TAG=c.getElementsByTagName?function(a,b){return"undefined"!=typeof b.getElementsByTagName?b.getElementsByTagName(a):c.qsa?b.querySelectorAll(a):void 0}:function(a,b){var c,d=[],e=0,f=b.getElementsByTagName(a);if("*"===a){while(c=f[e++])1===c.nodeType&&d.push(c);return d}return f},d.find.CLASS=c.getElementsByClassName&&function(a,b){return"undefined"!=typeof b.getElementsByClassName&&p?b.getElementsByClassName(a):void 0},r=[],q=[],(c.qsa=Z.test(n.querySelectorAll))&&(ia(function(a){o.appendChild(a).innerHTML="<a id='"+u+"'></a><select id='"+u+"-\r\\' msallowcapture=''><option selected=''></option></select>",a.querySelectorAll("[msallowcapture^='']").length&&q.push("[*^$]="+L+"*(?:''|\"\")"),a.querySelectorAll("[selected]").length||q.push("\\["+L+"*(?:value|"+K+")"),a.querySelectorAll("[id~="+u+"-]").length||q.push("~="),a.querySelectorAll(":checked").length||q.push(":checked"),a.querySelectorAll("a#"+u+"+*").length||q.push(".#.+[+~]")}),ia(function(a){var b=n.createElement("input");b.setAttribute("type","hidden"),a.appendChild(b).setAttribute("name","D"),a.querySelectorAll("[name=d]").length&&q.push("name"+L+"*[*^$|!~]?="),a.querySelectorAll(":enabled").length||q.push(":enabled",":disabled"),a.querySelectorAll("*,:x"),q.push(",.*:")})),(c.matchesSelector=Z.test(s=o.matches||o.webkitMatchesSelector||o.mozMatchesSelector||o.oMatchesSelector||o.msMatchesSelector))&&ia(function(a){c.disconnectedMatch=s.call(a,"div"),s.call(a,"[s!='']:x"),r.push("!=",O)}),q=q.length&&new RegExp(q.join("|")),r=r.length&&new RegExp(r.join("|")),b=Z.test(o.compareDocumentPosition),t=b||Z.test(o.contains)?function(a,b){var c=9===a.nodeType?a.documentElement:a,d=b&&b.parentNode;return a===d||!(!d||1!==d.nodeType||!(c.contains?c.contains(d):a.compareDocumentPosition&&16&a.compareDocumentPosition(d)))}:function(a,b){if(b)while(b=b.parentNode)if(b===a)return!0;return!1},B=b?function(a,b){if(a===b)return l=!0,0;var d=!a.compareDocumentPosition-!b.compareDocumentPosition;return d?d:(d=(a.ownerDocument||a)===(b.ownerDocument||b)?a.compareDocumentPosition(b):1,1&d||!c.sortDetached&&b.compareDocumentPosition(a)===d?a===n||a.ownerDocument===v&&t(v,a)?-1:b===n||b.ownerDocument===v&&t(v,b)?1:k?J(k,a)-J(k,b):0:4&d?-1:1)}:function(a,b){if(a===b)return l=!0,0;var c,d=0,e=a.parentNode,f=b.parentNode,g=[a],h=[b];if(!e||!f)return a===n?-1:b===n?1:e?-1:f?1:k?J(k,a)-J(k,b):0;if(e===f)return ka(a,b);c=a;while(c=c.parentNode)g.unshift(c);c=b;while(c=c.parentNode)h.unshift(c);while(g[d]===h[d])d++;return d?ka(g[d],h[d]):g[d]===v?-1:h[d]===v?1:0},n):n},fa.matches=function(a,b){return fa(a,null,null,b)},fa.matchesSelector=function(a,b){if((a.ownerDocument||a)!==n&&m(a),b=b.replace(T,"='$1']"),c.matchesSelector&&p&&!A[b+" "]&&(!r||!r.test(b))&&(!q||!q.test(b)))try{var d=s.call(a,b);if(d||c.disconnectedMatch||a.document&&11!==a.document.nodeType)return d}catch(e){}return fa(b,n,null,[a]).length>0},fa.contains=function(a,b){return(a.ownerDocument||a)!==n&&m(a),t(a,b)},fa.attr=function(a,b){(a.ownerDocument||a)!==n&&m(a);var e=d.attrHandle[b.toLowerCase()],f=e&&D.call(d.attrHandle,b.toLowerCase())?e(a,b,!p):void 0;return void 0!==f?f:c.attributes||!p?a.getAttribute(b):(f=a.getAttributeNode(b))&&f.specified?f.value:null},fa.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)},fa.uniqueSort=function(a){var b,d=[],e=0,f=0;if(l=!c.detectDuplicates,k=!c.sortStable&&a.slice(0),a.sort(B),l){while(b=a[f++])b===a[f]&&(e=d.push(f));while(e--)a.splice(d[e],1)}return k=null,a},e=fa.getText=function(a){var b,c="",d=0,f=a.nodeType;if(f){if(1===f||9===f||11===f){if("string"==typeof a.textContent)return a.textContent;for(a=a.firstChild;a;a=a.nextSibling)c+=e(a)}else if(3===f||4===f)return a.nodeValue}else while(b=a[d++])c+=e(b);return c},d=fa.selectors={cacheLength:50,createPseudo:ha,match:W,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(a){return a[1]=a[1].replace(ba,ca),a[3]=(a[3]||a[4]||a[5]||"").replace(ba,ca),"~="===a[2]&&(a[3]=" "+a[3]+" "),a.slice(0,4)},CHILD:function(a){return a[1]=a[1].toLowerCase(),"nth"===a[1].slice(0,3)?(a[3]||fa.error(a[0]),a[4]=+(a[4]?a[5]+(a[6]||1):2*("even"===a[3]||"odd"===a[3])),a[5]=+(a[7]+a[8]||"odd"===a[3])):a[3]&&fa.error(a[0]),a},PSEUDO:function(a){var b,c=!a[6]&&a[2];return W.CHILD.test(a[0])?null:(a[3]?a[2]=a[4]||a[5]||"":c&&U.test(c)&&(b=g(c,!0))&&(b=c.indexOf(")",c.length-b)-c.length)&&(a[0]=a[0].slice(0,b),a[2]=c.slice(0,b)),a.slice(0,3))}},filter:{TAG:function(a){var b=a.replace(ba,ca).toLowerCase();return"*"===a?function(){return!0}:function(a){return a.nodeName&&a.nodeName.toLowerCase()===b}},CLASS:function(a){var b=y[a+" "];return b||(b=new RegExp("(^|"+L+")"+a+"("+L+"|$)"))&&y(a,function(a){return b.test("string"==typeof a.className&&a.className||"undefined"!=typeof a.getAttribute&&a.getAttribute("class")||"")})},ATTR:function(a,b,c){return function(d){var e=fa.attr(d,a);return null==e?"!="===b:b?(e+="","="===b?e===c:"!="===b?e!==c:"^="===b?c&&0===e.indexOf(c):"*="===b?c&&e.indexOf(c)>-1:"$="===b?c&&e.slice(-c.length)===c:"~="===b?(" "+e.replace(P," ")+" ").indexOf(c)>-1:"|="===b?e===c||e.slice(0,c.length+1)===c+"-":!1):!0}},CHILD:function(a,b,c,d,e){var f="nth"!==a.slice(0,3),g="last"!==a.slice(-4),h="of-type"===b;return 1===d&&0===e?function(a){return!!a.parentNode}:function(b,c,i){var j,k,l,m,n,o,p=f!==g?"nextSibling":"previousSibling",q=b.parentNode,r=h&&b.nodeName.toLowerCase(),s=!i&&!h,t=!1;if(q){if(f){while(p){m=b;while(m=m[p])if(h?m.nodeName.toLowerCase()===r:1===m.nodeType)return!1;o=p="only"===a&&!o&&"nextSibling"}return!0}if(o=[g?q.firstChild:q.lastChild],g&&s){m=q,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n&&j[2],m=n&&q.childNodes[n];while(m=++n&&m&&m[p]||(t=n=0)||o.pop())if(1===m.nodeType&&++t&&m===b){k[a]=[w,n,t];break}}else if(s&&(m=b,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n),t===!1)while(m=++n&&m&&m[p]||(t=n=0)||o.pop())if((h?m.nodeName.toLowerCase()===r:1===m.nodeType)&&++t&&(s&&(l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),k[a]=[w,t]),m===b))break;return t-=e,t===d||t%d===0&&t/d>=0}}},PSEUDO:function(a,b){var c,e=d.pseudos[a]||d.setFilters[a.toLowerCase()]||fa.error("unsupported pseudo: "+a);return e[u]?e(b):e.length>1?(c=[a,a,"",b],d.setFilters.hasOwnProperty(a.toLowerCase())?ha(function(a,c){var d,f=e(a,b),g=f.length;while(g--)d=J(a,f[g]),a[d]=!(c[d]=f[g])}):function(a){return e(a,0,c)}):e}},pseudos:{not:ha(function(a){var b=[],c=[],d=h(a.replace(Q,"$1"));return d[u]?ha(function(a,b,c,e){var f,g=d(a,null,e,[]),h=a.length;while(h--)(f=g[h])&&(a[h]=!(b[h]=f))}):function(a,e,f){return b[0]=a,d(b,null,f,c),b[0]=null,!c.pop()}}),has:ha(function(a){return function(b){return fa(a,b).length>0}}),contains:ha(function(a){return a=a.replace(ba,ca),function(b){return(b.textContent||b.innerText||e(b)).indexOf(a)>-1}}),lang:ha(function(a){return V.test(a||"")||fa.error("unsupported lang: "+a),a=a.replace(ba,ca).toLowerCase(),function(b){var c;do if(c=p?b.lang:b.getAttribute("xml:lang")||b.getAttribute("lang"))return c=c.toLowerCase(),c===a||0===c.indexOf(a+"-");while((b=b.parentNode)&&1===b.nodeType);return!1}}),target:function(b){var c=a.location&&a.location.hash;return c&&c.slice(1)===b.id},root:function(a){return a===o},focus:function(a){return a===n.activeElement&&(!n.hasFocus||n.hasFocus())&&!!(a.type||a.href||~a.tabIndex)},enabled:function(a){return a.disabled===!1},disabled:function(a){return a.disabled===!0},checked:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&!!a.checked||"option"===b&&!!a.selected},selected:function(a){return a.parentNode&&a.parentNode.selectedIndex,a.selected===!0},empty:function(a){for(a=a.firstChild;a;a=a.nextSibling)if(a.nodeType<6)return!1;return!0},parent:function(a){return!d.pseudos.empty(a)},header:function(a){return Y.test(a.nodeName)},input:function(a){return X.test(a.nodeName)},button:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&"button"===a.type||"button"===b},text:function(a){var b;return"input"===a.nodeName.toLowerCase()&&"text"===a.type&&(null==(b=a.getAttribute("type"))||"text"===b.toLowerCase())},first:na(function(){return[0]}),last:na(function(a,b){return[b-1]}),eq:na(function(a,b,c){return[0>c?c+b:c]}),even:na(function(a,b){for(var c=0;b>c;c+=2)a.push(c);return a}),odd:na(function(a,b){for(var c=1;b>c;c+=2)a.push(c);return a}),lt:na(function(a,b,c){for(var d=0>c?c+b:c;--d>=0;)a.push(d);return a}),gt:na(function(a,b,c){for(var d=0>c?c+b:c;++d<b;)a.push(d);return a})}},d.pseudos.nth=d.pseudos.eq;for(b in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})d.pseudos[b]=la(b);for(b in{submit:!0,reset:!0})d.pseudos[b]=ma(b);function pa(){}pa.prototype=d.filters=d.pseudos,d.setFilters=new pa,g=fa.tokenize=function(a,b){var c,e,f,g,h,i,j,k=z[a+" "];if(k)return b?0:k.slice(0);h=a,i=[],j=d.preFilter;while(h){c&&!(e=R.exec(h))||(e&&(h=h.slice(e[0].length)||h),i.push(f=[])),c=!1,(e=S.exec(h))&&(c=e.shift(),f.push({value:c,type:e[0].replace(Q," ")}),h=h.slice(c.length));for(g in d.filter)!(e=W[g].exec(h))||j[g]&&!(e=j[g](e))||(c=e.shift(),f.push({value:c,type:g,matches:e}),h=h.slice(c.length));if(!c)break}return b?h.length:h?fa.error(a):z(a,i).slice(0)};function qa(a){for(var b=0,c=a.length,d="";c>b;b++)d+=a[b].value;return d}function ra(a,b,c){var d=b.dir,e=c&&"parentNode"===d,f=x++;return b.first?function(b,c,f){while(b=b[d])if(1===b.nodeType||e)return a(b,c,f)}:function(b,c,g){var h,i,j,k=[w,f];if(g){while(b=b[d])if((1===b.nodeType||e)&&a(b,c,g))return!0}else while(b=b[d])if(1===b.nodeType||e){if(j=b[u]||(b[u]={}),i=j[b.uniqueID]||(j[b.uniqueID]={}),(h=i[d])&&h[0]===w&&h[1]===f)return k[2]=h[2];if(i[d]=k,k[2]=a(b,c,g))return!0}}}function sa(a){return a.length>1?function(b,c,d){var e=a.length;while(e--)if(!a[e](b,c,d))return!1;return!0}:a[0]}function ta(a,b,c){for(var d=0,e=b.length;e>d;d++)fa(a,b[d],c);return c}function ua(a,b,c,d,e){for(var f,g=[],h=0,i=a.length,j=null!=b;i>h;h++)(f=a[h])&&(c&&!c(f,d,e)||(g.push(f),j&&b.push(h)));return g}function va(a,b,c,d,e,f){return d&&!d[u]&&(d=va(d)),e&&!e[u]&&(e=va(e,f)),ha(function(f,g,h,i){var j,k,l,m=[],n=[],o=g.length,p=f||ta(b||"*",h.nodeType?[h]:h,[]),q=!a||!f&&b?p:ua(p,m,a,h,i),r=c?e||(f?a:o||d)?[]:g:q;if(c&&c(q,r,h,i),d){j=ua(r,n),d(j,[],h,i),k=j.length;while(k--)(l=j[k])&&(r[n[k]]=!(q[n[k]]=l))}if(f){if(e||a){if(e){j=[],k=r.length;while(k--)(l=r[k])&&j.push(q[k]=l);e(null,r=[],j,i)}k=r.length;while(k--)(l=r[k])&&(j=e?J(f,l):m[k])>-1&&(f[j]=!(g[j]=l))}}else r=ua(r===g?r.splice(o,r.length):r),e?e(null,g,r,i):H.apply(g,r)})}function wa(a){for(var b,c,e,f=a.length,g=d.relative[a[0].type],h=g||d.relative[" "],i=g?1:0,k=ra(function(a){return a===b},h,!0),l=ra(function(a){return J(b,a)>-1},h,!0),m=[function(a,c,d){var e=!g&&(d||c!==j)||((b=c).nodeType?k(a,c,d):l(a,c,d));return b=null,e}];f>i;i++)if(c=d.relative[a[i].type])m=[ra(sa(m),c)];else{if(c=d.filter[a[i].type].apply(null,a[i].matches),c[u]){for(e=++i;f>e;e++)if(d.relative[a[e].type])break;return va(i>1&&sa(m),i>1&&qa(a.slice(0,i-1).concat({value:" "===a[i-2].type?"*":""})).replace(Q,"$1"),c,e>i&&wa(a.slice(i,e)),f>e&&wa(a=a.slice(e)),f>e&&qa(a))}m.push(c)}return sa(m)}function xa(a,b){var c=b.length>0,e=a.length>0,f=function(f,g,h,i,k){var l,o,q,r=0,s="0",t=f&&[],u=[],v=j,x=f||e&&d.find.TAG("*",k),y=w+=null==v?1:Math.random()||.1,z=x.length;for(k&&(j=g===n||g||k);s!==z&&null!=(l=x[s]);s++){if(e&&l){o=0,g||l.ownerDocument===n||(m(l),h=!p);while(q=a[o++])if(q(l,g||n,h)){i.push(l);break}k&&(w=y)}c&&((l=!q&&l)&&r--,f&&t.push(l))}if(r+=s,c&&s!==r){o=0;while(q=b[o++])q(t,u,g,h);if(f){if(r>0)while(s--)t[s]||u[s]||(u[s]=F.call(i));u=ua(u)}H.apply(i,u),k&&!f&&u.length>0&&r+b.length>1&&fa.uniqueSort(i)}return k&&(w=y,j=v),t};return c?ha(f):f}return h=fa.compile=function(a,b){var c,d=[],e=[],f=A[a+" "];if(!f){b||(b=g(a)),c=b.length;while(c--)f=wa(b[c]),f[u]?d.push(f):e.push(f);f=A(a,xa(e,d)),f.selector=a}return f},i=fa.select=function(a,b,e,f){var i,j,k,l,m,n="function"==typeof a&&a,o=!f&&g(a=n.selector||a);if(e=e||[],1===o.length){if(j=o[0]=o[0].slice(0),j.length>2&&"ID"===(k=j[0]).type&&c.getById&&9===b.nodeType&&p&&d.relative[j[1].type]){if(b=(d.find.ID(k.matches[0].replace(ba,ca),b)||[])[0],!b)return e;n&&(b=b.parentNode),a=a.slice(j.shift().value.length)}i=W.needsContext.test(a)?0:j.length;while(i--){if(k=j[i],d.relative[l=k.type])break;if((m=d.find[l])&&(f=m(k.matches[0].replace(ba,ca),_.test(j[0].type)&&oa(b.parentNode)||b))){if(j.splice(i,1),a=f.length&&qa(j),!a)return H.apply(e,f),e;break}}}return(n||h(a,o))(f,b,!p,e,!b||_.test(a)&&oa(b.parentNode)||b),e},c.sortStable=u.split("").sort(B).join("")===u,c.detectDuplicates=!!l,m(),c.sortDetached=ia(function(a){return 1&a.compareDocumentPosition(n.createElement("div"))}),ia(function(a){return a.innerHTML="<a href='#'></a>","#"===a.firstChild.getAttribute("href")})||ja("type|href|height|width",function(a,b,c){return c?void 0:a.getAttribute(b,"type"===b.toLowerCase()?1:2)}),c.attributes&&ia(function(a){return a.innerHTML="<input/>",a.firstChild.setAttribute("value",""),""===a.firstChild.getAttribute("value")})||ja("value",function(a,b,c){return c||"input"!==a.nodeName.toLowerCase()?void 0:a.defaultValue}),ia(function(a){return null==a.getAttribute("disabled")})||ja(K,function(a,b,c){var d;return c?void 0:a[b]===!0?b.toLowerCase():(d=a.getAttributeNode(b))&&d.specified?d.value:null}),fa}(a);n.find=t,n.expr=t.selectors,n.expr[":"]=n.expr.pseudos,n.uniqueSort=n.unique=t.uniqueSort,n.text=t.getText,n.isXMLDoc=t.isXML,n.contains=t.contains;var u=function(a,b,c){var d=[],e=void 0!==c;while((a=a[b])&&9!==a.nodeType)if(1===a.nodeType){if(e&&n(a).is(c))break;d.push(a)}return d},v=function(a,b){for(var c=[];a;a=a.nextSibling)1===a.nodeType&&a!==b&&c.push(a);return c},w=n.expr.match.needsContext,x=/^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,y=/^.[^:#\[\.,]*$/;function z(a,b,c){if(n.isFunction(b))return n.grep(a,function(a,d){return!!b.call(a,d,a)!==c});if(b.nodeType)return n.grep(a,function(a){return a===b!==c});if("string"==typeof b){if(y.test(b))return n.filter(b,a,c);b=n.filter(b,a)}return n.grep(a,function(a){return h.call(b,a)>-1!==c})}n.filter=function(a,b,c){var d=b[0];return c&&(a=":not("+a+")"),1===b.length&&1===d.nodeType?n.find.matchesSelector(d,a)?[d]:[]:n.find.matches(a,n.grep(b,function(a){return 1===a.nodeType}))},n.fn.extend({find:function(a){var b,c=this.length,d=[],e=this;if("string"!=typeof a)return this.pushStack(n(a).filter(function(){for(b=0;c>b;b++)if(n.contains(e[b],this))return!0}));for(b=0;c>b;b++)n.find(a,e[b],d);return d=this.pushStack(c>1?n.unique(d):d),d.selector=this.selector?this.selector+" "+a:a,d},filter:function(a){return this.pushStack(z(this,a||[],!1))},not:function(a){return this.pushStack(z(this,a||[],!0))},is:function(a){return!!z(this,"string"==typeof a&&w.test(a)?n(a):a||[],!1).length}});var A,B=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,C=n.fn.init=function(a,b,c){var e,f;if(!a)return this;if(c=c||A,"string"==typeof a){if(e="<"===a[0]&&">"===a[a.length-1]&&a.length>=3?[null,a,null]:B.exec(a),!e||!e[1]&&b)return!b||b.jquery?(b||c).find(a):this.constructor(b).find(a);if(e[1]){if(b=b instanceof n?b[0]:b,n.merge(this,n.parseHTML(e[1],b&&b.nodeType?b.ownerDocument||b:d,!0)),x.test(e[1])&&n.isPlainObject(b))for(e in b)n.isFunction(this[e])?this[e](b[e]):this.attr(e,b[e]);return this}return f=d.getElementById(e[2]),f&&f.parentNode&&(this.length=1,this[0]=f),this.context=d,this.selector=a,this}return a.nodeType?(this.context=this[0]=a,this.length=1,this):n.isFunction(a)?void 0!==c.ready?c.ready(a):a(n):(void 0!==a.selector&&(this.selector=a.selector,this.context=a.context),n.makeArray(a,this))};C.prototype=n.fn,A=n(d);var D=/^(?:parents|prev(?:Until|All))/,E={children:!0,contents:!0,next:!0,prev:!0};n.fn.extend({has:function(a){var b=n(a,this),c=b.length;return this.filter(function(){for(var a=0;c>a;a++)if(n.contains(this,b[a]))return!0})},closest:function(a,b){for(var c,d=0,e=this.length,f=[],g=w.test(a)||"string"!=typeof a?n(a,b||this.context):0;e>d;d++)for(c=this[d];c&&c!==b;c=c.parentNode)if(c.nodeType<11&&(g?g.index(c)>-1:1===c.nodeType&&n.find.matchesSelector(c,a))){f.push(c);break}return this.pushStack(f.length>1?n.uniqueSort(f):f)},index:function(a){return a?"string"==typeof a?h.call(n(a),this[0]):h.call(this,a.jquery?a[0]:a):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(a,b){return this.pushStack(n.uniqueSort(n.merge(this.get(),n(a,b))))},addBack:function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))}});function F(a,b){while((a=a[b])&&1!==a.nodeType);return a}n.each({parent:function(a){var b=a.parentNode;return b&&11!==b.nodeType?b:null},parents:function(a){return u(a,"parentNode")},parentsUntil:function(a,b,c){return u(a,"parentNode",c)},next:function(a){return F(a,"nextSibling")},prev:function(a){return F(a,"previousSibling")},nextAll:function(a){return u(a,"nextSibling")},prevAll:function(a){return u(a,"previousSibling")},nextUntil:function(a,b,c){return u(a,"nextSibling",c)},prevUntil:function(a,b,c){return u(a,"previousSibling",c)},siblings:function(a){return v((a.parentNode||{}).firstChild,a)},children:function(a){return v(a.firstChild)},contents:function(a){return a.contentDocument||n.merge([],a.childNodes)}},function(a,b){n.fn[a]=function(c,d){var e=n.map(this,b,c);return"Until"!==a.slice(-5)&&(d=c),d&&"string"==typeof d&&(e=n.filter(d,e)),this.length>1&&(E[a]||n.uniqueSort(e),D.test(a)&&e.reverse()),this.pushStack(e)}});var G=/\S+/g;function H(a){var b={};return n.each(a.match(G)||[],function(a,c){b[c]=!0}),b}n.Callbacks=function(a){a="string"==typeof a?H(a):n.extend({},a);var b,c,d,e,f=[],g=[],h=-1,i=function(){for(e=a.once,d=b=!0;g.length;h=-1){c=g.shift();while(++h<f.length)f[h].apply(c[0],c[1])===!1&&a.stopOnFalse&&(h=f.length,c=!1)}a.memory||(c=!1),b=!1,e&&(f=c?[]:"")},j={add:function(){return f&&(c&&!b&&(h=f.length-1,g.push(c)),function d(b){n.each(b,function(b,c){n.isFunction(c)?a.unique&&j.has(c)||f.push(c):c&&c.length&&"string"!==n.type(c)&&d(c)})}(arguments),c&&!b&&i()),this},remove:function(){return n.each(arguments,function(a,b){var c;while((c=n.inArray(b,f,c))>-1)f.splice(c,1),h>=c&&h--}),this},has:function(a){return a?n.inArray(a,f)>-1:f.length>0},empty:function(){return f&&(f=[]),this},disable:function(){return e=g=[],f=c="",this},disabled:function(){return!f},lock:function(){return e=g=[],c||(f=c=""),this},locked:function(){return!!e},fireWith:function(a,c){return e||(c=c||[],c=[a,c.slice?c.slice():c],g.push(c),b||i()),this},fire:function(){return j.fireWith(this,arguments),this},fired:function(){return!!d}};return j},n.extend({Deferred:function(a){var b=[["resolve","done",n.Callbacks("once memory"),"resolved"],["reject","fail",n.Callbacks("once memory"),"rejected"],["notify","progress",n.Callbacks("memory")]],c="pending",d={state:function(){return c},always:function(){return e.done(arguments).fail(arguments),this},then:function(){var a=arguments;return n.Deferred(function(c){n.each(b,function(b,f){var g=n.isFunction(a[b])&&a[b];e[f[1]](function(){var a=g&&g.apply(this,arguments);a&&n.isFunction(a.promise)?a.promise().progress(c.notify).done(c.resolve).fail(c.reject):c[f[0]+"With"](this===d?c.promise():this,g?[a]:arguments)})}),a=null}).promise()},promise:function(a){return null!=a?n.extend(a,d):d}},e={};return d.pipe=d.then,n.each(b,function(a,f){var g=f[2],h=f[3];d[f[1]]=g.add,h&&g.add(function(){c=h},b[1^a][2].disable,b[2][2].lock),e[f[0]]=function(){return e[f[0]+"With"](this===e?d:this,arguments),this},e[f[0]+"With"]=g.fireWith}),d.promise(e),a&&a.call(e,e),e},when:function(a){var b=0,c=e.call(arguments),d=c.length,f=1!==d||a&&n.isFunction(a.promise)?d:0,g=1===f?a:n.Deferred(),h=function(a,b,c){return function(d){b[a]=this,c[a]=arguments.length>1?e.call(arguments):d,c===i?g.notifyWith(b,c):--f||g.resolveWith(b,c)}},i,j,k;if(d>1)for(i=new Array(d),j=new Array(d),k=new Array(d);d>b;b++)c[b]&&n.isFunction(c[b].promise)?c[b].promise().progress(h(b,j,i)).done(h(b,k,c)).fail(g.reject):--f;return f||g.resolveWith(k,c),g.promise()}});var I;n.fn.ready=function(a){return n.ready.promise().done(a),this},n.extend({isReady:!1,readyWait:1,holdReady:function(a){a?n.readyWait++:n.ready(!0)},ready:function(a){(a===!0?--n.readyWait:n.isReady)||(n.isReady=!0,a!==!0&&--n.readyWait>0||(I.resolveWith(d,[n]),n.fn.triggerHandler&&(n(d).triggerHandler("ready"),n(d).off("ready"))))}});function J(){d.removeEventListener("DOMContentLoaded",J),a.removeEventListener("load",J),n.ready()}n.ready.promise=function(b){return I||(I=n.Deferred(),"complete"===d.readyState||"loading"!==d.readyState&&!d.documentElement.doScroll?a.setTimeout(n.ready):(d.addEventListener("DOMContentLoaded",J),a.addEventListener("load",J))),I.promise(b)},n.ready.promise();var K=function(a,b,c,d,e,f,g){var h=0,i=a.length,j=null==c;if("object"===n.type(c)){e=!0;for(h in c)K(a,b,h,c[h],!0,f,g)}else if(void 0!==d&&(e=!0,n.isFunction(d)||(g=!0),j&&(g?(b.call(a,d),b=null):(j=b,b=function(a,b,c){return j.call(n(a),c)})),b))for(;i>h;h++)b(a[h],c,g?d:d.call(a[h],h,b(a[h],c)));return e?a:j?b.call(a):i?b(a[0],c):f},L=function(a){return 1===a.nodeType||9===a.nodeType||!+a.nodeType};function M(){this.expando=n.expando+M.uid++}M.uid=1,M.prototype={register:function(a,b){var c=b||{};return a.nodeType?a[this.expando]=c:Object.defineProperty(a,this.expando,{value:c,writable:!0,configurable:!0}),a[this.expando]},cache:function(a){if(!L(a))return{};var b=a[this.expando];return b||(b={},L(a)&&(a.nodeType?a[this.expando]=b:Object.defineProperty(a,this.expando,{value:b,configurable:!0}))),b},set:function(a,b,c){var d,e=this.cache(a);if("string"==typeof b)e[b]=c;else for(d in b)e[d]=b[d];return e},get:function(a,b){return void 0===b?this.cache(a):a[this.expando]&&a[this.expando][b]},access:function(a,b,c){var d;return void 0===b||b&&"string"==typeof b&&void 0===c?(d=this.get(a,b),void 0!==d?d:this.get(a,n.camelCase(b))):(this.set(a,b,c),void 0!==c?c:b)},remove:function(a,b){var c,d,e,f=a[this.expando];if(void 0!==f){if(void 0===b)this.register(a);else{n.isArray(b)?d=b.concat(b.map(n.camelCase)):(e=n.camelCase(b),b in f?d=[b,e]:(d=e,d=d in f?[d]:d.match(G)||[])),c=d.length;while(c--)delete f[d[c]]}(void 0===b||n.isEmptyObject(f))&&(a.nodeType?a[this.expando]=void 0:delete a[this.expando])}},hasData:function(a){var b=a[this.expando];return void 0!==b&&!n.isEmptyObject(b)}};var N=new M,O=new M,P=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,Q=/[A-Z]/g;function R(a,b,c){var d;if(void 0===c&&1===a.nodeType)if(d="data-"+b.replace(Q,"-$&").toLowerCase(),c=a.getAttribute(d),"string"==typeof c){try{c="true"===c?!0:"false"===c?!1:"null"===c?null:+c+""===c?+c:P.test(c)?n.parseJSON(c):c;
}catch(e){}O.set(a,b,c)}else c=void 0;return c}n.extend({hasData:function(a){return O.hasData(a)||N.hasData(a)},data:function(a,b,c){return O.access(a,b,c)},removeData:function(a,b){O.remove(a,b)},_data:function(a,b,c){return N.access(a,b,c)},_removeData:function(a,b){N.remove(a,b)}}),n.fn.extend({data:function(a,b){var c,d,e,f=this[0],g=f&&f.attributes;if(void 0===a){if(this.length&&(e=O.get(f),1===f.nodeType&&!N.get(f,"hasDataAttrs"))){c=g.length;while(c--)g[c]&&(d=g[c].name,0===d.indexOf("data-")&&(d=n.camelCase(d.slice(5)),R(f,d,e[d])));N.set(f,"hasDataAttrs",!0)}return e}return"object"==typeof a?this.each(function(){O.set(this,a)}):K(this,function(b){var c,d;if(f&&void 0===b){if(c=O.get(f,a)||O.get(f,a.replace(Q,"-$&").toLowerCase()),void 0!==c)return c;if(d=n.camelCase(a),c=O.get(f,d),void 0!==c)return c;if(c=R(f,d,void 0),void 0!==c)return c}else d=n.camelCase(a),this.each(function(){var c=O.get(this,d);O.set(this,d,b),a.indexOf("-")>-1&&void 0!==c&&O.set(this,a,b)})},null,b,arguments.length>1,null,!0)},removeData:function(a){return this.each(function(){O.remove(this,a)})}}),n.extend({queue:function(a,b,c){var d;return a?(b=(b||"fx")+"queue",d=N.get(a,b),c&&(!d||n.isArray(c)?d=N.access(a,b,n.makeArray(c)):d.push(c)),d||[]):void 0},dequeue:function(a,b){b=b||"fx";var c=n.queue(a,b),d=c.length,e=c.shift(),f=n._queueHooks(a,b),g=function(){n.dequeue(a,b)};"inprogress"===e&&(e=c.shift(),d--),e&&("fx"===b&&c.unshift("inprogress"),delete f.stop,e.call(a,g,f)),!d&&f&&f.empty.fire()},_queueHooks:function(a,b){var c=b+"queueHooks";return N.get(a,c)||N.access(a,c,{empty:n.Callbacks("once memory").add(function(){N.remove(a,[b+"queue",c])})})}}),n.fn.extend({queue:function(a,b){var c=2;return"string"!=typeof a&&(b=a,a="fx",c--),arguments.length<c?n.queue(this[0],a):void 0===b?this:this.each(function(){var c=n.queue(this,a,b);n._queueHooks(this,a),"fx"===a&&"inprogress"!==c[0]&&n.dequeue(this,a)})},dequeue:function(a){return this.each(function(){n.dequeue(this,a)})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(a,b){var c,d=1,e=n.Deferred(),f=this,g=this.length,h=function(){--d||e.resolveWith(f,[f])};"string"!=typeof a&&(b=a,a=void 0),a=a||"fx";while(g--)c=N.get(f[g],a+"queueHooks"),c&&c.empty&&(d++,c.empty.add(h));return h(),e.promise(b)}});var S=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,T=new RegExp("^(?:([+-])=|)("+S+")([a-z%]*)$","i"),U=["Top","Right","Bottom","Left"],V=function(a,b){return a=b||a,"none"===n.css(a,"display")||!n.contains(a.ownerDocument,a)};function W(a,b,c,d){var e,f=1,g=20,h=d?function(){return d.cur()}:function(){return n.css(a,b,"")},i=h(),j=c&&c[3]||(n.cssNumber[b]?"":"px"),k=(n.cssNumber[b]||"px"!==j&&+i)&&T.exec(n.css(a,b));if(k&&k[3]!==j){j=j||k[3],c=c||[],k=+i||1;do f=f||".5",k/=f,n.style(a,b,k+j);while(f!==(f=h()/i)&&1!==f&&--g)}return c&&(k=+k||+i||0,e=c[1]?k+(c[1]+1)*c[2]:+c[2],d&&(d.unit=j,d.start=k,d.end=e)),e}var X=/^(?:checkbox|radio)$/i,Y=/<([\w:-]+)/,Z=/^$|\/(?:java|ecma)script/i,$={option:[1,"<select multiple='multiple'>","</select>"],thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};$.optgroup=$.option,$.tbody=$.tfoot=$.colgroup=$.caption=$.thead,$.th=$.td;function _(a,b){var c="undefined"!=typeof a.getElementsByTagName?a.getElementsByTagName(b||"*"):"undefined"!=typeof a.querySelectorAll?a.querySelectorAll(b||"*"):[];return void 0===b||b&&n.nodeName(a,b)?n.merge([a],c):c}function aa(a,b){for(var c=0,d=a.length;d>c;c++)N.set(a[c],"globalEval",!b||N.get(b[c],"globalEval"))}var ba=/<|&#?\w+;/;function ca(a,b,c,d,e){for(var f,g,h,i,j,k,l=b.createDocumentFragment(),m=[],o=0,p=a.length;p>o;o++)if(f=a[o],f||0===f)if("object"===n.type(f))n.merge(m,f.nodeType?[f]:f);else if(ba.test(f)){g=g||l.appendChild(b.createElement("div")),h=(Y.exec(f)||["",""])[1].toLowerCase(),i=$[h]||$._default,g.innerHTML=i[1]+n.htmlPrefilter(f)+i[2],k=i[0];while(k--)g=g.lastChild;n.merge(m,g.childNodes),g=l.firstChild,g.textContent=""}else m.push(b.createTextNode(f));l.textContent="",o=0;while(f=m[o++])if(d&&n.inArray(f,d)>-1)e&&e.push(f);else if(j=n.contains(f.ownerDocument,f),g=_(l.appendChild(f),"script"),j&&aa(g),c){k=0;while(f=g[k++])Z.test(f.type||"")&&c.push(f)}return l}!function(){var a=d.createDocumentFragment(),b=a.appendChild(d.createElement("div")),c=d.createElement("input");c.setAttribute("type","radio"),c.setAttribute("checked","checked"),c.setAttribute("name","t"),b.appendChild(c),l.checkClone=b.cloneNode(!0).cloneNode(!0).lastChild.checked,b.innerHTML="<textarea>x</textarea>",l.noCloneChecked=!!b.cloneNode(!0).lastChild.defaultValue}();var da=/^key/,ea=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,fa=/^([^.]*)(?:\.(.+)|)/;function ga(){return!0}function ha(){return!1}function ia(){try{return d.activeElement}catch(a){}}function ja(a,b,c,d,e,f){var g,h;if("object"==typeof b){"string"!=typeof c&&(d=d||c,c=void 0);for(h in b)ja(a,h,c,d,b[h],f);return a}if(null==d&&null==e?(e=c,d=c=void 0):null==e&&("string"==typeof c?(e=d,d=void 0):(e=d,d=c,c=void 0)),e===!1)e=ha;else if(!e)return a;return 1===f&&(g=e,e=function(a){return n().off(a),g.apply(this,arguments)},e.guid=g.guid||(g.guid=n.guid++)),a.each(function(){n.event.add(this,b,e,d,c)})}n.event={global:{},add:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=N.get(a);if(r){c.handler&&(f=c,c=f.handler,e=f.selector),c.guid||(c.guid=n.guid++),(i=r.events)||(i=r.events={}),(g=r.handle)||(g=r.handle=function(b){return"undefined"!=typeof n&&n.event.triggered!==b.type?n.event.dispatch.apply(a,arguments):void 0}),b=(b||"").match(G)||[""],j=b.length;while(j--)h=fa.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o&&(l=n.event.special[o]||{},o=(e?l.delegateType:l.bindType)||o,l=n.event.special[o]||{},k=n.extend({type:o,origType:q,data:d,handler:c,guid:c.guid,selector:e,needsContext:e&&n.expr.match.needsContext.test(e),namespace:p.join(".")},f),(m=i[o])||(m=i[o]=[],m.delegateCount=0,l.setup&&l.setup.call(a,d,p,g)!==!1||a.addEventListener&&a.addEventListener(o,g)),l.add&&(l.add.call(a,k),k.handler.guid||(k.handler.guid=c.guid)),e?m.splice(m.delegateCount++,0,k):m.push(k),n.event.global[o]=!0)}},remove:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=N.hasData(a)&&N.get(a);if(r&&(i=r.events)){b=(b||"").match(G)||[""],j=b.length;while(j--)if(h=fa.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o){l=n.event.special[o]||{},o=(d?l.delegateType:l.bindType)||o,m=i[o]||[],h=h[2]&&new RegExp("(^|\\.)"+p.join("\\.(?:.*\\.|)")+"(\\.|$)"),g=f=m.length;while(f--)k=m[f],!e&&q!==k.origType||c&&c.guid!==k.guid||h&&!h.test(k.namespace)||d&&d!==k.selector&&("**"!==d||!k.selector)||(m.splice(f,1),k.selector&&m.delegateCount--,l.remove&&l.remove.call(a,k));g&&!m.length&&(l.teardown&&l.teardown.call(a,p,r.handle)!==!1||n.removeEvent(a,o,r.handle),delete i[o])}else for(o in i)n.event.remove(a,o+b[j],c,d,!0);n.isEmptyObject(i)&&N.remove(a,"handle events")}},dispatch:function(a){a=n.event.fix(a);var b,c,d,f,g,h=[],i=e.call(arguments),j=(N.get(this,"events")||{})[a.type]||[],k=n.event.special[a.type]||{};if(i[0]=a,a.delegateTarget=this,!k.preDispatch||k.preDispatch.call(this,a)!==!1){h=n.event.handlers.call(this,a,j),b=0;while((f=h[b++])&&!a.isPropagationStopped()){a.currentTarget=f.elem,c=0;while((g=f.handlers[c++])&&!a.isImmediatePropagationStopped())a.rnamespace&&!a.rnamespace.test(g.namespace)||(a.handleObj=g,a.data=g.data,d=((n.event.special[g.origType]||{}).handle||g.handler).apply(f.elem,i),void 0!==d&&(a.result=d)===!1&&(a.preventDefault(),a.stopPropagation()))}return k.postDispatch&&k.postDispatch.call(this,a),a.result}},handlers:function(a,b){var c,d,e,f,g=[],h=b.delegateCount,i=a.target;if(h&&i.nodeType&&("click"!==a.type||isNaN(a.button)||a.button<1))for(;i!==this;i=i.parentNode||this)if(1===i.nodeType&&(i.disabled!==!0||"click"!==a.type)){for(d=[],c=0;h>c;c++)f=b[c],e=f.selector+" ",void 0===d[e]&&(d[e]=f.needsContext?n(e,this).index(i)>-1:n.find(e,this,null,[i]).length),d[e]&&d.push(f);d.length&&g.push({elem:i,handlers:d})}return h<b.length&&g.push({elem:this,handlers:b.slice(h)}),g},props:"altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(a,b){return null==a.which&&(a.which=null!=b.charCode?b.charCode:b.keyCode),a}},mouseHooks:{props:"button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(a,b){var c,e,f,g=b.button;return null==a.pageX&&null!=b.clientX&&(c=a.target.ownerDocument||d,e=c.documentElement,f=c.body,a.pageX=b.clientX+(e&&e.scrollLeft||f&&f.scrollLeft||0)-(e&&e.clientLeft||f&&f.clientLeft||0),a.pageY=b.clientY+(e&&e.scrollTop||f&&f.scrollTop||0)-(e&&e.clientTop||f&&f.clientTop||0)),a.which||void 0===g||(a.which=1&g?1:2&g?3:4&g?2:0),a}},fix:function(a){if(a[n.expando])return a;var b,c,e,f=a.type,g=a,h=this.fixHooks[f];h||(this.fixHooks[f]=h=ea.test(f)?this.mouseHooks:da.test(f)?this.keyHooks:{}),e=h.props?this.props.concat(h.props):this.props,a=new n.Event(g),b=e.length;while(b--)c=e[b],a[c]=g[c];return a.target||(a.target=d),3===a.target.nodeType&&(a.target=a.target.parentNode),h.filter?h.filter(a,g):a},special:{load:{noBubble:!0},focus:{trigger:function(){return this!==ia()&&this.focus?(this.focus(),!1):void 0},delegateType:"focusin"},blur:{trigger:function(){return this===ia()&&this.blur?(this.blur(),!1):void 0},delegateType:"focusout"},click:{trigger:function(){return"checkbox"===this.type&&this.click&&n.nodeName(this,"input")?(this.click(),!1):void 0},_default:function(a){return n.nodeName(a.target,"a")}},beforeunload:{postDispatch:function(a){void 0!==a.result&&a.originalEvent&&(a.originalEvent.returnValue=a.result)}}}},n.removeEvent=function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c)},n.Event=function(a,b){return this instanceof n.Event?(a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||void 0===a.defaultPrevented&&a.returnValue===!1?ga:ha):this.type=a,b&&n.extend(this,b),this.timeStamp=a&&a.timeStamp||n.now(),void(this[n.expando]=!0)):new n.Event(a,b)},n.Event.prototype={constructor:n.Event,isDefaultPrevented:ha,isPropagationStopped:ha,isImmediatePropagationStopped:ha,isSimulated:!1,preventDefault:function(){var a=this.originalEvent;this.isDefaultPrevented=ga,a&&!this.isSimulated&&a.preventDefault()},stopPropagation:function(){var a=this.originalEvent;this.isPropagationStopped=ga,a&&!this.isSimulated&&a.stopPropagation()},stopImmediatePropagation:function(){var a=this.originalEvent;this.isImmediatePropagationStopped=ga,a&&!this.isSimulated&&a.stopImmediatePropagation(),this.stopPropagation()}},n.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(a,b){n.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c,d=this,e=a.relatedTarget,f=a.handleObj;return e&&(e===d||n.contains(d,e))||(a.type=f.origType,c=f.handler.apply(this,arguments),a.type=b),c}}}),n.fn.extend({on:function(a,b,c,d){return ja(this,a,b,c,d)},one:function(a,b,c,d){return ja(this,a,b,c,d,1)},off:function(a,b,c){var d,e;if(a&&a.preventDefault&&a.handleObj)return d=a.handleObj,n(a.delegateTarget).off(d.namespace?d.origType+"."+d.namespace:d.origType,d.selector,d.handler),this;if("object"==typeof a){for(e in a)this.off(e,b,a[e]);return this}return b!==!1&&"function"!=typeof b||(c=b,b=void 0),c===!1&&(c=ha),this.each(function(){n.event.remove(this,a,c,b)})}});var ka=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,la=/<script|<style|<link/i,ma=/checked\s*(?:[^=]|=\s*.checked.)/i,na=/^true\/(.*)/,oa=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;function pa(a,b){return n.nodeName(a,"table")&&n.nodeName(11!==b.nodeType?b:b.firstChild,"tr")?a.getElementsByTagName("tbody")[0]||a.appendChild(a.ownerDocument.createElement("tbody")):a}function qa(a){return a.type=(null!==a.getAttribute("type"))+"/"+a.type,a}function ra(a){var b=na.exec(a.type);return b?a.type=b[1]:a.removeAttribute("type"),a}function sa(a,b){var c,d,e,f,g,h,i,j;if(1===b.nodeType){if(N.hasData(a)&&(f=N.access(a),g=N.set(b,f),j=f.events)){delete g.handle,g.events={};for(e in j)for(c=0,d=j[e].length;d>c;c++)n.event.add(b,e,j[e][c])}O.hasData(a)&&(h=O.access(a),i=n.extend({},h),O.set(b,i))}}function ta(a,b){var c=b.nodeName.toLowerCase();"input"===c&&X.test(a.type)?b.checked=a.checked:"input"!==c&&"textarea"!==c||(b.defaultValue=a.defaultValue)}function ua(a,b,c,d){b=f.apply([],b);var e,g,h,i,j,k,m=0,o=a.length,p=o-1,q=b[0],r=n.isFunction(q);if(r||o>1&&"string"==typeof q&&!l.checkClone&&ma.test(q))return a.each(function(e){var f=a.eq(e);r&&(b[0]=q.call(this,e,f.html())),ua(f,b,c,d)});if(o&&(e=ca(b,a[0].ownerDocument,!1,a,d),g=e.firstChild,1===e.childNodes.length&&(e=g),g||d)){for(h=n.map(_(e,"script"),qa),i=h.length;o>m;m++)j=e,m!==p&&(j=n.clone(j,!0,!0),i&&n.merge(h,_(j,"script"))),c.call(a[m],j,m);if(i)for(k=h[h.length-1].ownerDocument,n.map(h,ra),m=0;i>m;m++)j=h[m],Z.test(j.type||"")&&!N.access(j,"globalEval")&&n.contains(k,j)&&(j.src?n._evalUrl&&n._evalUrl(j.src):n.globalEval(j.textContent.replace(oa,"")))}return a}function va(a,b,c){for(var d,e=b?n.filter(b,a):a,f=0;null!=(d=e[f]);f++)c||1!==d.nodeType||n.cleanData(_(d)),d.parentNode&&(c&&n.contains(d.ownerDocument,d)&&aa(_(d,"script")),d.parentNode.removeChild(d));return a}n.extend({htmlPrefilter:function(a){return a.replace(ka,"<$1></$2>")},clone:function(a,b,c){var d,e,f,g,h=a.cloneNode(!0),i=n.contains(a.ownerDocument,a);if(!(l.noCloneChecked||1!==a.nodeType&&11!==a.nodeType||n.isXMLDoc(a)))for(g=_(h),f=_(a),d=0,e=f.length;e>d;d++)ta(f[d],g[d]);if(b)if(c)for(f=f||_(a),g=g||_(h),d=0,e=f.length;e>d;d++)sa(f[d],g[d]);else sa(a,h);return g=_(h,"script"),g.length>0&&aa(g,!i&&_(a,"script")),h},cleanData:function(a){for(var b,c,d,e=n.event.special,f=0;void 0!==(c=a[f]);f++)if(L(c)){if(b=c[N.expando]){if(b.events)for(d in b.events)e[d]?n.event.remove(c,d):n.removeEvent(c,d,b.handle);c[N.expando]=void 0}c[O.expando]&&(c[O.expando]=void 0)}}}),n.fn.extend({domManip:ua,detach:function(a){return va(this,a,!0)},remove:function(a){return va(this,a)},text:function(a){return K(this,function(a){return void 0===a?n.text(this):this.empty().each(function(){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||(this.textContent=a)})},null,a,arguments.length)},append:function(){return ua(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=pa(this,a);b.appendChild(a)}})},prepend:function(){return ua(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=pa(this,a);b.insertBefore(a,b.firstChild)}})},before:function(){return ua(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this)})},after:function(){return ua(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this.nextSibling)})},empty:function(){for(var a,b=0;null!=(a=this[b]);b++)1===a.nodeType&&(n.cleanData(_(a,!1)),a.textContent="");return this},clone:function(a,b){return a=null==a?!1:a,b=null==b?a:b,this.map(function(){return n.clone(this,a,b)})},html:function(a){return K(this,function(a){var b=this[0]||{},c=0,d=this.length;if(void 0===a&&1===b.nodeType)return b.innerHTML;if("string"==typeof a&&!la.test(a)&&!$[(Y.exec(a)||["",""])[1].toLowerCase()]){a=n.htmlPrefilter(a);try{for(;d>c;c++)b=this[c]||{},1===b.nodeType&&(n.cleanData(_(b,!1)),b.innerHTML=a);b=0}catch(e){}}b&&this.empty().append(a)},null,a,arguments.length)},replaceWith:function(){var a=[];return ua(this,arguments,function(b){var c=this.parentNode;n.inArray(this,a)<0&&(n.cleanData(_(this)),c&&c.replaceChild(b,this))},a)}}),n.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){n.fn[a]=function(a){for(var c,d=[],e=n(a),f=e.length-1,h=0;f>=h;h++)c=h===f?this:this.clone(!0),n(e[h])[b](c),g.apply(d,c.get());return this.pushStack(d)}});var wa,xa={HTML:"block",BODY:"block"};function ya(a,b){var c=n(b.createElement(a)).appendTo(b.body),d=n.css(c[0],"display");return c.detach(),d}function za(a){var b=d,c=xa[a];return c||(c=ya(a,b),"none"!==c&&c||(wa=(wa||n("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement),b=wa[0].contentDocument,b.write(),b.close(),c=ya(a,b),wa.detach()),xa[a]=c),c}var Aa=/^margin/,Ba=new RegExp("^("+S+")(?!px)[a-z%]+$","i"),Ca=function(b){var c=b.ownerDocument.defaultView;return c&&c.opener||(c=a),c.getComputedStyle(b)},Da=function(a,b,c,d){var e,f,g={};for(f in b)g[f]=a.style[f],a.style[f]=b[f];e=c.apply(a,d||[]);for(f in b)a.style[f]=g[f];return e},Ea=d.documentElement;!function(){var b,c,e,f,g=d.createElement("div"),h=d.createElement("div");if(h.style){h.style.backgroundClip="content-box",h.cloneNode(!0).style.backgroundClip="",l.clearCloneStyle="content-box"===h.style.backgroundClip,g.style.cssText="border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute",g.appendChild(h);function i(){h.style.cssText="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%",h.innerHTML="",Ea.appendChild(g);var d=a.getComputedStyle(h);b="1%"!==d.top,f="2px"===d.marginLeft,c="4px"===d.width,h.style.marginRight="50%",e="4px"===d.marginRight,Ea.removeChild(g)}n.extend(l,{pixelPosition:function(){return i(),b},boxSizingReliable:function(){return null==c&&i(),c},pixelMarginRight:function(){return null==c&&i(),e},reliableMarginLeft:function(){return null==c&&i(),f},reliableMarginRight:function(){var b,c=h.appendChild(d.createElement("div"));return c.style.cssText=h.style.cssText="-webkit-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0",c.style.marginRight=c.style.width="0",h.style.width="1px",Ea.appendChild(g),b=!parseFloat(a.getComputedStyle(c).marginRight),Ea.removeChild(g),h.removeChild(c),b}})}}();function Fa(a,b,c){var d,e,f,g,h=a.style;return c=c||Ca(a),g=c?c.getPropertyValue(b)||c[b]:void 0,""!==g&&void 0!==g||n.contains(a.ownerDocument,a)||(g=n.style(a,b)),c&&!l.pixelMarginRight()&&Ba.test(g)&&Aa.test(b)&&(d=h.width,e=h.minWidth,f=h.maxWidth,h.minWidth=h.maxWidth=h.width=g,g=c.width,h.width=d,h.minWidth=e,h.maxWidth=f),void 0!==g?g+"":g}function Ga(a,b){return{get:function(){return a()?void delete this.get:(this.get=b).apply(this,arguments)}}}var Ha=/^(none|table(?!-c[ea]).+)/,Ia={position:"absolute",visibility:"hidden",display:"block"},Ja={letterSpacing:"0",fontWeight:"400"},Ka=["Webkit","O","Moz","ms"],La=d.createElement("div").style;function Ma(a){if(a in La)return a;var b=a[0].toUpperCase()+a.slice(1),c=Ka.length;while(c--)if(a=Ka[c]+b,a in La)return a}function Na(a,b,c){var d=T.exec(b);return d?Math.max(0,d[2]-(c||0))+(d[3]||"px"):b}function Oa(a,b,c,d,e){for(var f=c===(d?"border":"content")?4:"width"===b?1:0,g=0;4>f;f+=2)"margin"===c&&(g+=n.css(a,c+U[f],!0,e)),d?("content"===c&&(g-=n.css(a,"padding"+U[f],!0,e)),"margin"!==c&&(g-=n.css(a,"border"+U[f]+"Width",!0,e))):(g+=n.css(a,"padding"+U[f],!0,e),"padding"!==c&&(g+=n.css(a,"border"+U[f]+"Width",!0,e)));return g}function Pa(a,b,c){var d=!0,e="width"===b?a.offsetWidth:a.offsetHeight,f=Ca(a),g="border-box"===n.css(a,"boxSizing",!1,f);if(0>=e||null==e){if(e=Fa(a,b,f),(0>e||null==e)&&(e=a.style[b]),Ba.test(e))return e;d=g&&(l.boxSizingReliable()||e===a.style[b]),e=parseFloat(e)||0}return e+Oa(a,b,c||(g?"border":"content"),d,f)+"px"}function Qa(a,b){for(var c,d,e,f=[],g=0,h=a.length;h>g;g++)d=a[g],d.style&&(f[g]=N.get(d,"olddisplay"),c=d.style.display,b?(f[g]||"none"!==c||(d.style.display=""),""===d.style.display&&V(d)&&(f[g]=N.access(d,"olddisplay",za(d.nodeName)))):(e=V(d),"none"===c&&e||N.set(d,"olddisplay",e?c:n.css(d,"display"))));for(g=0;h>g;g++)d=a[g],d.style&&(b&&"none"!==d.style.display&&""!==d.style.display||(d.style.display=b?f[g]||"":"none"));return a}n.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=Fa(a,"opacity");return""===c?"1":c}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":"cssFloat"},style:function(a,b,c,d){if(a&&3!==a.nodeType&&8!==a.nodeType&&a.style){var e,f,g,h=n.camelCase(b),i=a.style;return b=n.cssProps[h]||(n.cssProps[h]=Ma(h)||h),g=n.cssHooks[b]||n.cssHooks[h],void 0===c?g&&"get"in g&&void 0!==(e=g.get(a,!1,d))?e:i[b]:(f=typeof c,"string"===f&&(e=T.exec(c))&&e[1]&&(c=W(a,b,e),f="number"),null!=c&&c===c&&("number"===f&&(c+=e&&e[3]||(n.cssNumber[h]?"":"px")),l.clearCloneStyle||""!==c||0!==b.indexOf("background")||(i[b]="inherit"),g&&"set"in g&&void 0===(c=g.set(a,c,d))||(i[b]=c)),void 0)}},css:function(a,b,c,d){var e,f,g,h=n.camelCase(b);return b=n.cssProps[h]||(n.cssProps[h]=Ma(h)||h),g=n.cssHooks[b]||n.cssHooks[h],g&&"get"in g&&(e=g.get(a,!0,c)),void 0===e&&(e=Fa(a,b,d)),"normal"===e&&b in Ja&&(e=Ja[b]),""===c||c?(f=parseFloat(e),c===!0||isFinite(f)?f||0:e):e}}),n.each(["height","width"],function(a,b){n.cssHooks[b]={get:function(a,c,d){return c?Ha.test(n.css(a,"display"))&&0===a.offsetWidth?Da(a,Ia,function(){return Pa(a,b,d)}):Pa(a,b,d):void 0},set:function(a,c,d){var e,f=d&&Ca(a),g=d&&Oa(a,b,d,"border-box"===n.css(a,"boxSizing",!1,f),f);return g&&(e=T.exec(c))&&"px"!==(e[3]||"px")&&(a.style[b]=c,c=n.css(a,b)),Na(a,c,g)}}}),n.cssHooks.marginLeft=Ga(l.reliableMarginLeft,function(a,b){return b?(parseFloat(Fa(a,"marginLeft"))||a.getBoundingClientRect().left-Da(a,{marginLeft:0},function(){return a.getBoundingClientRect().left}))+"px":void 0}),n.cssHooks.marginRight=Ga(l.reliableMarginRight,function(a,b){return b?Da(a,{display:"inline-block"},Fa,[a,"marginRight"]):void 0}),n.each({margin:"",padding:"",border:"Width"},function(a,b){n.cssHooks[a+b]={expand:function(c){for(var d=0,e={},f="string"==typeof c?c.split(" "):[c];4>d;d++)e[a+U[d]+b]=f[d]||f[d-2]||f[0];return e}},Aa.test(a)||(n.cssHooks[a+b].set=Na)}),n.fn.extend({css:function(a,b){return K(this,function(a,b,c){var d,e,f={},g=0;if(n.isArray(b)){for(d=Ca(a),e=b.length;e>g;g++)f[b[g]]=n.css(a,b[g],!1,d);return f}return void 0!==c?n.style(a,b,c):n.css(a,b)},a,b,arguments.length>1)},show:function(){return Qa(this,!0)},hide:function(){return Qa(this)},toggle:function(a){return"boolean"==typeof a?a?this.show():this.hide():this.each(function(){V(this)?n(this).show():n(this).hide()})}});function Ra(a,b,c,d,e){return new Ra.prototype.init(a,b,c,d,e)}n.Tween=Ra,Ra.prototype={constructor:Ra,init:function(a,b,c,d,e,f){this.elem=a,this.prop=c,this.easing=e||n.easing._default,this.options=b,this.start=this.now=this.cur(),this.end=d,this.unit=f||(n.cssNumber[c]?"":"px")},cur:function(){var a=Ra.propHooks[this.prop];return a&&a.get?a.get(this):Ra.propHooks._default.get(this)},run:function(a){var b,c=Ra.propHooks[this.prop];return this.options.duration?this.pos=b=n.easing[this.easing](a,this.options.duration*a,0,1,this.options.duration):this.pos=b=a,this.now=(this.end-this.start)*b+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),c&&c.set?c.set(this):Ra.propHooks._default.set(this),this}},Ra.prototype.init.prototype=Ra.prototype,Ra.propHooks={_default:{get:function(a){var b;return 1!==a.elem.nodeType||null!=a.elem[a.prop]&&null==a.elem.style[a.prop]?a.elem[a.prop]:(b=n.css(a.elem,a.prop,""),b&&"auto"!==b?b:0)},set:function(a){n.fx.step[a.prop]?n.fx.step[a.prop](a):1!==a.elem.nodeType||null==a.elem.style[n.cssProps[a.prop]]&&!n.cssHooks[a.prop]?a.elem[a.prop]=a.now:n.style(a.elem,a.prop,a.now+a.unit)}}},Ra.propHooks.scrollTop=Ra.propHooks.scrollLeft={set:function(a){a.elem.nodeType&&a.elem.parentNode&&(a.elem[a.prop]=a.now)}},n.easing={linear:function(a){return a},swing:function(a){return.5-Math.cos(a*Math.PI)/2},_default:"swing"},n.fx=Ra.prototype.init,n.fx.step={};var Sa,Ta,Ua=/^(?:toggle|show|hide)$/,Va=/queueHooks$/;function Wa(){return a.setTimeout(function(){Sa=void 0}),Sa=n.now()}function Xa(a,b){var c,d=0,e={height:a};for(b=b?1:0;4>d;d+=2-b)c=U[d],e["margin"+c]=e["padding"+c]=a;return b&&(e.opacity=e.width=a),e}function Ya(a,b,c){for(var d,e=(_a.tweeners[b]||[]).concat(_a.tweeners["*"]),f=0,g=e.length;g>f;f++)if(d=e[f].call(c,b,a))return d}function Za(a,b,c){var d,e,f,g,h,i,j,k,l=this,m={},o=a.style,p=a.nodeType&&V(a),q=N.get(a,"fxshow");c.queue||(h=n._queueHooks(a,"fx"),null==h.unqueued&&(h.unqueued=0,i=h.empty.fire,h.empty.fire=function(){h.unqueued||i()}),h.unqueued++,l.always(function(){l.always(function(){h.unqueued--,n.queue(a,"fx").length||h.empty.fire()})})),1===a.nodeType&&("height"in b||"width"in b)&&(c.overflow=[o.overflow,o.overflowX,o.overflowY],j=n.css(a,"display"),k="none"===j?N.get(a,"olddisplay")||za(a.nodeName):j,"inline"===k&&"none"===n.css(a,"float")&&(o.display="inline-block")),c.overflow&&(o.overflow="hidden",l.always(function(){o.overflow=c.overflow[0],o.overflowX=c.overflow[1],o.overflowY=c.overflow[2]}));for(d in b)if(e=b[d],Ua.exec(e)){if(delete b[d],f=f||"toggle"===e,e===(p?"hide":"show")){if("show"!==e||!q||void 0===q[d])continue;p=!0}m[d]=q&&q[d]||n.style(a,d)}else j=void 0;if(n.isEmptyObject(m))"inline"===("none"===j?za(a.nodeName):j)&&(o.display=j);else{q?"hidden"in q&&(p=q.hidden):q=N.access(a,"fxshow",{}),f&&(q.hidden=!p),p?n(a).show():l.done(function(){n(a).hide()}),l.done(function(){var b;N.remove(a,"fxshow");for(b in m)n.style(a,b,m[b])});for(d in m)g=Ya(p?q[d]:0,d,l),d in q||(q[d]=g.start,p&&(g.end=g.start,g.start="width"===d||"height"===d?1:0))}}function $a(a,b){var c,d,e,f,g;for(c in a)if(d=n.camelCase(c),e=b[d],f=a[c],n.isArray(f)&&(e=f[1],f=a[c]=f[0]),c!==d&&(a[d]=f,delete a[c]),g=n.cssHooks[d],g&&"expand"in g){f=g.expand(f),delete a[d];for(c in f)c in a||(a[c]=f[c],b[c]=e)}else b[d]=e}function _a(a,b,c){var d,e,f=0,g=_a.prefilters.length,h=n.Deferred().always(function(){delete i.elem}),i=function(){if(e)return!1;for(var b=Sa||Wa(),c=Math.max(0,j.startTime+j.duration-b),d=c/j.duration||0,f=1-d,g=0,i=j.tweens.length;i>g;g++)j.tweens[g].run(f);return h.notifyWith(a,[j,f,c]),1>f&&i?c:(h.resolveWith(a,[j]),!1)},j=h.promise({elem:a,props:n.extend({},b),opts:n.extend(!0,{specialEasing:{},easing:n.easing._default},c),originalProperties:b,originalOptions:c,startTime:Sa||Wa(),duration:c.duration,tweens:[],createTween:function(b,c){var d=n.Tween(a,j.opts,b,c,j.opts.specialEasing[b]||j.opts.easing);return j.tweens.push(d),d},stop:function(b){var c=0,d=b?j.tweens.length:0;if(e)return this;for(e=!0;d>c;c++)j.tweens[c].run(1);return b?(h.notifyWith(a,[j,1,0]),h.resolveWith(a,[j,b])):h.rejectWith(a,[j,b]),this}}),k=j.props;for($a(k,j.opts.specialEasing);g>f;f++)if(d=_a.prefilters[f].call(j,a,k,j.opts))return n.isFunction(d.stop)&&(n._queueHooks(j.elem,j.opts.queue).stop=n.proxy(d.stop,d)),d;return n.map(k,Ya,j),n.isFunction(j.opts.start)&&j.opts.start.call(a,j),n.fx.timer(n.extend(i,{elem:a,anim:j,queue:j.opts.queue})),j.progress(j.opts.progress).done(j.opts.done,j.opts.complete).fail(j.opts.fail).always(j.opts.always)}n.Animation=n.extend(_a,{tweeners:{"*":[function(a,b){var c=this.createTween(a,b);return W(c.elem,a,T.exec(b),c),c}]},tweener:function(a,b){n.isFunction(a)?(b=a,a=["*"]):a=a.match(G);for(var c,d=0,e=a.length;e>d;d++)c=a[d],_a.tweeners[c]=_a.tweeners[c]||[],_a.tweeners[c].unshift(b)},prefilters:[Za],prefilter:function(a,b){b?_a.prefilters.unshift(a):_a.prefilters.push(a)}}),n.speed=function(a,b,c){var d=a&&"object"==typeof a?n.extend({},a):{complete:c||!c&&b||n.isFunction(a)&&a,duration:a,easing:c&&b||b&&!n.isFunction(b)&&b};return d.duration=n.fx.off?0:"number"==typeof d.duration?d.duration:d.duration in n.fx.speeds?n.fx.speeds[d.duration]:n.fx.speeds._default,null!=d.queue&&d.queue!==!0||(d.queue="fx"),d.old=d.complete,d.complete=function(){n.isFunction(d.old)&&d.old.call(this),d.queue&&n.dequeue(this,d.queue)},d},n.fn.extend({fadeTo:function(a,b,c,d){return this.filter(V).css("opacity",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){var e=n.isEmptyObject(a),f=n.speed(b,c,d),g=function(){var b=_a(this,n.extend({},a),f);(e||N.get(this,"finish"))&&b.stop(!0)};return g.finish=g,e||f.queue===!1?this.each(g):this.queue(f.queue,g)},stop:function(a,b,c){var d=function(a){var b=a.stop;delete a.stop,b(c)};return"string"!=typeof a&&(c=b,b=a,a=void 0),b&&a!==!1&&this.queue(a||"fx",[]),this.each(function(){var b=!0,e=null!=a&&a+"queueHooks",f=n.timers,g=N.get(this);if(e)g[e]&&g[e].stop&&d(g[e]);else for(e in g)g[e]&&g[e].stop&&Va.test(e)&&d(g[e]);for(e=f.length;e--;)f[e].elem!==this||null!=a&&f[e].queue!==a||(f[e].anim.stop(c),b=!1,f.splice(e,1));!b&&c||n.dequeue(this,a)})},finish:function(a){return a!==!1&&(a=a||"fx"),this.each(function(){var b,c=N.get(this),d=c[a+"queue"],e=c[a+"queueHooks"],f=n.timers,g=d?d.length:0;for(c.finish=!0,n.queue(this,a,[]),e&&e.stop&&e.stop.call(this,!0),b=f.length;b--;)f[b].elem===this&&f[b].queue===a&&(f[b].anim.stop(!0),f.splice(b,1));for(b=0;g>b;b++)d[b]&&d[b].finish&&d[b].finish.call(this);delete c.finish})}}),n.each(["toggle","show","hide"],function(a,b){var c=n.fn[b];n.fn[b]=function(a,d,e){return null==a||"boolean"==typeof a?c.apply(this,arguments):this.animate(Xa(b,!0),a,d,e)}}),n.each({slideDown:Xa("show"),slideUp:Xa("hide"),slideToggle:Xa("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){n.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),n.timers=[],n.fx.tick=function(){var a,b=0,c=n.timers;for(Sa=n.now();b<c.length;b++)a=c[b],a()||c[b]!==a||c.splice(b--,1);c.length||n.fx.stop(),Sa=void 0},n.fx.timer=function(a){n.timers.push(a),a()?n.fx.start():n.timers.pop()},n.fx.interval=13,n.fx.start=function(){Ta||(Ta=a.setInterval(n.fx.tick,n.fx.interval))},n.fx.stop=function(){a.clearInterval(Ta),Ta=null},n.fx.speeds={slow:600,fast:200,_default:400},n.fn.delay=function(b,c){return b=n.fx?n.fx.speeds[b]||b:b,c=c||"fx",this.queue(c,function(c,d){var e=a.setTimeout(c,b);d.stop=function(){a.clearTimeout(e)}})},function(){var a=d.createElement("input"),b=d.createElement("select"),c=b.appendChild(d.createElement("option"));a.type="checkbox",l.checkOn=""!==a.value,l.optSelected=c.selected,b.disabled=!0,l.optDisabled=!c.disabled,a=d.createElement("input"),a.value="t",a.type="radio",l.radioValue="t"===a.value}();var ab,bb=n.expr.attrHandle;n.fn.extend({attr:function(a,b){return K(this,n.attr,a,b,arguments.length>1)},removeAttr:function(a){return this.each(function(){n.removeAttr(this,a)})}}),n.extend({attr:function(a,b,c){var d,e,f=a.nodeType;if(3!==f&&8!==f&&2!==f)return"undefined"==typeof a.getAttribute?n.prop(a,b,c):(1===f&&n.isXMLDoc(a)||(b=b.toLowerCase(),e=n.attrHooks[b]||(n.expr.match.bool.test(b)?ab:void 0)),void 0!==c?null===c?void n.removeAttr(a,b):e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:(a.setAttribute(b,c+""),c):e&&"get"in e&&null!==(d=e.get(a,b))?d:(d=n.find.attr(a,b),null==d?void 0:d))},attrHooks:{type:{set:function(a,b){if(!l.radioValue&&"radio"===b&&n.nodeName(a,"input")){var c=a.value;return a.setAttribute("type",b),c&&(a.value=c),b}}}},removeAttr:function(a,b){var c,d,e=0,f=b&&b.match(G);if(f&&1===a.nodeType)while(c=f[e++])d=n.propFix[c]||c,n.expr.match.bool.test(c)&&(a[d]=!1),a.removeAttribute(c)}}),ab={set:function(a,b,c){return b===!1?n.removeAttr(a,c):a.setAttribute(c,c),c}},n.each(n.expr.match.bool.source.match(/\w+/g),function(a,b){var c=bb[b]||n.find.attr;bb[b]=function(a,b,d){var e,f;return d||(f=bb[b],bb[b]=e,e=null!=c(a,b,d)?b.toLowerCase():null,bb[b]=f),e}});var cb=/^(?:input|select|textarea|button)$/i,db=/^(?:a|area)$/i;n.fn.extend({prop:function(a,b){return K(this,n.prop,a,b,arguments.length>1)},removeProp:function(a){return this.each(function(){delete this[n.propFix[a]||a]})}}),n.extend({prop:function(a,b,c){var d,e,f=a.nodeType;if(3!==f&&8!==f&&2!==f)return 1===f&&n.isXMLDoc(a)||(b=n.propFix[b]||b,e=n.propHooks[b]),
void 0!==c?e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:a[b]=c:e&&"get"in e&&null!==(d=e.get(a,b))?d:a[b]},propHooks:{tabIndex:{get:function(a){var b=n.find.attr(a,"tabindex");return b?parseInt(b,10):cb.test(a.nodeName)||db.test(a.nodeName)&&a.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}}),l.optSelected||(n.propHooks.selected={get:function(a){var b=a.parentNode;return b&&b.parentNode&&b.parentNode.selectedIndex,null},set:function(a){var b=a.parentNode;b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex)}}),n.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){n.propFix[this.toLowerCase()]=this});var eb=/[\t\r\n\f]/g;function fb(a){return a.getAttribute&&a.getAttribute("class")||""}n.fn.extend({addClass:function(a){var b,c,d,e,f,g,h,i=0;if(n.isFunction(a))return this.each(function(b){n(this).addClass(a.call(this,b,fb(this)))});if("string"==typeof a&&a){b=a.match(G)||[];while(c=this[i++])if(e=fb(c),d=1===c.nodeType&&(" "+e+" ").replace(eb," ")){g=0;while(f=b[g++])d.indexOf(" "+f+" ")<0&&(d+=f+" ");h=n.trim(d),e!==h&&c.setAttribute("class",h)}}return this},removeClass:function(a){var b,c,d,e,f,g,h,i=0;if(n.isFunction(a))return this.each(function(b){n(this).removeClass(a.call(this,b,fb(this)))});if(!arguments.length)return this.attr("class","");if("string"==typeof a&&a){b=a.match(G)||[];while(c=this[i++])if(e=fb(c),d=1===c.nodeType&&(" "+e+" ").replace(eb," ")){g=0;while(f=b[g++])while(d.indexOf(" "+f+" ")>-1)d=d.replace(" "+f+" "," ");h=n.trim(d),e!==h&&c.setAttribute("class",h)}}return this},toggleClass:function(a,b){var c=typeof a;return"boolean"==typeof b&&"string"===c?b?this.addClass(a):this.removeClass(a):n.isFunction(a)?this.each(function(c){n(this).toggleClass(a.call(this,c,fb(this),b),b)}):this.each(function(){var b,d,e,f;if("string"===c){d=0,e=n(this),f=a.match(G)||[];while(b=f[d++])e.hasClass(b)?e.removeClass(b):e.addClass(b)}else void 0!==a&&"boolean"!==c||(b=fb(this),b&&N.set(this,"__className__",b),this.setAttribute&&this.setAttribute("class",b||a===!1?"":N.get(this,"__className__")||""))})},hasClass:function(a){var b,c,d=0;b=" "+a+" ";while(c=this[d++])if(1===c.nodeType&&(" "+fb(c)+" ").replace(eb," ").indexOf(b)>-1)return!0;return!1}});var gb=/\r/g,hb=/[\x20\t\r\n\f]+/g;n.fn.extend({val:function(a){var b,c,d,e=this[0];{if(arguments.length)return d=n.isFunction(a),this.each(function(c){var e;1===this.nodeType&&(e=d?a.call(this,c,n(this).val()):a,null==e?e="":"number"==typeof e?e+="":n.isArray(e)&&(e=n.map(e,function(a){return null==a?"":a+""})),b=n.valHooks[this.type]||n.valHooks[this.nodeName.toLowerCase()],b&&"set"in b&&void 0!==b.set(this,e,"value")||(this.value=e))});if(e)return b=n.valHooks[e.type]||n.valHooks[e.nodeName.toLowerCase()],b&&"get"in b&&void 0!==(c=b.get(e,"value"))?c:(c=e.value,"string"==typeof c?c.replace(gb,""):null==c?"":c)}}}),n.extend({valHooks:{option:{get:function(a){var b=n.find.attr(a,"value");return null!=b?b:n.trim(n.text(a)).replace(hb," ")}},select:{get:function(a){for(var b,c,d=a.options,e=a.selectedIndex,f="select-one"===a.type||0>e,g=f?null:[],h=f?e+1:d.length,i=0>e?h:f?e:0;h>i;i++)if(c=d[i],(c.selected||i===e)&&(l.optDisabled?!c.disabled:null===c.getAttribute("disabled"))&&(!c.parentNode.disabled||!n.nodeName(c.parentNode,"optgroup"))){if(b=n(c).val(),f)return b;g.push(b)}return g},set:function(a,b){var c,d,e=a.options,f=n.makeArray(b),g=e.length;while(g--)d=e[g],(d.selected=n.inArray(n.valHooks.option.get(d),f)>-1)&&(c=!0);return c||(a.selectedIndex=-1),f}}}}),n.each(["radio","checkbox"],function(){n.valHooks[this]={set:function(a,b){return n.isArray(b)?a.checked=n.inArray(n(a).val(),b)>-1:void 0}},l.checkOn||(n.valHooks[this].get=function(a){return null===a.getAttribute("value")?"on":a.value})});var ib=/^(?:focusinfocus|focusoutblur)$/;n.extend(n.event,{trigger:function(b,c,e,f){var g,h,i,j,l,m,o,p=[e||d],q=k.call(b,"type")?b.type:b,r=k.call(b,"namespace")?b.namespace.split("."):[];if(h=i=e=e||d,3!==e.nodeType&&8!==e.nodeType&&!ib.test(q+n.event.triggered)&&(q.indexOf(".")>-1&&(r=q.split("."),q=r.shift(),r.sort()),l=q.indexOf(":")<0&&"on"+q,b=b[n.expando]?b:new n.Event(q,"object"==typeof b&&b),b.isTrigger=f?2:3,b.namespace=r.join("."),b.rnamespace=b.namespace?new RegExp("(^|\\.)"+r.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,b.result=void 0,b.target||(b.target=e),c=null==c?[b]:n.makeArray(c,[b]),o=n.event.special[q]||{},f||!o.trigger||o.trigger.apply(e,c)!==!1)){if(!f&&!o.noBubble&&!n.isWindow(e)){for(j=o.delegateType||q,ib.test(j+q)||(h=h.parentNode);h;h=h.parentNode)p.push(h),i=h;i===(e.ownerDocument||d)&&p.push(i.defaultView||i.parentWindow||a)}g=0;while((h=p[g++])&&!b.isPropagationStopped())b.type=g>1?j:o.bindType||q,m=(N.get(h,"events")||{})[b.type]&&N.get(h,"handle"),m&&m.apply(h,c),m=l&&h[l],m&&m.apply&&L(h)&&(b.result=m.apply(h,c),b.result===!1&&b.preventDefault());return b.type=q,f||b.isDefaultPrevented()||o._default&&o._default.apply(p.pop(),c)!==!1||!L(e)||l&&n.isFunction(e[q])&&!n.isWindow(e)&&(i=e[l],i&&(e[l]=null),n.event.triggered=q,e[q](),n.event.triggered=void 0,i&&(e[l]=i)),b.result}},simulate:function(a,b,c){var d=n.extend(new n.Event,c,{type:a,isSimulated:!0});n.event.trigger(d,null,b)}}),n.fn.extend({trigger:function(a,b){return this.each(function(){n.event.trigger(a,b,this)})},triggerHandler:function(a,b){var c=this[0];return c?n.event.trigger(a,b,c,!0):void 0}}),n.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(a,b){n.fn[b]=function(a,c){return arguments.length>0?this.on(b,null,a,c):this.trigger(b)}}),n.fn.extend({hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)}}),l.focusin="onfocusin"in a,l.focusin||n.each({focus:"focusin",blur:"focusout"},function(a,b){var c=function(a){n.event.simulate(b,a.target,n.event.fix(a))};n.event.special[b]={setup:function(){var d=this.ownerDocument||this,e=N.access(d,b);e||d.addEventListener(a,c,!0),N.access(d,b,(e||0)+1)},teardown:function(){var d=this.ownerDocument||this,e=N.access(d,b)-1;e?N.access(d,b,e):(d.removeEventListener(a,c,!0),N.remove(d,b))}}});var jb=a.location,kb=n.now(),lb=/\?/;n.parseJSON=function(a){return JSON.parse(a+"")},n.parseXML=function(b){var c;if(!b||"string"!=typeof b)return null;try{c=(new a.DOMParser).parseFromString(b,"text/xml")}catch(d){c=void 0}return c&&!c.getElementsByTagName("parsererror").length||n.error("Invalid XML: "+b),c};var mb=/#.*$/,nb=/([?&])_=[^&]*/,ob=/^(.*?):[ \t]*([^\r\n]*)$/gm,pb=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,qb=/^(?:GET|HEAD)$/,rb=/^\/\//,sb={},tb={},ub="*/".concat("*"),vb=d.createElement("a");vb.href=jb.href;function wb(a){return function(b,c){"string"!=typeof b&&(c=b,b="*");var d,e=0,f=b.toLowerCase().match(G)||[];if(n.isFunction(c))while(d=f[e++])"+"===d[0]?(d=d.slice(1)||"*",(a[d]=a[d]||[]).unshift(c)):(a[d]=a[d]||[]).push(c)}}function xb(a,b,c,d){var e={},f=a===tb;function g(h){var i;return e[h]=!0,n.each(a[h]||[],function(a,h){var j=h(b,c,d);return"string"!=typeof j||f||e[j]?f?!(i=j):void 0:(b.dataTypes.unshift(j),g(j),!1)}),i}return g(b.dataTypes[0])||!e["*"]&&g("*")}function yb(a,b){var c,d,e=n.ajaxSettings.flatOptions||{};for(c in b)void 0!==b[c]&&((e[c]?a:d||(d={}))[c]=b[c]);return d&&n.extend(!0,a,d),a}function zb(a,b,c){var d,e,f,g,h=a.contents,i=a.dataTypes;while("*"===i[0])i.shift(),void 0===d&&(d=a.mimeType||b.getResponseHeader("Content-Type"));if(d)for(e in h)if(h[e]&&h[e].test(d)){i.unshift(e);break}if(i[0]in c)f=i[0];else{for(e in c){if(!i[0]||a.converters[e+" "+i[0]]){f=e;break}g||(g=e)}f=f||g}return f?(f!==i[0]&&i.unshift(f),c[f]):void 0}function Ab(a,b,c,d){var e,f,g,h,i,j={},k=a.dataTypes.slice();if(k[1])for(g in a.converters)j[g.toLowerCase()]=a.converters[g];f=k.shift();while(f)if(a.responseFields[f]&&(c[a.responseFields[f]]=b),!i&&d&&a.dataFilter&&(b=a.dataFilter(b,a.dataType)),i=f,f=k.shift())if("*"===f)f=i;else if("*"!==i&&i!==f){if(g=j[i+" "+f]||j["* "+f],!g)for(e in j)if(h=e.split(" "),h[1]===f&&(g=j[i+" "+h[0]]||j["* "+h[0]])){g===!0?g=j[e]:j[e]!==!0&&(f=h[0],k.unshift(h[1]));break}if(g!==!0)if(g&&a["throws"])b=g(b);else try{b=g(b)}catch(l){return{state:"parsererror",error:g?l:"No conversion from "+i+" to "+f}}}return{state:"success",data:b}}n.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:jb.href,type:"GET",isLocal:pb.test(jb.protocol),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":ub,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":n.parseJSON,"text xml":n.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(a,b){return b?yb(yb(a,n.ajaxSettings),b):yb(n.ajaxSettings,a)},ajaxPrefilter:wb(sb),ajaxTransport:wb(tb),ajax:function(b,c){"object"==typeof b&&(c=b,b=void 0),c=c||{};var e,f,g,h,i,j,k,l,m=n.ajaxSetup({},c),o=m.context||m,p=m.context&&(o.nodeType||o.jquery)?n(o):n.event,q=n.Deferred(),r=n.Callbacks("once memory"),s=m.statusCode||{},t={},u={},v=0,w="canceled",x={readyState:0,getResponseHeader:function(a){var b;if(2===v){if(!h){h={};while(b=ob.exec(g))h[b[1].toLowerCase()]=b[2]}b=h[a.toLowerCase()]}return null==b?null:b},getAllResponseHeaders:function(){return 2===v?g:null},setRequestHeader:function(a,b){var c=a.toLowerCase();return v||(a=u[c]=u[c]||a,t[a]=b),this},overrideMimeType:function(a){return v||(m.mimeType=a),this},statusCode:function(a){var b;if(a)if(2>v)for(b in a)s[b]=[s[b],a[b]];else x.always(a[x.status]);return this},abort:function(a){var b=a||w;return e&&e.abort(b),z(0,b),this}};if(q.promise(x).complete=r.add,x.success=x.done,x.error=x.fail,m.url=((b||m.url||jb.href)+"").replace(mb,"").replace(rb,jb.protocol+"//"),m.type=c.method||c.type||m.method||m.type,m.dataTypes=n.trim(m.dataType||"*").toLowerCase().match(G)||[""],null==m.crossDomain){j=d.createElement("a");try{j.href=m.url,j.href=j.href,m.crossDomain=vb.protocol+"//"+vb.host!=j.protocol+"//"+j.host}catch(y){m.crossDomain=!0}}if(m.data&&m.processData&&"string"!=typeof m.data&&(m.data=n.param(m.data,m.traditional)),xb(sb,m,c,x),2===v)return x;k=n.event&&m.global,k&&0===n.active++&&n.event.trigger("ajaxStart"),m.type=m.type.toUpperCase(),m.hasContent=!qb.test(m.type),f=m.url,m.hasContent||(m.data&&(f=m.url+=(lb.test(f)?"&":"?")+m.data,delete m.data),m.cache===!1&&(m.url=nb.test(f)?f.replace(nb,"$1_="+kb++):f+(lb.test(f)?"&":"?")+"_="+kb++)),m.ifModified&&(n.lastModified[f]&&x.setRequestHeader("If-Modified-Since",n.lastModified[f]),n.etag[f]&&x.setRequestHeader("If-None-Match",n.etag[f])),(m.data&&m.hasContent&&m.contentType!==!1||c.contentType)&&x.setRequestHeader("Content-Type",m.contentType),x.setRequestHeader("Accept",m.dataTypes[0]&&m.accepts[m.dataTypes[0]]?m.accepts[m.dataTypes[0]]+("*"!==m.dataTypes[0]?", "+ub+"; q=0.01":""):m.accepts["*"]);for(l in m.headers)x.setRequestHeader(l,m.headers[l]);if(m.beforeSend&&(m.beforeSend.call(o,x,m)===!1||2===v))return x.abort();w="abort";for(l in{success:1,error:1,complete:1})x[l](m[l]);if(e=xb(tb,m,c,x)){if(x.readyState=1,k&&p.trigger("ajaxSend",[x,m]),2===v)return x;m.async&&m.timeout>0&&(i=a.setTimeout(function(){x.abort("timeout")},m.timeout));try{v=1,e.send(t,z)}catch(y){if(!(2>v))throw y;z(-1,y)}}else z(-1,"No Transport");function z(b,c,d,h){var j,l,t,u,w,y=c;2!==v&&(v=2,i&&a.clearTimeout(i),e=void 0,g=h||"",x.readyState=b>0?4:0,j=b>=200&&300>b||304===b,d&&(u=zb(m,x,d)),u=Ab(m,u,x,j),j?(m.ifModified&&(w=x.getResponseHeader("Last-Modified"),w&&(n.lastModified[f]=w),w=x.getResponseHeader("etag"),w&&(n.etag[f]=w)),204===b||"HEAD"===m.type?y="nocontent":304===b?y="notmodified":(y=u.state,l=u.data,t=u.error,j=!t)):(t=y,!b&&y||(y="error",0>b&&(b=0))),x.status=b,x.statusText=(c||y)+"",j?q.resolveWith(o,[l,y,x]):q.rejectWith(o,[x,y,t]),x.statusCode(s),s=void 0,k&&p.trigger(j?"ajaxSuccess":"ajaxError",[x,m,j?l:t]),r.fireWith(o,[x,y]),k&&(p.trigger("ajaxComplete",[x,m]),--n.active||n.event.trigger("ajaxStop")))}return x},getJSON:function(a,b,c){return n.get(a,b,c,"json")},getScript:function(a,b){return n.get(a,void 0,b,"script")}}),n.each(["get","post"],function(a,b){n[b]=function(a,c,d,e){return n.isFunction(c)&&(e=e||d,d=c,c=void 0),n.ajax(n.extend({url:a,type:b,dataType:e,data:c,success:d},n.isPlainObject(a)&&a))}}),n._evalUrl=function(a){return n.ajax({url:a,type:"GET",dataType:"script",async:!1,global:!1,"throws":!0})},n.fn.extend({wrapAll:function(a){var b;return n.isFunction(a)?this.each(function(b){n(this).wrapAll(a.call(this,b))}):(this[0]&&(b=n(a,this[0].ownerDocument).eq(0).clone(!0),this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstElementChild)a=a.firstElementChild;return a}).append(this)),this)},wrapInner:function(a){return n.isFunction(a)?this.each(function(b){n(this).wrapInner(a.call(this,b))}):this.each(function(){var b=n(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=n.isFunction(a);return this.each(function(c){n(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(){return this.parent().each(function(){n.nodeName(this,"body")||n(this).replaceWith(this.childNodes)}).end()}}),n.expr.filters.hidden=function(a){return!n.expr.filters.visible(a)},n.expr.filters.visible=function(a){return a.offsetWidth>0||a.offsetHeight>0||a.getClientRects().length>0};var Bb=/%20/g,Cb=/\[\]$/,Db=/\r?\n/g,Eb=/^(?:submit|button|image|reset|file)$/i,Fb=/^(?:input|select|textarea|keygen)/i;function Gb(a,b,c,d){var e;if(n.isArray(b))n.each(b,function(b,e){c||Cb.test(a)?d(a,e):Gb(a+"["+("object"==typeof e&&null!=e?b:"")+"]",e,c,d)});else if(c||"object"!==n.type(b))d(a,b);else for(e in b)Gb(a+"["+e+"]",b[e],c,d)}n.param=function(a,b){var c,d=[],e=function(a,b){b=n.isFunction(b)?b():null==b?"":b,d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(b)};if(void 0===b&&(b=n.ajaxSettings&&n.ajaxSettings.traditional),n.isArray(a)||a.jquery&&!n.isPlainObject(a))n.each(a,function(){e(this.name,this.value)});else for(c in a)Gb(c,a[c],b,e);return d.join("&").replace(Bb,"+")},n.fn.extend({serialize:function(){return n.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var a=n.prop(this,"elements");return a?n.makeArray(a):this}).filter(function(){var a=this.type;return this.name&&!n(this).is(":disabled")&&Fb.test(this.nodeName)&&!Eb.test(a)&&(this.checked||!X.test(a))}).map(function(a,b){var c=n(this).val();return null==c?null:n.isArray(c)?n.map(c,function(a){return{name:b.name,value:a.replace(Db,"\r\n")}}):{name:b.name,value:c.replace(Db,"\r\n")}}).get()}}),n.ajaxSettings.xhr=function(){try{return new a.XMLHttpRequest}catch(b){}};var Hb={0:200,1223:204},Ib=n.ajaxSettings.xhr();l.cors=!!Ib&&"withCredentials"in Ib,l.ajax=Ib=!!Ib,n.ajaxTransport(function(b){var c,d;return l.cors||Ib&&!b.crossDomain?{send:function(e,f){var g,h=b.xhr();if(h.open(b.type,b.url,b.async,b.username,b.password),b.xhrFields)for(g in b.xhrFields)h[g]=b.xhrFields[g];b.mimeType&&h.overrideMimeType&&h.overrideMimeType(b.mimeType),b.crossDomain||e["X-Requested-With"]||(e["X-Requested-With"]="XMLHttpRequest");for(g in e)h.setRequestHeader(g,e[g]);c=function(a){return function(){c&&(c=d=h.onload=h.onerror=h.onabort=h.onreadystatechange=null,"abort"===a?h.abort():"error"===a?"number"!=typeof h.status?f(0,"error"):f(h.status,h.statusText):f(Hb[h.status]||h.status,h.statusText,"text"!==(h.responseType||"text")||"string"!=typeof h.responseText?{binary:h.response}:{text:h.responseText},h.getAllResponseHeaders()))}},h.onload=c(),d=h.onerror=c("error"),void 0!==h.onabort?h.onabort=d:h.onreadystatechange=function(){4===h.readyState&&a.setTimeout(function(){c&&d()})},c=c("abort");try{h.send(b.hasContent&&b.data||null)}catch(i){if(c)throw i}},abort:function(){c&&c()}}:void 0}),n.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(a){return n.globalEval(a),a}}}),n.ajaxPrefilter("script",function(a){void 0===a.cache&&(a.cache=!1),a.crossDomain&&(a.type="GET")}),n.ajaxTransport("script",function(a){if(a.crossDomain){var b,c;return{send:function(e,f){b=n("<script>").prop({charset:a.scriptCharset,src:a.url}).on("load error",c=function(a){b.remove(),c=null,a&&f("error"===a.type?404:200,a.type)}),d.head.appendChild(b[0])},abort:function(){c&&c()}}}});var Jb=[],Kb=/(=)\?(?=&|$)|\?\?/;n.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var a=Jb.pop()||n.expando+"_"+kb++;return this[a]=!0,a}}),n.ajaxPrefilter("json jsonp",function(b,c,d){var e,f,g,h=b.jsonp!==!1&&(Kb.test(b.url)?"url":"string"==typeof b.data&&0===(b.contentType||"").indexOf("application/x-www-form-urlencoded")&&Kb.test(b.data)&&"data");return h||"jsonp"===b.dataTypes[0]?(e=b.jsonpCallback=n.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,h?b[h]=b[h].replace(Kb,"$1"+e):b.jsonp!==!1&&(b.url+=(lb.test(b.url)?"&":"?")+b.jsonp+"="+e),b.converters["script json"]=function(){return g||n.error(e+" was not called"),g[0]},b.dataTypes[0]="json",f=a[e],a[e]=function(){g=arguments},d.always(function(){void 0===f?n(a).removeProp(e):a[e]=f,b[e]&&(b.jsonpCallback=c.jsonpCallback,Jb.push(e)),g&&n.isFunction(f)&&f(g[0]),g=f=void 0}),"script"):void 0}),n.parseHTML=function(a,b,c){if(!a||"string"!=typeof a)return null;"boolean"==typeof b&&(c=b,b=!1),b=b||d;var e=x.exec(a),f=!c&&[];return e?[b.createElement(e[1])]:(e=ca([a],b,f),f&&f.length&&n(f).remove(),n.merge([],e.childNodes))};var Lb=n.fn.load;n.fn.load=function(a,b,c){if("string"!=typeof a&&Lb)return Lb.apply(this,arguments);var d,e,f,g=this,h=a.indexOf(" ");return h>-1&&(d=n.trim(a.slice(h)),a=a.slice(0,h)),n.isFunction(b)?(c=b,b=void 0):b&&"object"==typeof b&&(e="POST"),g.length>0&&n.ajax({url:a,type:e||"GET",dataType:"html",data:b}).done(function(a){f=arguments,g.html(d?n("<div>").append(n.parseHTML(a)).find(d):a)}).always(c&&function(a,b){g.each(function(){c.apply(this,f||[a.responseText,b,a])})}),this},n.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(a,b){n.fn[b]=function(a){return this.on(b,a)}}),n.expr.filters.animated=function(a){return n.grep(n.timers,function(b){return a===b.elem}).length};function Mb(a){return n.isWindow(a)?a:9===a.nodeType&&a.defaultView}n.offset={setOffset:function(a,b,c){var d,e,f,g,h,i,j,k=n.css(a,"position"),l=n(a),m={};"static"===k&&(a.style.position="relative"),h=l.offset(),f=n.css(a,"top"),i=n.css(a,"left"),j=("absolute"===k||"fixed"===k)&&(f+i).indexOf("auto")>-1,j?(d=l.position(),g=d.top,e=d.left):(g=parseFloat(f)||0,e=parseFloat(i)||0),n.isFunction(b)&&(b=b.call(a,c,n.extend({},h))),null!=b.top&&(m.top=b.top-h.top+g),null!=b.left&&(m.left=b.left-h.left+e),"using"in b?b.using.call(a,m):l.css(m)}},n.fn.extend({offset:function(a){if(arguments.length)return void 0===a?this:this.each(function(b){n.offset.setOffset(this,a,b)});var b,c,d=this[0],e={top:0,left:0},f=d&&d.ownerDocument;if(f)return b=f.documentElement,n.contains(b,d)?(e=d.getBoundingClientRect(),c=Mb(f),{top:e.top+c.pageYOffset-b.clientTop,left:e.left+c.pageXOffset-b.clientLeft}):e},position:function(){if(this[0]){var a,b,c=this[0],d={top:0,left:0};return"fixed"===n.css(c,"position")?b=c.getBoundingClientRect():(a=this.offsetParent(),b=this.offset(),n.nodeName(a[0],"html")||(d=a.offset()),d.top+=n.css(a[0],"borderTopWidth",!0),d.left+=n.css(a[0],"borderLeftWidth",!0)),{top:b.top-d.top-n.css(c,"marginTop",!0),left:b.left-d.left-n.css(c,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var a=this.offsetParent;while(a&&"static"===n.css(a,"position"))a=a.offsetParent;return a||Ea})}}),n.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(a,b){var c="pageYOffset"===b;n.fn[a]=function(d){return K(this,function(a,d,e){var f=Mb(a);return void 0===e?f?f[b]:a[d]:void(f?f.scrollTo(c?f.pageXOffset:e,c?e:f.pageYOffset):a[d]=e)},a,d,arguments.length)}}),n.each(["top","left"],function(a,b){n.cssHooks[b]=Ga(l.pixelPosition,function(a,c){return c?(c=Fa(a,b),Ba.test(c)?n(a).position()[b]+"px":c):void 0})}),n.each({Height:"height",Width:"width"},function(a,b){n.each({padding:"inner"+a,content:b,"":"outer"+a},function(c,d){n.fn[d]=function(d,e){var f=arguments.length&&(c||"boolean"!=typeof d),g=c||(d===!0||e===!0?"margin":"border");return K(this,function(b,c,d){var e;return n.isWindow(b)?b.document.documentElement["client"+a]:9===b.nodeType?(e=b.documentElement,Math.max(b.body["scroll"+a],e["scroll"+a],b.body["offset"+a],e["offset"+a],e["client"+a])):void 0===d?n.css(b,c,g):n.style(b,c,d,g)},b,f?d:void 0,f,null)}})}),n.fn.extend({bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return 1===arguments.length?this.off(a,"**"):this.off(b,a||"**",c)},size:function(){return this.length}}),n.fn.andSelf=n.fn.addBack,"function"==typeof define&&define.amd&&define("jquery",[],function(){return n});var Nb=a.jQuery,Ob=a.$;return n.noConflict=function(b){return a.$===n&&(a.$=Ob),b&&a.jQuery===n&&(a.jQuery=Nb),n},b||(a.jQuery=a.$=n),n});
/**
 * Swiper 4.0.7
 * Most modern mobile touch slider and framework with hardware accelerated transitions
 * http://www.idangero.us/swiper/
 *
 * Copyright 2014-2017 Vladimir Kharlampidi
 *
 * Released under the MIT License
 *
 * Released on: November 28, 2017
 */
!function(e,t){"object"==typeof exports&&"undefined"!=typeof module?module.exports=t():"function"==typeof define&&define.amd?define(t):e.Swiper=t()}(this,function(){"use strict";function e(e,t){var a=[],i=0;if(e&&!t&&e instanceof r)return e;if(e)if("string"==typeof e){var s,n,o=e.trim();if(o.indexOf("<")>=0&&o.indexOf(">")>=0){var l="div";for(0===o.indexOf("<li")&&(l="ul"),0===o.indexOf("<tr")&&(l="tbody"),0!==o.indexOf("<td")&&0!==o.indexOf("<th")||(l="tr"),0===o.indexOf("<tbody")&&(l="table"),0===o.indexOf("<option")&&(l="select"),(n=document.createElement(l)).innerHTML=o,i=0;i<n.childNodes.length;i+=1)a.push(n.childNodes[i])}else for(s=t||"#"!==e[0]||e.match(/[ .<>:~]/)?(t||document).querySelectorAll(e.trim()):[document.getElementById(e.trim().split("#")[1])],i=0;i<s.length;i+=1)s[i]&&a.push(s[i])}else if(e.nodeType||e===window||e===document)a.push(e);else if(e.length>0&&e[0].nodeType)for(i=0;i<e.length;i+=1)a.push(e[i]);return new r(a)}function t(e){for(var t=[],a=0;a<e.length;a+=1)-1===t.indexOf(e[a])&&t.push(e[a]);return t}function a(){var e="onwheel"in d;if(!e){var t=d.createElement("div");t.setAttribute("onwheel","return;"),e="function"==typeof t.onwheel}return!e&&d.implementation&&d.implementation.hasFeature&&!0!==d.implementation.hasFeature("","")&&(e=d.implementation.hasFeature("Events.wheel","3.0")),e}var i,s=i="undefined"==typeof window?{navigator:{userAgent:""},location:{},history:{},addEventListener:function(){},removeEventListener:function(){},getComputedStyle:function(){return{}},Image:function(){},Date:function(){},screen:{}}:window,r=function(e){for(var t=this,a=0;a<e.length;a+=1)t[a]=e[a];return t.length=e.length,this};e.fn=r.prototype,e.Class=r,e.Dom7=r;"resize scroll".split(" ");var n={addClass:function(e){var t=this;if(void 0===e)return this;for(var a=e.split(" "),i=0;i<a.length;i+=1)for(var s=0;s<this.length;s+=1)void 0!==t[s].classList&&t[s].classList.add(a[i]);return this},removeClass:function(e){for(var t=this,a=e.split(" "),i=0;i<a.length;i+=1)for(var s=0;s<this.length;s+=1)void 0!==t[s].classList&&t[s].classList.remove(a[i]);return this},hasClass:function(e){return!!this[0]&&this[0].classList.contains(e)},toggleClass:function(e){for(var t=this,a=e.split(" "),i=0;i<a.length;i+=1)for(var s=0;s<this.length;s+=1)void 0!==t[s].classList&&t[s].classList.toggle(a[i]);return this},attr:function(e,t){var a=arguments,i=this;if(1!==arguments.length||"string"!=typeof e){for(var s=0;s<this.length;s+=1)if(2===a.length)i[s].setAttribute(e,t);else for(var r in e)i[s][r]=e[r],i[s].setAttribute(r,e[r]);return this}if(this[0])return this[0].getAttribute(e)},removeAttr:function(e){for(var t=this,a=0;a<this.length;a+=1)t[a].removeAttribute(e);return this},data:function(e,t){var a,i=this;if(void 0!==t){for(var s=0;s<this.length;s+=1)(a=i[s]).dom7ElementDataStorage||(a.dom7ElementDataStorage={}),a.dom7ElementDataStorage[e]=t;return this}if(a=this[0]){if(a.dom7ElementDataStorage&&e in a.dom7ElementDataStorage)return a.dom7ElementDataStorage[e];var r=a.getAttribute("data-"+e);if(r)return r}},transform:function(e){for(var t=this,a=0;a<this.length;a+=1){var i=t[a].style;i.webkitTransform=e,i.transform=e}return this},transition:function(e){var t=this;"string"!=typeof e&&(e+="ms");for(var a=0;a<this.length;a+=1){var i=t[a].style;i.webkitTransitionDuration=e,i.transitionDuration=e}return this},on:function(){function t(t){var a=t.target;if(a){var i=t.target.dom7EventData||[];if(i.unshift(t),e(a).is(o))l.apply(a,i);else for(var s=e(a).parents(),r=0;r<s.length;r+=1)e(s[r]).is(o)&&l.apply(s[r],i)}}function a(e){var t=e&&e.target?e.target.dom7EventData||[]:[];t.unshift(e),l.apply(this,t)}for(var i=this,s=[],r=arguments.length;r--;)s[r]=arguments[r];var n=s[0],o=s[1],l=s[2],d=s[3];if("function"==typeof s[1]){var p;n=(p=s)[0],l=p[1],d=p[2],o=void 0}d||(d=!1);for(var c,u=n.split(" "),h=0;h<this.length;h+=1){var v=i[h];if(o)for(c=0;c<u.length;c+=1)v.dom7LiveListeners||(v.dom7LiveListeners=[]),v.dom7LiveListeners.push({type:n,listener:l,proxyListener:t}),v.addEventListener(u[c],t,d);else for(c=0;c<u.length;c+=1)v.dom7Listeners||(v.dom7Listeners=[]),v.dom7Listeners.push({type:n,listener:l,proxyListener:a}),v.addEventListener(u[c],a,d)}return this},off:function(){for(var e=this,t=[],a=arguments.length;a--;)t[a]=arguments[a];var i=t[0],s=t[1],r=t[2],n=t[3];if("function"==typeof t[1]){var o;i=(o=t)[0],r=o[1],n=o[2],s=void 0}n||(n=!1);for(var l=i.split(" "),d=0;d<l.length;d+=1)for(var p=0;p<this.length;p+=1){var c=e[p];if(s){if(c.dom7LiveListeners)for(var u=0;u<c.dom7LiveListeners.length;u+=1)r?c.dom7LiveListeners[u].listener===r&&c.removeEventListener(l[d],c.dom7LiveListeners[u].proxyListener,n):c.dom7LiveListeners[u].type===l[d]&&c.removeEventListener(l[d],c.dom7LiveListeners[u].proxyListener,n)}else if(c.dom7Listeners)for(var h=0;h<c.dom7Listeners.length;h+=1)r?c.dom7Listeners[h].listener===r&&c.removeEventListener(l[d],c.dom7Listeners[h].proxyListener,n):c.dom7Listeners[h].type===l[d]&&c.removeEventListener(l[d],c.dom7Listeners[h].proxyListener,n)}return this},trigger:function(){for(var e=this,t=[],a=arguments.length;a--;)t[a]=arguments[a];for(var i=t[0].split(" "),s=t[1],r=0;r<i.length;r+=1)for(var n=0;n<this.length;n+=1){var o=void 0;try{o=new window.CustomEvent(i[r],{detail:s,bubbles:!0,cancelable:!0})}catch(e){(o=document.createEvent("Event")).initEvent(i[r],!0,!0),o.detail=s}e[n].dom7EventData=t.filter(function(e,t){return t>0}),e[n].dispatchEvent(o),e[n].dom7EventData=[],delete e[n].dom7EventData}return this},transitionEnd:function(e){function t(r){if(r.target===this)for(e.call(this,r),a=0;a<i.length;a+=1)s.off(i[a],t)}var a,i=["webkitTransitionEnd","transitionend"],s=this;if(e)for(a=0;a<i.length;a+=1)s.on(i[a],t);return this},outerWidth:function(e){if(this.length>0){if(e){var t=this.styles();return this[0].offsetWidth+parseFloat(t.getPropertyValue("margin-right"))+parseFloat(t.getPropertyValue("margin-left"))}return this[0].offsetWidth}return null},outerHeight:function(e){if(this.length>0){if(e){var t=this.styles();return this[0].offsetHeight+parseFloat(t.getPropertyValue("margin-top"))+parseFloat(t.getPropertyValue("margin-bottom"))}return this[0].offsetHeight}return null},offset:function(){if(this.length>0){var e=this[0],t=e.getBoundingClientRect(),a=document.body,i=e.clientTop||a.clientTop||0,s=e.clientLeft||a.clientLeft||0,r=e===window?window.scrollY:e.scrollTop,n=e===window?window.scrollX:e.scrollLeft;return{top:t.top+r-i,left:t.left+n-s}}return null},css:function(e,t){var a,i=this;if(1===arguments.length){if("string"!=typeof e){for(a=0;a<this.length;a+=1)for(var s in e)i[a].style[s]=e[s];return this}if(this[0])return window.getComputedStyle(this[0],null).getPropertyValue(e)}if(2===arguments.length&&"string"==typeof e){for(a=0;a<this.length;a+=1)i[a].style[e]=t;return this}return this},each:function(e){var t=this;if(!e)return this;for(var a=0;a<this.length;a+=1)if(!1===e.call(t[a],a,t[a]))return t;return this},html:function(e){var t=this;if(void 0===e)return this[0]?this[0].innerHTML:void 0;for(var a=0;a<this.length;a+=1)t[a].innerHTML=e;return this},text:function(e){var t=this;if(void 0===e)return this[0]?this[0].textContent.trim():null;for(var a=0;a<this.length;a+=1)t[a].textContent=e;return this},is:function(t){var a,i,s=this[0];if(!s||void 0===t)return!1;if("string"==typeof t){if(s.matches)return s.matches(t);if(s.webkitMatchesSelector)return s.webkitMatchesSelector(t);if(s.msMatchesSelector)return s.msMatchesSelector(t);for(a=e(t),i=0;i<a.length;i+=1)if(a[i]===s)return!0;return!1}if(t===document)return s===document;if(t===window)return s===window;if(t.nodeType||t instanceof r){for(a=t.nodeType?[t]:t,i=0;i<a.length;i+=1)if(a[i]===s)return!0;return!1}return!1},index:function(){var e,t=this[0];if(t){for(e=0;null!==(t=t.previousSibling);)1===t.nodeType&&(e+=1);return e}},eq:function(e){if(void 0===e)return this;var t,a=this.length;return e>a-1?new r([]):e<0?(t=a+e,new r(t<0?[]:[this[t]])):new r([this[e]])},append:function(){for(var e=this,t=[],a=arguments.length;a--;)t[a]=arguments[a];for(var i,s=0;s<t.length;s+=1){i=t[s];for(var n=0;n<this.length;n+=1)if("string"==typeof i){var o=document.createElement("div");for(o.innerHTML=i;o.firstChild;)e[n].appendChild(o.firstChild)}else if(i instanceof r)for(var l=0;l<i.length;l+=1)e[n].appendChild(i[l]);else e[n].appendChild(i)}return this},prepend:function(e){var t,a,i=this;for(t=0;t<this.length;t+=1)if("string"==typeof e){var s=document.createElement("div");for(s.innerHTML=e,a=s.childNodes.length-1;a>=0;a-=1)i[t].insertBefore(s.childNodes[a],i[t].childNodes[0])}else if(e instanceof r)for(a=0;a<e.length;a+=1)i[t].insertBefore(e[a],i[t].childNodes[0]);else i[t].insertBefore(e,i[t].childNodes[0]);return this},next:function(t){return new r(this.length>0?t?this[0].nextElementSibling&&e(this[0].nextElementSibling).is(t)?[this[0].nextElementSibling]:[]:this[0].nextElementSibling?[this[0].nextElementSibling]:[]:[])},nextAll:function(t){var a=[],i=this[0];if(!i)return new r([]);for(;i.nextElementSibling;){var s=i.nextElementSibling;t?e(s).is(t)&&a.push(s):a.push(s),i=s}return new r(a)},prev:function(t){if(this.length>0){var a=this[0];return new r(t?a.previousElementSibling&&e(a.previousElementSibling).is(t)?[a.previousElementSibling]:[]:a.previousElementSibling?[a.previousElementSibling]:[])}return new r([])},prevAll:function(t){var a=[],i=this[0];if(!i)return new r([]);for(;i.previousElementSibling;){var s=i.previousElementSibling;t?e(s).is(t)&&a.push(s):a.push(s),i=s}return new r(a)},parent:function(a){for(var i=this,s=[],r=0;r<this.length;r+=1)null!==i[r].parentNode&&(a?e(i[r].parentNode).is(a)&&s.push(i[r].parentNode):s.push(i[r].parentNode));return e(t(s))},parents:function(a){for(var i=this,s=[],r=0;r<this.length;r+=1)for(var n=i[r].parentNode;n;)a?e(n).is(a)&&s.push(n):s.push(n),n=n.parentNode;return e(t(s))},closest:function(e){var t=this;return void 0===e?new r([]):(t.is(e)||(t=t.parents(e).eq(0)),t)},find:function(e){for(var t=this,a=[],i=0;i<this.length;i+=1)for(var s=t[i].querySelectorAll(e),n=0;n<s.length;n+=1)a.push(s[n]);return new r(a)},children:function(a){for(var i=this,s=[],n=0;n<this.length;n+=1)for(var o=i[n].childNodes,l=0;l<o.length;l+=1)a?1===o[l].nodeType&&e(o[l]).is(a)&&s.push(o[l]):1===o[l].nodeType&&s.push(o[l]);return new r(t(s))},remove:function(){for(var e=this,t=0;t<this.length;t+=1)e[t].parentNode&&e[t].parentNode.removeChild(e[t]);return this},add:function(){for(var t=[],a=arguments.length;a--;)t[a]=arguments[a];var i,s,r=this;for(i=0;i<t.length;i+=1){var n=e(t[i]);for(s=0;s<n.length;s+=1)r[r.length]=n[s],r.length+=1}return r},styles:function(){return this[0]?window.getComputedStyle(this[0],null):{}}};Object.keys(n).forEach(function(t){e.fn[t]=n[t]});var o,l={deleteProps:function(e){var t=e;Object.keys(t).forEach(function(e){try{t[e]=null}catch(e){}try{delete t[e]}catch(e){}})},nextTick:function(e,t){return void 0===t&&(t=0),setTimeout(e,t)},now:function(){return Date.now()},getTranslate:function(e,t){void 0===t&&(t="x");var a,i,r,n=s.getComputedStyle(e,null);return s.WebKitCSSMatrix?((i=n.transform||n.webkitTransform).split(",").length>6&&(i=i.split(", ").map(function(e){return e.replace(",",".")}).join(", ")),r=new s.WebKitCSSMatrix("none"===i?"":i)):a=(r=n.MozTransform||n.OTransform||n.MsTransform||n.msTransform||n.transform||n.getPropertyValue("transform").replace("translate(","matrix(1, 0, 0, 1,")).toString().split(","),"x"===t&&(i=s.WebKitCSSMatrix?r.m41:16===a.length?parseFloat(a[12]):parseFloat(a[4])),"y"===t&&(i=s.WebKitCSSMatrix?r.m42:16===a.length?parseFloat(a[13]):parseFloat(a[5])),i||0},parseUrlQuery:function(e){var t,a,i,r,n={},o=e||s.location.href;if("string"==typeof o&&o.length)for(r=(a=(o=o.indexOf("?")>-1?o.replace(/\S*\?/,""):"").split("&").filter(function(e){return""!==e})).length,t=0;t<r;t+=1)i=a[t].replace(/#\S+/g,"").split("="),n[decodeURIComponent(i[0])]=void 0===i[1]?void 0:decodeURIComponent(i[1])||"";return n},isObject:function(e){return"object"==typeof e&&null!==e&&e.constructor&&e.constructor===Object},extend:function(){for(var e=[],t=arguments.length;t--;)e[t]=arguments[t];for(var a=Object(e[0]),i=1;i<e.length;i+=1){var s=e[i];if(void 0!==s&&null!==s)for(var r=Object.keys(Object(s)),n=0,o=r.length;n<o;n+=1){var d=r[n],p=Object.getOwnPropertyDescriptor(s,d);void 0!==p&&p.enumerable&&(l.isObject(a[d])&&l.isObject(s[d])?l.extend(a[d],s[d]):!l.isObject(a[d])&&l.isObject(s[d])?(a[d]={},l.extend(a[d],s[d])):a[d]=s[d])}}return a}},d=o="undefined"==typeof document?{addEventListener:function(){},removeEventListener:function(){},activeElement:{blur:function(){},nodeName:""},querySelector:function(){return{}},querySelectorAll:function(){return[]},createElement:function(){return{style:{},setAttribute:function(){},getElementsByTagName:function(){return[]}}},location:{hash:""}}:document,p={touch:s.Modernizr&&!0===s.Modernizr.touch||!!("ontouchstart"in s||s.DocumentTouch&&d instanceof s.DocumentTouch),transforms3d:s.Modernizr&&!0===s.Modernizr.csstransforms3d||function(){var e=d.createElement("div").style;return"webkitPerspective"in e||"MozPerspective"in e||"OPerspective"in e||"MsPerspective"in e||"perspective"in e}(),flexbox:function(){for(var e=d.createElement("div").style,t="alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "),a=0;a<t.length;a+=1)if(t[a]in e)return!0;return!1}(),observer:"MutationObserver"in s||"WebkitMutationObserver"in s,passiveListener:function(){var e=!1;try{var t=Object.defineProperty({},"passive",{get:function(){e=!0}});s.addEventListener("testPassiveListener",null,t)}catch(e){}return e}(),gestures:"ongesturestart"in s},c=function(e){void 0===e&&(e={});var t=this;t.params=e,t.eventsListeners={},t.params&&t.params.on&&Object.keys(t.params.on).forEach(function(e){t.on(e,t.params.on[e])})},u={components:{}};c.prototype.on=function(e,t){var a=this;return"function"!=typeof t?a:(e.split(" ").forEach(function(e){a.eventsListeners[e]||(a.eventsListeners[e]=[]),a.eventsListeners[e].push(t)}),a)},c.prototype.once=function(e,t){function a(){for(var s=[],r=arguments.length;r--;)s[r]=arguments[r];t.apply(i,s),i.off(e,a)}var i=this;return"function"!=typeof t?i:i.on(e,a)},c.prototype.off=function(e,t){var a=this;return e.split(" ").forEach(function(e){void 0===t?a.eventsListeners[e]=[]:a.eventsListeners[e].forEach(function(i,s){i===t&&a.eventsListeners[e].splice(s,1)})}),a},c.prototype.emit=function(){for(var e=[],t=arguments.length;t--;)e[t]=arguments[t];var a=this;if(!a.eventsListeners)return a;var i,s,r;return"string"==typeof e[0]||Array.isArray(e[0])?(i=e[0],s=e.slice(1,e.length),r=a):(i=e[0].events,s=e[0].data,r=e[0].context||a),(Array.isArray(i)?i:i.split(" ")).forEach(function(e){if(a.eventsListeners[e]){var t=[];a.eventsListeners[e].forEach(function(e){t.push(e)}),t.forEach(function(e){e.apply(r,s)})}}),a},c.prototype.useModulesParams=function(e){var t=this;t.modules&&Object.keys(t.modules).forEach(function(a){var i=t.modules[a];i.params&&l.extend(e,i.params)})},c.prototype.useModules=function(e){void 0===e&&(e={});var t=this;t.modules&&Object.keys(t.modules).forEach(function(a){var i=t.modules[a],s=e[a]||{};i.instance&&Object.keys(i.instance).forEach(function(e){var a=i.instance[e];t[e]="function"==typeof a?a.bind(t):a}),i.on&&t.on&&Object.keys(i.on).forEach(function(e){t.on(e,i.on[e])}),i.create&&i.create.bind(t)(s)})},u.components.set=function(e){var t=this;t.use&&t.use(e)},c.installModule=function(e){for(var t=[],a=arguments.length-1;a-- >0;)t[a]=arguments[a+1];var i=this;i.prototype.modules||(i.prototype.modules={});var s=e.name||Object.keys(i.prototype.modules).length+"_"+l.now();return i.prototype.modules[s]=e,e.proto&&Object.keys(e.proto).forEach(function(t){i.prototype[t]=e.proto[t]}),e.static&&Object.keys(e.static).forEach(function(t){i[t]=e.static[t]}),e.install&&e.install.apply(i,t),i},c.use=function(e){for(var t=[],a=arguments.length-1;a-- >0;)t[a]=arguments[a+1];var i=this;return Array.isArray(e)?(e.forEach(function(e){return i.installModule(e)}),i):i.installModule.apply(i,[e].concat(t))},Object.defineProperties(c,u);var h={updateSize:function(){var e,t,a=this,i=a.$el;e=void 0!==a.params.width?a.params.width:i[0].clientWidth,t=void 0!==a.params.height?a.params.height:i[0].clientHeight,0===e&&a.isHorizontal()||0===t&&a.isVertical()||(e=e-parseInt(i.css("padding-left"),10)-parseInt(i.css("padding-right"),10),t=t-parseInt(i.css("padding-top"),10)-parseInt(i.css("padding-bottom"),10),l.extend(a,{width:e,height:t,size:a.isHorizontal()?e:t}))},updateSlides:function(){var e=this,t=e.params,a=e.$wrapperEl,i=e.size,s=e.rtl,r=e.wrongRTL,n=a.children("."+e.params.slideClass),o=e.virtual&&t.virtual.enabled?e.virtual.slides.length:n.length,d=[],c=[],u=[],h=t.slidesOffsetBefore;"function"==typeof h&&(h=t.slidesOffsetBefore.call(e));var v=t.slidesOffsetAfter;"function"==typeof v&&(v=t.slidesOffsetAfter.call(e));var f=o,m=e.snapGrid.length,g=e.snapGrid.length,b=t.spaceBetween,w=-h,y=0,x=0;if(void 0!==i){"string"==typeof b&&b.indexOf("%")>=0&&(b=parseFloat(b.replace("%",""))/100*i),e.virtualSize=-b,s?n.css({marginLeft:"",marginTop:""}):n.css({marginRight:"",marginBottom:""});var T;t.slidesPerColumn>1&&(T=Math.floor(o/t.slidesPerColumn)===o/e.params.slidesPerColumn?o:Math.ceil(o/t.slidesPerColumn)*t.slidesPerColumn,"auto"!==t.slidesPerView&&"row"===t.slidesPerColumnFill&&(T=Math.max(T,t.slidesPerView*t.slidesPerColumn)));for(var E,S=t.slidesPerColumn,C=T/S,M=C-(t.slidesPerColumn*C-o),z=0;z<o;z+=1){E=0;var P=n.eq(z);if(t.slidesPerColumn>1){var k=void 0,$=void 0,I=void 0;"column"===t.slidesPerColumnFill?(I=z-($=Math.floor(z/S))*S,($>M||$===M&&I===S-1)&&(I+=1)>=S&&(I=0,$+=1),k=$+I*T/S,P.css({"-webkit-box-ordinal-group":k,"-moz-box-ordinal-group":k,"-ms-flex-order":k,"-webkit-order":k,order:k})):$=z-(I=Math.floor(z/C))*C,P.css("margin-"+(e.isHorizontal()?"top":"left"),0!==I&&t.spaceBetween&&t.spaceBetween+"px").attr("data-swiper-column",$).attr("data-swiper-row",I)}"none"!==P.css("display")&&("auto"===t.slidesPerView?(E=e.isHorizontal()?P.outerWidth(!0):P.outerHeight(!0),t.roundLengths&&(E=Math.floor(E))):(E=(i-(t.slidesPerView-1)*b)/t.slidesPerView,t.roundLengths&&(E=Math.floor(E)),n[z]&&(e.isHorizontal()?n[z].style.width=E+"px":n[z].style.height=E+"px")),n[z]&&(n[z].swiperSlideSize=E),u.push(E),t.centeredSlides?(w=w+E/2+y/2+b,0===y&&0!==z&&(w=w-i/2-b),0===z&&(w=w-i/2-b),Math.abs(w)<.001&&(w=0),x%t.slidesPerGroup==0&&d.push(w),c.push(w)):(x%t.slidesPerGroup==0&&d.push(w),c.push(w),w=w+E+b),e.virtualSize+=E+b,y=E,x+=1)}e.virtualSize=Math.max(e.virtualSize,i)+v;var L;if(s&&r&&("slide"===t.effect||"coverflow"===t.effect)&&a.css({width:e.virtualSize+t.spaceBetween+"px"}),p.flexbox&&!t.setWrapperSize||(e.isHorizontal()?a.css({width:e.virtualSize+t.spaceBetween+"px"}):a.css({height:e.virtualSize+t.spaceBetween+"px"})),t.slidesPerColumn>1&&(e.virtualSize=(E+t.spaceBetween)*T,e.virtualSize=Math.ceil(e.virtualSize/t.slidesPerColumn)-t.spaceBetween,e.isHorizontal()?a.css({width:e.virtualSize+t.spaceBetween+"px"}):a.css({height:e.virtualSize+t.spaceBetween+"px"}),t.centeredSlides)){L=[];for(var D=0;D<d.length;D+=1)d[D]<e.virtualSize+d[0]&&L.push(d[D]);d=L}if(!t.centeredSlides){L=[];for(var O=0;O<d.length;O+=1)d[O]<=e.virtualSize-i&&L.push(d[O]);d=L,Math.floor(e.virtualSize-i)-Math.floor(d[d.length-1])>1&&d.push(e.virtualSize-i)}0===d.length&&(d=[0]),0!==t.spaceBetween&&(e.isHorizontal()?s?n.css({marginLeft:b+"px"}):n.css({marginRight:b+"px"}):n.css({marginBottom:b+"px"})),l.extend(e,{slides:n,snapGrid:d,slidesGrid:c,slidesSizesGrid:u}),o!==f&&e.emit("slidesLengthChange"),d.length!==m&&e.emit("snapGridLengthChange"),c.length!==g&&e.emit("slidesGridLengthChange"),(t.watchSlidesProgress||t.watchSlidesVisibility)&&e.updateSlidesOffset()}},updateAutoHeight:function(){var e,t=this,a=[],i=0;if("auto"!==t.params.slidesPerView&&t.params.slidesPerView>1)for(e=0;e<Math.ceil(t.params.slidesPerView);e+=1){var s=t.activeIndex+e;if(s>t.slides.length)break;a.push(t.slides.eq(s)[0])}else a.push(t.slides.eq(t.activeIndex)[0]);for(e=0;e<a.length;e+=1)if(void 0!==a[e]){var r=a[e].offsetHeight;i=r>i?r:i}i&&t.$wrapperEl.css("height",i+"px")},updateSlidesOffset:function(){for(var e=this,t=e.slides,a=0;a<t.length;a+=1)t[a].swiperSlideOffset=e.isHorizontal()?t[a].offsetLeft:t[a].offsetTop},updateSlidesProgress:function(e){void 0===e&&(e=this.translate||0);var t=this,a=t.params,i=t.slides,s=t.rtl;if(0!==i.length){void 0===i[0].swiperSlideOffset&&t.updateSlidesOffset();var r=-e;s&&(r=e),i.removeClass(a.slideVisibleClass);for(var n=0;n<i.length;n+=1){var o=i[n],l=(r+(a.centeredSlides?t.minTranslate():0)-o.swiperSlideOffset)/(o.swiperSlideSize+a.spaceBetween);if(a.watchSlidesVisibility){var d=-(r-o.swiperSlideOffset),p=d+t.slidesSizesGrid[n];(d>=0&&d<t.size||p>0&&p<=t.size||d<=0&&p>=t.size)&&i.eq(n).addClass(a.slideVisibleClass)}o.progress=s?-l:l}}},updateProgress:function(e){void 0===e&&(e=this.translate||0);var t=this,a=t.params,i=t.maxTranslate()-t.minTranslate(),s=t.progress,r=t.isBeginning,n=t.isEnd,o=r,d=n;0===i?(s=0,r=!0,n=!0):(r=(s=(e-t.minTranslate())/i)<=0,n=s>=1),l.extend(t,{progress:s,isBeginning:r,isEnd:n}),(a.watchSlidesProgress||a.watchSlidesVisibility)&&t.updateSlidesProgress(e),r&&!o&&t.emit("reachBeginning toEdge"),n&&!d&&t.emit("reachEnd toEdge"),(o&&!r||d&&!n)&&t.emit("fromEdge"),t.emit("progress",s)},updateSlidesClasses:function(){var e=this,t=e.slides,a=e.params,i=e.$wrapperEl,s=e.activeIndex,r=e.realIndex,n=e.virtual&&a.virtual.enabled;t.removeClass(a.slideActiveClass+" "+a.slideNextClass+" "+a.slidePrevClass+" "+a.slideDuplicateActiveClass+" "+a.slideDuplicateNextClass+" "+a.slideDuplicatePrevClass);var o;(o=n?e.$wrapperEl.find("."+a.slideClass+'[data-swiper-slide-index="'+s+'"]'):t.eq(s)).addClass(a.slideActiveClass),a.loop&&(o.hasClass(a.slideDuplicateClass)?i.children("."+a.slideClass+":not(."+a.slideDuplicateClass+')[data-swiper-slide-index="'+r+'"]').addClass(a.slideDuplicateActiveClass):i.children("."+a.slideClass+"."+a.slideDuplicateClass+'[data-swiper-slide-index="'+r+'"]').addClass(a.slideDuplicateActiveClass));var l=o.nextAll("."+a.slideClass).eq(0).addClass(a.slideNextClass);a.loop&&0===l.length&&(l=t.eq(0)).addClass(a.slideNextClass);var d=o.prevAll("."+a.slideClass).eq(0).addClass(a.slidePrevClass);a.loop&&0===d.length&&(d=t.eq(-1)).addClass(a.slidePrevClass),a.loop&&(l.hasClass(a.slideDuplicateClass)?i.children("."+a.slideClass+":not(."+a.slideDuplicateClass+')[data-swiper-slide-index="'+l.attr("data-swiper-slide-index")+'"]').addClass(a.slideDuplicateNextClass):i.children("."+a.slideClass+"."+a.slideDuplicateClass+'[data-swiper-slide-index="'+l.attr("data-swiper-slide-index")+'"]').addClass(a.slideDuplicateNextClass),d.hasClass(a.slideDuplicateClass)?i.children("."+a.slideClass+":not(."+a.slideDuplicateClass+')[data-swiper-slide-index="'+d.attr("data-swiper-slide-index")+'"]').addClass(a.slideDuplicatePrevClass):i.children("."+a.slideClass+"."+a.slideDuplicateClass+'[data-swiper-slide-index="'+d.attr("data-swiper-slide-index")+'"]').addClass(a.slideDuplicatePrevClass))},updateActiveIndex:function(e){var t,a=this,i=a.rtl?a.translate:-a.translate,s=a.slidesGrid,r=a.snapGrid,n=a.params,o=a.activeIndex,d=a.realIndex,p=a.snapIndex,c=e;if(void 0===c){for(var u=0;u<s.length;u+=1)void 0!==s[u+1]?i>=s[u]&&i<s[u+1]-(s[u+1]-s[u])/2?c=u:i>=s[u]&&i<s[u+1]&&(c=u+1):i>=s[u]&&(c=u);n.normalizeSlideIndex&&(c<0||void 0===c)&&(c=0)}if((t=r.indexOf(i)>=0?r.indexOf(i):Math.floor(c/n.slidesPerGroup))>=r.length&&(t=r.length-1),c!==o){var h=parseInt(a.slides.eq(c).attr("data-swiper-slide-index")||c,10);l.extend(a,{snapIndex:t,realIndex:h,previousIndex:o,activeIndex:c}),a.emit("activeIndexChange"),a.emit("snapIndexChange"),d!==h&&a.emit("realIndexChange"),a.emit("slideChange")}else t!==p&&(a.snapIndex=t,a.emit("snapIndexChange"))},updateClickedSlide:function(t){var a=this,i=a.params,s=e(t.target).closest("."+i.slideClass)[0],r=!1;if(s)for(var n=0;n<a.slides.length;n+=1)a.slides[n]===s&&(r=!0);if(!s||!r)return a.clickedSlide=void 0,void(a.clickedIndex=void 0);a.clickedSlide=s,a.virtual&&a.params.virtual.enabled?a.clickedIndex=parseInt(e(s).attr("data-swiper-slide-index"),10):a.clickedIndex=e(s).index(),i.slideToClickedSlide&&void 0!==a.clickedIndex&&a.clickedIndex!==a.activeIndex&&a.slideToClickedSlide()}},v={getTranslate:function(e){void 0===e&&(e=this.isHorizontal()?"x":"y");var t=this,a=t.params,i=t.rtl,s=t.translate,r=t.$wrapperEl;if(a.virtualTranslate)return i?-s:s;var n=l.getTranslate(r[0],e);return i&&(n=-n),n||0},setTranslate:function(e,t){var a=this,i=a.rtl,s=a.params,r=a.$wrapperEl,n=a.progress,o=0,l=0;a.isHorizontal()?o=i?-e:e:l=e,s.roundLengths&&(o=Math.floor(o),l=Math.floor(l)),s.virtualTranslate||(p.transforms3d?r.transform("translate3d("+o+"px, "+l+"px, 0px)"):r.transform("translate("+o+"px, "+l+"px)")),a.translate=a.isHorizontal()?o:l;var d=a.maxTranslate()-a.minTranslate();(0===d?0:(e-a.minTranslate())/d)!==n&&a.updateProgress(e),a.emit("setTranslate",a.translate,t)},minTranslate:function(){return-this.snapGrid[0]},maxTranslate:function(){return-this.snapGrid[this.snapGrid.length-1]}},f={setTransition:function(e,t){var a=this;a.$wrapperEl.transition(e),a.emit("setTransition",e,t)},transitionStart:function(e){void 0===e&&(e=!0);var t=this,a=t.activeIndex,i=t.params,s=t.previousIndex;i.autoHeight&&t.updateAutoHeight(),t.emit("transitionStart"),e&&a!==s&&(t.emit("slideChangeTransitionStart"),a>s?t.emit("slideNextTransitionStart"):t.emit("slidePrevTransitionStart"))},transitionEnd:function(e){void 0===e&&(e=!0);var t=this,a=t.activeIndex,i=t.previousIndex;t.animating=!1,t.setTransition(0),t.emit("transitionEnd"),e&&a!==i&&(t.emit("slideChangeTransitionEnd"),a>i?t.emit("slideNextTransitionEnd"):t.emit("slidePrevTransitionEnd"))}},m=function(){return{isSafari:function(){var e=s.navigator.userAgent.toLowerCase();return e.indexOf("safari")>=0&&e.indexOf("chrome")<0&&e.indexOf("android")<0}(),isUiWebView:/(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(s.navigator.userAgent),ie:s.navigator.pointerEnabled||s.navigator.msPointerEnabled,ieTouch:s.navigator.msPointerEnabled&&s.navigator.msMaxTouchPoints>1||s.navigator.pointerEnabled&&s.navigator.maxTouchPoints>1,lteIE9:function(){var e=d.createElement("div");return e.innerHTML="\x3c!--[if lte IE 9]><i></i><![endif]--\x3e",1===e.getElementsByTagName("i").length}()}}(),g={slideTo:function(e,t,a,i){void 0===e&&(e=0),void 0===t&&(t=this.params.speed),void 0===a&&(a=!0);var s=this,r=e;r<0&&(r=0);var n=s.params,o=s.snapGrid,l=s.slidesGrid,d=s.previousIndex,p=s.activeIndex,c=s.rtl,u=s.$wrapperEl,h=Math.floor(r/n.slidesPerGroup);h>=o.length&&(h=o.length-1),(p||n.initialSlide||0)===(d||0)&&a&&s.emit("beforeSlideChangeStart");var v=-o[h];if(s.updateProgress(v),n.normalizeSlideIndex)for(var f=0;f<l.length;f+=1)-Math.floor(100*v)>=Math.floor(100*l[f])&&(r=f);return!(!s.allowSlideNext&&v<s.translate&&v<s.minTranslate()||!s.allowSlidePrev&&v>s.translate&&v>s.maxTranslate()&&(p||0)!==r||(c&&-v===s.translate||!c&&v===s.translate?(s.updateActiveIndex(r),n.autoHeight&&s.updateAutoHeight(),s.updateSlidesClasses(),"slide"!==n.effect&&s.setTranslate(v),1):(0===t||m.lteIE9?(s.setTransition(0),s.setTranslate(v),s.updateActiveIndex(r),s.updateSlidesClasses(),s.emit("beforeTransitionStart",t,i),s.transitionStart(a),s.transitionEnd(a)):(s.setTransition(t),s.setTranslate(v),s.updateActiveIndex(r),s.updateSlidesClasses(),s.emit("beforeTransitionStart",t,i),s.transitionStart(a),s.animating||(s.animating=!0,u.transitionEnd(function(){s&&!s.destroyed&&s.transitionEnd(a)}))),0)))},slideNext:function(e,t,a){void 0===e&&(e=this.params.speed),void 0===t&&(t=!0);var i=this,s=i.params,r=i.animating;return s.loop?!r&&(i.loopFix(),i._clientLeft=i.$wrapperEl[0].clientLeft,i.slideTo(i.activeIndex+s.slidesPerGroup,e,t,a)):i.slideTo(i.activeIndex+s.slidesPerGroup,e,t,a)},slidePrev:function(e,t,a){void 0===e&&(e=this.params.speed),void 0===t&&(t=!0);var i=this,s=i.params,r=i.animating;return s.loop?!r&&(i.loopFix(),i._clientLeft=i.$wrapperEl[0].clientLeft,i.slideTo(i.activeIndex-1,e,t,a)):i.slideTo(i.activeIndex-1,e,t,a)},slideReset:function(e,t,a){void 0===e&&(e=this.params.speed),void 0===t&&(t=!0);var i=this;return i.slideTo(i.activeIndex,e,t,a)},slideToClickedSlide:function(){var t,a=this,i=a.params,s=a.$wrapperEl,r="auto"===i.slidesPerView?a.slidesPerViewDynamic():i.slidesPerView,n=a.clickedIndex;if(i.loop){if(a.animating)return;t=parseInt(e(a.clickedSlide).attr("data-swiper-slide-index"),10),i.centeredSlides?n<a.loopedSlides-r/2||n>a.slides.length-a.loopedSlides+r/2?(a.loopFix(),n=s.children("."+i.slideClass+'[data-swiper-slide-index="'+t+'"]:not(.'+i.slideDuplicateClass+")").eq(0).index(),l.nextTick(function(){a.slideTo(n)})):a.slideTo(n):n>a.slides.length-r?(a.loopFix(),n=s.children("."+i.slideClass+'[data-swiper-slide-index="'+t+'"]:not(.'+i.slideDuplicateClass+")").eq(0).index(),l.nextTick(function(){a.slideTo(n)})):a.slideTo(n)}else a.slideTo(n)}},b={loopCreate:function(){var t=this,a=t.params,i=t.$wrapperEl;i.children("."+a.slideClass+"."+a.slideDuplicateClass).remove();var s=i.children("."+a.slideClass);if(a.loopFillGroupWithBlank){var r=a.slidesPerGroup-s.length%a.slidesPerGroup;if(r!==a.slidesPerGroup){for(var n=0;n<r;n+=1){var o=e(d.createElement("div")).addClass(a.slideClass+" "+a.slideBlankClass);i.append(o)}s=i.children("."+a.slideClass)}}"auto"!==a.slidesPerView||a.loopedSlides||(a.loopedSlides=s.length),t.loopedSlides=parseInt(a.loopedSlides||a.slidesPerView,10),t.loopedSlides+=a.loopAdditionalSlides,t.loopedSlides>s.length&&(t.loopedSlides=s.length);var l=[],p=[];s.each(function(a,i){var r=e(i);a<t.loopedSlides&&p.push(i),a<s.length&&a>=s.length-t.loopedSlides&&l.push(i),r.attr("data-swiper-slide-index",a)});for(var c=0;c<p.length;c+=1)i.append(e(p[c].cloneNode(!0)).addClass(a.slideDuplicateClass));for(var u=l.length-1;u>=0;u-=1)i.prepend(e(l[u].cloneNode(!0)).addClass(a.slideDuplicateClass))},loopFix:function(){var e,t=this,a=t.params,i=t.activeIndex,s=t.slides,r=t.loopedSlides,n=t.allowSlidePrev,o=t.allowSlideNext;t.allowSlidePrev=!0,t.allowSlideNext=!0,i<r?(e=s.length-3*r+i,e+=r,t.slideTo(e,0,!1,!0)):("auto"===a.slidesPerView&&i>=2*r||i>s.length-2*a.slidesPerView)&&(e=-s.length+i+r,e+=r,t.slideTo(e,0,!1,!0)),t.allowSlidePrev=n,t.allowSlideNext=o},loopDestroy:function(){var e=this,t=e.$wrapperEl,a=e.params,i=e.slides;t.children("."+a.slideClass+"."+a.slideDuplicateClass).remove(),i.removeAttr("data-swiper-slide-index")}},w={setGrabCursor:function(e){var t=this;if(!p.touch&&t.params.simulateTouch){var a=t.el;a.style.cursor="move",a.style.cursor=e?"-webkit-grabbing":"-webkit-grab",a.style.cursor=e?"-moz-grabbin":"-moz-grab",a.style.cursor=e?"grabbing":"grab"}},unsetGrabCursor:function(){var e=this;p.touch||(e.el.style.cursor="")}},y={appendSlide:function(e){var t=this,a=t.$wrapperEl,i=t.params;if(i.loop&&t.loopDestroy(),"object"==typeof e&&"length"in e)for(var s=0;s<e.length;s+=1)e[s]&&a.append(e[s]);else a.append(e);i.loop&&t.loopCreate(),i.observer&&p.observer||t.update()},prependSlide:function(e){var t=this,a=t.params,i=t.$wrapperEl,s=t.activeIndex;a.loop&&t.loopDestroy();var r=s+1;if("object"==typeof e&&"length"in e){for(var n=0;n<e.length;n+=1)e[n]&&i.prepend(e[n]);r=s+e.length}else i.prepend(e);a.loop&&t.loopCreate(),a.observer&&p.observer||t.update(),t.slideTo(r,0,!1)},removeSlide:function(e){var t=this,a=t.params,i=t.$wrapperEl,s=t.activeIndex;a.loop&&(t.loopDestroy(),t.slides=i.children("."+a.slideClass));var r,n=s;if("object"==typeof e&&"length"in e){for(var o=0;o<e.length;o+=1)r=e[o],t.slides[r]&&t.slides.eq(r).remove(),r<n&&(n-=1);n=Math.max(n,0)}else r=e,t.slides[r]&&t.slides.eq(r).remove(),r<n&&(n-=1),n=Math.max(n,0);a.loop&&t.loopCreate(),a.observer&&p.observer||t.update(),a.loop?t.slideTo(n+t.loopedSlides,0,!1):t.slideTo(n,0,!1)},removeAllSlides:function(){for(var e=this,t=[],a=0;a<e.slides.length;a+=1)t.push(a);e.removeSlide(t)}},x=function(){var e=s.navigator.userAgent,t={ios:!1,android:!1,androidChrome:!1,desktop:!1,windows:!1,iphone:!1,ipod:!1,ipad:!1,cordova:s.cordova||s.phonegap,phonegap:s.cordova||s.phonegap},a=e.match(/(Windows Phone);?[\s\/]+([\d.]+)?/),i=e.match(/(Android);?[\s\/]+([\d.]+)?/),r=e.match(/(iPad).*OS\s([\d_]+)/),n=e.match(/(iPod)(.*OS\s([\d_]+))?/),o=!r&&e.match(/(iPhone\sOS|iOS)\s([\d_]+)/);if(a&&(t.os="windows",t.osVersion=a[2],t.windows=!0),i&&!a&&(t.os="android",t.osVersion=i[2],t.android=!0,t.androidChrome=e.toLowerCase().indexOf("chrome")>=0),(r||o||n)&&(t.os="ios",t.ios=!0),o&&!n&&(t.osVersion=o[2].replace(/_/g,"."),t.iphone=!0),r&&(t.osVersion=r[2].replace(/_/g,"."),t.ipad=!0),n&&(t.osVersion=n[3]?n[3].replace(/_/g,"."):null,t.iphone=!0),t.ios&&t.osVersion&&e.indexOf("Version/")>=0&&"10"===t.osVersion.split(".")[0]&&(t.osVersion=e.toLowerCase().split("version/")[1].split(" ")[0]),t.desktop=!(t.os||t.android||t.webView),t.webView=(o||r||n)&&e.match(/.*AppleWebKit(?!.*Safari)/i),t.os&&"ios"===t.os){var l=t.osVersion.split("."),p=d.querySelector('meta[name="viewport"]');t.minimalUi=!t.webView&&(n||o)&&(1*l[0]==7?1*l[1]>=1:1*l[0]>7)&&p&&p.getAttribute("content").indexOf("minimal-ui")>=0}return t.pixelRatio=s.devicePixelRatio||1,t}(),T=function(t){var a=this,i=a.touchEventsData,s=a.params,r=a.touches,n=t;if(n.originalEvent&&(n=n.originalEvent),i.isTouchEvent="touchstart"===n.type,(i.isTouchEvent||!("which"in n)||3!==n.which)&&(!i.isTouched||!i.isMoved))if(s.noSwiping&&e(n.target).closest("."+s.noSwipingClass)[0])a.allowClick=!0;else if(!s.swipeHandler||e(n).closest(s.swipeHandler)[0]){r.currentX="touchstart"===n.type?n.targetTouches[0].pageX:n.pageX,r.currentY="touchstart"===n.type?n.targetTouches[0].pageY:n.pageY;var o=r.currentX,p=r.currentY;if(!(x.ios&&!x.cordova&&s.iOSEdgeSwipeDetection&&o<=s.iOSEdgeSwipeThreshold&&o>=window.screen.width-s.iOSEdgeSwipeThreshold)){if(l.extend(i,{isTouched:!0,isMoved:!1,allowTouchCallbacks:!0,isScrolling:void 0,startMoving:void 0}),r.startX=o,r.startY=p,i.touchStartTime=l.now(),a.allowClick=!0,a.updateSize(),a.swipeDirection=void 0,s.threshold>0&&(i.allowThresholdMove=!1),"touchstart"!==n.type){var c=!0;e(n.target).is(i.formElements)&&(c=!1),d.activeElement&&e(d.activeElement).is(i.formElements)&&d.activeElement.blur(),c&&a.allowTouchMove&&n.preventDefault()}a.emit("touchStart",n)}}},E=function(t){var a=this,i=a.touchEventsData,s=a.params,r=a.touches,n=a.rtl,o=t;if(o.originalEvent&&(o=o.originalEvent),!i.isTouchEvent||"mousemove"!==o.type){var p="touchmove"===o.type?o.targetTouches[0].pageX:o.pageX,c="touchmove"===o.type?o.targetTouches[0].pageY:o.pageY;if(o.preventedByNestedSwiper)return r.startX=p,void(r.startY=c);if(!a.allowTouchMove)return a.allowClick=!1,void(i.isTouched&&(l.extend(r,{startX:p,startY:c,currentX:p,currentY:c}),i.touchStartTime=l.now()));if(i.isTouchEvent&&s.touchReleaseOnEdges&&!s.loop)if(a.isVertical()){if(c<r.startY&&a.translate<=a.maxTranslate()||c>r.startY&&a.translate>=a.minTranslate())return i.isTouched=!1,void(i.isMoved=!1)}else if(p<r.startX&&a.translate<=a.maxTranslate()||p>r.startX&&a.translate>=a.minTranslate())return;if(i.isTouchEvent&&d.activeElement&&o.target===d.activeElement&&e(o.target).is(i.formElements))return i.isMoved=!0,void(a.allowClick=!1);if(i.allowTouchCallbacks&&a.emit("touchMove",o),!(o.targetTouches&&o.targetTouches.length>1)){r.currentX=p,r.currentY=c;var u=r.currentX-r.startX,h=r.currentY-r.startY;if(void 0===i.isScrolling){var v;a.isHorizontal()&&r.currentY===r.startY||a.isVertical()&&r.currentX===r.startX?i.isScrolling=!1:u*u+h*h>=25&&(v=180*Math.atan2(Math.abs(h),Math.abs(u))/Math.PI,i.isScrolling=a.isHorizontal()?v>s.touchAngle:90-v>s.touchAngle)}if(i.isScrolling&&a.emit("touchMoveOpposite",o),"undefined"==typeof startMoving&&(r.currentX===r.startX&&r.currentY===r.startY||(i.startMoving=!0)),i.isTouched)if(i.isScrolling)i.isTouched=!1;else if(i.startMoving){a.allowClick=!1,o.preventDefault(),s.touchMoveStopPropagation&&!s.nested&&o.stopPropagation(),i.isMoved||(s.loop&&a.loopFix(),i.startTranslate=a.getTranslate(),a.setTransition(0),a.animating&&a.$wrapperEl.trigger("webkitTransitionEnd transitionend"),i.allowMomentumBounce=!1,!s.grabCursor||!0!==a.allowSlideNext&&!0!==a.allowSlidePrev||a.setGrabCursor(!0),a.emit("sliderFirstMove",o)),a.emit("sliderMove",o),i.isMoved=!0;var f=a.isHorizontal()?u:h;r.diff=f,f*=s.touchRatio,n&&(f=-f),a.swipeDirection=f>0?"prev":"next",i.currentTranslate=f+i.startTranslate;var m=!0,g=s.resistanceRatio;if(s.touchReleaseOnEdges&&(g=0),f>0&&i.currentTranslate>a.minTranslate()?(m=!1,s.resistance&&(i.currentTranslate=a.minTranslate()-1+Math.pow(-a.minTranslate()+i.startTranslate+f,g))):f<0&&i.currentTranslate<a.maxTranslate()&&(m=!1,s.resistance&&(i.currentTranslate=a.maxTranslate()+1-Math.pow(a.maxTranslate()-i.startTranslate-f,g))),m&&(o.preventedByNestedSwiper=!0),!a.allowSlideNext&&"next"===a.swipeDirection&&i.currentTranslate<i.startTranslate&&(i.currentTranslate=i.startTranslate),!a.allowSlidePrev&&"prev"===a.swipeDirection&&i.currentTranslate>i.startTranslate&&(i.currentTranslate=i.startTranslate),s.threshold>0){if(!(Math.abs(f)>s.threshold||i.allowThresholdMove))return void(i.currentTranslate=i.startTranslate);if(!i.allowThresholdMove)return i.allowThresholdMove=!0,r.startX=r.currentX,r.startY=r.currentY,i.currentTranslate=i.startTranslate,void(r.diff=a.isHorizontal()?r.currentX-r.startX:r.currentY-r.startY)}s.followFinger&&((s.freeMode||s.watchSlidesProgress||s.watchSlidesVisibility)&&(a.updateActiveIndex(),a.updateSlidesClasses()),s.freeMode&&(0===i.velocities.length&&i.velocities.push({position:r[a.isHorizontal()?"startX":"startY"],time:i.touchStartTime}),i.velocities.push({position:r[a.isHorizontal()?"currentX":"currentY"],time:l.now()})),a.updateProgress(i.currentTranslate),a.setTranslate(i.currentTranslate))}}}},S=function(e){var t=this,a=t.touchEventsData,i=t.params,s=t.touches,r=t.rtl,n=t.$wrapperEl,o=t.slidesGrid,d=t.snapGrid,p=e;if(p.originalEvent&&(p=p.originalEvent),a.allowTouchCallbacks&&t.emit("touchEnd",p),a.allowTouchCallbacks=!1,a.isTouched){i.grabCursor&&a.isMoved&&a.isTouched&&(!0===t.allowSlideNext||!0===t.allowSlidePrev)&&t.setGrabCursor(!1);var c=l.now(),u=c-a.touchStartTime;if(t.allowClick&&(t.updateClickedSlide(p),t.emit("tap",p),u<300&&c-a.lastClickTime>300&&(a.clickTimeout&&clearTimeout(a.clickTimeout),a.clickTimeout=l.nextTick(function(){t&&!t.destroyed&&t.emit("click",p)},300)),u<300&&c-a.lastClickTime<300&&(a.clickTimeout&&clearTimeout(a.clickTimeout),t.emit("doubleTap",p))),a.lastClickTime=l.now(),l.nextTick(function(){t.destroyed||(t.allowClick=!0)}),!a.isTouched||!a.isMoved||!t.swipeDirection||0===s.diff||a.currentTranslate===a.startTranslate)return a.isTouched=!1,void(a.isMoved=!1);a.isTouched=!1,a.isMoved=!1;var h;if(h=i.followFinger?r?t.translate:-t.translate:-a.currentTranslate,i.freeMode){if(h<-t.minTranslate())return void t.slideTo(t.activeIndex);if(h>-t.maxTranslate())return void(t.slides.length<d.length?t.slideTo(d.length-1):t.slideTo(t.slides.length-1));if(i.freeModeMomentum){if(a.velocities.length>1){var v=a.velocities.pop(),f=a.velocities.pop(),m=v.position-f.position,g=v.time-f.time;t.velocity=m/g,t.velocity/=2,Math.abs(t.velocity)<i.freeModeMinimumVelocity&&(t.velocity=0),(g>150||l.now()-v.time>300)&&(t.velocity=0)}else t.velocity=0;t.velocity*=i.freeModeMomentumVelocityRatio,a.velocities.length=0;var b=1e3*i.freeModeMomentumRatio,w=t.velocity*b,y=t.translate+w;r&&(y=-y);var x,T=!1,E=20*Math.abs(t.velocity)*i.freeModeMomentumBounceRatio;if(y<t.maxTranslate())i.freeModeMomentumBounce?(y+t.maxTranslate()<-E&&(y=t.maxTranslate()-E),x=t.maxTranslate(),T=!0,a.allowMomentumBounce=!0):y=t.maxTranslate();else if(y>t.minTranslate())i.freeModeMomentumBounce?(y-t.minTranslate()>E&&(y=t.minTranslate()+E),x=t.minTranslate(),T=!0,a.allowMomentumBounce=!0):y=t.minTranslate();else if(i.freeModeSticky){for(var S,C=0;C<d.length;C+=1)if(d[C]>-y){S=C;break}y=-(y=Math.abs(d[S]-y)<Math.abs(d[S-1]-y)||"next"===t.swipeDirection?d[S]:d[S-1])}if(0!==t.velocity)b=r?Math.abs((-y-t.translate)/t.velocity):Math.abs((y-t.translate)/t.velocity);else if(i.freeModeSticky)return void t.slideReset();i.freeModeMomentumBounce&&T?(t.updateProgress(x),t.setTransition(b),t.setTranslate(y),t.transitionStart(),t.animating=!0,n.transitionEnd(function(){t&&!t.destroyed&&a.allowMomentumBounce&&(t.emit("momentumBounce"),t.setTransition(i.speed),t.setTranslate(x),n.transitionEnd(function(){t&&!t.destroyed&&t.transitionEnd()}))})):t.velocity?(t.updateProgress(y),t.setTransition(b),t.setTranslate(y),t.transitionStart(),t.animating||(t.animating=!0,n.transitionEnd(function(){t&&!t.destroyed&&t.transitionEnd()}))):t.updateProgress(y),t.updateActiveIndex(),t.updateSlidesClasses()}(!i.freeModeMomentum||u>=i.longSwipesMs)&&(t.updateProgress(),t.updateActiveIndex(),t.updateSlidesClasses())}else{for(var M=0,z=t.slidesSizesGrid[0],P=0;P<o.length;P+=i.slidesPerGroup)void 0!==o[P+i.slidesPerGroup]?h>=o[P]&&h<o[P+i.slidesPerGroup]&&(M=P,z=o[P+i.slidesPerGroup]-o[P]):h>=o[P]&&(M=P,z=o[o.length-1]-o[o.length-2]);var k=(h-o[M])/z;if(u>i.longSwipesMs){if(!i.longSwipes)return void t.slideTo(t.activeIndex);"next"===t.swipeDirection&&(k>=i.longSwipesRatio?t.slideTo(M+i.slidesPerGroup):t.slideTo(M)),"prev"===t.swipeDirection&&(k>1-i.longSwipesRatio?t.slideTo(M+i.slidesPerGroup):t.slideTo(M))}else{if(!i.shortSwipes)return void t.slideTo(t.activeIndex);"next"===t.swipeDirection&&t.slideTo(M+i.slidesPerGroup),"prev"===t.swipeDirection&&t.slideTo(M)}}}},C=function(){var e=this,t=e.params,a=e.el;if(!a||0!==a.offsetWidth){t.breakpoints&&e.setBreakpoint();var i=e.allowSlideNext,s=e.allowSlidePrev;if(e.allowSlideNext=!0,e.allowSlidePrev=!0,e.updateSize(),e.updateSlides(),t.freeMode){var r=Math.min(Math.max(e.translate,e.maxTranslate()),e.minTranslate());e.setTranslate(r),e.updateActiveIndex(),e.updateSlidesClasses(),t.autoHeight&&e.updateAutoHeight()}else e.updateSlidesClasses(),("auto"===t.slidesPerView||t.slidesPerView>1)&&e.isEnd&&!e.params.centeredSlides?e.slideTo(e.slides.length-1,0,!1,!0):e.slideTo(e.activeIndex,0,!1,!0);e.allowSlidePrev=s,e.allowSlideNext=i}},M=function(e){var t=this;t.allowClick||(t.params.preventClicks&&e.preventDefault(),t.params.preventClicksPropagation&&t.animating&&(e.stopPropagation(),e.stopImmediatePropagation()))},z={init:!0,direction:"horizontal",touchEventsTarget:"container",initialSlide:0,speed:300,iOSEdgeSwipeDetection:!1,iOSEdgeSwipeThreshold:20,freeMode:!1,freeModeMomentum:!0,freeModeMomentumRatio:1,freeModeMomentumBounce:!0,freeModeMomentumBounceRatio:1,freeModeMomentumVelocityRatio:1,freeModeSticky:!1,freeModeMinimumVelocity:.02,autoHeight:!1,setWrapperSize:!1,virtualTranslate:!1,effect:"slide",breakpoints:void 0,spaceBetween:0,slidesPerView:1,slidesPerColumn:1,slidesPerColumnFill:"column",slidesPerGroup:1,centeredSlides:!1,slidesOffsetBefore:0,slidesOffsetAfter:0,normalizeSlideIndex:!0,roundLengths:!1,touchRatio:1,touchAngle:45,simulateTouch:!0,shortSwipes:!0,longSwipes:!0,longSwipesRatio:.5,longSwipesMs:300,followFinger:!0,allowTouchMove:!0,threshold:0,touchMoveStopPropagation:!0,touchReleaseOnEdges:!1,uniqueNavElements:!0,resistance:!0,resistanceRatio:.85,watchSlidesProgress:!1,watchSlidesVisibility:!1,grabCursor:!1,preventClicks:!0,preventClicksPropagation:!0,slideToClickedSlide:!1,preloadImages:!0,updateOnImagesReady:!0,loop:!1,loopAdditionalSlides:0,loopedSlides:null,loopFillGroupWithBlank:!1,allowSlidePrev:!0,allowSlideNext:!0,swipeHandler:null,noSwiping:!0,noSwipingClass:"swiper-no-swiping",passiveListeners:!0,containerModifierClass:"swiper-container-",slideClass:"swiper-slide",slideBlankClass:"swiper-slide-invisible-blank",slideActiveClass:"swiper-slide-active",slideDuplicateActiveClass:"swiper-slide-duplicate-active",slideVisibleClass:"swiper-slide-visible",slideDuplicateClass:"swiper-slide-duplicate",slideNextClass:"swiper-slide-next",slideDuplicateNextClass:"swiper-slide-duplicate-next",slidePrevClass:"swiper-slide-prev",slideDuplicatePrevClass:"swiper-slide-duplicate-prev",wrapperClass:"swiper-wrapper",runCallbacksOnInit:!0},P={update:h,translate:v,transition:f,slide:g,loop:b,grabCursor:w,manipulation:y,events:{attachEvents:function(){var e=this,t=e.params,a=e.touchEvents,i=e.el,s=e.wrapperEl;e.onTouchStart=T.bind(e),e.onTouchMove=E.bind(e),e.onTouchEnd=S.bind(e),e.onClick=M.bind(e);var r="container"===t.touchEventsTarget?i:s,n=!!t.nested;if(m.ie)r.addEventListener(a.start,e.onTouchStart,!1),(p.touch?r:d).addEventListener(a.move,e.onTouchMove,n),(p.touch?r:d).addEventListener(a.end,e.onTouchEnd,!1);else{if(p.touch){var o=!("touchstart"!==a.start||!p.passiveListener||!t.passiveListeners)&&{passive:!0,capture:!1};r.addEventListener(a.start,e.onTouchStart,o),r.addEventListener(a.move,e.onTouchMove,p.passiveListener?{passive:!1,capture:n}:n),r.addEventListener(a.end,e.onTouchEnd,o)}(t.simulateTouch&&!x.ios&&!x.android||t.simulateTouch&&!p.touch&&x.ios)&&(r.addEventListener("mousedown",e.onTouchStart,!1),d.addEventListener("mousemove",e.onTouchMove,n),d.addEventListener("mouseup",e.onTouchEnd,!1))}(t.preventClicks||t.preventClicksPropagation)&&r.addEventListener("click",e.onClick,!0),e.on("resize observerUpdate",C)},detachEvents:function(){var e=this,t=e.params,a=e.touchEvents,i=e.el,s=e.wrapperEl,r="container"===t.touchEventsTarget?i:s,n=!!t.nested;if(m.ie)r.removeEventListener(a.start,e.onTouchStart,!1),(p.touch?r:d).removeEventListener(a.move,e.onTouchMove,n),(p.touch?r:d).removeEventListener(a.end,e.onTouchEnd,!1);else{if(p.touch){var o=!("onTouchStart"!==a.start||!p.passiveListener||!t.passiveListeners)&&{passive:!0,capture:!1};r.removeEventListener(a.start,e.onTouchStart,o),r.removeEventListener(a.move,e.onTouchMove,n),r.removeEventListener(a.end,e.onTouchEnd,o)}(t.simulateTouch&&!x.ios&&!x.android||t.simulateTouch&&!p.touch&&x.ios)&&(r.removeEventListener("mousedown",e.onTouchStart,!1),d.removeEventListener("mousemove",e.onTouchMove,n),d.removeEventListener("mouseup",e.onTouchEnd,!1))}(t.preventClicks||t.preventClicksPropagation)&&r.removeEventListener("click",e.onClick,!0),e.off("resize observerUpdate",C)}},breakpoints:{setBreakpoint:function(){var e=this,t=e.activeIndex,a=e.loopedSlides;void 0===a&&(a=0);var i=e.params,s=i.breakpoints;if(s&&(!s||0!==Object.keys(s).length)){var r=e.getBreakpoint(s);if(r&&e.currentBreakpoint!==r){var n=r in s?s[r]:e.originalParams,o=i.loop&&n.slidesPerView!==i.slidesPerView;l.extend(e.params,n),l.extend(e,{allowTouchMove:e.params.allowTouchMove,allowSlideNext:e.params.allowSlideNext,allowSlidePrev:e.params.allowSlidePrev}),e.currentBreakpoint=r,o&&(e.loopDestroy(),e.loopCreate(),e.updateSlides(),e.slideTo(t-a+e.loopedSlides,0,!1)),e.emit("breakpoint",n)}}},getBreakpoint:function(e){if(e){var t=!1,a=[];Object.keys(e).forEach(function(e){a.push(e)}),a.sort(function(e,t){return parseInt(e,10)-parseInt(t,10)});for(var i=0;i<a.length;i+=1){var r=a[i];r>=s.innerWidth&&!t&&(t=r)}return t||"max"}}},classes:{addClasses:function(){var e=this,t=e.classNames,a=e.params,i=e.rtl,r=e.$el,n=[];n.push(a.direction),a.freeMode&&n.push("free-mode"),p.flexbox||n.push("no-flexbox"),a.autoHeight&&n.push("autoheight"),i&&n.push("rtl"),a.slidesPerColumn>1&&n.push("multirow"),x.android&&n.push("android"),x.ios&&n.push("ios"),(s.navigator.pointerEnabled||s.navigator.msPointerEnabled)&&n.push("wp8-"+a.direction),n.forEach(function(e){t.push(a.containerModifierClass+e)}),r.addClass(t.join(" "))},removeClasses:function(){var e=this,t=e.$el,a=e.classNames;t.removeClass(a.join(" "))}},images:{loadImage:function(e,t,a,i,r,n){function o(){n&&n()}var l;e.complete&&r?o():t?((l=new s.Image).onload=o,l.onerror=o,i&&(l.sizes=i),a&&(l.srcset=a),t&&(l.src=t)):o()},preloadImages:function(){var e=this;e.imagesToLoad=e.$el.find("img");for(var t=0;t<e.imagesToLoad.length;t+=1){var a=e.imagesToLoad[t];e.loadImage(a,a.currentSrc||a.getAttribute("src"),a.srcset||a.getAttribute("srcset"),a.sizes||a.getAttribute("sizes"),!0,function(){void 0!==e&&null!==e&&e&&!e.destroyed&&(void 0!==e.imagesLoaded&&(e.imagesLoaded+=1),e.imagesLoaded===e.imagesToLoad.length&&(e.params.updateOnImagesReady&&e.update(),e.emit("imagesReady")))})}}}},k={},$=function(t){function a(){for(var i=[],r=arguments.length;r--;)i[r]=arguments[r];var n,o;if(1===i.length&&i[0].constructor&&i[0].constructor===Object)o=i[0];else{var d;n=(d=i)[0],o=d[1]}o||(o={}),o=l.extend({},o),n&&!o.el&&(o.el=n),t.call(this,o),Object.keys(P).forEach(function(e){Object.keys(P[e]).forEach(function(t){a.prototype[t]||(a.prototype[t]=P[e][t])})});var c=this;void 0===c.modules&&(c.modules={}),Object.keys(c.modules).forEach(function(e){var t=c.modules[e];if(t.params){var a=Object.keys(t.params)[0],i=t.params[a];if("object"!=typeof i)return;if(!(a in o&&"enabled"in i))return;!0===o[a]&&(o[a]={enabled:!0}),"object"!=typeof o[a]||"enabled"in o[a]||(o[a].enabled=!0),o[a]||(o[a]={enabled:!1})}});var u=l.extend({},z);c.useModulesParams(u),c.params=l.extend({},u,k,o),c.originalParams=l.extend({},c.params),c.passedParams=l.extend({},o);var h=e(c.params.el);if(n=h[0]){if(h.length>1){var v=[];return h.each(function(e,t){var i=l.extend({},o,{el:t});v.push(new a(i))}),v}n.swiper=c,h.data("swiper",c);var f=h.children("."+c.params.wrapperClass);return l.extend(c,{$el:h,el:n,$wrapperEl:f,wrapperEl:f[0],classNames:[],slides:e(),slidesGrid:[],snapGrid:[],slidesSizesGrid:[],isHorizontal:function(){return"horizontal"===c.params.direction},isVertical:function(){return"vertical"===c.params.direction},rtl:"horizontal"===c.params.direction&&("rtl"===n.dir.toLowerCase()||"rtl"===h.css("direction")),wrongRTL:"-webkit-box"===f.css("display"),activeIndex:0,realIndex:0,isBeginning:!0,isEnd:!1,translate:0,progress:0,velocity:0,animating:!1,allowSlideNext:c.params.allowSlideNext,allowSlidePrev:c.params.allowSlidePrev,touchEvents:function(){var e=["touchstart","touchmove","touchend"],t=["mousedown","mousemove","mouseup"];return s.navigator.pointerEnabled?t=["pointerdown","pointermove","pointerup"]:s.navigator.msPointerEnabled&&(t=["MSPointerDown","MsPointerMove","MsPointerUp"]),{start:p.touch||!c.params.simulateTouch?e[0]:t[0],move:p.touch||!c.params.simulateTouch?e[1]:t[1],end:p.touch||!c.params.simulateTouch?e[2]:t[2]}}(),touchEventsData:{isTouched:void 0,isMoved:void 0,allowTouchCallbacks:void 0,touchStartTime:void 0,isScrolling:void 0,currentTranslate:void 0,startTranslate:void 0,allowThresholdMove:void 0,formElements:"input, select, option, textarea, button, video",lastClickTime:l.now(),clickTimeout:void 0,velocities:[],allowMomentumBounce:void 0,isTouchEvent:void 0,startMoving:void 0},allowClick:!0,allowTouchMove:c.params.allowTouchMove,touches:{startX:0,startY:0,currentX:0,currentY:0,diff:0},imagesToLoad:[],imagesLoaded:0}),c.useModules(),c.params.init&&c.init(),c}}t&&(a.__proto__=t),a.prototype=Object.create(t&&t.prototype),a.prototype.constructor=a;var i={extendedDefaults:{},defaults:{},Class:{},$:{}};return a.prototype.slidesPerViewDynamic=function(){var e=this,t=e.params,a=e.slides,i=e.slidesGrid,s=e.size,r=e.activeIndex,n=1;if(t.centeredSlides){for(var o,l=a[r].swiperSlideSize,d=r+1;d<a.length;d+=1)a[d]&&!o&&(n+=1,(l+=a[d].swiperSlideSize)>s&&(o=!0));for(var p=r-1;p>=0;p-=1)a[p]&&!o&&(n+=1,(l+=a[p].swiperSlideSize)>s&&(o=!0))}else for(var c=r+1;c<a.length;c+=1)i[c]-i[r]<s&&(n+=1);return n},a.prototype.update=function(){function e(){a=Math.min(Math.max(t.translate,t.maxTranslate()),t.minTranslate()),t.setTranslate(a),t.updateActiveIndex(),t.updateSlidesClasses()}var t=this;if(t&&!t.destroyed){t.updateSize(),t.updateSlides(),t.updateProgress(),t.updateSlidesClasses();var a;t.params.freeMode?(e(),t.params.autoHeight&&t.updateAutoHeight()):(("auto"===t.params.slidesPerView||t.params.slidesPerView>1)&&t.isEnd&&!t.params.centeredSlides?t.slideTo(t.slides.length-1,0,!1,!0):t.slideTo(t.activeIndex,0,!1,!0))||e(),t.emit("update")}},a.prototype.init=function(){var e=this;e.initialized||(e.emit("beforeInit"),e.params.breakpoints&&e.setBreakpoint(),e.addClasses(),e.params.loop&&e.loopCreate(),e.updateSize(),e.updateSlides(),e.params.grabCursor&&e.setGrabCursor(),e.params.preloadImages&&e.preloadImages(),e.params.loop?e.slideTo(e.params.initialSlide+e.loopedSlides,0,e.params.runCallbacksOnInit):e.slideTo(e.params.initialSlide,0,e.params.runCallbacksOnInit),e.attachEvents(),e.initialized=!0,e.emit("init"))},a.prototype.destroy=function(e,t){void 0===e&&(e=!0),void 0===t&&(t=!0);var a=this,i=a.params,s=a.$el,r=a.$wrapperEl,n=a.slides;a.emit("beforeDestroy"),a.initialized=!1,a.detachEvents(),i.loop&&a.loopDestroy(),t&&(a.removeClasses(),s.removeAttr("style"),r.removeAttr("style"),n&&n.length&&n.removeClass([i.slideVisibleClass,i.slideActiveClass,i.slideNextClass,i.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-slide-index").removeAttr("data-swiper-column").removeAttr("data-swiper-row")),a.emit("destroy"),Object.keys(a.eventsListeners).forEach(function(e){a.off(e)}),!1!==e&&(a.$el[0].swiper=null,a.$el.data("swiper",null),l.deleteProps(a)),a.destroyed=!0},a.extendDefaults=function(e){l.extend(k,e)},i.extendedDefaults.get=function(){return k},i.defaults.get=function(){return z},i.Class.get=function(){return t},i.$.get=function(){return e},Object.defineProperties(a,i),a}(c),I={name:"device",proto:{device:x},static:{device:x}},L={name:"support",proto:{support:p},static:{support:p}},D={name:"browser",proto:{browser:m},static:{browser:m}},O={name:"resize",create:function(){var e=this;l.extend(e,{resize:{resizeHandler:function(){e&&!e.destroyed&&e.initialized&&(e.emit("beforeResize"),e.emit("resize"))},orientationChangeHandler:function(){e&&!e.destroyed&&e.initialized&&e.emit("orientationchange")}}})},on:{init:function(){var e=this;s.addEventListener("resize",e.resize.resizeHandler),s.addEventListener("orientationchange",e.resize.orientationChangeHandler)},destroy:function(){var e=this;s.removeEventListener("resize",e.resize.resizeHandler),s.removeEventListener("orientationchange",e.resize.orientationChangeHandler)}}},A={func:s.MutationObserver||s.WebkitMutationObserver,attach:function(e,t){void 0===t&&(t={});var a=this,i=new(0,A.func)(function(e){e.forEach(function(e){a.emit("observerUpdate",e)})});i.observe(e,{attributes:void 0===t.attributes||t.attributes,childList:void 0===t.childList||t.childList,characterData:void 0===t.characterData||t.characterData}),a.observer.observers.push(i)},init:function(){var e=this;if(p.observer&&e.params.observer){if(e.params.observeParents)for(var t=e.$el.parents(),a=0;a<t.length;a+=1)e.observer.attach(t[a]);e.observer.attach(e.$el[0],{childList:!1}),e.observer.attach(e.$wrapperEl[0],{attributes:!1})}},destroy:function(){var e=this;e.observer.observers.forEach(function(e){e.disconnect()}),e.observer.observers=[]}},H={name:"observer",params:{observer:!1,observeParents:!1},create:function(){var e=this;l.extend(e,{observer:{init:A.init.bind(e),attach:A.attach.bind(e),destroy:A.destroy.bind(e),observers:[]}})},on:{init:function(){this.observer.init()},destroy:function(){this.observer.destroy()}}},N={update:function(e){function t(){a.updateSlides(),a.updateProgress(),a.updateSlidesClasses(),a.lazy&&a.params.lazy.enabled&&a.lazy.load()}var a=this,i=a.params,s=i.slidesPerView,r=i.slidesPerGroup,n=i.centeredSlides,o=a.virtual,d=o.from,p=o.to,c=o.slides,u=o.slidesGrid,h=o.renderSlide,v=o.offset;a.updateActiveIndex();var f,m=a.activeIndex||0;f=a.rtl&&a.isHorizontal()?"right":a.isHorizontal()?"left":"top";var g,b;n?(g=Math.floor(s/2)+r,b=Math.floor(s/2)+r):(g=s+(r-1),b=r);var w=Math.max((m||0)-b,0),y=Math.min((m||0)+g,c.length-1),x=(a.slidesGrid[w]||0)-(a.slidesGrid[0]||0);if(l.extend(a.virtual,{from:w,to:y,offset:x,slidesGrid:a.slidesGrid}),d===w&&p===y&&!e)return a.slidesGrid!==u&&x!==v&&a.slides.css(f,x+"px"),void a.updateProgress();if(a.params.virtual.renderExternal)return a.params.virtual.renderExternal.call(a,{offset:x,from:w,to:y,slides:function(){for(var e=[],t=w;t<=y;t+=1)e.push(c[t]);return e}()}),void t();var T=[],E=[];if(e)a.$wrapperEl.find("."+a.params.slideClass).remove();else for(var S=d;S<=p;S+=1)(S<w||S>y)&&a.$wrapperEl.find("."+a.params.slideClass+'[data-swiper-slide-index="'+S+'"]').remove();for(var C=0;C<c.length;C+=1)C>=w&&C<=y&&(void 0===p||e?E.push(C):(C>p&&E.push(C),C<d&&T.push(C)));E.forEach(function(e){a.$wrapperEl.append(h(c[e],e))}),T.sort(function(e,t){return e<t}).forEach(function(e){a.$wrapperEl.prepend(h(c[e],e))}),a.$wrapperEl.children(".swiper-slide").css(f,x+"px"),t()},renderSlide:function(t,a){var i=this,s=i.params.virtual;if(s.cache&&i.virtual.cache[a])return i.virtual.cache[a];var r=e(s.renderSlide?s.renderSlide.call(i,t,a):'<div class="'+i.params.slideClass+'" data-swiper-slide-index="'+a+'">'+t+"</div>");return r.attr("data-swiper-slide-index")||r.attr("data-swiper-slide-index",a),s.cache&&(i.virtual.cache[a]=r),r},appendSlide:function(e){var t=this;t.virtual.slides.push(e),t.virtual.update(!0)},prependSlide:function(e){var t=this;if(t.virtual.slides.unshift(e),t.params.virtual.cache){var a=t.virtual.cache,i={};Object.keys(a).forEach(function(e){i[e+1]=a[e]}),t.virtual.cache=i}t.virtual.update(!0),t.slideNext(0)}},X={name:"virtual",params:{virtual:{enabled:!1,slides:[],cache:!0,renderSlide:null,renderExternal:null}},create:function(){var e=this;l.extend(e,{virtual:{update:N.update.bind(e),appendSlide:N.appendSlide.bind(e),prependSlide:N.prependSlide.bind(e),renderSlide:N.renderSlide.bind(e),slides:e.params.virtual.slides,cache:{}}})},on:{beforeInit:function(){var e=this;if(e.params.virtual.enabled){e.classNames.push(e.params.containerModifierClass+"virtual");var t={watchSlidesProgress:!0};l.extend(e.params,t),l.extend(e.originalParams,t),e.virtual.update()}},setTranslate:function(){var e=this;e.params.virtual.enabled&&e.virtual.update()}}},Y={handle:function(e){var t=this,a=e;a.originalEvent&&(a=a.originalEvent);var i=a.keyCode||a.charCode;if(!t.allowSlideNext&&(t.isHorizontal()&&39===i||t.isVertical()&&40===i))return!1;if(!t.allowSlidePrev&&(t.isHorizontal()&&37===i||t.isVertical()&&38===i))return!1;if(!(a.shiftKey||a.altKey||a.ctrlKey||a.metaKey||d.activeElement&&d.activeElement.nodeName&&("input"===d.activeElement.nodeName.toLowerCase()||"textarea"===d.activeElement.nodeName.toLowerCase()))){if(37===i||39===i||38===i||40===i){var r=!1;if(t.$el.parents("."+t.params.slideClass).length>0&&0===t.$el.parents("."+t.params.slideActiveClass).length)return;var n={left:s.pageXOffset,top:s.pageYOffset},o=s.innerWidth,l=s.innerHeight,p=t.$el.offset();t.rtl&&(p.left-=t.$el[0].scrollLeft);for(var c=[[p.left,p.top],[p.left+t.width,p.top],[p.left,p.top+t.height],[p.left+t.width,p.top+t.height]],u=0;u<c.length;u+=1){var h=c[u];h[0]>=n.left&&h[0]<=n.left+o&&h[1]>=n.top&&h[1]<=n.top+l&&(r=!0)}if(!r)return}t.isHorizontal()?(37!==i&&39!==i||(a.preventDefault?a.preventDefault():a.returnValue=!1),(39===i&&!t.rtl||37===i&&t.rtl)&&t.slideNext(),(37===i&&!t.rtl||39===i&&t.rtl)&&t.slidePrev()):(38!==i&&40!==i||(a.preventDefault?a.preventDefault():a.returnValue=!1),40===i&&t.slideNext(),38===i&&t.slidePrev()),t.emit("keyPress",i)}},enable:function(){var t=this;t.keyboard.enabled||(e(d).on("keydown",t.keyboard.handle),t.keyboard.enabled=!0)},disable:function(){var t=this;t.keyboard.enabled&&(e(d).off("keydown",t.keyboard.handle),t.keyboard.enabled=!1)}},G={name:"keyboard",params:{keyboard:{enabled:!1}},create:function(){var e=this;l.extend(e,{keyboard:{enabled:!1,enable:Y.enable.bind(e),disable:Y.disable.bind(e),handle:Y.handle.bind(e)}})},on:{init:function(){var e=this;e.params.keyboard.enabled&&e.keyboard.enable()},destroy:function(){var e=this;e.keyboard.enabled&&e.keyboard.disable()}}},B={lastScrollTime:l.now(),event:s.navigator.userAgent.indexOf("firefox")>-1?"DOMMouseScroll":a()?"wheel":"mousewheel",normalize:function(e){var t=0,a=0,i=0,s=0;return"detail"in e&&(a=e.detail),"wheelDelta"in e&&(a=-e.wheelDelta/120),"wheelDeltaY"in e&&(a=-e.wheelDeltaY/120),"wheelDeltaX"in e&&(t=-e.wheelDeltaX/120),"axis"in e&&e.axis===e.HORIZONTAL_AXIS&&(t=a,a=0),i=10*t,s=10*a,"deltaY"in e&&(s=e.deltaY),"deltaX"in e&&(i=e.deltaX),(i||s)&&e.deltaMode&&(1===e.deltaMode?(i*=40,s*=40):(i*=800,s*=800)),i&&!t&&(t=i<1?-1:1),s&&!a&&(a=s<1?-1:1),{spinX:t,spinY:a,pixelX:i,pixelY:s}},handle:function(e){var t=e,a=this,i=a.params.mousewheel;t.originalEvent&&(t=t.originalEvent);var r=0,n=a.rtl?-1:1,o=B.normalize(t);if(i.forceToAxis)if(a.isHorizontal()){if(!(Math.abs(o.pixelX)>Math.abs(o.pixelY)))return!0;r=o.pixelX*n}else{if(!(Math.abs(o.pixelY)>Math.abs(o.pixelX)))return!0;r=o.pixelY}else r=Math.abs(o.pixelX)>Math.abs(o.pixelY)?-o.pixelX*n:-o.pixelY;if(0===r)return!0;if(i.invert&&(r=-r),a.params.freeMode){var d=a.getTranslate()+r*i.sensitivity,p=a.isBeginning,c=a.isEnd;if(d>=a.minTranslate()&&(d=a.minTranslate()),d<=a.maxTranslate()&&(d=a.maxTranslate()),a.setTransition(0),a.setTranslate(d),a.updateProgress(),a.updateActiveIndex(),a.updateSlidesClasses(),(!p&&a.isBeginning||!c&&a.isEnd)&&a.updateSlidesClasses(),a.params.freeModeSticky&&(clearTimeout(a.mousewheel.timeout),a.mousewheel.timeout=l.nextTick(function(){a.slideReset()},300)),a.emit("scroll",t),a.params.autoplay&&a.params.autoplayDisableOnInteraction&&a.stopAutoplay(),0===d||d===a.maxTranslate())return!0}else{if(l.now()-a.mousewheel.lastScrollTime>60)if(r<0)if(a.isEnd&&!a.params.loop||a.animating){if(i.releaseOnEdges)return!0}else a.slideNext(),a.emit("scroll",t);else if(a.isBeginning&&!a.params.loop||a.animating){if(i.releaseOnEdges)return!0}else a.slidePrev(),a.emit("scroll",t);a.mousewheel.lastScrollTime=(new s.Date).getTime()}return t.preventDefault?t.preventDefault():t.returnValue=!1,!1},enable:function(){var t=this;if(!B.event)return!1;if(t.mousewheel.enabled)return!1;var a=t.$el;return"container"!==t.params.mousewheel.eventsTarged&&(a=e(t.params.mousewheel.eventsTarged)),a.on(B.event,t.mousewheel.handle),t.mousewheel.enabled=!0,!0},disable:function(){var t=this;if(!B.event)return!1;if(!t.mousewheel.enabled)return!1;var a=t.$el;return"container"!==t.params.mousewheel.eventsTarged&&(a=e(t.params.mousewheel.eventsTarged)),a.off(B.event,t.mousewheel.handle),t.mousewheel.enabled=!1,!0}},V={name:"mousewheel",params:{mousewheel:{enabled:!1,releaseOnEdges:!1,invert:!1,forceToAxis:!1,sensitivity:1,eventsTarged:"container"}},create:function(){var e=this;l.extend(e,{mousewheel:{enabled:!1,enable:B.enable.bind(e),disable:B.disable.bind(e),handle:B.handle.bind(e),lastScrollTime:l.now()}})},on:{init:function(){var e=this;e.params.mousewheel.enabled&&e.mousewheel.enable()},destroy:function(){var e=this;e.mousewheel.enabled&&e.mousewheel.disable()}}},R={update:function(){var e=this,t=e.params.navigation;if(!e.params.loop){var a=e.navigation,i=a.$nextEl,s=a.$prevEl;s&&s.length>0&&(e.isBeginning?s.addClass(t.disabledClass):s.removeClass(t.disabledClass)),i&&i.length>0&&(e.isEnd?i.addClass(t.disabledClass):i.removeClass(t.disabledClass))}},init:function(){var t=this,a=t.params.navigation;if(a.nextEl||a.prevEl){var i,s;a.nextEl&&(i=e(a.nextEl),t.params.uniqueNavElements&&"string"==typeof a.nextEl&&i.length>1&&1===t.$el.find(a.nextEl).length&&(i=t.$el.find(a.nextEl))),a.prevEl&&(s=e(a.prevEl),t.params.uniqueNavElements&&"string"==typeof a.prevEl&&s.length>1&&1===t.$el.find(a.prevEl).length&&(s=t.$el.find(a.prevEl))),i&&i.length>0&&i.on("click",function(e){e.preventDefault(),t.isEnd&&!t.params.loop||t.slideNext()}),s&&s.length>0&&s.on("click",function(e){e.preventDefault(),t.isBeginning&&!t.params.loop||t.slidePrev()}),l.extend(t.navigation,{$nextEl:i,nextEl:i&&i[0],$prevEl:s,prevEl:s&&s[0]})}},destroy:function(){var e=this,t=e.navigation,a=t.$nextEl,i=t.$prevEl;a&&a.length&&(a.off("click"),a.removeClass(e.params.navigation.disabledClass)),i&&i.length&&(i.off("click"),i.removeClass(e.params.navigation.disabledClass))}},W={name:"navigation",params:{navigation:{nextEl:null,prevEl:null,hideOnClick:!1,disabledClass:"swiper-button-disabled",hiddenClass:"swiper-button-hidden"}},create:function(){var e=this;l.extend(e,{navigation:{init:R.init.bind(e),update:R.update.bind(e),destroy:R.destroy.bind(e)}})},on:{init:function(){var e=this;e.navigation.init(),e.navigation.update()},toEdge:function(){this.navigation.update()},fromEdge:function(){this.navigation.update()},destroy:function(){this.navigation.destroy()},click:function(t){var a=this,i=a.navigation,s=i.$nextEl,r=i.$prevEl;!a.params.navigation.hideOnClick||e(t.target).is(r)||e(t.target).is(s)||(s&&s.toggleClass(a.params.navigation.hiddenClass),r&&r.toggleClass(a.params.navigation.hiddenClass))}}},F={update:function(){var t=this,a=t.rtl,i=t.params.pagination;if(i.el&&t.pagination.el&&t.pagination.$el&&0!==t.pagination.$el.length){var s,r=t.virtual&&t.params.virtual.enabled?t.virtual.slides.length:t.slides.length,n=t.pagination.$el,o=t.params.loop?Math.ceil((r-2*t.loopedSlides)/t.params.slidesPerGroup):t.snapGrid.length;if(t.params.loop?((s=Math.ceil((t.activeIndex-t.loopedSlides)/t.params.slidesPerGroup))>r-1-2*t.loopedSlides&&(s-=r-2*t.loopedSlides),s>o-1&&(s-=o),s<0&&"bullets"!==t.params.paginationType&&(s=o+s)):s=void 0!==t.snapIndex?t.snapIndex:t.activeIndex||0,"bullets"===i.type&&t.pagination.bullets&&t.pagination.bullets.length>0){var l=t.pagination.bullets;if(i.dynamicBullets&&(t.pagination.bulletSize=l.eq(0)[t.isHorizontal()?"outerWidth":"outerHeight"](!0),n.css(t.isHorizontal()?"width":"height",5*t.pagination.bulletSize+"px")),l.removeClass(i.bulletActiveClass+" "+i.bulletActiveClass+"-next "+i.bulletActiveClass+"-next-next "+i.bulletActiveClass+"-prev "+i.bulletActiveClass+"-prev-prev"),n.length>1)l.each(function(t,a){var r=e(a);r.index()===s&&(r.addClass(i.bulletActiveClass),i.dynamicBullets&&(r.prev().addClass(i.bulletActiveClass+"-prev").prev().addClass(i.bulletActiveClass+"-prev-prev"),r.next().addClass(i.bulletActiveClass+"-next").next().addClass(i.bulletActiveClass+"-next-next")))});else{var d=l.eq(s);d.addClass(i.bulletActiveClass),i.dynamicBullets&&(d.prev().addClass(i.bulletActiveClass+"-prev").prev().addClass(i.bulletActiveClass+"-prev-prev"),d.next().addClass(i.bulletActiveClass+"-next").next().addClass(i.bulletActiveClass+"-next-next"))}if(i.dynamicBullets){var p=Math.min(l.length,5),c=(t.pagination.bulletSize*p-t.pagination.bulletSize)/2-s*t.pagination.bulletSize,u=a?"right":"left";l.css(t.isHorizontal()?u:"top",c+"px")}}if("fraction"===i.type&&(n.find("."+i.currentClass).text(s+1),n.find("."+i.totalClass).text(o)),"progressbar"===i.type){var h=(s+1)/o,v=h,f=1;t.isHorizontal()||(f=h,v=1),n.find("."+i.progressbarFillClass).transform("translate3d(0,0,0) scaleX("+v+") scaleY("+f+")").transition(t.params.speed)}"custom"===i.type&&i.renderCustom?(n.html(i.renderCustom(t,s+1,o)),t.emit("paginationRender",t,n[0])):t.emit("paginationUpdate",t,n[0])}},render:function(){var e=this,t=e.params.pagination;if(t.el&&e.pagination.el&&e.pagination.$el&&0!==e.pagination.$el.length){var a=e.virtual&&e.params.virtual.enabled?e.virtual.slides.length:e.slides.length,i=e.pagination.$el,s="";if("bullets"===t.type){for(var r=e.params.loop?Math.ceil((a-2*e.loopedSlides)/e.params.slidesPerGroup):e.snapGrid.length,n=0;n<r;n+=1)t.renderBullet?s+=t.renderBullet.call(e,n,t.bulletClass):s+="<"+t.bulletElement+' class="'+t.bulletClass+'"></'+t.bulletElement+">";i.html(s),e.pagination.bullets=i.find("."+t.bulletClass)}"fraction"===t.type&&(s=t.renderFraction?t.renderFraction.call(e,t.currentClass,t.totalClass):'<span class="'+t.currentClass+'"></span> / <span class="'+t.totalClass+'"></span>',i.html(s)),"progressbar"===t.type&&(s=t.renderProgressbar?t.renderProgressbar.call(e,t.progressbarFillClass):'<span class="'+t.progressbarFillClass+'"></span>',i.html(s)),"custom"!==t.type&&e.emit("paginationRender",e.pagination.$el[0])}},init:function(){var t=this,a=t.params.pagination;if(a.el){var i=e(a.el);0!==i.length&&(t.params.uniqueNavElements&&"string"==typeof a.el&&i.length>1&&1===t.$el.find(a.el).length&&(i=t.$el.find(a.el)),"bullets"===a.type&&a.clickable&&i.addClass(a.clickableClass),i.addClass(a.modifierClass+a.type),"bullets"===a.type&&a.dynamicBullets&&i.addClass(""+a.modifierClass+a.type+"-dynamic"),a.clickable&&i.on("click","."+a.bulletClass,function(a){a.preventDefault();var i=e(this).index()*t.params.slidesPerGroup;t.params.loop&&(i+=t.loopedSlides),t.slideTo(i)}),l.extend(t.pagination,{$el:i,el:i[0]}))}},destroy:function(){var e=this,t=e.params.pagination;if(t.el&&e.pagination.el&&e.pagination.$el&&0!==e.pagination.$el.length){var a=e.pagination.$el;a.removeClass(t.hiddenClass),a.removeClass(t.modifierClass+t.type),e.pagination.bullets&&e.pagination.bullets.removeClass(t.bulletActiveClass),t.clickable&&a.off("click","."+t.bulletClass)}}},j={name:"pagination",params:{pagination:{el:null,bulletElement:"span",clickable:!1,hideOnClick:!1,renderBullet:null,renderProgressbar:null,renderFraction:null,renderCustom:null,type:"bullets",dynamicBullets:!1,bulletClass:"swiper-pagination-bullet",bulletActiveClass:"swiper-pagination-bullet-active",modifierClass:"swiper-pagination-",currentClass:"swiper-pagination-current",totalClass:"swiper-pagination-total",hiddenClass:"swiper-pagination-hidden",progressbarFillClass:"swiper-pagination-progressbar-fill",clickableClass:"swiper-pagination-clickable"}},create:function(){var e=this;l.extend(e,{pagination:{init:F.init.bind(e),render:F.render.bind(e),update:F.update.bind(e),destroy:F.destroy.bind(e)}})},on:{init:function(){var e=this;e.pagination.init(),e.pagination.render(),e.pagination.update()},activeIndexChange:function(){var e=this;e.params.loop?e.pagination.update():void 0===e.snapIndex&&e.pagination.update()},snapIndexChange:function(){var e=this;e.params.loop||e.pagination.update()},slidesLengthChange:function(){var e=this;e.params.loop&&(e.pagination.render(),e.pagination.update())},snapGridLengthChange:function(){var e=this;e.params.loop||(e.pagination.render(),e.pagination.update())},destroy:function(){this.pagination.destroy()},click:function(t){var a=this;a.params.pagination.el&&a.params.pagination.hideOnClick&&a.pagination.$el.length>0&&!e(t.target).hasClass(a.params.pagination.bulletClass)&&a.pagination.$el.toggleClass(a.params.pagination.hiddenClass)}}},q={setTranslate:function(){var e=this;if(e.params.scrollbar.el&&e.scrollbar.el){var t=e.scrollbar,a=e.rtl,i=e.progress,s=t.dragSize,r=t.trackSize,n=t.$dragEl,o=t.$el,l=e.params.scrollbar,d=s,c=(r-s)*i;a&&e.isHorizontal()?(c=-c)>0?(d=s-c,c=0):-c+s>r&&(d=r+c):c<0?(d=s+c,c=0):c+s>r&&(d=r-c),e.isHorizontal()?(p.transforms3d?n.transform("translate3d("+c+"px, 0, 0)"):n.transform("translateX("+c+"px)"),n[0].style.width=d+"px"):(p.transforms3d?n.transform("translate3d(0px, "+c+"px, 0)"):n.transform("translateY("+c+"px)"),n[0].style.height=d+"px"),l.hide&&(clearTimeout(e.scrollbar.timeout),o[0].style.opacity=1,e.scrollbar.timeout=setTimeout(function(){o[0].style.opacity=0,o.transition(400)},1e3))}},setTransition:function(e){var t=this;t.params.scrollbar.el&&t.scrollbar.el&&t.scrollbar.$dragEl.transition(e)},updateSize:function(){var e=this;if(e.params.scrollbar.el&&e.scrollbar.el){var t=e.scrollbar,a=t.$dragEl,i=t.$el;a[0].style.width="",a[0].style.height="";var s,r=e.isHorizontal()?i[0].offsetWidth:i[0].offsetHeight,n=e.size/e.virtualSize,o=n*(r/e.size);s="auto"===e.params.scrollbar.dragSize?r*n:parseInt(e.params.scrollbar.dragSize,10),e.isHorizontal()?a[0].style.width=s+"px":a[0].style.height=s+"px",i[0].style.display=n>=1?"none":"",e.params.scrollbarHide&&(i[0].style.opacity=0),l.extend(t,{trackSize:r,divider:n,moveDivider:o,dragSize:s})}},setDragPosition:function(e){var t,a=this,i=a.scrollbar,s=i.$el,r=i.dragSize,n=i.trackSize;t=((a.isHorizontal()?"touchstart"===e.type||"touchmove"===e.type?e.targetTouches[0].pageX:e.pageX||e.clientX:"touchstart"===e.type||"touchmove"===e.type?e.targetTouches[0].pageY:e.pageY||e.clientY)-s.offset()[a.isHorizontal()?"left":"top"]-r/2)/(n-r),t=Math.max(Math.min(t,1),0),a.rtl&&(t=1-t);var o=a.minTranslate()+(a.maxTranslate()-a.minTranslate())*t;a.updateProgress(o),a.setTranslate(o),a.updateActiveIndex(),a.updateSlidesClasses()},onDragStart:function(e){var t=this,a=t.params.scrollbar,i=t.scrollbar,s=t.$wrapperEl,r=i.$el,n=i.$dragEl;t.scrollbar.isTouched=!0,e.preventDefault(),e.stopPropagation(),s.transition(100),n.transition(100),i.setDragPosition(e),clearTimeout(t.scrollbar.dragTimeout),r.transition(0),a.hide&&r.css("opacity",1),t.emit("scrollbarDragStart",e)},onDragMove:function(e){var t=this,a=t.scrollbar,i=t.$wrapperEl,s=a.$el,r=a.$dragEl;t.scrollbar.isTouched&&(e.preventDefault?e.preventDefault():e.returnValue=!1,a.setDragPosition(e),i.transition(0),s.transition(0),r.transition(0),t.emit("scrollbarDragMove",e))},onDragEnd:function(e){var t=this,a=t.params.scrollbar,i=t.scrollbar.$el;t.scrollbar.isTouched&&(t.scrollbar.isTouched=!1,a.hide&&(clearTimeout(t.scrollbar.dragTimeout),t.scrollbar.dragTimeout=l.nextTick(function(){i.css("opacity",0),i.transition(400)},1e3)),t.emit("scrollbarDragEnd",e),a.snapOnRelease&&t.slideReset())},enableDraggable:function(){var t=this;if(t.params.scrollbar.el){var a=t.scrollbar.$el,i=p.touch?a[0]:document;a.on(t.scrollbar.dragEvents.start,t.scrollbar.onDragStart),e(i).on(t.scrollbar.dragEvents.move,t.scrollbar.onDragMove),e(i).on(t.scrollbar.dragEvents.end,t.scrollbar.onDragEnd)}},disableDraggable:function(){var t=this;if(t.params.scrollbar.el){var a=t.scrollbar.$el,i=p.touch?a[0]:document;a.off(t.scrollbar.dragEvents.start),e(i).off(t.scrollbar.dragEvents.move),e(i).off(t.scrollbar.dragEvents.end)}},init:function(){var t=this;if(t.params.scrollbar.el){var a=t.scrollbar,i=t.$el,s=t.touchEvents,r=t.params.scrollbar,n=e(r.el);t.params.uniqueNavElements&&"string"==typeof r.el&&n.length>1&&1===i.find(r.el).length&&(n=i.find(r.el));var o=n.find(".swiper-scrollbar-drag");0===o.length&&(o=e('<div class="swiper-scrollbar-drag"></div>'),n.append(o)),t.scrollbar.dragEvents=!1!==t.params.simulateTouch||p.touch?s:{start:"mousedown",move:"mousemove",end:"mouseup"},l.extend(a,{$el:n,el:n[0],$dragEl:o,dragEl:o[0]}),r.draggable&&a.enableDraggable()}},destroy:function(){this.scrollbar.disableDraggable()}},K={name:"scrollbar",params:{scrollbar:{el:null,dragSize:"auto",hide:!1,draggable:!1,snapOnRelease:!0}},create:function(){var e=this;l.extend(e,{scrollbar:{init:q.init.bind(e),destroy:q.destroy.bind(e),updateSize:q.updateSize.bind(e),setTranslate:q.setTranslate.bind(e),setTransition:q.setTransition.bind(e),enableDraggable:q.enableDraggable.bind(e),disableDraggable:q.disableDraggable.bind(e),setDragPosition:q.setDragPosition.bind(e),onDragStart:q.onDragStart.bind(e),onDragMove:q.onDragMove.bind(e),onDragEnd:q.onDragEnd.bind(e),isTouched:!1,timeout:null,dragTimeout:null}})},on:{init:function(){var e=this;e.scrollbar.init(),e.scrollbar.updateSize(),e.scrollbar.setTranslate()},update:function(){this.scrollbar.updateSize()},resize:function(){this.scrollbar.updateSize()},observerUpdate:function(){this.scrollbar.updateSize()},setTranslate:function(){this.scrollbar.setTranslate()},setTransition:function(e){this.scrollbar.setTransition(e)},destroy:function(){this.scrollbar.destroy()}}},U={setTransform:function(t,a){var i=this,s=i.rtl,r=e(t),n=s?-1:1,o=r.attr("data-swiper-parallax")||"0",l=r.attr("data-swiper-parallax-x"),d=r.attr("data-swiper-parallax-y"),p=r.attr("data-swiper-parallax-scale"),c=r.attr("data-swiper-parallax-opacity");if(l||d?(l=l||"0",d=d||"0"):i.isHorizontal()?(l=o,d="0"):(d=o,l="0"),l=l.indexOf("%")>=0?parseInt(l,10)*a*n+"%":l*a*n+"px",d=d.indexOf("%")>=0?parseInt(d,10)*a+"%":d*a+"px",void 0!==c&&null!==c){var u=c-(c-1)*(1-Math.abs(a));r[0].style.opacity=u}if(void 0===p||null===p)r.transform("translate3d("+l+", "+d+", 0px)");else{var h=p-(p-1)*(1-Math.abs(a));r.transform("translate3d("+l+", "+d+", 0px) scale("+h+")")}},setTranslate:function(){var t=this,a=t.$el,i=t.slides,s=t.progress,r=t.snapGrid;a.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(e,a){t.parallax.setTransform(a,s)}),i.each(function(a,i){var n=i.progress;t.params.slidesPerGroup>1&&"auto"!==t.params.slidesPerView&&(n+=Math.ceil(a/2)-s*(r.length-1)),n=Math.min(Math.max(n,-1),1),e(i).find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(e,a){t.parallax.setTransform(a,n)})})},setTransition:function(t){void 0===t&&(t=this.params.speed),this.$el.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function(a,i){var s=e(i),r=parseInt(s.attr("data-swiper-parallax-duration"),10)||t;0===t&&(r=0),s.transition(r)})}},_={name:"parallax",params:{parallax:{enabled:!1}},create:function(){var e=this;l.extend(e,{parallax:{setTransform:U.setTransform.bind(e),setTranslate:U.setTranslate.bind(e),setTransition:U.setTransition.bind(e)}})},on:{beforeInit:function(){this.params.watchSlidesProgress=!0},init:function(){var e=this;e.params.parallax&&e.parallax.setTranslate()},setTranslate:function(){var e=this;e.params.parallax&&e.parallax.setTranslate()},setTransition:function(e){var t=this;t.params.parallax&&t.parallax.setTransition(e)}}},Z={getDistanceBetweenTouches:function(e){if(e.targetTouches.length<2)return 1;var t=e.targetTouches[0].pageX,a=e.targetTouches[0].pageY,i=e.targetTouches[1].pageX,s=e.targetTouches[1].pageY;return Math.sqrt(Math.pow(i-t,2)+Math.pow(s-a,2))},onGestureStart:function(t){var a=this,i=a.params.zoom,s=a.zoom,r=s.gesture;if(s.fakeGestureTouched=!1,s.fakeGestureMoved=!1,!p.gestures){if("touchstart"!==t.type||"touchstart"===t.type&&t.targetTouches.length<2)return;s.fakeGestureTouched=!0,r.scaleStart=Z.getDistanceBetweenTouches(t)}r.$slideEl&&r.$slideEl.length||(r.$slideEl=e(this),0===r.$slideEl.length&&(r.$slideEl=a.slides.eq(a.activeIndex)),r.$imageEl=r.$slideEl.find("img, svg, canvas"),r.$imageWrapEl=r.$imageEl.parent("."+i.containerClass),r.maxRatio=r.$imageWrapEl.attr("data-swiper-zoom")||i.maxRatio,0!==r.$imageWrapEl.length)?(r.$imageEl.transition(0),a.zoom.isScaling=!0):r.$imageEl=void 0},onGestureChange:function(e){var t=this,a=t.params.zoom,i=t.zoom,s=i.gesture;if(!p.gestures){if("touchmove"!==e.type||"touchmove"===e.type&&e.targetTouches.length<2)return;i.fakeGestureMoved=!0,s.scaleMove=Z.getDistanceBetweenTouches(e)}s.$imageEl&&0!==s.$imageEl.length&&(p.gestures?t.zoom.scale=e.scale*i.currentScale:i.scale=s.scaleMove/s.scaleStart*i.currentScale,i.scale>s.maxRatio&&(i.scale=s.maxRatio-1+Math.pow(i.scale-s.maxRatio+1,.5)),i.scale<a.minRatio&&(i.scale=a.minRatio+1-Math.pow(a.minRatio-i.scale+1,.5)),s.$imageEl.transform("translate3d(0,0,0) scale("+i.scale+")"))},onGestureEnd:function(e){var t=this,a=t.params.zoom,i=t.zoom,s=i.gesture;if(!p.gestures){if(!i.fakeGestureTouched||!i.fakeGestureMoved)return;if("touchend"!==e.type||"touchend"===e.type&&e.changedTouches.length<2&&!x.android)return;i.fakeGestureTouched=!1,i.fakeGestureMoved=!1}s.$imageEl&&0!==s.$imageEl.length&&(i.scale=Math.max(Math.min(i.scale,s.maxRatio),a.minRatio),s.$imageEl.transition(t.params.speed).transform("translate3d(0,0,0) scale("+i.scale+")"),i.currentScale=i.scale,i.isScaling=!1,1===i.scale&&(s.$slideEl=void 0))},onTouchStart:function(e){var t=this.zoom,a=t.gesture,i=t.image;a.$imageEl&&0!==a.$imageEl.length&&(i.isTouched||(x.android&&e.preventDefault(),i.isTouched=!0,i.touchesStart.x="touchstart"===e.type?e.targetTouches[0].pageX:e.pageX,i.touchesStart.y="touchstart"===e.type?e.targetTouches[0].pageY:e.pageY))},onTouchMove:function(e){var t=this,a=t.zoom,i=a.gesture,s=a.image,r=a.velocity;if(i.$imageEl&&0!==i.$imageEl.length&&(t.allowClick=!1,s.isTouched&&i.$slideEl)){s.isMoved||(s.width=i.$imageEl[0].offsetWidth,s.height=i.$imageEl[0].offsetHeight,s.startX=l.getTranslate(i.$imageWrapEl[0],"x")||0,s.startY=l.getTranslate(i.$imageWrapEl[0],"y")||0,i.slideWidth=i.$slideEl[0].offsetWidth,i.slideHeight=i.$slideEl[0].offsetHeight,i.$imageWrapEl.transition(0),t.rtl&&(s.startX=-s.startX),t.rtl&&(s.startY=-s.startY));var n=s.width*a.scale,o=s.height*a.scale;if(!(n<i.slideWidth&&o<i.slideHeight)){if(s.minX=Math.min(i.slideWidth/2-n/2,0),s.maxX=-s.minX,s.minY=Math.min(i.slideHeight/2-o/2,0),s.maxY=-s.minY,s.touchesCurrent.x="touchmove"===e.type?e.targetTouches[0].pageX:e.pageX,s.touchesCurrent.y="touchmove"===e.type?e.targetTouches[0].pageY:e.pageY,!s.isMoved&&!a.isScaling){if(t.isHorizontal()&&(Math.floor(s.minX)===Math.floor(s.startX)&&s.touchesCurrent.x<s.touchesStart.x||Math.floor(s.maxX)===Math.floor(s.startX)&&s.touchesCurrent.x>s.touchesStart.x))return void(s.isTouched=!1);if(!t.isHorizontal()&&(Math.floor(s.minY)===Math.floor(s.startY)&&s.touchesCurrent.y<s.touchesStart.y||Math.floor(s.maxY)===Math.floor(s.startY)&&s.touchesCurrent.y>s.touchesStart.y))return void(s.isTouched=!1)}e.preventDefault(),e.stopPropagation(),s.isMoved=!0,s.currentX=s.touchesCurrent.x-s.touchesStart.x+s.startX,s.currentY=s.touchesCurrent.y-s.touchesStart.y+s.startY,s.currentX<s.minX&&(s.currentX=s.minX+1-Math.pow(s.minX-s.currentX+1,.8)),s.currentX>s.maxX&&(s.currentX=s.maxX-1+Math.pow(s.currentX-s.maxX+1,.8)),s.currentY<s.minY&&(s.currentY=s.minY+1-Math.pow(s.minY-s.currentY+1,.8)),s.currentY>s.maxY&&(s.currentY=s.maxY-1+Math.pow(s.currentY-s.maxY+1,.8)),r.prevPositionX||(r.prevPositionX=s.touchesCurrent.x),r.prevPositionY||(r.prevPositionY=s.touchesCurrent.y),r.prevTime||(r.prevTime=Date.now()),r.x=(s.touchesCurrent.x-r.prevPositionX)/(Date.now()-r.prevTime)/2,r.y=(s.touchesCurrent.y-r.prevPositionY)/(Date.now()-r.prevTime)/2,Math.abs(s.touchesCurrent.x-r.prevPositionX)<2&&(r.x=0),Math.abs(s.touchesCurrent.y-r.prevPositionY)<2&&(r.y=0),r.prevPositionX=s.touchesCurrent.x,r.prevPositionY=s.touchesCurrent.y,r.prevTime=Date.now(),i.$imageWrapEl.transform("translate3d("+s.currentX+"px, "+s.currentY+"px,0)")}}},onTouchEnd:function(){var e=this.zoom,t=e.gesture,a=e.image,i=e.velocity;if(t.$imageEl&&0!==t.$imageEl.length){if(!a.isTouched||!a.isMoved)return a.isTouched=!1,void(a.isMoved=!1);a.isTouched=!1,a.isMoved=!1;var s=300,r=300,n=i.x*s,o=a.currentX+n,l=i.y*r,d=a.currentY+l;0!==i.x&&(s=Math.abs((o-a.currentX)/i.x)),0!==i.y&&(r=Math.abs((d-a.currentY)/i.y));var p=Math.max(s,r);a.currentX=o,a.currentY=d;var c=a.width*e.scale,u=a.height*e.scale;a.minX=Math.min(t.slideWidth/2-c/2,0),a.maxX=-a.minX,a.minY=Math.min(t.slideHeight/2-u/2,0),a.maxY=-a.minY,a.currentX=Math.max(Math.min(a.currentX,a.maxX),a.minX),a.currentY=Math.max(Math.min(a.currentY,a.maxY),a.minY),t.$imageWrapEl.transition(p).transform("translate3d("+a.currentX+"px, "+a.currentY+"px,0)")}},onTransitionEnd:function(){var e=this,t=e.zoom,a=t.gesture;a.$slideEl&&e.previousIndex!==e.activeIndex&&(a.$imageEl.transform("translate3d(0,0,0) scale(1)"),a.$imageWrapEl.transform("translate3d(0,0,0)"),a.$slideEl=void 0,a.$imageEl=void 0,a.$imageWrapEl=void 0,t.scale=1,t.currentScale=1)},toggle:function(e){var t=this.zoom;t.scale&&1!==t.scale?t.out():t.in(e)},in:function(t){var a=this,i=a.zoom,s=a.params.zoom,r=i.gesture,n=i.image;if(r.$slideEl||(r.$slideEl=a.clickedSlide?e(a.clickedSlide):a.slides.eq(a.activeIndex),r.$imageEl=r.$slideEl.find("img, svg, canvas"),r.$imageWrapEl=r.$imageEl.parent("."+s.containerClass)),r.$imageEl&&0!==r.$imageEl.length){r.$slideEl.addClass(""+s.zoomedSlideClass);var o,l,d,p,c,u,h,v,f,m,g,b,w,y,x,T;void 0===n.touchesStart.x&&t?(o="touchend"===t.type?t.changedTouches[0].pageX:t.pageX,l="touchend"===t.type?t.changedTouches[0].pageY:t.pageY):(o=n.touchesStart.x,l=n.touchesStart.y),i.scale=r.$imageWrapEl.attr("data-swiper-zoom")||s.maxRatio,i.currentScale=r.$imageWrapEl.attr("data-swiper-zoom")||s.maxRatio,t?(x=r.$slideEl[0].offsetWidth,T=r.$slideEl[0].offsetHeight,d=r.$slideEl.offset().left+x/2-o,p=r.$slideEl.offset().top+T/2-l,h=r.$imageEl[0].offsetWidth,v=r.$imageEl[0].offsetHeight,f=h*i.scale,m=v*i.scale,w=-(g=Math.min(x/2-f/2,0)),y=-(b=Math.min(T/2-m/2,0)),c=d*i.scale,u=p*i.scale,c<g&&(c=g),c>w&&(c=w),u<b&&(u=b),u>y&&(u=y)):(c=0,u=0),r.$imageWrapEl.transition(300).transform("translate3d("+c+"px, "+u+"px,0)"),r.$imageEl.transition(300).transform("translate3d(0,0,0) scale("+i.scale+")")}},out:function(){var t=this,a=t.zoom,i=t.params.zoom,s=a.gesture;s.$slideEl||(s.$slideEl=t.clickedSlide?e(t.clickedSlide):t.slides.eq(t.activeIndex),s.$imageEl=s.$slideEl.find("img, svg, canvas"),s.$imageWrapEl=s.$imageEl.parent("."+i.containerClass)),s.$imageEl&&0!==s.$imageEl.length&&(a.scale=1,a.currentScale=1,s.$imageWrapEl.transition(300).transform("translate3d(0,0,0)"),s.$imageEl.transition(300).transform("translate3d(0,0,0) scale(1)"),s.$slideEl.removeClass(""+i.zoomedSlideClass),s.$slideEl=void 0)},enable:function(){var t=this,a=t.zoom;if(!a.enabled){a.enabled=!0;var i=t.slides,s=!("touchstart"!==t.touchEvents.start||!p.passiveListener||!t.params.passiveListeners)&&{passive:!0,capture:!1};p.gestures?(i.on("gesturestart",a.onGestureStart,s),i.on("gesturechange",a.onGestureChange,s),i.on("gestureend",a.onGestureEnd,s)):"touchstart"===t.touchEvents.start&&(i.on(t.touchEvents.start,a.onGestureStart,s),i.on(t.touchEvents.move,a.onGestureChange,s),i.on(t.touchEvents.end,a.onGestureEnd,s)),t.slides.each(function(i,s){var r=e(s);r.find("."+t.params.zoom.containerClass).length>0&&r.on(t.touchEvents.move,a.onTouchMove)})}},disable:function(){var t=this,a=t.zoom;if(a.enabled){t.zoom.enabled=!1;var i=t.slides,s=!("touchstart"!==t.touchEvents.start||!p.passiveListener||!t.params.passiveListeners)&&{passive:!0,capture:!1};p.gestures?(i.off("gesturestart",a.onGestureStart,s),i.off("gesturechange",a.onGestureChange,s),i.off("gestureend",a.onGestureEnd,s)):"touchstart"===t.touchEvents.start&&(i.off(t.touchEvents.start,a.onGestureStart,s),i.off(t.touchEvents.move,a.onGestureChange,s),i.off(t.touchEvents.end,a.onGestureEnd,s)),t.slides.each(function(i,s){var r=e(s);r.find("."+t.params.zoom.containerClass).length>0&&r.off(t.touchEvents.move,a.onTouchMove)})}}},Q={name:"zoom",params:{zoom:{enabled:!1,maxRatio:3,minRatio:1,toggle:!0,containerClass:"swiper-zoom-container",zoomedSlideClass:"swiper-slide-zoomed"}},create:function(){var e=this,t={enabled:!1,scale:1,currentScale:1,isScaling:!1,gesture:{$slideEl:void 0,slideWidth:void 0,slideHeight:void 0,$imageEl:void 0,$imageWrapEl:void 0,maxRatio:3},image:{isTouched:void 0,isMoved:void 0,currentX:void 0,currentY:void 0,minX:void 0,minY:void 0,maxX:void 0,maxY:void 0,width:void 0,height:void 0,startX:void 0,startY:void 0,touchesStart:{},touchesCurrent:{}},velocity:{x:void 0,y:void 0,prevPositionX:void 0,prevPositionY:void 0,prevTime:void 0}};"onGestureStart onGestureChange onGestureEnd onTouchStart onTouchMove onTouchEnd onTransitionEnd toggle enable disable in out".split(" ").forEach(function(a){t[a]=Z[a].bind(e)}),l.extend(e,{zoom:t})},on:{init:function(){var e=this;e.params.zoom.enabled&&e.zoom.enable()},destroy:function(){this.zoom.disable()},touchStart:function(e){var t=this;t.zoom.enabled&&t.zoom.onTouchStart(e)},touchEnd:function(e){var t=this;t.zoom.enabled&&t.zoom.onTouchEnd(e)},doubleTap:function(e){var t=this;t.params.zoom.enabled&&t.zoom.enabled&&t.params.zoom.toggle&&t.zoom.toggle(e)},transitionEnd:function(){var e=this;e.zoom.enabled&&e.params.zoom.enabled&&e.zoom.onTransitionEnd()}}},J={loadInSlide:function(t,a){void 0===a&&(a=!0);var i=this,s=i.params.lazy;if(void 0!==t&&0!==i.slides.length){var r=i.virtual&&i.params.virtual.enabled?i.$wrapperEl.children("."+i.params.slideClass+'[data-swiper-slide-index="'+t+'"]'):i.slides.eq(t),n=r.find("."+s.elementClass+":not(."+s.loadedClass+"):not(."+s.loadingClass+")");!r.hasClass(s.elementClass)||r.hasClass(s.loadedClass)||r.hasClass(s.loadingClass)||(n=n.add(r[0])),0!==n.length&&n.each(function(t,n){var o=e(n);o.addClass(s.loadingClass);var l=o.attr("data-background"),d=o.attr("data-src"),p=o.attr("data-srcset"),c=o.attr("data-sizes");i.loadImage(o[0],d||l,p,c,!1,function(){if(void 0!==i&&null!==i&&i&&(!i||i.params)&&!i.destroyed){if(l?(o.css("background-image",'url("'+l+'")'),o.removeAttr("data-background")):(p&&(o.attr("srcset",p),o.removeAttr("data-srcset")),c&&(o.attr("sizes",c),o.removeAttr("data-sizes")),d&&(o.attr("src",d),o.removeAttr("data-src"))),o.addClass(s.loadedClass).removeClass(s.loadingClass),r.find("."+s.preloaderClass).remove(),i.params.loop&&a){var e=r.attr("data-swiper-slide-index");if(r.hasClass(i.params.slideDuplicateClass)){var t=i.$wrapperEl.children('[data-swiper-slide-index="'+e+'"]:not(.'+i.params.slideDuplicateClass+")");i.lazy.loadInSlide(t.index(),!1)}else{var n=i.$wrapperEl.children("."+i.params.slideDuplicateClass+'[data-swiper-slide-index="'+e+'"]');i.lazy.loadInSlide(n.index(),!1)}}i.emit("lazyImageReady",r[0],o[0])}}),i.emit("lazyImageLoad",r[0],o[0])})}},load:function(){function t(e){if(l){if(s.children("."+r.slideClass+'[data-swiper-slide-index="'+e+'"]').length)return!0}else if(n[e])return!0;return!1}function a(t){return l?e(t).attr("data-swiper-slide-index"):e(t).index()}var i=this,s=i.$wrapperEl,r=i.params,n=i.slides,o=i.activeIndex,l=i.virtual&&r.virtual.enabled,d=r.lazy,p=r.slidesPerView;if("auto"===p&&(p=0),i.lazy.initialImageLoaded||(i.lazy.initialImageLoaded=!0),i.params.watchSlidesVisibility)s.children("."+r.slideVisibleClass).each(function(t,a){var s=l?e(a).attr("data-swiper-slide-index"):e(a).index();i.lazy.loadInSlide(s)});else if(p>1)for(var c=o;c<o+p;c+=1)t(c)&&i.lazy.loadInSlide(c);else i.lazy.loadInSlide(o);if(d.loadPrevNext)if(p>1||d.loadPrevNextAmount&&d.loadPrevNextAmount>1){for(var u=d.loadPrevNextAmount,h=p,v=Math.min(o+h+Math.max(u,h),n.length),f=Math.max(o-Math.max(h,u),0),m=o+p;m<v;m+=1)t(m)&&i.lazy.loadInSlide(m);for(var g=f;g<o;g+=1)t(g)&&i.lazy.loadInSlide(g)}else{var b=s.children("."+r.slideNextClass);b.length>0&&i.lazy.loadInSlide(a(b));var w=s.children("."+r.slidePrevClass);w.length>0&&i.lazy.loadInSlide(a(w))}}},ee={name:"lazy",params:{lazy:{enabled:!1,loadPrevNext:!1,loadPrevNextAmount:1,loadOnTransitionStart:!1,elementClass:"swiper-lazy",loadingClass:"swiper-lazy-loading",loadedClass:"swiper-lazy-loaded",preloaderClass:"swiper-lazy-preloader"}},create:function(){var e=this;l.extend(e,{lazy:{initialImageLoaded:!1,load:J.load.bind(e),loadInSlide:J.loadInSlide.bind(e)}})},on:{beforeInit:function(){var e=this;e.params.lazy.enabled&&e.params.preloadImages&&(e.params.preloadImages=!1)},init:function(){var e=this;e.params.lazy.enabled&&!e.params.loop&&0===e.params.initialSlide&&e.lazy.load()},scroll:function(){var e=this;e.params.freeMode&&!e.params.freeModeSticky&&e.lazy.load()},resize:function(){var e=this;e.params.lazy.enabled&&e.lazy.load()},scrollbarDragMove:function(){var e=this;e.params.lazy.enabled&&e.lazy.load()},transitionStart:function(){var e=this;e.params.lazy.enabled&&(e.params.lazy.loadOnTransitionStart||!e.params.lazy.loadOnTransitionStart&&!e.lazy.initialImageLoaded)&&e.lazy.load()},transitionEnd:function(){var e=this;e.params.lazy.enabled&&!e.params.lazy.loadOnTransitionStart&&e.lazy.load()}}},te={LinearSpline:function(e,t){var a=function(){var e,t,a;return function(i,s){for(t=-1,e=i.length;e-t>1;)i[a=e+t>>1]<=s?t=a:e=a;return e}}();this.x=e,this.y=t,this.lastIndex=e.length-1;var i,s;return this.interpolate=function(e){return e?(s=a(this.x,e),i=s-1,(e-this.x[i])*(this.y[s]-this.y[i])/(this.x[s]-this.x[i])+this.y[i]):0},this},getInterpolateFunction:function(e){var t=this;t.controller.spline||(t.controller.spline=t.params.loop?new te.LinearSpline(t.slidesGrid,e.slidesGrid):new te.LinearSpline(t.snapGrid,e.snapGrid))},setTranslate:function(e,t){function a(e){var t=e.rtl&&"horizontal"===e.params.direction?-r.translate:r.translate;"slide"===r.params.controller.by&&(r.controller.getInterpolateFunction(e),s=-r.controller.spline.interpolate(-t)),s&&"container"!==r.params.controller.by||(i=(e.maxTranslate()-e.minTranslate())/(r.maxTranslate()-r.minTranslate()),s=(t-r.minTranslate())*i+e.minTranslate()),r.params.controller.inverse&&(s=e.maxTranslate()-s),e.updateProgress(s),e.setTranslate(s,r),e.updateActiveIndex(),e.updateSlidesClasses()}var i,s,r=this,n=r.controller.control;if(Array.isArray(n))for(var o=0;o<n.length;o+=1)n[o]!==t&&n[o]instanceof $&&a(n[o]);else n instanceof $&&t!==n&&a(n)},setTransition:function(e,t){function a(t){t.setTransition(e,s),0!==e&&(t.transitionStart(),t.$wrapperEl.transitionEnd(function(){r&&(t.params.loop&&"slide"===s.params.controller.by&&t.loopFix(),t.transitionEnd())}))}var i,s=this,r=s.controller.control;if(Array.isArray(r))for(i=0;i<r.length;i+=1)r[i]!==t&&r[i]instanceof $&&a(r[i]);else r instanceof $&&t!==r&&a(r)}},ae={name:"controller",params:{controller:{control:void 0,inverse:!1,by:"slide"}},create:function(){var e=this;l.extend(e,{controller:{control:e.params.controller.control,getInterpolateFunction:te.getInterpolateFunction.bind(e),setTranslate:te.setTranslate.bind(e),setTransition:te.setTransition.bind(e)}})},on:{update:function(){var e=this;e.controller.control&&e.controller.spline&&(e.controller.spline=void 0,delete e.controller.spline)},resize:function(){var e=this;e.controller.control&&e.controller.spline&&(e.controller.spline=void 0,delete e.controller.spline)},observerUpdate:function(){var e=this;e.controller.control&&e.controller.spline&&(e.controller.spline=void 0,delete e.controller.spline)},setTranslate:function(e,t){var a=this;a.controller.control&&a.controller.setTranslate(e,t)},setTransition:function(e,t){var a=this;a.controller.control&&a.controller.setTransition(e,t)}}},ie={makeElFocusable:function(e){return e.attr("tabIndex","0"),e},addElRole:function(e,t){return e.attr("role",t),e},addElLabel:function(e,t){return e.attr("aria-label",t),e},disableEl:function(e){return e.attr("aria-disabled",!0),e},enableEl:function(e){return e.attr("aria-disabled",!1),e},onEnterKey:function(t){var a=this,i=a.params.a11y;if(13===t.keyCode){var s=e(t.target);a.navigation&&a.navigation.$nextEl&&s.is(a.navigation.$nextEl)&&(a.isEnd&&!a.params.loop||a.slideNext(),a.isEnd?a.a11y.notify(i.lastSlideMessage):a.a11y.notify(i.nextSlideMessage)),a.navigation&&a.navigation.$prevEl&&s.is(a.navigation.$prevEl)&&(a.isBeginning&&!a.params.loop||a.slidePrev(),a.isBeginning?a.a11y.notify(i.firstSlideMessage):a.a11y.notify(i.prevSlideMessage)),a.pagination&&s.is("."+a.params.pagination.bulletClass)&&s[0].click()}},notify:function(e){var t=this.a11y.liveRegion;0!==t.length&&(t.html(""),t.html(e))},updateNavigation:function(){var e=this;if(!e.params.loop){var t=e.navigation,a=t.$nextEl,i=t.$prevEl;i&&i.length>0&&(e.isBeginning?e.a11y.disableEl(i):e.a11y.enableEl(i)),a&&a.length>0&&(e.isEnd?e.a11y.disableEl(a):e.a11y.enableEl(a))}},updatePagination:function(){var t=this,a=t.params.a11y;t.pagination&&t.params.pagination.clickable&&t.pagination.bullets&&t.pagination.bullets.length&&t.pagination.bullets.each(function(i,s){var r=e(s);t.a11y.makeElFocusable(r),t.a11y.addElRole(r,"button"),t.a11y.addElLabel(r,a.paginationBulletMessage.replace(/{{index}}/,r.index()+1))})},init:function(){var e=this;e.$el.append(e.a11y.liveRegion);var t,a,i=e.params.a11y;e.navigation&&e.navigation.$nextEl&&(t=e.navigation.$nextEl),e.navigation&&e.navigation.$prevEl&&(a=e.navigation.$prevEl),t&&(e.a11y.makeElFocusable(t),e.a11y.addElRole(t,"button"),e.a11y.addElLabel(t,i.nextSlideMessage),t.on("keydown",e.a11y.onEnterKey)),a&&(e.a11y.makeElFocusable(a),e.a11y.addElRole(a,"button"),e.a11y.addElLabel(a,i.prevSlideMessage),a.on("keydown",e.a11y.onEnterKey)),e.pagination&&e.params.pagination.clickable&&e.pagination.bullets&&e.pagination.bullets.length&&e.pagination.$el.on("keydown","."+e.params.pagination.bulletClass,e.a11y.onEnterKey)},destroy:function(){var e=this;e.a11y.liveRegion&&e.a11y.liveRegion.length>0&&e.a11y.liveRegion.remove();var t,a;e.navigation&&e.navigation.$nextEl&&(t=e.navigation.$nextEl),e.navigation&&e.navigation.$prevEl&&(a=e.navigation.$prevEl),t&&t.off("keydown",e.a11y.onEnterKey),a&&a.off("keydown",e.a11y.onEnterKey),e.pagination&&e.params.pagination.clickable&&e.pagination.bullets&&e.pagination.bullets.length&&e.pagination.$el.off("keydown","."+e.params.pagination.bulletClass,e.a11y.onEnterKey)}},se={name:"a11y",params:{a11y:{enabled:!1,notificationClass:"swiper-notification",prevSlideMessage:"Previous slide",nextSlideMessage:"Next slide",firstSlideMessage:"This is the first slide",lastSlideMessage:"This is the last slide",paginationBulletMessage:"Go to slide {{index}}"}},create:function(){var t=this;l.extend(t,{a11y:{liveRegion:e('<span class="'+t.params.a11y.notificationClass+'" aria-live="assertive" aria-atomic="true"></span>')}}),Object.keys(ie).forEach(function(e){t.a11y[e]=ie[e].bind(t)})},on:{init:function(){var e=this;e.params.a11y.enabled&&(e.a11y.init(),e.a11y.updateNavigation())},toEdge:function(){var e=this;e.params.a11y.enabled&&e.a11y.updateNavigation()},fromEdge:function(){var e=this;e.params.a11y.enabled&&e.a11y.updateNavigation()},paginationUpdate:function(){var e=this;e.params.a11y.enabled&&e.a11y.updatePagination()},destroy:function(){var e=this;e.params.a11y.enabled&&e.a11y.destroy()}}},re={init:function(){var e=this;if(e.params.history){if(!s.history||!s.history.pushState)return e.params.history.enabled=!1,void(e.params.hashNavigation.enabled=!0);var t=e.history;t.initialized=!0,t.paths=re.getPathValues(),(t.paths.key||t.paths.value)&&(t.scrollToSlide(0,t.paths.value,e.params.runCallbacksOnInit),e.params.history.replaceState||s.addEventListener("popstate",e.history.setHistoryPopState))}},destroy:function(){var e=this;e.params.history.replaceState||s.removeEventListener("popstate",e.history.setHistoryPopState)},setHistoryPopState:function(){var e=this;e.history.paths=re.getPathValues(),e.history.scrollToSlide(e.params.speed,e.history.paths.value,!1)},getPathValues:function(){var e=s.location.pathname.slice(1).split("/").filter(function(e){return""!==e}),t=e.length;return{key:e[t-2],value:e[t-1]}},setHistory:function(e,t){var a=this;if(a.history.initialized&&a.params.history.enabled){var i=a.slides.eq(t),r=re.slugify(i.attr("data-history"));s.location.pathname.includes(e)||(r=e+"/"+r);var n=s.history.state;n&&n.value===r||(a.params.history.replaceState?s.history.replaceState({value:r},null,r):s.history.pushState({value:r},null,r))}},slugify:function(e){return e.toString().toLowerCase().replace(/\s+/g,"-").replace(/[^\w-]+/g,"").replace(/--+/g,"-").replace(/^-+/,"").replace(/-+$/,"")},scrollToSlide:function(e,t,a){var i=this;if(t)for(var s=0,r=i.slides.length;s<r;s+=1){var n=i.slides.eq(s);if(re.slugify(n.attr("data-history"))===t&&!n.hasClass(i.params.slideDuplicateClass)){var o=n.index();i.slideTo(o,e,a)}}else i.slideTo(0,e,a)}},ne={name:"history",params:{history:{enabled:!1,replaceState:!1,key:"slides"}},create:function(){var e=this;l.extend(e,{history:{init:re.init.bind(e),setHistory:re.setHistory.bind(e),setHistoryPopState:re.setHistoryPopState.bind(e),scrollToSlide:re.scrollToSlide.bind(e),destroy:re.destroy.bind(e)}})},on:{init:function(){var e=this;e.params.history.enabled&&e.history.init()},destroy:function(){var e=this;e.params.history.enabled&&e.history.destroy()},transitionEnd:function(){var e=this;e.history.initialized&&e.history.setHistory(e.params.history.key,e.activeIndex)}}},oe={onHashCange:function(){var e=this,t=d.location.hash.replace("#","");t!==e.slides.eq(e.activeIndex).attr("data-hash")&&e.slideTo(e.$wrapperEl.children("."+e.params.slideClass+'[data-hash="'+t+'"]').index())},setHash:function(){var e=this;if(e.hashNavigation.initialized&&e.params.hashNavigation.enabled)if(e.params.hashNavigation.replaceState&&s.history&&s.history.replaceState)s.history.replaceState(null,null,"#"+e.slides.eq(e.activeIndex).attr("data-hash")||"");else{var t=e.slides.eq(e.activeIndex),a=t.attr("data-hash")||t.attr("data-history");d.location.hash=a||""}},init:function(){var t=this;if(!(!t.params.hashNavigation.enabled||t.params.history&&t.params.history.enabled)){t.hashNavigation.initialized=!0;var a=d.location.hash.replace("#","");if(a)for(var i=0,r=t.slides.length;i<r;i+=1){var n=t.slides.eq(i);if((n.attr("data-hash")||n.attr("data-history"))===a&&!n.hasClass(t.params.slideDuplicateClass)){var o=n.index();t.slideTo(o,0,t.params.runCallbacksOnInit,!0)}}t.params.hashNavigation.watchState&&e(s).on("hashchange",t.hashNavigation.onHashCange)}},destroy:function(){var t=this;t.params.hashNavigation.watchState&&e(s).off("hashchange",t.hashNavigation.onHashCange)}},le={name:"hash-navigation",params:{hashNavigation:{enabled:!1,replaceState:!1,watchState:!1}},create:function(){var e=this;l.extend(e,{hashNavigation:{initialized:!1,init:oe.init.bind(e),destroy:oe.destroy.bind(e),setHash:oe.setHash.bind(e),onHashCange:oe.onHashCange.bind(e)}})},on:{init:function(){var e=this;e.params.hashNavigation.enabled&&e.hashNavigation.init()},destroy:function(){var e=this;e.params.hashNavigation.enabled&&e.hashNavigation.destroy()},transitionEnd:function(){var e=this;e.hashNavigation.initialized&&e.hashNavigation.setHash()}}},de={run:function(){var e=this,t=e.slides.eq(e.activeIndex),a=e.params.autoplay.delay;t.attr("data-swiper-autoplay")&&(a=t.attr("data-swiper-autoplay")||e.params.autoplay.delay),e.autoplay.timeout=l.nextTick(function(){e.params.loop?(e.loopFix(),e.slideNext(e.params.speed,!0,!0),e.emit("autoplay")):e.isEnd?e.params.autoplay.stopOnLastSlide?e.autoplay.stop():(e.slideTo(0,e.params.speed,!0,!0),e.emit("autoplay")):(e.slideNext(e.params.speed,!0,!0),e.emit("autoplay"))},a)},start:function(){var e=this;return void 0===e.autoplay.timeout&&(!e.autoplay.running&&(e.autoplay.running=!0,e.emit("autoplayStart"),e.autoplay.run(),!0))},stop:function(){var e=this;return!!e.autoplay.running&&(void 0!==e.autoplay.timeout&&(e.autoplay.timeout&&(clearTimeout(e.autoplay.timeout),e.autoplay.timeout=void 0),e.autoplay.running=!1,e.emit("autoplayStop"),!0))},pause:function(e){var t=this;t.autoplay.running&&(t.autoplay.paused||(t.autoplay.timeout&&clearTimeout(t.autoplay.timeout),t.autoplay.paused=!0,0===e?(t.autoplay.paused=!1,t.autoplay.run()):t.$wrapperEl.transitionEnd(function(){t&&!t.destroyed&&(t.autoplay.paused=!1,t.autoplay.running?t.autoplay.run():t.autoplay.stop())})))}},pe={name:"autoplay",params:{autoplay:{enabled:!1,delay:3e3,disableOnInteraction:!0,stopOnLastSlide:!1}},create:function(){var e=this;l.extend(e,{autoplay:{running:!1,paused:!1,run:de.run.bind(e),start:de.start.bind(e),stop:de.stop.bind(e),pause:de.pause.bind(e)}})},on:{init:function(){var e=this;e.params.autoplay.enabled&&e.autoplay.start()},beforeTransitionStart:function(e,t){var a=this;a.autoplay.running&&(t||!a.params.autoplay.disableOnInteraction?a.autoplay.pause(e):a.autoplay.stop())},sliderFirstMove:function(){var e=this;e.autoplay.running&&(e.params.autoplay.disableOnInteraction?e.autoplay.stop():e.autoplay.pause())},destroy:function(){var e=this;e.autoplay.running&&e.autoplay.stop()}}},ce={setTranslate:function(){for(var e=this,t=e.slides,a=0;a<t.length;a+=1){var i=e.slides.eq(a),s=-i[0].swiperSlideOffset;e.params.virtualTranslate||(s-=e.translate);var r=0;e.isHorizontal()||(r=s,s=0);var n=e.params.fadeEffect.crossFade?Math.max(1-Math.abs(i[0].progress),0):1+Math.min(Math.max(i[0].progress,-1),0);i.css({opacity:n}).transform("translate3d("+s+"px, "+r+"px, 0px)")}},setTransition:function(e){var t=this,a=t.slides,i=t.$wrapperEl;if(a.transition(e),t.params.virtualTranslate&&0!==e){var s=!1;a.transitionEnd(function(){if(!s&&t&&!t.destroyed){s=!0,t.animating=!1;for(var e=["webkitTransitionEnd","transitionend"],a=0;a<e.length;a+=1)i.trigger(e[a])}})}}},ue={name:"effect-fade",params:{fadeEffect:{crossFade:!1}},create:function(){var e=this;l.extend(e,{fadeEffect:{setTranslate:ce.setTranslate.bind(e),setTransition:ce.setTransition.bind(e)}})},on:{beforeInit:function(){var e=this;if("fade"===e.params.effect){e.classNames.push(e.params.containerModifierClass+"fade");var t={slidesPerView:1,slidesPerColumn:1,slidesPerGroup:1,watchSlidesProgress:!0,spaceBetween:0,virtualTranslate:!0};l.extend(e.params,t),l.extend(e.originalParams,t)}},setTranslate:function(){var e=this;"fade"===e.params.effect&&e.fadeEffect.setTranslate()},setTransition:function(e){var t=this;"fade"===t.params.effect&&t.fadeEffect.setTransition(e)}}},he={setTranslate:function(){var t,a=this,i=a.$el,s=a.$wrapperEl,r=a.slides,n=a.width,o=a.height,l=a.rtl,d=a.size,p=a.params.cubeEffect,c=a.isHorizontal(),u=a.virtual&&a.params.virtual.enabled,h=0;p.shadow&&(c?(0===(t=s.find(".swiper-cube-shadow")).length&&(t=e('<div class="swiper-cube-shadow"></div>'),s.append(t)),t.css({height:n+"px"})):0===(t=i.find(".swiper-cube-shadow")).length&&(t=e('<div class="swiper-cube-shadow"></div>'),i.append(t)));for(var v=0;v<r.length;v+=1){var f=r.eq(v),g=v;u&&(g=parseInt(f.attr("data-swiper-slide-index"),10));var b=90*g,w=Math.floor(b/360);l&&(b=-b,w=Math.floor(-b/360));var y=Math.max(Math.min(f[0].progress,1),-1),x=0,T=0,E=0;g%4==0?(x=4*-w*d,E=0):(g-1)%4==0?(x=0,E=4*-w*d):(g-2)%4==0?(x=d+4*w*d,E=d):(g-3)%4==0&&(x=-d,E=3*d+4*d*w),l&&(x=-x),c||(T=x,x=0);var S="rotateX("+(c?0:-b)+"deg) rotateY("+(c?b:0)+"deg) translate3d("+x+"px, "+T+"px, "+E+"px)";if(y<=1&&y>-1&&(h=90*g+90*y,l&&(h=90*-g-90*y)),f.transform(S),p.slideShadows){var C=c?f.find(".swiper-slide-shadow-left"):f.find(".swiper-slide-shadow-top"),M=c?f.find(".swiper-slide-shadow-right"):f.find(".swiper-slide-shadow-bottom");0===C.length&&(C=e('<div class="swiper-slide-shadow-'+(c?"left":"top")+'"></div>'),f.append(C)),0===M.length&&(M=e('<div class="swiper-slide-shadow-'+(c?"right":"bottom")+'"></div>'),f.append(M)),C.length&&(C[0].style.opacity=Math.max(-y,0)),M.length&&(M[0].style.opacity=Math.max(y,0))}}if(s.css({"-webkit-transform-origin":"50% 50% -"+d/2+"px","-moz-transform-origin":"50% 50% -"+d/2+"px","-ms-transform-origin":"50% 50% -"+d/2+"px","transform-origin":"50% 50% -"+d/2+"px"}),p.shadow)if(c)t.transform("translate3d(0px, "+(n/2+p.shadowOffset)+"px, "+-n/2+"px) rotateX(90deg) rotateZ(0deg) scale("+p.shadowScale+")");else{var z=Math.abs(h)-90*Math.floor(Math.abs(h)/90),P=1.5-(Math.sin(2*z*Math.PI/360)/2+Math.cos(2*z*Math.PI/360)/2),k=p.shadowScale,$=p.shadowScale/P,I=p.shadowOffset;t.transform("scale3d("+k+", 1, "+$+") translate3d(0px, "+(o/2+I)+"px, "+-o/2/$+"px) rotateX(-90deg)")}var L=m.isSafari||m.isUiWebView?-d/2:0;s.transform("translate3d(0px,0,"+L+"px) rotateX("+(a.isHorizontal()?0:h)+"deg) rotateY("+(a.isHorizontal()?-h:0)+"deg)")},setTransition:function(e){var t=this,a=t.$el;t.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e),t.params.cubeEffect.shadow&&!t.isHorizontal()&&a.find(".swiper-cube-shadow").transition(e)}},ve={name:"effect-cube",params:{cubeEffect:{slideShadows:!0,shadow:!0,shadowOffset:20,shadowScale:.94}},create:function(){var e=this;l.extend(e,{cubeEffect:{setTranslate:he.setTranslate.bind(e),setTransition:he.setTransition.bind(e)}})},on:{beforeInit:function(){var e=this;if("cube"===e.params.effect){e.classNames.push(e.params.containerModifierClass+"cube"),e.classNames.push(e.params.containerModifierClass+"3d");var t={slidesPerView:1,slidesPerColumn:1,slidesPerGroup:1,watchSlidesProgress:!0,resistanceRatio:0,spaceBetween:0,centeredSlides:!1,virtualTranslate:!0};l.extend(e.params,t),l.extend(e.originalParams,t)}},setTranslate:function(){var e=this;"cube"===e.params.effect&&e.cubeEffect.setTranslate()},setTransition:function(e){var t=this;"cube"===t.params.effect&&t.cubeEffect.setTransition(e)}}},fe={setTranslate:function(){for(var t=this,a=t.slides,i=0;i<a.length;i+=1){var s=a.eq(i),r=s[0].progress;t.params.flipEffect.limitRotation&&(r=Math.max(Math.min(s[0].progress,1),-1));var n=-180*r,o=0,l=-s[0].swiperSlideOffset,d=0;if(t.isHorizontal()?t.rtl&&(n=-n):(d=l,l=0,o=-n,n=0),s[0].style.zIndex=-Math.abs(Math.round(r))+a.length,t.params.flipEffect.slideShadows){var p=t.isHorizontal()?s.find(".swiper-slide-shadow-left"):s.find(".swiper-slide-shadow-top"),c=t.isHorizontal()?s.find(".swiper-slide-shadow-right"):s.find(".swiper-slide-shadow-bottom");0===p.length&&(p=e('<div class="swiper-slide-shadow-'+(t.isHorizontal()?"left":"top")+'"></div>'),s.append(p)),0===c.length&&(c=e('<div class="swiper-slide-shadow-'+(t.isHorizontal()?"right":"bottom")+'"></div>'),s.append(c)),p.length&&(p[0].style.opacity=Math.max(-r,0)),c.length&&(c[0].style.opacity=Math.max(r,0))}s.transform("translate3d("+l+"px, "+d+"px, 0px) rotateX("+o+"deg) rotateY("+n+"deg)")}},setTransition:function(e){var t=this,a=t.slides,i=t.activeIndex,s=t.$wrapperEl;if(a.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e),t.params.virtualTranslate&&0!==e){var r=!1;a.eq(i).transitionEnd(function(){if(!r&&t&&!t.destroyed){r=!0,t.animating=!1;for(var e=["webkitTransitionEnd","transitionend"],a=0;a<e.length;a+=1)s.trigger(e[a])}})}}},me={name:"effect-flip",params:{flipEffect:{slideShadows:!0,limitRotation:!0}},create:function(){var e=this;l.extend(e,{flipEffect:{setTranslate:fe.setTranslate.bind(e),setTransition:fe.setTransition.bind(e)}})},on:{beforeInit:function(){var e=this;if("flip"===e.params.effect){e.classNames.push(e.params.containerModifierClass+"flip"),e.classNames.push(e.params.containerModifierClass+"3d");var t={slidesPerView:1,slidesPerColumn:1,slidesPerGroup:1,watchSlidesProgress:!0,spaceBetween:0,virtualTranslate:!0};l.extend(e.params,t),l.extend(e.originalParams,t)}},setTranslate:function(){var e=this;"flip"===e.params.effect&&e.flipEffect.setTranslate()},setTransition:function(e){var t=this;"flip"===t.params.effect&&t.flipEffect.setTransition(e)}}},ge={setTranslate:function(){for(var t=this,a=t.width,i=t.height,s=t.slides,r=t.$wrapperEl,n=t.slidesSizesGrid,o=t.params.coverflowEffect,l=t.isHorizontal(),d=t.translate,p=l?a/2-d:i/2-d,c=l?o.rotate:-o.rotate,u=o.depth,h=0,v=s.length;h<v;h+=1){var f=s.eq(h),g=n[h],b=(p-f[0].swiperSlideOffset-g/2)/g*o.modifier,w=l?c*b:0,y=l?0:c*b,x=-u*Math.abs(b),T=l?0:o.stretch*b,E=l?o.stretch*b:0;Math.abs(E)<.001&&(E=0),Math.abs(T)<.001&&(T=0),Math.abs(x)<.001&&(x=0),Math.abs(w)<.001&&(w=0),Math.abs(y)<.001&&(y=0);var S="translate3d("+E+"px,"+T+"px,"+x+"px)  rotateX("+y+"deg) rotateY("+w+"deg)";if(f.transform(S),f[0].style.zIndex=1-Math.abs(Math.round(b)),o.slideShadows){var C=l?f.find(".swiper-slide-shadow-left"):f.find(".swiper-slide-shadow-top"),M=l?f.find(".swiper-slide-shadow-right"):f.find(".swiper-slide-shadow-bottom");0===C.length&&(C=e('<div class="swiper-slide-shadow-'+(l?"left":"top")+'"></div>'),f.append(C)),0===M.length&&(M=e('<div class="swiper-slide-shadow-'+(l?"right":"bottom")+'"></div>'),f.append(M)),C.length&&(C[0].style.opacity=b>0?b:0),M.length&&(M[0].style.opacity=-b>0?-b:0)}}m.ie&&(r[0].style.perspectiveOrigin=p+"px 50%")},setTransition:function(e){this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e)}},be={name:"effect-coverflow",params:{coverflowEffect:{rotate:50,stretch:0,depth:100,modifier:1,slideShadows:!0}},create:function(){var e=this;l.extend(e,{coverflowEffect:{setTranslate:ge.setTranslate.bind(e),setTransition:ge.setTransition.bind(e)}})},on:{beforeInit:function(){var e=this;"coverflow"===e.params.effect&&(e.classNames.push(e.params.containerModifierClass+"coverflow"),e.classNames.push(e.params.containerModifierClass+"3d"),e.params.watchSlidesProgress=!0,e.originalParams.watchSlidesProgress=!0)},setTranslate:function(){var e=this;"coverflow"===e.params.effect&&e.coverflowEffect.setTranslate()},setTransition:function(e){var t=this;"coverflow"===t.params.effect&&t.coverflowEffect.setTransition(e)}}};return $.use([I,L,D,O,H,X,G,V,W,j,K,_,Q,ee,ae,se,ne,le,pe,ue,ve,me,be]),$});
//# sourceMappingURL=swiper.min.js.map
"use strict";

var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5a97f2c64b401e45400d5118/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
})();
"use strict";

//---------StartNow Swiper---------
function MaxSwiper(container, params) {

    if( $(container).length ) {

        this.params = params;

        if(this.params.wayBlock == undefined) {
            this.params.wayBlock = ".slide-block";
        }
        if(this.params.wayIcons == undefined) {
            this.params.wayIcons = ".icons-block .icon";
        }
        
        this.slideBlock = $(container+">"+this.params.wayBlock);
        this.slides =  this.slideBlock.children(".slide");
        this.itemIcons = $(container+">"+this.params.wayIcons).children("div");
        
        let _self = this;
        

        //--set num
        for(let i = 0; i < this.slides.length; i++) {
            this.itemIcons.eq(i).data("numslide", i);
            this.slides.eq(i).data("numslide", i);
        }
        //--/set num

        //--start after load page
        this.slides.css({
            "opacity": "0",
            "display": "none"
        });
        this.slides.eq(0).addClass("active");
        this.itemIcons.eq(0).addClass("active");
        this.fadeInCustom( this.slides.eq(0) );
        //--/start after load page

        //--switch by icon click
        this.itemIcons.on("click", function(event){
                let numSlide = $(this).data("numslide");
                _self.switchSlide(numSlide);
        })
        //--/switch by icon click

        //--Swipe & Pan switch

        // create a simple instance
        // by default, it only adds horizontal recognizers
        var hammer = new Hammer(this.slideBlock[0] );

        this.doSwipe = false;
        this.allowSwitch = true;

        let     isDragging = false,
                isLeftPan = false,
                isRightPan = false;

        let beginPosMouseX = 0,
            deviation = 0;

        let numShowedSlide = undefined;

        let distAbsSwitch = this.slideBlock.width() / 4 * 3;
        $(window).resize(function(){
            distAbsSwitch = _self.slideBlock.width()/4 * 3;
        })

        hammer.on('swipe', function(ev){
           if ( _self.allowSwitch ){
                _self.doSwipe = true;
            }
        });
        hammer.on("pan", function(ev){
            if ( _self.allowSwitch ){

            if ( ! isDragging ) {
                isDragging = true;
                beginPosMouseX = ev.deltaX;
            }
            deviation = beginPosMouseX - ev.deltaX;
            //setDinamic(deviation); //test

            if(0 < deviation && deviation <= distAbsSwitch){

                _self.slides.filter(".active").css("opacity", String( 1 - Math.abs(deviation)/distAbsSwitch ) );
                
                if( ! isLeftPan ){
                    if(numShowedSlide != undefined){
                        _self.slides.eq(numShowedSlide).css({
                            "opacity": "0",
                            "display": "none" 
                        });
                    }
                    numShowedSlide = _self.numOfNext();
                    isLeftPan = true;
                    isRightPan = false;
                    _self.slides.eq(numShowedSlide).css("display", "flex");
                }

                _self.slides.eq(numShowedSlide).css( "opacity", String( Math.abs(deviation)/distAbsSwitch ) );
            }
            
            if (0 > deviation && deviation >= -distAbsSwitch) {

                _self.slides.filter(".active").css("opacity", String( 1 - Math.abs(deviation)/distAbsSwitch ) );
                
                if( ! isRightPan ){
                    if(numShowedSlide != undefined){
                        _self.slides.eq(numShowedSlide).css({ 
                            "opacity": "0",
                            "display": "none" 
                        });
                    }
                    numShowedSlide = _self.numOfPrev();
                    isLeftPan = false;
                    isRightPan = true;
                    _self.slides.eq(numShowedSlide).css("display", "flex");
                }

                _self.slides.eq(numShowedSlide).css( "opacity", String( Math.abs(deviation)/distAbsSwitch ) );
            }

            if (ev.isFinal && numShowedSlide !=undefined) {
                isDragging = false;
                isLeftPan = false;
                isRightPan = false;
                if( Math.abs(deviation) > distAbsSwitch/2 || _self.doSwipe) {
                    _self.switchSlide(numShowedSlide);
                }else{
                    _self.fadeOutCustom( _self.slides.eq(numShowedSlide) );
                    _self.slides.filter(".active").animate({opacity: "1"}, 800);
                }
                numShowedSlide = undefined;
            }

            }
        });

        //--/Swipe & Pan switch
    }


}

//--fade function
MaxSwiper.prototype.fadeInCustom = function(elem) {
    let animSpeed = Math.floor( ( 1-elem.css("opacity") )*200 );
    elem.css("display", "flex");
    elem.animate({
            opacity: 1,
        },animSpeed, "linear")
}
MaxSwiper.prototype.fadeOutCustom = function(elem) {
    let _self = this;
    let animSpeed = Math.floor( elem.css("opacity") * 200 );
    elem.animate({
        opacity: 0,
    },
    animSpeed, "linear",
    function(){
        elem.css("display", "none");
        _self.doSwipe = false;
        _self.allowSwitch = true;
        if(elem.hasClass("active")){
            elem.css("display", "flex");
        }
    })
}
//--/fade function


//--numOfNext&prev
MaxSwiper.prototype.numOfNext = function(){
    let num = this.slides.filter(".active").data("numslide")+1;
    if( num >= this.slides.length ){
        num = 0;
    }
    return num;
}
MaxSwiper.prototype.numOfPrev = function(){
    let num = this.slides.filter(".active").data("numslide")-1;
    if( num <= -1 ){
        num = this.slides.length-1;
    }
    return num;
}
//--/numOfNext&prev


//--switch function
MaxSwiper.prototype.switchSlide = function(num) {
    let slideElem = this.slides.eq(num);
    if( ! slideElem.hasClass("active") ){
        this.allowSwitch = false;
        this.fadeOutCustom ( this.slides.filter(".active") );
        this.slides.filter(".active").removeClass("active");
        this.itemIcons.filter(".active").removeClass("active");
        slideElem.addClass("active");
        this.itemIcons.eq(num).addClass("active");
        this.fadeInCustom( slideElem );
    }
}
//--/switch function

//---------/StartNow Swiper---------
"use strict";

$(document).ready(function(){

    //---------Main Page Diagram---------

    if ( $(".rates-diagram").length ) {

		// var dataInput = [
		// 	{
		// 		title: "Июль 2015",
		// 		value: 400
		// 	},{
		// 		title:  "Сент 2015",
		// 		value: 530
		// 	},{
		// 		title: "Нояб 2015",
		// 		value: 600
		// 	},{
		// 		title: "Янв 2016",
		// 		value: 900
		// 	},{
		// 		title: "Май 2016",
		// 		value: 900
		// 	},{
		// 		title: "Июль 2016",
		// 		value: 1500
		// 	},{
		// 		title:  "Сент 2016",
		// 		value: 1000
		// 	},{
		// 		title: "Нояб 2016",
		// 		value: 2000
		// 	},{
		// 		title: "Янв 2017",
		// 		value: 2550
		// 	},{
		// 		title: "Май 2017",
		// 		value: 2500
		// 	},{
		// 		title: "Июль 2017",
		// 		value: 3000
		// 	}
		// ];
        
        // var scaleStepSize = 250;
        
		// var scaleLabels = [];
        // var scaleData = [];
		// for (let elem of dataInput) {
        //     scaleLabels.push(elem.title);
		// }
		// for (let elem of dataInput) {
		// 	scaleData.push(elem.value);
        // }
        
        

        //---Диаграмма

        // let draw = Chart.controllers.line.prototype.draw;
        // Chart.controllers.line = Chart.controllers.line.extend({
        //     draw: function() {
        //         draw.apply(this, arguments);
        //         let ctx = this.chart.chart.ctx;
        //         let _stroke = ctx.stroke;
        //         ctx.stroke = function() {
        //             ctx.save();
        //             ctx.shadowColor = '#E56590';
        //             ctx.shadowBlur = 10;
        //             ctx.shadowOffsetX = 0;
        //             ctx.shadowOffsetY = 8;
        //             _stroke.apply(this, arguments)
        //             ctx.restore();
        //         }
        //     }
        // });

        // var ctx = $(".dollar-rate");
        // Chart.defaults.global.defaultFontColor = "#000";
        // Chart.defaults.global.defaultFontFamily = "Roboto";
        // Chart.defaults.global.defaultFontSize = 14;

        // var myChart = new Chart(ctx, {
        //     type: 'line',
        //     data: {
        //         labels:  scaleLabels,
        //         datasets: [{
        //             label: 'Course',
        //             data: scaleData,
        //             backgroundColor: 'transparent',
        //             borderColor: '#007FFF',
        //             borderWidth: 3,
        //             lineTension: 0.1,
        //             pointRadius: 0.5
        //         }]
        //     },
        //     options: {
        //         legend: {
        //             labels: {
        //                     fontSize: 17
        //                 }
        //         },
        //         elements: {
        //             line: {
        //                 tension: 0.3, // disables bezier curves
        //             }
        //         },
        //         scales: {
        //             xAxes: [{
        //                 gridLines: {
        //                     display: false,
        //                     color: "black"
        //                 }
        //             }],
        //             yAxes: [{
        //                 ticks: {
        //                     stepSize: scaleStepSize
        //                 }
        //             }]
        //         }
        //     }
        // });

        //--draw scaleY
        // let minData = scaleData[0];
        // let maxData = scaleData[0];
        // for (let i=0; i<scaleData.length; i++ ) {
        //     if(scaleData[i] > maxData){
        //         maxData = scaleData[i];
        //     }
        //     if(scaleData[i] < minData){
        //         minData = scaleData[i];
        //     } 
        // }

        //forced parameters
        // var scaleStepSize = 1400;
        // var minData = 1400;
        // var maxData = 14000;
        // //forced parameters

        // var numOfMin;
        // var numOfMax;
        // for (var i = 0; true; i++ ) {
        //     if (minData-scaleStepSize < i*scaleStepSize){
        //         numOfMin = i;
        //         break;
        //     }
        // }
        // for (var i = numOfMin; true; i++ ) {
        //     if (maxData <= i*scaleStepSize){
        //         numOfMax = i;
        //         break;
        //     }
        // }
        
        // var arrYScale = [];
        // for (var i = numOfMin; i <= numOfMax; i++) {
        //     arrYScale.push(i*scaleStepSize);
        // }
        // $(".rates-diagram .yScale").children("ul").append('<li class="units">USD</li>');
        // for(var i = arrYScale.length-1; i>=0; i--){
        //     $(".rates-diagram .yScale").children("ul").append("<li>"+arrYScale[i]+"</li>");
        // }
        
        //--/draw scaleY
    }

    //---------/Main Page Diagram---------


    //---------SoluteHome Swiper---------

    if ($(".solute-home__swiper").length) {

    var soluteHomeSwiper = new Swiper ('.solute-home__swiper', {
        // Optional parameters
        direction: 'horizontal',
        effect: 'fade',
        // If we need pagination
        pagination: {
          el: '.swiper-pagination',
          clickable: true
        },
    
        // Navigation arrows
        navigation: {
          nextEl: '.swiper-btn-next',
          prevEl: '.swiper-btn-prev',
        }
    })

    }

    //---------/SoluteHome Swiper---------


    //---------StartNow Swiper be here
    var StartNowSwiper = new MaxSwiper(".start-now", {
        wayBlock: ".container.gen-block__container>.slide-block",
        wayIcons: ".icons-block>.container"
    });

    //StartNowSwiper.switchSlide(3);

    //---------News Swiper---------
    
    if ( $(".news__swiper").length ){

        var newsSwiper = new Swiper ('.news__swiper', {
            // Optional parameters
            direction: 'horizontal',
            loop: true,
            effect: 'fade',
            // Navigation arrows
            navigation: {
            nextEl: '.arrow>.swiper-btn-next',
            prevEl: '.arrow>.swiper-btn-prev'
            }
        })

    }

    //---------Phone Swiper---------

    if( $(".phone-swiper").length ) {

    var phoneSwiper = new Swiper ('.phone-swiper', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,
        slidesPerView: 3,
        centeredSlides: true,
        spaceBetween: 120,
    
        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
    
        // Navigation arrows
        navigation: {
            nextEl: '.swiper-btn-next>div',
            prevEl: '.swiper-btn-prev>div',
        },
        breakpoints: {
            991:{
                spaceBetween: 0,
                slidesPerView: 2.5
            },
            767:{
                spaceBetween: 0,
                slidesPerView: 2.2
            },
            639: {
                spaceBetween: 0,
                slidesPerView: 2
            },
            479: {
                spaceBetween: 0,
                slidesPerView: 1.3
            }
            
        }
    })

    }


    //---------/Phone Swiper---------


    //---------Circle Swiper---------
    
    ;(function(){
        //--self-evocative function
    if( $(".circle-swiper").length ) {

    var circleIcons = $(".circle-swiper .big-circle").children(".icon");
    var circleItems = $(".circle-swiper>.sw-items").find("li");
    var circleSlides = $(".circle-swiper .big-circle").children(".slide");
    
    var startPeriod = 5;//Set-Up in sec
    var clickPeriod = 15;//Set-Up in sec
    var isSwiperStarted = false;
    
    //Set h3 for Slides & distribution data
    for(var i = 0; i < circleItems.length; i++) {
        circleSlides.eq(i).prepend("<h3>"+circleItems.eq(i).text()+"</h3>");
        circleSlides.eq(i).prepend('<div class="s-mob-pic">'+circleIcons.eq(i).find("div").html()+'</div>');
    
        circleIcons.eq(i).find("div").html()
        circleIcons.eq(i).data("numslide", i+1);
        circleItems.eq(i).data("numslide", i+1);
        circleSlides.eq(i).data("numslide", i+1);
    }
    
    
    // set active slide with other elements. give num of slide (value 1...5)
    function setActive(elemNum) {
        circleIcons.eq(elemNum-1).addClass("active");
        circleItems.eq(elemNum-1).addClass("active");
        circleSlides.eq(elemNum-1).addClass("active");
    }
    // do all slides not-active with other elements
    function removeActive() {
        circleIcons.filter(".active").removeClass("active");
        circleItems.filter(".active").removeClass("active");
        circleSlides.filter(".active").removeClass("active");
    }
    
    var intervalCircle;
    function startCircleSwiper(elemNum, myTime){
        clearInterval(intervalCircle);
        removeActive();
        setActive(elemNum);
        isSwiperStarted = true;
    
        var circle_growth = 5/myTime;
    
        var secLittleCircle = 0;
        intervalCircle = setInterval(function(){
            secLittleCircle = secLittleCircle+circle_growth;
            circleIcons.filter(".active").css("animation-delay","-"+secLittleCircle+"s");
            if(secLittleCircle > 100-circle_growth ){
                var i = circleIcons.filter(".active").data("numslide");
        
                if(i == circleIcons.length){
                    startCircleSwiper(1, startPeriod);
                }else{
                    startCircleSwiper(i+1, startPeriod);
                }
            }
        },50);
    }

    function stopCircleSwiper(){
        clearInterval(intervalCircle);
        removeActive();
        isSwiperStarted = false;
    }
    
    // start Swipe
    if($(window).width() > 767-17){
        startCircleSwiper(1, startPeriod);
    }
    var wind;
    $(window).resize(function(){
        wind = $(window).width();
        if(wind > 767-17) {
            if(!isSwiperStarted) {
                startCircleSwiper(1, startPeriod);
                console.log("start circle swiper");
            }	
        }else{
            if(isSwiperStarted) {
                stopCircleSwiper();
                console.log("stop circle swiper");
            }
        }
    })
    
    function onClickOfElem(elem) {
        startCircleSwiper(elem.data("numslide"), clickPeriod);
    }
    
    circleItems.on("click", function(event){
        event.preventDefault();
        onClickOfElem($(this));
    })
    circleIcons.on("click", function(event){
        onClickOfElem($(this));
    })

    }
    //--/self-evocative function close
    })();

    //---------/Circle Swiper---------


    //---------Perspective Subscribe---------

    ;(function(){   //--self-evocative function

        var perspForm = $(".persp-block .easy-form");
        var perspBtn = $(".persp-block .easy-form").children(".transp-btn");
        
        var beginingText = "";
        perspBtn.on("click", function(event){
            
            if( ! $(this).parent().hasClass("active") ){

                event.preventDefault();
                var otherBtn = perspForm.filter(".active").children("button");
                

                var _self = $(this);
                _self.prev().css("display", "block");
                _self.next().css("display", "block");
                setTimeout(function(){
                    _self.parent().addClass("active");
                    beginingText = _self.text();
                    _self.text("Подписаться");
                    _self.removeClass().addClass("easy-btn");
                },5)



                otherBtn.text(beginingText);
                otherBtn.removeClass().addClass("transp-btn");
                otherBtn.parent().removeClass("active");
                
                setTimeout(function(){
                    otherBtn.prev().css("display", "none");
                    otherBtn.next().css("display", "none");
                },100)

            }

        })
    
    })();  //--/self-evocative function close

    //---------/Perspective Subscribe---------

    //--popup

    var popUp = $(".gen-popup");
    
    var timerClose;

    function AddAppPopUp(button) {
        var buttonApp = $(button);
    
        buttonApp.on("click", function(event){
            popUp.css("display", "flex");
            setTimeout(function(){
                popUp.addClass("active");
            },5)
        })
    }

    function ClosePopUp() {
        clearTimeout(timerClose);
        popUp.removeClass("active");
        setTimeout(function(){
            popUp.css("display", "none");
        }, 400);
    }

    AddAppPopUp(".phone-app .mobileApp:nth-child(1) button");
    AddAppPopUp(".solute-start-mine .mobileApp:nth-child(1) .easy-btn");
    AddAppPopUp(".slider-miner-app .mobileApp:nth-child(1) .easy-btn");
    AddAppPopUp(".download-app-eco .mobileApp:nth-child(1) .easy-btn");
        
    // AddSubscribePopUp(".gen-popup .easy-form button");
    
    //--close popUp
    var closeBtn = popUp.find(".close-btn");
    closeBtn.on("click", function(event){
        ClosePopUp()
    })
    popUp.click(function(event) {
        if ($(event.target).closest(".container").length || $(event.target).closest("input").length){
            return;
        }
        ClosePopUp();
        event.stopPropagation();
    });
    //--/close popUp

    //--/popup


    //-----Browser Support------
    function get_name_browser(){
        // получаем данные userAgent
        var ua = navigator.userAgent;
        // с помощью регулярок проверяем наличие текста,
        // соответствующие тому или иному браузеру
        if(ua.search(/Edge/) >0) return 'Edge';
        if (ua.search(/Trident/) > 0) return 'Internet Explorer';
        if (ua.search(/Firefox/) > 0) return 'Firefox';
        if (ua.search(/Opera/) > 0) return 'Opera';
        if (ua.search(/Chrome/) > 0) return 'Google Chrome';
        if (ua.search(/Safari/) > 0) return 'Safari';
        
        // условий может быть и больше.
        // сейчас сделаны проверки только 
        // для популярных браузеров
        return 'Не определен';
    }
     
    var browser = get_name_browser();
    if (browser == 'Не определен' || browser == 'Internet Explorer' ||  browser == 'Edge') {
        
        $(".slider-miner-app .phone-swiper").prepend('<div class="gradient-bg"></div>');

        //----Resize to work of news__swiper on IE11 
        if ( $(".news__swiper").length ){
            $(".news .news__swiper").css("flex-basis", "450px");

            var resizeEvent = window.document.createEvent('UIEvents'); 
            resizeEvent .initUIEvent('resize', true, false, window, 0); 
            window.dispatchEvent(resizeEvent);
        }
        //----/Resize to work of news__swiper on IE11 
        
    }
    //-----/Browser Support------

})
"use strict";

function loadManager(){
    var count=0,
          load=0,
          me=this; 
              
    //add loading
    this.load=function(url, callback){
         count++;
         $.ajax({
             url:url,
             success:function(data){
                 load++;

                 //count %
                 me.procent=(100/count)*load;

                 callback(data);
             }
        })
    };
}

$(document).ready(function(){
    var ldr = new loadManager();
    ldr.load(location.href, function(){

        //--remove preloader (when site is loaded)
        setTimeout(function(){ 
            $(".preloader").animate({opacity: "0"},500, function(){
                $(this).css("display", "none");
                $("body").css("overflow-y", "auto"); 
            })
        }, 1500)
        //--/remove preloader

        //--email validation header
        $('#nf-field-41').keyup(function() {
            if($(this).val() != '') {
                var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
                if(pattern.test($(this).val())){
                    $("#nf-field-42").removeClass('transp-btn-not-active');
                    $("#nf-field-42").addClass('easy-btn');
                }
                else {
                    $("#nf-field-42").removeClass('easy-btn');
                    $("#nf-field-42").addClass('transp-btn-not-active');
                }
            }
            else {
                $("#nf-field-42").removeClass('easy-btn');
                $("#nf-field-42").addClass('transp-btn-not-active');
            }
        });
        // --/ email validation header


        //--list to StartNowSwiper (when was redirect from other page by "Soon-item menu")
        var mapLocationArr = window.location.pathname.split("/");
        if (mapLocationArr[1].length == 2 && mapLocationArr[2] == "" || mapLocationArr[1] == "") {
            if(sessionStorage.getItem("switchSlide")){
                // StartNowSwiper.switchSlide( Number(sessionStorage.getItem("switchSlide")) );
                $(".start-now .icons-block__container .icon:nth-child(" + (Number(sessionStorage.getItem("switchSlide")) + 1) + ")").trigger('click');
                var id  = ".start-now",
                top = $(id).offset().top;
                $('body,html').animate({scrollTop: top}, 1500);
                sessionStorage.removeItem('switchSlide');
            }
        }
        //--/list to StartNowSwiper

    })
})
"use strict";
$(document).ready(function() {
    // menu
    $("#menu-item-17").addClass(' menu-item to-start');
    $("#menu-item-76").addClass(' menu-item to-know');
    $("#menu-item-45").addClass(' menu-item');
    $("<i class='fa fa-chevron-left hed-right' aria-hidden='true'></i> <i class='fa fa-chevron-right hed-left' aria-hidden='true'></i>").prependTo(".header-swiper");

    $("<div class='sec-lang-bl'></div>").appendTo('#qtranslate-2');

    //adaptation on load
    if ($(window).width() <= 744) { 
        $('#qtranslate-2').appendTo( $('#menu-topmenu') );
    }
    else if ($(window).width() >= 744) { 
        $('#qtranslate-2').appendTo( $('.header-sidebar') );
        $('.btn-background').appendTo( $('.header-sidebar') );
    }
    //adaptation on resize
    $(window).on('resize', function(event){
        $('.backghound-shadow').css('height', $('body').innerHeight() + 'px');
        var win = $(this);
        $(".content-wrapper").css('padding-top', ($('.header').innerHeight()) - 2 + 'px');
        if (win.width() <= 744) { 
            $('#qtranslate-2').appendTo( $('#menu-topmenu') );
        }
        else if (win.width() >= 744) { 
            $('#qtranslate-2').appendTo( $('.header-sidebar') );
            $('.btn-background').appendTo( $('.header-sidebar') );
        }
    });
    //i want to start drop-down on click
    $( "#menu-link-17" ).click(function(event) {
        if (event.target.id == 'menu-link-17'){
            $('#menu-topmenu .to-start .header-swiper').toggleClass('open-drop-down');
            $('#menu-link-17').toggleClass('link-active');
            //background-shadow
            $('.backghound-shadow').toggleClass('visible');
            $('.backghound-shadow').css('height', $('body').innerHeight() + 'px');
    }});
    $(document).click(function(event) {
        if ($(event.target).closest("#menu-topmenu .to-start").length) return;
        if ($('#menu-topmenu .to-start .header-swiper').hasClass('open-drop-down')) {
            $('#menu-topmenu .to-start .header-swiper').toggleClass('open-drop-down');
            $('#menu-link-17').toggleClass('link-active');
            $('.backghound-shadow').toggleClass('visible');
        }
    });

    //i want to know drop-down on click
    $( "#menu-link-76" ).click(function(event) {
        if (event.target.id == 'menu-link-76'){
            $('#menu-topmenu .to-know .header-swiper').toggleClass('open-drop-down');
            $('#menu-link-76').toggleClass('link-active');
            //background-shadow
            $('.backghound-shadow').toggleClass('visible');
            $('.backghound-shadow').css('height', $('body').innerHeight() + 'px');
    }});
    $(document).click(function(event) {
        if ($(event.target).closest("#menu-topmenu .to-know").length) return;
        if ( $('#menu-topmenu .to-know .header-swiper').hasClass('open-drop-down')) {
            $('#menu-topmenu .to-know .header-swiper').toggleClass('open-drop-down');
            $('#menu-link-76').toggleClass('link-active');
            $('.backghound-shadow').toggleClass('visible');
        }
    });
    //background-shadow
    $('.backghound-shadow').css('height', $('body').innerHeight() + 'px');
    


    // cut to three first letters of the languages
    var option = $("#qtranxs_select_qtranslate-2-chooser option");
    option.each(function(){
        $(this).html($(this).text().substring(0, 3));
    })
    
    //language drop-down row on click
    // option.css("display", "block");
    $("<div class='menu-dot'></div> <div class='select-wrap'><div class='lang-opener' id='lg-op'></div></div>").prependTo(".sec-lang-bl");
    $("<div class='lang-list-item'><div class='languages-menu-list'></div></div>").prependTo(".sec-lang-bl");
    $('#qtranslate-2-chooser').appendTo( $('.languages-menu-list') );
    $('#qtranxs_select_qtranslate-2-chooser').appendTo( $('.select-wrap') );
    
    $("<p>&nbsp</p>").appendTo('.lang-ru a');
    $("<p>&nbsp</p>").appendTo('.lang-en a');
    $('.lang-zh a').addClass('soon-lang');

    $("<div class='ring'><img src='/wp-content/uploads/2018/02/russia.svg'></div>").prependTo('.lang-ru a');
    $("<div class='ring'><img src='/wp-content/uploads/2018/02/united-kingdom.svg'></div>").prependTo('.lang-en a');
    $("<div class='ring'><img src='/wp-content/uploads/2018/02/germany.svg'></div>").prependTo('.lang-de a');
    $("<div class='ring'><img src='/wp-content/uploads/2018/02/china.svg'></div>").prependTo('.lang-zh a');
    $("<img src='/wp-content/uploads/2018/02/world-wide-web.svg' class='www' id='www'>").prependTo('#qtranslate-2');

    $(".lang-zh").remove();
    
    // $(".select-wrap").appendTo('.sec-lang-bl');

    
    
    $( "#qtranslate-2" ).click(function(event) {
        if (event.target.id == 'qtranslate-2' || event.target.id == 'lg-op' || event.target.id == 'www'){
        $( "#qtranslate-2" ).toggleClass('lang-open');
        //background-shadow
        $('.backghound-shadow').toggleClass('visible');
        $('.backghound-shadow').css('height', $('body').innerHeight() + 'px');
    }});
    
    $(document).click(function(event) {
        if ($(event.target).closest("#qtranslate-2").length) return;
        if ($( "#qtranslate-2" ).hasClass('lang-open')) {
            $( "#qtranslate-2" ).toggleClass('lang-open');
            $('.backghound-shadow').toggleClass('visible');
      }
    });
    
    //registration menu drop-down
    var btnWidth =  $('#header-btn').css('width'),
    reg = $('.header-sidebar .reg-menu');
    $( "#header-btn" ).click(function(event) {
        if (btnWidth=='120px') {
            reg.toggleClass('open-reg-menu');
            $('#header-btn').css('width', '30px');
            $('#header-btn span').fadeOut(0);
            $('#header-btn .close').fadeIn(500);
            btnWidth =  '30px';
        }
        else if (btnWidth=='30px') {
            reg.toggleClass('open-reg-menu');
            $('#header-btn').css('width', '120px');
            $('#header-btn .close').fadeOut(0);
            $('#header-btn span').fadeIn(500);
            btnWidth =  '120px';
        }
    });
    $(document).click(function(event){ 
        var regMenu = $(".header-sidebar .reg-menu"),
        regBtn = $("#header-btn");
        if (!regMenu.is(event.target) && regMenu.has(event.target).length === 0
        && !regBtn.is(event.target) && regBtn.has(event.target).length === 0) { 
            if ($(".header-sidebar .reg-menu").hasClass('open-reg-menu') && btnWidth=='30px') {
                $(".header-sidebar .reg-menu").removeClass('open-reg-menu');
                $('#header-btn').css('width', '120px');
                $('#header-btn .close').fadeOut(0);
                $('#header-btn span').fadeIn(500);
                btnWidth =  '120px';
            }
		}
    });
    //progressbar (i) opener
    $( ".time-info .info" ).click(function(event) {
        $( ".reg-box .progressbar" ).toggleClass('prog-bar-visible');
        $( ".reg-box .reg-separator").toggleClass('prog-bar-visible');
    });

    //header swiper to start
    // $(".menu-item .swiper-container").addClass(' header-swiper');
    $(".sub-menu li").addClass('swiper-slide sub-slide');

    var swiper = new Swiper('.header-swiper', {
        slidesPerView: 5,
        spaceBetween: 0,
        navigation: {
          nextEl: '.hed-left',
          prevEl: '.hed-right',
        },
        breakpoints: {
            825: {
                spaceBetween: 0,
                slidesPerView: 5
            },
            590: {
                spaceBetween: 0,
                slidesPerView: 4
            },
            569: {
                spaceBetween: 0,
                slidesPerView: 3
            },
            446: {
                spaceBetween: 0,
                slidesPerView: 2
            }
        }
    });




//--------------------------------------------------------------------------------

    //start now scroll & open link

    //StartNowSwiper.switchSlide(3);

    function startNow(link, item){
        $( link ).click(function(event) {
            var mapLocationArr = window.location.pathname.split("/");
            var mapPage;
             if (mapLocationArr[1].length == 2 && mapLocationArr[2] == "" || mapLocationArr[1] == "") {
                // $(link).on("click", function (event) {
                    event.preventDefault();
                    var id  = ".start-now",
                        top = $(".start-now").offset().top;
                    $('body,html').animate({scrollTop: top}, 1500);
                // });
                $(".start-now .icons-block__container .icon:nth-child(" + (item + 1) + ")").trigger('click');
                // StartNowSwiper.switchSlide(Number(sessionStorage.getItem("switchSlide")));
            }
            else{
                sessionStorage.setItem('switchSlide', item);
                window.location.href = "/";
            }
        })
    }
    startNow("#menu-link-79", 1);
    startNow("#menu-link-80", 2);
    startNow("#menu-link-81", 3);

   

//--------------------------------------------------------------------------------



    //active pages:
    //ecosystem
    if (window.location.pathname == "/ecosystem/" || window.location.pathname == "/en/ecosystem/" || window.location.pathname == "/de/ecosystem/") {
        $("#menu-link-77 .know-item").addClass('active-page');
        $("#menu-link-77 .know-item .ring img").addClass('active-img');
         }
    else{
        $("#menu-link-77 .know-item").removeClass('active-page');
        $("#menu-link-77 .know-item .ring img").removeClass('active-img');
          }
    //fa-step
    if (window.location.pathname == "/fa-step/" || window.location.pathname == "/en/fa-step/" || window.location.pathname == "/de/fa-step/") {
        $("#menu-link-78 .know-item").addClass('active-page');
        $("#menu-link-78 .know-item .ring img").addClass('active-img');
          }
    else{
        $("#menu-link-78 .know-item").removeClass('active-page');
        $("#menu-link-78 .know-item .ring img").removeClass('active-img');
    }
    //i want to know link
    if (window.location.pathname == "/fa-step/" || window.location.pathname == "/en/fa-step/" || window.location.pathname == "/de/fa-step/" || window.location.pathname == "/ecosystem/" || window.location.pathname == "/en/ecosystem/" || window.location.pathname == "/de/ecosystem/") {
        $("#menu-link-76").addClass('active-page');
    }
    else{
        $("#menu-link-76").removeClass('active-page');
    }
    //contacts
    if (window.location.pathname == "/contacts/" || window.location.pathname == "/en/contacts/" || window.location.pathname == "/de/contacts/") {
        $("#menu-link-45").addClass('active-page');
         }
    else{
        $("#menu-link-45").removeClass('active-page');
          }
    //start-miner & i want to start link
    if (window.location.pathname == "/start-miner/" || window.location.pathname == "/en/start-miner/" || window.location.pathname == "/de/start-miner/") {
        $("#menu-link-18 .start-item").addClass('active-page');
        $("#menu-link-18 .start-item .ring").css('border', '1px solid #D8AD4C');
        $("#menu-link-17").addClass('active-page');
         }
    else{
        $("#menu-link-18 .start-item").removeClass('active-page');
        $("#menu-link-18 .start-item .ring").css('border', '1px solid #dadada;');
        $("#menu-link-17").removeClass('active-page');
          }


    $(".content-wrapper").css('padding-top', ($('.header').innerHeight()) - 2 + 'px');
})

"use strict";
$(document).ready(function() {

    // ----------------TIMER------------------
        var countDownDate = new Date("May 31 2018 23:59:59").getTime();
        var x = setInterval(function() {
            var now = new Date().getTime();
            var distance = countDownDate - now;
            var days = Math.floor(distance / (1000 * 60 * 60 * 24)) + '';
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)) + '';
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))  + '';
            var seconds = Math.floor((distance % (1000 * 60)) / 1000)  + '';
            if(days.length < 2){
                days = '0' + days; 
            }
            if(hours.length < 2){
                hours = '0' + hours; 
            }
            if(minutes.length < 2){
                minutes = '0' + minutes; 
            }
            if(seconds.length < 2){
                seconds = '0' + seconds; 
            }
            if (seconds < 0){
                $(".days").html(0);
                $(".hours").html('00');
                $(".minutes").html('00');
                $(".seconds").html('00');
            }
            else{
            $(".days").html(days);
            $(".hours").html(hours);
            $(".minutes").html(minutes);
            $(".seconds").html(seconds);
            }
        }, 1000);
    
        //-------------PROGRESSBAR----------------
        var elem = $(".bar"),
        pointer = $(".pointer"),
        width = 0,
        id = setInterval(frame, 10);        
        function frame() {
        if (width >= 100) {
        clearInterval(id);
        } else {
        width=0; 
        // var marg = $(".bar").width() / $('.bar').parent().width() * 100;
        elem.css('width', width + '%');
        pointer.css('margin', 'calc(' + width + '% - 5px)');
        }
    }

    // email validation
    $('.timer .email').keyup(function() {
        if($(this).val() != '') {
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if(pattern.test($(this).val())){
                $(".timer .registration .reg-btn").removeClass('transp-btn-not-active');
                $(".timer .registration .reg-btn").addClass('easy-btn');
            }
            else {
                $(".timer .registration .reg-btn").removeClass('easy-btn');
                $(".timer .registration .reg-btn").addClass('transp-btn-not-active');
            }
        }
        else {
            $(".timer .registration .reg-btn").removeClass('easy-btn');
            $(".timer .registration .reg-btn").addClass('transp-btn-not-active');
        }
    });

    $('.timer .reg-btn').click(function() {
        if( $('.timer .reg-btn').attr('class') == "reg-btn easy-btn" ){
            $("#nf-field-41").val($(".timer .registration .email").val());
            $('#nf-field-42').trigger('click');
            $(".timer .registration .email").fadeOut(500);
            $(".timer .registration .reg-btn").fadeOut(500);
            var regMessage = setInterval(function() {
                if ($(".timer .registration p").length){
                    clearInterval(regMessage);
                }
                else{
                    $( ".timer .registration" ).html($(".header-sidebar .gen-form .nf-response-msg").html());
                }
            }, 500);
        }
    });

});
// $(document).ready(function() {
// if ($("#map").length){
  var mapLocationArr = window.location.pathname.split("/");
  var mapPage;
   if (mapLocationArr[1].length == 2) {
    mapPage = mapLocationArr[2];
   }else{
    mapPage = mapLocationArr[1];
  }
  
  
  if (mapPage === "" || mapPage ==="contacts") {

    var simplemaps_worldmap_mapdata={
        main_settings: {
         //General settings
          // width: "655", //'700' or 'responsive'
          background_color: "#FFFFFF",
          background_transparent: "yes",
          border_color: "#7c8ac2",
         
          //Zoom settings
          zoom: "yes",
          state_description: "",
          state_color: "#fff",
          state_hover_color: "#8bae54",
          state_url: "",
          all_states_inactive: "no",
          location_type: "image",
          location_image_url: "/wp-content/uploads/2018/03/maps-and-flags.svg",
          
          //Location defaults
          location_description: "Regional Office",
          location_color: "#de6667",
          location_opacity: ".9",
          location_url: "",
          location_size: "25",
          all_locations_inactive: "no",
          url_new_tab: "yes",
          initial_zoom: "-1",
          initial_zoom_solo: "no",
          auto_load: "yes",
          hide_labels: "no",
          popups: "detect",
          state_image_url: "",
          state_image_position: "",
          all_states_zoomable: "",
          label_color: "",
          label_hover_color: "",
          label_size: "",
          label_font: "",
          border_size: "1",
          border_hover_size: "1",
          div: "map"
        },
        state_specific: {
          AE: {
            name: "United Arab Emirates",
            inactive: "no",
            hide: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          AF: {
            name: "Afghanistan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          AL: {
            name: "Albania",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          AM: {
            name: "Armenia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          AO: {
            name: "Angola",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          AR: {
            name: "Argentina",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          AT: {
            name: "Austria",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          AU: {
            name: "Australia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          AZ: {
            name: "Azerbaidjan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BA: {
            name: "Bosnia-Herzegovina",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BD: {
            name: "Bangladesh",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BE: {
            name: "Belgium",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BF: {
            name: "Burkina Faso",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BG: {
            name: "Bulgaria",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BH: {
            name: "Bahrain",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BI: {
            name: "Burundi",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BJ: {
            name: "Benin",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BN: {
            name: "Brunei Darussalam",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BO: {
            name: "Bolivia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BR: {
            name: "Brazil",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BS: {
            name: "Bahamas",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BT: {
            name: "Bhutan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BW: {
            name: "Botswana",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BY: {
            name: "Belarus",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          BZ: {
            name: "Belize",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CA: {
            name: "Canada",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CD: {
            name: "Congo",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CF: {
            name: "Central African Republic",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CG: {
            name: "Congo",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CH: {
            name: "Switzerland",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CI: {
            name: "Ivory Coast",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CL: {
            name: "Chile",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CM: {
            name: "Cameroon",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CN: {
            name: "China",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CO: {
            name: "Colombia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CR: {
            name: "Costa Rica",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CU: {
            name: "Cuba",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CV: {
            name: "Cape Verde",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CY: {
            name: "Cyprus",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          CZ: {
            name: "Czech Republic",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          DE: {
            name: "Germany",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          DJ: {
            name: "Djibouti",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          DK: {
            name: "Denmark",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          DO: {
            name: "Dominican Republic",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          DZ: {
            name: "Algeria",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          EC: {
            name: "Ecuador",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          EE: {
            name: "Estonia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          EG: {
            name: "Egypt",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          EH: {
            name: "Western Sahara",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          ER: {
            name: "Eritrea",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          ES: {
            name: "Spain",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          ET: {
            name: "Ethiopia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          FI: {
            name: "Finland",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          FJ: {
            name: "Fiji",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          FK: {
            name: "Falkland Islands",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          FR: {
            name: "France",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GA: {
            name: "Gabon",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GB: {
            name: "Great Britain",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GE: {
            name: "Georgia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GF: {
            name: "French Guyana",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GH: {
            name: "Ghana",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GL: {
            name: "Greenland",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GM: {
            name: "Gambia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GN: {
            name: "Guinea",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GQ: {
            name: "Equatorial Guinea",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GR: {
            name: "Greece",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GS: {
            name: "S. Georgia & S. Sandwich Isls.",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GT: {
            name: "Guatemala",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GW: {
            name: "Guinea Bissau",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          GY: {
            name: "Guyana",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          HN: {
            name: "Honduras",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          HR: {
            name: "Croatia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          HT: {
            name: "Haiti",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          HU: {
            name: "Hungary",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          IC: {
            name: "Canary Islands",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          ID: {
            name: "Indonesia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          IE: {
            name: "Ireland",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          IL: {
            name: "Israel",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          IN: {
            name: "India",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          IQ: {
            name: "Iraq",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          IR: {
            name: "Iran",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          IS: {
            name: "Iceland",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          IT: {
            name: "Italy",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          JM: {
            name: "Jamaica",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          JO: {
            name: "Jordan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          JP: {
            name: "Japan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          KE: {
            name: "Kenya",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          KG: {
            name: "Kyrgyzstan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          KH: {
            name: "Cambodia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          KP: {
            name: "North Korea",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          KR: {
            name: "South Korea",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          KW: {
            name: "Kuwait",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          KZ: {
            name: "Kazakhstan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          LA: {
            name: "Laos",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          LK: {
            name: "Sri Lanka",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          LR: {
            name: "Liberia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          LS: {
            name: "Lesotho",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          LT: {
            name: "Lithuania",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          LU: {
            name: "Luxembourg",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          LV: {
            name: "Latvia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          LY: {
            name: "Libya",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MA: {
            name: "Morocco",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MD: {
            name: "Moldavia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          ME: {
            name: "Montenegro",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MG: {
            name: "Madagascar",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MK: {
            name: "Macedonia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          ML: {
            name: "Mali",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MM: {
            name: "Myanmar",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MN: {
            name: "Mongolia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MR: {
            name: "Mauritania",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MW: {
            name: "Malawi",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MX: {
            name: "Mexico",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MY: {
            name: "Malaysia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          MZ: {
            name: "Mozambique",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          NA: {
            name: "Namibia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          NC: {
            name: "New Caledonia (French)",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          NE: {
            name: "Niger",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          NG: {
            name: "Nigeria",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          NI: {
            name: "Nicaragua",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          NL: {
            name: "Netherlands",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          NO: {
            name: "Norway",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          NP: {
            name: "Nepal",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          NZ: {
            name: "New Zealand",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          OM: {
            name: "Oman",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PA: {
            name: "Panama",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PE: {
            name: "Peru",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PG: {
            name: "Papua New Guinea",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PH: {
            name: "Philippines",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PK: {
            name: "Pakistan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PL: {
            name: "Poland",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PR: {
            name: "Puerto Rico",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PS: {
            name: "Palestine",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PT: {
            name: "Portugal",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          PY: {
            name: "Paraguay",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          QA: {
            name: "Qatar",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          RO: {
            name: "Romania",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          RS: {
            name: "Serbia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          RU: {
            name: "Russia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          RW: {
            name: "Rwanda",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SA: {
            name: "Saudi Arabia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SB: {
            name: "Solomon Islands",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SD: {
            name: "Sudan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SE: {
            name: "Sweden",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SI: {
            name: "Slovenia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SK: {
            name: "Slovak Republic",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SL: {
            name: "Sierra Leone",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SN: {
            name: "Senegal",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SO: {
            name: "Somalia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SR: {
            name: "Suriname",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SS: {
            name: "South Sudan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SV: {
            name: "El Salvador",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SY: {
            name: "Syria",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          SZ: {
            name: "Swaziland",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TD: {
            name: "Chad",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TG: {
            name: "Togo",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TH: {
            name: "Thailand",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TJ: {
            name: "Tadjikistan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TL: {
            name: "East Timor",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TM: {
            name: "Turkmenistan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TN: {
            name: "Tunisia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TR: {
            name: "Turkey",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TT: {
            name: "Trinidad and Tobago",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TW: {
            name: "Taiwan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          TZ: {
            name: "Tanzania",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          UA: {
            name: "Ukraine",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          UG: {
            name: "Uganda",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          US: {
            name: "United States",
            inactive: "no",
            hide: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          UY: {
            name: "Uruguay",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          UZ: {
            name: "Uzbekistan",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          VE: {
            name: "Venezuela",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          VN: {
            name: "Vietnam",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          VU: {
            name: "Vanuatu",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          YE: {
            name: "Yemen",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          ZA: {
            name: "South Africa",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          ZM: {
            name: "Zambia",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          },
          ZW: {
            name: "Zimbabwe",
            hide: "no",
            inactive: "no",
            hover_color: "#8691ba",
            color: "#d4d7e2"
          }
        },
        locations: {
          "0": {
            name: "New York",
            lat: "41.700000000",
            lng: "-75.900000000",
            type: "image",
            description: "Company Headquarters",
            opacity: "1",
            size: "30"
          },
          "1": {
            name: "Москва",
            lat: "57.666666670",
            lng: "37.750000000",
            type: "image"
          },
          "2": {
            name: "Tbilisi",
            lat: "42.69411",
            lng: "44.83368",
            type: "image"
          },
          "4": {
            name: "Singapur",
            lat: "4.28967",
            lng: "103.85007",
            type: "image"
          },
          "5": {
            name: "Dortmund",
            lat: "51.51494",
            lng: "7.466",
            type: "image"
          }
        },
        regions: {
          "0": {
            name: "North and South America",
            states: [
              "MX",
              "CA",
              "US",
              "GL",
              "EC",
              "AR",
              "VE",
              "BR",
              "CO",
              "BO",
              "PE",
              "BZ",
              "CL",
              "CR",
              "CU",
              "DO",
              "SV",
              "GT",
              "GY",
              "GF",
              "HN",
              "NI",
              "PA",
              "PY",
              "PR",
              "SR",
              "UY",
              "JM",
              "HT",
              "BS"
            ],
            hover_color: "#aab1cd",
            color: "#e5e8f1"
          },
          "1": {
            name: "Europe",
            states: [
              "IT",
              "NL",
              "NO",
              "DK",
              "IE",
              "GB",
              "RO",
              "DE",
              "FR",
              "AL",
              "AM",
              "AT",
              "BY",
              "BE",
              "LU",
              "BG",
              "CZ",
              "EE",
              "GE",
              "GR",
              "HU",
              "IS",
              "LV",
              "LT",
              "MD",
              "PL",
              "PT",
              "RS",
              "SI",
              "HR",
              "BA",
              "ME",
              "MK",
              "SK",
              "ES",
              "FI",
              "SE",
              "CH",
              "TR",
              "CY",
              "UA"
            ],
            hover_color: "#aab1cd",
            color: "#e5e8f1"
          },
          "2": {
            name: "Africa and the Middle East",
            states: [
              "QA",
              "SA",
              "AE",
              "SY",
              "OM",
              "KW",
              "PK",
              "AZ",
              "AF",
              "IR",
              "IQ",
              "IL",
              "PS",
              "JO",
              "LB",
              "YE",
              "TJ",
              "TM",
              "UZ",
              "KG",
              "NE",
              "AO",
              "EG",
              "TN",
              "GA",
              "DZ",
              "LY",
              "CG",
              "GQ",
              "BJ",
              "BW",
              "BF",
              "BI",
              "CM",
              "CF",
              "TD",
              "CI",
              "CD",
              "DJ",
              "ET",
              "GM",
              "GH",
              "GN",
              "GW",
              "KE",
              "LS",
              "LR",
              "MG",
              "MW",
              "ML",
              "MA",
              "MR",
              "MZ",
              "NA",
              "NG",
              "ER",
              "RW",
              "SN",
              "SL",
              "SO",
              "ZA",
              "SD",
              "SS",
              "SZ",
              "TZ",
              "TG",
              "UG",
              "EH",
              "ZM",
              "ZW"
            ],
            hover_color: "#aab1cd",
            color: "#e5e8f1"
          },
          "3": {
            name: "South Asia and Australia",
            states: [
              "TW",
              "IN",
              "AU",
              "MY",
              "IN",
              "NP",
              "NZ",
              "TH",
              "BN",
              "JP",
              "VN",
              "LK",
              "SB",
              "FJ",
              "BD",
              "BT",
              "KH",
              "LA",
              "MM",
              "NP",
              "KP",
              "PG",
              "PH",
              "KR",
              "ID",
              "CN"
            ],
            hover_color: "#aab1cd",
            color: "#e5e8f1"
          },
          "4": {
            name: "North Asia",
            states: [
              "MN",
              "RU",
              "KZ"
            ],
            hover_color: "#aab1cd",
            color: "#e5e8f1"
          }
        },
        labels: {}
      };
    
    
    };
//Copyright 2010-2017 Simplemaps.com
//html5worldmapv3.7
//Use pursuant to license agreement at https://simplemaps.com/license

/* shifty (tweaked to avoid AMD conflict) - v1.5.2, Copyright (c) 2013 Jeremy Kahn, MIT license   http://jeremyckahn.github.io/shifty */
eval((function(x){var d="";var p=0;while(p<x.length){if(x.charAt(p)!="`")d+=x.charAt(p++);else{var l=x.charCodeAt(p+3)-28;if(l>4)d+=d.substr(d.length-x.charCodeAt(p+1)*96-x.charCodeAt(p+2)+3104-l,l);else d+="`";p+=4}}return d})("(function () {var root = this || F` 9#(\"return` 4!\")();var Tweenable = ` Y.formula` E!DEFAULT_SCHEDULE_FUNCTION` 1)EASING = \"linear\"` 1)DURATION = 500` 6!UPDATE_TIME = 16.66666666` \"\"8` @!_now = Date.now ? ` \"%: `!i)`\"=#+ new` @!;}` ]!` [\"typeof SHIFTY_DEBUG_NOW !== \"undefined\" ?` 1.:`!@!;if (` _#window` L,) {`\"q5 =` N#.requestAnimationFrame ||` 8$webkitR` ';oR` \";ms` C<mozCancel` 32&&` >'` S5setTimeout;} else`\"F:` C(`$T%noop() {` (&each(obj, fn`&i#key;for (key in obj) {if (Object.hasOwnProperty.call` ]\"key)) {fn(key);}}` ~&shallowCopy(targetObj, srcO` z!`!C!` (\",`&B'prop) {` I%[prop] =` R#` )\";});`)/$` A$`\"J'defaults`!1#`!0!`!''` |/`&U'` N\"`!/$`&U-` 1* src`!K$})`!8'twee`#,!s(forPosition, currentState, original` (#` v\"` &#dur`%J!, timestamp, easing`$P#normalized` x$ = `!#' <` Q& ? 0 : `!A( -` 5&) /`!(%`*4!prop` $!`!,\"`%B\"P` '*Fn`%p\"prop in`\"/)`#M#` '(`%w+`#u\"`$2!` t* =` y#`#R#`!%$`+_&` @.== \"`#t$\" ?` 0.:`.M$[` +,];`!Y(`$p%`$X%(`$8)` :\"`$>)` +$`!`$,`$&/);}}`'*#`!\"(`%]0(start, end` k%unc, p` d$`.f%` E! + (end -` )\") *` K'(` L%`! 'applyFilter(`!2!able, f` -!Name`1_$` ,!s =`2%&.prototype.` 8\"`%J!args`#+$` A!_` 7\"Args;`)1!` d#`).(n`!#\"`)/'` >#[name][`!E&]`/z/` 35.`\"9!`\",(args)`)Y\"var`'o!outHandler_endTime`\"#!` ,+`$C#` '4isEnded` '0offset;`$s&` 2)`!Q(`*1'delay`*L(`*bF`%+\", step, schedule, opt`!z(Override) {`\"P2 =`+''+`!L\" +`*p&`\"g6 = Math.min(` 3 || now()`\"P\"`!,.)` p,`#[#`!O#`!\"3>` --`$k$`$ 1 =`\"&% - (`\"Q3`-U\"` |2);`'/!`'d%isPlaying()`'H$`!r1) {step`1N#`$_$`(J&attachment`\"p-`!u\");` D&stop(true);} else {` `'`%:$Id =`%F%`!h'_` r*, UPDATE_TIME);`*i3\"before`*g!\"`\"^#`#}6`10(`&##)`!]#`2q\"1`'5H1, 1`2U%`\"D*` h\"`!06`35[`!u$`!3'`\"h4after`\"w$`%&!`!,*`$^J}}`/(&composeE`1p'(from`!(!Params`!W%`/6\"` K#d` M\" = {}`,C\"`.F!` -%`3D)`$m\"` 4)== \"string\" ||` O+`3h)`4d!ch`!E.`/h&prop) {`!M*`3h%`!=#}`$u&` ICif (!` a0` l<` (#|| DEFAULT_EASING`0F\"`4G$` U)`3;'`2k%`-+!initi`&,%`-=!onfig) {this.`&j$` <! =` =!` H( || {};` E#` V!ur`-6!false` 2#`*#$F`!=$=`!r%SCHEDULE_FUNCTION`$c'`!E'`38/` v!setC` <!`/8\"`!v\";}}`\"=%.prototype.`'C! =`#'`\"C)`!3!`!j!is` Z!`&p\"`#I#this;}if` I(`!P!`!K% || !`\"Y,`!I;` I\"`)k&=`1$\"`#/$tart`!N\"get`1<!` 3!`)<&);`!T'.resume();};`\"L0`!;%`\"W)`\"W%`\"-#= ` \"#`$a5tru`$q$`!@&` O%.` *&` >#pausedA`3h$null`%I+`/k!` -'`4[\"` i%` )\"|| 0`\"m(` 6&` )\"`4B!op` :%ep` 6(ep` 2+finish` 9&` )#` 7*`3j%`!B&` *$`)4'DURA`'6!`(>1shallowCopy({},`!'%rom`,7!`$v%`%/$`/]) =` 7'` :#`/q'` f6to` n,var self` l#` e$`/:)`%N)) {` 0*(self,` `!`'+'` )$`1)#` &#`1J%` *\"`0e*` .\"`2$+` p#`2-(` -\"`-1\"` &$ste`!*%`+I,);`0I\"`#t+`$&.`0o\"`#;)`#K-;defaults(`!G)` [(`$*$`!\\\"`%a!`2@.`\"M*`$8#` K#`/4-` j$f`4C!Args = [`4\"+`%U.,`\"!(` :$` ~\"];applyF` l!`+I!, \"`-G!Created\"`+9)`+&3get`%L,` N#`&P,`#[.`+v7` g)s` H!`1#3` 8!` R3`+]!`'*-`+h1`.4'isP` 8!`,k$`\"C>`.;\"` y,`0M(` l\"`!5%`/P&+`!5# -`\"z#`!M(`0!$`!G'`33(`1F&`.P*`)N+`$C@seek`!s)millisecond) {` #' = Math.max` 9(, 0`+D\"`$8#`#K)`\"`&`\"N' ` h)== 0`3F,`2T.` q(-` U(` ~!`3`\"isPlaying()`#g%`\"r4`#G.`#-*`(0\"`!9+` )$`,q#`0$*`'T0`)B4`+I,`)R*` &$`,r\"`2k*`,p$`+m%Tim`+m$`%|!();}`$s?top`%))gotoE`%-!`#%/`&M*`&]1`0D-`3/!(root.cancelAnim`#$!Frame || ` 8!webkitC` '8oC` \"8ms` @9moz` :\"Request` 03clearT`!\")`',#`#l$Id`'C\"`\"z&`-m/before`#0!\");`.)!Props(1`${Z1, 0`%Q*)`/+0after`!6$` #9End\"`0j&nish.call`'e)`!W0attachment`%}A`)D%`0.3`&6-&& `*!\"`&:%`0+6S`'+`0G*` .+`'K%` ), = ` #,` |3dispo`0_.var prop;for (prop i`\"4\"`/y(hasOw`%H!erty` B!)) {delete` I![prop];}}`!(2f`$e! = {` )3ormula = {linear:`!\\&pos`#j&pos;}};` F&` S7;`4Z(` ?%, {now:now, each:each, `'e&:`'p&` ,'` ,&, `&r':`&~', `!&':`!2', defaults:d` \"#, composeE`(,!Object:c` \".}`1;#ypeof SHIFTY_DEBUG_NOW`17!\"`#$$\") {`*f!`,Z-` #*;}` A!`\"j%`#;(`3h$` '&})();(`%i)`#k&`#N1`#y., {easeInQuad`$d4`46!pow(pos, 2);}, easeOut` ;8- (` N( - 1, 2) - 1` Z%In` M4if (` R!/= 0.5) < 1` r&0.5 *`!F/`!3$` :\"` [\"-= 2) * `!>\"`!u&InCubic`\"0B3`\"J(` /E`\"N\"3) + 1`\"F)` L2`\"\"N3`\"J&`\"L#`!0+2`!8#`\"K(Quart`\"1B4`$~+` >6`$y14`$x2` S/`\")N4`$p;`\"y,`%-+Quin`\">C5`\"W*` 1C`\"\\\"5`%*.` L2`\"/N5`$y?`!:!`\"K(Sine`$I6` \\!cos` \\!` i$PI / 2)`!|)Out` N8` S!sin` S1`\"`)`!39`\"+(cos` ^%`*'!`&+,Expo`1C7`.m!0 ? 0 :`#?&2, 10 *` J!` k!`$(` N@1 ? 1 :`#4$` m#-` p!` L!`%.,` `1if`!<\"`!_!`%+';}` .(`%E'1` 4\"`%HE`\"40`%n*`!m0--`!|#`-h)ir`-#5`*]$sqrt(1 -`#\"!`$;/Out` K8` V*`0Q0`%w*` W1`\"b:`&/)`!U6`(m/` D%`0m,`%'!`\"7(Bounc`'L.`$k$< 0.3636363636` \"\"5`!X&7.5625`1q#` |!;} else ` U(727272727` \"\"3` M/`!`$0.5454545454` \"\"`2n$+ 0.75` k0909090909` \"\"`#6'` l/818181818` \"\"`3l%+ 0.93` w&` H79`!S*6` V)84` `!`%1%Back`#M-var s = 1.70158;`)e'`#C$((s`$F!` *#- s`$I)` KG`)U\"` _#` h%` q-+ `*V/` cA`*!@`.>#`!2$(s *= 1.52`16\"`\";(`'A-`'4,` E9`\" #`*f#lasti`*P61`+s(4, -8`(>$` 1#sin`!9\"* 6`#6$(2` 6$PI)`0I!`#/$swingFromT`-l.`$!8`#-' ?`\"^O :`\"I^`!s%`&Bg` o!`\"=K-`0^!`&a?b`+W~`+W~`+W~`,&O`#T\"Past`\"\\~`#H22 - (`#2M)`#6J` m4`#I<` ~%` I<`#M>)`0@%`*3`-w@`,X%pos, 4`-u&-`*a0` @*3) -`-d$`!?#`-U4` u.` N\"`!c/` =10.2`\"K!);})();(` K&) {` $%cubicBezierAtTime(t, p1x, p1y, p2x, p2y, duration`+H#ax = 0, bx` \"\"c` )#ay` 1#y` 1#` #!;`!'%sampleCurveX(t`!r&((ax * t + bx)` ##c` $\";}` K0Y` P+y` W$y` V%` $\"` I2Derivati`!8,3 * `!B%2 *`!B)` _(olveEpsilon(`\"r'` a#1 / (200 *`#0&` N,(x, e` \\\"` O&`\"5)` B!`#&#` B'` ^(fabs(n`'4#n >= 0` c&n`(K,0 - n;}`!:+` p.`$\"t0, t1, t2, x2, d2, i;for (t2 = x, i`$z! i < 8; i++) {x2 =`$z+2) - x;if (`!l!x2) <`\"K.t2;}d` O+`$<)2)` Z&d` _!0.000001) {break;}`!V!t2 - x2 / d2;}t0`!b!t1 = 1;`!v\"` d!t2 < t`#''t0;}` 2#> t`*Q't1;}while (t0 <` 6\"`\"51`\":( - x`\"15if (x > x2) {`!^!t2`$H%`!g!t2`\"(#(t1 -`!d!*`,!!+`!d!` a'`);!3 * p1x;`)P!3`-W!2x - p1x) - cx;`)r!1` '! - bx;`)[!` R\"y;`)p!` Q#y` S!y` S!y;`*2!` S!y - by;`'&$`'C!t,`'x3`&}(getC`+c&Transition(x1, y1`&<\"y2`\"y&`,o3`,@.`-*!` X*, 1);};}Tweenable.set` O\"F` o$=` w'name` T,`'~#`!%'`!k& =`!gE;` L1.displayName = name` 23x1 = x1` '3y1 = y` &4`&{!x2` D42 = y2`$d$`#\"&prototype.formula[name] =`\"W2;};` R&un`#H>) {delete` r>;}`1b9getInterpolatedValues(from, current, targetState, po`!o\"`3H!ing, delay`&6&`!I&tweenProps(` L&` m%` \"` o)1` f#` t$);}var mock` m% = new` |&;` 1)._filterArgs = []`#O'i`\"?&`#E)`!6/`\"3.opt_`\"A$`'P\"`\"\"! =`\"E'shallowCopy({}`\"<\");var`\"0\" =` \\& || 0` 6!` {\"Objec` `*composeE` 5'`!d#` M\" || \"linear\")`\"W+set({}`!6\"`\"d)`\"t5;` =&.length`/m!` ,&[0`':\"`\"R!` ,(1] =`\"F!` )(2] =`#G(` 0(3] =`\"J)`$9'applyF` G!(`!_), \"`&*!Created\")` .Cbefore` -!\"`\"j\"`%<'`'i#`,'\"`'I[`\"%\"`(&$`!7Cafter`!U$`(Y#`!N.`)q/` \\%`&h#formatManifest`\"C!R_NUMBER_COMPONENT = /(\\d|\\-|\\.)/` ?#FORMAT_CHUNKS` >![^\\-0-9\\.]+)/g` @#UN` C\"TED_VALUE` G![0-9.\\-]+` ?%RGB`)\\#RegExp(\"rgb\\\\(\" +` Q1.source + /,\\s*/` &&`! 0.source + /,` \"H\"\\\\)\", \"g`%>#`!c!_PREFIX = /^.*\\(`\"j$HE` /!#([0-9]|[a-f]){3,6}/gi` ?!` z!_PLACEHOLDER = \"VAL\";`..(F`$0!ChunksFrom(raw`$l\", prefix`$W#accumulator`,O\"var ` D%L`)G$` )%`)[#`'I\";for (i`)j! i <` I,; i++) {` z'.push(\"_\" +`!B# + ` )\"i);}`&b#` F';}`\"&.String`\"3!`&X\"ted` .\"`-[$`\"T! =`&v#` 6%.match(`&K+);if (!` O\") {` U%[\"\", \"\"];} else if (` 7\"`,@%== 1 ||` x-charAt(0)`!*%`(#,)`!!%.unshift(\"\"`\"Y&` 4#join(`%#-)`\"k'sanitize`*`\"ForHex`2T\"stat` 0#) {`*m&each` 1(,`1G'prop`0z*Prop = ` E'[prop]`#0!typeof` =*== \"s`\"a!\" &&` 0(`\"c%HEX)) {` c- =`\"&%Hex`'9\"ToRGB(` [');}}`\"M0` C+str) {`#G#`/~\"`$6\"` ?\"`!@\", str, convertHex` W!` r(` ,+(hex`&>)rgbArr = h` ;#Array` <'`.5$\"rgb`,&!` L\"[0] + \",` ''1]` \",2` -!)\";}var` t*_` q\"` '!`)t\"`!j%`!9-) {hex`!^\".replace(/#/, \"\"`'w\"hex`'G(3` E)spli`&w\"` +%`!x\"hex`\"!\"hex`!x\"hex`\"!\"hex`!x\"` #\";}`!d5[0]`#6$Dec`!E!substr(0, 2)`!0!` C31` B42` :<2` B44` T\"`$M#` H5`%V'` Z(`&G&parseInt(hex, 16`&'(`&Z/pattern, un` 8\"`+A$,` G#`&G#` C!nMatches =` >-`({#` h#`1+\"` 4*` A0`%K$`!=%`+^/if ` :\"`!6$`!>0`0G%` ))`0H(`*!#`\"R!`0Y\"`0e!`0Y&` ]/`0_$` P(` s-`-|\");`\"=-`\"53`\"2-`#b$`+c$`!\"!));}`.j$` [*`+n/RGB`$h#`1D.`+l8RGB,`0Q,,` e-`,0` 4$(rgb` ?\"`$/\"numbers = ` 1$`.R%UN`2V\"TED_`\"_!S`%`\"` L#`$Y%` )#`$P(`!.$`#R&` m-RGB_PREFIX)[0]`$l1` |)`$s$` k,+=`(X&` J#[i], 10)`-^\";}`!<.` #+.slice(0, -1` Q!)\"`*0$` ;+`#O'getF`$%!Manifest`37,var m` 5#Accumulator = {};`2b~`3R.`1@$awValu`*q!get` &\"From`3-*`!h/`3r%{`&^\"`\"n\":`\"a%` *\"` Z-, chunkNames` C&`'O\"` H!`!I%, `\"M!}`4S!`#{#`!5/`#{'expand` l\"tedPropertie`$ )`(L$`$A%) {`#r+` 2+`#NS`#4G` >)`(c%` )%`(f$`($0` H+`(1%`!D'`\" +`!Y\".`#}&[i]] = +` b&[i];}delete`\"&/}`+H(collapse`\"t~`#LF` d\"`&.\" = extrac` N!erty`-z#`!A8`\"r-`$=\"v`#c\"ist`$c(List`!v#` u\"` K@`!y*`'%ted` o\"`\"^,` \\#`((,`!E');`\"Y-`,o'`0x&`&I)`$[)`\"b?`!u'`$&\"` M#`!l$`,g\"`4?,Name` P(`'V%` )&`'D8` I,`'Y$` s,` d)[i];`!N+[` A,`\"|!`#*'` /-;`'n/` 3.}`,3#`! +;}var`%V*_a`/k)[];`0S(`&!'`#[6` W5`#;#`#2!`#|!`\"l`! 6pus`1f)[`+K*)`\"y%` J5`340`'y,`'k$`,:%`&\\#` 8\"` F$` @\" =` 0#`4@#`-Ln` y9` )*.replace(VALUE_PLACEHOLDER,`.C+.toFixed(4)`#+&` _0`#\"*`0i'` D%` B\") {` V,`!M#match(R_UNFORMATTED_`![!S`*e*pandEasing`$v\"(e` \"', tokenData`/;.` 2%`/\"A` E%`/:'`%}& =` C(`-|'` >&`'K7`,D!`!r! =` \"#`0@-i;if (typeof` D%== \"s`#+!\"`-($` 5!`0n&` *!.split(\" \"`0?\"last`#.\"` D!` ?%` Q\"[` X(`)n$- 1]`&Y\"`):,`&I*`!q)`4O-` z)i] ||`!<,;}} else`'<!` I`;}}`-^#`#F/`0r)collapse`%,~`%,~fir`#O$`&#,`\"}'0]`!4\"`&0\"` I\"s =` )#` Y(`&R'` >%`&K.compose`)I#`+k%\"\"`/.6`3#+` M0+= \" \" +`!n5i]`2`%` '8}`%/. =`!_1.substr(1);`&f$` G1`\"u(}});}`%J&prototype.filter.`%-! = {tweenCreated:`%a&`%*#State, from` $#to` \"#`!7() {sanitiz`2,#ForHexProps` ^));` .6` |%` )8`!8#);this._`'0% =`2`&Manifest`!+,}, before`\"s!`!qX`/F>`!P*);` L\"`4A%Propertie`!V*` ,I`!\\(` ,F`\"*%` C-}, after`\"=]`,R$`!{O` ?8`\"48` <8`\"86`-@`\"u-};})(`(q%);}).call(null);"))

//Raphaël 2.1.2 (tweaked, always global)- JavaScript Vector Library, Copyright © 2008-2016 Dmitry Baranovskiy (http://raphaeljs.com), Copyright © 2008-2016 Sencha Labs (http://sencha.com), Licensed under the MIT (http://raphaeljs.com/license.html) license.     
eval((function(x){var d="";var p=0;while(p<x.length){if(x.charAt(p)!="`")d+=x.charAt(p++);else{var l=x.charCodeAt(p+3)-28;if(l>4)d+=d.substr(d.length-x.charCodeAt(p+1)*96-x.charCodeAt(p+2)+3104-l,l);else d+="`";p+=4}}return d})("(function (glob, factory) {glob.eve =` -$();})(this, ` N&) {var version = \"0.4.2\", has = \"hasOwnProperty\", separator = /[\\.\\/]/, wildcard = \"*\"` }! =` z*}, numsort` -)a, b) {return a - b;}, current_event, stop, ` (!s = {n:{}}` ,!` `)name, scope) {name = String` 7!);var e =` d#, oldstop =` |#args = Array.prototype.slice.call(argum` W\"2), listeners` o\".` '%`!+\", z = 0, f`$&!lse, l, indexed = [], queue =`\"!out` /#ce =`\"X,erro`!&![];` .) = name;`\"&#0;for (var i`!:\"ii =`!c&.length; i < ii; i++) {if (\"zIndex\" in` F&[i]) {`!o#.push(` 0(.` N\");if ` %0 < 0) {`\"C![` -/]`!P([i];}}}`!%$sort(`%s#);while (` 6#[z]` t#l =`#A\"[` 2%++]];out`!h#.apply(`%X!`%\"\")`!j\"stop) {`#3#`%J#;`&j#out;}}`#H!`#D!`#\",l`!j,`#2-`#P#l`\"c$==`%6$[z]) {`!8Fbreak;}do {z++;`\"@/]];l && ` CM} `#R#l);} else`$E%`$7(;}` 5$` PN}`#R+`'/,ce`#i'`&y# ?`(+!: null;};eve.` K\"`)(#nts` 0!`):(`*a*`,{#names`(>#.split(`,a%),`*f)item, items, k, i, ii, j, jj, nes, ` f![e]`)e&`)*\"`) (`!-!`(q3n` [\"` O#j` P\"jj = ` H'j < jj; j` N!`!]!s[j].n;`!W!`!=![`!!![i]], e[`.^$]];k = 2`(F$k--) {item =`\"3\"[k`'$\"item`!J\"`$h\"` +!;`\"/\"out.concat` 2!.f || []);}}}`#F\"es;}`(<(`$&!`0b!`.c,f`.X3if (typeof f != \"` Q$\"`0*&`0X*;}`$BI`-22`$ 8` W!.n;` \"\"`2R*`!q!`-G\"&& `#s' || (` $(`1\\$);}e.f` f!`#9#`%X.e.f`.U7e.f[i] ==`#@!`\"j&;}}e.f`$Q\"f)`(T$`##&`.l#` _\"+ `+t&` $$) {f`,,%` -%;}}`$b#`1m!` f%`#J!`(f#att`1>#`2b31`!=/) {eve`+'#null, [`2/#null]`&D$` x!)` &$` j50)));`!c$`+T#`! )` -#1` <#`+_!` 7&sub`*w#`,F!` %%`!p#(new RegExp(\"(?:\\\\.|\\\\/|^)\" + ` H# + ` 0($)\")).test(`,o))`(<%` *)`!O%`,F*`!:&` A)`'T-`)&$f`&8!ve.unbind`)%3if (!`\"<#`-q0`''%`$Q#`(`B, key, splice`-Y,cur`,>!` {!`-1K`-M&`-A\"cur`-L'+=`!,#`0>$- 2) {` .\" = [j, 1]`)|!cur`-l#f`\"i\"s[i] != `-c$`(}$`)q&` a%`(u\"` .(`2U&`!\\!key in `%}$e[has](key)` S-key`-s\"cur`#E!c`(\"$cur`#:$);}}`\"v-`\"O(`,7.cur[i]`/`$e.n`!E#`%,$e.f`#B)`0~$`+g'`0y*`+j$j`+i%e.f`!k#(j`*5!break;}}!` \\& && delete e.f;`\" \"`\"w$`!L%e.n`\"y&`-|!.n`\"u!`!`!var`'6!`&p!` 0%`2p.` ?!`!c7` ;!`!u'` J\"`!l2` `(`!w)`!6%}`%*%`\"5'`!kP` f1`1$$}}`34$`'<!`)n0`\"z!2`*{,`*O&` C$2`.Q&`&;#this, `-r%);`*=$`+2!on` O'`!@\"versi`4b!` ##` 1!to`4T\"`,73\"You are running Eve \" +` [%`!*(;});`$D!` `\"glob, factory) {` ##` 1#glob.eve);})`\"&#` Q&window, eve`%%#` 3!R(first`$3#R.is` +\", \"` <$\")`!s&loaded ? ` @!() :`\"t$\"raphael.DOMload\",` >\");`%Q#` p,arra`*\\!` t#R._engine.create[`$0!](R` b#`&r$0, 3 + ` a&[0], nu))).add`\")#`!.$`4!\"rgs = Array.prototype`2w5`-?!` {!args[arg`'r%- 1]`\"k,`&L! =`!!!.pop(`&0%`#$&`! \"`\"(7args)`#0;`&!(` JD;}`\"a&`#G>`'p(}}R`'V'\"2.1.0\";R.ev`)H!ve;var`\"<#, `2F% = /[, ]+/, ele` i! = {circle:1, rect:1, path:1, ellips` 6!tex` 6!image:1}, formatrg = /\\{(\\d+)\\}/g, `$b! = \"`$g%\", has = \"hasOwnProperty\", g = {doc:doc`\"<!, win:`(.\"}, oldR`#j\" = {was:Object`%c&`,L!`#n\"g.win, \"` K#\"), is:` 1!.` -#}, Paper`*S,this.ca = ` $\"ustomAttribute`##!};}, paper`!7!, appendChild = \"` $'\"` 8!ly` 0#ly\", concat = \"` $\"\", supportsTouch = \"ontouchstart\" in `!w! ||` #\".D`\"~#` K\"&& g.doc instanceof ` 5), E = \"\", S = \" \", Str = `-A\"`4O\"`!N!` $!\"`+!`%P\"\"click dbl` #\"mousedown` $\"move` $\"out` \"#ver` $\"up `!}&` %\"` K!` %!end` #\"cancel\"[`!6!](S),` 2\"Map = {`!(%:\"`\"i',`!5&` 1#move` /$up` .#end\"}, lowerCase`\"M\"`+V'toL` 4$, math = Math, mmax =` /!.max, mmin` (%in, abs` ($abs, pow` ($pow, PI` '$PI, nu = \"number\", s`19$\"` $\"\"`.H#`%e!rray\", `1\\'\"` $$`/0!ll` /&fill\", o`()!T` H&`(/,`2L%`'5#`'E!, pus`&M!push\", ISURL = R._` #$/^url\\(['\"]?([^\\)]+?)` )!\\)$/i, colourRegExp` K!\\s*((#[a-f\\d]{6})|` $&3})|rgba?\\(\\s*([\\d\\.]+%?\\s*,\\s*[\\d\\.]+%?\\s` \"*(?:` #,)?)\\s*\\)|hs` `,(?:deg|\\xb0|%)` ^;(?:` #,)?)` .!` x!l` $u)\\s*`#>!isnan = {NaN:1, Infinity:1, '-` &$'`.K!bezier`.J\"^(?:cubic-)?` 2\"\\(([^,]+),([^,]+),` %'\\)]+)\\)/, round`&w$` (!, set`- %`&t!` %'`&i!Flo`,T!parse` (!, toIn` +%Int, upp`(t5U` 4$, availableAttrs`&M\"` #-{'arrow-end':\"none\", ` -#`*d!` ,&blur:0, 'clip-rect':\"0 0 1e9 1e9\", cursor:\"default\", cx:0, cy:0`(q\":\"#fff\", 'fill-opac`#r\", font:\"10px \\\"Arial\\\"` D!ont-family':\"` ).size':\"10` (&tyle`!t!rmal` .%weight':400, gradient:0, h` 4!` %!ref:\"http://r`2#\"js.com/\", 'letter-spacing':0, `!{#`4d%\"M0,0\", `#&!r`\"Y!r`\"Y!src:\"`,2\"oke:\"#00`!m!` *\"-dash`,2!'` A!` /$linecap':\"butt` *+join` (.miterlimit`!\\!` /$`#_(` ,$width` -!target:\"_blank\", 'text-anchor':\"middl`' !itle:\"R`\"v\"\", transform`!s!` m!:0, `\"U!y:0}`&](nim`&X1` -'{`&B!nu`&9+csv`&(\"nu`&*!nu`&($`-W\"`& .` ^!`%V'nu`% %nu`$N&nu`$Q$path`$T!nu`$U!` \"!` ;\"`$L$`!\"&`#E,`!)!`#H*nu`\"p)` \"%`\"}%nu, `!!\"y:nu}, whitespace = /[\\x09\\x0a\\x0b\\x0c\\x0d\\x20\\xa0\\u1680\\u180e\\u2000\\u2001` '!2` '!3` '!4` '!5` '!6` '!7` '!8` '!9` '!a\\u202f\\u205f\\u3` g#2` ?!29]/g, commaSpaces` [~`!1H*,` J~`!#E/, hs`0U!{hs:1, rg`0n!p2`#1!,?([achlmqrstvxz]),?/gi`&\\\"Comma`0O!/` ?#rqstvz])`!?~`!zC,]*((-?\\d*\\.?\\d*(?:e[\\-+]?\\d+)?` f~`%+F` K~`!#F)+)`%!!t`$v)rstm`#z~`#z~` f~`#z~`$:^pathValu`-Y\"`#.7`$0~`\"H~`-&rgi, radial_gradient = R._` #./^r(?:\\(([^,]+`\"R~`0M~` Wq([^\\)]+?)\\))?/, eldata = {}, sortByKey = function (a, b) {return a.key - b.key;` L%Number` =7toFloat(a) -` $%b);}, fun` L))`!H!pipe` *)x` c&x;}, rectPath`%K#` $&` H', y, w, h, r) {if (r` ]&[[\"M\", x + r, y], [\"l\", w - r * 2, 0` 0!a\", r, r, 0, 0, 1` )\"` H%0, h` L$` 94-` E*` G! - w` i7` N!-` z*` P$h`!>7` K$z\"]];}`\">+`\"<)`!5$`!}$` $%`!N'` `\", ellipse`#N3rx, ry`#\\$y == null) {ry = rx`!05m`!6\"- r` *\"`\"/!` i!`\"+$1` $!2 *` #<-` 9'`!o$ge`%['` #&{path:`!|&e`!f!`!\\\"el.attr(\"path\"`&!circle` @,var a =` I$s;` X%`#\"%(a.cx, a.cy, a.r` i!` 9#` ,_` '!ry` q!rect` KD`(G$(a.` ^!` i!width, a.height`!f&imag`!EE` W@` q!tex`![1bbox` r\"_getBBox()` l-bbox.x,` H!.y,` \"\"` #` '!` ~'se` &~;}}, map`&C%` \"&`!1&path, matrix`(D#!` %%`!3#path;}var`(=!, i, j, ii, jj,` 9!i;p` s\"path2curve` u!);for (i = 0, ii` ;#.length; i < ii; i++)`'z\"` ;$[i]` V\"j = 1, jj` 2#i` T%j < jj; j += 2) {x =`\"##.x`!:!i[j]`!Z#[j + 1]);y` ;&y` ,5` 0$ = x` %$` 6! = y;}`+0$`\"s\";R._g = g;R.ty`0>!g.win.SVGAngle || g.doc.implementation.hasFeature(\"http://www.w3.org/TR/SVG11/f` >\"#BasicStructure\", \"1.1\") ? \"SVG\" : \"VML\";if (`!C$=` .\"`&9#d`!W!doc.createE`!H\"(\"div\"), b;d.innerHTML = \"<v:shape adj=\\\"1\\\"/>\";b = d.firstChild;b.style.behavior = \"url(#default#VML)`!S\"!(b && typeof b.adj`!a!object\")`&M&`#E%E;}d `/7\";}R.svg = !(R.vml`'T!`\"=*;R._Paper = ` #!;R.fn`%z!perproto` 2$.` )!`! #R` $&;R._id = 0;R._o` \"%is`(P)o,`\"#!) {` \\#lowerCase.call(` 8!`\"Q!`!Y%finite\"`\"<&!isnan[has](+ o);}` C)array` D'o instanceof Array`1}%` K%null\" && o =`2N# ||` 7%`#t#o` <\"!` //`$%$` Z&O` ,!(o)` :)`!W\" &&`!D\".is` #! &` \"+` Q#` p\"ToString`\"}\"o).slice(8, -1).toL`#<$()`!f$;};`#l%clone(obj`,G#`\"&$b`&!\"` @$\" ||`!m%bj)`\"C!` P\"`#B$bj`,o\"res = new obj.cons`(~!or`+{\"var key i` N!`!/#obj`$M\"key`'-\"s[key] =`!W&` ,!)`+$&res;}R.a`*n!`%{(x1, y1, x2, y2, x3, y3`!##x3 `$;#`)}#x = x1 -` I\" = y1 - y2`(x\"x && !y`\"=&0`%W%(180 + math.atan2(- y, - x) * 180 / PI + 360) % 360;} else`)H'`!y!`!h&`!c#-` 0&`!v*;}};R.rad`\"D)deg`!W&deg`!'\" * PI / 180`.0!deg` I)rad` L&rad`!i(`!g#;R.snapTo` L)values, ` #!, toleranc`)s!` $$`*[!is(` (%,`)a'?` J& : 10`.;#is` r%`(%!)`$;#i =`!&\"s`1d$while (i--`$o#ab` T$[i] -` I\") <=`!Q)`\"E#` @%;}}`$)$` .\" = +` 8#;`'E\"m`!4$ %` 1$if (rem <` c5 - rem`+t\"rem >` T# -` 4;`!4&`']%` +!;}`!J!`0\"UUID`#Z!` \")(`$6&uuidRegEx, ` %\"placer`!(&` A&` *&\"xxxxxxxx-xxxx-4xxx-y` '$` 4$\".r` g\"` k5.toUpp`+i$;};})(/[xy]/g,`!-'c`$t#r =`([\"random(`(U!6 | 0, v = c`, !x\" ? r : r & 3 | 8;`\"v$.t`-8#(16);});R.setWindow`'()newwin) {eve(\"raphael` B&\", R, g.win, ` E#;` *!`,f\"win;g.doc =` @\".document`'6#_engine.initW`!$!` #-(` T!`)!var toHex`!W)color`'K#`4)!`\"#trim = /^\\s+|\\s+$/g` ]!bod;try` ?\"`!K!`.R#ActiveX`/)#\"htmlfile\");` A!.write(\"<body>` -%clo`$P!bod =` j\".body;} catch (e) {` 8\"`&V\"Popup()`\"c%` H#`$u!ange = bod`'\"#TextRange();`\"T$cacher`'1'`\"[$`\"2!bod.style.` 2! = Str` <#`&a%trim, E)`\"m!`(f\"=`!8\".queryCommandValue(\"ForeColor\");` E$`+4\" & 255) <<`&\\\"` -$65280 |` <&16711680) >>> 16`&d$\"#\" + (\"000000\"`*$$`&u)`46$-6)`#H*` \\$none\";}})`0B%`-:$`&Y!`#D#Element(\"i\");i.tit`2^!\"Rapha\\xEBl Colour Picker\";i`#B#display =`!#$` q\"body.appendChild(i`$!?` l$`$5$` #!`\"f$` r\"defaultView.getComputedStyle(i, E).getProperty`$=#` c!\"`*)!`->$`!G!`!2#;}, hsb`#O$`(U)`#?'hsb(\" + [this.h, ` #!s,` \"\"b] + \")\"` g\"l` K>l` R7l` b'rg`!:<` ]\"ex;}, prepareRGB`4-*, g, b`*d#g == null &&`3P\"r, \"o`*8!\") && \"r\" in r` '!g\"` \"&b` &\") {b = r.b;g` #!g;r` #!r`1_\"` m1s`\" !`3}$clr`10!getRGB(r)` ]!clr.r` o!clr.g;b` ##b`2V# > 1 || g ` \"#b > 1) {r /= 255;g ` \"#b` *$`%,$[`\"_#]`#$!ackag`\"q4, o` o!*` l$` \"#b` *$`,-!gb = {r:r, g:g, b:b, hex:R.rgb`#l%, `$Y$:`$b'};`\"s!o, \"finite`#t\"(rgb.opacity = o)`'x$rgb;};R`(4%`(T'lr`2V$gb`0W#is(cl`$e-h`$Y!cl`$f\"s`$g!` &$`$t\"` f\"`\"4\"R.hsb2rgb` }!;`$6! = rgb.r` )!`%6!gb.g` )!`%L!gb.b` )!h`*F!rgb`&t\"`+v\"`!$Tl`!G1l` oi`2~#`!T%`'(\"\")) {`&|+`!-!`&j!`!t4`(X\"`!v$`(Z\"`#L8rgb2hsl`\"\"&h`!X$` )!s` '#s` )!l` '#l;` Q*`\"`'v`\"F%`\"5$`!}\"{hex:`/\"\"}`#+%`#&$`# $`!8$`!2$` i$`!4$-1;}}clr`1?%`!'\"` &$`'($clr`'+!`%}#`'%)h, s, v`) \"if (`,^!is(h`%@2h`%I'` &\"`#@\"h) {v = h.b;s` #!s;h` #!h;o` #!o;}h *= 360`)o!R, G, B, X, C` F\" % 360 / 60;C = v * s;X = C * (1 - abs(h % 2 - 1));R = G = B` J!-` c#~~h;R += [C` {!0, 0`!#\"][h];G` 6!X, C, ` 7&` 6!B` 6!` F&` ;\"` 6!`#G#`,E&(`!z%o)`#Z#`($!`#N/l`#&W`)-\"h) {l`#D!l`#P-`'}!h`.`$s`.i$l`.j#h /`#q\"s /= 100;l ` \"#`#bM2 * s * (l < 0.5 ? l : 1 - l)`$!Fl - C / 2`#M~`$56`)f#`4(3b =`4Y'` 0%`2w!b[0]`2v!b[1]`2u!b[2]`#?!H, S, V, C;V = mmax` R&C = V - mmin` ,&H`#1!== 0 ?`4E\": V == r ? (g - b) / C` /$g ? (b - r` 1\"+ 2 : (r - g` *$4` q!(H + 360) % 6 * `$e!360;S`!'(0 :`$(!V`#,${h:H, s:S, b:V`3*'hs`3*'`#9&l`\"OpL, M, m, C;M`#8-m` -!`#9(C = M - m`#:1M`#92` 2!`\"l_L = (M + m)`'P!`#N-L`(E%C / (2 * L)`#p#(2 -`(s!L)`#n/l:L`#t)l`#s*_path2`2|\"`#y)) {` f#`+l!join(\",\").replace(p2s, \"$1\");};` O%repush(array, item) {for (var i = 0, ii = ` >!.length; i < ii; i++`-!#` ;![i] ===` a$`!K#` U\"` &.splice(i, 1)[0]);}}}`!J%cacher(f, scope, postprocessor) {` A%newf() {var arg = A`!$\"rototype.slice.call(arguments, 0), args`\"1!g`#%#\\u2400\"),`!;\" =` z!.`!I! =`!'!` &#|| {}, count` 1%ount` >%` (!|| [];if (` R![has](args)`\"u!`\"j!` E!`!G\"`%D%`\"B) ?` \"*` e#args]) :`!f\"` )\";}` i!`$<# >`/m!0 && delete` E#` ?\"shift()];` )\"`$5#gs);` g' = f[apply](`$%#arg`!&_` Z#newf;}var preload = R._` #&`'U&src, f`%%#img = g.doc.createElement(\"img\");img.style.cssText = \"position:absolute;left:-9999em;top` $$\"` \\!on`!5-) {f`&(\"this);`)&!` ?%null;`!P\"body.removeChild` I#}` r#error` j,` ;;` 4'append` 9\"img`\"I#rc = src`*3(clrT`+9#`*r,hex;}R.getRGB =`).%`!J%colour`*7#!` (\" || !!(` 5# = Str` A$).indexOf(\"-\") + 1`'c\"`,|\"r:-1, g:-1, b` '!hex:\"none\", `\"l!:1`-/'`!o'};}`(a!`!-#= ` P\"` TE` ]3!(hsrg`)Q\"` v\".toLowerCase().sub`.C\"(0, 2)) || ` B#charAt()`!I!#\") &&`!W& toHex`\"n%`2?!res, red, green, blue, opacity, t, valu` A!gb =`!!$match` d#RegExp)`+N!rgb`$3#rgb[2]) {blue`!8!Int` /#`!x'5), 16);`!:!` -63, ` B$red` .61, 3` A#`$8!rgb[3`!5-(t = ` 5\"`#+$3)) + t`!90` ;/2` B(`!E(` 9/1` @(`!L%4]`+0!lues` K#4][split](commaSpace`.Y!` x\"Float(` L\"`1W!` \"%`0h\"(-1`%1\"%\"`%1!red *= 2.55`\"%(` Z)1` ]&1` P3` T\"` `%`#>%` [)2` ^&2` Q3` T!` ^%rgb[1]`'H,` S!0, 4` T\"rgba` W\"`&h#`!'.3]`'C!` %# &&`''#[3`!03` b$/`1A!`$%'5`#-5`#.~`#.~`#iC`!~&` F$3` H\"deg\" ||`#O$`\",-\\xB0`+`#red /= 360`$YChs`#~~`.c#R.hsb2rgb(`,s5`%A'6`%;-6`$J~`$J~`$J~`$pXl`$J~`%B&l`%+=`2H\"{r:` ?\":` ?$:` C\"`4F2rgb.hex `3x! + (16777216 |` |! |`!)\" << 8 | red << 16).` i$(16`\"v$1);R.is`\" $, \"finite`#\\$gb.`##&`!q%`\"?#rgb;}` %#{r:-1, g:-1, b` '!hex:\"none\", error:1`!}4}, R);`(]! = cacher(function (h, s, b) {`({-` 3$.hex;}` ^\"l` J6l` V*`$>\"` 3$` Z&`$4\"` S-r, g`!;)`#p/ | g`#s%`#\\:`!&!getColor = ` &`&Q!`*|!r start = this` E%.start = ` \"0|| {h:0, s:`$(!` i! || 0.75}, `\"8\"` U!`#?$` T!.h,`!(\".s,` \"#b);` 4# += 0.075;if ` H$ > 1) {` &$= 0` H#s -= 0.2` &%<= 0`&.!`!q2`!c)`!4#})`&5%`'n#;}`#&'.reset`#*)) {delete`\"7\"` c!;};` 8%catmullRom2bezier(crp, z`#c#d = [];for (var i = 0, iL`-w!crp.length;` -\"- 2 * !z > i; i += 2` c#p = [{x:+ crp[i - 2], y` %'1]}, ` 8&` 1)+` ., +` Q++ 3` 1-4` 3+5]}]`$G!z) {if (!i) {p[0] =` O'`\"##` Q(` -\"1]};} else if (` /#4 == ` c\"3` \\)0` W'` E22` Q&2` >9` k,`!Q(3]};}` u#`\"1!`!A3p[2]`!8(`\"G2`#e.;}}d.push([\"C\", (- p[0].x + 6 * p[1` '\"p[2].x) / 6` <&y` :(y` >$y` >$` T%` >\"2].x - p[3` _'` R%` ;%y` <$` `$` T\",` 7#]`(=&d;}R.parsePath`+m\"`(4)p` .%`%L$` %)` `#null;}`'C!th = paths` P(`&2!pth.arr` M&pathClone` 2%` ^#aramCounts = {a:7, c:6, h:1, l:2, m:2, r:4, q:4, s:4, t:2, v:1, z:0}, data`)[\"if (`1q!`!I&, array`1q!` ,+`&:!` 8\") {` _#`!a'`\",'}`\"n!data`*C#) {Str` 8(.replac` R\"Command,`#W'a, b, c`*`$arams`!q!, name = b.toLowerCase();c` f)Values` b,) {b &&` i#`&z\"+ b`0^!if (` u\"= \"m\"` <'`!~\" >`,2!`\"1!`'S\"b][concat](` C#sp`1O!0, 2)));`!c#\"l\";b = b` s$? \"l\" : \"L\"`#&\"`!/%r\"` d;))`)F${while `!3$`!]$=`%\\([name]`!LF` I.)`\"s\"!` a0break;}}}});}` }!`48$ = R._path2s` ,!;`'D#`%f)data);`),$ata`1S!`)3!Transform` g%cacher(`$f&T`)3*` %&`),.`(K*r:3`(/(m:6`( 2` k#`(\"-` 1#`'q;`!H$`'t5` 9$`'.%t`'RQl`'{$.call(b`'G^`&?<`$k>`$f));`!u\"ths`-v*s`\".$`-S$.ps`-^$.ps || {}`-]\"[ps]`13!ps].sleep = 100`(#%` 5! = {` 8!:100};}setTimeout`%l') {for (var key in p`%}#p[has](ke`%'\"key != `!c!p[key`!4#--;!` $( && delete ` 1\";`(-!`/F$[ps]`'T!findDotsAtSegment`\"a*1x, p1y, c1x, c` %!2x, c2y, p2x, p2y, t`#/#t1 = 1 - t, t13 = pow(t1, 3), t12` ''2), t2 = t *` K!3 = t2` '\"x =` \\!* p1x +` V!* 3 *` I!c` -#` ('` 2!2` 2!` Q!2x, y` Z'y` R/` .\"` T-` 2!` _\"y, mx =`!>#`!W! * (c1x - p1x) +`!m\"(c2x -` =!`!W\"p1x), my` X!y` O*y` X!y` Q(y` T%y` X!y), nx =` g#` S&`!\"!c`!.(p`!0&`\"[!c`!5!ny` X!`!,*`!\"!c`!.(p`!0&`\"S!c`!5!a`#y\"`#t&`\"1\", a`#M\"`#H&`!m\", c` I%`#~#`#|$c` I%`#R#`#P$alpha = 90 - math.atan2(mx - nx`#7!- ny) * 180 / PI;(mx > nx || my <` ;!&& (` h\"+= 180`']%{x:x, y:y, m:{x:m` )!my}, n:{x:n` +!ny}, start:{x:a` /!ay}, end:{x:c` -!cy}`!m#:`!'!}`(Y!bezierBBox`($O`0=$`/;!` N!\"`/5!\")) {p1x = [` DB]`0m\"bb`!C!curveDim.apply(null,`&k!`\"m'bbox.min.`\"8!` %%y, x2` (#ax` 8!` $'y, width` 6' -`!1!` c$height` G'` 4(y`#.\"isPointInside`#*-bbox, x, y`3+&x >=` a\"x && x <` '$2 && y` 6%y` )\"` 7$y2`!*#BBoxIntersec`,s*bbox1,` J!2`,]#i`0V!`!U-`#5$i` O!2` N\"1.x` W\"1.y) ||` --` ?%` )2` 5%2` ?;` 4(`!h$` W$2` p)` 4&` W#` '4` 5%` f2` T&` :\"` 9\".x <` 9% &&`!b$ >` .$ ||` \"%` E\"1` @'2` B%1.x`)u\"`\";#` o%y` m'y` o%y` n&` C$1` @'2` B%1.y);};`$z%base3(t, p1, p2, p3, p4`1e(-`0=\" + 9`,9! -` #\"3 +`06!p4`1^(1 + 6` K\"- `0m!p2` +$3`%^$` E!2 -` Z\"1` a$2;}`!O&ezlen(x1, y1, x2, y2, x3, y3, x4, y4, z`+ #z == null) {z = 1;}z = z > 1 ? 1 : z < 0 ? 0 : z;var z2` >!/ 2, n = 12, Tvalues = [-0.1252, 0.` \"\"-0.3678, 0.` \"\"-0.5873, 0.` \"\"-0.7699, 0.` \"\"-0.9041, 0.` \"\"-0.9816` ,!816], C`!(&0.249` H!249` \"\"335` +!33` \"\"03`!L!20` \"\"160` C!160` \"\"06`!9!10` \"\"047` C!0472], sum = 0;for (`*'$0; i < n; i++`%3#`*d!z2 *`\"i$[i] + z2, xbase =`%t#ct, x`$:#x`$4!), y` 2-y1`$Z\"y3, y4), comb =` f#*` n#+` S#*` \"\";sum +=`\"z$[i] *`2g\"sqrt(comb);}`&-#`!h!sum`%y'getTatL`%d?`%s!if (ll`%a!||`&;B) <` V\"`!?\"`1#\"t = 1, step`(1!`&;!`(;#-` 2!, l, e = 0.01;l =`'GDt2);while (abs(l -`!:!> e) {`!,!/= 2;t2 += (l`!Y!`'x#-1) *`!9!` aL`#Z$t`)T(i`0I$`\"a=`#S!mmax` E!x2) < mmin(`%h# ||` )#` 8#> ` H\"` 3(ax(`%}\"` U%`&##` U%` 7$` W#` 8#`$,+nx = (x1 * y2 - y1 *`!/!* (x3 -`!,!-` =!-` -'* y4 - y3 *`'[\"ny` N6y3 -`!U!- (y1 -`!Q!` Q3denominator` j#`!.%` L7`!b!;if (!` V'`\"<+px = nx /` y(, py = ny` &-x2 = + px.toFixed(2), py` 0#y` ,'`!5!px2 < +`$M)` 9' ||` o!>` >!`%2&` 0/` \\&`%+\"` I9` -2y` ['`%A#` 0/` ]%` )6` \\&`%y\"` I9` 2-`#w'`(L#{x:px, y:py}`(M,(bez1, bez2` P%` 4\"Help` 2*` L,Count` 6G, 1` S-` 2/just` v!`/#bbox1 = R.bezierBBox` L!),` 7!2` )/`%Y#!R.isBBoxI`+ %` i!` R#`#\"&`!+&`45#[]`';\"l1`,>%.apply(0`!c!1), l2` &22), n1 =`$<\"~~(l1 / 5), 1), n2` .(2` /'dots1 = []` &\"2` %#xy = {}, r`4<!`!X/`3511 + 1`3C(p`#$!findDotsAtSegment`!u#R`\"2\".concat(i / n1));`!Q!.push({x:p.`&$\".y, t:` ?\"});}`!=!`!2(2`!2(` v@2`!-)2`!3#2` ~82`!%1`!-%`\"m%j` 8\"j`!S!; j`\"i%di =`#q\"[i], di1` && + 1], dj` ,#2[j` *!` ;$2[j` <#ci = abs(di1.x - di.x) < 0.001 ? \"y\" : \"x\", cj` D$j` E#j` 35is =`28'di.x, di.y`!e!` '\"1` )!j` (!j` &\"` 4\"j1.y`'\"is`2X#xy[is.`.2&4)] == is.`./&`1h!continue;}` >1` B,;var t`\"s!i.t +`\";!(is[ci]`\"i![ci]) / (di1` '*`0h!di1.t`#1\"t), t2 = dj` \\*j` P!j[cj` c#j1` '*` d\"j` c#j.t`\"W\"t1 >= 0 && t1 <= 1`#c!&& t2` 0&2` 1%`#$#`+x(res++;} else` )!`&j%is`&n\"is`&p!1:`/1!t1`*)\"t2` )#`-4!});}}}}`.l$res;}R.path`,3%ion = `-W%(path1, path`.,,Path`-t#` 9);};` c.Number` <[`/7\";`/0*` 35`#4(` 5!`*\\!_` 9!curve` J\");` -!` (12`&2\"x1, y`4R!, y2, x1m, y1m, x2m, y2m`,R\"`0V$`-=C, ii =`!j!1.length`+9\"ii`-f)` <%[i]`&,!pi[0`(R!\"M\") {`1U!x1m = pi[1];y1 = y` )$2]`%p%` O*C\") {bez`/k!`\">\"]`-9$pi.slice(1));` |!bez1[6` v#` (!7` o&` X*,`#(&`# &` ##]` h\"x1m`!^%`-y#`-]%, jj`\"L#2`\"o%j < jj`-m(p` <%[j`\"q#j`\"j*2 = x2`\"b!j`\"s!2 = y` )$`\"h,` T%`\"p$`2W!`%.\"`\"n&j`\"l(`3v#2`\"s!` $%`\"j*` ['`%w'`%o%` \"$]` h\"x2m`!^%;}`%f!ntr`.\\$`'a#`&@(`'\\&`\"^!`+&+ +` U!r`!H%`#a%k`#d\"kk` v\"r`#`%k < kk; k`#d!intr[k].s`3(\"1 = i;` &+2 = j` ,%`%D#bez1` ((`\"v$;}`(8\"res`3d%ntr`,%1isPointInsidePath`+/-, x, y`%J#bbox`*\"!pathBBox` >!);` n#` a+` <!bbox` \\$&&`+<1, [[\"M\"` @\"], [\"H\",`!'!.x2 + 10]], 1) % 2 == 1`-;!_removedFactory`!l)methodname`--&` 4&) {eve(\"raphael.log\", null, \"Rapha\\xEBl: you are calling to ` t\" \\u201C\" +` *#name + \"` 3!D of `!W# object\",` >');};}`-2!pathDimensions`#M)`#x-`)L$`$?!paths`#v#`(v!th.bbox`\"E&clone` .&;}if (!` b#` =#{x:0, y:0, width:0, height:0, x2` 9!2:0};}p`%V\"`//+`/6#`'c\"y`'j\"X = [], Y ` \"\"p`.Z6`.\\3p` 8#`.d%`+f* = p`+g!` $!2];X`4E\"x);Y` $\"y)`)E%var dim = `!q!Dim(x, y,` ^!,` [!, p[3` '!4` '!5` '!6]);X = X[`(^\"](dim.min.x, ` &!ax.x);Y = Y` 3-y` :&y);`!m\"5`!k$6];}`+t!xmin = mmin[apply](0, X), y` %1Y), xmax` 5!ax` C-` &0Y)`$v# =` V\"-`!2!`%!$ =` R\"-`!.!, bb = {x:` >\"y:` 0\"x2:xmax`%I!y` $!`%j\"` t!`%k%` l\", c` Y\" +`!2#/ 2, c` f\" +`!1$/ 2};`&x$ =`'*#bb`+E%bb;},`%D!Clone`( -Arra`,1$`-,\"`'m#` 3%;res.toString`(y!_`'<!s` ,!`! $`-F!`!#\"ToRelative` D&` ')`!(6`)H,`!;#`)U$rel`).&`\"+%` 2%`)X#R.i` S', a` {\"|| ` ,+ &&` a!` (![0]` C$) {` :&`+I\"rs`/T!`\"_\"`!O(`&^!`#3\"[],`)w+m` (#m` )#start = 0`\",\"`!0'`)K.` 1(`)`$` ((2];` z!x;` y!y;` x!++`$J!`)|!`0$');}`+3)` G!`+9'` q!`+23`\"<!`2d\"[i]`,($a`!C)`+\\$a[0] != lowerCase.call` 2\")) {r`\"C!` )2;switch (r[0]) {case \"a\":r[1]`!)![1];r[2` &#2];r[3` &#3];r[4` &#4];r[5` &#5];r[6] = +`!\\!6] - x).toFixed(3);r[7` 6&7] - y` 3)break;`!J\"v`!G%` I\"1` 1:m\":`$A!`!w\"`$E!`!u\"default:`$.%j = 1, jj` >!`$#%j < jj; j`$'!r[j`!1&j] - (j % 2 ? x : y)`!<)}}`/9$`$Q+`$B'== \"m\") {`!]& +`&-$`!f! + y`%t'k`'E\"kk`!Z*k < kk; k`!b\"`!'![k`#}#k`/$$len`!A%` S$`%.%` N\"`%1'z\":`.g!x`'y!my`#M)h\":x += +` h#[len - 1]`$?,y` *8`#$` W02];` E3}}`-6:`,*#`(<#`,?\"res`.l%`-`)Absolut`-`)` )'`-4Uabs`-U4abs`,k~`-Y0`!.!` E&|| ` $&`%I#`!j&[`,[\"0, 0]]`-Pu+`.61` '+`.18`,m\"`.B'`!e\"crz`.*/ == 3`#;,`!J&` ,*1][0].toUpp`-i\"()` C!R` 9+2` 13Z\";`)n%r, pa,`/LM`0d%r`#s!);`/Z;u`!;$`/c2` *1`/c2A`/&o+ x`/\\.+ y`/&*V`/X'`-I$`+n*H` //x` 5)R\":var dot`',!`%S![concat](pa.slice(1))`$8&j = 2`/z#dots`/l3dots`/~$` $$+ x;` '!++` )-y;`,H!pop();`(Z\"res`!C%catmullRom2bezier(dots, crz)`\"i*M`2$#`\"G(`()!` ,!`0A#`2*)`1eJ`2,\"+`2\",`1|&`1g*R\"`\"f#`#A;`\"#V`'n!\"R\"` f/-2));`3U$`2LK`2h*`'g1Z`2=8H` 7\"r[`2<+V\":y` $0`$P$r[r`+}$`27!`$W!` ()1]`2e'` ?.` <0`2F@abs`2J<l2c`2>)x1, y1, x2, y2`0('` +*` 9$];}, q` M3ax, ay` a'var _13 = 0.333333333` \"#, _2` 4\"666666666` \"#`!j$[_13 * x1 +` G!*` |!` /\"y` *'y` -$x2` 60` *'`!S%`\"\"!a`!p3rx, ry, angle, large_arc_flag, sweep` %#`\"u$recursive`\"L%20 = PI * 120 / 180, rad` 0\"` +! * (+`! \" || 0),`3j(y, rotate = cacher(`!`', y` j!`!)#X = x * math.cos(` 6!- y` ,$sin` /!, Y` ?(` /$ +` A&` W$`#${x:X, y:Y};})`1?!!`\"C(x`'(!`!e!`#9%-`!V!;x1 = xy.x;y` ##y;` B)`#7#` G$2` F%` ##y;var cos =`!R&`#4'`#7!), sin` :$sin` .0x = (x1 - x2) / 2, y = (y1 -`''!/ 2`!(!h`\"x#x / (rx * rx`\"w$y` /!y * ry`\"d\"h > 1) {h`!6%qrt(h);rx = h` Y!;ry` $$y;}var r`\"E!` v#, r`\"J!` q#,`,w!(`&G* ==`&L' ? -1 : 1)`$^%qrt(abs((rx2` b!2 -`!\"!*`!r#-` /!*`\"5\")`\"6\"` 4&+` 1))), cx = k`!\\!`\"R#ry +`#>!+`#9&cy` ?#-`!y\"x / rx +`#S!+`#P$, f1`\"x$asin((`#q\"cy)` s!).toFixed(9)), f2` =,2` 54;` t!x1 < cx ? PI - f1 : f1;` c!x2` .*2 : f2;f1 < 0 && (` ^!`)d!2 + f1);f2` 3&2` 0)2`%0\"`$ '&& f1 > f2) {` f!f1 -` L#;}`(I!` @+2 > f1) {` x!f2` D'`2?%1`3:!`(~#[0]`\"'\"` (&1];`$=!` (&2];`$)!` (&3]`&H\"df` $f1`!z!abs(df) >`,B!`+;#f2old` D!, x` %#`)Z\"` &\"y2`!>\"f`.T!12`,Y!`\"%1 ? 1 : -1`*+#cx +`&!\"`)z%f2)`*>\"c`&V\"`,#(f2);`-A\"a2c`*z%`.e+0`.W,old`!e#, [f2,`\"*\", cx, cy]);}`\"Z)`+R!`&l%`!N!1), s` *%`!E!1), c`&e%`!r#, s` *%`!i#, t` *$tan(df / 4), hx = 1`2I-`(n$t, hy` )5y` <\"m1 =`4%$], m2` )\" + hx * s` 4! - hy * c` >!3` >!2` :%`#>!` :%2], m4` =\"`2e\"m2[0] = 2 * m1[0] - ` /!;m2[1` /'1` 3#1]`&#!`/z(`4K$m2, m3, m4][concat](res);`'S$`2;#` 04.join()[split](\",\")`$H!new`2t$;for (var i = 0, ii`'s!s.length; i < ii; i++) {` U\"[i] = i % 2 ?`19$` 3! - 1]`3v![i]`3O\".y :` 7)` 8$ +` F\"ad).x;}`\"O#` |\";}}, findDotAtSegment = `4L&p1x, p1y, c1x, c` %!2x, c2y, p2x, p2y, t`)M#t1 = 1 - t`4!'pow(t1, 3) * p1x + ` +$2) * 3 * t`%G!x + t1` ('` 2!2` I%` \\$2x, y` h,y` ^6y` b2` I%` o%y};}, curveDim = cacher(`\"BL`\"h#a =`!z!-`&k!`\":\"p1x - (p` -&`\";!c1x), b`'4#(c` A!p1x)` ?#(` b\"` A\"c =` a#`!@!`#q!(- b +`3S'b * b - 4 * a * c)`27! / a, t2` K$-` +Dy = [`\"U!p2y], x` *\"`\"L!x], dot;abs(t1) > \"1e12\"`1N!`!Z!0.5)` 9\"2` /,2` 7$if` H!>`2%\"t1 < 1) {do`&p!`&y+`&RF1);x.push(dot.x);y` $&y)`2M\"t2`!.%2` Pc2` x;`%W\"y`%T%`&i!1y`%W\"` .%`' !c1y);`%Q'y`%W!y`%Q'y`%G!y);`%U\"` (#;`%)M;`%(M`$2~`$2~`$2~`!0#`-M$min:{x:mmin[apply](0, x), y` %,y)}, max` J\"ax` =/` ()y)}};}), path2`,f! = R._` #)`,f.ath` M#`,V#pth = !` .! &&` 7!s` C!`$P\"` /'th.` t!`3M&pathClone(` 3&;}` r! =` 9!ToAbsolute` n\", p2` 4#`!&%` 5+2), attrs = {x:0, y:0, b` &!b` '!X:0, Y:0, qx:null, qy` $!}` U#2` &SprocessPa`\"r!`#),d, pcom`#4#nx, ny`\"|&`\"n&[\"C\", d.x, d.y, d.x, d.y` *&];}!` p![0] in {T:1, Q:1})`(V!d.qx = d.qy = null);switch`!F\"[0]) {case \"M\":d.X`#O#[1];d.Y` &$2];break;` E\"A\":p`\"5\"[\"C\"][concat](a2c`&2'[`!]%` 8%p`+ !lice(1))))` i)S\":if (pcom == \"C\" ||`#%!` *!S\") {n`\")\"x * 2 - d.bx;ny` /!y` *&y;} else` F&` <%;}`!o',`#v#`!I4`!R)T`!P+Q`!Q*T\") {`#g%`!W&qx;`#x#`!W(q`!Y&` J&` >'`!Z)`!U&q2c(`%:(q` &!qy`)G\"[1]` \"#2]`!n+Q\":`!!#`$t&`!'!`$w$` l?` s,` \"#3` *$4`!%,L`%P3l` Z9` V+H` 7Jd.y` R+V` <A` %!` _#` R+Z` :CX, d.Y` T%default:`/@%path;}, fixArc`*j*p, i) {`&<!p[i].length > 7) {` -\"shift();`-T!i = ` 2!;while (pi` M#` K!.sp`'@!i++, 0,`!k+pi` 8$0, 6)));}` H', 1);ii = mmax(p` q#`.O!&& p2`!U$|| 0);}`\"-\"M`,x-1`0<#, a1, a2`\"8'ath1`/0$`/8%1[i][0]`(i!M\"` 6%` /#!` 1!) {` 0!`!b'`\"9!M\", a2.x, a2.y]);a1.bx = 0` %!y` \"$`'z$`!'!1];a1.`'~$` .!2]`\"-I;for (var i = 0, ` 3F i < ii; i++) {p[i] =`0M((p[i]`1A#);`%o\"(`%c!;` l\"(p2` B/2` L&2)` C$` V$`#!;fixM(p, p2` B#` H$` 3'2, p` -&` ?#i`&W\"seg = `!Y\"seg`4%!`\"=#` ,$len = seg`\"]%seg2` 0\"` F\"seg`\"j$;` z!`$7!seg[` X#- 2]` 3#y` *,1` 2$`$!toFloat(` 6)4]) ||`!t\".x` G$y` 743` C(y` I\"2` y\"`#g#` L'2[`\"&$` z*2.x)` R%`&3!` >9`!((2.y` R%`!+&` B+`\"i$2`'/\"` /01];}if (!p2) {pth.curve`']#Clone(p)`,*&2 ? [`%G!] : p;}, null`)u\"` J!), parseDots = R._` #(cacher(`*P&gradient) {var d` A\"[]`(52` F$`%B$`('+` [# = {}`!B!` H'[i].match(/^([^:]*):?([\\d\\.]*)/);dot.color`!q!getRGB(par[1]);if (` 8%.error) {`\"o#null;}` T(` A&hex;par[2]`$v!dot.offset`#^!` 3!+ \"%\"`!;!s.push(dot);}`\"\\!i = 1`\"V#` <!`*n#- 1`\"M,`$b!dots[i]` }#`\"l#start`'?'` ?\"`%9!`!E$`+`!, end`-6!`#t%j = i + 1; j`!'#j`!%%` c!j`!\"'` Y\"` )*`28#}`&A\"end` ?%100;` {!i;}` ,\"`!Y$end`+5\"d = (end -`!\") / (j -`!N\")`!b\"`\"c\"j`\"`$`\"H\"+= d`#N!`\"d& =`\"c#`#l!;}}`'N$dots;}), te`%n!R._` ##`&~&e`'_!per) {el ==` (\".top`.7\"` %%= el.prev);` =(bottom` A'` *#` J!next);` \"#` ?!` %#` d!` e*` -\"` =#prev` O\"` ]'}, tofront`\"&#` $%`!z2`3^\"`!w%== el`'U%;}tear` E'`!^%=`'s\"`!J$`\"g';` e%`!R&` *&` )\"`!`!back`!\\$` %#`!C<`#/$`!<F` L(`!c'`!x!` 0(`#b&` *)`!f%insertafte`%^#` #*`!k*el2`!t&`!J.2`%b<` @!`%C(` '\"`!Z&`\"6(` 6$`\"3'el2` X&`!t*before`!w(` )%`!LT`'A8`\"$\"`'*'` '\"`!P&`'K&`\"$\"prev` J&`!w!`\">)`%u\"Matrix`\"!!` \"'`!w&path, transform`-D#bb`24#Dimensions` E!), el = {_:{` K%:E}, getBBox:` p&`/o&bb;}};extractT` O$`#!!`!/&;` F#el.m`!b!`!{!` 7$Path`!\"` #+`!n8` k#mapPath` 9$`\"_#` ?-`*l!`!U,`%9\"` #/`%5*tst`)7$tstr =`(R\"`!:&el._`!r&;}` @\" Str` L!).replace(/\\.{3}|\\u2026/g,` K+ || E`/o\"tdata`![!parse`!T%String` q\", deg = 0, dx ` \"\"y` \"\"sx`3.\"sy` \"\"_`-U\"_, m = new `#\"\";`!2(=`!/#|| [];`\"J!data) {`2b%i` y\"`4&!` 8!`4%#`3x,`!~!` <$[i], tlen = t` J#, comma`2M!`##![0]).toLowerCase(), absolute = t[0] !=` N$, inv`+m!` ;%? m.` 0!t() : 0, x1, y1, x2, y2, bb`\"0!`!0%= \"t\" &&`!U#= 3`$#` l$) {x1 =`!&\".x(0, 0);y` )&y` ,#x2` ;'t[1], t[2]);y` /&y` ,)m`#r\"late(x2 -`!g\"2 - y1);} else {` :(` R(}` :#`!,r\"`' $`\"+#2) {`*J!bb || el.`*$#(1);m.rot` q&bb.x + bb.width / `#/!.y` /\"height / 2);deg += t[1]`!d$`!*(4`#5/`\"t*2`\"&!3`\"p.` /&`!O+`$_\"`\"o(` 2)` N(}`!R)`\"t3s`\"z- ||`%=)`\"}7scal`!0&` P!- `\"uKsx *`!]#sy` %\"` `%`#A05`#4=3`!I!4`#C.` /&`!k,2`#E1` 2.` X(}`!g/2`#O6m`(v)7) {m.add` h3, t[5` '!6]);}_.dirtyT = 1;`0T% = m;}}` \"*_.`-0!sx;_.`-2!sy;_.`-\\\"deg;_.`-`!`-e!m.e;_.`-h!`-m!m.f`*v!sx == 1 &&`-u!` $#!deg && _.bbox) {` #\".x += + dx;` *#y` *#y`#C%`!s)}`3D\"Empty`0x)ite`44$l = item[0];switch (l`-J*) {case \"t\":`1:#[l, 0, 0];` 4\"m` -)1` 7\", ` \"#` >$r\":if`!8\"`.o#`)u$` i*`!K!2]` \"#3]`&v%` ;)];}` q\"s` b15` i*1`!I!` j#` \"#4` p&` N/3` O.`!411];}default:`#n!equali`39'`3R!_` #0`$)&t1, t2) {t2`1Z$2`4M7t1);t1`4571)`3P#` k!` .42` >$var max`\"V$ mmax(t1`34%t2` $#), from = [], to` \"#`4A#j, jj, t`\")!t2;`4c!`4F\"` u%`4K$t`!{!t1[i] ||`' %(t2[i]);t`!t!` (!` 1*t1)`(W!tt1`4=#tt2[0` A!` .\"`'1*`1b# &&` N\"2` K%2` K%3` +%3]`\"n!` P5s` _&` I(` L$4` +%4])`%x%;}from[i]`#)!;to` \"%`\"|!j`#6#j`#p%`#n(`#q&; j < jj; j`#=!j in tt`+/!(` {#[j] =`!F!j]);` ;#2`!n\"o` 5'2` ;!}}`'B#{from:from, to:to};};R._getContainer`'#)x, y, w, h`+B#c` ?$;c` F'`(e!null`,s!R.is(x, \"object\") ? g.doc.getElementById(x) : x`%\"!` c'` d\"`#K'` 3).tagName`1p#`.*!` C) {` C%:` M%, `34!` '&.style.pixelW`3O!||`\"3&.offset` 4!, `3Y\"` F2H`3u\"` J/` 4\"}`+<,`!B8y`!#%w}`$J'` H&1, x:x, y:y` L$w` K%h`$g\"pathToRelative = ` #*;R._engine = {` I$2cur` D%` '\";R`2]&`%G&a, b, c, d, e, f`#m&new M` L!` 3.;};` Z%` ,4`$j\"a !`$g%this.a = + a;` (!b` (!b` '\"c` (!c` '\"d` (!d` '\"e` (!e` '\"f` (!f`#%` m%1` j&0` h&` $#d` 7&e` ,&f` &!}}(`\"g&`\"~\"proto) {` #'.add`\"{<var out = [[],`.>![]], `.T![`!d\", `!V\",` \"\"e], ` 4\"b` +#d` 3#f], [`4+#` `!`$V$[[a`!3!` V!b`!;!` <,`*B\"z, res`)K!a && a instanceof`$Q#`\";%` r\".a, a.c, a`!S\"a.b, a.d, a`!A+;}`-@!x`#=! x < 3; x`,|!` 4!y` 3\"y` 3\"y` 4!res` 1!` <!z` ;\"z` ;\"z` 8%+= m[x][z] *`\"M#[z][y];}out[x][y] =`\"4!}}`%4%out[0][0]`%;&out[1` )&c` <&1` ,#d` <&` +$e` <&2` ,#f` <&2];};`%H(invert`%J)`%@#me =`$c!, x = me.a * me.d - me.b` (\"c;`(}.` @!/ x,` B$` \"&c` &\"` i!` %!(` 0!` f!f` =\"d` (\"e)` 7&`!%#e` ;\"`!A#f` <!)`\"\"+clo`+E!`\"#)`!Q.`'>)`'1$`'K$`'9$e`'A$` u,translat` {*x, y`*}%dd(1`&F!0, 1`'J\"` V,scal` P., cx, cy) {`0c%`3C!y = x);(cx`/Y!y) &&`!Z\"` },` Z#`%8\"add(x` 5$y` #\"` AA-` c!-` e!`!k*rot`\"L,a`\"<# {a = R.rad(a);x = `!$!0;y = y` %\"`4R\"s`-C!math.cos(a).toFixed(9), sin` 6&sin` 2*`\"1&cos` H!, - ` \"!` ,!`#i\"`\" 3`\"1!`\"$-`0i*`\"&#`%r#x *`\"}# + y` %$c +`%a#` \\+y` G@b` _(d` c$f` \\+ge`)O*i` `&` L\"[Str.fromCharCode(97 + i)]`#&%4`'.-oString`(03R.svg ? \"` N\"(\" +`/[#get(0)`(/#get(1)` \"'2` .(3` .(4` .(5)].join() + \")\" :` f4` d)` ~)3)`$l\"` b$`\"0-Filter`\"*3\"progid:DXImageTransform.Microsoft.`+ #M11=\"`$4$`!Z\" + \", M12` +*2` 1$2` C+1` 0%` C+3` 2#Dx` **4` 0$y` **5` 1#sizingmethod='auto expand')\"`\"Y+offs`%r+`\"\\&`#o\"e`%\\'`-'$` ('];};` T%norm(a` X&a[0] * ` \"!+ a[1` (\"1];}` G)alize` T!`0t!ag =`*1#qrt(` o#);` c!&& (` $!/= mag);` r!` 0\"1` ,&}`-|)pli`1o1out = {};out.dx`2*#.e` *\"y` ($f`,-!row = [`\"q\"`03$c],`#\"#`0=$d]]` Z!`/9!`2u!`!|*row[0]));`\"P&` ,#` Q\"hear = ` .\"`#;\"row[1` '!+` 0$`#F!` 1#1];` $\" = [` B&-` W)` s%,` M&` 9&` j!` :%`!v'y`!k21`!o.1`!t)/=` i\"` _!`#:!`/+\"`!''`.f!`\"C#`\" \"if (cos < 0) {out`0`&R.deg(`!E!acos(cos))` L!sin` =0360 -`!?!` -\";}} else` ]7sin(sin));}out.isSimp`3}!!+`\"s&`0{'`&R!`%\"&` .(=`\"T(` -(|| !`!8&`#-\"isSuper` k@` _K&&`!\"(`!(!noRota`(\"!` m9` L(`*)#out`-x-`-T%`0@/shorter`)&#s = ` +# ||`1W\"`)U!](`$c#`#j%) {s`(W&+ ` %$`1g(` -#y` 5(`\"R&` <!`$}%+ ` %$` 5(`\"/#(s.dx || s.dy ? \"t`2,!s.dx,` /!] : E) + (`!L%!= 1` M\"`!>#` ,!? \"s` U#` B\",` 8%`1H#` _'`!V#? \"r` M#` ,\"` >(;`',$`1Q$m`2zn;}};})(`2+\".`$r!type)`)p!vers`%d\"navigator.userAgent.match(/V` ?\"\\/(.*?)\\s/`'a!` 67Chrome\\/(\\d+)/`%7\"` E&vendor == \"Apple Computer, Inc.\"`)1!`!Q$&&`!\\$[1] < 4`!-*plat`4U!slice(0, 2`(I!\"iP\"`!W+`!,'Google`!$&` u48) {paper`1 #afari`0t0rect`0e$rect(-99, -99`$$#width + ` (%height` -!).attr({stroke:\"none\"});setTimeout(`4O+ct.remove();});}`%}%`!U3;}var preventDefaul`2}-`!t#turnValue = false;},` R$Touch` H,`'2#` Z!originalEvent.`!\"*();}, stopPropag`,9$`!3.cancelBubb`-g!tru`!@!stop` sK` +`!=\"get` 9!Posi`!3-e`,c$crollY = g.doc.documentElem` u!` ;!Top ||` <#body` .&,` \\#X` D;Left` O1Left`,Y${x:e.clientX +` r$, y` .%Y` .%Y};}, add`\"@! = `%o*if (`!&\"` ;$Listen`/@!`!(#` G&obj, type, fn, e`!u\"`\"}#f`#%1pos =`#T-(e`.e%fn.call(` f#, e, pos.x,` \"!y);};obj`!O-(`!A#,`&r\"`0k#upports`%S\"&& touchMap[type]`!M#_`!<K, olde = e;for (var i = 0, ii = e.targe`(+\"es &&` #,.length; i < ii; i++`#w#` ;+[i]` *# == obj) {`!6!` 6-;e`'Q* =`!b!;e`({+ =`)d);e`'x, =`(c&;break;}}`#]]`#l), _`$?&` s%`&](obj`,^#`$B`` U5`!G+`!C.`+Y\"`-z&`(6'ttach` k!`'Y_`$o!`*D\"win.`$;!`3}!`*i~`+G@, x`&P!`+R0` .'`+]'`&D0` \"-||`1(+`&Y1` \".||`0;,`+#8x, `&o&`$L&(\"on\" +`$=$)`#j!detacher`0V,obj.` 6\"` F2`%a*` '#` e$;};}})(), drag = []` %\"Mov`3E!`+k-`#a)`#U+`$i$`$R~`%4<dragi, j =` &!`,n$while (j--) {` ?!` :#[j]`)[2e.`)E!es`\"X#`-#`-`),`*1\"` v$i` y!` .!` A([i` }\"` *!.identifier =`!=\"i.el._`!i!id) {x =` r\"`#e$;y` #+Y;(`.*,? ` \",: e)`'N+()`-t%`+5#{`'p,();}var no`0j!`!W%node, o, next =` ;!.nextSibling, par`/]\"` 6!` (\"N` Q!display` 3$style.` .#;`+W\"opera &&` _#`-S#Child(node);` I. = \"none\";o`!^(paper.ge`%p$ByPoint(`)-\"` R1`!53(`\"G!?`!L$insertBefore`!P!`\"g\") :` :$append`!k');o`&'!ve(\"raphael.`%$!over.\" +`!m&id`'6#.el, o);x +=`(,$;y` #&Y;` ].move` W4move_scop`/G!` y&x -`&O,x, y` $.y`,I\", e);}}` q\"Up`*s-R.unmous`$U!(`+C$)` .$up` /!Up`,o\"`)8$`(a%`!0!`(\\*`)[)i];`!J* = {}`\"Y/end`#A5nd`\"Z,start` %,`\"{4e);}`-f%;}, elproto = R.el`!L\"for (`+)%vent`+'$; i--;) {(`#/'ventNam`#=![` %%] =` u$` '*` P&fn, `!_!) {if (R.is` 0!\"` <$\")) {this`4V\"s = ` \"(|| [];` ''.push({name:`!,%, f:fn, unbind:add`1(\"` S!sha`\"~\"` )!`+/!`.s$,`\"V\"` \\#`!d$` D$)});}`1Y$his;};R[\"u`1|!`\"P1` (0`\"`(`.#`\"/6, l`#-`&G#l`&I!if`$'#s[l].name =` J#Name`*l!`#Z&undefined\") ||` q#[l].f == fn)) {` +&`#7\"();`!9#splice(l, 1);!`!H) && delete`!x(;}`\"+})`!e$i]);}`# #.data`\"m)key, valu`4_$` ;#eldata[` ~!id`#d\"` $*|| {}`1t!arg`4$!`!Z%== 0) {`!K#data;}` 141`&s(`!I!\"object`&y!`(B'in key` G#key[has](i`';%data(i, key`\"N\"`\"d*`*[*ata.get`*d!`\"+#,` $!,`!j![key]` f!);`!{'` 2!;}` \"% =`#5\"`+W+ata.s` g2` J!` k*`$J#`$:$`24\"D`$5/`\"Q& == null) {`$6.{};`4T%` .+`%q&` ++`\"2#`!<2get`!>-`$t&clone(`%H1)` Z'hover`)F*_in, f_out`*V#` ,!` $\"out` y&` q!`0@!over` R#` J$)` 3#ut(` _)out ||` =&`!A'un`!19`!*+`1Y#`!2%`1i%`!*%);}`1o!draggable`/z\"`)+%`0,\"`!\"&onmove, on`0y!, onend, `0m&, `1-', `1O%) {` ^%` =!`3U!(e.original`.W!`+l!).pr`*]!Default(`3R\"x = e.clientX, y` $'Y, scrollY =`.~\".doc`)l!Element.` :\"Top`/>%.body` .&` [$X` D;Lef`!b!` S-Left`1.\"`4`!.id`!]!identifier`+m!supportsTouc`-M!e.touches`,_#`3W!` ,$`+g#, ` +!`/N$i`/Q!` .!` A([i`2M#`!5'` 6!`!4,` %, =`0o#` Q$) {x` T%`#k#;y` #+Y;break;}}}` R'x = x +`#E$`!E(y = y` 2%Y;!` 1!`02&R`(*\"move(dragMove`(=#up` -!Up);` P!`4i\"el:`,6\"`&U&:`&S3:`&`2:`&w%});`'N#`$0!ve.on`-N'rag.` 9!`-M*` L#);`(3\"` B5move` J,move);`(\\!` ?5end` G,end`3h!`!;>`\"H' ||`\"z' ||`/N#`(g%`$H&` +&Y`$B&, e);`$|'`/'\"`+-%`$ ,`!5!`#m\"})`%=\"`$b!down(`#+#`.|2onDragO`,s-) {f ?`\"o2over`\"L*f) :` G!unbind` 1;`.3*`-#-`)M'`\"G&`'0\"`)C*if `'!!` =![i].el`(r$) {`.f(`\"c!` >)`\"n#` w&splice(i, 1`%*!`!u2*`!y+}`(o!`!W(`(s\"`!1#`(l+` /#`(n+`/}\"};paper`\"p\"circ`07!`\"l&x, y, r`\"v#out = R._engine` J#(`%H\"x || 0, y ` \"\"r` #!`%Q#__set__ &&`+|#` *\"`&,\"`1]!`%b#out`!G*rect`!@/w, h`!?6rect`!@3w` )#h` yjellips`#(1x, ry`!R3` P#`#+4` )$r` ,\"`!%]path`!V)pathString) {` #& && !R.is` 6', s` B#` */[0], array` :!` /' += E);`\"I0path(R.format[apply](R, arg`4-!s),`(q\"`!w\\imag`$-*src, `& &`$'3` S!`$5#src || \"about:blank\"`%~<`!8]tex`'i0text`!\\3tex`'b4Str(` U!` v]se`!H*itemsA`%8\"{`%U\"` )&, \"`%T!\"`%T\"` /& = ` #!.` p!typ`-G$.call(`%=%, 0`%I'`-*#)`&'(new Set`!<(`!{Dout.`\"5! `/=\"` ,!type = \"set\"`\"I8Star`\"\\*set`/}$`!4$= set`4$$.set()` X-Finis`)L*` d\"`\":&` h(;delete` &*`!L8iz`'q*width, height) {` U#`%}&` N#`$$\"`&)\"` F*` s-ViewBox`.r5fi` q4` S#`! (` I,`('+op = ` &'bottom = null`0P)`3@\" = R`%j!getOff`'/,elem`#e#b`!}!elem.getBoundingClientRect(), doc` <$ownerDoc`&v!, body = doc.body` F!Elem` -#d` B#Ele` J\"c` s!T`\"*!` D#.` *&||` l!` $*0` N$Left` F-` 0!` J*` +$0, `#2\"box`#=!+ (g.win.pageY`\"p#||` i%scroll`!=(` )%) -`!r&, l`!E\"box.` &!` m)X` b4`!_)` )&` r&Left`'N${y:top, x:left}`%B*ge`#K$ByPoin`-a.`$w#`*=(, svg`%$.canvas, targ`%Y!g.doc.e` q\"From` t!` g\";if`\":$opera`+Z!` T!.tagName == \"svg\"`!2#so =`&S&(svg), sr = svg.createSVG`&<\";sr.x = x - so.x;sr.y = y` *\"y;sr.`)H! = sr.`)K\" = 1`'^!hits` q#getInterse`\"f!List(sr,`(A!`!\"hi`.'& {`\"T%hits[` 1' - 1];}}if (!` @\"`*\"&`)/!}while (`\"`#parentNode`\"r& !`!P\"` .*` h#`)b$`!:(` [-;}` 4$`$I*` d+`!>#`\"_\");` a+`$B'`*v$?` g#getById`!}$` 8#id) :`\"@\"`\"K#` :\"`&L4sByB`-9,bbox`%C$`!F\"`0\"&` &!forEach(`,)() {if (R.isBBox`$b%(el`!f!Box(), ` w!) {set`22\"el);}}`4U%s`!X/ById`4T*d`-8%`!V%`.0\";`$s#bot`!J#bot.id == ` Q!`! #bot;}` X\"bot.next;}`%V)`!8(`\"I#`!7)callback,`!:!Arg`!)F` S$`0Z&Arg, ` <!=== false`!b&this`!T5` 7\"`$c5`+D9`$LMel.is` n!Inside` g\"`$M=` a%x_y(`\"**.x + S +` '\"y;}` @(_w_h` 2:` Q(`+_\"+ \" \\xD7 \"` 0$`+l\";}el`\"z\"`!u)`\"c4rp`\"r$realPa`,Z!getPath[` 3!type]`$X!`,7\"` 0!attr(\"transform\")`){!` %1`,_&`!'!R.` 3%Path(rp`&3\"` G.)`%3%R`!~*` O%`/q\"};`\"J$`)8#`(U*sWithoutT` r$`$}#`\"N#move`(E'{};}var _`\"o$_`\"P!` M5_.dirty || !_.bboxwt) {`#)E` L$`2N!thDimensions`!X$` ]\"` ?&.toString =`%s$;`!=$= 0`#+%` H$;`0:!`!\\'` $#T`!h'`!y/`!0* {`!]'0`(Y\"`!z@}` Q\"`\"+.map`$a!` ])`%B#matrix)`\"K$`\"3:`\"%%`\"J.`%N'clon`'u*`%(9`-S\"var ou`+1%`+u!`\"7()`'-\"`'3&)`+R#__set__`(*%` )#`+3\"out`+-%out`'2(low`!W)glow`!\\(type == \"text\"`!_,` X#` \"!|| {};`-6! = {`+-!:` k!`+7#|| 10) + (`+6#`)8\"stroke-` ?!\")` A!), fill:` X!fill ||`/3\", opacity` 6\"` &# || 0.5, offsetx` 6#` '\"` 8!` 3$` P$` '\"` 4#color` 3\"` &! || \"#000\"}, c = `-8$/ 2, r`$0),`$H#r`/`\",`&P!`,n-||`&v6` E(`&a\" ? `'$$p`&r- :` ~!;for (var i = 1; i < c +` &!++) {ou`0P#r.p` c$`%x#{`#n\":s`\"J\"`#g#s`#e\"? ` 0# : \"none\", '`$>#linejoin':\"round` ,+cap` '/`#7!':+ (`#>&c * i).toFixed(3)`$o&` C!`$q$/ c` :(}`.v'out.insertBefore`#I\"`/d\"late(s`%:$, ` #$`/5!var curves`0@\"`'*!},`$=!ointAtSegmentL` :!`(2)p1x, p1y, c1x, c` %!2x, c2y, p2x, p2y, `17%if (` '\" ==`(Q!`(Z&bezlen` MC);} else` U%R.findDots`!h%`!6EgetTatL` tE`\"(%);}`#$\"`\"v\"Factory`2N+total, sub`&O!`!i%`#<'`'R!` n\", onlystart) {`('#path2`$@!`'2\"`$P!x, y, p, l, sp = \"\"` }%`$]$point` v!`.y!`(9)0, ii` v#.`!7\"`(R\"ii`(M$p` 8#[i]`3!p[0]`-:!M\") {x = + p[1];y` $#2]`$/%l`1O#`%q/(`!t#[1],` P!, p[3` '!4` '!5` '!6])`!1!len + l >`%z*`\"D# && !`\"O$.`#0%oint` __`$/$ - len);sp += [\"C\" +`#X\"`!-\".x`#f#` (#y` '$m` 0&m` ,&` +%y`#S\"`%((`%X#sp;}`\")* = sp;`%&![\"M`!7&` c& + `!N(n` 1&n`!2&end` .&end` 5!`\"H%].join();`#q!= l;`$$5`$}&6];continue;}if (!`'`#`$!(`#+w`\"|#{x:`\"Y%y` $#y, alpha` (#` '!};}}`!p;}`$s\"p.shift() + `#t(end`#x\"`\"*$`\"H$?`(w!:`)0$ ?`)8&:`+1`\"K$0`\"6!`\"=81)`!(\"`!~\" && (`!6$`\"0E`#\"%` 0!;}`/i#getTotal`/J%`,S,(1)`/v(` .6` A\"S`\"M#` .80`\"-!R.`!(0` &'` <\"`!%/` &)` @\"`!0#`1E*`.'!from, to`+$#this`!*+`.#\" - to < 0.000001`)5&`\"#/` q').end;}var a`!Y\"` 63to`\"R!`/k$rom ?` :1a` n' : a;};elproto`# .`\"D&) {var`.! = `\"?$Path(`-v\"!`1')`)g\"` B!node` k+`\"S&` *4();}`\"v&`#=-`!f*`$T,`!l&`/>%`!NJ`!0&` n)`32)`!4,`%O,`!*'`'[\"` ;\"R._` &#[`!D!type`.t\"` &%`2S!text\" ||`!i\"` .%set\"`#V+` h#`4Z&`\"4$this`#=&path`!e*`'A0`'D'`\"^Q`(B(`(2,`*l#ef`\"b!easing_formulas = {linear:`!D&n`%_&n;}, '<'` (2pow(n, 1.7)` C!>` *:0.48` C\"<` 9-var q =` ?! - n / 1.04, Q = math.sqrt(0.1734 + q * q), x = Q - q, X =`!%!abs(x), 0.333333333` \"#) * (x < 0 ? -1 : 1), y = -` ]$Y` Z'y` K6y` U,t = X + Y + 0.5`*i$(1 - t) * 3 * t * t + ` \"\"* t;}, backIn`\"\\/s = 1.70158` g$n * n * ((s + 1)` *!- s`#Q!backOut` [+n =` ?!1`%A!` LD+ s) + 1;}, elastic` i+if (n == !!`%b*`%)'2, -10 * n) *`$_#in((n - 0.075`#G!2 * PI) / 0.3`!-%bounce`\"`37.5625, p = 2.75, l`(:!n < 1 / p) {l = s`\"5#n;} else`!o$< 2` ?#n -= 1.5 / p;` F)`$d!75` H.` G\"` T$2.2` E493` T'` F#6` 4684` K!}`#:$l;}};ef.easeIn = ef['ease-in']` )#<']` <$Out` 9(out` =%>` <&In` 9+in` >)<` F\"['back`! &.`'!\"` 0&` J&` 6!Out`&<!animationElements = [], requestAnimFrame = window.` /'` T!` 9\"||` 8$webkitR` ';moz` #<o` C=s` )5`&]&callback) {setTimeout` -%, 16`)H!`\"_%`1J0Now = + new Date, l = 0;for (; l <`#6..`2Z\"; l++` `#e =` 6.[l`22\"e.el.removed || e.paused) {continue;}var ti`$)!Now - e.start, ms = e.ms, `0M\"` *!` %\"`0w\"` ,!`1 \"diff` *!diff, to` (!to`-Q\"e.t, tha` &\"el, set = {}, now, ini` ($key`!w#initstatus) {`!]#` *) * e.anim.top`!v!prev) / (e.percent` ,'* ms;`\":!tu`\"6\"` a&;delete` &*e.stop &&`#y/splice(l--, 1)`*;%` t'(`!2\" +`!64(`\"/!/ ms)) /`\" ';}if` 7#< 0`$B)` .'ms`%>#po`\")!`$4!` i'`&&\"`)M!ttr in`$O!`/1#from[has](attr)) {switch (availableAnimAttrs[attr]) {case nu:n`'6#` `!` 8! +`!A!* ms *`%H!` 2\";break;` T!\"colour\"` X#\"rgb(\" + [upto255(round`!T\"` X!.r` _4.r)), ` ?5g` ?5g` ;9b` ?5b))].join(\",\") + \")\"`\"/)path`\"2$[]`#q&i = 0, ii =`#$'`*!%i < ii; i`*-!now[i] = [` A&[i][0]` g'j = 1, jj` e)[i` k&j < jj; j` j'[j]`$D+` .#`\";3` 8\";}`!J%` #\"`\"^\"S)` 4!` 0\"` *%`\"c(transform\":if (`$N(eal`!P\"`\"'`\"8j`\"FX`\"PL}`*Q$var g`,p!`/b&i) {`4c#`#k+` \\7;};`\"v#[\"m\", get(0)` \"\"1)` \"\"2` )#3` )#4` )#5)]];}`)')sv`#}\"`*_!== \"clip-rect\"`#{(i = 4;while (i--`#\\(`!PH`!6#default:var` X!2`!!![concat]`))')`\"E$`!=\"that.paper.custo`+|!ibute`+#`$[$`!N82`!E<`!^\"}set`,W#`'D!;}}`!:!attr(set);(`$`'d`1{\",`0&!) {setTimeout` ;') {eve(\"raphael`/K\"frame.\" + ` V+;});})(`!0!` 1&`0!\"`0h&` t'f, el, a` hSel`!\"!` ]\";` 7/inish` 8/R.is(f, \"`!#$\") && f.call(el`\"\"$e` ,!back`!},`#O&to);`3-=if (e.repeat > 1 && !e.next) {`*@!key in to`2*#to`2)\"key)) {init[key] = e.totalOrigin` 0!;}}e.el`!L\"init);runA`!O$(`!q\", e.el`!}$`4[$s[0], nul` 5!` o', `!j%- 1)`4F\"`!o\"`!y#stop) {` n9next` `;);}}}R.svg &&`#d! &` \"\"`(U\"` .$`(b#safari(`#c0`(g\" && requestAnimFrame(` D%);}`3*%`,q)color`,x&` *! > 255 ? 255 :` -#< 0 ? 0` ($;};elproto`\"V!ateWith` n)`&S!`\"r!params, ms, easing, `&=$)`.P\"e`\"(\"`+9!is`%v\"` -\".removed) {` L$ &&` W%`'7$` G!);`\"!#` )#;}var a =`!<# instanceof `$U% ?` 6$: R`\"'#ion(`!Y9, x, y`&C*a,`!0$, a`&51`\"+#`'-!))`2C\"var `3/(`$i4`32,`0+!` <,[i]`!}! =` c\" &&` _.[i].el == el) {` -/i - 1].start`!?0[i` 6#;`._#}`#h,;`%?%CubicBezierA`-*!(t, p1x, p1y, p2x, p2y, dur`'&\"`%L\"cx = 3 *` F\"b` &$(p2x - p1x) - cx, ax = 1` (! - bx, cy` R%y, b` &$(p2y` V!y` V!y, ay` S$y - by`!l&sampleCurveX(t`(7&((ax * t + bx)` ##c` $\";}` U&olve(x, epsil`\"+%t =` 5\"` r#` 6'`'2$((ay` y$y` x%` $\"` q,` T.`! #0, t1, t2, x2, d2, i`&H\"t2 = x,`&L\"`&-\"8`&*$x2 =`\"J+2) - x`)J!abs(x2) <`!\"'`!m#t2;}d2 = (3 * `\"{\"2 + 2 *`\"~$2`#\"!` `%d` d!0.000001) {`&!#`!Z!t2 - x2 / d2;}t0`!f!t1 = 1;`!z\"` c!t2 < t0`!<'0`/)\"t2 > t1` .'1;}while (t0 <` 6\"`\"91`\"?' - x`\"55if (x > x2) {`!]!t2;} else {`!f!t2`\"'#(t1 -`!c!/ 2 +`!a!` _'` $#`%k\"t, 1 / (200 *`('&);}`.E$o`+f&`.@)f) {f ? eve.on(\"raphael`2O\"frame.\" +`.8!.id, f) :` H!unbind` 1<`&v%`.y!`*3'`-3'`/f!ms`&]#`-7$ = [], newA`,3! {};` p!ms = ms` '\"times`$|!`,o$) {`-V%attr in`,#!`-/'[has](attr)) {` {#[toFloat` 2\"]`,W#[attr];`!M$.push(` =));}}` 6%sort(sortByNumber);}`!c!`.6\"`\"&$`!u#op =`\"G%[` \\%`41#- 1]` C\"`\"h'` #$;}`#8%.`$|!type.del`,5!`$u&` -!`#S#a`!;\"`#p'`!R%,`$J\"ms);a`#K%`#W&;a.del = + ` }\"|| 0`$r$a;};`!90repeat`!A)` h!` K`!3\"` ,!del`!T'math.floor(mmax` w\", 0)) || 1`!S(`!9%`3i*`!&!`3q%`#T#, status, totalOrigin, `!s$` ?# = `%>$` +#);`'9!arams, isI`!%!, ` \"$Se` z&`'S&xt, prev` z#tamp, `']!`(k!ms, fro`'y\", to` \"#diff`(-\"if (`!k\"`'q$`.}!, ii`3m0`&6#`/;\"ii`/:$var e`4:3` ~!e.el.id ==`#)$.id && e`'T#` V\"`(~#e`'+$ !`'*%) {`!<.splice(i, 1);`\"'`*-!`-O$` 0$ = e;}`!7$attr(e.`$:');`0&#}` R$`\"s\"`'3!to;}`*o%`\"s,`(y%`\"h3`+1$` ?%[i] =`\"2% ||` Y*[i] >`&!# *` 6\"top`%r)` A,;prev` &.`*b!`)*\"`-2# /` h% * `&W$ -`&#!);nex` l/ + 1];`&x\"` 6$`,{!` <#]`#4$`#4\"`&))`#e)` P&`!q,]);}`2;!!`!!\"`2P%`2P\"!`$_$`.m0` I%if (` '\"`.w*`#}!vailableAnimAttr` 8( ||`&q%paper.custo` C!ibute` b+fro`/V# `'I&`\"<\"ttr);` 4(= null && (` J)`!G&`!H!` 2!);to` 8%`\"##`0^\"switch`!r1` T\" {case nu:diff` \\%(` h%-`*g!` F$/ ms`$O#` S!\"colour\":`!W)R.getRGB`!o'`,D\"toC` K!` 8(`!\"$);`!5){r:(` G$.r`!:).r`!C\", g` 9'g` 6*g` <$b` 9'b` 6*b` >\"}`\"%)path\":`.2\"th`/q!path2curve`\"'', `!q%, toPath` D#es[1]`$`)` /$0]`\"7*[];`-n-`!U'`*c2` X&`*m! [0` _#var j = 1, jj` ])[i` c&j < jj; j` [.[j`%5#Path` )#`#%(` -\"`%?#}}`#*(transform`#5\"_`'V'_, eq = equaliseT` C$(_`#9-`/r\"q`(;-q.from`'[(eq.to`#=-` &&.rea`4`!rue`#!``\"X*0]`#m#`#*b` %)`#W7`0|$var `1V!`#\\#matrix || new M` )!, to2 = {_:{`$5%:_.` #%}, getBBox:function (`-x%` s%` =#(1);}}`'H*[m.a, m.b, m.c, m.d, m.e, m.f];extract`%&&to2`% (`$c'to2.`!P'`$i+(to2`\"=#.a - m.a`*6$` /(b` 6!`*.#` -*c` 6!c` '0d` 6!d` '0e` 6!e` '0f` 6!f` 5\"];`')csv`( \"valu`+<!Str`0z$`\"V\"[split](separator),`%1!2` F#`-n'` 9.`(@!`2!!== \"clip-rect\"`(F,` l!`(2-`'w$2`&z$while (i--`'e/(`!~\"[i`&g-`&l%`$\\'` G\"`.(#default:`\"]%[][concat]`\"_*`&0!2` 3*`0N(`!n0`3X;`*.)`\"#:`\"7'|| 0) -`3l\"2` )%`\"B$`\"1\"}}}}var easing`3h%.` *\", easyeasy`2?!` /\"_formulas[` *\"]`$]!!` B$) {` J'Str(` A\").match(bezierrg`-W#` C$&&`!#%`\"C# == 5)`*y\"`1j! =` :%;` x'`*E&t`*F&CubicB`!&!AtTime(t, +` b\"[1]` \"&2]` \"&3` -'4], ms);};`,?$`!('pipe;}}timestamp`#.&start || anim` $&+`,f!Date;e = {anim:anim, percent:p` \"\", ` m%:` w%, ` e!` '& + (` }!del`$q\"` >!tus:0, init` '#` /\"` >!, stop:false, ms:ms`${!ing:`\"4$`)r\":from, diff:diff, to:to, el:`'$#, callback:`\"S#` ($, prev:prev, next:next, repea`!|#`\"x%` )!, origin` t$.attr(), totalO` 6\"t` \"&};animationE` L\"s.push(e`&.\"`\"M#&& !isInAnim &` \"'Set) {e.stop`3L$e`$E#=`$@% - ms *`#P#`,@\"`!&-`' &1`&R&` >%();}}if (`!(.` {-e.`!\"(}` n9 && requestAnimFrame`!S&)`')%`!/$.`%G& =`\"(%` 5$`!:/` 3%`!G)eve(\"raphael.`'V&.\" +`,Y%id,` $$,`\"d!);}R` H!`!W!`)b)`%s\",`1I!`+h$`%~$) {if` ;$ instanceof A`\">%`#g%` ?\";`#e!R.is`+j#, \"`!$$\") || !`,\"# {` ~$ =`!(% ||`-=$|| null;`-K%` )!}`!E#= Object`!U#);` /!+ m`) \";var p = {}, json, attr;for`2@#i`!j$`\"7)[has]` <!) && toFloat` *#!=` `!` '.+ \"%\"` 7$) {js`#_!`'N!p`0}%`1T(`&k#!js`#7(new`#P&`$.'`&\"&`\"f#&& (p`0\"#`.j\"ing);`#=%` :\"`#M/);` v1{100:p}`!%#};elproto`%k#e`%DH`2\"!`&U\" = thi`)o\"`&r$removed`%2(&&` W%`!n!` D$`!c%` )#;`2~!anim`#D%`&^1 ?` 6$: `'j'`![:;ru`)'!` E\"`0.\"`(U).`03#s[0],`&g!` 8%`-c#`!^.`#?%se`2Y!`#;)` ~\"value`&\\#`\"6!&&` .\" !`'l\") {thi`2.!tus` O#mmin(` B!`!V#ms) /` \"%;}`!C#`#z!`!<'`+u$`!.4var out = [], i = 0, len, e`$a!`!J,`#\"/this, -1`!W*1`\"t&`!N\"`'I#len =`/!&`.8+`)i\"; i < len; i++) {e` ?0[i]`&C\".el.id =`&W\".id`()!!`#X!|| e`%M! =` ]\")`#p'`)>&`06!`.H!out`2/\"{anim:` W\",`.e#:` @$}`0z$` Y*0`#%out`(k(paus`%;.) {`\"K!var`#u\"`\"S\"`\"d5`\"g#`2^1[i]`\"I;` >1`\"g0`0~.`!z!`1+!` s#`%.$` [5) !== false) `#F!` 6-` p!d`-n$}}`',3resu`(V/`\":~`\"kM`-D!`&m9`#H-`\"7\"`#F0`&S\"`#9*delete e`#4#;`*{(`'!$`&z$)`#86stop`\"N~`%ttstop`%h.splice(i--, 1`#$/`\"%stop`4A(per`\"Co` s! == ` y$`!N@eve.on`# &`3o\"\",`!f*);` 8,clear` 4/`%c#toString`%a)`-%&\"Rapha\\xEBl\\u2019s object\";};var Set` N)items`28$` (!`14!`'C\"`\"}\"`#@!` +!type = \"set\"`(Z!` V$`#d*, ii = ` 8!`#]& < ii`#c(` ;![i]`&c!` %$.constructo`#n!`\"P$` +(||` v\"` </Set)`\"'#[`\")&`!>#] `'t$` Y!` +1` q$`\"U(++;}}}}, set`!I! `!$!.` '!type;` 1$`1c!`$+,`\"s!tem`4a!`3I\"`\"{,argu`&g+`#!*tem` ;(`,W$` 3!`#7$`\"pG`# 3len`\"v)`!F$`#J!len`#2+` +#item`\"~-`)c*`\"v&`,y+`&R$`&C#&& `.+#`$X&` 5\"--];` i'`!Y#pop()` t(forEac`#u*callback`,1\"Arg`&y4`\"G.`'#/` f$.call(` k#` u\"`\"p#i], i) =`,x'`\"a)`,],`!C$method in`$R$`.I$` (\"[has](` ?\")) {`\"L$[` -\"] = (`\"K&` /\"nam`!4'`&e-arg`&*(`#T)`#C#` h'el) {el`!)#name][apply](el, arg);});};})`!_$;}}`$2%attr`$+)name, value`\"C#name && R.is` 8#array)` ()[0], \"`,p#)`$a(j`$g\"jj = name`$^%j < jj; j`$b!`$A'j]`!S!` r\"j]);}} else`%*T`%:)` q&`\"?$`'6`/r!`(+,while `&@!`!)#` {$`'r#` V'`1<\"`/N*ndex, count, inser`0h! {` 5! = ` \"\"< 0 ? mmax` ( +` 9\", 0) :` &\";` g! =` G\"0, mmin` H)-` M$` E!))`10!tail`0r!, tode` $$arg`1(\", i`(!\"i = 2`4J#`-A.`#]!args`.B!(`-B();}` X%0` [\"`!<!`$/%odel` Q\"`+U!`\"S\"+ ` P&` O\"`!x/` Z%ai` I5`(f#`-z\"`!]!`!n#`!E,` >#+`\"n!`\"++`%p(` t%`&N#` $*` _'?`!*![i] :` o![i -` 5#];}`&1` ,$`\"?$=`#%\"` O%;`&L'[i]) {`.h(i++];`'>$new Set(`#Z!`.\\)exclud`&b*`+<!`(^6`.:6`!C#`2/\"`#\"$`'k\"(i, 1)`,Q%rue;}`(+(animat`!:*params, ms, easing, `/O$) {(`+e!` 1$\"` P$\") || !` 2\"`,-!`0#% =` a# || null`'q\"`383,`\"V!len,`36!, set` A#, collector;if (!len`0O,`!,%`!=!` G$`++,!--len &&`\"1%`1k\"set`/!!`!i#=`.1\"`\"E$str`\"8!?`\"($:`!F'`'`!nim` P!`#R\"ion`#82` M$);item`\"Y)[--i]`$0$(anim)`&J$i--`-t, && !`.(*removed &&`3h*` l$With(`#T\"anim,`!p!);` OD|| len--`'\\%`.{,`.#\"Afte`#h*`&v!`'R$`08.`\"*6` f((`(c!`!#3getBBox`$|,var x`.\"#y`.*#x2` )$` #\"`,$\"`!D6 i--;`)(#`\"c2`!\"#b`!;!` 4*`!Q#();x`-\\\"box.x);y` $&y);x2` 2' + box.width);y` 0'y` 4#height);}}x`1,!in[apply](0, x);y` $.`! !`1W#` A(2);`\"b!` )+y2`+0%{x:x, y:y, x2:x2, y2:y2, `!\\!:x2 - x, `!N\":y2 - y}`4R*on`+X*s) {s`##$paper.set()`#|*`-5)`$#+`-;)`2N#`#k*`!1!()`%h&`%b)toS`*+!`%`,` E#\"Rapha\\xEBl\\u2018s set\"`&D)low` Q)glowConfig`%H#r`,S%`\"7)` ,!forEach(`\"i'hape,`3R\"` Z#g = ` 2!`!(!` u(`-=!g !=`.-\" {g` Z42` l#2`\"7\"`#$\"` 4\"`-(!}}`% %ret`))sPointInsi`1^+x, y`*($` 5,alse`\"+4`*a!if (el` p*` l\") {console.log(\"runned\");` u,`2&!`!n#`!+\"`!{'` @);};R.registerFont`!{)font`*0$font.face`%;&font;}`\"%#nt`'(%` '\"|| {}`/t!fontcopy = {w:` d!w, face:{}, glyphs:{}}, family =` O!`!*!['font-` 3\"'`,!'prop i`!@\"`!Q$if`!o\"` T\"has](prop)) {`!=$` 5\"prop]` z)` -!;}}if `(C\"`\"!![`!2\"]`.O$` ()`%f\"` q$);} else` 60 = [` @$];` !`#K\"svg`!K-'units-per-em'] = toInt`\".'` 3+, 10`*p'`#K!`\"q%`#X\"`\"p(` ,\"`\"x\"` '!)`'%#path`\"l$` >#` \"!];`!X%` *)`$j\"path.w, k`$h!d` *\"`3]!\"M\" +` q!.d.replace(/[mlcxtrv]/g,`&A'command`&4&{l:\"L\", c:\"C\", x:\"z\", t:\"m\", r:\"l\", v:\"c\"}[` U#] || \"M\";}) + \"z\"}`*b!`!<!k`#b!`\"{#k in`!R\"k`\"r#path`\"l\"k`%`)`\"C).k[k] =` S#[k];}}}}}`-x$`()\";`,f!`3k%`(b.`%M!, w`0*!, style, stretch) {` ## =` ,$`\"1!normal\";` J!` 7!yle` -)` k\" = +` u#`)=!` 8\":400, bold:700, lighter:3` /$er:800}[` T\"`#D!400`#:!!R`'F\"`$1%;}`*-$ = ` 6#`'a$` L\"`+1#var name = new RegExp(\"(^|\\\\s)\" +`*H#`%Q'^\\w\\d\\s+!~.:_-`%^!E`$h!(\\\\s|$)\", \"i\"`'b'fontName in`!D$`$l#`!R$`$q!` ?$)` 6#name.tes`(p\"` 2$`\"$,` 4#];break`%)!var the`%'!`(j$`&/(`4,(`(d!`3|2` \\#` >#[i`#3\"` /#`-,(`$+\"'] ==`$o$&&` 51`%M!` C\"`%T%!` .6) &&`\"\"$` 1*`&P!` a$`&n$`\"Y%`'f#`\"\\$`'c)pri`'_+x, y`'X!ing,`\"G!, size, origin, letter_spac` ?!line` &$) {` A\" =` I#`'m!middle\";` M* = mmax(mmin(` ,+|| 0, 1), -1);` w(` H*` -(|| 1, 3), 1)`2\"!` n\"s = Str(`\".\")[split](E), shift`%:\"notfirs` '#`.2#E, scale;R.is`%u!, \"` b\"\"`$*!` /!`3>$`+6#`&<\")`&A(` `! = (siz`%\"!16) /`3'(`02*`!y!bb`2C(.bbox`!t$separator), top`+>!bb[0]`$(\"H`+Q$bb[3] - bb[1]`\"H#y`\"<\"h`+s&` 9! + (`$S$= \"baseline\" ?` g(+ +`!H'descent :` 7(/ 2`*C'`(r(`$(#`(j3if `%>#s[i`'G!\"\\n\") {`$<%;curr` $!`$A(;`\"/#+=`!:(*`&j);} else`3.#rev =`%'&&&`34)`!;% - 1]`.4!{}, `!5#` 51]]`!?\" +` l'? (prev.w ||` Q\"w) +` .#k &&`!D!.k` [(`'r!)`#\\$w *`(~+ :`\"O*1;}if (`!W!&&`!_!.d) {`':!+= R.transformPath` D!.d, [\"t\"`'v$*`']\"`%U%` '%\"s\"`'x#`'#`&G!`%r$, ` _!(x - top) /` =$(y -` >#` -%])`+})is.path`4`!).attr({fill:\"#000\"`+{!oke:\"none\"})`,G*add`,F)json`0Z%is` +!, \"array\")`20#res`)`$set(),`&n)json`&q#, j`'9\"`&q,j` A#[i`%N#;eleme`1i%j.type`*r!res.push(this[` 3\"](`\"C#j));`\"c%res;};R.forma`.\\*token, params`!#args`2K!is(` 3\", `\"C!) ? [0][concat]` 6#) : argu`!a!;` l! &&` Y\"` x#`-O# &&` x!`\"^# - 1`1q\"` O!= ` #!.replace(`!h\"rg,`!d'str, i) {`\"3#args[++`*%\"null ? E`!J\"s[i];}));`%I$`! !|| E`\"f\"ullfill = (` v&`\"[#` G!Regex = /\\{([^\\}]+)\\}/g, objNotatio` :&(?:(?:^|\\.)(.+?)(?=\\[|\\.|$|\\()|\\[('|\"` 5\"\\2\\])(\\(\\))?/g, `\"I#`*T!`!@%all, key`!)!`&1)obj;key`\"~%`!>,`##(` a!name, quote, ` \"!dN` .!isFunc) {name =` D! ||` 9'`0z!res`'r#` <!i`&+!`#~!s = res[name];}typeof`!a\"= \"`!?$\" &&`!%#`%)!` O%(`&!);` +\"` 2\"`$_#||` _$obj ? all :`!2\"+ \"\"`$l$`'S\"` &#`%W+`#2\"` 5#S`&\\!(str)`#0%`${&`#\"-key` R&`$8$`$%+;`+-!})();R.ninja`$T)) {oldRaphael.was ? (g.win.` .# = ` 7'i`(c!delete window` A$`\"9$R`'4!`.o!set`,B!;`'1'doc, loaded, f`$L#doc.readyState`#W%&& doc.addEventListener) {` #0(` m%`\"3,` y\"move` :3, false);`!9, \"complete\";}` +7loading\";}`!0%isL` z!() {/in/.test`\";+) ? setTimeout(` K$, 9) : R.eve(\"r`#u#DOMload\");}` r&;})(doc`,r!, \"DOMContent` ;\"\");eve.on` U.`&#() {`\"f\" = true;})`$H(`$?#!R.svg`&J%;}var has = \"hasOwnProperty\", Str =`'K#, toFlo`/P!parse` (!, toIn` +%Int, math = Math, mmax =` /!.max, abs` ($abs, pow` ($pow, separator = /[, ]+/, eve`0B!eve, E = \"\", S = \" \";var xlink`!!ttp://www.w3.org/1999/` ;!\", markers = {block:\"M5,0 0,2.5 5,5z\", classic` +, 3.5,3 3.5,2z\", diamond:\"M2.5,0 5` D!2.5,5` O\"z\", open:\"M6,1 1,3.5 6,6\", oval` Q$A2.` V!,0,0,1` %!` a\"` &*0z\"}`!x$Count`/B!{};R.to`$=\"`(C,`*&#\"Your browser supports SVG.\\nYou are running `*d!\\xEBl \" + this.version;}`#f!$` z)el, attr`&8#at` \"%`.}#el`.~!`42\"\") {`,*!$(el);}for (var key in` Z,[has](key)` .#key.sub` g\"(0, 6)` y!`$z!:` x\".setAttributeNS(` 7!`.)!` P'6)`'Y!`! \"key]));} else` R-(`.c!` <-}}` F&`&w!_g.doc.createElementNS(`&_/2000/svg\", el);el.style`1n!` %$.webkitTapHighlightCol`(1!\"rgba(0`%W!0)\");}`%\"#el;}, addGradientFill`$?+`!R!, g` ;#) {var typ`-=\"inear\", id = ` G#.id +` I%, fx = 0.5, fy` \"$o` D'node, SVG` )'paper, s = o`\";\",`%U!`#.&get`#.#ById(id);`,K!el) {`!5$`,(\"(`!u%`3Y%R._radial_`!]'`3\\*_fx, _fy) {`\"G$` M\"\"`!$!_fx &&` :#`\">!`-)#(_fx);`\"F!` (&y)`('!dir = (fy > 0.5) * 2 - 1;pow(fx -`\"x\"2) + ` .!y` ('> 0.25`%1!` y!`-?!sqrt(` 3!-` L\"` Z') *`!-!+`!&\"&& fy !`#}!` \\&fy.toFixed(5)` W!00001` U\"`%b&E;});`#L'` #$.split(/\\s*\\-\\s*/`$!\"`#,\"`%X&`%q#angle` Q)hift();` 1$-`#+%` -!` g\"isNaN` *#`,=&null`1I\"vec`/z\"[0, 0`0k\".cos(R.rad` Q$` 1#sin` **], `1.\"1 / (mmax(abs(` o\"[2])`1?!` ($3])) || 1);` 9% *= max` )$3` (%if ` ^& < 0) {` )#0]`\"I!` 5%` c'= 0;}` P'3` L+1` N)3` R%3` T#`\"o!dots`(I\"`3d!Dots`'}&`(D\"dots`#?,`*#!id`(?%/[\\(\\)\\s,\\xb0#]/g, \"_\"` [\"`)d$`%M%&& id !`)|&`$~%id) {SVG.defs.removeChild` T-);delete` S-`\"m\"!` ?-`08%`&N!+ \"`,e$\", {id:id});`!Z-= el;$`1I!`'\"%`*7# ? {fx:fx, fy:fy} : {x1:`$P%, y` %%1], x2` &$2], y` %%3]`-n&Transform:`!C$matrix.invert()});`\"w%append`\"x$);`2M%i = 0, ii =`$|!.length; i < ii; i++`2.\"` W($(\"stop\", {offset:dots[i].` )\" ?` h!` &': i ? \"100%\" : \"0%\", 'stop-color'` Y%` *! || \"#fff\"}`2D\"$(o, {fill:\"url(#\" + `01!\")\", opacity:1, 'fill-` *#':1});s.f`1A\"E;s.` 3# = 1` 2#O` ''`'4#1;}, updatePosi`/?!`1z(o`+w#bbox`0l!getBBox(1);$(o.pattern, {` ##`$D&o`$8, + \" translate(\" +` r!.x + \",` &%y`\"F\"})`3[#Arrow`!G*, value, isEnd) {if (o.`&f%path`-p$` E!s`2'#` (!).toLowerCase()`.`#\"-\"), p`\"@!`3;$e =` }\" ? \"end`$m!start\", node` I!`3\"attr`3n\"` %!, stroke =` 4\"['` *\"-width'],`&m!`!W\"`&e#`(h$ \"classic\", from, to, dx, refX`!$\", w = 3, h ` \"!t = 5;while (i--) {switch `\"N\"s[i]) {case \"block\":` '\"`!$$` (#oval` $$diamond` 2%pen` $$none\":`!i#` y%;break;` =\"wide\":h`!T!` ,(narrow` 4\"2` -)long\":w` A-short` 3\"` I$default:;}`,w!`%1%`!_!) {w += 2;h ` \"!t` (\"d`1f!;refX`$n'4 : 1;attr =`)2$`\":!`$[$:`$\\!.` '\"};} else {` `#` p!w / 2` V*` I(` a%` s\"}`.k\"o._.`\"g!s`';#`'F$` /&.endPath && markerCounter[` 2.]--;` (*M` M!` <<` @\"]--`\"1%` 5'`'h!`!!=` =%`!5+` 3!`!#?` ='`!C!`!7. = {`#0#`%'!!=`&]#`*?#pathId `1F!phael-`!'\"-\" +`(r!,`!7#` ,5se` C# + w + h`4D!!R._g.doc.getE`1:\"ById(`!+\")) {p`1(.$($(`+~#, {`*R$linecap':\"round\", d:`!;\"s[type], id:` s\"}));`#!*` 2\"]`'=!`\"x$` -1++;}var`\"I# = `!k4`\"i$), use`\"I\"` .\")` s$ = `\"'!` (\"`\")!i`!m$Id`#L$Height:h` ($Width:w, orient:\"auto\"`,Y\":`,^\"refY:h / 2});u`.D!`!$!use`#.\"xlink:href':\"`2`!`\"9\",`0\\\"`0}!`(K\" ? \"rotate(180 \"`$[!/ 2`1*!\" + `!\"!`0p! \" : E`1D!scal`1>\"w / t`1:%h` &$)`4R\"`/)':(1 / ((` I$` F!) / 2)).toFixed(4)}`$R$`%F)use);`%X/`#j#`$~+`#Z$`$s8` ;%++`#;#`!0#`$}&sByTagName`#S#[0];}$(us`1j#);var delta`,e\"*`(u'`/w$ &&`(3\"!=`09#)`%h!`,G$from`2T!`*\"*` i!`2^#|| 0;to`&h!getTotalL`2P!(`-b\"path) -`!D#` P$`\"Q%` |#` /+` M@`.;'.end`!E,);}`/.$}`/;![`'e#`*m#] = \"url(`&^!`+:%`%[!`\"`!to ||`4B!) {attr.d`!M$Subpa`!D)`4f&);}$(nod`#%`!]&[`,%!\"Path\"] =`'q#` /.`-j\"` >!`!P$` 3.dx` <!`#4!` ,.Type` ;!type` -.String` <!valu`$%&`$Ftfrom`%$,` 4B`$e>`#I4&& `$\"${d:`$7>});delete`\"H'` e)` &5`$I$` (5dx` \"7`$F\"` &5`$D$;}for `!v! in`3e*)`$T\"`+9*has]` J!) && `/z#` 5$attr]`3\\#item`0S7`'A\"` @!&&` H!.parentNode.remove`--\"item);}}}}, dasharray = {'':[0], none` $\"'-':[3, 1], '.':[1` %#-.` 2#, ` )&` &*` 0&. ` T\"3` B! ':[4` %$-':[8` 1%.` 4#` V!` :#.` ;#` ')` '*` 4#}, addDashes = function (o,`(Z\", params`#L!lue =`\"V&[Str(` 1!).toLowerCase()]`+b!` 4\"`$#\"`1I!`).!`&|![`1X*] || \"1\", butt = {round:` 9!, square` &$butt:0}[` [,linecap` g\"`!o\"` *.` 8!0`$^\"`\"M![], i`+%$.l`)c!;while (i--) {` E\"[i`+J%[i] *`\"+#+ (i % 2 ? 1 : -1) *`!]!;}$(o.`)]#`!?$`#1%':` p\".join(\",\")});}}, setFillAndS`*d\"`$%+`$\"'r node`#O!`.k&s`#Z&, vis =` A!.style.visibility;n` \"0 = \"hidden\";`)N!var att in`!0&if (`#X#`)@$)` 0#!R._availableA`$9!` 7(continue;}var`#F\"`0G!` g!att]`2)!` %\"`/7%switch`*y!) {case \"blur\":o.blu`&Y$;break;` :\"href\":` &\"title\":var hl = $(` ,#);`!?#`*u(createTex`*^!` t$hl.append`*g\"val)`#;\"` *(hl`!7*target`!4\"pn`$!$`+V&`(?!pn.tagName`(T* != \"a\"`%\"#`!q$a\");pn.insertBefore(hl,` u!`!U-` /\"`!5!hl;}if`#?! ==`!R%) {pn.setAttributeNS(xlink, \"show\"`*^#` P!blank\" ? \"new\" :` 5\")`2S%` R5att` a#);}`\"v(cursor\":`&a'` -\"`%4%`#C)ransform\":o.` $%`%+0`1 !-start\":addArrow`,i%` 80end` 7/, 1` ?*clip-rec`$o#rect = `-;'split(separator)`$!rect`+S# == 4) {o.clip && ` $\"`1+(`1+3` 8-`':\"e`%T#clip`4g!), rc` .\"`!e!);el.id`'`!`'X\"UUID();$(rc, {x:rect`1u!y` %\"1],`,x\"` )\"2], height` *\"3]}` o!`&@(rc);o.paper.defs` 0)el);$`&j!, {'`#?!path':\"url(#\" + `!]\"+ \")\"});`\"w#= rc`'2\"!`0_(pa`0e!`%p!g`&F'`\"^\"` v!\"`#s\"path` N#` j#`*?%getElementById` E!.replace(/(^url\\(#|\\)$)/g, E));`$I$`$?,`$8(clip`\"53E});delete`%,#;}`')`!r!:if (o.typ`)-\"`\"&\" {` i%d:`)J\"?`*'!rs.`\"f#R._pathToAbsolut`,R$) : \"M0,0`#Q\"_.dir`/b!1`\"|!o._.`'|!s) {\"`(L!String\" in ` 4& && `(2(` 0&.` J');\"e`1s!` 4I` J%`))!`\"g*`&a!`*x$`$(`+G(`\"%.`\"u\"fx) {`--! \"x\";`1'$` 7\"x`,N%`!*\"}`!+\"x`#}\"` V'` O$-` Q$ -` 8$`!Y! || 0)` X$r` W&`$\\!rx\"`*?\"`$j%`)N\"`!(+cx`\"#>pattern && updatePosition(o`\"T,`2^)`)u!`\"kUy`#:&y`#4,y`#.2y`#7)y`#1/y`#:&`!Y\"`#6+` X&`#-\"y`\"By`\"Lur`)I-`!:$`)Q%rx`)U\", ry` $\"}`2q&`!9:}` 6src`!*-image\")` c/`4S'href`4V$`3g+stroke-`(u#`*_$sx != 1 ||`)X!sy` *!`$T%/= mmax(abs` H#), ` $%y)) || 1`/(\"`0!$_vbSiz`/1\"lue *= ` ,+;}`\"\\:`%%['`!o#dasharray']) {addDashes(o,`&6\"` 50, params)`!W$`,3~`,fK`$,/`!w%\":`\"9)`&>#`\"*$` N(fill\":var isURL = Str`0\"#.match(R._ISURL`#U\"` B!`'9(fill`'4%`(v#`38\"r`1 !getRGB` l#` ^!!clr.error) {`29#`!V\".gradient`2O$`*K\"` -%!R.is`*_#opacity, \"undefined\") && ` ?!` i#` ,5`!v%` ;#:` h)})` (['fill-` 5#']` v9` +>`!2%` <*`!<\"` O,`*g&if (`*'(circle\"`)7\"` -%ellips` 1\"`$_'charAt() != \"r`!B\"addG`#R#Fill`%`%)) {if (\"`!>#`&}!`!X! ||`%l\"`!X$` 1&`)l!r `$B$`%5!_g.doc.getElementById`\"X!.g`)r(`&_\").replace(/^url\\(#|\\)$/g, E)`%r\"` u$`!%#stops =`!+%` ~'sByTagName(\"stop\");$(` K![` Q!.length - 1], {'stop`#&`\"F0?`&d#` 3# : 1) * `!v\"`\"].` H$`$_,` S!});}}`'L* =`#v\";`2[#ill = \"none\"`(m$clr[has]`!U&`%r9clr`!e%> 1 ? ` &(/ 100 :` ((})`+A)\":`)l2`.G3clr.hex);`3z$` _# && `!Y=`,Y#`!?Y`/q#`!,,`.8~`.]Y`%9$\":`):~`$Z!`!1(`$&$:`3+%`&g&&& !`'=\"`$T\"`$5*\")`/w(`$O-`4V\"`$T\"` &\"`$L$`0A$}`15&` k%`!4/) {`*Q~`!\"\"`*Vu`\"J%`*!#default:`'%$font-siz`'*\"`$p\" = toInt` (\", 10) + \"px\");`3X!ssrule = att`\"<&(\\-.)/g, function (w) {return w.subs`&d!(1).toUpperCase();}`*2$tyle[` y#]`,/%o._.dirty = 1`*L4`&6)}}}tuneText(o, `2x\"` y(.visibili` p!vis;}, leading = 1.2, ` Z$ =`\"('el` f%`1d\"el`(?\"!= \"text`(=!!`4)$`':\"` 3!) ||` T#` 0#font\")` \"1`$.\"` *.x` =/y\"))`#`%;}var a = el.`'K!, node` +\"`(N\"fontSize =` 6!.firstChild ?`%4#`'e%`%j#View.getComputedStyle`'v\"` T&, E).getPropertyValue`\"#)`&'\": 10`'}!`\"r/) {a.t`#p\"` 9\"` )!;while `!\",) {` -!remove` .!` 2-`\"l\"texts =`,W!` m').split(\"\\n\"), tspans = []` &#;for (var i = 0, ii =` h\"`)3#; i < ii; i++) {` R! = $(\"` &!\");i`1,\"` *!, {dy:`#z%*`&N$, x:a.x});` B!.append`\"-\"`$%%creat`&p!Node(`!B![i])`'T#` H(` a!` f#s[i] =`\"*#}} else`!b#s`%=$`+\\2`!z$`\"a!`\"U)` X!`\"L3if (i`/ !`!6%`\"5>`!O$` L%0` N#0});}}}`/i%` L!, y:a.y});el`*r)var bb`'i\"_getBBox(), dif = a.y - (bb.y + bb.height / 2);dif && R.is(dif, \"finite`21\"`!G-dif});}, `#1#`*{)`!f\"svg) {var X`#6\"Y = 0;this[0`$.!his`)Q!`$ #`$b\"raphael = true` J!.id`1?\"oid++` :)` 5!` =#` F\"matrix` K!` %\"()` 3\"realPath = null` /\"paper = svg` +\"`+2!` l$` '\"|| {}` 8\"_ = {transform:[], sx:1, sy:1, deg:0, dx:0, dy` '!irty:1};!svg.bottom`1*!` $'`!(\"`!`#prev`!K\".top;s` \"\"` J%top.n`*}\"` O\"` 9$` *\"` ^\"` :#`\"B!}, elproto`\"o!el;`$L#.` 0!`/;!=` ;$;` D#.constructor =`$}$;R._engine.p`#C\"`%*&pathS`2>!, SVG`%3#`$c!$(\"path\");SVG.canvas && ` $&`)p)el`3e\"p = new`!5$`19!SVG);p`1/\"= ` q\";setFillAndStroke(p, {fill:\"none\", s` 3!:\"#000\", path:`!g&});`4B#p;}`\"Y%rotate`\"7)deg, cx, cy`*B#`&2#moved`1>%`$#\"}deg`.M#deg`.D$separator)`0 !deg`+@# - 1) {cx = toFloat(deg[1]);cy` &+2])` x$` +(0` E#`%4\"`%n!` k!cy`!(\"cx` 3%|| ` @&`$F#bbo`!9!his.`+5$1);` ^!bbox.x`+2!ox.width / 2`!U\"` /!`+N\"ox`+K';}` g!`(G%`#)\"_` '&.concat([[\"r\"`(Z!`#X$]])`$-%`#I\"`$0%scal`$**sx, sy`#pGsx`$3#sx`$\"3sx`$**s`#w(sx`$1\"`$@(sx`$1!`$>)sx[3])`!(#` *'0]);s`$:*` {!sx`#lp}` ^\"` R&?`$V4 : cx`!}\"`!<'` H#`$m/ : cy`+l\"`$_As\", `$>*`$l7` \\!l`)2-x, d`(yA`${$d`$k4d`$t+d`(y)`$}\"` a\"` *'0]) || 0;` I!+ dy` )\"`\";Ft\", `\" \"`\"=<form`\"P)tstr`%%#_`%!$_`\"'!tstr`%C'` n#`!6';}R._extractT`!Z), ` r!`!|\"cli`1&!$`!|\"clip,`2E(`3W'.invert()}`1q$attern && updatePosition` g!` >#node` v'node` i4}`$A\"_.sx != 1 || _.sy` (!`\"`#sw`4>)[has](\"`/i\"-`'S!\") ?` 6('` 1('] : 1`!T\"attr({` 1*:sw});}`$.2hid`&n*) {!`&g( &&`!4\"paper.safari`\"N&.style.display = `1h\"`,~5how` Aj` r5` k\"` },`)\", || `!;\"` z!parentNode`&X%;}`4c!aper`$?$` (!;`!\\\"__set__ &&` >\"` )$.exclude`%q#eve.unbind(\"raphael.*.*.\" +` n\"id`%d\"`-3\"radient) {` o\"def`!k$Child` 8+`(4!tear`(&#` P!` e'`\"-+.tagName.toLowerCase() == \"a\") {` ?1` T'`!8-`#,,;} else` S3` J1);}for (var i i`$i\"`!E#[i] = typeof` 3!` ,!= \"`$s$\" ? R._`$m#Factory(i) :`+H!`4\\#`%+$= true`%`'_`1\"#`%T5`&G0`'b%`\"?#show();var `(n#`! \"`2*'{};try {`28(` v!`2A$);} catch (e) {} finall` I&` \"!|| {};}`!!!`(G$hide(`(\"%bbox`\"5'attr`\"/)name, value`1#?if` L\"`4;+res`\".\"`$O%a`$P$`,A\"` o(`,o'a)` |!s[a`$q!` 4&a];}}re`'N& && res.fill`#g& && (` -&` =!`(G&&& delete` +);re`2/'`1.%`0p'`\"K#re`\"J#`\"x!`\"J$ && R.is`#2#\"string\")`\";#`\"u$\"fill`![!`\",&`!j/` 2'`*>'`#h'` .+`#t*\"`!k%\"` G+`2})var nam`$?!name.split(separator), out`$R+i = 0, ii` N#s.length; i < ii; i++) {`!F\"` =\"[i]`+d!` /!`%0,out[name`${+` .!`*}$if (`#X!`/m'customAttribute` K#,`*G')` j0` A8.def`,/%` N(R._availableA`!S(`3+$ii - 1 ?`#,!: ` T$s[0]]`%J?array`!h$`#f'`#],`#U3`!'$[i]`#9)` z![i]);`!`$ou`%d#`!I\"!`)a)param`)j#` &\"`\"X%` J!`$\"(`$f!` X#`!~,object`\"*!` j%name`/4'key in`!+#) {eve`2K&attr`2Q!key + \"`2V(,` $!,` S#[key]`0/$` m#`$k7`+b(`%13`+z!key`+!!` ~#` ')`&F=key`&U-`#W#`&E;key].apply`4=#[].concat(`\"C();`(<'key] =`\"c(`)a&sub`#^&`\"U#par`\"*\"` 7\"`$:&[` +\"` f#` &$;}}}}setFillAndStroke`!W#`$V#`-O$`0#\"`0h%toFront`2w5`0N7`2K&parentNode.tagName.toLowerCase()`-(!a`3W%` D,` T'appendChild` k1)`*O%` U1` J1)`-}\"svg`$l);svg.top !` 0\"`%p\"_tofront`#6#svg`# 6Back`\"iL`&<#e`#n!`!p0`.]!` )\"`#-<` ?#`\"U'insertBefor`%9\"` n,`)R\"`#f8first`#=!`#c%`!G'` 4&`#/$`#]\"`!D%`!,2`! 3` )`#w!back`#t#`$@&);`$K1`$!2`!;\"After`$/)elem`49\"`#@node = ` N#`!t! ||` ($[` 0$`/b\" - 1]` ?!`39\"ode.nextSibling) {`\"I,`\"n4` J-`'f%`'BDR._` w\"after`#D#`!h#`#F*`##8`!T\"`\"Fv0`#(#`\"LH);`\"3%b` 7&`!pIblu`%G*size`/q#`)H$`$^!+ size !== 0` 9#fltr = $(\"filter\"), ` i#$(\"feGaussianBlur\");t`/s\"`!+$size;fltr.id = R.createUUID();$(blur, {stdDeviation:`!>#|| 1.5})` Y\"`%,(blur);t`1a#defs` 2)flt` :!_`\"O$ltr;$(`$(\", {`\"\"\":\"url(#\" +` <!.id + \")\"}`&R&`%,!` a\") {` ##`$K(`%C\"`&f#` B#;delete ` )#` %&`\"n&;}`!F\"` Y#`3Z%`#Y&;}`$a$;};R._engine.circl`&q*svg, x, y, r`$K#el`$6\"` H\"\")`/l!canva`/g!` $&`#))el`+i\"res = new E`&;\"(el`0'%s`\" \" = {cx:x, cy:y, r:r, fill:\"none\", s`4%!:\"#000\"}` U!type = `!O$;$` t!` k%`'3%res`\"H)rec`49*`\"H'w, h`\"G.rect`!_t`\"Q!`\"P!width:w, height:h`\"c! || 0, rx:` \"%y` \"%`\"[D`!|!`\"XEellips`%=6x, ry`\"t+` M#`$[~`\"e\"`!>!:ry`\"9F`!Y$`\"=Eimag`\"Q/src`%a(`\"X+` P!\")` ~#`$_:preserveAspectRatio`\"\"#});el.set`*!%NS(xlink, \"href\"`!C!`%p~`!c.src:src`#N*`\"P\"`#14tex`(w5text`#4+tex`(+~ 'text-anchor':\"middle\"`!K\":text, font:R._availabl`.3!s.font`&@&`&R#`&`\"`&D/`\")!;setFillAndS` S!(res`&@@setSiz`&X*`$0!`$+$) {`4I!` 0! =`$L\" ||`4^\"` )!;` &!` I\" =` Q#` :%` )\"` >\"`#Z#`&L((\"` _!\",` f')` 37` i\"` G$`!]#;`2S!his._viewBox`!o$setV` ,\".apply` A!` V#` C%`1r&hi`\"q*creat`\"p*`&&#con = R._getContainer` }#0, arguments), c` 7$ =` R!&& con.` .%, x` 4\".x, y` $#`(5$` (#`$')` -#`#S#if (!` d%`\"T!row`'&\"rror(\"SVG`!:'not found.\");}`\"'!nvs`(5\"svg\"), css = \"overflow:hidden;\", isFloating;x = x`0B!;y = y` %\"`%W-512;`%I/342;$(cnvs, {`*V$` \"!, version:1.1`*z%`\"Z\"xmlns:\"http://www.w3.org/2000/svg\"}`%G\"`#i'= 1) {cnvs.style.cssT`+,\"css + \"position:absolute;left:\" + x` ;!x;top` +!y` *\"\";R._g.doc.body`+&)cnvs);`\"& = 1;} else`! Crelative\"`!}*.first`! !) {` -&insertBefore`#K#` ?1`!;&`'$(`!i+}}`'#(new R._Paper;` M&`$x)` -'`$y+` /'`.B#=`&G!` ,(lear()` ''_lef`'[#` *$top = 0`#H(&&`\"m(renderfix`)Z,}` n(` =%(`-F%` 4%`-E,`+.#` c)`4*&, fit) {eve(\"rapha`3H\"` O#`,7#`+X+, [` S+]`0v\"s`.[\"mmax(w /`->', h` ($`-##, `\"g\"` /!top, aspectRatio = fit ? \"meet\" : \"xMinYMin\", vb, sw`&.!x == null) {`-p'bSize) {`!J#1;}delete`\" $` ;!;vb = \"0 0 \" +`!b' + S` )$`%9#`&I$` T( =`\"L!` a\"x` N#y` V#w` )#h;}$`!P\"`%s\", {`#?#:vb, preserveA`\"R&:`\"]'});while (`\"%!&& top) {sw = \"`3S\"-`11\" in top`4Z#?` \"&['` <('] : 1;` 6$({` 0*:sw})` 9!_.dirty`#9!` %'T` )$ =` ~!prev;}`%R) =`%R*!!fit]`'!$`1Q%prototype`'[6`/H'`#6)`)7$`+z\", pos;try {po` 2%getScreenCTM() ||` 1\"`2b\"SVGMatr`(\\!} catch (e)` T)` 7/var `*.#- pos.e % 1`'B$` -\"f % 1`&w!` E!||`$L#` *$`4h$`*v$`',#` )!+` y!)` ]!s.` :#` 3)`/-!}`'e!` q!` 7\"`!:\"` c#top +`!/\"` b\"`(}'` 6#` `\"}`#x*`,\\!`#r,R.`*m)` ?!`*n#`*M\"c`$.*`''$c`/T+.remove`.y#`/>*` Z!bottom`*e' `*2\";`\"0\"desc = $(\"desc\"))`/d)`1z%`$N\"TextNode(\"C` +!d with Rapha\\xEBl`*K!R.`47#));c` b)`!)%` \"3fs`!C$fs\"))`'=-move`#9,`#:)` =\"`#?%`#5'.paren`!y!`*I!` %1`#@)` 8');for (var i`*j!his`%]#[i] = typeof` 3!` ,!= \"`!]$\" ? R._` t\"dFactory(i) :`$!\"}}`/\\\"et`\"E! = R.st`!'&method in el` =!`'l#` '#[has](` ?\") && !` b$` -)) {` /%` -\"] = (`#@&` /\"name) {`+K#`+\"-arg = arguments`+n(.forEach` h'el) {el`!)#name].apply(el, arg);});};})`!^$;}}})();` Z'`\"G#!R.vml`!R%`+,\"has = \"hasOwnProperty\", Str = String, toFloat = parse` (!, math = Math, round =` 0!.` (!,`3g!` +$max, mmin` (%in, abs` ($abs, fill`!/\" = \"fill\", separator = /[, ]+/, e`'B!`*^!, m`\" ! progid:DXImageTransform.Microsoft\", S` E!\", E = \"\", map = {M:\"m\", L:\"l\", C:\"c\", Z:\"x\", m:\"t\", l:\"r\", c:\"v\", z:\"x\"}, bites = /([clmz]),?([^` &!*)/gi, blurregexp = /`!^$\\S+Blur\\([^\\)]+\\)/g, val = /-?[^,\\s-]+/g, cssDot = \"position:absolute;left:0;top:0;`3\"!:1px;height:1px\", zo`,X!21600, pathTyp`!o!{path:1, rect:1, image:1}, oval` >%circle:1, ellips` =\"path2vml`+B)path`'d#tot`\")\"[ahqstv]`\"i!comma`%f!R._`!H!oA`\"&$Str` [\".match(` ]!`)N!(` F-2curve);` |&`#o!/g`2;!` F%` t/`*>!` u2`!s#r`\"R!` 9&replace(`%'!,`\"C'all`\")%`)K!s` [#vals = [], isM`.N\"` A#.toLowerCase()`-$!m\",`!-#map[` A#];args`!3%val`!.(value`*3#` {#&&`!2!.length == 2`*I!s +=`!K\"+` t(`!3# ? \"l\" : \"L\"];`!o%;}` g!push(`**!`!,\" *`&*!)`+s!`,U#` }!` |!` '*`+\\\"pa`\"Q&`#I\", p, r;`\"K\"[]`0-(= 0, ii`+d!`\"!#; i < ii; i++) {p` 8![i];r` \"$[0]`#G*;r`\"8!z\"`%v!r = \"x\"`17'j = 1, jj = p` %j < jj; j`!#!r +=`,p\"(p[j]`\"Q$ + (j != jj - 1 ? \",\" : E);}re`#)$` (!`\"`$.join(S);}`%g!pensa`$`!`(L(deg, dx, dy`%|#m`'g!matrix();m.rotate(- ` G!0.5, 0.5`#i%{dx:m.x(` ^#, dy:m.y` ($};}, setCoords`)f*, sx, sy`!:$, deg`!@#_`\"!_,`!K!p`!I#`/$\"pos = _.` %#, o` >!node, s = o.style, y`#k\"flip`.P#dxdy, kx =`,:\"/`!>!ky` %'y;s.visibility`1b!idden\"`*=!!sx || !sy`2('o.c`\":!ize = abs(kx) + S +` (\"y);s`#C\"`#{\"deg * (sx * sy < 0 ? -1 : 1)`!\"!`\"_&c`'B\"`$V%`$F);dx = c.dx;dy` $\"y;}sx` m!&& (`\"V!+`&b#`!##` +) y\"`-$\"y = -1);s.`#(#flip;`\"3#orig`3?!dx * - kx`\"7#dy` *\"y`!q!`$6$||`$8#size`\"!#fill`$7!getElementsByTagName` U!`3|\");` C#` \"!&&` #![0];o.removeChild` L!`#%\"`!.#) {`\"u2m.x` <$[0]`%z%[1]), m.y` %4`!C\".`2U$`#a# * y`\"U#c.`\"Y!y;}if (`\"D)` U!`%H#` /&[0] *`%L!s`%V%` 3'1` 5%y);}o.append`\"C(}`&j,` )!le\";};R.to`#<\"`(x)`&}% \"Your browser doesn\\u2019t support SVG. Falling down to VML.\\nYou are running Rapha\\xEBl \" + this.version;};var addArrow`!B)o, `/L!, isEnd`1m&u`2S%`0}\"`.,*.split(\"-\"), se =` Y\" ? \"end`0b!start\",`/I!` j\"`.;#, type = \"classic\", w = \"medium\", h` #';while (i--) {switch`2P#s[i]) {case \"block\":` '\"` q$` (#oval` $$diamond` 2%pen` $$none\":`!V#` y%;break;` =\"wide` F%arrow\":h` 55long` F$short\":w` :/default:;}`3%!stroke`)V!node`)J2\"` @\"\")[0];` &\"[se + \"`!P\"] =`#b!` )/`$ \"` =!w` ,/wid` 7#h`0D#FillAndS`!P$`%q)params) {o.attr`/i\"` $\"|| {`&U\"nod`\"'&, a` ;&`0;\"`\"C!`0=#xy, newpath = pathTypes[o.type]`-W!`!/\".x != a.`/|!` ,#y` -\"y` ('`\",!` 1\"` %\"` 1&height` 6\"` %#` 3&c` x#c` t(c` z#c` v(r` C#r` >(r` C#r` =)` -#), isOva`.]!val`\"1.`!3!!=`!B'||`!2\"` *'` v!a` p\"` y%` -\"` Q(`!R\"`!F!` *'y), r`*A!o;for (var par in`$l&if`#f$[has](par)) {a[par]`$;!` 7!par];}`.S!`$U#) {a.`$^#R._getPath`\";$(o);o._.dir`-t!1;}`$+$ref`\"\\!`%O!` )!`!j%href);` &#title` @&` )\"` B%` *!` B&arget` B'` *\"` D&` +!` F%cursor` J!` $%`#k&` +!);\"blur\"`##& && o.blur`&w$blur`2m\"` *#`\"j!` B!`+_\"= \"path\" ||`'k$) {`!]!`'t'2vml(~Str(`#K\"`.[,indexOf(\"r\") ? R._`(P!oAbsolute` O$ : ` ##`!O!`!:'image\"`)!`2Y\"pos = [a.x, a.y]`$G!`2p$` 4\"`(f!,`(O%];setCoords(o, 1, 1, 0, 0, 0);}}\"transform`#\"-` 1%`\"$` (%`!c\"`(C\"`1S#cx = +`)Q!, cy` %$y, r` 2$`'h\"` %! || 0, r` @$`)F\"` 1&;`#`(R.format(\"ar{0},{1},{2},{3},{4` )#` \"\"x\", round((cx - rx) * zoom)` /&y - ry` &/x +` :4+` >0cx` *$`(D-if (\"clip-rect`#P'`#(#rect`4`#`)c#'` E%']`4_$separato`''#rect`4E# == 4) {rect[2]`#B!` $$+` $$0];` #!3` 5'3` 5'1]`0 !div`/c$clipRec`.I!R._g.doc.create`2D#(\"div\"), d`02! = div`0=\";` -\"` `!`$J)rect({1}px {2}px {3` '!0}px)\",`!H!`\"<\"!`!9)) {` l#posi`25!= \"a`(X#\"`!($top = 0` '$left` $(`0v\"= o.paper` )#+ \"px` Z%`0}#` ;&` *#` ?#`&g#rentNode.insertBefore(div,`#9!);div.appendChild`,o!)` X\"`#Q%`#,!`.l#!`%(0`+b#` H%`-T%` *$`#h\"`#_$\"auto\")` k#o.text`,J#var ` '$S`$G#` 7&`$N#`*5#fon`!%\"` D)` 1\"`.:%fon`.d%['font-family'`2v\"` J.F` :! = \"\\\"\" +`1O$` N*`'g#\",\")[0].replace(/^['\"]+|['\"]+$/g, E) +` e!`!6+size`!*5S`-U\"` >/` W+w`%/!` O5W`%\\$` @1`!B,tyl`!87`$&#` ?0)`+6#arrow-star`+0+addArrow(res,` Z%` D'` T,end` 8Fend'], 1` \\#`%2#opacity != null ||` N%stroke-`(q!']` 0.`1Z!` $/src` \"0` i!` TP` 9%`!e#` .2fill` $:` _#dasharra` (:miterlimit` *9linejoin` $=cap` :&`){#`#;!`.y#get`.a#sByTagName(fillString), new` J#false;` %$ill &&` #![0];!` )$(` F'` E\"`/i\"Nod` m)`.z\"o.type == \"image\" &&`$f') {fill`$w!`+,%src;}`%@(&& ` u!.`/K!true` w\"` +%`%m2== \"none\"` '.`#'%` m&`\"X\"`'?!` -$`!a&fi`#U%isURL`3})` 8\".match(R._ISURL`!\\\"` H!` $`0&&`!m!ode &&`$A\"remove`/~\"` i!`$\"!.rotate`\"M#` -\"`#$\"` s![1]` .\"`#]\" \"tile\"`4=!bbox`/7!getBBox(1` k#`2m'bbox.x + S +` '\"y;o._`\")!pos = [` =\",` 7#];R._preload`\"4\"[1], func` n!() {` Y$s`-~\"[this.offsetWidth, ` ''H`-F!];});} else`#\"#color = R.getRGB`#Z*hex`\"_(E`\"V*solid\"`#x!` I2error`&.!res` Q\"in {circle:1, ellipse:1} ||`$u.charAt() != \"r\") && addGradientFill`-[(` Q!,`(F!)) {a`&[#`&s#;a.g` R#`3*'ill`%1+`&v#`4M\"\"`,((`.~' || \"` %.`'1#`.j$= ((+ a`,i-+ 1 || 2) - 1) *` A\"`/@%` *1`#L2o` >+;`!1&mmin(mmax(` -#, 0)`0X!`).\"` @%` ##`*=&`+\"'`%W$`#<#}}`(^!append`(W(var `08#`-28\"` >\"`$d\"` $?[0]`-q!` r%`$7\"!` )#`-\\#` 5%` >%`-\\'` c%`+ \"`1}*`-V'`23%`,X,`2--`1gK`1a/`1M;`1G9`1A7) {` -\"`/3&;}`\"C+`.u0` 2%`!s&` ]'`/`-` `&0`\"k7== 0`%\"!(`!H(`$[!`%c(C`+i3` 8\");` S&`$;-` q'`&$` i'.hex`(#(`)C#`$A-`(xP` {(`)1-var `\"`! = (toFloat`\"/#`\"u,) ||` p\"0.75`)WA`#T6`$A!&& (`!5$`\"D&`!'$` F4`#?'w`0C! =`!}\");` g\"&&`\",#< 1` I!`!c$*` A$` O11`$V&`+]-`'H6` _'joinstyle`/)%`'|3\"`(\\!\"`!'%`(f&` H.`(|,8`!80`(l!`!?(endcap`!22` G\"== \"butt\" ? \"flat\" :` 0:square` K!` #$: \"round\"`,C(`+10`1$#` )% = {'-':\"shortdash\", '.` (%ot\", '-` '&ash` ,%` &,` 2#. ':\"` =$` '!` p#--':\"long` (% .` 8#` G$`!%!` ;$` +&` '+dot\"}`$M$dash`%&$`\"/%[has]`\"K9?` B'` .6] : E;}`0E&`1:$`2!(`+n$}if (res.typ`-n\"text\") {res.paper.canvas.`!l!.displ`$ !E`,w\"pan = ` D&span, m = 100, fontSize = a.font &&` ##.match(/\\d+(?:\\.\\d*)?(?=px)/);s =` x!`!2\";` S&(s` %\"` i$);a['font-family`'F$` 8!F` .!`*~\"` 7)` L&`)\"` I)W`*0$` 4,` L&`!Q!` H)S`$j#` 3+);`\"X'`-^$` ;%ize`)V\"` <%&&` #%[0`-r#0;` |#` `\"` \"%* m + \"px\";`$_!extpath.string`!V\"pan.innerHTML = Str`%+\"` ?*).replace(/</g, \"&#60;\"` ,'&` 0#38` *)\\n` 3!<br>\")`0/\"brect`$W$getBoundingClientRect()`!l!W`$`!w = (` O!.r`#s!-` \\\".left) / m` H!H` H!`1(!` 9\"bottom` E%top` C&X` H!x` '!Y` '!y +`'!!H / 2;(\"x\" in`,O#`.4!y` &'`/`\"`'O\"th.v`4S!format(\"m{0},{1}l{2` #!\", `,e!(a.x * zoom)` )&y ` \"-` =% + 1`#-#dirtyattrs = [\"x\", \"y\",`)I#, \"font\",` \"\"`'?#` &%`&y\"` &%`&U!` $&ize\"];for (`!%! = 0, dd =`!+'.length; d < dd; d++) {if (` ;&[d]`\"x(`*!_.` ;! = 1;break;}}switch (a['text-anchor`/F!case \"start\":`&Y+yle['v-` M\"lign'] = \"left`'[\"bbx`+e#W`$a!`!*\"` m\"end` ID`&A!` f(-` c-default` JCcenter` f(0`\"d$` F7ker` Z\"true;}}, addGradientFill = function (o, g` 5#, fill) {o.`%j$` \"$|| {}`&,!` ++, pow = Math.pow, opacity, oindex, `/}\" \"linear\", fxfy = \".5 .5\";` c#.`!?$ =`!I%;` )'Str(` '$`*~&R._radial_`!{'`\"4%all, fx, fy) {`!A$` K\"\";if (fx`-U!y) {fx`-'fx);`!a!` (%y);pow(fx - 0.5, 2) + ` .!y` ('> 0.25`*C!` [!m`$<!qrt(` 3!-` L\"` Z') * ((fy` U!5) * 2 - 1) +` +!;`#'#fx + S + fy;}return E;})`\"w(` #$.split(/\\s*\\-\\s*/)`\"G!`4#%`$##) {`$m!ng`0|!` T&hift();` 1$-`0v&ngle` g\"isNaN` *#`)n!`!M!null;}}`*k!ots`,{!_parseDots`$V&` ^!!dot`*I\"` Q'o`&7!shape || o.node` J!dot`+G$`&!removeChild(`'4!;fill.on`'s$` *!method = \"none\"` /\"color =`!_![0]` *\"` 0'2` 4$`!)' - 1` B$var cl`..\"`-8'i`-=\"ii` X#`-4%i < ii; i`-8!` u!i].offset &&` g!.push`\"@!` 1'`%-\"` -$`!8!);}`!_&s =` W\"`!]#?` (\"join() : \"0% \" +`*4!`!u#`%=)`($#) {` =!`(:$`$@$Titl`#,$focus = \"100%` ((siz` P!0 0` +(posi`)A!=`*L!` 4\"`&+$0;} else`!!3` a#` F$(270 -`'#\") % 360;}o.append`%5(`(1$1;}, Elem`(2\"`*g&node, vml) {this[0`-d!his`&C! = `&J!node.raphael`&&$` @!id`'M\"oid++` :)` 5!` =#` F\"X`\"C!` &!Y`\"N!` &!`-x${}` *\"paper = vml` +\"matrix`!\"!` %\"()` 3\"_ = {transform:[], sx:1, sy:1, dx:0, dy:0, deg` \"!irt` 6\"irtyT:1};!vml.bottom`,?!` $'`!y\"`!&#prev`!P\".top;v` \"\"` J%top.next` L%` 9$` *\"` ^\"` :#`*2\"`(O!elproto`\"8!el;`$;#.` 0!`%=#` <#;` D#.construct`)l!` K#` 5%`\"f%`$y)tstr) {`'e!str =`!?\"`+a&`#J\"` T&;`,M!vbs`$\\$`$1!._viewBoxShift, vbt =` A!? \"s\" + [vbs.scale, ` \"%] + \"-1-1t` :%dx` :\"dy] : E, oldt`)E!vbs) {old`#F!`!h!`2E!`!~!`2=%/\\.{3}|\\u2026/g,`!p- || E);}R._extractT` 3$(this`!w\"+` }!)`$&!`&>%`&J'.clone(), skew` 4$skew, o`(<(, `0}!, isGrad = ~`!p!`'X%.`)K!.indexOf(\"-\"), isPatt = !` /:url(\");`!M#`\"?!late(1, 1`15$` e!||`!9$||`!W\"`,p%image\") {skew`(j&\"1 0 0 1\";` 3!`.?#`,U$`\"0!`4N\"rix`3?#`!*$`!#!&&`\"T\".noRota`&J!|| !` /\"isSimple`1Z!style.filt`*;!`\"&$oF` -!(`$ \"bb`#O$getBBox(), b`&C!` ))1), dx = bb.x - bbt.x, dy` -\"y` ,#y;o.coordorigin = dx * - zoom`0e$y` )%;setCoords`%^#1, `+E!, dy, 0)`.}%`!~-E` K-`\"]\"`'}!x`%Y#` (\"y` &$d` /%d` +%rotate);}` n8`$V)Str(` '\")`$V+`#\\#` *\"();}`(r!&& `&F\"`(C(=`)=!);`*e';}`+M%`!O\"`+F)deg, cx, cy`+R$his.removed`+L*;}if` M!`+h-;}` 1!`!!deg)`&<#separator`&F\"deg`4h$- 1) {c`)c!oFloat(deg[1]);cy` &+2])` x$` +(0` E#`!E\"`\"j!` k!cy`!(\"cx` 3%|| ` @&) {`&|\"o`*}%`&d&;` ^!bbox.x +` #\"width / 2`!U\"` /!y` 5$height` 9!}`$'#`1O\" = 1`0I\"`$5%`$>-.concat([[\"r\"`29!`$$$]])`$M4`+/%`$Z*`'W!`$@?`))!`$C!x`$/4x`$8*`)H!`#q%x`$@!` a\"` *'0]) || 0;` I!`)I!` *!`!F%_.bbox) {` #'.x += dx`#\"\"` -#y` /!y`#H#`\"xAt\"`*5$`\"}7`)o!`#')sx, sy`'TGs`#4$s`#$4s`#-+s`#0(s`#6\"`'i)sx`'[!`'h)sx[3]);isNaN(cx)`'R&`';!` 1$y` 2#y` 0%`![\"` \\'0]);s`(6*`!M!sx`'hp}` ^\"` R&?`(R4 : cx`\"O\"`!<'` H#`(i/ : cy`(CGs\", `$p*`%T!`)O.`%U2hid`%\\*) {!`%M(`.S&node`/S#display = \"none\"`&P5how` BXE` a4_`$%#` m,`'=6{};}` #$x:` ?!X +` G#bbx`)!) -`%%\"W / 2, y` C\"Y` /$H, `$t!` 1\"W, `$R\"` )\"H}`14(`!;!`!?< || `\"j\"`\"Z!parentNode`1$'` :!paper.__set__ &&`!I\"` )).exclude` |!);R.eve.unbind(\"raphael.*.*.\" +` Y\"id);R._tear` P!,` k'`%{#`!H+`!q#Child`$M&` E#shape`!S%` )!` >9` =!);for (var i i`%\"\"`-s#[i] = typeof` 3!` ,!= \"`#K$\" ? R._`#E#Factory(i) :`) !`#4#`#c$= true`$8'attr`$4)name, value`-??if` L\"`+\"+res = {}`\".&a`\"0$`!-!s` o(` ,![has](a)` |!s[a`\"Q!` 4&a];}}res.gradient && res.fill`\"k!`)>!`(e!` -&` =!` J$`-y!delete` +);re`+['`!/$`+]'`)6$re`\"J#`\"x!`.!(R.is`#2#\"string\")`\";#`\"u$fillS` 6!`%T%`\"5!`!n/` 2'`!r&`#k(` .+;}var nam`#p!name`1[-, out`$#+i = 0, ii` N#s`2##; i < ii; i++) {`!|\"` =\"[i]`0K!` /!`$a,out[name`$L+` .!;} else if (`#)!`)p'customAttribute` K#,`'X')` j0` A8.def`!+$` M)R._availableA`!S(`-O$ii - 1 ?`#,!: ` T$s[0]]`%8\"`\"1& &&`(C\"`%44array`!v$`#t'`#k,`#c3`!5$[i]`#G)` z![i])`/D%ou`%?#params`$?!`!T\"!`)O%` 5\"`!@\"` &\"`\"m%` F!;}`!o9object\"`(a!(` c%nam`,R(key in`!?#) {eve`.M&attr`.S!key + \"`.X(`.P\",` S#[key])`#i\"` c%`! !` z#`%&7`+N(`%L3`+f!key`\"'!`!+#` ')`&a=key`&p-`#x#`&`;key].apply`1'#[].concat`\"C#`\"V\"`0g#`(\\\"key] =`\"p(`#h&sub`#k&`\"U#par`\"*\"` 7\")`%4%[` +\"` f#` &$;}}}`#`'.tex`.:!`!=!typ`%N!\"text\"`1N#` >!path.`-(\"`!T%` 4!;}setFillAndStrok`4(\"`$u$`',&`0]\"`1B%toFront`1A)) {!`1t)`!P$`4$,append`3~2`$&!` Q%`$3\"top !`$E\"`%,\"_tofront`!e#` A&)`/x$`!^-Back`!^,`2m@`!l1first`!{!`!P$`!}\"`#k#` ?,insertBefor`#Y\"` =!`!q#` b6);`\"E!back`\"6/`$\"3`!&\"After`\"B)elem`1W\"`\"2@` G#.constructor == R.s` ')) {` C# = ` ##[` T$`-b\" - 1`.f#` 1$`\"M!nextSibling` \\&`\"qE` R5`0`%` Z4`&E3}R._`!'\"after`#i#` ^#`%~@`!^(`\"w~`#d10];}`\"mX`&K!` C\"b` ?&`\"9Iblu`&I*size`.I#s`.F$`!W!runtimeStyle, f = s.filter;f = f.replace(blurregexp, E)`3)!+ size !== 0`),$`.Y!`!8$size;` h$ = f + S + ms`2'!Blur(pixelradius=\" +` m%|| 1.5) + \")\";s.margin = R.format(\"-{0}px 0 0 ` %\"\", round` U+`&d&`!@(` h(0;delete`\"s\"`!z&`*.,R._engine.path`#U)pathS`/U!, vml`#f#el = createNode(\"shape\");el.style.cssText = cssDot;el.coord`!}!= zoom`#0#zoom` 6%ori`!v\"vm` %);`3O! = new E`%m\"(el`!O\", attr = {fill:\"none\", s`1-!:\"#000\"};`\"!& && (attr`\"H$` 1&);p`2B# \"path\";p` =$[];p.P` &\"E;`2(-p`!@\");`!u!anvas`*b)el)`\" !skew`#.,kew\");skew.on = true;el` S)skew);p.` Y#skew;p.transform(E`(M%p`$f)rec`3;*vml, x, y, w, h, r`$o#`\"A#R._rectPath(` 9*, res`$5#path`%Y!), a = re`&;#;res.X = a.x = x` +!Y` +!y = y` +!W` +!wid`!-!w` /!H` /!height = h;a.r = r;a`$;(` F!`$:$rect\"`\"O$re`'>*ellips`-a*`\"Q'rx, ry`\"T#`\")+`!{5x - r`\")&y - r`\"(&rx * 2`\"$%ry` &%`!i$`!M#\"`%m.res, {cx:x, cy:`!\\!:`!^\":ry}`$t%`\"6,circl`\"+6`!lP`\"1*`\"4&`\"/$`\"%-`!>\"`!oB:r`!r6imag`!x/src`'%(`&j@`&t3`\"I!({`*q$`+#!}`\"],, node` +#`2\"\"fill =` 3!.get`+y#sByTagName(fill`+:#[0];a.src = src`'Ch`'o4`#3!\";fill`4+' =`!l\" &&`!t\"remove`+H\"fill)` L\"rotate`+n$` .!`!t&` *!`! $tile\"`!5!_.fillpos = [x, y]` ,'`/\\#[w, h];`!6!`,](`!5\"setC`0+!`%_\"1, 1, 0, 0, 0`%H5t`0m\"`'V1text`1@;,`%m$` 0(`/a!), o` )+text` 6\";`$T! || 0;`$O!` %\"`!F#` \"!|| \"\"`0}!.v`4`)m{0},{1}l{2` #!`4c%x *`2U!)` )$y ` \"+` 9% + 1)` w\"`!M$ok`$H$o.s`2.\"= Str(`\"a!;o`0Y*`3Lc\"0 0\"`3\\I`3u!`3&`41#font:R._availableAttrs.font`$a\":text};p.`$S! = el`3}&`'y!p`\"a% = o`4I'text\";p`*$\"`%h$`\"o&` 1$`)E\"` &$`)C\"` &$w = 1` %%h` )!`,i-p`\"E\")`46,o)` \",`+t!;vml.canvas` 0)el)`#L!skew`'$,kew\");skew`$`*` T(skew);p.` Y#skew;p.transform(E`(b%p`(\\)setS`%0\"`(b&`+z!, `+l\"`(c#cs = this`!u$`&&!;` -!`,F%idth` -\"`,D&` $!;` <#= +` @\" &&`!!# += \"px\");` L$= +` P#` A!` $#` >%c`!!,c` }.cs.clip = \"rect(0 \"`!1%+ \"` )!` p$ \" 0)\";if (`!v!_viewBox) {`\"z)V` 1\".apply` F!,`\"k\"` H%;}`#\\#thi`,A*` U&`#\\)`1]&, fit`!-!eve(\"raphael` M'\"`!'\"`!$+,`.N\"` Y']`&!\"`\"z$`$6&`$h$` -$` (\", `*M#1 / mmax(w /` X\", h /`%=$, H, W`# !`!c\"H`#d% / h;W`$&$ / w` B!w * H <` 0\") {x -=`%+$- ` 9!) / 2 / H;}if (h * W <`&N&y` N!` #- ` ;!` L$W;}}`\"^)`18$`\"d$!!fit]`&h\"` <$Shift = {dx:- x, dy:- y, scale:size}` L\"forEach(`$B&el) {el`(h'\"...\");}`(q%`%0#`(J!`)w%`%;'initW`-|!`) (n`({#doc`#@!n.document;doc.` d\"StyleSheet().addRule(\".rvml\", \"behavior:url(#default#VML)\");try {!doc.namespaces` U! && ` (+add(\"` n$urn:schemas-microsoft-com:vml\");`\";&`\"&)tagName) {`\"q#`!}&`0?$\"<rvml:\" + ` G#`)C!class=\\`!<!\\\">\");};} catch (e) {` VX` },xmlns=\\`\"02.`\"?#\\`!>3`*1(`$p#(R._g.win)` 1'`!?\"`!`)`.)$`/F!R._getContainer`+V#0, arg`%?!s), c` 7$ = con.` '%`*)'con`*)&,`*W%con`*W$x` (#x, y` $#y`)e!!` g%) {throw new Error(\"VML`!6'not found`'u!var res =` L!R._Paper, c = re`0C$`\"@#.`$\"/div\"),`0w\"c`0l#`4-! || 0;`4*!` %\"`/f) || 512`0I%`\"_$|| 342;re`03,re`0Vy`\"G!oord`.&#zoom * 1000 + S +` &(` D&orig`*j!\"0 0\"` 3!spa`%O$`\"|0span`!)#span`#,\".cssText = \"position:absolute;left:-9999em;top` #%padding:0;margin:0;line-`\"7\":1;\";c.appendChild(`!.$`3~!`!*&R.format(\"top:0`!+\"`$J#:{0}`#>#:{1};display:in`!&!block;`!k%relative;clip:`4R#{0} {1} 0);overflow:hidden\"`'`#`'~$)`'I!`(B'= 1`4_\"`#F\"body`\"$)c`\"%!le`0/!x +`$i!;cs.top`&U!` (&`!\\$ = \"`#S$\";} else {`!+).first` }!) {` -&insertBefore(c`*+'` E(` j%` 4&`!c+}}res.renderfix`+K,}`1E$res;};R.prototype.clear` @,R.eve(\"raphael` >\"\",`2&!)`2e\"`)\".innerHTML = E` 4\"`&gB` B%`&0z`&-*;\"`!p)`#8(`!G%`!T#bottom =`\"O!`%4#null`#/+remov`/9-`#0+` ?\"`#+1parentNod` c$`!C'` =\");for (var i in` ^\" {this[i] = typeof` 3!` ,!= \"`!E$\" ? R._` t\"dFactory(i) :`\"%#`%O#true;};var set`\"9!`$]!st`!3&method in el` =!)`'\\\"` '#[has](` ?\") && !` b$` -)) {` /%` -\"] = (`#4&` /\"n`4K)`2u-arg =`2c&`'U$`#1!forEach` h'el) {el`!)#name]`3K#el`3N!);}`4Z!)`!^$;}}})();oldR`$]#was ? (`4f!.` .# = R) : (window` *)`!L$R;});"))

//Map path and default settings - you can edit this
var simplemaps_worldmap_mapinfo={map_name:"world",state_bbox_array:{BD:{y:332,x:1472,x2:1501,y2:369},BE:{y:173,x:999,x2:1017,y2:185},BF:{y:405,x:956,x2:999,y2:440},BG:{y:218,x:1101,x2:1133,y2:237},BA:{y:212,x:1067,x2:1087,y2:228},BB:{y:416.1,x:650.8,x2:652,y2:418},BL:{y:386.7,x:635.1,x2:635.4,y2:387},BM:{y:293.9,x:636.5,x2:637.3,y2:294.5},BN:{y:466.8,x:1633.1,x2:1639.9,y2:476.1},BO:{y:564,x:594,x2:665,y2:649},JP:{y:210,x:1683,x2:1736,y2:303},BI:{y:517,x:1150.8,x2:1160.6,y2:531},BJ:{y:423,x:991,x2:1008,y2:462},BT:{y:320,x:1474,x2:1492,y2:330},JM:{y:382.9,x:549,x2:561,y2:388.2},BW:{y:615,x:1096,x2:1150,y2:674},WS:{y:588.3,x:14.7,x2:22.8,y2:592},BR:{y:468,x:568,x2:790,y2:719},BS:{y:329,x:553,x2:583,y2:368},BY:{y:144,x:1098,x2:1143,y2:174},BZ:{y:383,x:486.1,x2:494.3,y2:400},RU:{y:10,x:1080,x2:1780,y2:238},RW:{y:509,x:1151,x2:1161,y2:521},RS:{y:206,x:1081,x2:1104,y2:231},LT:{y:143,x:1085,x2:1112,y2:158},RE:{y:635.8,x:1293.4,x2:1296.8,y2:639},LU:{y:181.3,x:1014.4,x2:1017.1,y2:185.5},LR:{y:447,x:922,x2:944,y2:474},RO:{y:193,x:1088,x2:1137,y2:222},GW:{y:421,x:892,x2:909,y2:431},GU:{y:414.2,x:1800,x2:1801.5,y2:416.8},GT:{y:387,x:468,x2:492,y2:414},GR:{y:234,x:1092,x2:1127,y2:278},GQ:{y:487.2,x:1039,x2:1050,y2:495.3},GP:{y:395.8,x:640.1,x2:643.5,y2:399.9},BH:{y:333.3,x:1264,x2:1265,y2:336.2},GY:{y:448,x:639,x2:666,y2:494},GF:{y:465,x:677,x2:694,y2:489},GE:{y:223,x:1191,x2:1229,y2:238},GD:{y:423.3,x:638.5,x2:639.4,y2:424.8},GB:{y:130,x:950,x2:995,y2:182},GA:{y:487,x:1036,x2:1068,y2:527},SV:{y:409.2,x:480,x2:493,y2:417.4},GN:{y:421,x:901,x2:942,y2:455},GM:{y:412.7,x:892,x2:909,y2:417.5},GL:{y:1,x:720,x2:944,y2:121},KW:{y:308.8,x:1241,x2:1251,y2:318.7},GH:{y:431,x:968,x2:992,y2:472},OM:{y:332,x:1276,x2:1318,y2:395},JO:{y:288,x:1177,x2:1198,y2:314},HR:{y:204,x:1056,x2:1085,y2:229},HT:{y:374,x:570,x2:587,y2:386},HU:{y:191,x:1068,x2:1100,y2:209},HN:{y:399,x:484,x2:520,y2:418},PR:{y:382.9,x:607,x2:622,y2:386.7},PS:{y:293,x:1175.5,x2:1178.7,y2:300.5},PW:{y:452.8,x:1746.8,x2:1747.7,y2:454.6},PT:{y:231,x:936,x2:953,y2:265},PY:{y:626,x:639,x2:687,y2:679},IQ:{y:262,x:1195,x2:1251,y2:315},PA:{y:440,x:518,x2:550,y2:455},PF:{y:558,x:138,x2:206,y2:679},PG:{y:518,x:1783,x2:1869,y2:570},PE:{y:502,x:525,x2:600,y2:620},PK:{y:263,x:1318,x2:1402,y2:350},PH:{y:383,x:1649,x2:1702,y2:466},PL:{y:152,x:1054,x2:1104,y2:188},ZM:{y:555,x:1109,x2:1175,y2:617},BQSE:{y:389.4,x:634.1,x2:634.3,y2:389.7},EH:{y:324,x:891,x2:939,y2:367},BQSA:{y:388.5,x:632.7,x2:632.9,y2:388.7},EE:{y:124,x:1093,x2:1115,y2:137},EG:{y:299,x:1121,x2:1191,y2:361},ZA:{y:644,x:1076,x2:1167,y2:725},EC:{y:493,x:527,x2:560,y2:534},AL:{y:228,x:1086.1,x2:1095.8,y2:247},AO:{y:530,x:1051,x2:1122,y2:617},KZ:{y:149,x:1217,x2:1419,y2:241},ET:{y:406,x:1173,x2:1257,y2:480},ZW:{y:601,x:1128,x2:1170,y2:645},SA:{y:295,x:1176,x2:1295,y2:397},ES:{y:221,x:938,x2:1002,y2:271},ER:{y:386,x:1190,x2:1229,y2:422},ME:{y:223,x:1081.5,x2:1091,y2:233},MD:{y:192,x:1119,x2:1138,y2:210},MG:{y:579,x:1226,x2:1270,y2:666},MF:{y:385.5,x:633.8,x2:634.3,y2:386},MA:{y:272,x:892,x2:980,y2:364},UZ:{y:210,x:1271,x2:1366,y2:263},MM:{y:320,x:1499,x2:1548,y2:438},ML:{y:342,x:918,x2:1010,y2:437},MN:{y:169,x:1420,x2:1588,y2:235},MH:{y:456,x:1953.8,x2:1954.8,y2:456.4},MK:{y:230.3,x:1092,x2:1106,y2:239.7},MU:{y:628.1,x:1305,x2:1340,y2:633.5},MT:{y:270.2,x:1061.8,x2:1063.9,y2:272},MW:{y:561,x:1170,x2:1187,y2:610},MV:{y:474.7,x:1402.7,x2:1402.9,y2:475},MQ:{y:406.3,x:642.6,x2:644.8,y2:409.3},MP:{y:381,x:1799.1,x2:1804.5,y2:411},MS:{y:393.9,x:637.9,x2:638.4,y2:394.8},MR:{y:326,x:892,x2:959,y2:408},UG:{y:474,x:1154,x2:1185,y2:511},MY:{y:457,x:1553,x2:1661,y2:497},MX:{y:292,x:354,x2:504,y2:409},VU:{y:586,x:1921,x2:1932,y2:632},FR:{y:175,x:963,x2:1036,y2:236},AF:{y:255,x:1312,x2:1385,y2:314},FI:{y:64,x:1071,x2:1125,y2:122},FJ:{y:606,x:1977,x2:1995,y2:625},FK:{y:828,x:688,x2:705,y2:836.5},FM:{y:440,x:1766,x2:1909,y2:468},FO:{y:107.6,x:953.4,x2:958.4,y2:113.3},NI:{y:405,x:493,x2:520,y2:433},NL:{y:160,x:1003,x2:1020,y2:177},NO:{y:13,x:1008,x2:1114,y2:133},NA:{y:611,x:1052,x2:1127,y2:688},NC:{y:628,x:1898,x2:1920,y2:647},NE:{y:351,x:988,x2:1075,y2:427},NG:{y:413,x:1002,x2:1069,y2:475},NZ:{y:723,x:1827,x2:1930,y2:801},BQBO:{y:422.8,x:601,x2:602.3,y2:424.6},NP:{y:307,x:1424,x2:1471,y2:332},NR:{y:505,x:1932.7,x2:1933,y2:505.3},XK:{y:224.3,x:1089.8,x2:1098.5,y2:233.4},CI:{y:434,x:938,x2:972,y2:474},CH:{y:196,x:1017,x2:1039,y2:208},CO:{y:422,x:539,x2:607,y2:529},CN:{y:161,x:1372,x2:1662,y2:385},CM:{y:419,x:1034,x2:1077,y2:491},CL:{y:615,x:593,x2:669,y2:856},CA:{y:2,x:348,x2:778,y2:232},CG:{y:478,x:1049,x2:1091,y2:534},CF:{y:430,x:1068,x2:1141,y2:487},CD:{y:468,x:1055,x2:1163,y2:587},CZ:{y:175,x:1047,x2:1080,y2:191},CY:{y:275.9,x:1158.8,x2:1168.6,y2:279.5},CR:{y:430,x:502,x2:520,y2:449},CW:{y:422.3,x:596.8,x2:599.1,y2:424.5},CV:{y:391,x:844,x2:859,y2:407},CU:{y:353,x:515,x2:573,y2:374},SZ:{y:667,x:1154.9,x2:1162.6,y2:677},SY:{y:263,x:1178,x2:1211,y2:294},SX:{y:385.9,x:633.8,x2:634.2,y2:386.1},KG:{y:224,x:1350,x2:1401,y2:250},KE:{y:467,x:1179,x2:1224,y2:532},SS:{y:423,x:1121,x2:1186,y2:479},SR:{y:463,x:658,x2:681,y2:490},KH:{y:408,x:1562,x2:1592,y2:435},KN:{y:390,x:634.7,x2:636.4,y2:392},KM:{y:574.8,x:1229.9,x2:1237.1,y2:581.3},ST:{y:491,x:1023,x2:1028.6,y2:502},SK:{y:185,x:1070,x2:1098,y2:196},KR:{y:254,x:1653,x2:1677,y2:281},SI:{y:201.7,x:1055,x2:1070,y2:210.5},KP:{y:226,x:1636,x2:1662,y2:260},SO:{y:425,x:1219,x2:1275,y2:513},SN:{y:395,x:887,x2:922,y2:423},SL:{y:437,x:912,x2:929,y2:458},SC:{y:531,x:1247,x2:1301,y2:563},SB:{y:544,x:1866,x2:1927,y2:578},KY:{y:375,x:532.5,x2:542.1,y2:378.1},SG:{y:494.1,x:1574.4,x2:1576.5,y2:495.3},SE:{y:70,x:1037,x2:1088,y2:149},SD:{y:361,x:1110,x2:1201,y2:447},DO:{y:374,x:585,x2:605,y2:389},DM:{y:401.5,x:641.5,x2:642.7,y2:404.2},DJ:{y:420,x:1221.1,x2:1230.3,y2:432},DK:{y:135,x:1024,x2:1046,y2:153},DE:{y:152,x:1015,x2:1060,y2:199},YE:{y:380,x:1225,x2:1284,y2:421},AT:{y:188,x:1034,x2:1071,y2:204},DZ:{y:263,x:939,x2:1053,y2:380},US:{y:58,x:100,x2:646,y2:380},LV:{y:134,x:1085,x2:1118,y2:148},UY:{y:695,x:672,x2:700,y2:726},YT:{y:583.1,x:1239.9,x2:1240.9,y2:585.2},LB:{y:279,x:1175.5,x2:1182.7,y2:289},LC:{y:411.3,x:643,x2:644.1,y2:413.8},LA:{y:358,x:1544,x2:1590,y2:413},TV:{y:556.4,x:1998.9,x2:1999,y2:556.6},TW:{y:339,x:1651,x2:1659.2,y2:361},TT:{y:429,x:636.9,x2:645.2,y2:437.3},TR:{y:232,x:1122,x2:1224,y2:272},LK:{y:439,x:1437,x2:1449,y2:464},TN:{y:262,x:1027,x2:1048,y2:307},TO:{y:602,x:1,x2:16,y2:640},TL:{y:554.9,x:1692,x2:1706,y2:562.1},TM:{y:228,x:1258,x2:1338,y2:275},TJ:{y:239,x:1340,x2:1383,y2:266},LS:{y:686,x:1133,x2:1146,y2:699},TH:{y:371,x:1530,x2:1579,y2:465},TG:{y:431,x:986,x2:997,y2:464},TD:{y:352,x:1062,x2:1120,y2:454},TC:{y:360.8,x:584.7,x2:588.7,y2:362},LY:{y:289,x:1038,x2:1126,y2:376},VC:{y:416,x:641.6,x2:642.5,y2:417.5},AE:{y:335,x:1271,x2:1298,y2:357},VE:{y:424,x:572,x2:649,y2:497},AG:{y:388.2,x:640,x2:641.3,y2:392.6},VG:{y:383.3,x:625.2,x2:626.1,y2:383.8},AI:{y:384.5,x:634,x2:634.6,y2:384.9},VI:{y:383.8,x:623.2,x2:625.2,y2:388.3},IS:{y:84,x:882,x2:928,y2:101},IR:{y:247,x:1218,x2:1334,y2:341},AM:{y:237,x:1213,x2:1231,y2:253},IT:{y:200,x:1021,x2:1083,y2:267},VN:{y:352,x:1553,x2:1602,y2:447},AS:{y:593.1,x:26.6,x2:34.3,y2:594.1},AR:{y:642,x:604,x2:692,y2:854},AU:{y:570,x:1603,x2:1829,y2:782},IL:{y:288,x:1172.1,x2:1179.5,y2:312},AW:{y:420.7,x:591.9,x2:592.9,y2:422},IN:{y:274,x:1364,x2:1520,y2:451},TZ:{y:508,x:1154,x2:1214,y2:577},IC:{y:314,x:887,x2:913,y2:324},AZ:{y:233,x:1220,x2:1249,y2:256},IE:{y:151,x:938,x2:957,y2:172},ID:{y:467,x:1526,x2:1786,y2:568},UA:{y:168,x:1096,x2:1185,y2:217},QA:{y:334,x:1266,x2:1270.9,y2:344},MZ:{y:568,x:1156,x2:1215,y2:674}},paths:{BD:"M1500.6 360.3l0.6 4.6-2.1-1 1.1 5.2-2.1-3.3-0.8-3.3-1.5-3.1-2.8-3.7-5.2-0.3 0.9 2.7-1.2 3.5-2.6-1.3-0.6 1.2-1.7-0.7-2.2-0.6-1.6-5.3-2.6-4.8 0.3-3.9-3.7-1.7 0.9-2.3 3-2.4-4.6-3.4 1.2-4.4 4.9 2.8 2.7 0.3 1.2 4.5 5.4 0.9 5.1-0.1 3.4 1.1-1.6 5.4-2.4 0.4-1.2 3.6 3.6 3.4 0.3-4.2 1.5 0 4.4 10.2z",BE:"M1016.5 177.1l-0.4 4.2-1.3 0.2-0.4 3.5-4.4-2.9-2.5 0.5-3.5-2.9-2.4-2.5-2.2-0.1-0.8-2.2 3.9-1.2 3.6 0.5 4.5-1.3 3.1 2.7 2.8 1.5z",BF:"M988.5 406l-0.5 3.1 0.8 2.9 3.1 4.2 0.2 3.1 6.5 1.5-0.1 4.4-1.2 1.9-2.8 0.6-1.1 2.8-2 0.7-4.9-0.1-2.6-0.5-1.8 1-2.5-0.5-9.8 0.3-0.2 3.7 0.8 4.8-3.9-1.6-2.6 0.2-2 1.6-2.5-1.3-1-2.2-2.5-1.4-0.4-3.7 1.6-2.7-0.2-2.2 4.5-5.3 0.9-4.4 1.5-1.6 2.7 0.9 2.4-1.3 0.8-1.7 4.3-2.8 1.1-2 5.3-2.7 3.1-0.9 1.4 1.2 3.6 0z",BG:"M1132.6 221.6l-2.3 2.6-1.3 4.5 2.1 3.6-4.6-0.8-5 2 0.3 3.2-4.6 0.6-3.9-2.3-4 1.8-3.8-0.2-0.8-4.2-2.8-2.1 0.7-0.8-0.6-0.8 0.6-2 1.8-2-2.8-2.7-0.7-2.4 1.1-1.4 1.8 2.6 1.9-0.4 4 0.9 7.6 0.4 2.3-1.6 5.9-1.5 4 2.3 3.1 0.7z",BA:"M1083 214.3l1.9-0.1-1.1 2.8 2.7 2.5-0.5 2.9-1.1 0.3-0.9 0.6-1.6 1.5-0.4 3.5-4.8-2.4-2.1-2.7-2.1-1.4-2.5-2.4-1.3-1.9-2.7-3 0.8-2.6 2 1.5 1-1.4 2.3-0.1 4.5 1.1 3.5-0.1 2.4 1.4z",BB:"M651.5 418l-0.6-0.2-0.1-0.5 0-0.8 0.2-0.4 0.2 0.2 0.2 0.6 0.5 0.3 0.1 0.4-0.5 0.4z",BL:"M635.2 387l-0.1-0.3 0.3 0.1-0.2 0.2z",BM:"M637.3 294l-0.3 0-0.1 0.1 0.1 0.2-0.4 0.2-0.1-0.1 0.2-0.1 0.1 0 0.1-0.3 0.3-0.1 0.1 0.1z",BN:"M1633.1 472.8l2.2-2.4 4.6-3.6-0.1 3.2-0.1 4.1-2.7-0.2-1.1 2.2-2.8-3.3z",BO:"M662.5 631.4l-0.3-2-5.4-3.3-5.2-0.1-9.6 1.9-2.1 5.6 0.2 3.5-1.5 7.7-1-1.4-6.4-0.3-1.6 5.2-3.7-4.6-7.5-1.6-4 5.8-3.9 0.9-3.1-8.9-3.7-7.2 1.1-6.2-3.2-2.7-1.2-4.6-3.2-4.4 2.9-6.9-2.9-5.4 1.1-2.2-1.2-2.4 1.9-3.2-0.3-5.4 0-4.6 1.1-2.1-5.5-10.4 4.2 0.6 2.9-0.2 1.1-1.9 4.8-2.6 2.9-2.4 7.3-1.1-0.4 4.8 0.9 2.5-0.3 4.3 6.5 5.7 6.4 1.1 2.3 2.4 3.9 1.3 2.5 1.8 3.5 0 3.4 1.9 0.5 3.7 1.2 1.9 0.3 2.7-1.7 0.1 2.8 7.5 10.7 0.3-0.5 3.7 0.8 2.5 3.2 1.8 1.7 4-0.6 5.1-1.3 2.8 0.8 3.6-1.6 1.4z",JP:"M1708.5 282.6l1.6 2.2-1.3 3.9-3.1-2.1-2.1 1.5 0.1 3.7-4.3-1.8-1.2-3 1.3-3.9 3.4 0.8 1-2.7 4.6 1.4z m24.6-19.2l0.6 5.1 2.5 3.2-0.6 4.5-5.4 3-9.2 0.4-4.4 7.4-4.7-2.5-2.4-4.8-8.6 1.4-5.1 3-6.2 0.2 7.4 4.7 0.8 10.9-2.5 2.7-3.6-2.5-0.9-5.8-4.1-1.8-4-4.4 4.3-2 1.1-4.1 3.9-3.3 2-4.4 9.7-1.9 6.3 1.3 0-11.4 5.1 3.1 4.5-6.4 1.7-2.5-1-7.8-5.1-7.2-0.2-4 4.8-1.2 8.2 8.9 2.8 5.1-1.3 6.5 3.6 6.6z m-11.9-44.8l4.5 1.3 1.8-2.6 6 7.1-6.4 1.7-0.4 6.3-10.9-4.3 1.6 6.9-5.7 0.1-4.7-6.3-0.6-4.9 5.2-0.3-4.4-8.8-1.8-4.9 10.5 6.6 5.3 2.1z",BI:"M1154.9 530.4l-0.6 0.1 0-0.3-2-6.1 0-0.1-0.1-1-1.4-2.9 3.5 0.5 1.7-3.7 3.1 0.4 0.3 2.5 1.2 1.5 0 2.1-1.4 1.3-2.3 3.4-2 2.3z",BJ:"M1006.7 427l-0.2 2.1 1.3 3.8-1.1 2.6 0.6 1.7-2.8 4-1.7 2-1.1 4 0.2 4.1-0.3 10.3-4.7 0.8-1.4-4.4 0.3-14.8-1.2-1.3-0.2-3.2-2-2.2-1.7-1.9 0.7-3.4 2-0.7 1.1-2.8 2.8-0.6 1.2-1.9 1.9-1.9 2 0 4.3 3.7z",BT:"M1488.8 323.5l2.6 2.1 0.5 3.9-4.5 0.2-4.7-0.4-3.2 1-5.5-2.5-0.4-1.2 2.6-4.8 2.6-1.6 4.3 1.4 2.9 0.2 2.8 1.7z",JM:"M556.5 387.1l-1.8 1.1-3-1.1-2.9-2.3 0.8-1.5 2.4-0.4 1.3 0.2 3.7 0.6 2.7 1.5 0.8 1.8-4 0.1z",BW:"M1127.6 615.7l1.9 5.1 1.1 1.2 1.6 3.7 6.1 7 2.3 0.7-0.1 2.3 1.5 4.1 4.3 1 3.4 2.9-8.1 4.7-5.2 4.8-2 4.3-1.8 2.4-3 0.5-1.2 3.1-0.6 2-3.6 1.4-4.5-0.3-2.5-1.8-2.3-0.7-2.8 1.4-1.5 3.1-2.7 1.9-2.8 2.9-4 0.7-1.1-2.3 0.6-3.9-3-6.1-1.4-1 0.6-18.7 5.5-0.2 0.8-22.9 4.2-0.2 8.7-2.3 2 2.7 3.7-2.5 1.7 0 3.2-1.5 1 0.5z",WS:"M21.9 591.3l0.3 0.1 0.3 0.1 0.3 0.2-0.1 0.3-1 0-0.3-0.1-0.3 0.1-0.2-0.1-0.4-0.2-0.3 0.1-0.3-0.2-0.2-0.2-0.6-0.3 0.1-0.4 0.4-0.2 0.4 0 0.5 0 0.9 0.4 0.5 0.1 0 0.3z m-4.5-2.8l0.6 0.6 0.2 0.7-0.1 0.3 0.1 0.3-0.3 0-0.3-0.2-0.3 0.2-0.9 0.1-0.3-0.3-0.2-0.4-0.2-0.1-0.3-0.3-0.5-0.4-0.2-0.2 0-0.2 0.4 0.1 0.3-0.1 0.6-0.2 0.4 0 0.5-0.1 0.3 0 0.2 0.2z",BR:"M665.8 489.6l3.1 0.6 0.6-1.4-1-1.2 0.6-1.9 2.3 0.6 2.7-0.7 3.2 1.4 2.5 1.3 1.7-1.7 1.3 0.2 0.8 1.8 2.7-0.4 2.2-2.5 1.8-4.7 3.4-5.9 2-0.3 1.3 3.6 3 11.2 3.1 1.1 0.1 4.4-4.3 5.3 1.7 1.9 10.1 1 0.2 6.5 4.3-4.2 7.1 2.3 9.5 3.9 2.8 3.7-0.9 3.6 6.6-2 11 3.4 8.5-0.2 8.4 5.3 7.4 7.2 4.4 1.8 4.8 0.3 2.1 2 2 8.2 1.1 3.9-2.1 10.6-2.7 4.2-7.7 8.9-3.4 7.3-4 5.5-1.4 0.2-1.3 4.7 0.9 12-1.1 9.9-0.3 4.2-1.6 2.6-0.5 8.6-5.2 8.3-0.5 6.7-4.3 2.7-1.1 3.9-6 0-8.5 2.4-3.7 2.9-6 1.9-6.1 5.1-4.1 6.4-0.3 4.8 1.3 3.5-0.3 6.5-0.8 3.1-3.4 3.6-4.5 11.3-4 5-3.2 3.1-1.5 6.1-2.9 3.6-2.1-3.6 1.8-3.1-3.8-4.3-4.8-3.6-6.3-4.1-1.9 0.2-6.3-5-3.4 0.7 6-8.7 5.3-6.3 3.3-2.6 4.2-3.5-0.4-5.1-3.2-3.8-2.6 1.3 0.7-3.7 0.3-3.8-0.3-3.6-2.1-1.1-2 1-2.1-0.3-0.8-2.4-1.1-5.9-1.2-1.9-3.9-1.8-2.2 1.3-5.9-1.3-0.4-8.7-2-3.5 1.6-1.4-0.8-3.6 1.3-2.8 0.6-5.1-1.7-4-3.2-1.8-0.8-2.5 0.5-3.7-10.7-0.3-2.8-7.5 1.7-0.1-0.3-2.7-1.2-1.9-0.5-3.7-3.4-1.9-3.5 0-2.5-1.8-3.9-1.3-2.3-2.4-6.4-1.1-6.5-5.7 0.3-4.3-0.9-2.5 0.4-4.8-7.3 1.1-2.9 2.4-4.8 2.6-1.1 1.9-2.9 0.2-4.2-0.6-3.2 1.1-2.6-0.7-0.1-9.7-4.4 3.7-5-0.1-2.3-3.5-3.8-0.3 1-2.8-3.3-3.9-2.6-5.8 1.5-1.1-0.2-2.8 3.4-1.8-0.7-3.5 1.4-2.2 0.3-3 6.3-4.4 4.6-1.2 0.8-1 5.1 0.3 2.2-17.6 0.1-2.8-0.9-3.6-2.6-2.4 0.1-4.7 3.2-1 1.1 0.7 0.2-2.5-3.3-0.7 0-4 11 0.2 1.9-2.3 1.6 2.1 1 3.8 1.1-0.8 3.1 3.4 4.4-0.4 1.1-2 4.2-1.5 2.4-1.1 0.7-2.7 4.1-1.8-0.3-1.4-4.8-0.5-0.7-4.1 0.3-4.3-2.5-1.6 1.1-0.6 4.1 0.8 4.5 1.6 1.7-1.5 4.1-1 6.4-2.4 2.1-2.5-0.7-1.8 3-0.2 1.2 1.4-0.8 2.9 2 0.9 1.2 3-1.6 2.3-1 5.4 1.4 3.3 0.3 3 3.5 3 2.8 0.3 0.6-1.3 1.8-0.3 2.6-1.1 1.8-1.7 3.2 0.6 1.3-0.3z",BS:"M580.4 366l-0.4 0.5-0.2 0.5-0.6 0.3-0.5 0-0.1-0.1-0.4 0.2-0.5 0.1-0.6-0.2-0.4 0.1-0.1-0.5 0.3-0.2 0.2-0.3 0.4-0.2 0.3-0.4 0.4 0 0.3-0.2 0.2 0.3 0.5 0.2 0.5-0.2 0.7-1 0.3 0 0 0.2-0.3 0.9z m0.2-1.7l-0.2-0.6 0.6-0.3 0.3 0.3 0 0.2-0.3 0.1-0.4 0.3z m0.4-6.5l0.2-0.1 0.5 0.3 0.4 0 0.4 0.1 0.3 0.2 0 0.3-0.2 0.1-0.5-0.4-0.4 0-0.1-0.1-0.5 0.2-0.5-0.2 0-0.1 0.2-0.5 0.2 0.2z m-3.9-1.8l-0.2 0.4 0 0.7-0.1 0.4-0.4 0.3-0.2 0.4-0.4 0.3-0.7 0.3-0.1 0.2-0.2-0.2 0.1-0.2 0.4-0.1 0.2-0.2 0-0.2 0.4-0.1 0.2-0.4 0.4-0.1 0.3-0.4-0.2-0.3-0.4 0-0.2-0.2 0.4-0.5 0.1-0.1 0.6 0z m-1.9-0.3l0.1 0.1 0.4 0 0.4 0.2-0.4 0.4-0.1-0.2-0.2 0-0.4-0.1-0.4-0.2-0.3-0.6 0.3-0.1 0.4 0.1 0.2 0.4z m-9.2-5.9l0.6 0.7 0.3 0.2 0.3 0.5-0.3-0.1-0.1-0.2-0.3-0.1-0.4-0.4-0.2-0.1-0.2-0.4 0.3-0.1z m3.7 0l0.3 0.6 0.2 0.5 0 0.4 0.2 0.2 0.2 0.7 0 0.9 0.2 0.3 0.5 0.2 0.4 0.6 0 0.8-0.4-0.7 0-0.2-0.3-0.4-0.6-0.2 0.2-0.2-0.3-0.3 0-0.3 0.2-0.3-0.1-0.5-0.3-0.4 0.1-0.1-0.3-0.4-0.1-0.6-0.2 0.1 0.1-0.7z m2.9 0l-0.3 0.2-0.3-0.1-0.1-0.2 0.6-0.2 0.1 0.3z m-8.2-1.9l-0.1-0.1-0.3-0.7 0.2 0 0.2 0.8z m10.5-0.5l-0.4 0.6-0.4 0 0.2-0.3 0.1-0.6 0.4-0.3 0.1 0.4 0 0.2z m-18.5-1.2l-0.3 0.1-0.2 0.3-0.3-0.1 0.6-0.4 0.2 0.1z m0.2 0.7l0.4 0.2 0.2-0.2 0.1-0.5 0.2-0.1 0.2 0.6 0 0.5-0.1 0.3 0.1 0.6 0 0.3-0.4 0.8-0.2 0.1-0.4-0.2 0.2-0.3 0.4-0.2-0.1-0.1-0.6 0.4-0.2-0.2 0.4-0.3-0.4 0-0.2 0.1-0.2-0.2 0-0.7 0.1-0.2-0.4-0.5 0-0.2 0.4-0.2 0.1-0.3 0.3-0.1 0.1-0.2 0.6-0.3 0.1 0.4-0.5 0.4-0.2 0.3z m13.5-0.6l0.1 0.5-0.2-0.1-0.5 0.1-0.4 0.1-0.1-0.2 0.3-0.2 0.4-0.3 0-0.2-0.4-0.2-0.2-0.6-0.1-0.6-0.4-0.4 0-0.2-0.5-0.5 0.3-0.3 0.4 0.4 0 0.5 0.2 0.3 0.1 0.5 0.3 0.5 0.4 0.3 0.1 0.3 0.2 0.3z m-10.3-5.5l0.3 0.2-0.6 0.3-0.3-0.1-0.2 0.2-0.5-0.1 0.2-0.3 0.4-0.2 0.7 0z m-4.2-0.5l0.4-0.2 0.3 0.3-0.1 0.2 0.1 0.2-0.2 0.4 0.3 0.3 0.2 0.9 0.2 0.3 0.4 0.5-0.2 0.7 0.1 0.6-0.2 0.3-0.3 0.1-0.4 0.4-0.3 0.2-0.3 0-0.3 0.2-0.3 0 0-0.4-0.3-0.2 0.1-0.6-0.3 0.3-0.2-0.1-0.6-0.4-0.3-0.3-0.2-0.3 0.7-0.3 0.5 0.7 0.1-0.6-0.3-0.1-0.1-0.3 0.1-0.3 0.6-0.6 0.2-0.4-0.1-0.1 0.3-0.4 0-0.5-0.2-0.7 0.1-0.1 0.3 0 0.2 0.3z m8.1-2.6l0.1 0.2 0.3 0.4 0.8 0.7 0.5 0.1 0.1 0.1 0.2 0.4 0.3 0.4 0.5 0.4 0 0.2-0.2 0.8-0.1 0.2 0 0.3-0.1 0.5-0.2 0.3-0.1 0.7-0.1 0.1-0.2-0.6-0.4-0.3 0.1-0.2 0.5 0.1 0.2-0.2-0.1-0.2 0.1-0.4 0.3-0.4 0.1-0.5 0-0.3-0.5-0.4-0.4-0.7-0.3-0.2-0.6-0.3-0.3-0.3-0.6-0.2-0.4 0.2 0.4-0.9 0.1 0z m-8.8-7.5l0.5 0.2 0.1 0 0.7 0.1 0.2-0.2 0.2 0.1 0.5 0 0.4-0.2 0.4 0 0.3-0.2 0.1 0.5-0.1 0.2-0.3 0-0.4 0-1.1 0.2-0.3 0-1.5 0.4-0.7 0.4-0.4 0-0.2-0.1-0.3-0.3-0.5-0.6 0.6 0.3 0.1 0.2 0.5 0 0.5-0.2 0.4-0.6-0.3-0.1 0.2-0.3 0.1-0.3 0.3 0.5z m4.3-1.3l0.8 0.2 0.2-0.1 0.4 0.3 0.9 1 0.2 0.6 0.4 0.1 0.7 0.5 0 0.2-0.2 0.5 0.2 0.7-0.4 0-0.5 0.4-0.1 0.2-0.3 1-0.1 0.9-0.2 0.2-0.3-0.2-0.1-0.4-0.4-0.1 0.3-0.5 0.2 0 0.5-0.4-0.1-0.4 0.2-0.4 0-0.3 0.2-0.3 0-0.6 0.2-0.1 0.2-0.3 0.1-0.3-0.3-0.1-0.5-0.1-0.1-0.3 0-0.2-0.4-0.2 0-0.3-0.3-0.7-0.2-0.1-0.3 0.1-0.5-0.1-0.3 0-0.6-0.4 0.5 0z",BY:"M1141.6 162.7l-3.9-0.2-0.8 0.6 1.5 2 2 4-4.1 0.3-1.3 1.4 0.3 3.1-2.1-0.6-4.3 0.3-1.5-1.5-1.7 1.1-1.9-0.9-3.9-0.1-5.7-1.5-4.9-0.5-3.8 0.2-2.4 1.6-2.3 0.3-0.5-2.8-1.9-2.8 2.8-1.3-0.4-2.4-1.7-2.3-0.6-2.7 4.7 0 4.8-2.3 0.5-3.4 3.6-2-1-2.7 2.7-1 4.6-2.3 5.3 1.5 0.9 1.5 2.4-0.7 4.8 1.4 1.1 2.9-0.7 1.6 3.8 4 2.1 1.1 0 1.1 3.4 1.1 1.7 1.6-1.6 1.3z",BZ:"M487.8 399.8l-1.7 0 1.3-7.2 0.7-5.1 0.1-1 0.7-0.3 0.9 0.8 2.5-3.9 1.1-0.1-0.1 1 1 0-0.3 1.8-1.3 2.7 0.4 1-0.9 2.3 0.3 0.6-1 3.3-1.3 1.7-1.1 0.2-1.3 2.2z",TN:"M1048.2 289.1l-0.1 4.9-2.6 1.8-1.6 2.1-3.6 2.5 0.6 2.6-0.4 2.8-2.6 1.4-2.6-11.5-3.4-2.6-0.1-1.5-4.5-3.9-0.6-4.8 3.2-3.6 1.1-5.3-1-6.1 1-3.3 5.7-2.5 3.7 0.7 0 3.3 4.4-2.4 0.4 1.2-2.5 3.2 0.1 2.9 1.9 1.6-0.5 5.6-3.5 3.2 1.2 3.5 2.8 0.1 1.4 3.1 2.1 1z",RW:"M1158.8 509.1l2.2 3.6-0.3 3.8-1.6 0.8-3.1-0.4-1.7 3.7-3.5-0.5 0.6-3.6 0.8-0.5 0.2-3.8 1.6-1.8 1.4 0.7 3.4-2z",RS:"M1102 218.2l-1.1 1.4 0.7 2.4 2.8 2.7-1.8 2-0.6 2 0.6 0.8-0.7 0.8-2.4 0.2-1.7 0.3-0.3-0.5 0.6-0.7 0.4-1.6-0.7 0.1-1.1-1.2-0.9-0.3-0.8-1-1-0.4-0.8-0.9-0.9 0.4-0.5 2.1-1.2 0.4 0.4-0.5-2.1-1.3-1.7-0.7-0.9-0.9-1.4-1.1 1.1-0.3 0.5-2.9-2.7-2.5 1.1-2.8-1.9 0.1 1.7-2.4-1.7-1.8-1.5-2.5 3.7-1.6 3.2 0.3 3 2.4 0.8 2.1 3.2 1.4 0.7 2.6 3.1 1.9 1.5-1.4 1.3 0.7-1 1.1 1 1.1z",TL:"M1692.7 562.1l0.1-1.9-0.5-1.3 0.8-1.5 4.9-1.4 4-0.3 1.8-0.8 2.1 0.8-2.2 1.8-6.1 2.8-4.9 1.8z",RE:"M1295 635.8l0.4 0 0.4 0.2 0.3 0.3 0 0.3 0.1 0.5 0.3 0.2 0.2 0.2 0.1 0.2-0.2 0.6-0.1 0.4-0.2 0.2-0.4 0.1-0.9 0-0.2-0.2-0.8-0.4-0.3-0.5 0-0.3-0.3-0.6 0.1-0.4 0.2-0.2 0.2-0.4 0.1 0 0.5-0.2 0.5 0z",TM:"M1338.3 262l-1.6-0.2-2.9-1.7-0.3 2.2-4.2 1.3 0.2 5.1-2.6 2-4 0.9-0.4 2.9-3.9 0.9-5.9-2.5-1.7-5.3-4-0.3-7.3-5.6-4.3-0.7-6.6-3.3-3.9-0.6-2 1.2-3.6-0.2-3 3.7-4.4 1.2-1.9-4.5-0.6-6.7-4.6-2.2 0.4-4.3-3.5-0.4-0.1-5.4 5.3 1.6 4.1-2-4.7-3.9-2.4-3.6-3.8 1.6 0.6 4.7-2.6-4.1 1.8-2.2 5.6-1.3 3.9 1.8 4.8 5 2.6-0.3 5.9-0.1-1.7-3.2 3.8-2.2 3.4-3.7 7.9 3.4 1.9 5 2.3 1.3 5.5-0.3 2.1 1.2 4.3 6.6 7.1 4.4 4.2 3 6.3 3.1 7.7 2.8 0.8 3.9z",TJ:"M1357 243.6l-1.4 1.9-6-1 0.6 3.6 5.5-0.5 7.1 2.1 9.6-1 3.1 6 1.5-0.7 3.7 1.5 0.5 2.5 1.8 3.6-5.4 0-3.8-0.5-2.5 2.9-2.2 0.6-1.5 1.4-2.7-2.1-0.9-5.4-1.7-0.3 0.1-2-3.3-1.4-1.7 2.2 0.2 2.6-0.6 0.9-3.2-0.1-0.9 2.9-2.1-1.2-3.4 2-1.8-0.7 1.3-6.5-2.4-4.8-4.2-1.5 0.6-2.8 4.4 0.3 1.5-3.5 0.5-4.1 6.5-1.5-0.2 3 1.3 1.7 2.1-0.1z",RO:"M1118.9 193.1l1.6 0.7 1.8 1.8 2 2.6 3.4 3.8 0.6 2.7-0.2 2.7 1.3 2.9 2.4 1.2 2.3-1.1 2.4 1.1 0.4 1.7-2.3 1.3-1.6-0.6-0.4 7.7-3.1-0.7-4-2.3-5.9 1.5-2.3 1.6-7.6-0.4-4-0.9-1.9 0.4-1.8-2.6-1-1.1 1-1.1-1.3-0.7-1.5 1.4-3.1-1.9-0.7-2.6-3.2-1.4-0.8-2.1-3-2.4 3.9-1.2 2.6-4.3 1.9-4.2 2.9-1.3 2-1.4 3.2 0.7 3.2 0 2.5 1.6 1.6-1 3.6-0.6 1-1.5 2.1 0z",GW:"M909.2 421l-0.1 2.2-0.6 0.7 0.4 2.1-0.9 0.8-1.2 0.1-1.5 1-1.7-0.1-2.6 3.1-2.9-2.6-2.4-0.5-1.3-1.8 0.1-1-1.7-1.3-0.4-1.4 3-1 1.9 0.2 1.5-0.8 10.4 0.3z",GU:"M1800.8 415.5l-0.1 0.4 0 0.6-0.1 0.3-0.3 0-0.2-0.3-0.1-0.5 0-0.5 0.6-0.5 0.1-0.2 0.1-0.5 0.2-0.1 0.1 0.2 0.4 0.1-0.2 0.5-0.5 0.5z",GT:"M488.1 387.5l-0.7 5.1-1.3 7.2 1.7 0 1.7 1.2 0.6-1 1.5 0.8-2.8 2.5-2.9 1.8-0.5 1.2 0.3 1.3-1.3 1.6-1.4 0.4 0.3 0.8-1.2 0.7-2 1.6-0.3 0.9-2.8-1.1-3.5-0.1-2.4-1.3-2.8-2.6 0.4-1.9 0.8-1.5-0.7-1.2 3.3-5.2 7.2 0 0.4-2.2-0.8-0.4-0.5-1.4-1.9-1.5-1.8-2.1 2.5-0.1 0.5-3.6 5.2 0 5.2 0.1z",GR:"M1112.7 272.6l3.1 2.2 4.1-0.4 4 0.4 0 1.2 2.8-0.8-0.5 1.9-7.6 0.5-0.1-1-6.6-1.3 0.8-2.7z m9.2-32.7l-3.2-0.2-2.7-0.6-6.2 1.6 4 3.6-2.5 1.1-2.9 0-3.1-3.3-0.9 1.4 1.6 3.8 2.9 3-1.9 1.4 3.2 2.9 2.8 1.9 0.4 3.6-5-1.7 1.8 3.3-3.3 0.6 2.5 5.7-3.5 0.1-4.6-2.8-2.4-5.1-1.3-4.3-2.3-2.9-3-3.7-0.5-1.8 2.2-3.1 0.1-2.1 1.7-0.9-0.1-1.7 3.4-0.5 1.8-1.4 2.8 0.1 0.8-1.1 1-0.2 3.8 0.2 4-1.8 3.9 2.3 4.6-0.6-0.3-3.2 2.7 1.7-1.1 4-1.2 0.7z",GQ:"M1050.3 487.3l0 7.7-8.2 0-1.9 0.3-1.1-0.9 1.9-7.2 9.3 0.1z",GP:"M643 399.9l-0.3 0-0.2-0.4 0.1-0.2 0.3-0.3 0.3 0.2 0.1 0.3 0 0.2-0.3 0.2z m-1.8-2.6l0.3 0 0 0.2 0 0.1-0.2 0.1 0 0.8 0 0.4-0.2 0.2-0.6 0.3 0-0.2-0.2-0.2-0.1-0.5 0-0.5-0.1-0.5 0-0.3 0.2-0.3 0.3-0.1 0.5 0.3 0.1 0.2z m1.4-0.4l0.4 0.1 0.5 0.4-1.3 0.3-0.3 0.1-0.4-0.3 0.1-0.6 0.2-0.1-0.1-0.6 0.1-0.2 0.3-0.2 0.3 0.4 0 0.4 0.2 0.3z",BH:"M1264.1 333.3l0.3 0.1 0.2-0.1 0.4 0.7-0.1 0.2 0.1 0.9 0 0.7-0.2 0.4-0.1-0.4-0.6-0.8 0.1-0.4-0.2-0.7 0-0.4 0.1-0.2z",GY:"M662.9 463.5l-1 5.8-3.5 1.6 0.3 1.5-1.1 3.4 2.4 4.6 1.8 0 0.7 3.6 3.3 5.6-1.3 0.3-3.2-0.6-1.8 1.7-2.6 1.1-1.8 0.3-0.6 1.3-2.8-0.3-3.5-3-0.3-3-1.4-3.3 1-5.4 1.6-2.3-1.2-3-2-0.9 0.8-2.9-1.2-1.4-3 0.2-3.7-4.8 1.6-1.8 0-3 3.5-1 1.4-1.2-1.8-2.4 0.5-2.3 4.7-3.8 3.6 2.4 3.3 4.1 0.1 3.4 2.1 0.1 3 3.1 2.1 2.3z",GF:"M677.3 487l1.5-2.8 0.5-2.9 1-2.7-2.1-3.8-0.3-4.4 3.1-5.5 1.9 0.7 4.1 1.5 5.9 5.4 0.8 2.6-3.4 5.9-1.8 4.7-2.2 2.5-2.7 0.4-0.8-1.8-1.3-0.2-1.7 1.7-2.5-1.3z",GE:"M1215.7 227.9l5.1 1.3 2.1 2.6 3.6 1.5-1.2 0.8 3.3 3.5-0.6 0.7-2.9-0.3-4.2-1.9-1.1 1.1-7 1-5.6-3.2-5.5 0.3 0.3-2.7-2.1-4.3-3.4-2.4-3-0.7-2.2-1.9 0.4-0.8 4.6 1.1 7.7 1 7.6 3.1 1.2 1.2 2.9-1z",GD:"M639.2 424.5l-0.4 0.3-0.2-0.1-0.1-0.5 0.2-0.5 0.3-0.4 0.3 0 0.1 0.3-0.1 0.7-0.1 0.2z",GB:"M956.7 158.2l-3.5-1.2-3 0.1 1.2-3.3-0.9-3.2 4-0.3 4.9 3.8-2.7 4.1z m15.9-28.7l-5.1 6.5 4.7-0.8 5.1 0-1.3 4.9-4.3 5.4 4.9 0.3 0.3 0.7 4.2 7.1 3.2 1 2.9 7 1.4 2.4 5.9 1.1-0.6 4-2.4 1.8 1.9 3.2-4.4 3.2-6.5-0.1-8.4 1.8-2.2-1.3-3.3 2.9-4.5-0.7-3.6 2.4-2.5-1.2 7.3-6.5 4.4-1.4-7.6-1-1.3-2.5 5.1-1.9-2.5-3.3 1-4 7.1 0.6 0.8-3.6-3.1-3.7-0.1-0.1-5.7-1.1-1.1-1.6 1.8-2.7-1.5-1.7-2.6 2.9-0.1-5.9-2.2-3 1.9-6.2 3.8-4.8 3.6 0.4 5.6-0.5z",GA:"M1060.5 487.3l-0.4 2.8 1.6 3.3 4.2-0.5 1.4 1.2-2.5 7.5 2.7 3.8 0.6 5-0.7 4.3-1.7 3-5-0.3-3.1-3.1-0.4 2.9-3.8 0.8-2 1.6 2.1 4.2-4.3 3.6-5.8-6.5-3.7-5.3-3.5-6.6 0.2-2.2 1.3-2 1.3-4.7 1.2-4.8 1.9-0.3 8.2 0 0-7.7 2.7-0.4 3.4 0.8 3.4-0.8 0.7 0.4z",GN:"M921.5 421.9l0.3 2.4 0.9 0 1.5-0.9 0.9 0.2 1.6 1.7 2.4 0.5 1.5-1.4 1.9-0.9 1.3-0.9 1.1 0.2 1.3 1.4 0.6 1.8 2.3 2.7-1.1 1.6-0.3 2.1 1.2-0.6 0.7 0.7-0.3 1.9 1.7 1.9-1.1 0.5-0.5 2.2 1.3 2.6 1.4 5.2-2.1 0.7-0.5 0.9 0.4 1.3-0.4 2.8-0.9 0-1.6-0.2-1.1 2.6-1.6 0-1.1-1.4 0.4-2.6-2.4-3.9-1.4 0.7-1.3 0.2-1.5 0.3 0.1-2.3-0.9-1.7 0.2-1.9-1.2-2.7-1.6-2.3-4.5 0-1.3 1.2-1.6 0.2-1 1.4-0.6 1.7-3.1 2.9-2.4-3.8-2.2-2.5-1.4-0.9-1.4-1.3-0.6-2.8-0.8-1.4-1.7-1.1 2.6-3.1 1.7 0.1 1.5-1 1.2-0.1 0.9-0.8-0.4-2.1 0.6-0.7 0.1-2.2 2.7 0.1 4.1 1.5 1.2-0.1 0.4-0.7 3.1 0.5 0.8-0.4z",GM:"M891.6 417.4l0.8-2.9 6.1-0.1 1.3-1.6 1.8-0.1 2.2 1.6 1.7 0 1.9-1 1.1 1.8-2.5 1.5-2.4-0.2-2.4-1.3-2.1 1.5-1 0-1.4 0.9-5.1-0.1z",GL:"M896.3 1.4l19.9 3-6.7 1.4-13 0.2-18.5 0.4 1.4 0.7 12.3-0.5 9.7 1.4 7-1.2 2.4 1.4-4.5 2.4 9.2-1.6 17.1-1.5 10 0.8 1.7 1.7-14.8 2.9-2.2 1-11.4 0.8 8.1 0.2-4.9 3.2-3.6 2.9-1.2 5.2 3.7 3.2-5.9 0.1-6.5 1.6 6.3 2.6-0.1 4.2-4.2 0.5 4.1 4.3-8.7 0.4 4 2-1.6 1.8-5.7 0.8-5.5 0.1 4.2 3.4-0.5 2.4-7.3-2.2-2.4 1.4 5 1.3 4.6 3.2 0.6 4.3-7.4 1-2.7-2.1-4.2-3 0.5 3.6-5.4 2.8 10.7 0.2 5.5 0.3-11.9 4.7-12.2 4.3-12.7 1.8-4.6 0.1-4.9 2.1-7.5 5.8-10.2 3.9-3 0.3-6.1 1.3-6.6 1.4-4.8 3.4-1.4 4-3.4 3.8-8.6 4.6 0.3 4.5-3.6 4.8-4.1 5.7-6.5 0.4-5-4.8-9-0.1-3.2-3.2-0.8-5.6-4.8-7.2-0.7-3.7 1.5-5.1-3.7-5.1 3.3-4.1-1.9-2 7-6.4 7.2-2.1 2.6-2.2 2.8-4.2-5.5 1.9-2.6 0.8-4.1 0.7-4.2-1.7 1.5-3.7 3-2.8 3.8-0.1 7.6 1.5-5.3-3.4-2.7-1.8-4.4 0.7-2.6-1.3 7-4.8-1.3-2-1.1-3.5-1.4-5.4-3.6-1.9 1.4-2.1-8.1-2.9-7.7-0.4-10.1 0.2-9.5 0.4-3.1-1.6-3.8-3.1 11-1.5 7.6-0.2-14.6-1.3-6.5-1.9 2.2-1.8 15.7-2.2 15-2.2 2.8-1.6-8.1-1.6 4.6-1.7 14.7-2.9 5.4-0.4 0.2-1.8 9-1.1 11-0.6 10.4 0 2.8 1.2 10.6-2.2 7.1 1.5 4.6 0.3 6.2 1.3-6.7-2.1 1.6-1.7 12.7-2.2 11.6 0.2 5.1-1.4 11.9-0.3 26.3 0.4z",GH:"M986.5 431.1l-0.4 2 2.3 3.3 0 4.7 0.6 5 1.4 2.4-1.3 5.7 0.5 3.2 1.5 4.1 1.3 2.3-8.9 3.7-3.2 2.2-5.1 1.9-5-1.8 0.2-2.6-2.4-5.5 1.5-7.3 2.4-5.3-1.5-9.2-0.8-4.8 0.2-3.7 9.8-0.3 2.5 0.5 1.8-1 2.6 0.5z",OM:"M1283.8 394.9l-2.2-4.5-5.2-10.6 16.3-6.4 2.6-12.8-3-4.6 0-2.6 1.3-2.6-0.2-2.6 2.4-1.3-1.1-0.9 0-4.2 2.8 0 3 4.4 3.3 2.3 4.1 0.9 3.4 1.1 2.9 3.7 1.7 2.1 2 0.9 0.2 1.4-1.7 3.8-0.7 1.8-2.2 2.1-1.7 4.4-2.5-0.4-1 1.6-0.7 3.2 1.1 4.3-0.5 0.8-2.5 0-3.3 2.4-0.3 3.1-1.2 1.4-3.5-0.1-2 1.6 0.2 2.6-2.6 1.8-3.1-0.6-3.6 2.2-2.5 0.3z m12.4-58.2l-1.3-2.2 1.4-2.1 0.7 0.5-0.2 2.7-0.6 1.1z",JO:"M1198.1 295.3l-0.9 1-10.4 3.2 6 6.5-1.6 1-0.7 2.2-4.1 0.9-1.1 2.3-2.1 2-6.2-1.1-0.3-0.9 1.8-10.2-0.4-2.5 0.6-1.9-0.4-4 0.7-2 6.3 2.6 9.7-6.9 3.1 7.8z",HR:"M1081.5 207.6l1.5 2.5 1.7 1.8-1.7 2.4-2.4-1.4-3.5 0.1-4.5-1.1-2.3 0.1-1 1.4-2-1.5-0.8 2.6 2.7 3 1.3 1.9 2.5 2.4 2.1 1.4 2.1 2.7 4.8 2.4-0.5 1-5-2.3-3.2-2.3-4.8-1.9-4.7-4.6 1-0.5-2.5-2.7-0.3-2.1-3.3-1-1.4 2.7-1.6-2.1 0-2.2 0.1-0.1 3.6 0.2 0.8-1 1.8 1 2 0.1-0.1-1.7 1.7-0.7 0.3-2.5 3.9-1.7 1.6 0.8 4 2.7 4.3 1.2 1.8-1z",HT:"M586.8 375.3l0.1 3.4-0.7 2.5-1.5 1.1 1.3 1.9-0.3 1.8-3.6-1.1-2.7 0.4-3.4-0.4-2.7 1.2-2.8-2 0.7-2.1 5.1 0.9 4.1 0.5 2.2-1.4-2.3-2.8 0.4-2.5-3.5-1 1.5-1.7 3.4 0.2 4.7 1.1z",HU:"M1096.2 191.9l3 1.7 0.5 1.7-2.9 1.3-1.9 4.2-2.6 4.3-3.9 1.2-3.2-0.3-3.7 1.6-1.8 1-4.3-1.2-4-2.7-1.6-0.8-1.2-2.1-0.8-0.1 1.3-4-1.1-1.4 2.8 0 0.2-2.6 2.7 1.7 1.9 0.6 4.1-0.7 0.3-1.3 1.9-0.2 2.3-0.9 0.6 0.4 2.3-0.8 1-1.5 1.6-0.4 5.5 1.9 1-0.6z",HN:"M519.6 405.5l-1.9-0.1-0.9 0.9-2 0.8-1.4 0-1.3 0.8-1.1-0.2-0.9-1-0.6 0.2-0.9 1.5-0.5-0.1-0.2 1.4-2.1 1.7-1.2 0.8-0.6 0.8-1.5-1.3-1.4 1.7-1.2 0-1.3 0.1-0.2 3.2-0.8 0-0.8 1.5-1.8 0.3-0.8-2-1.7-0.6 0.7-2.6-0.7-0.7-1.2-0.4-2.5 0.7-0.1-0.8-1.6-1.1-1.1-1.2-1.6-0.6 1.3-1.6-0.3-1.3 0.5-1.2 2.9-1.8 2.8-2.5 0.6 0.3 1.3-1.1 1.6-0.1 0.5 0.5 0.9-0.3 2.6 0.6 2.6-0.2 1.8-0.7 0.8-0.7 1.7 0.3 1.3 0.4 1.5-0.1 1.2-0.6 2.5 0.9 0.8 0.2 1.6 1.2 1.5 1.4 1.9 1 1.3 1.7z",PR:"M607.1 385.9l-0.3-0.2 0.1-0.2 0.3 0 0.1 0.1-0.2 0.3z m14.6-0.5l-0.2 0-0.9 0.3-0.6 0 0.2-0.2 0.4-0.2 0.4-0.1 0.6 0.1 0.1 0.1z m-9.5-2.4l0.6 0.1 0.6 0 0.3 0.1 0.6-0.1 0.3 0.1 0.4-0.1 0.2 0.1 0.5-0.1 0.4 0.1 0.3 0 0.8 0.2 0-0.2 0.6 0.2 0.1-0.1 0.8 0.1 0.2 0.2 0.2 0 0.6 0.3 0.2-0.1-0.1 0.7 0.2 0.3-0.9 0.3-0.3 0.4-0.3 0.6-0.3 0.2-0.3 0.1-0.4 0-0.6 0.1-0.3 0.2-0.7-0.1-0.2-0.2-0.5 0.2-0.3-0.3-0.7 0.2-0.9-0.1-0.3 0.1-0.6 0.1-0.2 0.1-0.2-0.2-0.4-0.1-0.3 0.2-0.6-0.1 0.3-0.7 0-0.4 0.2-0.4-0.2-0.6-0.2-0.1-0.1-0.4 0.4-0.2 0.2-0.1 0-0.5 0.4-0.2 0.5 0.1z",PS:"M1178.3 293.8l0.4 4-0.6 1.9-2.5 0.8 0.1-1.7 1.3-0.9-1.5-0.7 0.7-4.2 2.1 0.8z",PW:"M1747.7 453.1l-0.2 0.4 0.1 0.1-0.2 0.6 0.1 0.2-0.5 0.2-0.2-0.7 0.3-0.2-0.2-0.2 0.3-0.6 0.3-0.1 0.2 0.3z",PT:"M946.9 263.7l-2.2 1.6-2.8-0.9-2.7 0.7 0.9-5-0.3-3.9-2.4-0.6-1.1-2.4 0.5-4.2 2.2-2.3 0.5-2.6 1.2-3.8 0-2.7-0.9-2.3-0.2-2.2 1.9-1.6 2.2-0.9 1.2 3.1 3 0 0.9-0.8 3.1 0.2 1.3 3.2-2.4 1.7-0.3 5-0.8 0.9-0.3 3.1-2.3 0.5 2 3.8-1.6 4.2 1.8 1.9-0.8 1.7-2 2.4 0.4 2.2z",KN:"M636.3 392l-0.3 0-0.1-0.2 0.2-0.4 0.2 0 0.1 0.2-0.1 0.4z m-0.7-1.5l-0.1 0.3-0.2 0-0.4-0.2-0.2-0.2 0.3-0.4 0.5 0.4 0.1 0.1z",PY:"M662.5 631.4l2 3.5 0.4 8.7 5.9 1.3 2.2-1.3 3.9 1.8 1.2 1.9 1.1 5.9 0.8 2.4 2.1 0.3 2-1 2.1 1.1 0.3 3.6-0.3 3.8-0.7 3.7-0.3 5.6-4.3 5-4.2 1-6.3-1-5.8-1.7 4.2-9.8-1.1-2.8-5.9-2.5-7.3-4.8-4.6-1-11.3-10.4 1.5-7.7-0.2-3.5 2.1-5.6 9.6-1.9 5.2 0.1 5.4 3.3 0.3 2z",AI:"M634.2 384.9l-0.2 0 0.3-0.4 0.3 0-0.2 0.3-0.2 0.1z",PA:"M549.3 446.2l-0.7 0.9 1.1 3.8-1.1 1.9-1.8-0.5-0.9 3.1-1.8-1.8-1-3.5 1.4-1.7-1.4-0.4-0.9-2.1-2.8-1.8-2.4 0.4-1.3 2.2-2.4 1.6-1.2 0.2-0.6 1.4 2.5 3.5-1.6 0.8-0.8 0.9-2.7 0.4-0.8-3.9-0.8 1.1-1.8-0.4-1-2.5-2.3-0.5-1.5-0.7-2.4 0-0.2 1.4-0.6-1 0.3-1.3 0.6-1.3-0.2-1.1 0.9-0.8-1.1-0.9 0.1-2.6 2.2-0.6 1.9 2.3-0.2 1.4 2.2 0.3 0.6-0.6 1.5 1.6 2.8-0.5 2.5-1.6 3.5-1.3 2-1.9 3.1 0.4-0.2 0.6 3.1 0.2 2.4 1.2 1.8 1.9 2 1.8z",PF:"M195.3 679.3l-0.1-0.1-0.2-0.3 0.1-0.2 0.3 0.2-0.1 0.4z m10.3-38.1l-0.1 0.1-0.3-0.2 0.2-0.2 0.2 0.3z m-6.2-24.7l-0.2-0.1 0.2-0.6 0.5-0.4 0.3 0 0 0.2-0.2 0.4-0.6 0.5z m-47.7-1l0.7 0.2 0.3 0.4 0 0.3-0.2 0.1-0.5-0.1-0.4-0.6-0.1-0.2-0.6 0.2-0.2 0-0.6-0.1-0.2-0.4-0.3-0.5 0-0.3 0.1-0.2 0.5-0.1 0.5 0 0.5 0.2 0.2 0.4 0.1 0.6 0.2 0.1z m-3-1.5l-0.2 0.6-0.5-0.2 0.1-0.3 0.5-0.2 0.1 0.1z m-7.6-4.3l-0.2 0.1-0.2-0.3 0.3-0.1 0.1 0.3z m-2.1 0.4l-0.3 0.2-0.3 0-0.2-0.8 0.2-0.2 0.7 0.8-0.1 0z m-1-1.8l0.2 0.1 0 0.5-0.4-0.2-0.1-0.3 0.3-0.1z m28.3-2.9l-0.3 0.1 0.1-0.3 0.2 0.2z m38-35.9l-0.3 0 0-0.4-0.2-0.3 0.4 0.1 0.1 0.4 0 0.2z m-3.1-3.5l-0.2 0 0-0.4 0.2-0.3 0.3 0.2-0.2 0.2-0.1 0.3z m1.2-1.6l0 0.3-0.4 0.1-0.6 0 0 0.3-0.4-0.1-0.2-0.2-0.1-0.3 0.7-0.4 0.3 0 0.2 0.2 0.5 0.1z m-6.9-2.6l0 0.4 0 0.2-0.3 0-0.3-0.4 0.3-0.3 0.3 0.1z m2.4-3l0.3 0.1 0.1 0.1-0.6 0.2-0.1-0.3 0.3-0.1z m-3.7-0.6l0.9 0.1 0.3 0.3 0 0.2-0.4 0.3-0.8 0-0.1-0.5-0.1-0.4 0.2 0z",PG:"M1868.1 545.6l-1.6 0.7-2.3-2.5-2.2-4.1-0.8-4.9 0.8-0.6 0.5 1.9 1.6 1.5 2.4 4 2.5 2.2-0.9 1.8z m-21.4-8.6l-3 0.5-1 1.8-3.2 1.6-3 1.5-3.1 0-4.5-1.9-3.1-1.8 0.6-2 5 1 3.2-0.5 1-3.1 0.9-0.2 0.3 3.4 3.2-0.4 1.8-2.2 3.3-2.3-0.4-3.8 3.4-0.1 1.1 1-0.4 3.6-2.1 3.9z m-63.8 23.4l1.6-21 1-20.9 9.6 4.4 10.3 3.7 3.7 3.3 3 3.2 0.6 3.8 9.2 4 1.1 3.4-5.2 0.7 0.9 4.3 4.7 4.2 3 6.8 3.3-0.2-0.6 2.8 4.3 1.1-1.8 1.2 5.7 2.7-0.9 1.9-3.8 0.4-1.2-1.6-4.7-0.8-5.6-0.9-4-4.1-2.8-3.6-2.5-5.6-7.1-2.8-4.9 1.8-3.7 2.2 0.3 4.7-4.6 2.2-3.1-1.1-5.8-0.2z m70.7-29.7l-1.8 1.7-0.9-3.8-1.1-2.4-2.5-2.1-3.1-2.8-4-1.9 1.6-1.5 3 1.8 1.9 1.4 2.3 1.5 2.1 2.7 2.1 2.1 0.4 3.3z",PE:"M590.5 529.4l-5.1-0.3-0.8 1-4.6 1.2-6.3 4.4-0.3 3-1.4 2.2 0.7 3.5-3.4 1.8 0.2 2.8-1.5 1.1 2.6 5.8 3.3 3.9-1 2.8 3.8 0.3 2.3 3.5 5 0.1 4.4-3.7 0.1 9.7 2.6 0.7 3.2-1.1 5.5 10.4-1.1 2.1 0 4.6 0.3 5.4-1.9 3.2 1.2 2.4-1.1 2.2 2.9 5.4-2.9 6.9-1.1 3.3-2.8 1.6-5.9-3.7-0.8-2.6-11.7-6.4-10.7-7.1-4.7-3.9-2.8-5.3 0.8-1.9-5.4-8.4-6.4-11.8-6-12.8-2.4-3-2-4.7-4.6-4.2-4.1-2.6 1.7-2.8-3-6.2 1.7-4.5 4.4-4 0.7 2.7-1.6 1.5 0.3 2.3 2.3-0.5 2.3 0.7 2.5 3.3 3.1-2.7 0.9-4.3 3.4-5.6 6.7-2.5 6.1-6.8 1.7-4.1-0.8-4.9 1.5-0.6 3.8 3 1.8 3.1 2.6 1.6 3.5 6.7 4.2 0.9 3.1-1.7 2.1 1.1 3.4-0.6 4.4 3-3.5 6.6 1.7 0.1 2.9 3.4z",PK:"M1401.6 273.9l-3.8 5.4-5.7 1-8.5-1.6-2 2.8 3.3 5.6 2.9 4.4 5.1 3.1-3.8 3.7 1 4.6-3.9 6.5-2.2 6.5-4.5 6.7-6.5-0.5-4.9 6.8 4 2.8 1.4 5 3.5 3.2 1.8 5.5-12.1 0-3.2 4.3-4.2-1.6-2.2-4.6-4.9-4.9-10 1.2-9 0.1-7.6 0.9 1.1-7.4 7.5-3.4-0.9-2.9-2.7-1.1-1-5.6-5.7-2.8-2.8-3.9-3.2-3.4 9.6 3.3 5.3-1 3.4 0.8 0.9-1.4 3.9 0.6 6.6-2.7-0.8-5.4 2.3-3.7 4.1 0 0.2-1.7 4-0.9 2.1 0.6 1.8-1.8-1.1-3.9 1.4-3.8 3.1-1.7-3.1-4.2 5.2 0.2 0.9-2.3-0.8-2.5 2-2.7-1.4-3.2-1.9-2.7 2.4-2.8 5.3-1.3 5.9-0.8 2.4-1.1 2.9-0.8 4.7 3 2.9 5 9.5 2.5z",PH:"M1700.5 447.8l0.9 4.3 0.6 3.6-1.6 5.8-2.5-6.5-2.4 3.3 2.1 4.7-1.4 3-6.9-3.7-1.9-4.7 1.5-3-3.8-3.1-1.6 2.7-2.6-0.3-4 3.6-1-1.9 1.8-5.4 3.4-1.8 2.9-2.4 2.2 2.9 4.2-1.7 0.7-2.9 4-0.2-0.8-4.9 4.9 3 0.7 3.2 0.6 2.4z m-14.6-12l-1.8 2.1-1.4 4.1-1.6 1.9-3.9-4.4 1-1.8 1.3-1.8 0.2-3.9 3.1-0.4-0.5 4.3 3.6-6.2 0 6.1z m-30.3 6.2l-7.1 6.1 2.4-4.5 3.8-4 3-4.4 2.3-6.4 1.6 5.3-3.5 3.5-2.5 4.4z m17.5-16.5l3.6 2 3.5 0 0.2 2.6-2.3 2.8-3.4 1.9-0.5-3 0.1-3.3-1.2-3z m20.1-1.8l2.4 7.2-4.5-1.7 0.3 2.2 1.8 3.9-2.6 1.5-0.6-4.6-1.8-0.3-1.2-3.9 3.3 0.5-0.3-2.4-4-4.9 5.4 0.1 1.8 2.4z m-23-5.8l-0.9 5.6-2.7-3.2-3.5-4.9 4.8 0.2 2.3 2.3z m-6.5-34.9l3.8 1.9 1.4-1.7 0.8 1.6-0.4 2.7 2.6 4.6-0.6 5.3-2.9 2.1-0.1 5.2 2 5.1 3 0.7 2.4-0.7 7.5 3.5-0.1 3.5 2.1 1.6-0.3 2.9-4.7-3.1-2.5-3.4-1.2 2.4-4-3.9-5 1-3-1.4-0.1-2.7 1.6-1.6-1.9-1.5-0.4 2.3-3.3-3.7-1.3-2.8-1.1-6.1 2.6 2.1-1.1-10.1 0.8-5.8 3.4 0z",PL:"M1079.9 154.8l5.9 0.7 8.8-0.1 2.5 0.7 1.4 1.9 0.6 2.7 1.7 2.3 0.4 2.4-2.8 1.3 1.9 2.8 0.5 2.8 3.2 5.4-0.3 1.7-2.3 0.7-3.8 5.2 1.6 2.8-1.1-0.4-5-2.4-3.5 0.9-2.4-0.6-2.8 1.3-2.7-2.2-1.9 0.9-0.3-0.4-2.6-3.1-3.7-0.3-0.7-2-3.4-0.7-0.6 1.6-2.8-1.3 0.2-1.7-3.7-0.5-2.5-2-2.4-3.9 0.2-2.2-1.5-3.3-1.9-2.1 1.2-1.7-1.4-3.1 3.1-1.8 7.1-2.8 5.8-2 4.8 1 0.6 1.5 4.6 0z",ZM:"M1162.1 556.8l0.3 0.2 2.2 1.1 3.6 1.1 3.2 1.9 2.6 2.9 1.3 5.4-1 1.7-1.3 5.2 0.9 5.4-1.8 2.2-2 6 2.9 1.6-17.2 5.3 0.4 4.6-4.3 0.9-3.3 2.5-0.8 2.2-2 0.6-5.1 5.2-3.2 4.2-1.9 0.1-1.8-0.7-6.2-0.7-1-0.5 0-0.5-2.2-1.5-3.6-0.3-4.6 1.4-3.6-4-3.6-5.2 0.8-20.5 11.7 0.1-0.4-2.2 0.9-2.4-0.9-3 0.7-3.1-0.6-2 1.9 0.1 0.3 2 2.7-0.1 3.5 0.6 1.9 2.9 4.4 0.9 3.5-2 1.2 3.3 4.3 0.9 2 2.8 2.2 3.5 4.3 0-0.2-6.9-1.6 1.2-3.9-2.5-1.5-1.2 0.9-6.4 1.1-7.6-1.2-2.8 1.7-4.1 1.5-0.8 7.7-1.1 0.9 0.3-0.3 1.4 1.9 0.5 1.2 1.3 1-0.3-0.5-1.1z",BQSE:"M634.3 389.6l-0.2 0.1 0-0.2 0.1-0.1 0.1 0.2z",EH:"M938.9 324.3l-0.1 0.4-0.1 1.2-0.2 9.8-18-0.4-0.2 16.5-5.2 0.5-1.5 3.3 0.9 9.3-21.7 0-1.3 2.1 0.3-2.7 0.1 0 12.5-0.5 0.7-2.3 2.4-2.9 2-8.9 7.9-6.9 2.7-8.1 1.8-0.5 1.9-5 4.6-0.7 2 0.9 2.5 0 1.8-1.5 3.4-0.2-0.1-3.4 0.9 0z",RU:"M1689.5 177.4l13.7 11-8.9-2 3.7 9 9.6 6.4 3 4.4-6.5-3.8 0.1 4.9-4.7-5.3-3.8-6.1-5.6-6.7-2.4-4.8-6.4-8.2-8-6.1-6.8-8.4 1.9-2.8-4.4-2.8 1.3-0.9 4.9 4 6.9 5.9 5.2 6.1 7.2 6.2z m-594.9-22l-8.8 0.1-5.9-0.7 0.7-2.6 6.3-2 5.1 1.1 2.2 0.9-0.2 1.7 0.6 1.5z m453.8-107.2l-5.9 0.1-8.9-0.6-1-0.3 0.7-2 4.2-0.5 8.4 2 2.5 1.3z m12.6-9.5l-1.1 2-6.9-0.4-10.3-2-1.8-1.6 8.3 0.7 11.8 1.3z m-25.5-2.4l3.1 3.8-14.3-0.2-4.6 1.2-12.5-3.3-3.4-3.4 3.5-0.9 10.2 0.2 18 2.6z m-316.7 25l-2.2 0.4-13.5-0.7-2.3-2.3-7.9-1.4-2-2.9 3.5-1.1-1.6-2.8 5.4-4.4-4-0.6 6.7-4.5-2.5-2.3 6.8-2.6 10.3-3.2 11.7-0.9 5-1.8 6.7-0.6 4.2 1.9-1.3 1.5-11.4 2.5-9.9 2.3-8.7 4.8-2.8 5-3.3 5 3.1 4.3 10 4.4z m442.9 169.7l-1.4-1.1-1.9-3.3 2.5-0.1-3.9-7.5-4.7-5.4 2.9-2.2 6.9 1.1-0.6-6.2-2.8-6.8 0.3-2.3-1.3-5.7-6.9 1.9-2.6 2.4-7.6 0-6-5.8-9-4.5-10-2-6.2-6-4.5-3.8-3.8-2.7-7.7-6.2-6-2.2-8.6-1.9-6.2 0.2-5.1 1.1-1.7 3.1 3.7 1.4 2.5 3.4-1.3 2 0.2 6.5 1.9 2.8-4.4 3.9-7.4-2.4-5.6 0.6-3.9-2.1-3.4-0.7-4.4 4.4-5.9 1-3.6 1.6-6.8-1-4.6 0-4.9-3.2-6.6-2.9-5.4-0.9-5.7 0.8-4 1.2-8.5-2.6-3.6-4.6-6.7-1.6-4.8-0.8-7-2.5-1.3 6.4 4 3.6-2.4 4.4-8-1.6-5-0.2-4.8-2.9-5.2-0.1-5.3-1.9-5.9 2.9-6.7 5.3-4.7 1.1-1.7 0.5-4.4-3.8-6 0.9-3.3-2.7-4-1.2-4.1-3.6-3.3-1.1-6.2 1.6-8.4-3.5-1.1 3.2-18.3-15.6-8.4-4.7 0.8-2-9.2 5.8-4.4 0.3-1.1-3.3-7.1-2.1-4.3 1.5-4.4-6.3-9.1-1.3-3.1 2.5-10.9 2.3-1.7 1.5-17 2.1-1.4 2.1 5.1 4.2-4 1.6 1.5 1.6-3.5 3 9.4 4.3-0.2 2.9-7-0.3-0.8 1.9-7.3-3.2-7.7 0.1-4.4 2.6-6.6-2.5-12-4.3-7.6 0.2-8.1 6.7 0.6 4.5-6-3.6-2.2 6.9 1.8 1.2-1.7 4.8 5.3 4.2 3.6-0.2 4.3 4.2 0.2 3.2 2.8 1.1-1.4 3.7-4.6 1-3.6 6.5 6 6.1 0.4 4.2 7.3 7.5-2.5 2.6-0.6 1.6-2.4-0.5-4.3-3.8-1.5-0.2-3.6-1.5-2.1-2.6-5.1-1.3-2.9 1-1.2-1.2-7.6-3.1-7.7-1-4.6-1.1-0.4 0.8-7.6-5.4-6.2-2.4-5.1-3.7 3.6-1 3.2-5.2-3.3-2.5 6.9-2.6-0.4-1.4-4.3 1-0.4-2.7 2.2-1.8 4.8-0.4 0.3-2.1-1.8-3.5 1.3-3.2-0.4-1.9-7.7-2-2.9 0.1-3.6-2.9-3.6 1-6.7-2.2-0.1-1.2-2.3-2.7-3.9-0.3-0.8-1.9 0.9-1.3-3.8-3.5-4.9 0.6-1.5-0.3-1 1.4-1.8-0.2-2-4-1.5-2 0.8-0.6 3.9 0.2 1.6-1.3-1.7-1.6-3.4-1.1 0-1.1-2.1-1.1-3.8-4 0.7-1.6-1.1-2.9-4.8-1.4-2.4 0.7-0.9-1.5-5.3-1.5-2.1-3.6-1-2.9-2.5-1.3 1.6-1.9-2.4-5.6 2.6-3.4-0.9-1 4.5-3.3-5.4-2.8 8-7.4 3.4-3.4 0.9-2.9-7.4-3.9 0.9-3.8-4.9-4.2 1.7-4.8-6.5-6.3 2.9-4.2-7.3-3.7-0.4-3.8 3.2-0.5 6.4-2.1 3.7-1.9 7.7 3.2 11.7 1.3 17.7 6.2 4.1 2.6 1.5 3.7-3.7 2.9-6.5 1.5-20.2-4.2-2.9 0.7 8.1 4.1 1 2.6 1.9 5.8 6.1 1.7 3.8 1.5-0.2-2.8-3.4-2.4 2.1-2.2 11.6 3.6 3.2-1.4-4.3-4.2 8.1-5.4 4.1 0.3 4.6 1.9 1.1-3.8-4.7-3.3 0.8-3.3-4.3-3.4 12.3 1.8 3.6 3-5.1 0.7 1.2 3.1 4.1 1.9 6-1.2-0.4-3.5 7.7-2.7 12.4-4.6 3.2 0.2-2.5 3.3 5.4 0.6 2.1-1.9 7.7-0.1 5-2.3 6.4 3.3 2.8-3.6-6-3.1 1.2-1.8 13 1.6 6.6 1.7 18.6 6.2 1.1-2.8-5.9-2.9-0.8-1.1-5.3-0.6-0.2-2.5-4.7-4.2-1.1-1.7 4.3-4.7-0.6-4.8 2.2-1 11.4 1.4 2.9 2.9-0.9 4.2 3.7 1.7 3.8 3.7 3.8 7.4 6.7 3.3 0.5 3.7-3.5 7.8 5.3 0.8 0.5-2 3.8-1.4-0.5-2.7 1.9-2.7-4.4-3.1-0.4-3.6-4.8-0.4-2.9-3-0.4-5.4-8.2-4.3 4.7-3.5-3.7-3.7 1.9-0.1 4.2 2.8 2.1 5.1 5 1-4.6-3.8 5.1-2.1 8.1-0.3 9.6 3-6.9-4.3-4.9-5.5 5.9-1 9.5 0.2 7.8-0.7-5.6-2.6 1.3-3.3 4.2-0.2 4.9-2.4 9.3-0.7-0.2-1.4 9.3-0.4 4.3 1.1 5.4-2.6 7 0.1-1.5-2.2 0.9-2 6.2-2 8.3 1.6-3.5 1.2 9.4 0.7 4.1 2.4 1.9-1.2 10.9 0.1 11.6 2.4 5.5 1.8 2.5 2.6-2.4 1.5-6.6 2.8-1.1 1.5 5.6 0.7 7.2 1.3 2.4-1 5.8 3.3 0.2-1.3 5.2-0.8 13.6 0.8 3.8 2.4 17.4 0.8-4.8-3.9 9.5 0.9 6.1-0.1 9.9 2.7 6 3.3 0.4 2.2 10.1 4.2 9 2.1-3-5.5 9.4 2.3 5-1.4 9.8 1.6 1-1.4 7.6 0.7-9.4-4.9 2.1-2.2 40.4 3.4 7.9 3.1 16.3 4 14.8-1 9.3 0.9 6.6 2.2 5.1 3.9 7.3 1.5 3.9-1.1 7-0.1 9.3 1 6.9-0.6 14.3 4.8 2.5-1.7-8.5-3.4-1.8-2.4 15.3 1.5 8-0.3 15.8 2.5 9.5 2.4 33.2 22.1-2 2.5-6.2-0.4 8.2 3 9.1 4.7 4.2 1.5 3.8 2.4 1 1.5-10-1.2-6.6 4.3-3 0.7-1.6 4.1-2 3.6 1.7 2.7-11.5-4.1-6.1 4.6-4.7-2.2-1.4 2.6-7-0.9 3.1 3.9 1.1 5.8 2.9 2.4 6.8 1.3 9.1 8.7-4.1 0.3 3.4 5 4.7 2.6-5 3.1 5.4 7-5.7 1.5 4.5 6.2-1.7 5.8-5.7-4.3-10.5-8.9-16.2-13.6-6.5-8.3 0.1-3.6-2.8-2.8 5.7-1.3-0.2-7.5 0.7-6 2.5-4.7-6.6-8.2-4.7 0.5 3.2 4.8-3.6 6.4-12.3-7.2-9.1 2 0 9.8 7.7 3.6-8.5 1.6-6.4 0.6-4.3-4.3-8-0.9-2.5 2.9-15.1-1-13.2 1.7-3.5 11.7-5 14.2 8.2 0.8 5.7 3.8 5.8 1.3 0.4-3 5.6 0.4 12.8 6.7 4.7 5.2 1.4 6.2 5.5 7.4 5.3 9.9-1 9.1 1.3 4.3-2 7.4-2.1 7.3-0.9 3.7-4.6 3.7-3.1 0.1-5.2-3.1-4.1 4.7 0.5 2.1z m-294.6-207.9l-18.1 1.8-1.3-6.1 2.2-0.5 3.1 0.3 12.6 2.6 1.5 1.9z m-202.3-10l-4.3 0.5-2.9 0.4 0 0.7-3.6 0.7-4.5-1 1.1-1.4-7.8-0.1 6.3-0.8 5.2 0 1.5 1.1 1.3-1 2.8-0.7 5.8 0.9-0.9 0.7z m180.3 7.3l-7.1 0.6-11.7-1.3-8.1-1.7-6.6-3.2-5.9-0.8 5.4-2.9 6.2-0.9 9.6 2 13.7 4.2 4.5 4z",EE:"M1113.7 124.6l0.9 1-2.6 3.4 2.4 5.6-1.6 1.9-3.8-0.1-4.4-2.2-2.1-0.7-3.8 1-0.1-3.5-1.5 0.8-3.3-2.1-1-3.4 5.5-1.7 5.6-0.8 5.1 0.9 4.7-0.1z",EG:"M1172.1 301.4l3.9 9.4 0.7 1.6-1.3 2.6-0.7 4.8-1.2 3.4-1.2 1.1-2-2.1-2.7-2.8-4.7-9.2-0.5 0.6 2.8 6.7 3.9 6.5 4.9 10 2.3 3.5 2 3.6 5.4 7.1-1 1.1 0.4 4.2 6.8 5.8 1.1 1.3-22.1 0-21.5 0-22.3 0-1-23.7-1.3-22.8-2-5.2 1.1-3.9-1-2.8 1.7-3.1 7.2-0.1 5.4 1.7 5.5 1.9 2.6 1 4-2 2.1-1.8 4.7-0.6 3.9 0.8 1.8 3.2 1.1-2.1 4.4 1.5 4.3 0.4 2.5-1.6z",ZA:"M1159.4 644.7l2.2 9 1.1 4.6-1.4 7.1 0.4 2.3-2.7-1.1-1.7 0.4-0.6 1.9-1.7 2.4-0.1 2.2 3.1 3.5 3.2-0.7 1.4-2.8 4.1 0-1.7 4.7-1 5.3-1.7 2.9-4 3.3-1.1 0.9-2.6 3.3-1.8 3.3-3.5 4.6-6.7 6.6-4.1 3.8-4.3 3-5.9 2.4-2.7 0.4-0.9 1.8-3.2-1-2.7 1.2-5.7-1.2-3.3 0.8-2.2-0.4-5.8 2.6-4.6 1-3.5 2.4-2.4 0.2-2.1-2.3-1.8-0.1-2.2-2.9-0.3 0.9-0.6-1.7 0.3-3.8-1.5-4.3 1.8-1.2 0.1-4.9-3.3-6-2.4-5.4 0-0.1-3.6-8.3 2.8-3.2 2 1.8 0.8 2.7 2.5 0.5 3.4 1.2 2.9-0.5 5-3.3 1.1-23.7 1.4 1 3 6.1-0.6 3.9 1.1 2.3 4-0.7 2.8-2.9 2.7-1.9 1.5-3.1 2.8-1.4 2.3 0.7 2.5 1.8 4.5 0.3 3.6-1.4 0.6-2 1.2-3.1 3-0.5 1.8-2.4 2-4.3 5.2-4.8 8.1-4.7 2.2 0 2.7 1.1 1.9-0.8 2.9 0.7z m-20.3 53.2l1.1-2 3.1-1 1.1-2.1 1.9-3.1-1.7-2-2.2-2-2.7 1.4-3.1 2.5-3.2 4 3.7 5 2-0.7z",EC:"M559 502.8l0.8 4.9-1.7 4.1-6.1 6.8-6.7 2.5-3.4 5.6-0.9 4.3-3.1 2.7-2.5-3.3-2.3-0.7-2.3 0.5-0.3-2.3 1.6-1.5-0.7-2.7 2.9-4.8-1.3-2.8-2.1 3-3.5-2.9 1.1-1.8-1-5.8 2-1 1-4 2.1-4.1-0.3-2.6 3.1-1.4 3.9-2.5 5.6 3.6 1.1-0.1 1.4 2.8 4.8 0.9 1.6-1 2.8 2.1 2.4 1.5z",IT:"M1068.2 256.4l-1.7 5.1 0.9 1.9-0.9 3.3-4.2-2.4-2.7-0.7-7.5-3.2 0.5-3.3 6.2 0.6 5.4-0.7 4-0.6z m-34-19l3.3 4.5-0.4 8.5-2.4-0.4-2.1 2.1-2-1.7-0.5-7.7-1.3-3.6 2.9 0.3 2.5-2z m21.7-33.5l-0.4 3.1 1.4 2.7-4.1-1-3.9 2.3 0.4 3.1-0.5 1.8 1.9 3.2 5 3.2 2.9 5.3 6.1 5.1 4-0.1 1.4 1.4-1.4 1.3 4.8 2.3 4 1.9 4.7 3.4 0.6 1.1-0.8 2.3-3.1-3-4.6-1-1.9 4.1 3.9 2.4-0.4 3.3-2.1 0.4-2.5 5.5-2.2 0.5-0.1-2 0.9-3.4 1.1-1.4-2.3-3.7-1.8-3.2-2.2-0.8-1.8-2.7-3.4-1.2-2.4-2.6-3.8-0.4-4.3-2.8-4.9-4.2-3.7-3.6-1.9-6.3-2.6-0.7-4.2-2.1-2.3 0.8-2.9 3-2.1 0.4 0.5-2.7-2.8-0.8-1.5-4.9 1.7-1.9-1.5-2.4 0.1-1.8 2.2 1.4 2.5-0.3 2.7-2.2 0.9 1 2.4-0.2 1-2.5 3.8 0.8 2.1-1.1 0.3-2.5 3.1 0.9 0.5-1.2 4.9-1.1 1.3 2.1 7.3 1.7z",VN:"M1586.5 363.5l-6.5 5.4-3.7 6.1-0.6 4.5 5.3 6.7 6.5 8.4 5.7 4 4.1 5.1 4 11.9 0.4 11.3-4.3 4.2-6.1 4.2-4.2 5.3-6.6 6-2.3-4.1 1.2-4.4-4.4-3.6 4.6-2.6 5.9-0.5-2.8-3.8 9-5-0.1-7.7-1.8-4.3 0.2-6.4-2-4.5-4.9-4.5-4.4-5.6-5.7-7.6-7.3-3.9 1.2-2.3 3.3-1.7-3-5.6-6.8 0-3.5-5.8-4-5.1 2.7-1.6 4.4 0.1 5.3-0.8 4.1-3.4 3.1 2.4 5.3 1.2-0.3 3.7 3.1 2.6 5.9 1.7z",SB:"M1926.8 576.2l0.3 0.2-0.1 0.4 0.3 0 0 0.2-0.8 0-0.3-0.5 0.2-0.3 0.4 0z m-38.3-0.6l0.2 0 1 0.8 0.5 0.3 0.9 0.7-0.1 0.3-0.2 0-0.2 0.2-0.2-0.3-0.1 0-0.2-0.2-0.1-0.4-0.4-0.1 0-0.3-0.3-0.1-0.1 0.1-0.2 0-0.9-0.4-0.2-0.4 0.3-0.4 0.3 0.2z m36.9-1.5l-0.2 0.3-0.3-0.2 0-0.2 0.3-0.2 0.2 0.3z m-2.3-3.9l0.1 0.1 0.3 0-0.1 0.4-0.5 0.2-0.3 0-0.4 0.1-0.4 0.6-0.1-0.3-0.3 0.1-0.2-0.2 0-0.3 0.5-0.1 0-0.2 0.3-0.4 0.7 0.1 0.4-0.1z m-26.3-2.8l0.3-0.1 0.6 0.3 0.5 0.5 0.3 0.3 0.2 0 0.2 0.2 0.2 0 0.3 0.3 0.3-0.1 0.4 0.1 0.2 0.2 0.4 0 0.3-0.1 0.2 0.3 0.2 0.8 0.3 0.3 0 0.5-0.2 0.2 0.1 0.3-0.4-0.2-0.3 0.2-0.3-0.1-0.5-0.3-0.2 0-0.6-0.2-0.4-0.1-0.2-0.3-0.3-0.2-0.3-0.1-0.2-0.3-0.4-0.2 0.2-0.4-0.1-0.4 0-0.4-0.2-0.1-0.6-0.2-0.3 0.1-0.1-0.2 0.1-0.3 0.1-0.3 0.2 0z m4-3.4l-0.1 0.2 0 0.7-0.2-0.2 0-0.5 0.3-0.2z m-2.9-1.8l0.3 0.9 0.2 0.4 0 0.4 0.1 0.5-0.2 0.3-0.4-0.7-0.1 0.3-0.3-0.5 0.1-1 0.1-0.2 0-0.3 0.2-0.1z m-8.4 0.2l0.3 0 0.3-0.1 0.4 0.1 0.1-0.1 0.5 0 0.8 0 0.2 0.3 0.3 0.1 0.2 0.3 0.2 0 0.2 0.1 0.4 0.4 0 0.6 0.2 0 0.4 0.2 0.1 0.3 0 0.6-0.1 0.1-0.7 0.2-0.2 0.1-0.4-0.1-0.4-0.2-0.4 0-0.1-0.2-0.5-0.3-0.6 0-0.5 0.1-0.5 0-0.5-0.1-0.3-0.2-0.3 0.1-0.3-0.4-0.3-0.2-0.3-0.8-0.2-0.4 0.2-0.2-0.1-0.2 0-0.7 0.1-0.3 0.6-0.3 0.4 0.2 0.4 0.5 0.1 0.3 0.3 0.2z m2.3-2.4l0.5 0.5-0.3 0.3-0.3-0.2 0-0.1 0.1-0.5z m0 0l-0.2 0.4-0.1 0-0.4-0.3-0.3 0.1-0.2-0.1-0.1-0.2 0.3-0.1 0.2-0.2 0.5 0.1 0.3 0.3z m-6.6-0.5l0.1 0.2 0.2 0.1-0.2 0.4-0.4 0.2-0.3-0.4 0.6-0.5z m-8.5-1.8l0.2 0.3-0.1 0.3-0.8-0.4 0.5-0.3 0.2 0.1z m3.4 0.8l-0.3-0.2 0.1-0.3 0.3-0.3 0 0.5 0.1 0.2-0.2 0.1z m-0.9-1.8l0.2-0.1 0.4 0 0 0.2-0.4 0.3 0.2 0.2 0 0.3-0.4 0.4-0.2 0.1-0.3-0.1-0.4-0.4 0.1-0.6 0.3-0.1 0.2-0.4 0.3-0.1 0 0.3z m-3.6 0.1l-0.2 0.1-0.2 0.5 0.4 0.5-0.5-0.2 0-0.2-0.1-0.3-0.4-0.4 0-0.2 0.4-0.2 0.1-0.3 0.4-0.3 0.3 0.2 0 0.4-0.2 0.4z m12.9-0.5l0.2 0.2-0.1 0.2-0.2-0.1-0.6-0.5 0.2-0.4 0.3-0.1 0.2 0.2-0.1 0.3 0.1 0.2z m6.5-0.8l0.4 0.8 0.6 0.8 0 0.3-0.4 0.2 0 0.5 0 0.3 0.1 0.2 0.2 0 0.2 0.3 0.5 0.4 0.1 0.5 0.1 0.3 0.2 0.2-0.2 0.2 0.3 0.3-0.1 0.1 0.1 0.5-0.2 0.2 0.3 0.3 0.1 0.5 0.2 0.5 0 0.6-0.1 0.1-0.3-0.3-0.3-0.5 0-0.3-0.3-0.4-0.4-0.3-0.3-0.3-0.5-0.6-0.4-0.3-0.3-0.9-0.2-1-0.2-0.7 0-0.5-0.1-0.4 0.4-0.4-0.1-0.3-0.5-0.7-0.1-0.3 0.1-0.1 0.4 0.1 0.2 0.1 0.3-0.3 0.2 0.3z m-20.7-0.3l-0.2-0.4-0.3-0.3 0.4 0 0.1 0.7z m-0.1-1l0.4 0.1 0.1 0.1 0.1 0.5-0.2 0-0.2-0.4-0.2-0.3z m3.1 0.2l-0.1 0.3 0.2 0 0.5 0 0.4 0.7 0 0.2 0.2 0.3 0.1 0.5-0.2 0.2 0 0.3-0.6-0.2-0.2-0.2-0.3 0-0.4-0.3-0.1-0.2-0.1-0.3 0.4 0.1-0.3-0.8-0.1-0.2-0.5 0.1-0.2 0.1-0.4 0-0.3 0.4-0.4-0.2 0-0.6 0.2-0.2 0.4 0 0-0.4 0.4-0.6 0.4-0.3 0.3-0.2 0.4 0.3 0.2 0.4-0.1 0.7 0.2 0.1z m-5.9 0l-0.1 0-0.2-0.8 0.1-0.3 0-0.4 0.2 0.1 0.1 0.4 0 0.7-0.1 0.3z m3.4-0.5l-0.3 0.2-0.1-0.1-0.4 0-0.3-0.3-0.1-0.3-0.1-0.3 0.1-0.3 0.3-0.4 0.5-0.2 0.2 0.2 0.3 0.5 0 0.6-0.1 0.4z m-3-3.5l0.4 0.5 0 0.1 0.7 0.4-0.1 0.3-0.6 0.7 0 0.3-0.3 0.1 0-0.4-0.2 0 0-0.4-0.2-0.4-0.3-0.2-0.1-0.3 0.1-0.3 0.2-0.2 0.2-0.2 0.2 0z m10.3 0.1l0.3 0.2-0.2 0.1-0.5 0.1-0.1-0.3 0.2-0.2 0.3 0.1z m0.9 0l0.2-0.1 0.5 0.3 0.3 0.1 0.2 0.2 0.3 0.5 0.3 0.1 0.3 0.3 0 0.3 0.5 0.1 0 0.3 0.3 0.1 0.3-0.1 0.1 0.1 0.4 0.2 0.1 0.2 0.3-0.1 0.3 0.1 0.1 0.3 0.4 0.3 0.3 0.3 0.4 0.3 1 1-0.3 0.4 0.3 0.5 0.1 0.6-0.3-0.1-0.3-0.4-0.7-0.8-0.4-0.1-0.5-0.4-0.6-0.1-0.1-0.3-0.6-0.2-0.5-0.5-0.2 0-0.8-0.6-0.5-0.3 0-0.2-0.4-0.2-0.2-0.2-0.2-0.4-0.4-0.3-0.1-0.2 0-0.4-0.4-0.5-0.2-0.1 0-0.3 0.7 0.3z m-1.5-0.6l0.2 0.2 0.2-0.1 0.3 0.2-0.1 0.2-0.3-0.1-0.3-0.4z m-4.5-0.6l0.5 0-0.2 0.2-0.3-0.1 0-0.1z m4.3 0l-0.2 0.5-0.1-0.2 0.3-0.3z m-2.7 0.4l-0.5 0.1-0.2-0.2 0.3-0.4 0.3 0.1 0.1 0.4z m-12.5-0.9l0.2 0.3-0.2 0.2-0.3-0.1-0.1-0.1 0.4-0.3z m1.5-1.9l0.3 0.2-0.1 0.3-0.7 0.2-0.3-0.3 0.3-0.6 0.2-0.1 0.3 0.3z m1.7-0.9l0 0.5-0.2-0.2 0-0.2 0.2-0.1z m2.9-1.5l0.3 0.1 0.4 0.5 0.3 0.2 0.3 0.1 0.3 0.3 0.8 0.4 0.4 0.5 0 0.4 0.1 0.6 0.2 0.2 0.3 0.3 0.2 0 0.1 0.4 0.7 0.3 0.4-0.1 0.1 0.1 0 0.3-0.3 0.1-0.2 0.3-0.5-0.2-0.8-0.4-0.5 0-0.3-0.4-0.7-0.4-0.6-1-0.6-1-0.5-0.3-0.7-0.7 0-0.5 0-0.2 0.3-0.2 0.3 0.1 0.2 0.2z",ET:"M1207.3 408.5l3.9 0.1 5 2.6 1.5 2.2 2.6 2.1 2.5 3.7 2 2.1-1.9 2.8-1.8 3 0.5 1.8 0.2 2 3.2 0.1 1.3-0.5 1.3 1.2-1.2 2.2 2.2 3.6 2.2 3.1 2.2 2.3 18.7 7.6 4.8-0.1-15.6 19.3-7.3 0.3-5 4.5-3.6 0.1-1.5 2.1-3.9 0-2.3-2.2-5.2 2.7-1.6 2.7-3.8-0.6-1.3-0.7-1.3 0.2-1.8-0.1-7.2-5.4-4 0-1.9-2.1-0.1-3.6-2.9-1.1-3.5-7-2.6-1.5-1-2.6-3-3.1-3.5-0.5 1.9-3.6 3-0.2 0.8-1.9-0.2-5 0-0.8 1.5-6.7 2.6-1.8 0.5-2.6 2.3-5 3.3-3.1 2-6.4 0.7-5.5 6.6 1.4 1.5-4.8 3.6 2.9 3.2-1.5 1.4 1.3z",SO:"M1222.1 512.6l-3.3-5.3-0.2-23.4 4.9-7.2 1.5-2.1 3.6-0.1 5-4.5 7.3-0.3 15.6-19.3-4.8 0.1-18.7-7.6-2.2-2.3-2.2-3.1-2.2-3.6 1.2-2.2 1.9-3.5 1.9 1.2 1.2 2.7 2.7 2.7 2.8 0 5.2-1.7 6.1-0.7 4.9-2 2.8-0.4 2-1.2 3.2-0.2 1.8-0.2 2.5-0.9 3-0.7 2.5-2.2 2.2 0 0.2 1.8-0.4 3.7 0.2 3.4-1.1 2.3-1.4 7-2.4 7.1-3.3 8.2-4.6 9.4-4.7 7.2-6.6 8.8-5.6 5.2-8.4 6.4-5.3 4.8-6.2 7.8-1.3 3.4-1.3 1.5z",ZW:"M1159.4 644.7l-2.9-0.7-1.9 0.8-2.7-1.1-2.2 0-3.4-2.9-4.3-1-1.5-4.1 0.1-2.3-2.3-0.7-6.1-7-1.6-3.7-1.1-1.2-1.9-5.1 6.2 0.7 1.8 0.7 1.9-0.1 3.2-4.2 5.1-5.2 2-0.6 0.8-2.2 3.3-2.5 4.3-0.9 0.2 2.4 4.7-0.1 2.6 1.3 1.1 1.6 2.7 0.5 2.8 2-0.4 8.2-1.3 4.4-0.4 4.8 0.8 1.9-0.9 3.8-0.8 0.6-1.7 4.6-6.2 7.3z",KY:"M532.7 377.8l0.3 0.1 0.2-0.4 0.4 0.1 0.5 0 0.1 0.2-0.2 0.2-0.2-0.1-0.4 0.1-0.2 0.1-0.7 0 0.2-0.3z m8.1-2.5l-0.7 0.3 0.2-0.3 0.5 0z m0.6 0.1l-0.1-0.1 0.8-0.3-0.2 0.3-0.5 0.1z",ES:"M976.6 223.4l2 2.4 9.5 2.9 1.9-1.4 5.8 2.9 5.9-0.8 0.4 3.7-4.9 4.2-6.6 1.4-0.5 2.1-3.2 3.5-2 5.2 2 3.7-3 2.8-1.2 4.2-4 1.3-3.7 4.9-6.8 0.1-5-0.1-3.4 2.2-2.1 2.4-2.6-0.5-1.9-2.2-1.4-3.6-4.9-1-0.4-2.2 2-2.4 0.8-1.7-1.8-1.9 1.6-4.2-2-3.8 2.3-0.5 0.3-3.1 0.8-0.9 0.3-5 2.4-1.7-1.3-3.2-3.1-0.2-0.9 0.8-3 0-1.2-3.1-2.2 0.9-1.9 1.6 0.5-4.5-2-2.7 7.4-4.6 6.2 1.1 6.9 0 5.4 1.1 4.3-0.4 8.3 0.3z",ER:"M1228.9 420.3l-1.7 1.6-2.4-0.6-2-2.1-2.5-3.7-2.6-2.1-1.5-2.2-5-2.6-3.9-0.1-1.4-1.3-3.2 1.5-3.6-2.9-1.5 4.8-6.6-1.4-0.7-2.5 2-9.5 0.3-4.2 1.7-2 4-1.1 2.7-3.6 3.6 7.4 1.9 5.9 3.2 3.1 8 6.1 3.3 3.6 3.2 3.8 1.8 2.2 2.9 1.9z",ME:"M1090.6 227.2l-0.8 1.4-1.4 0.6-0.4-1.2-1.9 3.1 0.5 2.1-1.1-0.5-1.7-2.1-2.3-1.3 0.5-1 0.4-3.5 1.6-1.5 0.9-0.6 1.4 1.1 0.9 0.9 1.7 0.7 2.1 1.3-0.4 0.5z",MD:"M1129.4 210.3l-1.3-2.9 0.2-2.7-0.6-2.7-3.4-3.8-2-2.6-1.8-1.8-1.6-0.7 1.1-0.9 3.2-0.6 4 1.9 2 0.3 2.6 1.7-0.1 2.1 2 1 1.1 2.6 2 1.6-0.2 1 1 0.6-1.3 0.5-3-0.2-0.6-0.9-1 0.5 0.6 1.1-1.1 2.1-0.6 2.1-1.2 0.7z",MG:"M1267.9 588.9l0.4 7.7 1.3 3-0.7 3.1-1.2 1.8-1.6-3.7-1.2 1.9 0.8 4.7-0.7 2.8-1.7 1.4-0.7 5.5-2.7 7.5-3.4 8.8-4.3 12.2-2.9 8.9-3.1 7.5-4.6 1.5-5.1 2.7-3-1.6-4.2-2.3-1.2-3.4 0-5.7-1.5-5.1-0.2-4.7 1.3-4.6 2.6-1.1 0.2-2.1 2.9-4.9 0.8-4.1-1.1-3-0.8-4.1-0.1-5.9 2.2-3.6 1-4.1 2.8-0.2 3.2-1.3 2.2-1.2 2.4-0.1 3.4-3.6 4.9-4 1.8-3.2-0.6-2.8 2.4 0.8 3.3-4.4 0.3-3.9 2-2.9 1.8 2.8 1.4 2.7 1.2 4.3z",MF:"M634.2 386l-0.4-0.1 0.2-0.3 0.3-0.1-0.1 0.5z",MA:"M974.8 276l1.9 4.1 0.3 3.9 1.9 6.8 1.4 1.4-1 2.5-7.1 1.1-2.5 2.3-3.1 0.6-0.3 4.8-6.4 2.5-2.1 3.2-4.5 1.7-5.4 1-8.9 4.8-0.1 7.6-0.9 0 0.1 3.4-3.4 0.2-1.8 1.5-2.5 0-2-0.9-4.6 0.7-1.9 5-1.8 0.5-2.7 8.1-7.9 6.9-2 8.9-2.4 2.9-0.7 2.3-12.5 0.5-0.1 0 0.3-3 2.2-1.7 1.9-3.4-0.3-2.2 2-4.5 3.2-4.1 1.9-1 1.6-3.7 0.2-3.5 2.1-3.9 3.8-2.4 3.6-6.5 0.1-0.1 2.9-2.5 5.1-0.7 4.4-4.4 2.8-1.7 4.7-5.4-1.2-7.9 2.2-5.6 0.9-3.4 3.6-4.3 5.4-2.9 4.1-2.7 3.7-6.6 1.8-4 3.9 0.1 3.1 2.7 5.1-0.4 5.5 1.4 2.4 0z",UZ:"M1352.7 230.7l1.7 0.6-3 4.1 4.6 2.4 3.2-1.6 7.2 3.4-5.3 4.6-4.1-0.6-2.1 0.1-1.3-1.7 0.2-3-6.5 1.5-0.5 4.1-1.5 3.5-4.4-0.3-0.6 2.8 4.2 1.5 2.4 4.8-1.3 6.5-4.3-1.4-3 0-0.8-3.9-7.7-2.8-6.3-3.1-4.2-3-7.1-4.4-4.3-6.6-2.1-1.2-5.5 0.3-2.3-1.3-1.9-5-7.9-3.4-3.4 3.7-3.8 2.2 1.7 3.2-5.9 0.1-6.2-23.4 12-3.7 1.1 0.5 9.2 4.5 4.8 2.4 6.6 5.7 5.7-0.9 8.6-0.5 7.6 4.6 1.5 6.4 2.6 0.1 2.5 5.2 6.7 0.2 2.3 3 2 0 0.9-4.6 5.4-4.4 2.6-1.2z",MM:"M1548.4 364.2l-4.1 4.2-0.8 2.3-3 1.5-2.8 2.8-3.9 0.3-1.5 6.9-2.2 1.2 3.5 5.6 4.1 4.7 2.9 4.3-1.4 5.5-1.8 1.2 1.8 3.2 4.3 5.1 1 3.6 0.2 3 2.7 5.9-2.6 6-2.2 6.6-0.9-4.8 1.3-4.9-2.2-3.8-0.2-7-2.6-3.4-2.7-7.6-2-8.1-3.1-5.4-3.2 3.3-5.8 4.5-3.3-0.5-3.6-1.5 0.9-8-2-6-5.3-7.4 0.3-2.3-3.4-0.9-4.6-5.2-1.1-5.2 2.1 1-0.6-4.6 2.5-1.5-1-2.7 1-2.2-0.9-6.7 4.6 1.5 1.6-5.3-0.3-3.1 2-5.4-0.9-3.7 6.2-4.4 4.2 1.1-1.4-3.9 1.7-1.2-1-2.4 3.1-0.5 2.7 3.8 2.7 1.5 1.3 4.9 0.9 5.3-4.2 5.4 0.7 7.6 5.6-1.1 2.4 5.9 3.7 1.3-0.8 5.3 4.5 2.4 2.6 1.2 3.8-1.9 0.5 2.7z",ML:"M1010.2 378.8l0.1 14.8-3.1 4.3-0.4 4-5 1-7.7 0.5-2 2.3-3.6 0.3-3.6 0-1.4-1.2-3.1 0.9-5.3 2.7-1.1 2-4.3 2.8-0.8 1.7-2.4 1.3-2.7-0.9-1.5 1.6-0.9 4.4-4.5 5.3 0.2 2.2-1.6 2.7 0.4 3.7-2.4 1-1.3 0.8-0.9-2.7-1.6 0.7-1-0.1-1 1.8-4.4 0-1.5-1-0.8 0.6-1.7-1.9 0.3-1.9-0.7-0.7-1.2 0.6 0.3-2.1 1.1-1.6-2.3-2.7-0.6-1.8-1.3-1.4-1.1-0.2-1.3 0.9-1.9 0.9-1.5 1.4-2.4-0.5-1.6-1.7-0.9-0.2-1.5 0.9-0.9 0-0.3-2.4 0.3-2-0.5-2.4-2-1.8-1.1-3.7-0.2-4 1.9-1.2 1-3.8 1.8-0.1 3.9 1.8 3.2-1.3 2.1 0.4 0.9-1.4 22.5-0.1 1.3-4.5-1-0.8-2.5-27.7-2.4-27.7 8.5-0.1 18.6 14 18.7 14 1.3 3 3.5 1.8 2.6 1.1 0.1 4.1 6.2-0.7z",MN:"M1496.2 181.5l4-1.2 5.7-0.8 5.4 0.9 6.6 2.9 4.9 3.2 4.6 0 6.8 1 3.6-1.6 5.9-1 4.4-4.4 3.4 0.7 3.9 2.1 5.6-0.6 0.6 4.7 0.3 6.3 2.8 2.5 2.3-0.8 5.5 1 2.5-2.3 5.2 2 7.2 4.4 0.8 2.2-4.4-0.7-6.8 0.8-2.5 1.8-1.3 4.2-6.4 2.4-3.2 3.4-5.9-1.3-3.2-0.6-0.5 4.1 2.9 2.4 1.9 2.1-2.4 2.2-2 3.4-4.9 2.2-7.6 0.2-7.2 2.2-4.4 3.4-3.3-1.9-6.2 0-9.4-3.9-5.6-0.9-6.4 0.9-11.3-1.5-5.6 0.2-4.7-3.8-5-5.8-3.4-0.7-8-4-7.2-0.8-6.5-1.1-3-2.8-1.4-7.3-5.8-5.1-8.2-2.3-5.7-3.3-3.3-4.4 4.7-1.1 6.7-5.3 5.9-2.9 5.3 1.9 5.2 0.1 4.8 2.9 5 0.2 8 1.6 2.4-4.4-4-3.6 1.3-6.4 7 2.5 4.8 0.8 6.7 1.6 3.6 4.6 8.5 2.6z",MH:"M1953.8 456l1 0.4-0.2 0-0.8-0.4z",MK:"M1105.5 236.6l-1 0.2-0.8 1.1-2.8-0.1-1.8 1.4-3.4 0.5-2.3-1.5-1-2.7 0.5-2.2 0.7 0.1 0.1-1.3 2.9-1 1.2-0.3 1.7-0.3 2.4-0.2 2.8 2.1 0.8 4.2z",MU:"M1307.7 630.8l0.4 0.9-0.2 0.6-0.4 0.4 0.1 0.3-0.3 0.3-0.5 0.2-0.5 0-0.6-0.1-0.1 0.1-0.3-0.3 0.2-0.2 0.1-0.4 0.1-0.7 0.2-0.4 0.5-0.4 0.1-0.2 0.2-0.5 0.4-0.3 0.4 0.1 0.2 0.6z m31.8-2.1l-0.3-0.3 0.4-0.2 0.3-0.1 0.2 0.1-0.1 0.2-0.5 0.3z",MT:"M1063.9 271.7l-0.2 0.3-0.5-0.2-0.5-0.3 0-0.5-0.1-0.1 0.6 0 0.4 0.3 0.2 0.2 0.1 0.3z m-1.6-1.1l-0.5-0.1 0-0.2 0.4-0.1 0.4 0.3-0.3 0.1z",MW:"M1182.3 588.9l0.4 0 1.9 2.1 2.2 4.6 0.1 8.3-2.5 1.3-1.9 4.5-3.5-4-0.2-4.5 1.3-3-0.2-2.6-2.2-1.6-1.5 0.6-3.2-3.1-2.9-1.6 2-6 1.8-2.2-0.9-5.4 1.3-5.2 1-1.7-1.3-5.4-2.6-2.9 5.5 1.2 1 1.7-0.1 0.8 1.8 4.1 0.2 7.7-1.8 3.6 1.6 4.7-0.2 2.8 1.2 1.9-0.1 2.4 0.9 1.4 1-1.6 1.9 2.5 0.2-0.8-1-3.4-1.1-0.3-0.1-0.9z",MV:"M1402.9 474.8l0 0.2-0.2-0.1 0.2-0.2 0 0.1z",MQ:"M644 406.9l0 0.2 0.4-0.1-0.2 0.5 0.2 0.2 0 0.2 0.2 0.2 0.2 0.9-0.3 0.3-0.1-0.4-0.1 0.1-0.6-0.1-0.4 0-0.2-0.3 0.6-0.5-0.4 0-0.4-0.4-0.1-0.5-0.2-0.5 0.3-0.4 0.4 0.1 0.5 0.3 0.2 0.2z",MP:"M1802.5 411.1l-0.1 0.1-0.2-0.1-0.1-0.2 0.5-0.2 0.2 0.2-0.3 0.2z m1.5-5.1l-0.2 0-0.3-0.6 0.1-0.3 0.3 0 0 0.3 0.2 0.2-0.1 0.4z m0.5-1.6l-0.2 0.2 0.1 0.3-0.4 0 0-0.7 0.1-0.2 0.4-0.2-0.1 0.5 0.1 0.1z m-1.9-7.5l-0.3 0-0.1-0.2 0.4 0 0 0.2z m-1.9-11l-0.1-0.2 0-0.5 0.3-0.1 0.2 0.3-0.4 0.5z m-1.2-4.3l-0.4-0.1 0-0.3 0.2-0.1 0.2 0.2 0 0.3z",MS:"M638.2 393.9l0.2 0.4 0 0.3-0.2 0.2-0.2-0.1-0.1-0.3 0.3-0.5z",MR:"M959.2 341.5l-8.5 0.1 2.4 27.7 2.5 27.7 1 0.8-1.3 4.5-22.5 0.1-0.9 1.4-2.1-0.4-3.2 1.3-3.9-1.8-1.8 0.1-1 3.8-1.9 1.2-3.6-4.4-3.4-4.8-3.6-1.7-2.7-1.8-3.1 0-2.8 1.4-2.7-0.5-2 2-0.4-3.4 1.6-3.2 0.8-6-0.4-6.4-0.6-3.2 0.6-3.2-1.4-3-2.8-2.8 1.3-2.1 21.7 0-0.9-9.3 1.5-3.3 5.2-0.5 0.2-16.5 18 0.4 0.2-9.8 20.5 15.6z",UG:"M1179 474.5l2.7 4.5 0.7 3.2 2.6 7.4-2.1 4.7-2.8 4.2-1.6 2.6 0 0.3-0.2-0.4-3-1.3-2.4 1.6-3.6 0.9-2.6 3.7 0.3 2.5-6.2-0.1-2 0.8-3.4 2-1.4-0.7 0.1-4.8 1.3-2.5 0.3-5.1 1.2-3 2.2-3.3 2.1-1.8 1.9-2.2-2.3-0.9 0.3-7.5 2.3-1.8 3.6 1.5 4.5-1.5 4 0 3.5-3z",MY:"M1564.3 461.9l1.4 0.6 3.5 3.9 2.5 4.3 0.6 4.3-0.5 2.9 0.6 2.2 0.5 3.8 2.1 1.8 2.3 5.7 0 2.1-4 0.5-5.5-4.8-6.8-5.1-0.8-3.3-3.4-4.3-1-5.3-2.2-3.5 0.4-4.7-1.4-2.7 0.9-1.1 4.8 2.8 0.6 3.3 3.7-0.8 1.7-2.6z m89.8 13.4l-5-1.1-6.5 0-1.7 7.3-2.1 2.2-2.6 8.9-4.6 1.4-5.4-1.8-2.7 0.6-3.3 3.2-3.6-0.4-3.7 1.3-3.9-3.7-1-4.3 4.2 2.2 4.3-1.2 1-5.4 2.4-1.2 6.8-1.4 3.8-5.1 2.6-4 2.8 3.3 1.1-2.2 2.7 0.2 0.1-4.1 0.1-3.2 4.1-4.4 2.6-5 2.3-0.1 3.1 3.3 0.4 2.8 3.8 1.8 4.8 1.9-0.2 2.5-3.8 0.3 1.1 3.2-4 2.2z",MX:"M449.3 335.9l2.2-0.2-3.2 5.7-1.8 4.6-1.8 8.6-1.1 3.1 0.4 3.5 1.3 3.2 0.4 4.9 3 4.8 0.8 3.7 1.7 3.1 5.7 1.7 1.9 2.7 5.2-1.8 4.3-0.6 4.4-1.2 3.6-1.1 3.9-2.6 1.8-3.7 1.2-5.4 1.2-1.9 4-1.7 6.1-1.5 4.9 0.3 3.4-0.6 1.2 1.4-0.6 3.1-3.5 3.8-1.8 3.9 0.9 1.1-1.2 2.8-2.1 5-1.2-1.7-1.1 0.1-1.1 0.1-2.5 3.9-0.9-0.8-0.7 0.3-0.1 1-5.2-0.1-5.2 0-0.5 3.6-2.5 0.1 1.8 2.1 1.9 1.5 0.5 1.4 0.8 0.4-0.4 2.2-7.2 0-3.3 5.2 0.7 1.2-0.8 1.5-0.4 1.9-5.6-6.9-2.6-2.1-4.4-1.7-3.2 0.5-4.8 2.4-2.9 0.6-3.7-1.7-4.1-1.2-4.8-2.9-4.1-0.9-5.9-3-4.3-3.1-1.1-1.7-3.1-0.4-5.4-2-1.9-2.9-5.4-3.7-2.2-4-0.8-3.2 1.9-0.6-0.3-1.8 1.6-1.7 0.4-2.2-1.5-2.9 0-2.5-1.3-3.3-3.8-6.4-4.6-5-1.9-4-4.1-2.6-0.7-1.6 1.7-3.9-2.4-1.5-2.5-3.2-0.2-4.4-2.8-0.6-2.3-3.3-1.7-3.2 0.3-2-1.5-4.8-0.3-4.9 0.8-2.5-3.1-2.6-1.9 0.3-2.4-1.7-1.8 2.6-0.1 3-1 4.9 1 2.6 2.8 4.4 0.4 1.6 0.7 0.4 0.1 2.2 1-0.1 0 4.2 1.3 1.6 0.5 2.3 2.7 3.2 0.4 6 1 2.8 0.9 3-0.3 3.4 2.6 0.2 1.6 2.9 1.5 2.9-0.3 1.2-2.8 2.3-1 0-0.7-3.9-2.9-3.7-3.4-3.1-2.5-1.6 1.2-4.7-0.1-3.5-2.1-2-3.1-2.8-0.9 0.8-1-1.7-3-1.5-2.2-3.8 0.5-0.4 2.1 0.3 2.7-2.4 1-2.9-2.9-4.6-2.6-1.7-0.8-4-0.6-4.3-0.8-5.1-0.2-5.8 6.3-0.5 7.1-0.7-0.9 1.3 7 3.1 10.9 4.5 10.8 0 4.3 0 0.8-2.7 9.4 0 1.3 2.3 2.1 2.1 2.4 2.8 0.8 3.3 0.4 3.6 2.3 1.9 4 1.9 4.8-5 4.5-0.2 3.2 2.6 1.6 4.4 0.9 3.8 2.4 3.6 0.2 4.5 0.9 3 3.9 2 3.6 1.4z",IL:"M1179.1 288.2l0.4 2.6-0.6 1 0.1 0-0.7 2-2.1-0.8-0.7 4.2 1.5 0.7-1.3 0.9-0.1 1.7 2.5-0.8 0.4 2.5-1.8 10.2-0.7-1.6-3.9-9.4 1.4-2.1-0.4-0.4 1.1-3 0.6-4.8 0.6-1.7 0.1 0 1.8 0 0.4-1.1 1.4-0.1z",FR:"M1035.7 231.4l-1.5 4.9-2.4-1.3-1.3-4.2 0.9-2.4 3.2-2.4 1.1 5.4z m-21.3-46.4l1.1 0.5 1.4-0.1 2.4 1.6 7.2 1.2-2.4 4.2-0.4 4.5-1.3 1.1-2.3-0.6 0.2 1.6-3.6 3.5 0 2.9 2.4-1 1.8 2.7-0.1 1.8 1.5 2.4-1.7 1.9 1.5 4.9 2.8 0.8-0.5 2.7-4.5 3.6-10.2-1.7-7.4 2.1-0.6 3.8-5.9 0.8-5.8-2.9-1.9 1.4-9.5-2.9-2-2.4 2.7-3.8 1-12.6-5.1-6.6-3.7-3.2-7.6-2.4-0.4-4.6 6.5-1.3 8.3 1.6-1.5-7.1 4.7 2.7 11.4-4.8 1.5-5.1 4.2-1.3 0.8 2.2 2.2 0.1 2.4 2.5 3.5 2.9 2.5-0.5 4.4 2.9z",FI:"M1104.1 70.1l0.4 3.8 7.3 3.7-2.9 4.2 6.5 6.3-1.7 4.8 4.9 4.2-0.9 3.8 7.4 3.9-0.9 2.9-3.4 3.4-8 7.4-8 0.5-7.6 2.1-7.1 1.3-3.2-3.2-4.7-1.9 0.1-5.8-3-5.2 1.6-3.4 3.3-3.5 8.8-6.2 2.6-1.2-0.9-2.4-6.5-2.6-1.8-2.2-1.8-8.5-7.2-3.7-6-2.7 2.2-1.4 5.1 2.8 5.3-0.2 4.7 1.3 3.4-2.4 1.1-4 5.9-1.8 5.8 2.1-0.8 3.8z",FJ:"M1989.1 624.9l-0.3 0.1 0.1-0.4 0.2 0.3z m-7.5-1.6l0.2 0.2 0.3 0.1 0 0.3-0.5 0.2-0.5-0.3-0.5 0.3-0.3 0-0.2 0.3 0.1 0.3-0.4-0.1-0.1 0.2-0.4-0.1-0.1 0.1-0.4-0.2 0.3-0.1 0.2-0.1 0.2-0.2 0.4 0.1 0.3-0.3 0.2-0.3 0.5-0.1 0.5-0.3 0.2 0z m0.9-0.1l-0.2 0.1-0.2-0.1 0.2-0.2 0.2 0.2z m5.9-6.1l0.1 0.5-0.2 0.6-0.1-0.2-0.1-0.4-0.2 0 0.1-0.5 0.2-0.1 0.2 0.1z m-2.6-1.4l-0.1 0-0.1-0.4 0.2-0.2 0.4-0.1 0 0.4-0.2 0.2-0.2 0.1z m-2.2-2.5l-0.1 0.4 0.3 0.2 0.1 0.3 0.3 0.4 0.3 0.1 0.1 0.3 0.2 0 0.1 0.4-0.3 0.5 0.1 0.2 0.1 0.5-0.3 0.2 0 0.5 0.2 0 0.1 0.5-0.3 0.3-0.6 0.2-0.1-0.2-0.3 0.1-0.1 0.2-0.3-0.3-0.8 0.3-0.8 0.7-0.3-0.1-0.4 0.2-0.3-0.1-0.6 0.1-0.3-0.2-0.9-0.3-0.3-0.1-0.6-0.1-0.2-0.1-0.5-0.2-0.2-1 0.3-0.7 0.3 0 0.3-0.1 0.1-0.5 0.3-0.1 0.1-0.4-0.2-0.2 0.6-0.4 0.4-0.5 0.1 0.1 0.6-0.5 0.2 0 0.9-0.4 0.6 0.2 0.3-0.1 0.5-0.2 0.6-0.1 0.3-0.3 0.4 0.3z m6.3 0.2l-0.2 0.1 0.2-0.9 0.4 0-0.1 0.4-0.3 0.4z m-11.4-2l-0.2 0.3-0.5 0.3 0-0.3 0.4-0.4 0.3 0.1z m14.9-0.4l-0.2-0.2 0.1-0.2 0.5-0.5 0.3-0.5-0.3 1.1-0.4 0.3z m1-5l-0.4 0.4-0.8 1.1-0.3 0.1-0.7 0.4-0.2 0.6-0.4 0.2-0.2 0.3-0.1 0.2 0.3 0.1 0.6-0.3 0.1-0.1 0.3-0.3 0.2-0.3 0.6-0.3 0.3-0.3 0.6-0.3 0 0.3-0.5 0.7-0.2 0.1 0.1 0.6-0.3 0.3-0.3-0.3-0.4 0-0.5 0.1-0.4 0.3-0.7 0.1-1 0 0.5-0.5-0.4-0.2-0.6 0.2-0.4 0.2 0 0.2-0.3 0.1-0.2 0.1-0.1 0.4-0.2 0.3-0.3-0.1 0-0.2-0.4-0.1-0.4 0.2-0.2 0.5-0.3 0.2-0.3 0 0-0.3 0-0.4-0.2-0.4 0.1-0.2-0.1-0.1-0.6 0.2 0-0.4 0.4-0.3 0.1 0-0.1-0.5 0.3-0.1 0.5 0.3 0.6-0.4 0.2 0 0.3-0.3 0.2 0 0.3-0.3 0-0.2 0.8-0.1 0.9-0.3 0.3-0.1 0.4 0.1 0.5-0.2 0.2-0.4 0.2 0 0.2-0.4 0.4 0.1 0.1-0.2 0.2 0.1 1-0.5 0.1 0.3 0.2-0.1 0.2 0.1 0.3-0.3 0.5-0.2 0.1 0.1-0.5 0.4-0.2 0z",FK:"M697.4 836.2l0.4-0.1 0 0.4-0.8-0.3 0.3-0.3 0.2-0.1-0.1 0.4z m-0.7-1.4l0.2 0.2 0.4 0.1 0.1 0.4-0.3 0.1-0.1-0.4-0.2-0.2-0.1-0.2z m6.2-0.8l0.2 0.1-0.1 0.4-0.3-0.1-0.2-0.3 0.3-0.2 0.1 0.1z m-12.1-0.9l0 0.2 0.1 0.3-0.4 0.1-0.3-0.1-0.2-0.2-0.3-0.1-0.1-0.4 0.2 0 0.1-0.2 0.3-0.1 0.2 0.1-0.3 0.3 0.3 0.1 0.3-0.3 0.1 0.3z m2.9-1.3l-0.4-0.2-0.5-0.3-0.8-0.3 0.1-0.4-0.7-0.1-0.3-0.1-0.3-0.4 0.2-0.1 0.8 0.3 0.8 0.4 0.5 0.1 0.4 0.1 0.3-0.3 0.1-0.3 0.2 0.1 0.4-0.2 0.1 0.2 0.5 0 0 0.3 0.3 0 0.9-0.2 0.1 0.2 0.3 0 0.4-0.1-0.3-0.2-0.1-0.3 0.4 0 0.6 0.2 0.1 0.4-0.4 0.5 0.1 0.3-0.2 0.2-0.4 0-0.3 0.8-0.3 0.3-0.3 0.6-0.1 0.3-0.5 0.3-0.1-0.2-0.4 0 0 0.3-0.8-0.1-0.1 0.1 0.3 0.4-0.1 0.1-0.2 0.4-0.4 0-0.3 0.4-0.3 0.1-0.2 0-0.2-0.3-0.4-0.3 0-0.2-0.5 0 0.6 0.4-0.4 0-0.5-0.3-0.2-0.1-0.4-0.2-0.3-0.2 0-0.2 0.7 0.2 0.6-0.2-0.5-0.2-0.1-0.1 0.9 0 0.4 0.1 0.3 0.2 0-0.2 0.4-0.1 0.1-0.3-0.4-0.2-0.2-0.5 0.1-0.2 0.6 0 0.2 0.1 0.4-0.1-0.1-0.3-0.8 0 0 0.1-0.4 0.2-0.6 0-0.5-0.3 0-0.3 0.7 0.2 0.6-0.2 0.3 0 0.5-0.1 0.4 0.2 0-0.1-0.4-0.1z m1.6-1.9l0.3 0 0.3 0.3-0.3 0.1-0.3 0 0-0.4z m-1 0l-0.3 0-0.1-0.3 0.4 0.1 0 0.2z m-1.4-0.2l0.6-0.1 0.2 0.3-0.3 0.4-0.2-0.1-0.3 0.1-0.3-0.2 0.3-0.2 0.3 0.2 0.1-0.2-0.5-0.1 0.1-0.1z m7.2 0.1l0.6 0 0.1-0.1 0.5-0.1 0.4 0.2 0.4 0.3 0.1 0.3 0 0.1-0.5 0.1-0.2-0.3-0.2-0.1-0.1 0.3 0.2 0.1 0.1 0.3 0.3-0.2 0.2 0.1 0.1 0.4 0.3 0.1 0.1-0.1 0.5 0.3 0.1-0.4-0.8-0.2 0.1-0.5 0.3-0.2 1.5-0.1 0.7 0.6 0.2 0.1 0 0.4-0.6-0.1-0.5-0.2-0.6 0.2 0.3 0.2 0.4 0.1 0.9 0.1 0.3 0.1 0.1 0.2-0.2 0.5-0.5 0-0.1 0.1-0.7 0.1-0.5 0.2 0.3 0.2-0.9 0.4-0.4-0.1-0.5 0.1-0.7 0-0.6-0.2-0.6-0.4 0 0.3 0.3 0.2 0.6 0.2 0.3 0.3 0.2-0.1 0.4 0.1 0.4 0.3-0.2 0.1 0.2 0.5-0.2 0-0.3-0.3-0.4-0.1-0.5 0.4-0.2-0.3-0.3-0.2-0.3 0-0.5-0.2-0.2 0.1 0.2 0.4 0.3 0 0.2 0.2-0.2 0.1 0.7 0.6-0.2 0.1-0.4-0.2-0.3 0-0.3-0.3-0.3-0.1-0.3-0.1-0.1 0.6 0.5 0.1 0 0.3-0.1 0.2-0.4-0.1-0.1-0.2-0.5-0.2-0.1-0.2-0.2 0-0.4-0.5 0.3-0.4 0-0.2-0.4-0.5 0-0.2 0.5-0.2-0.1-0.2 0.5-0.1-0.1-0.3 0.6-0.4 0.6-0.2 0.4 0.4 0 0.4 0.3-0.1-0.5-0.6 0.1-0.3-0.6-0.2-0.4-0.3 0.1-0.2 0.4 0.2 0.1-0.5-0.7-0.2 0.1-0.3 0.2 0.1 0.3-0.2 0.3 0 0.2-0.3-0.3-0.3 0.4 0 0.5 0.2z m-5-0.4l0.7 0 0.2 0.2 0.2-0.2 0.2 0.1 0.2 0.3-0.2 0.1-0.4-0.1-0.4-0.2-0.8-0.1 0.1-0.2 0.2 0.1z m-6.8-1.1l-0.5-0.1 0.1-0.2 0.4 0.3z",FM:"M1909.4 467.7l-0.2 0.3-0.5-0.1 0-0.2 0.5-0.4 0.2 0.4z m-28.2-10.6l0.4 0 0.3 0.2 0.1 0.4-0.2 0.1 0.1 0.2-0.1 0.2-0.6 0-0.2-0.2 0-0.2-0.2-0.3 0-0.3 0.4-0.1z m-114.7-16.6l0.1 0.3-0.3 0-0.3 0.5-0.1-0.1 0.3-0.8 0.4-0.1-0.1 0.2z",FO:"M955.6 112l0.4 0.2 0.3 0 0.1 0.2 0 0.4 0.2 0.3-0.1 0.2-0.6-0.4-0.2-0.4-0.2-0.2-0.2-0.3 0.3 0z m0.3-1.6l0.8 0.2 0.2 0.2-0.1 0.4-0.1 0.1-0.4-0.5-0.5-0.1-0.2-0.4 0.3 0.1z m-1.3-1.4l0.1 0.2 0.3 0 0 0.3-0.4 0-0.2 0.2-0.6-0.2-0.2-0.1-0.2-0.3 0.5-0.1 0.1-0.1 0.6 0.1-0.3-0.2 0.2-0.8 0.5 0.1 0.3 0.5 0 0.1 0.6 0.2 0.3 0.5 0.3 0.2-0.1 0.6-0.5-0.4-0.2-0.2-0.2-0.1-0.1-0.2-0.2-0.2-0.5 0-0.1-0.1z m3.7-0.6l-0.2-0.2 0.3-0.2-0.1 0.4z m-2.4 0.5l-0.4-0.2-0.3-0.3-0.1-0.3 0.1-0.1 0.4-0.1 0.3 0 0.6 0.4-0.1 0.2 0.1 0.2 0.6 0.2 0 0.2-0.4 0.3-0.8-0.5z m2.2-0.6l-0.3 0.4-0.3-0.2-0.3 0 0.2-0.3-0.1-0.4 0.1-0.1 0.2 0.4 0.5 0.2z m-0.9 0l-0.2 0-0.2-0.4 0.1-0.3 0.2 0.4 0.1 0.3z",NI:"M519.6 405.5l-0.5 0.7-0.5 1.4 0.4 2.3-1.5 2.2-0.8 2.6-0.5 2.8 0.2 1.7-0.1 2.9-0.9 0.6-0.7 2.8 0.2 1.7-1.2 1.6 0.1 1.7 0.8 1.1-1.4 1.4-1.7-0.5-0.8-1.3-1.8-0.5-1.3 0.8-3.6-1.7-0.9 0.8-1.8-2-2.5-2.6-1.1-2.1-2.2-2.1-2.5-2.9 0.7-1 0.8 1 0.5-0.4 1.8-0.3 0.8-1.5 0.8 0 0.2-3.2 1.3-0.1 1.2 0 1.4-1.7 1.5 1.3 0.6-0.8 1.2-0.8 2.1-1.7 0.2-1.4 0.5 0.1 0.9-1.5 0.6-0.2 0.9 1 1.1 0.2 1.3-0.8 1.4 0 2-0.8 0.9-0.9 1.9 0.1z",NL:"M1016.5 177.1l-2.8-1.5-3.1-2.7-4.5 1.3-3.6-0.5 2.5-1.7 4-9 6.5-2.6 4 0.2 0.9 2.1-0.9 5.6-1.2 2.3-2.9 0 1.1 6.5z",NO:"M1113.7 67.5l-6.4 2.1-3.2 0.5 0.8-3.8-5.8-2.1-5.9 1.8-1.1 4-3.4 2.4-4.7-1.3-5.3 0.2-5.1-2.8-2.2 1.4-2.6 0.2 0.1 3.6-8-0.9-0.6 3.1-4 0-2.3 3.9-3.4 6.1-5.7 7.9 1.8 2-1.3 2.2-4.3-0.1-2.4 5.4 1 7.7 3.1 2.9-0.8 6.9-3.4 4-1.8 3.4-3.3-3.6-8.6 6.8-6.1 1.4-6.5-3-1.8-6.3-2-13.5 4-3.7 11.3-4.9 8.1-5.9 7.2-7.8 8.9-10.7 6.4-4.1 10.3-6.8 8.5-2.4 6.7 0.3 5.2-4.4 7.4 0.2 7-1 13.7 3.9-4.9 1.4 5.4 3.4z m-37.1-42.3l-7.6 1.9-6.8-1.1 2.2-1.2-2.6-1.5 7.3-0.9 1.9 1.7 5.6 1.1z m-25.6-8.5l12.6 3.4-8.6 1.8-1.2 3.4-3 0.9-0.9 4-4.4 0.2-8.5-2.9 3-1.7-5.7-1.4-7.7-3.9-3.2-3.5 9.3-1.6 2.3 1.5 5 0 1-1.5 5.2-0.2 4.8 1.5z m24.4-3l7.4 1.5-4.4 2.4-10.1 0.5-10.7-0.8-1-1.2-5.1 0-4.3-2 10.5-1.2 5.4 1 3.1-1.3 9.2 1.1z",NA:"M1116.2 614.3l4.6-1.4 3.6 0.3 2.2 1.5 0 0.5-3.2 1.5-1.7 0-3.7 2.5-2-2.7-8.7 2.3-4.2 0.2-0.8 22.9-5.5 0.2-0.6 18.7-1.1 23.7-5 3.3-2.9 0.5-3.4-1.2-2.5-0.5-0.8-2.7-2-1.8-2.8 3.2-3.9-4.9-2-4.6-1-6.3-1.2-4.6-1.6-9.9 0.1-7.7-0.6-3.5-2.1-2.7-2.8-5.3-2.8-7.7-1.1-4-4.4-6.3-0.3-4.9 2.7-1.2 3.4-1.1 3.6 0.2 3.3 2.9 0.8-0.5 22.7-0.2 3.8 3 13.5 0.9 10.4-2.6z",VU:"M1931.8 631.8l-0.5-0.2 0.2-0.4 0.5-0.1 0.2 0.4-0.1 0.2-0.3 0.1z m-1.2-3.8l-0.2 0-0.3-0.1-0.4-0.5-0.1-0.4 0-0.4 0.2-0.2 0.1-0.4 0.3-0.1 0.4 0-0.2 0.3 0 0.5 0.7 0.5-0.3 0.3-0.2 0.5z m0-5.9l0.3 0 0.1 0.2-0.5 0.1 0.1 0.3 0.4 0.2 0.1 0.3-0.1 0.4-0.4 0.1-0.4-0.3-0.4 0-0.2-0.2-0.3-0.2 0.1-0.4 0-0.3 0.2-0.5 0.3-0.4 0.5 0 0.2 0.1-0.1 0.4 0.1 0.2z m-3-7.6l0.6-0.1 0.1 0.1 0.2 0.6 0.2 0.3 0.1 0.1-0.4 0.6-0.8 0.2-0.3-0.3-0.1-0.5-0.3-0.1-0.4 0.3-0.1-0.1 0.2-0.4 0.4-0.3 0.4-0.5 0.2 0.1z m2.3-3.9l-0.1-0.3 0.3 0-0.2 0.3z m-1.6-2.4l0.1 0.5 0 0.2 0.4 0.1 0.2 0.5 0.5 0 0 0.4-0.1 0.1-0.3-0.3-0.3-0.1-0.6 0.2-0.3-0.1-0.1-0.2-0.1-0.4 0.2-0.5 0.2-0.3 0.2-0.1z m0.9-0.2l-0.1 0-0.1-0.3 0.2 0 0 0.3z m0.2-1.4l-0.2 0.2-0.6 0-0.3 0.1-0.2-0.2-0.4-0.1-0.5-0.4 0.1-0.2 0.5-0.2 0.4-0.1 0.4-0.3 0.1-0.3 0.2 0 0.1 0.3 0 0.6 0.5 0.4-0.1 0.2z m-4.7-1.3l0.2 0.3 0.6 0.4 0.7 0.7 0.1 0.1 0 0.4 0.2 0.3-0.3 0.2-0.4 0.4 0-0.2-0.3-0.1-0.4 0-0.2 0.1 0 0.2-0.4 0.2-0.3-0.1 0-0.7-0.1-0.6 0.1-0.6 0.1-0.5-0.2-0.5-0.6 0.3-0.2-0.2-0.1-0.3 0-0.3 0.3-0.3 0-0.4 0.1-0.3 0.3-0.1 0.6 0.3-0.1 0.2 0.2 0.5-0.1 0.2 0.2 0.4z m-0.6-2.4l-0.3 0-0.5-0.3 0-0.1 0.2-0.3 0.2-0.1 0.5 0.1-0.1 0.4 0 0.3z m-0.5-1l0.4-0.3 0.1 0.1 0.1 0.3-0.5 0-0.1-0.1z m5.7 2.6l-0.2-0.1 0.1-1.2-0.1-0.3 0.1-0.5 0.3-0.8 0.1-0.5 0.2 0.6-0.1 0.3 0 0.6 0 1-0.1 0.6-0.3 0.3z m-1.3-3.3l-0.7 0-0.3-0.1 0-0.2 0.9-0.7 0.5-0.2 0.5-0.1 0 0.2-0.3 0.4-0.2 0.3-0.4 0.4z m1.9-0.7l0 0.2-0.3 0 0.1-0.3 0.2-1.3 0-0.1 0.1-1.2 0.2 0.1-0.1 1.3 0.1 0.3-0.1 0.5-0.2 0.5z m-6.3 1.4l-0.9-0.1-0.6 0.3-0.1 0.2-0.2 0-0.1-0.6-0.2-0.1-0.4-1 0.4-0.8 0-0.4-0.1-0.2 0-1.4-0.1-0.3 0-0.5 0.2-0.3 0-0.3 0.2-0.3 0-0.2 0.3-0.1 0.1 0.5 0.2 0.4 0.2 0.3 0.1 1.3-0.1 0.8 0 0.1 0.6-0.1 0.3-0.3 0.2-0.8 0-0.2 0.3-0.2 0.3 0.1-0.1 0.4 0 0.9 0.3 0.1 0 0.5-0.2 0.1 0.3 0.4-0.2 0.2 0 0.5 0.2 0.5-0.1 0.2-0.3 0-0.5 0.4z m4.2-9.3l0.4 0.3-0.1 0.4-0.3 0.3-0.2 0.2-0.6 0 0-0.4 0.1-0.4 0.2-0.3 0.5-0.1z m0.6-1.9l-0.3 0.2-0.2 0.1 0 0.2-0.2 0.1 0-0.3-0.2-0.2-0.1-0.3 0.1-0.4 0.2-0.1 0.4-0.2 0.1 0.2 0.1 0.4 0 0.1 0.1 0.2z m-5-4.2l-0.1-0.1 0.1-0.5 0.2 0.2-0.2 0.4z",NC:"M1915.2 646.9l-0.2 0.1-0.2 0.4-0.4-0.2 0.2-0.5 0.1-0.2 0.3 0.1 0.2 0.3z m4.4-7.8l-0.1 0.1 0.2 0.4 0.3 0 0.4-0.2-0.1 0.6-0.2 0.2-0.1 0.4-0.5 0.3-0.4-0.2 0-0.1-0.3-0.3 0.1-0.2-0.2-0.2 0.1-0.4-0.1-0.4 0.1 0 0.4 0.1 0.4-0.1z m-2.7-4.3l-0.2 0.5 0 0.4-0.2 0.3 0.3 0.2 0.2 0.1 0.1 0.2-0.1 0.2 0.3 0.4-0.2 0-0.2 0.5-0.2 0.2-0.3-0.1-0.2-0.3-0.1-0.2-0.6-0.1-0.3-0.5 0-0.2-0.1-0.3 0.3 0 0.3-0.2 0.4-0.3 0.1-0.3-0.4-0.2-0.4 0.1 0.2-0.4 0.3-0.1 0.2 0.1 0.4-0.3 0.4 0.3z m-3.4-1.6l-0.3 0.6-0.1 0.3 0.3 0-0.5 0.5-0.2 0.2-0.1-0.2 0.3-0.4 0.2-0.5 0.4-0.5z m0 0l-0.2-0.4 0.4 0 0.1 0.4-0.3 0z m-13.9-2.4l0.2 0.1 0.5 0.9 0.3 0.3 0.3-0.1 0.2-0.3 0.9 0.5 0.3 0.5 0.7 0.6 0.3 0.4 0.1 0.3 0.4 0.4 0.1 0.2 0.2 0 0.3 0.3 0.9 0.2 0.1 0.3 0 0.4 0.3 0.4 0.4 0.1-0.3 0.5 0.1 0.2 0.4 0.5 0.1 0 0.4 0.5-0.1 0.4 0.7 0.2 0.4 0.5 0.3 0 0.3 0.2 0.5 0.4-0.1 0.3 0.3 0 0.4 0.5 0.6 0.4 0 0.2 0.3 0.1 0.3 0.3 0 0.2 0.3 0.6 0.4 0.4 0.1-0.1 0.4 0.6 0.5 0.1 0.3 0.3 0 0.4 0.1 0.4 0.1 0.1-0.2 0.6-0.7 0.4-0.1-0.3-0.3 0.1-0.3 0.3-0.6-0.8-0.4 0-0.1-0.2-0.2 0-0.2 0.3-0.9-0.9-0.2 0-0.4-0.5 0.1-0.8-0.3 0-0.4 0-0.3-0.2-0.1-0.2-0.2-0.1-0.3 0 0-0.3-0.2-0.5-0.4 0.1-0.8-0.5-0.1-0.2 0-0.3-0.3 0.2-0.6-0.3-0.1-0.4-0.4-0.3-0.2-0.5-0.5-0.1 0.2-0.2-0.3-0.3-0.3-0.1-0.3-0.7 0-0.4-0.2-0.3-0.3 0-0.2-0.6 0.1-0.2-0.7-0.4-0.3-0.2-0.4-0.6 0.3-0.2-0.2-0.2-0.2-0.3 0-0.5-0.5-0.3 0-0.6-0.2-0.3 0.3 0 0.1-0.3-0.1-0.2-0.6 0-0.2-0.3 0.5-0.7-0.3-0.3 0.2-0.1z m1.1 0.3l-0.3-0.1 0.1-0.4 0.2 0.5z m-2.3-2.4l-0.2 0.2-0.1-0.4 0.2-0.2 0.1 0.4z",NE:"M1068.6 355l1.6 10 2.2 1.7 0.1 2 2.4 2.2-1.2 2.8-1.8 13-0.2 8.4-7 6-2.3 8.5 2.4 2.4 0 4.1 3.7 0.1-0.6 3.1-1.5 0.3-0.2 2.1-1 0.1-3.9-7-1.4-0.3-4.3 3.6-4.4-1.9-3-0.3-1.6 0.9-3.3-0.2-3.3 2.7-2.9 0.2-6.8-3.3-2.7 1.5-2.9-0.1-2.1-2.4-5.6-2.4-6.1 0.8-1.4 1.3-0.8 3.7-1.6 2.6-0.4 5.8-4.3-3.7-2 0-1.9 1.9 0.1-4.4-6.5-1.5-0.2-3.1-3.1-4.2-0.8-2.9 0.5-3.1 3.6-0.3 2-2.3 7.7-0.5 5-1 0.4-4 3.1-4.3-0.1-14.8 7.8-2.8 16-12.6 18.8-12.3 8.8 2.8 3.2 3.5 3.8-2.4z",NG:"M1066.2 421.7l2.3 2.5-0.6 1.2-0.3 2.1-4.7 5-1.4 4.1-0.8 3.3-1.2 1.5-1.1 4.5-3 2.6-0.8 3.2-1.3 2.6-0.5 2.7-3.9 2.2-3.2-2.7-2.1 0.1-3.4 3.8-1.6 0-2.7 6.2-1.4 4.6-5.9 2.3-2.1-0.3-2.2 1.4-4.5-0.1-3.1-4.1-1.9-4.6-4-4.2-4.2 0-5 0 0.3-10.3-0.2-4.1 1.1-4 1.7-2 2.8-4-0.6-1.7 1.1-2.6-1.3-3.8 0.2-2.1 0.4-5.8 1.6-2.6 0.8-3.7 1.4-1.3 6.1-0.8 5.6 2.4 2.1 2.4 2.9 0.1 2.7-1.5 6.8 3.3 2.9-0.2 3.3-2.7 3.3 0.2 1.6-0.9 3 0.3 4.4 1.9 4.3-3.6 1.4 0.3 3.9 7 1-0.1z",NZ:"M1886.2 764.4l-0.8 2.6 5.6-2.6-0.5 2.7-2.1 2.7-4.2 2.9-7.1 4.7-4.7 2.6-0.6 3-4 0.1-6.3 2.4-4.7 4.1-8.2 6.4-6.3 2.8-4 1.8-4.6-0.1-1.5-2.1-5.1-0.4 1-2.4 6.5-4.6 11.4-6.3 4.3-1.2 5.6-2.4 7-3.3 5.7-3.3 6-4.7 3.1-1.6 3.5-3.6 5.8-2.9-0.8 2.7z m29-30.5l-0.4 6.8 2.9-4.4 1.3 1.8-2.4 4.8 2.9 2.1 3.2 0.5 4.7-2.4 2.2 0.7-5.2 5.7-4.2 3.7-3.9-0.1-2.8 1.9-1.5 2.8-1.6 1.1-4.6 3.5-5.9 4.3-6 2.6 0.5-1.7-1.5-0.9 6.9-5.2 0.9-3.6-3.8-2.5 1.8-2.3 5.3-2.2 4.2-5 2.5-4.1 0.6-4.3 0.8-1.1-0.9-2.7-0.7-5.6 0.4-4.6 2.2-0.5 0.9 3.6 3.2 1.6-2 5.7z",BQBO:"M602 424.6l-0.2 0-0.1-0.5 0.1-0.3-0.1-0.3-0.4-0.1-0.3-0.3 0.1-0.3 1.2 0.6-0.1 0.2 0 0.4-0.2 0.2 0 0.4z",NP:"M1469 322.9l0.2 2.7 1.5 4.1-0.1 2.5-4.6 0.1-6.9-1.5-4.3-0.6-3.8-3.2-7.7-0.9-7.8-3.6-5.8-3.1-5.8-2.4 0.9-6 2.8-3 1.9-1.5 4.8 2 6.4 4.2 3.3 0.9 2.5 3.1 4.5 1.2 5 2.9 6.5 1.4 6.5 0.7z",NR:"M1933 505.3l-0.2 0-0.1-0.3 0.3 0 0 0.3z",XK:"M1097.8 230.8l-1.2 0.3-2.9 1-0.1 1.3-0.7-0.1-0.6-2.3-1.3-0.7-1.2-1.7 0.8-1.4 1.2-0.4 0.5-2.1 0.9-0.4 0.8 0.9 1 0.4 0.8 1 0.9 0.3 1.1 1.2 0.7-0.1-0.4 1.6-0.6 0.7 0.3 0.5z",CI:"M955.9 435.2l2.5 1.4 1 2.2 2.5 1.3 2-1.6 2.6-0.2 3.9 1.6 1.5 9.2-2.4 5.3-1.5 7.3 2.4 5.5-0.2 2.6-2.6 0-3.9-1.2-3.7 0-6.7 1.2-3.9 1.8-5.6 2.4-1.1-0.2 0.4-5.3 0.6-0.8-0.2-2.5-2.4-2.7-1.8-0.4-1.6-1.8 1.2-2.9-0.5-3.1 0.2-1.8 0.9 0 0.4-2.8-0.4-1.3 0.5-0.9 2.1-0.7-1.4-5.2-1.3-2.6 0.5-2.2 1.1-0.5 0.8-0.6 1.5 1 4.4 0 1-1.8 1 0.1 1.6-0.7 0.9 2.7 1.3-0.8 2.4-1z",CH:"M1034.4 197.5l0.2 1.1-0.7 1.5 2.3 1.2 2.6 0.2-0.3 2.5-2.1 1.1-3.8-0.8-1 2.5-2.4 0.2-0.9-1-2.7 2.2-2.5 0.3-2.2-1.4-1.8-2.7-2.4 1 0-2.9 3.6-3.5-0.2-1.6 2.3 0.6 1.3-1.1 4.2 0 1-1.3 5.5 1.9z",CO:"M584.4 426.2l-3.7 1.1-1.6 3.2-2.3 1.8-1.8 2.4-0.9 4.6-1.8 3.8 2.9 0.4 0.6 2.9 1.2 1.5 0.3 2.5-0.7 2.4 0.1 1.4 1.4 0.5 1.2 2.2 7.3-0.6 3.3 0.8 3.8 5.6 2.3-0.7 4.1 0.3 3.2-0.7 2 1.1-1.2 3.4-1.3 2.2-0.6 4.6 1.1 4.3 1.5 1.9 0.2 1.4-2.9 3.2 2 1.4 1.5 2.3 1.6 6.4-1.1 0.8-1-3.8-1.6-2.1-1.9 2.3-11-0.2 0 4 3.3 0.7-0.2 2.5-1.1-0.7-3.2 1-0.1 4.7 2.6 2.4 0.9 3.6-0.1 2.8-2.2 17.6-2.9-3.4-1.7-0.1 3.5-6.6-4.4-3-3.4 0.6-2.1-1.1-3.1 1.7-4.2-0.9-3.5-6.7-2.6-1.6-1.8-3.1-3.8-3-1.5 0.6-2.4-1.5-2.8-2.1-1.6 1-4.8-0.9-1.4-2.8-1.1 0.1-5.6-3.6-0.7-2 2.1-0.5-0.2-3.2 1.4-2.4 2.8-0.4 2.5-4 2.2-3.4-2-1.5 1.2-3.7-1.1-5.9 1.3-1.7-0.7-5.4-2.2-3.5 0.9-3.1 1.8 0.5 1.1-1.9-1.1-3.8 0.7-0.9 2.9 0.2 4.5-4.5 2.4-0.7 0.1-2.1 1.4-5.5 3.4-2.9 3.5-0.2 0.6-1.3 4.4 0.5 4.6-3.2 2.3-1.4 2.9-3.1 2 0.4 1.3 1.7-1.2 2.1z",CN:"M1602.2 381.9l-4.3 3.1-4.9-2-1-5.5 2.2-2.9 5.8-1.8 3.3 0.1 1.6 2.5-2 2.8-0.7 3.7z m23.4-196.4l9 4.5 6 5.8 7.6 0 2.6-2.4 6.9-1.9 1.3 5.7-0.3 2.3 2.8 6.8 0.6 6.2-6.9-1.1-2.9 2.2 4.7 5.4 3.9 7.5-2.5 0.1 1.9 3.3-5.5-3.8 0 3.6-6.4 2.7 2.8 3.4-4.6-0.3-3.6-2-1.1 4.6-3.9 3.4-2.1 4.1-6.3 1.8-2.4 3-4.8 1.8 1.3-3-2.3-2.5 2-4.3-4.5-3.3-3.4 2.2-3.6 4.5-1.3 4.1-5 0.3-1.3 3 4.8 4.3 4.8 1.1 1.4 2.8 5.1 1.9 3.8-4.6 5.9 2.5 3.5 0.2 2.3 3.3-6.7 1.8-1 3.5-3.8 3.2-0.9 4.5 7.1 3.5 4.6 6.3 5.5 5.9 5.5 4.9 1.6 4.8-2.8 1.8 2.4 3.4 3.9 2 0.8 5.2 0.1 5.1-2.8 0.6-2.1 6.9-2.3 8.5-3.4 7.6-6.4 5.9-6.6 5.5-6.1 0.7-2.9 2.8-2.3-2-2.5 3.1-7.2 3.3-5.8 0.9-0.7 6.8-3.1 0.4-2.2-4.7 0.9-2.4-7.8-2.1-2.4 1.1-5.9-1.7-3.1-2.6 0.3-3.7-5.3-1.2-3.1-2.4-4.1 3.4-5.3 0.8-4.4-0.1-2.7 1.6-2.7 0.9 2 7.4-3-0.2-0.8-1.5-0.5-2.7-3.8 1.9-2.6-1.2-4.5-2.4 0.8-5.3-3.7-1.3-2.4-5.9-5.6 1.1-0.7-7.6 4.2-5.4-0.9-5.3-1.3-4.9-2.7-1.5-2.7-3.8-3.1 0.5-6.1-1 1.2-2.7-3.6-4-3.2 2.7-4.9-1.5-5.4 4-3.9 4.8-4.2 0.8-2.8-1.7-2.9-0.2-4.3-1.4-2.6 1.6-2.6 4.8-1.5-5.1-3.1 1.4-6.5-0.7-6.5-1.4-5-2.9-4.5-1.2-2.5-3.1-3.3-0.9-6.4-4.2-4.8-2-1.9 1.5-8.6-4.5-6.2-4-3.2-7.1 4.1 0.9-0.6-3.3-3-3.3-0.8-5.2-7.7-7.6-9.5-2.5-2.9-5-4.7-3-1.5-1.8-1.8-3.6-0.5-2.5-3.7-1.5-1.5 0.7-3.1-6 1.1-1.4-1.2-1.5 4.1-3 3.2-1.3 5.8 0.9 0.6-4.1 6.4-0.7 1-2.6 6.9-3.4 0.2-1.4-1.7-3.7 2.9-1.6-8.8-11 9.1-2.5 2-1.4-1-11.3 10.8 2.1 1.6-2.9-2.5-6.2 3.8-0.6 1.9-4.2 1.7-0.5 3.3 4.4 5.7 3.3 8.2 2.3 5.8 5.1 1.4 7.3 3 2.8 6.5 1.1 7.2 0.8 8 4 3.4 0.7 5 5.8 4.7 3.8 5.6-0.2 11.3 1.5 6.4-0.9 5.6 0.9 9.4 3.9 6.2 0 3.3 1.9 4.4-3.4 7.2-2.2 7.6-0.2 4.9-2.2 2-3.4 2.4-2.2-1.9-2.1-2.9-2.4 0.5-4.1 3.2 0.6 5.9 1.3 3.2-3.4 6.4-2.4 1.3-4.2 2.5-1.8 6.8-0.8 4.4 0.7-0.8-2.2-7.2-4.4-5.2-2-2.5 2.3-5.5-1-2.3 0.8-2.8-2.5-0.3-6.3-0.6-4.7 7.4 2.4 4.4-3.9-1.9-2.8-0.2-6.5 1.3-2-2.5-3.4-3.7-1.4 1.7-3.1 5.1-1.1 6.2-0.2 8.6 1.9 6 2.2 7.7 6.2 3.8 2.7 4.5 3.8 6.2 6 10 2z",CM:"M1072.8 454.2l-2.8 6.5-1.4 1.1-0.4 5 0.6 2.7-0.5 1.9 2.7 3.4 0.5 2.3 2.1 3.3 2.6 2.1 0.3 2.9 0.6 1.9-0.4 3.4-4.5-1.5-4.6-1.7-7.1-0.2-0.7-0.4-3.4 0.8-3.4-0.8-2.7 0.4-9.3-0.1 0.9-5.1-2.3-4.3-2.6-1-1.1-2.9-1.5-0.9 0.1-1.8 1.4-4.6 2.7-6.2 1.6 0 3.4-3.8 2.1-0.1 3.2 2.7 3.9-2.2 0.5-2.7 1.3-2.6 0.8-3.2 3-2.6 1.1-4.5 1.2-1.5 0.8-3.3 1.4-4.1 4.7-5 0.3-2.1 0.6-1.2-2.3-2.5 0.2-2.1 1.5-0.3 2.3 4.1 0.5 4.2-0.2 4.3 3.2 5.8-3.2 0-1.6 0.4-2.6-0.6-1.2 3 3.4 3.8 2.5 1.1 0.8 2.6 1.8 4.4-0.8 1.8z",CL:"M655.1 837.9l6 13.6 5 0 3 0.2-0.5 2.4-3.2 1.9-2.4-0.2-3.1-0.5-4.2-1.8-5.4-0.9-7.5-3.4-6.4-3.2-9.8-6.9 4.6 1.3 8.6 4.1 7.3 2.2 1.2-2.8-0.3-4.2 3.4-2.6 3.7 0.8z m-40.7-190.1l0.9 1.6-1.2 6.7-5.6 3.1 1.6 10.7-0.8 2.1 2 2.5-3.3 4-2.6 6-0.9 5.9 1.7 6.2-2.1 6.6 4.9 11.1 1.6 1.2 1.4 5.9-1.6 6.2 1.4 5.4-3 4.2 1.6 5.8 3.3 6.3-2.5 2.3 0.2 5.7 0.8 6.5 3.3 7.8-1.7 1.3 3.7 7.3 3.1 2.4-0.8 2.7 2.8 1.2 1.4 2.4-1.8 1.2 1.8 3.7 1.1 8.3-0.7 5.4 1.8 3.2-0.1 3.9-2.7 2.8 3.1 6.5 2.6 2.3 3.1-0.4 1.9 4.6 3.5 3.6 12.1 0.8 4.9 1-4.3 0-1.7 1.5-3.4 2.2 1.7 5.7-1.9 0.1-6.3-1.9-7.5-4.3-7.6-3.5-3.2-3.9-0.3-3.6-4.2-4.2-5-10.6-0.2-6 3.4-4.8-8.3-1.9 2.7-5.5-2-10.5 6.5 2.2-1.8-13.1-4-1.7 1 7.9-3.5-0.9-1.5-9-2-11.8 1.1-4.4-3.3-6.2-2.4-7.2 2.2-0.3 0.6-10.3 1.2-10.3 0.1-9.5-3.3-9.7 0.6-5.3-2.1-7.9 1.8-7.8-0.9-12.4 0-13.4 0.1-14.3-1.5-10.5-2.1-9.1 2.8-1.6 1.1-3.3 3.2 4.4 1.2 4.6 3.2 2.7-1.1 6.2 3.7 7.2 3.1 8.9 3.9-0.9z",CA:"M665.9 203.6l3.4 0.9 4.7-0.2-3.3 2.6-2 0.4-5.5-2.7-0.6-2.1 2.5-1.9 0.8 3z m14.4-16l-2.4 0.1-5.8-1.9-3.5-3 1.9-0.5 5.9 1.6 4.2 2.6-0.3 1.1z m-307.9 3.7l-3.1 0.9-6.3-2.8 0-2.2-2.9-2.2 0.3-1.8-4.3-1.1 0.6-3.4 1.5-1.4 4.1 1.3 2.4 1 4.1 0.6 0.2 2.2 0.4 2.9 3.2 2.6-0.2 3.4z m339.1-13.5l-5 5.4 3.9-2.1 2.9 1.4-2.4 2.1 3.8 1.7 2.8-1.5 4.3 1.9-2.8 4.6 3.8-1.1-0.3 3.3 0.5 3.9-3.6 5.6-2.4 0.2-2.9-1.2 2.5-5.1-1.2-0.8-7.3 5.4-3-0.2 4.4-3-4.4-1.5-5.5 0.4-9.6-0.2-0.2-1.8 3.8-2.3-1.7-1.6 5.4-3.8 8.2-9.9 4.2-3.5 5-2.1 2.1 0.2-1.5 1.7-3.8 3.9z m-360-21.4l1.5 0.8 5-0.5-7.2 6.9 0.3 5-1.9 0-0.7-2.8 0.5-2.9-0.8-1.9 1.3-2.7 2-1.9z m283.4-47.5l-3.6 3-1.7-0.5-0.1-1.7 0.4-0.4 2.8-1.7 1.7 0.1 0.5 1.2z m-9.7-3.2l-7.2 3.2-3-0.2 0-1.5 5-2.7 6 0.1-0.8 1.1z m-3.1-16.8l-0.9 2.5 2.7-0.9 1.5 1.5 3.5 2 3.8 1.7-1.4 2.7 3.5-0.4 1.9 1.9-5 1.8-5.9-1.4-0.8-2.6-6.3 3.1-8.2 2.9 0.7-3.3-6.3 0.5 5.7-2.8 3.6-4.5 5.1-5.1 2.8 0.4z m44.9-8.3l-4.9 0.3 0.7-2.7 3.8-3.1 4.3-0.7 2.3 1.5-1.5 2.3-0.9 0.8-3.8 1.6z m-74.5-10.7l-4.1 1.9-4.2-1.6-3.9 0.5-3.4-2.4 5-1.7 4.9-2.3 3 1.5 1.6 1 0.4 1 0.7 2.1z m53 142.6l-2.2-3.6 2.9-8.5-1.6-1.8-3.7 1-1.1-1.6-5.5 4.7-3.2 4.9-2.8 2.9-2.5 1-1.7 0.3-1.1 1.5-9.3 0-7.8 0.1-2.7 1.1-6.8 4.4 0-0.1-0.9-0.4-2 0.9-1.9 1.3-1.8-1.1-4.7 0.8-3.9 0.9-1.9 0.8-2.3 2.1 1.8 0.7 1.7-0.4 0.3 0-0.3 1.9-4.8 0.7-2.8 0.8-1.7 1-2.6-0.6-1.6 0.3-2.9 1.8-4.6 2-2.7-0.4 2-2.2 3.7-3.5 4.1-2.1 1.1-1.8 0.9-3 3.8-3.5 0.9-4 1.1 3.9 3.8 0.9 2.4-2.1-1.4-4.8-0.9-2-4-1.2-3.8-0.7-3.9 0-3.4-0.8-0.4-1.4-1.4 0.9-1.2-0.2 1.9-2.1-1.8-0.8 1.9-2.4-1.2-1.8 1.7-1.8-5.2-0.9-0.1-3.6-0.8-0.8-3.3-0.2-4.1-1.2-1.5 0.8-1.8 1.5-3.3 1-3.1 2.5-5.4-1.7-4.4 0.8-3.9-1.9-4.6-1-3.3-0.4-1-1 0.9-3.4-1.7 0-1.3 2.4-10.2 0-17 0-16.8 0-14.9 0-14.8 0-14.7 0-15.1 0-4.8 0-14.7 0-14.1 0-0.7 0-5.4-6.1-1.6-2.7-7-2.6 1.3-5.5 3.6-3.7-4.1-2.7 3.1-4.9-2.1-4.4 2.5-3.2 5.1-2.9 3.2-3.8-4.6-3.8 1.4-6.9 1.1-4.2-1.6-2.7-0.8-2.4 0.6-3.1-6.5 1.9-7.6 3.3-0.3-3.8-0.5-2.6-2.8-1.6-4.2-0.2 35.6-32.7 24.6-20.4 6 1.3 3.3 2.6 3.7 0.5 6.3-2.2 7-1.7 5.3 0.6 8.9-2.3 8.2-1.3 0.2 2.2 4.5-1.3 3.9-2.5 2.1 0.6 1.4 4.8 9.5-3.7-3.9 4.1 6-0.9 3.2-1.5 4.6 0.3 3.9 2.2 7.5 2 4.7 0.9 4.4-0.3 2.9 2.8-8.5 2.7 6.4 1.1 11.9-0.6 4.4-1 1.4 3.3 7.1-2.7-2.1-2.4 4.5-1.8 5.2-0.3 3.9-0.5 2.1 1.3 1.5 2.9 5-0.4 5.3 2.5 7.2-0.9 6 0.1 2.4-3.4 4.5-0.9 4.9 1.8-4.3 5.2 6.2-4.4 3.2 0.2 6.4-5.5-1.6-3.3-2.9-2.2 5.5-5.9 8.2-3.8 4.5 0.9 2 2.3 0.4 6-5.8 2.6 6.7 1.1-4.4 5.5 8.9-4.2 2.2 3.5-4.3 4 1.3 3.7 7.3-3.9 6.5-4.8 4.7-5.9 5.5 0.4 5.4 0.8 3.6 2.7-1.7 2.7-5.1 2.9 0.9 2.9-2.4 2.7-10.9 3.9-6.5 0.9-3.2-1.7-3.3 2.8-7.4 4.7-3 2.5-7.7 3.8-6.5 0.4-5.1 2.4-2.9 3.8-5.7 0.7-8.7 4.7-9.4 6.5-5 4.6-4.9 6.9 6 1-1.5 5.5-0.8 4.6 7.3-1.2 7 2.6 3.3 2.3 1.7 2.8 4.9 1.7 3.6 2.5 7.6 0.4 4.8 0.6-3.6 5.2-1.7 6.1 0.1 6.9 4.4 5.9 4.7-2 5.6-6.4 2.3-9.6-1.7-3.2 9-2.9 7.5-4.2 4.8-4.2 1.7-4-0.4-5.1-3.2-4.5 8.9-6.2 1-5.3 3.9-9 3.8-1.4 6.7 1.6 4.2 0.6 4.5-1.6 3.1 2 3.6 3.4 0.2 2.2 7.7 0.5-2.6 4.9-2.3 7.4 3.8 1 1.6 3.5 8.2-3.3 7.5-6.6 4.2-2.7 1.1 5.3 2.6 7.5 2 7.2-3.4 3.8 4.8 3.4 2.9 3.4 6.9 1.6 2.4 1.9 0 5.2 3.4 0.8 1.1 2.3-2 6.9-4.3 2.3-4.2 2.2-8.8 2.2-7.9 5-8.6 1.1-10.1-1.4-7.3 0-5.3 0.4-5.7 4.5-7.4 2.8-10.1 8.2-7.9 5.8 4.7-1 10.9-8.3 12.3-5.2 7.6-0.6 3.3 3.1-6.1 4.2-0.6 6.7 0.1 4.8 5.6 3.1 8.6-0.9 7.2-7.1-1 4.6 2.5 2.3-7.4 4.1-12.4 3.8-5.8 2.5-7.2 4.6-3.7-0.5 1.5-5.3 10.4-5.3-8.1 0.2-5.9 0.8z m-106.5-163.8l-4.7 2.4 10.5-1.5 2.6 2.6 7.3-2.7 1.8 1.7-2.2 5.1 4.3-2.1 2.8-5.3 4.3-0.8 3 0.8 2.2 2.1-2.7 5.1-2.4 3.7 4.2 2.6 5 2.6-2.6 2.3-7.1 0.5 0.8 2-3.2 2-6.7-0.8-5.7-1.5-5.1 0.3-9.5 1.9-11.3 0.8-7.9 0.5 0.3-2.6-4.1-1.4-4.3 0.6-0.6-4.3 3.3-0.6 7.3-0.9 5.5 0.3 6.2-1-6.5-1.2-9.1 0.4-5.7-0.1 0-1.9 11.7-2.2-6.3 0.1-5.4-1.4 7.8-3.9 5.2-2.1 14.2-3.1 2.8 1z m39.5-1.5l-7 3.4-2.3-3.6 2.1-0.8 5.5-0.2 1.7 1.2z m108.6 1.6l-0.7 1.4-4.1-0.1-4.1-0.1-4.9 0.7-0.9-0.4-2.1-2.7 1.7-1.8 2.2-0.4 8.4 0.6 4.5 2.8z m-39.8-0.3l0.3 3.2 7.2-4.1 11.7-2.1 2.3 5.3-3.2 3.4 9-1.5 5.3-2.1 6.8 2.6 3.7 2.5-1.1 2.3 8.2-1.2 1.9 3.4 8.4 2.1 2.2 2.1 0.9 5.1-9.1 2.5 7.7 3.6 5.9 1.2 3.3 5 6.5 0.4-3.3 3.9-10.9 6.5-4-2.4-3.8-5.4-5.9 0.7-2.3 3.3 2.7 3.2 4.5 2.6 1 1.5-0.1 5.7-3.6 4.1-4.7-1.6-8.6-4.5 3.6 4.9 2.8 3.5-0.3 2-10.9-2.3-7.6-3.4-3.8-2.7 2.4-1.7-4.8-2.9-4.7-2.7-0.9 1.6-13.1 0.9-2.4-1.9 5.4-4.2 8.1-0.1 9.3-0.7-0.2-2 3.1-2.8 8.8-5.4 0.4-2.5-0.5-1.8-4.7-2.7-7.1-1.8 3.5-1.4-2-3.4-3.4-0.3-1.9-1.8-3.3 1.6-7.9 0.7-14-1.2-7.4-1.6-5.9-0.8-1.9-1.9 6.3-2.4-5.7 0 3.3-5.3 7.1-4.6 6-2.1 11.2-1.4-5.8 3.3z m-50.4-3.5l3.6 1.1 7.6-0.7-0.5 1.5-6.1 2.5 3.8 2.2-5.2 4.7-8.4 2-3.4-0.4-0.9-2-5.9-4 1.7-1.7 7.4 0.6-0.9-3.3 7.2-2.5z m22.6 5.5l-7.8 3.9-4.4-0.2 1.8-4.6 2.6-2.5 4.2-2.2 5.2-1.4 7.9 0.2 6.2 1.2-10.1 4.6-5.6 1z m-115.8 7.2l-13.5 2.6 0.6-2.3-6-2.8 4.4-2.2 7.5-3.8 7.6-3.4 0.4-3.1 14-0.8 4.2 1.1 9.5 0.3 1.9 1.4 1.6 2.2-6.5 1.3-13.9 3.6-9.2 3.7-2.6 2.2z m125.2-18.4l-4.1 1.9-5.2-0.4-3.2-1.3 4.5-2.2 6.9-1.4 1.4 1.8-0.3 1.6z m-8.8-8.7l0.2 2.3-2.8 2.5-5.8 3.8-6.9 0.5-3.2-0.8 3.4-2.9-6.6 0.3 4.3-3.8 3.8 0.2 7.7-1.7 4.9 0.3 1-0.7z m-39.4 2.6l-0.7 1.7 4.4-0.8 3.6 0.2-2.3 2.5-5.1 2.3-13.9 0.8-12.3 2.2-6 0.1 1.5-1.6 10.7-2.3-17.9 0.6-4.2-0.9 11.7-4.8 5.4-1.4 8.3 1.7 2.9 2.9 6.1 0.4 0.7-4.7 5.8-1.8 3.1 0.5-1.8 2.4z m55.7-4.4l2.3 1.6 7.4 0 1.4 1.6-2.8 1.8 3.1 1.1 1.2 1.2 4.9 0.2 5.1 0.4 7.1-1 8.1-0.5 5.8 0.4 2.3 1.9-1 2-3.6 1.4-6.7 1.1-4.3-0.7-11.8 0.8-8 0.1-5.6-0.6-8.6-1.6 1.6-2.8 2.1-2.4-1.4-2.2-7-0.6-2.6-1.5 3.8-2 7.2 0.3z m-75.7-2.6l-5.8 3.7-5.3 1.7-3.8 0.2-9.9 2.1-7.1 0.8-3.6-1.1 11.7-3.7 12.2-3.1 5.5 0.1 6.1-0.7z m81.2 0.6l-1.8 0.1-6.6-0.3 0.7-1.3 7.2 0.1 1.6 0.8-1.1 0.6z m-58.8-0.9l-8.8 1.4-3.3-1.5 5.1-1.5 6-0.5 4.1 0.7-3.1 1.4z m7.8-4.2l-5.7 0.9-6 0 1-0.7 5.7-1.3 1.7 0.2 3.3 0.9z m46.8 2.5l-6.6 1-1.6-1.1 0.6-1.7 2.2-1.9 4.4 0.2 1.7 0.3 2.3 1.6-3 1.6z m-13.8-1.2l-1.1 1.9-5.3-0.5-4-1.5-7.8-0.2 5.3-1.3-2.8-1.1 2.2-1.8 6.1 0.6 7.5 1.7-0.1 2.2z m54.1-6.1l2.7 1.5-6.5 1.3-10.6 3.5-6.8 0.3-6.8-0.6-1.6-1.9 2.1-1.6 4.3-1.2-6.6 0-2-1.5 0.4-1.9 4.9-1.9 4.1-1.3 4-0.3-0.3-1 8.3-0.2 1.6 2.2 4.7 0.9 4.8 0.9-0.7 2.8z m79.2-14l8.7 0.3 6.7 0.5 5.2 1-1.1 1-9.8 1.7-9 0.8-4 0.9 7.5 0-10.5 2.5-6.8 1.2-9.3 3.5-8.1 0.7-3.1 1-11.3 0.4 4.4 0.6-3.3 0.8 0.7 2.3-5 1.6-6.9 1.3-3.5 1.8-6.6 1.5-0.5 1.1 6.5-0.2-1.1 1.2-12.7 2.9-8.5-1.4-11.7 0.8-5-0.6-6.8-0.3 2.1-2.3 8-1.1 2-3.4 2.6-0.3 7.5 2-1.7-3-4.9-0.9 5-1.8 7.6-1.1 2.8-1.6-3-1.7 1.1-2.2 9.4 0.1 2.2 0.5 7.3-1.6-7.3-0.5-12.6 0.3-4.4-1.4-0.8-1.7-2.5-1.2 1.1-1.3 6-0.8 4.2-0.1 7.4-0.6 6.8-1.5 3.9 0.2 2.4 1.1 5-2 5.2-0.6 6.5-0.4 10.4-0.2 1.3 0.4 10.3-0.6 7 0.2 7 0.2z",CG:"M1090.9 479.3l-0.3 3.9-1.7 3.4-1.1 4-0.7 5.7 0.3 3.7-0.9 2.2-0.1 2.4-0.7 2-3.7 3.1-2.6 3.3-2.5 6.2 0.1 5.3-1.4 2.1-3.3 3.1-3.4 4-2.1-1.1-0.3-1.8-3.1-0.1-2 2.5-1.5-0.7-2.1-2.2-1.7 1.1-2.3 2.8-4.6-6.8 4.3-3.6-2.1-4.2 2-1.6 3.8-0.8 0.4-2.9 3.1 3.1 5 0.3 1.7-3 0.7-4.3-0.6-5-2.7-3.8 2.5-7.5-1.4-1.2-4.2 0.5-1.6-3.3 0.4-2.8 7.1 0.2 4.6 1.7 4.5 1.5 0.4-3.4 3-6 3.3-3.4 3.9 1.1 3.6 0.3z",CF:"M1121.3 446.5l3.9 2.5 3.1 2.6 0.1 2.1 3.9 3.3 2.4 2.8 1.4 3.8 4.3 2.6 0.9 2-1.8 0.7-3.7-0.1-4.2-0.7-2.1 0.5-0.9 1.6-1.8 0.2-2.2-1.4-6.3 3.2-2.6-0.6-0.8 0.5-1.6 3.9-4.3-1.3-4.1-0.6-3.6-2.4-4.7-2.2-3 2.1-2.2 3.2-0.5 4.5-3.6-0.3-3.9-1.1-3.3 3.4-3 6-0.6-1.9-0.3-2.9-2.6-2.1-2.1-3.3-0.5-2.3-2.7-3.4 0.5-1.9-0.6-2.7 0.4-5 1.4-1.1 2.8-6.5 4.6-0.5 1-1.7 1 0.2 1.4 1.4 7.1-2.4 2.4-2.5 2.9-2.3-0.6-2.2 1.6-0.6 5.5 0.4 5.2-3 4-7 2.8-2.6 3.6-1.1 0.7 2.7 3.3 4 0 2.7-0.8 2.6 0.4 2 1.9 1.9 0.5 0.3z",CD:"M1141.3 468.2l3.5 5.3 2.6 0.8 1.5-1.1 2.6 0.4 3.1-1.3 1.4 2.7 5.1 4.3-0.3 7.5 2.3 0.9-1.9 2.2-2.1 1.8-2.2 3.3-1.2 3-0.3 5.1-1.3 2.5-0.1 4.8-1.6 1.8-0.2 3.8-0.8 0.5-0.6 3.6 1.4 2.9 0.1 1-1.2 10.3 1.5 3.6-1 2.7 1.8 4.6 3.4 3.5 0.7 3.5 1.6 1.7-0.3 1.1-0.9-0.3-7.7 1.1-1.5 0.8-1.7 4.1 1.2 2.8-1.1 7.6-0.9 6.4 1.5 1.2 3.9 2.5 1.6-1.2 0.2 6.9-4.3 0-2.2-3.5-2-2.8-4.3-0.9-1.2-3.3-3.5 2-4.4-0.9-1.9-2.9-3.5-0.6-2.7 0.1-0.3-2-1.9-0.1-2.6-0.4-3.5 1-2.4-0.2-1.4 0.6 0.4-7.6-1.8-2.4-0.4-4 0.9-3.9-1.1-2.4-0.1-4.1-6.8 0.1 0.5-2.3-2.9 0-0.3 1.1-3.5 0.3-1.5 3.7-0.9 1.6-3.1-0.9-1.8 0.9-3.8 0.5-2.1-3.3-1.3-2.1-1.6-3.9-1.3-4.7-16.7-0.1-2 0.7-1.7-0.1-2.3 0.9-0.8-2 1.4-0.7 0.2-2.8 1-1.6 2-1.4 1.5 0.7 2-2.5 3.1 0.1 0.3 1.8 2.1 1.1 3.4-4 3.3-3.1 1.4-2.1-0.1-5.3 2.5-6.2 2.6-3.3 3.7-3.1 0.7-2 0.1-2.4 0.9-2.2-0.3-3.7 0.7-5.7 1.1-4 1.7-3.4 0.3-3.9 0.5-4.5 2.2-3.2 3-2.1 4.7 2.2 3.6 2.4 4.1 0.6 4.3 1.3 1.6-3.9 0.8-0.5 2.6 0.6 6.3-3.2 2.2 1.4 1.8-0.2 0.9-1.6 2.1-0.5 4.2 0.7 3.7 0.1 1.8-0.7z",CZ:"M1059.7 175.2l2.5 2 3.7 0.5-0.2 1.7 2.8 1.3 0.6-1.6 3.4 0.7 0.7 2 3.7 0.3 2.6 3.1-1.5 0-0.7 1.1-1.1 0.3-0.2 1.4-0.9 0.3-0.1 0.6-1.6 0.6-2.2-0.1-0.6 1.4-2.4-1.2-2.3 0.3-4-1.9-1.7 0.5-2.6 2.6-3.8-2.1-3-2.6-2.6-1.5-0.7-2.7-1-1.8 3.4-1.3 1.7-1.6 3.5-1.2 1.1-1.2 1.3 0.7 2.2-0.6z",CY:"M1168.2 276.7l0.1 0.2 0.3 0.4-0.4-0.1-0.3 0-0.4 0.2-0.3-0.3 0-0.3 0.3-0.1 0.2 0.1 0.5-0.1z m-1.7 0.5l0 0.1-0.2 0.2-0.1 0.6-0.1 0.2-0.3 0-0.2 0.2-0.5 0.2-0.2 0.1-0.6 0.3-0.4-0.1-0.5 0.1-0.3 0.4-0.2-0.3-0.3 0.1-0.2 0-0.3-0.3-0.4 0.3-0.5 0-0.5-0.2-0.5-0.1-0.4-0.3-0.3-0.6-0.3-0.3-0.2-0.7-0.2-0.3 0.1-0.3 0.5 0.3 0.4-0.1 0.2-0.3 0.1-0.3 0.3-0.1 0.2 0.1 0.2-0.2 0.2 0 0.1 0.4 0.5 0.2 0.1 0.2 0.6 0 0.6-0.5 0.4 0.1 0.3-0.2 0.3 0.1 0.4 0.2 0.2-0.3 0.3 0 0.3 0.4 0 0.6 0.3-0.2 0.3 0.2 0.1-0.1 0.7 0.2z",CR:"M514.6 431.6l1.2 3.5 2 2.6 2.5 2.7-2.2 0.6-0.1 2.6 1.1 0.9-0.9 0.8 0.2 1.1-0.6 1.3-0.3 1.3-3-1.4-1.1-1.4 0.7-1.1-0.1-1.4-1.5-1.5-2.2-1.3-1.8-0.8-0.3-1.9-1.4-1.1 0.2 1.8-1.2 1.6-1.2-1.8-1.7-0.7-0.7-1.2 0.1-2 0.9-2-1.5-0.9 1.4-1.3 0.9-0.8 3.6 1.7 1.3-0.8 1.8 0.5 0.8 1.3 1.7 0.5 1.4-1.4z",CW:"M599 424.5l-0.3 0-0.4-0.3-0.3-0.1-0.3-0.3-0.1-0.2-0.3-0.1-0.2-0.4-0.3-0.3 0.1-0.5 0.5 0.3 0.1 0.5 0.4 0.4 0.7 0.2 0.2 0.3 0.3 0.4-0.1 0.1z",CV:"M847.8 406.5l-0.2 0.3-0.2-0.2 0-0.3 0.3-0.1 0.1 0.3z m1.9 0.1l-0.2 0.1-0.4-0.1-0.3-0.3-0.1-0.3 0.2-0.4 0.5-0.3 0.4 0 0.2 0.7-0.1 0.4-0.2 0.2z m3.5-3.1l0.2 0.1 0 0.2 0.2 0.2 0.4 0.5 0.2 0 0.2 0.3 0.2 0.5 0.2 0.2-0.3 0.5-0.3 0.1-0.5-0.1-0.3-0.1-0.3-0.3 0-0.2-0.2-0.1-0.1-0.4 0.1-0.2 0-0.3 0.2-0.4-0.2-0.5 0.3 0z m3.1 0l0.3 0.1 0.1 0.6-0.1 0.4-0.4 0.2-0.4-0.4 0.2-0.4-0.1-0.2 0.4-0.3z m2-5.7l0.3-0.2 0.5 0.2 0.1 0.2 0 0.7-0.4 0.3-0.3 0.2-0.3 0-0.5-0.2 0-0.5 0.2-0.2 0-0.6 0.1-0.1 0.3 0.2z m-8-2.8l0.4-0.1 0.3 0.2 0.4 0 0.3 0.2 0.1 0.2-0.5 0-0.7-0.3-0.2 0.1-0.3 0.6-0.3-0.6-0.1-0.1-0.1-0.4 0.3-0.1 0.4 0.3z m7.8-0.5l0.1 0.1-0.1 0.6-0.2 0.1 0-0.5-0.2-0.3 0-0.3-0.1-0.3 0.4-0.3 0.2 0.2-0.1 0.6 0 0.1z m-11.3-1.3l0.2 0.3 0.1 0.3-0.7 0.3-0.3-0.2-0.2-0.2 0.4-0.4 0.5-0.1z m-0.8-1.8l0.3 0.1 0 0.2 0.3 0.3-0.2 0.4-0.6 0.3-0.4 0.4-0.7 0.1 0-0.1 0-0.5-0.2-0.2 0-0.2 0.2-0.2 0.2 0 0.2-0.2 0.7-0.4 0.2 0z",CU:"M544.8 355.7l1.9 2.3 5.2-0.7 1.8 1.5 4.2 4 3.2 2.9 1.8-0.1 3.2 1.3-0.6 1.8 4 0.3 3.9 2.6-0.8 1.5-3.8 0.8-3.8 0.3-3.7-0.5-8.1 0.6 4.2-3.5-2.1-1.7-3.6-0.4-1.7-1.9-0.8-3.6-3.2 0.2-5-1.7-1.5-1.4-7.1-1-1.8-1.2 2.3-1.6-5.4-0.3-4.4 3.3-2.3 0.1-1 1.6-2.8 0.7-2.3-0.7 3.2-1.9 1.5-2.4 2.7-1.4 3-1.2 4.3-0.6 1.4-0.8 4.7 0.5 4.4 0.1 4.9 2.2z",SZ:"M1161.7 667.7l0.6 2.9 0.3 2.9-1.4 2.8-3.2 0.7-3.1-3.5 0.1-2.2 1.7-2.4 0.6-1.9 1.7-0.4 2.7 1.1z",SY:"M1195 287.5l-9.7 6.9-6.3-2.6-0.1 0 0.6-1-0.4-2.6 0.9-3.5 2.7-2.5-1.2-2.5-2.5-0.3-1.1-4.9 1-2.7 1.3-1.4 1.2-1.4-0.2-3.5 1.9 1.2 5.6-1.8 3 1.2 4.4 0 5.7-2.4 2.9 0.1 5.9-1-2.1 4-2.7 1.6 1.2 4.7-1 7.7-11 6.7z",SX:"M634.2 386l0 0.1-0.4-0.1 0-0.1 0.4 0.1z",KG:"M1400.5 230.2l-0.2 1.4-6.9 3.4-1 2.6-6.4 0.7-0.6 4.1-5.8-0.9-3.2 1.3-4.1 3 1.2 1.5-1.1 1.4-9.6 1-7.1-2.1-5.5 0.5-0.6-3.6 6 1 1.4-1.9 4.1 0.6 5.3-4.6-7.2-3.4-3.2 1.6-4.6-2.4 3-4.1-1.7-0.6 0.3-2.8 3.1-0.9 9.2 2.2-0.5-3.7 2.5-1.4 8.2 2.7 1.6-0.7 8.6 0.2 7.9 0.6 3.4 2.3 3.5 1z",KE:"M1223.5 476.7l-4.9 7.2 0.2 23.4 3.3 5.3-4 2.6-1.4 2.7-2.2 0.4-0.8 4.6-1.9 2.6-1.1 4.2-2.3 2.1-8.1-6.4-0.3-3.7-20.5-13.1 0.4-4.7-1.4-2.5 0-0.3 1.6-2.6 2.8-4.2 2.1-4.7-2.6-7.4-0.7-3.2-2.7-4.5 3.4-3.8 3.8-4.2 2.9 1.1 0.1 3.6 1.9 2.1 4 0 7.2 5.4 1.8 0.1 1.3-0.2 1.3 0.7 3.8 0.6 1.6-2.7 5.2-2.7 2.3 2.2 3.9 0z",SS:"M1178.1 441.1l0.2 5-0.8 1.9-3 0.2-1.9 3.6 3.5 0.5 3 3.1 1 2.6 2.6 1.5 3.5 7-3.8 4.2-3.4 3.8-3.5 3-4 0-4.5 1.5-3.6-1.5-2.3 1.8-5.1-4.3-1.4-2.7-3.1 1.3-2.6-0.4-1.5 1.1-2.6-0.8-3.5-5.3-0.9-2-4.3-2.6-1.4-3.8-2.4-2.8-3.9-3.3-0.1-2.1-3.1-2.6-3.9-2.5 1.7-0.7 2-1.2 1.3-5.8 1.5-2.9 4.1-0.9 1 1.7 2.9 3.8 1.6 0.5 2-1.1 4.1 0.3 0.8 1.3 5.6 0 0.2-1.3 2.9-1.3 0.5-1.8 2.1-1.3 4.8 3.7 2.9-0.7 2.7-4.6 3-3.5-0.5-3.9-1.5-1.8 3.4-0.4 0.4-1.4 2.6 0.4-0.6 4.8 0.9 4.6 2.9 2.5 0.8 2.2-0.1 3.2 0.8 0.2z",SR:"M681 464.9l-3.1 5.5 0.3 4.4 2.2 3.8-1.1 2.7-0.5 3-1.5 2.7-3.2-1.4-2.7 0.7-2.3-0.6-0.6 1.9 1 1.2-0.6 1.4-3.1-0.6-3.3-5.6-0.7-3.6-1.8 0-2.4-4.6 1.1-3.4-0.3-1.5 3.5-1.6 1-5.8 6.8 1.3 0.6-1.2 4.6-0.5 6.1 1.8z",KH:"M1589.8 410.6l1.8 4.3 0.1 7.7-9 5 2.8 3.8-5.9 0.5-4.6 2.6-4.8-0.9-2.6-3.4-3.5-6.6-2.1-7.8 3.1-5.3 7.1-1.2 5.3 0.9 5 2.5 2-4.4 5.3 2.3z",SV:"M492.5 415.9l-0.7 1.5-3.3-0.1-2-0.6-2.2-1.3-3-0.4-1.5-1.4 0.3-0.9 2-1.6 1.2-0.7-0.3-0.8 1.4-0.4 1.6 0.6 1.1 1.2 1.6 1.1 0.1 0.8 2.5-0.7 1.2 0.4 0.7 0.7-0.7 2.6z",KM:"M1233.2 581.3l-0.9-0.1-0.2-0.4 0-0.3 0.5 0.1 0.7 0.5-0.1 0.2z m3.7-1.9l0.2 1 0 0.7-0.1 0.2-0.2-0.3-0.4-0.3-0.1-0.2-0.3-0.1-0.6-0.4 0.1-0.1 0.5 0.2 0.3-0.1 0.2-0.4 0-0.2 0.3-0.1 0.1 0.1z m-5.7-1l-0.5-0.4-0.3-0.1-0.3-0.2-0.2-0.6 0.1-0.3 0-0.2 0.2-1.2-0.1-0.1 0.2-0.4 0.5-0.1 0.2 0.3-0.2 1.2 0.1 0.3 0.2 0.4 0.1 0.5 0.2 0.6-0.2 0.3z",ST:"M1023.9 501.3l-0.2 0-0.2 0.3-0.2 0-0.1-0.4-0.2-0.8 0-0.3 0.4-0.5 0.3-0.1 0.3-0.3 0.4 0.1 0.2 0.4 0.1 0.3-0.1 0.4-0.2 0.3-0.3 0.4-0.2 0.2z m4.6-9.8l-0.1 0.3-0.2 0.1-0.3-0.1 0-0.3 0.2-0.1 0-0.3 0.2-0.2 0.3 0.1-0.1 0.5z",SK:"M1098.1 187.7l-1.2 1.7-0.7 2.5-1 0.6-5.5-1.9-1.6 0.4-1 1.5-2.3 0.8-0.6-0.4-2.3 0.9-1.9 0.2-0.3 1.3-4.1 0.7-1.9-0.6-2.7-1.7-0.7-2.1 0.3-0.8 0.6-1.4 2.2 0.1 1.6-0.6 0.1-0.6 0.9-0.3 0.2-1.4 1.1-0.3 0.7-1.1 1.5 0 0.3 0.4 1.9-0.9 2.7 2.2 2.8-1.3 2.4 0.6 3.5-0.9 5 2.4z",KR:"M1652.9 259.5l0-0.6 2.5 0.2 0.6-2.8 3.6-0.4 2-0.4 0-1.5 8.3 7.5 3.3 4.2 3.4 7.4-0.5 3.5-4.3 1.2-3.1 2.7-4.6 0.5-2.1-3.5-1.1-4.8-5.3-6.6 3.4-1.1-6.1-5.5z",SI:"M1069.8 203.9l-3.9 1.7-0.3 2.5-1.7 0.7 0.1 1.7-2-0.1-1.8-1-0.8 1-3.6-0.2 1.1-0.5-1.4-2.7 0.4-3.1 4.2 0.5 2.4-1.5 4.4-0.1 0.9-1.1 0.8 0.1 1.2 2.1z",KP:"m 1660.3,229.9 1.4,1.1 -2.2,-0.4 -1,2.2 -0.3,2.1 2.8,4.6 -1.9,1.4 -0.3,1.1 -0.9,1.9 -2.9,1.1 -1.4,1.7 1.3,2.7 -0.3,0.7 2.6,1.1 4.4,2.8 v 1.5 l -2,0.4 -3.6,0.4 -0.6,2.8 -2.5,-0.2 v 0.6 l -3.2,-1.2 -0.1,1.2 -1.3,0.5 -0.7,-1.2 -1.7,-0.6 -1.9,-1 0.1,-2.8 0.9,-0.8 -1,-1.1 -0.4,-3.5 -0.9,-1 -3.4,-0.7 -3.4,-1.7 2.1,-4.1 3.9,-3.4 1.1,-4.6 3.6,2 4.6,0.3 -2.8,-3.4 6.4,-2.7 v -3.6 z",KW:"M1247.5 309.4l1.5 2.8-0.3 1.5 2.4 4.8-3.9 0.2-1.7-3.1-5-0.6 3.3-6.2 3.7 0.6z",SN:"M918 408l0.2 4 1.1 3.7 2 1.8 0.5 2.4-0.3 2-0.8 0.4-3.1-0.5-0.4 0.7-1.2 0.1-4.1-1.5-2.7-0.1-10.4-0.3-1.5 0.8-1.9-0.2-3 1-0.8-4.9 5.1 0.1 1.4-0.9 1 0 2.1-1.5 2.4 1.3 2.4 0.2 2.5-1.5-1.1-1.8-1.9 1-1.7 0-2.2-1.6-1.8 0.1-1.3 1.6-6.1 0.1-2.3-5-2.7-2.2 2.5-1.3 2.8-4.5 1.4-3.3 2-2 2.7 0.5 2.8-1.4 3.1 0 2.7 1.8 3.6 1.7 3.4 4.8 3.6 4.4z",SL:"M928.5 447.9l-2.6 3-2.6 3.4-0.3 1.9-1.4 2.1-1.5-0.5-4-2.6-3-3.4-0.9-2.4-0.7-4.7 3.1-2.9 0.6-1.7 1-1.4 1.6-0.2 1.3-1.2 4.5 0 1.6 2.3 1.2 2.7-0.2 1.9 0.9 1.7-0.1 2.3 1.5-0.3z",SC:"M1249 562.1l-0.3 0.2-0.5 0-0.4 0.2-0.4 0 0-0.2 0.5 0 0.4-0.1 0.4-0.3 0.2 0 0.1 0.2z m-0.5-0.3l-0.2 0.1-0.4 0 0-0.2 0.6 0.1z m51.9-30.3l0.4 0.4-0.2 0.3-0.2-0.3-0.3-0.2 0.2-0.5 0.1 0.3z",KZ:"M1338.3 160.5l4.4-0.3 9.2-5.8-0.8 2 8.4 4.7 18.3 15.6 1.1-3.2 8.4 3.5 6.2-1.6 3.3 1.1 4.1 3.6 4 1.2 3.3 2.7 6-0.9 4.4 3.8-1.9 4.2-3.8 0.6 2.5 6.2-1.6 2.9-10.8-2.1 1 11.3-2 1.4-9.1 2.5 8.8 11-2.9 1.6 1.7 3.7-3.5-1-3.4-2.3-7.9-0.6-8.6-0.2-1.6 0.7-8.2-2.7-2.5 1.4 0.5 3.7-9.2-2.2-3.1 0.9-0.3 2.8-2.6 1.2-5.4 4.4-0.9 4.6-2 0-2.3-3-6.7-0.2-2.5-5.2-2.6-0.1-1.5-6.4-7.6-4.6-8.6 0.5-5.7 0.9-6.6-5.7-4.8-2.4-9.2-4.5-1.1-0.5-12 3.7 6.2 23.4-2.6 0.3-4.8-5-3.9-1.8-5.6 1.3-1.8 2.2-0.6-1.6 0.6-2.6-1.5-2.2-6.5-2.2-3.7-5.7-3.2-1.6-0.6-2.1 5.1 0.6-1-4.6 4.1-1 4.7 0.9-0.7-6.1-1.9-3.9-5 0.3-4.7-1.5-5.1 2.7-4.4 1.4-2.8-1.1-0.2-3.2-4.3-4.2-3.6 0.2-5.3-4.2 1.7-4.8-1.8-1.2 2.2-6.9 6 3.6-0.6-4.5 8.1-6.7 7.6-0.2 12 4.3 6.6 2.5 4.4-2.6 7.7-0.1 7.3 3.2 0.8-1.9 7 0.3 0.2-2.9-9.4-4.3 3.5-3-1.5-1.6 4-1.6-5.1-4.2 1.4-2.1 17-2.1 1.7-1.5 10.9-2.3 3.1-2.5 9.1 1.3 4.4 6.3 4.3-1.5 7.1 2.1 1.1 3.3z",SA:"M1240.5 315l5 0.6 1.7 3.1 3.9-0.2 2.7 5.6 2.9 1.4 1.2 2.3 4 2.7 0.7 2.6-0.4 2.2 0.9 2.1 1.8 1.8 0.9 2.1 1 1.6 1.8 1.3 1.5-0.5 1.3 2.5 0.3 1.4 2.7 6.6 16.9 3.2 1-1.4 3 4.6-2.6 12.8-16.3 6.4-15.9 2.5-5 2.9-3.5 6.7-2.6 1.1-1.5-2.1-2.1 0.3-5.5-0.7-1.1-0.6-6.4 0.1-1.5 0.6-2.4-1.6-1.3 3.1 0.8 2.7-2.4 2.1-0.9-2.8-1.8-1.9-0.5-2.6-3.1-2.3-3.3-5.4-1.9-5.2-4.1-4.4-2.5-1.1-4.1-6.1-0.9-4.4 0-3.8-3.6-7.2-2.8-2.5-3-1.3-2.1-3.7 0.2-1.4-1.8-3.4-1.7-1.4-2.5-4.8-3.8-5.1-3.1-4.4-2.7 0 0.5-3.5 0.1-2.3 0.4-2.6 6.2 1.1 2.1-2 1.1-2.3 4.1-0.9 0.7-2.2 1.6-1-6-6.5 10.4-3.2 0.9-1 6.8 1.8 8.6 4.5 16.8 12.9 10.2 0.5z",SG:"m 1576.2725,494.53088 h 0.1 l 0.1,0.1 -0.1,0.1 -0.1,0.1 -0.1,0.1 v 0.1 h -0.1 -0.1 l -0.2,0.1 -0.1,0.1 h -0.1 v 0 0.1 h -0.1 -0.1 -0.1 v -0.1 0 l -0.1,-0.1 h -0.1 -0.1 l -0.2,-0.1 h -0.1 -0.1 -0.1 l -0.1,-0.1 0.1,-0.1 0.1,-0.2 0.1,-0.2 v -0.1 l 0.1,-0.1 h 0.1 0.1 0.1 l 0.2,-0.1 h 0.1 0.1 l 0.2,0.1 0.4,0.2 z",SE:"M1088.2 87l-7 1.6-3.5 3.9 1.3 3.5-6.2 4.5-7.8 5-2.1 8.1 3.7 4.1 4.8 3.3-3.3 6.6-4.6 1.4-0.6 10-2.1 5.7-5.7-0.6-2.2 4.8-5.5 0.3-1.9-5.7-4.5-6.9-4.2-8.4 1.8-3.4 3.4-4 0.8-6.9-3.1-2.9-1-7.7 2.4-5.4 4.3 0.1 1.3-2.2-1.8-2 5.7-7.9 3.4-6.1 2.3-3.9 4 0 0.6-3.1 8 0.9-0.1-3.6 2.6-0.2 6 2.7 7.2 3.7 1.8 8.5 1.8 2.2z",SD:"M1191 409.2l-0.7 5.5-2 6.4-3.3 3.1-2.3 5-0.5 2.6-2.6 1.8-1.5 6.7 0 0.8-0.8-0.2 0.1-3.2-0.8-2.2-2.9-2.5-0.9-4.6 0.6-4.8-2.6-0.4-0.4 1.4-3.4 0.4 1.5 1.8 0.5 3.9-3 3.5-2.7 4.6-2.9 0.7-4.8-3.7-2.1 1.3-0.5 1.8-2.9 1.3-0.2 1.3-5.6 0-0.8-1.3-4.1-0.3-2 1.1-1.6-0.5-2.9-3.8-1-1.7-4.1 0.9-1.5 2.9-1.3 5.8-2 1.2-1.7 0.7-0.5-0.3-1.9-1.9-0.4-2 0.8-2.6 0-2.7-3.3-4-0.7-2.7 0-1.6-2.1-1.9-0.1-3.7-1.3-2.5-1.9 0.4 0.5-2.4 1.4-2.6-0.7-2.7 1.8-2-1.2-1.5 1.3-3.9 2.5-4.8 4.8 0.5-1.1-25.5 0-2.7 6.4 0-0.5-12.8 22.3 0 21.5 0 22.1 0 2.1 6.3-1.2 1.1 1.2 6.7 2.5 7.6 2.2 1.6 3.2 2.4-2.7 3.6-4 1.1-1.7 2-0.3 4.2-2 9.5 0.7 2.5z",DO:"M585.7 386l0.3-1.8-1.3-1.9 1.5-1.1 0.7-2.5-0.1-3.4 0.8-1.1 4.3 0 3.2 1.6 1.5-0.1 0.7 2.3 3.1-0.2-0.4 1.9 2.5 0.3 2.5 2.3-2.3 2.6-2.6-1.4-2.6 0.3-1.8-0.3-1.1 1.2-2.2 0.4-0.7-1.6-1.9 0.9-2.7 4.4-1.3-1-0.1-1.8z",DM:"M642 401.7l0.5 0.1 0.2 0.5 0 0.6-0.1 0.8-0.1 0.2-0.2 0.1-0.4 0.2 0.1-0.4-0.1-0.2-0.1-0.7-0.3-0.5 0-0.3 0.1-0.3 0-0.3 0.2 0 0.2 0.2z",DJ:"M1229.5 428.2l-1.9 3.5-1.3-1.2-1.3 0.5-3.2-0.1-0.2-2-0.5-1.8 1.8-3 1.9-2.8 2.4 0.6 1.7-1.6 1.4 2-0.1 2.6-3.1 1.6 2.4 1.7z",DK:"M1046.1 147.7l-2.4 4.9-5.2-3.5-0.9-2.5 6.8-2 1.7 3.1z m-12.8 3.8l-2.9 0.9-3.7-0.8-2.1-3.4-0.4-6.1 0.6-1.7 1.3-1.8 4-0.3 1.6-1.7 3.6-1.7 0 3.1-1.2 2 0.7 1.6 2.6 0.9-1 2.3-1.4-0.6-3.1 4.3 1.4 3z",VG:"M626.1 383.4l-0.4 0.2-0.1 0-0.3 0.2-0.1-0.1 0.1-0.3 0.6-0.1 0.2 0.1z",DE:"M1053.9 158.9l1.4 3.1-1.2 1.7 1.9 2.1 1.5 3.3-0.2 2.2 2.4 3.9-2.2 0.6-1.3-0.7-1.1 1.2-3.5 1.2-1.7 1.6-3.4 1.3 1 1.8 0.7 2.7 2.6 1.5 3 2.6-1.6 2.9-1.7 0.8 1 4.1-0.4 1.1-1.7-1.3-2.4-0.2-3.5 1.1-4.4-0.3-0.6 1.7-2.7-1.7-1.4 0.3-5.5-1.9-1 1.3-4.2 0 0.4-4.5 2.4-4.2-7.2-1.2-2.4-1.6 0.2-2.7-1-1.4 0.4-4.2-1.1-6.5 2.9 0 1.2-2.3 0.9-5.6-0.9-2.1 0.8-1.3 4-0.3 1 1.3 3.1-3-1.3-2.3-0.4-3.4 3.7 0.8 2.9-0.9 0.3 2.3 4.9 1.4 0.1 2.2 4.7-1.2 2.6-1.6 5.6 2.4 2.4 1.9z",YE:"M1283.8 394.9l-4 1.7-0.9 2.9 0 2.2-5.4 2.7-8.8 3-4.7 4.5-2.5 0.4-1.7-0.4-3.2 2.7-3.5 1.2-4.7 0.3-1.4 0.4-1.1 1.7-1.5 0.5-0.8 1.6-2.8-0.2-1.7 0.9-4-0.3-1.6-3.8 0-3.5-1-1.9-1.3-4.7-1.8-2.6 1.1-0.4-0.7-2.9 0.6-1.2-0.4-2.8 2.4-2.1-0.8-2.7 1.3-3.1 2.4 1.6 1.5-0.6 6.4-0.1 1.1 0.6 5.5 0.7 2.1-0.3 1.5 2.1 2.6-1.1 3.5-6.7 5-2.9 15.9-2.5 5.2 10.6 2.2 4.5z",DZ:"M1031 264.6l-1 3.3 1 6.1-1.1 5.3-3.2 3.6 0.6 4.8 4.5 3.9 0.1 1.5 3.4 2.6 2.6 11.5 1.9 5.7 0.4 3-0.8 5.2 0.4 3-0.6 3.5 0.6 4-2.2 2.7 3.4 4.7 0.2 2.7 2.1 3.6 2.5-1.2 4.5 3 2.5 4-18.8 12.3-16 12.6-7.8 2.8-6.2 0.7-0.1-4.1-2.6-1.1-3.5-1.8-1.3-3-18.7-14-18.6-14-20.5-15.6 0.1-1.2 0.1-0.4 0.1-7.6 8.9-4.8 5.4-1 4.5-1.7 2.1-3.2 6.4-2.5 0.3-4.8 3.1-0.6 2.5-2.3 7.1-1.1 1-2.5-1.4-1.4-1.9-6.8-0.3-3.9-1.9-4.1 5.1-3.5 5.8-1.1 3.3-2.6 5.1-2 9-1.1 8.8-0.5 2.7 0.9 4.9-2.5 5.7-0.1 2.2 1.5 3.6-0.4z",US:"M118.8 379.3l-1.1 1.1-1.2-0.9 0.6-1.8-0.4-2.4 0.5-0.7 1.2-1-0.1-1.3 0.4-0.6 0.4 0.1 1.9 1.1 0.9 0.6 0.7 0.8 0.9 2.3-0.2 0.3-2.5 1.4-2 1z m-0.7-10l-2 0.4-0.6-1.3-0.5-0.5 0-0.4 0.7-0.6 1.8 0.6 1.2 1-0.6 0.8z m-3-3.4l-0.3 0.7-3-0.2 0.6-0.8 2.7 0.3z m-4.7-1l-0.4 0.4-0.4-0.1-1.9-0.2-0.3-1.5-0.2-0.2 1.7-0.9 0.4 0.4 1.1 2.1z m-8.4-4.2l-0.8 0.6-1.6-1.1 0.4-0.5 1-0.6 1.3 0.1-0.3 1.5z m437.5-166.2l-6.1 2-4.7 2.5-4.6 2.7-0.5 0.9 5.7-1.3 2.1 2.1 4.6-1.5 4.9-2.1 5.4-2.1-3.1 3.3 2.5 0.8 2.5 2.4 5.1-1.4 5.1-0.5 0.3 1.8 1.5 0.2 1.2 0.2 1.5 2.5-4.7 0.6-0.1 0-3.7-0.7-4.5 1.2-3.7 0.6-4.7 4.1-3 2.3 0.4 0.7 5.5-4.1 0.7 0-4.7 4.9-2.9 4.4-2.5 3.6-0.6 3.1-0.8 1.5-0.6 1.7 0.1 3.3 0.3 0.5 1.8-0.1 1.6-0.7 1.4-0.8 3.3-3.1 1.8-4.2-0.1-3.9 1.4-2.7 2.6-3.1 2.1-2.2 2.7-1.5-0.4 2.1 2.2-3.1 1.3-0.6 1.7-2.4 3.8 1.3 2.8 2.4-0.8 2.9-1.6 2.9-3.8 2.5-0.4 1.6 1 0 4.3-2.7 1.6 0.6-0.5 3.7-0.7 2.6-3.7 3.5-2 2.2-2.7 2.4 2.7 1.3 2.5 0.4 4-0.9 3.7-1.7 3-0.9 4.6-1.8 5.8-3.8 0.1-0.6 0.3-1.9 2.7-0.8 3.9 0.3 4 0.5 4.6-2.1 0.6-2.5-0.2-0.9 6.8-4.4 2.7-1.1 7.8-0.1 9.3 0 1.1-1.5 1.7-0.3 2.5-1 2.8-2.9 3.2-4.9 5.5-4.7 1.1 1.6 3.7-1 1.6 1.8-2.9 8.5 2.2 3.6 0.2 2.1-6.4 3-6 2.2-6 1.9-4 3.8-1.3 1.4-1.2 3.4 0.7 3.3 2.1 0.2 0.2-2.3 1.1 1.4-1 1.8-3.8 1-2.5-0.1-4.2 1.1-2.3 0.3-3.1 0.3-5 1.9 8.1-1.2 1.1 1.2-7.9 1.9-3.3 0 0.4-0.8-2.1 1.8 1.4 0.3-2.5 4.6-5.3 4.9 0.1-1.7-1.1-0.3-1.2-1.6 0 3.5 1 1.1-0.6 2.4-2.4 2.5-4.5 5.1-0.4-0.2 2.9-4.4-2-2.4 1-5.4-1.9 2.8 0 4.1-3.2-1 3 2-1.5 6.1 1.4 0.5 0 2.2-1 6.4-4.6 4.7-6.1 1.9-4.4 3.8-2.8 0.4-3.4 2.4-1.3 2.1-6.9 4.2-3.8 3.1-3.5 3.8-1.9 4.5 0 4.5 0.6 5.5 1.5 4.5-0.5 2.8 1.3 7.4-1 4.4-0.6 2.5-2 3.9-1.8 0.8-2.6-0.8-0.4-2.8-1.8-1.5-2-5.5-1.6-4.9-0.4-2.5 2-4.3-0.8-3.5-3.1-5.4-1.9-1-6.1 3-0.9-0.4-2-3-3-1.6-6.4 0.9-4.6-0.8-4.3 0.5-2.5 1 0.6 1.7-0.7 2.6 0.8 1.3-1.2 0.8-1.8-0.9-2.3 1.2-3.9-0.2-3.3-3.4-4.9 0.8-3.6-1.5-3.5 0.5-5 1.5-6.1 4.7-6.1 2.8-3.7 3-1.9 2.9-1 4.5-0.4 3 0.6 2.2-2.2 0.2-3.6-1.4-3.9-2-0.9-3-0.2-4.5-2.4-3.6-0.9-3.8-1.6-4.4-3.2-2.6-4.5 0.2-4.8 5-4-1.9-2.3-1.9-0.4-3.6-0.8-3.3-2.4-2.8-2.1-2.1-1.3-2.3-9.4 0-0.8 2.7-4.3 0-10.8 0-10.9-4.5-7-3.1 0.9-1.3-7.1 0.7-6.3 0.5 0.3-3.2-2.1-3.7-2.2-0.8 0.1-1.8-2.9-0.4-1.2-1.7-4.8-0.6-0.9-1.1 0.8-3.5-2.5-6.4-0.5-8.9 0.9-1.5-1.3-2.1-1.5-5.4 1.8-5.2-0.9-3.5 3.9-5.3 2.8-5.4 1.1-4.9 5.5-6 4-5.7 4-5.7 4.3-8.5 1.8-5.3 0.4-2.9 1.4-1.3 5.8 2.2-1 5.9 2.2-1.7 2.5-5.1 1.6-5.1 14.1 0 14.7 0 4.8 0 15.1 0 14.7 0 14.8 0 14.9 0 16.8 0 17 0 10.2 0 1.3-2.4 1.7 0-0.9 3.4 1 1 3.3 0.4 4.6 1 3.9 1.9 4.4-0.8 5.4 1.7z m-264.5-55.9l-7 2.3-0.8-1.6 2.3-2.8 6.4-2.1 3.5-0.9 2.6 0.4 0 1.9-7 2.8z m-39-16.6l-3.9 0.9-1.7-1.1-0.8-1.6 5.7-1 3 0.6-2.3 2.2z m1.2-22.4l1.2 1 3.5-0.5 1.6 1.5 3.3 0.7-1.2 0.7-4.9 1.2-1.7-1.3-0.3-1-4.3 0.3-0.3-0.5 3.1-2.1z m172.8-33l-24.6 20.4-35.6 32.7 4.2 0.2 2.8 1.6 0.5 2.6 0.3 3.8 7.6-3.3 6.5-1.9-0.6 3.1 0.8 2.4 1.6 2.7-1.1 4.2-1.4 6.9 4.6 3.8-3.2 3.8-5.1 2.9-0.6-2.2-2.5-2 3.3-5.2-1.6-4.9 2.7-5.6-4.1-0.4-7.1-0.1-3.8-1.8-3.3-6.1-3.3-1.1-5.7-2.1-6.8 0.5-6-2.7-2.7-2.5-6.3 1.2-3.5 4.1-2.9 0.4-6.6 1.2-6.2 2-6.4 1.3 3.2-3.5 8.4-5.8 6.8-1.8 0.4-1.4-9.4 3.2-7.4 3.9-11.2 4.2 0.2 2.9-9 4.2-7.7 2.5-6.6 1.9-4 2.6-10.6 3.1-4.5 2.8-8.2 2.6-2.7-0.4-6.2 1.6-7 2.1-6.1 2-10 1.8 0.4-1.1 8.2-2.8 6.6-1.9 8.6-3.3 6.5-0.6 5-2.5 10.4-3.6 2.3-1.2 5.7-2.1 5.8-4.5 6.2-3.5-7.3 1.8-0.3-1.1-4.9 2.2 0.4-3-3.6 2.1 1.6-2.9-7.3 2.3-2.8 0 3.7-3.5 3.3-2.2-0.4-2.1-7.2 1.2-0.6-2.8-1.3-1.4 4-3.3-0.4-2.5 5.9-3.3 7.7-3.3 5.3-2.9 4.1-0.4 1.8 0.9 7.1-2.8 2.5 0.5 5.6-1.8 2.5-2.6-1.1-1 6-2.2-2.8 0.1-6.2 1.2-2.9 1.3-1.8-1.3-6.9 0.7-4.6-1.4 1.2-2.3-1-3.2 9.2-2.4 13.2-2.7 3.5 0-4.3 2.8 9.2-0.2 1.2-3.5-2.3-2.1 0.8-2.8-0.7-2.3-3.3-1.7 6.5-2.9 7.5-0.2 8.8-2.4 4.8-2.6 7.9-2.6 4.8-0.6 11.2-2.4 3.1 0.4 10.4-2.8 4.4 1.1-0.5 2.4 3.3-1 6.3 0.3-1.9 1.2 4.9 0.9 4.9-0.5 6.2 1.6 7.2 0.6 2.2 0.6 6.6-0.8 4.1 1.6 3.5 0.7z",UY:"M699.7 718.6l-1.6 4.1-5.4 3.5-4.2-1.3-2.8 0.7-5.5-2.7-3.6 0.2-3.9-3.6-0.4-4.1 0.9-1.4-1.2-6.4 0.4-6.6 0.5-5.2 3.4-0.7 6.3 5 1.9-0.2 6.3 4.1 4.8 3.6 3.8 4.3-1.8 3.1 2.1 3.6z",YT:"M1240.2 583.1l0.2 0.3 0.5 0.2 0 0.3-0.2 0.2 0.1 0.2-0.3 0.6 0.1 0.2-0.3 0.1-0.2-0.3 0-0.3 0.2-0.2-0.2-0.7-0.1-0.1-0.1-0.2 0.3-0.3z",LB:"M1179.1 288.2l-1.4 0.1-0.4 1.1-1.8 0 1.3-5.3 2.2-4.5 0-0.2 2.5 0.3 1.2 2.5-2.7 2.5-0.9 3.5z",LC:"M643.7 413.8l-0.2-0.2-0.4-0.2-0.1-0.2 0-0.6 0.1-0.2 0.7-1.1 0.3 0.2 0 0.7-0.1 0.8-0.1 0.4-0.2 0.4z",LA:"M1589.8 410.6l-5.3-2.3-2 4.4-5-2.5 1.5-2.9-0.4-5.4-5.3-5.6-1.3-6.4-5-5.2-4.3-0.4-0.8 2.2-3.2 0.2-1.9-1.1-5.3 3.8-1-5.8 0.4-6.7-3.8-0.3-0.9-3.9-2.7-2 0.8-2.3 4.1-4.2 0.8 1.5 3 0.2-2-7.4 2.7-0.9 4 5.1 3.5 5.8 6.8 0 3 5.6-3.3 1.7-1.2 2.3 7.3 3.9 5.7 7.6 4.4 5.6 4.9 4.5 2 4.5-0.2 6.4z",TV:"M1998.9 556.6l0-0.2 0.1 0 0 0.1-0.1 0.1z",TW:"M1657.9 355.5l-1.4 5.3-4-5.5-1.5-4.7 1.9-6.3 3.3-4.9 3 1.9-0.1 3.9-1.2 10.3z",TT:"M642.8 432.2l-0.2 0.5-0.3 0.4-0.2 0.1-0.1 0.4 0.2 0.6-0.2 0.1 0 0.7 0.2 0.3 0.2 0.2-0.2 0.2-0.1 0.3 0 0.7-0.2 0.1-0.4 0.2-0.5 0.1-0.3 0-0.5 0.1-0.5-0.1-0.4 0.1-0.3-0.1-0.5 0.2-0.4-0.2-0.6 0-0.3 0.2-0.3-0.1 0.5-0.2 0.3-0.3 0.4-0.1 0.4-0.3 0.2-0.3 0.6 0.1 0.3-0.4-0.2-0.8 0.2-0.4 0-0.5 0-0.2-0.3-0.4-0.3-0.1-0.4-0.1-0.1-0.1 0.4-0.3 1.1-0.1 0.2-0.2 1.1 0 0.1-0.1 1.2-0.2 0.2 0z m2.4-3.2l-0.1 0.5-0.3 0.1-0.4 0.3-0.2 0-0.7 0.4-0.2-0.2 0.3-0.3 0.7-0.5 0.9-0.3z",TR:"M1201.7 235.3l5.5-0.3 5.6 3.2 1.3 2.2 0.1 3.1 4.2 1.6 2.4 1.8-3.3 1.9 2.9 7.3-0.7 2 3.8 5.1-2.4 1.1-2.1-1.6-6.3-0.9-2.1 1-5.9 1-2.9-0.1-5.7 2.4-4.4 0-3-1.2-5.6 1.8-1.9-1.2 0.2 3.5-1.2 1.4-1.3 1.4-2.3-2.9 1.7-2.4-3.2 0.6-4.6-1.5-3.2 3.7-8 0.7-4.7-3.4-5.7-0.2-1 2.6-3.6 0.8-5.4-3.4-5.8 0.1-3.8-6.4-4.2-3.5 2-5-3.6-3.1 5.1-6.1 8-0.2 1.6-4.9 10 0.9 5.6-4.1 5.8-1.8 8.5-0.2 9.8 4.5 7.9 2.5 5.8-1 4.6 0.6 5.5-3.4z m-79.8 4.6l1.2-0.7 1.1-4-2.7-1.7 5-2 4.6 0.8 0.9 2.5 4.8 2-0.7 1.6-6.2 0.3-2 2-3.9 3.4-2-2.9-0.1-1.3z",LK:"M1445.9 462l-4.8 1.5-2.9-5.1-1.4-9.2 2-10.4 4.1 3.5 2.8 4.5 3.1 6.7-0.6 6.7-2.3 1.8z",LV:"M1112.8 136.5l2.5 1.3 1 2.9 2.1 3.6-4.6 2.3-2.7 1-5-2.9-2.5-0.4-0.9-1.2-4.5 0.6-7.9-0.4-5.1 1.8-0.5-4.5 1.7-3.8 4.1-2 4.4 4.5 3.7-0.2 0.1-4.6 3.8-1 2.1 0.7 4.4 2.2 3.8 0.1z",TO:"M14.7 639.5l-0.5-0.3 0-0.2 0.3-0.2 0.2 0.7z m-2.8-2.1l0.3 0 0.4 0.3 0.3 0.1 0.2-0.4 0.3 0.3-0.3 0.3 0.1 0.2-0.1 0.2-0.2-0.1-0.3-0.3-0.7-0.2 0-0.4z m-0.8-8.6l-0.2 0.1-0.1-0.5 0.2 0.1 0.1 0.3z m4.4-7.5l-0.5 0.3-0.2 0-0.1-0.1 0.3-0.5 0.3 0.1 0.2 0.2z m-14.1-19.3l-0.1 0.1-0.3-0.1 0.1-0.2 0.3 0 0 0.2z",LT:"M1111.1 147.6l1 2.7-3.6 2-0.5 3.4-4.8 2.3-4.7 0-1.4-1.9-2.5-0.7-0.6-1.5 0.2-1.7-2.2-0.9-5.1-1.1-1.7-5.1 5.1-1.8 7.9 0.4 4.5-0.6 0.9 1.2 2.5 0.4 5 2.9z",LU:"M1016.9 185.4l-1.4 0.1-1.1-0.5 0.4-3.5 1.3-0.2 1 1.4-0.2 2.7z",LR:"M938.6 452.5l-0.2 1.8 0.5 3.1-1.2 2.9 1.6 1.8 1.8 0.4 2.4 2.7 0.2 2.5-0.6 0.8-0.4 5.3-1.5 0.1-5.8-3.1-5.2-4.9-4.8-3.5-3.8-4.1 1.4-2.1 0.3-1.9 2.6-3.4 2.6-3 1.3-0.2 1.4-0.7 2.4 3.9-0.4 2.6 1.1 1.4 1.6 0 1.1-2.6 1.6 0.2z",LS:"M1139.1 697.9l-2 0.7-3.7-5 3.2-4 3.1-2.5 2.7-1.4 2.2 2 1.7 2-1.9 3.1-1.1 2.1-3.1 1-1.1 2z",TH:"M1577.5 410.2l-5.3-0.9-7.1 1.2-3.1 5.3 2.1 7.8-5.3-3-4.8 0.2 0.3-5.1-4.9 0 0.2 7.1-2.2 9.4-1.4 5.7 0.7 4.6 3.7 0.2 2.7 5.9 1.3 5.5 3.4 3.7 3.4 0.7 3.1 3.4-1.7 2.6-3.7 0.8-0.6-3.3-4.8-2.8-0.9 1.1-2.3-2.4-1.2-3.2-3.2-3.6-2.9-3.1-0.7 3.8-1.3-3.6 0.4-4 1.2-6.1 2.2-6.6 2.6-6-2.7-5.9-0.2-3-1-3.6-4.3-5.1-1.8-3.2 1.8-1.2 1.4-5.5-2.9-4.3-4.1-4.7-3.5-5.6 2.2-1.2 1.5-6.9 3.9-0.3 2.8-2.8 3-1.5 2.7 2 0.9 3.9 3.8 0.3-0.4 6.7 1 5.8 5.3-3.8 1.9 1.1 3.2-0.2 0.8-2.2 4.3 0.4 5 5.2 1.3 6.4 5.3 5.6 0.4 5.4-1.5 2.9z",TG:"M991.4 431.2l-0.7 3.4 1.7 1.9 2 2.2 0.2 3.2 1.2 1.3-0.3 14.8 1.4 4.4-4.5 1.4-1.3-2.3-1.5-4.1-0.5-3.2 1.3-5.7-1.4-2.4-0.6-5 0-4.7-2.3-3.3 0.4-2 4.9 0.1z",TD:"M1119.2 376.1l1.1 25.5-4.8-0.5-2.5 4.8-1.3 3.9 1.2 1.5-1.8 2 0.7 2.7-1.4 2.6-0.5 2.4 1.9-0.4 1.3 2.5 0.1 3.7 2.1 1.9 0 1.6-3.6 1.1-2.8 2.6-4 7-5.2 3-5.5-0.4-1.6 0.6 0.6 2.2-2.9 2.3-2.4 2.5-7.1 2.4-1.4-1.4-1-0.2-1 1.7-4.6 0.5 0.8-1.8-1.8-4.4-0.8-2.6-2.5-1.1-3.4-3.8 1.2-3 2.6 0.6 1.6-0.4 3.2 0-3.2-5.8 0.2-4.3-0.5-4.2-2.3-4.1 0.6-3.1-3.7-0.1 0-4.1-2.4-2.4 2.3-8.5 7-6 0.2-8.4 1.8-13 1.2-2.8-2.4-2.2-0.1-2-2.2-1.7-1.6-10 5.5-3.5 22.5 12.3 22.6 12.3z",TC:"M587.7 361.6l0.7 0 0.3 0.4-0.3 0-0.3-0.1-0.5 0.1-0.1-0.3 0.2-0.1z m-2.7-0.1l0.3 0.4 0.6-0.1-0.2 0.2-0.6 0-0.4-0.2 0.3-0.3z m2.2-0.6l0 0.5-0.5-0.2-0.1-0.3 0.1-0.1 0.5 0.1z",BQSA:"M632.8 388.7l-0.1 0 0.1-0.2 0.1 0.1-0.1 0.1z",LY:"M1122.6 299.1l-1.7 3.1 1 2.8-1.1 3.9 2 5.2 1.3 22.8 1 23.7 0.5 12.8-6.4 0 0 2.7-22.6-12.3-22.5-12.3-5.5 3.5-3.8 2.4-3.2-3.5-8.8-2.8-2.5-4-4.5-3-2.5 1.2-2.1-3.6-0.2-2.7-3.4-4.7 2.2-2.7-0.6-4 0.6-3.5-0.4-3 0.8-5.2-0.4-3-1.9-5.7 2.6-1.4 0.4-2.8-0.6-2.6 3.6-2.5 1.6-2.1 2.6-1.8 0.1-4.9 6.4 2.2 2.3-0.6 4.5 1.1 7.3 2.9 2.8 5.7 4.9 1.2 7.8 2.7 6 3.2 2.5-1.7 2.5-2.9-1.6-4.9 1.5-3.2 3.7-3 3.7-0.8 7.4 1.3 2 2.8 2 0.1 1.8 1.1 5.4 0.7 1.5 2.1z",VC:"M642.2 417.4l-0.1 0.1-0.5-0.3 0-0.4 0.2-0.3 0.2-0.5 0.4 0 0.1 0.4-0.1 0.8-0.2 0.2z",AE:"M1296.2 336.7l1.3 5.1-2.8 0 0 4.2 1.1 0.9-2.4 1.3 0.2 2.6-1.3 2.6 0 2.6-1 1.4-16.9-3.2-2.7-6.6-0.3-1.4 0.9-0.4 0.4 1.8 4.2-1 4.6 0.2 3.4 0.2 3.3-4.4 3.7-4.1 3-4 1.3 2.2z",VE:"M648.7 448.1l-4.7 3.8-0.5 2.3 1.8 2.4-1.4 1.2-3.5 1 0 3-1.6 1.8 3.7 4.8 0.7 1.8-2.1 2.5-6.4 2.4-4.1 1-1.7 1.5-4.5-1.6-4.1-0.8-1.1 0.6 2.5 1.6-0.3 4.3 0.7 4.1 4.8 0.5 0.3 1.4-4.1 1.8-0.7 2.7-2.4 1.1-4.2 1.5-1.1 2-4.4 0.4-3.1-3.4-1.6-6.4-1.5-2.3-2-1.4 2.9-3.2-0.2-1.4-1.5-1.9-1.1-4.3 0.6-4.6 1.3-2.2 1.2-3.4-2-1.1-3.2 0.7-4.1-0.3-2.3 0.7-3.8-5.6-3.3-0.8-7.3 0.6-1.2-2.2-1.4-0.5-0.1-1.4 0.7-2.4-0.3-2.5-1.2-1.5-0.6-2.9-2.9-0.4 1.8-3.8 0.9-4.6 1.8-2.4 2.3-1.8 1.6-3.2 3.7-1.1-0.2 1.5-3.4 0.8 1.7 2.9-0.3 3.4-2.7 3.7 1.9 5.1 2.5-0.4 1.5-4.7-1.7-2.2 0-4.9 7.2-2.6-0.6-3 2.1-2.1 1.7 4.6 4 0.1 3.4 3.5 0.2 2.2 5 0 6.1-0.6 3.1 2.8 4.2 0.8 3.3-2 0.1-1.6 7.1-0.4 6.7-0.1-4.9 1.9 1.8 3.1 4.5 0.4 4.2 3.2 0.7 5.1 2.9-0.1 2.2 1.5z",AG:"M640.7 391.9l0.6 0.3-0.2 0.3-0.2 0-0.7 0.1-0.2-0.1 0-0.5 0.3-0.1 0.1-0.3 0.2 0 0.1 0.3z m0-3.7l0.2 0 0.2 0.2 0.1 0.4-0.1 0.3-0.2 0.1-0.1-0.2-0.3-0.2 0-0.5 0.2-0.1z",AF:"M1383 261.6l1.5 1.8-2.9 0.8-2.4 1.1-5.9 0.8-5.3 1.3-2.4 2.8 1.9 2.7 1.4 3.2-2 2.7 0.8 2.5-0.9 2.3-5.2-0.2 3.1 4.2-3.1 1.7-1.4 3.8 1.1 3.9-1.8 1.8-2.1-0.6-4 0.9-0.2 1.7-4.1 0-2.3 3.7 0.8 5.4-6.6 2.7-3.9-0.6-0.9 1.4-3.4-0.8-5.3 1-9.6-3.3 3.9-5.8-1.1-4.1-4.3-1.1-1.2-4.1-2.7-5.1 1.6-3.5-2.5-1 0.5-4.7 0.6-8 5.9 2.5 3.9-0.9 0.4-2.9 4-0.9 2.6-2-0.2-5.1 4.2-1.3 0.3-2.2 2.9 1.7 1.6 0.2 3 0 4.3 1.4 1.8 0.7 3.4-2 2.1 1.2 0.9-2.9 3.2 0.1 0.6-0.9-0.2-2.6 1.7-2.2 3.3 1.4-0.1 2 1.7 0.3 0.9 5.4 2.7 2.1 1.5-1.4 2.2-0.6 2.5-2.9 3.8 0.5 5.4 0z",IQ:"M1223.5 263.2l4.7 7.6 3.8 2 0.9 3.7-2.3 2.2-0.5 5 4.6 6.1 7.1 3.6 3.5 4.8-0.2 4.7 1.7 0 0.5 3.4 3.5 3.4-3.3-0.3-3.7-0.6-3.3 6.2-10.2-0.5-16.8-12.9-8.6-4.5-6.8-1.8-3.1-7.8 11-6.7 1-7.7-1.2-4.7 2.7-1.6 2.1-4 2.1-1 6.3 0.9 2.1 1.6 2.4-1.1z",VI:"M624.5 387.8l0.3 0.3-1.1 0.2-0.1-0.5 0.7-0.2 0.2 0.2z m0.7-3.8l-0.2 0.2-0.5 0 0-0.2 0.3-0.1 0.4 0.1z m-1.4-0.2l0.4 0.3-0.2 0.2-0.3-0.2-0.5 0 0.2-0.2 0.4-0.1z",IS:"M924.8 84.5l-1.4 3.6 4.4 3.8-6.1 4.3-13.1 3.9-3.9 1.1-5.6-0.9-11.9-1.8 4.8-2.5-9-2.7 7.9-1.1 0.1-1.7-8.8-1.3 3.6-3.7 6.6-0.8 6 3.8 7-3 5.1 1.5 7.3-2.9 7 0.4z",IR:"M1229 253.2l1.8-0.2 5.3-4.7 1.9-0.5 1.9 1.9-1.2 3.1 3.9 3.4 1.3-0.4 2.5 4.8 5.3 1.3 4.3 3.2 7.7 1.1 8-1.7 0.2-1.5 4.4-1.2 3-3.7 3.6 0.2 2-1.2 3.9 0.6 6.6 3.3 4.3 0.7 7.3 5.6 4 0.3 1.7 5.3-0.6 8-0.5 4.7 2.5 1-1.6 3.5 2.7 5.1 1.2 4.1 4.3 1.1 1.1 4.1-3.9 5.8 3.2 3.4 2.8 3.9 5.7 2.8 1 5.6 2.7 1.1 0.9 2.9-7.5 3.4-1.1 7.4-10.6-1.9-6.2-1.5-6.3-0.8-3.3-7.9-2.8-1.1-4.1 1.1-5.1 3.1-7-2.1-6.1-5-5.5-1.8-4.4-6.1-5.2-8.5-2.8 1-3.7-2.1-1.7 2.5-3.5-3.4-0.5-3.4-1.7 0 0.2-4.7-3.5-4.8-7.1-3.6-4.6-6.1 0.5-5 2.3-2.2-0.9-3.7-3.8-2-4.7-7.6-3.8-5.1 0.7-2-2.9-7.3 3.3-1.9 1.2 2.5 3.2 2.9 3.8 0.9z",AM:"M1230.8 253l-1.8 0.2-2.8-3.7-0.2-1-2.3 0-1.9-1.7-1 0.1-2.4-1.8-4.2-1.6-0.1-3.1-1.3-2.2 7-1 1.4 1.6 2.2 1.1-0.7 1.6 3.2 2.2-1.1 2.1 2.6 1.7 2.5 1 0.9 4.5z",AL:"M1088 228l0.4 1.2 1.4-0.6 1.2 1.7 1.3 0.7 0.6 2.3-0.5 2.2 1 2.7 2.3 1.5 0.1 1.7-1.7 0.9-0.1 2.1-2.2 3.1-0.9-0.4-0.2-1.4-3.1-2.2-0.7-3 0.1-4.4 0.5-1.9-0.9-1-0.5-2.1 1.9-3.1z",AO:"M1121.2 572l0.6 2-0.7 3.1 0.9 3-0.9 2.4 0.4 2.2-11.7-0.1-0.8 20.5 3.6 5.2 3.6 4-10.4 2.6-13.5-0.9-3.8-3-22.7 0.2-0.8 0.5-3.3-2.9-3.6-0.2-3.4 1.1-2.7 1.2-0.5-4 0.9-5.7 2-5.9 0.3-2.7 1.9-5.8 1.4-2.6 3.3-4.2 1.9-2.9 0.6-4.7-0.3-3.7-1.6-2.3-1.5-3.9-1.3-3.8 0.3-1.4 1.7-2.5-1.6-6.2-1.2-4.3-2.8-4.1 0.6-1.2 2.3-0.9 1.7 0.1 2-0.7 16.7 0.1 1.3 4.7 1.6 3.9 1.3 2.1 2.1 3.3 3.8-0.5 1.8-0.9 3.1 0.9 0.9-1.6 1.5-3.7 3.5-0.3 0.3-1.1 2.9 0-0.5 2.3 6.8-0.1 0.1 4.1 1.1 2.4-0.9 3.9 0.4 4 1.8 2.4-0.4 7.6 1.4-0.6 2.4 0.2 3.5-1 2.6 0.4z m-65.9-33l-1.5-4.8 2.3-2.8 1.7-1.1 2.1 2.2-2 1.4-1 1.6-0.2 2.8-1.4 0.7z",AS:"M27.7 593.3l-0.3 0.4-0.2 0.3-0.2 0.1-0.4-0.3 0.2-0.2 0.3 0 0.1-0.2 0.5-0.1z m6.6 0l-0.5 0.1-0.1-0.2 0.4-0.1 0.2 0.2z",AR:"M669.1 851.7l-3-0.2-5 0-6-13.6 3.1 2.8 4.3 4.6 7.8 3.7 7.3 1.5-0.8 3-4.4 0.3-3.3-2.1z m-30.5-207l11.3 10.4 4.6 1 7.3 4.8 5.9 2.5 1.1 2.8-4.2 9.8 5.8 1.7 6.3 1 4.2-1 4.3-5 0.3-5.6 2.6-1.3 3.2 3.8 0.4 5.1-4.2 3.5-3.3 2.6-5.3 6.3-6 8.7-0.5 5.2-0.4 6.6 1.2 6.4-0.9 1.4 0.4 4.1 0.3 3.4 7.8 5.5 0.2 4.4 3.9 2.8 0.3 3.1-3.3 8.2-7 3.5-10.2 1.3-6-0.7 2.1 3.9 0.1 4.7 1.8 3.2-2.5 2.3-5.1 0.9-5.6-2.4-1.5 1.7 2.5 6.3 4 1.9 2.3-2 2.5 3.3-4.2 2-2.9 4 1.2 6.3-0.1 3.4-4.8 0-3 3.2 0.1 4.8 6.5 4.6 5.2 1.2 0.2 5.7-4.6 3.5-0.6 7.3-3.5 2.4-0.9 2.9 4.2 6.5 4.6 3.5-2.1-0.3-4.9-1-12.1-0.8-3.5-3.6-1.9-4.6-3.1 0.4-2.6-2.3-3.1-6.5 2.7-2.8 0.1-3.9-1.8-3.2 0.7-5.4-1.1-8.3-1.8-3.7 1.8-1.2-1.4-2.4-2.8-1.2 0.8-2.7-3.1-2.4-3.7-7.3 1.7-1.3-3.3-7.8-0.8-6.5-0.2-5.7 2.5-2.3-3.3-6.3-1.6-5.8 3-4.2-1.4-5.4 1.6-6.2-1.4-5.9-1.6-1.2-4.9-11.1 2.1-6.6-1.7-6.2 0.9-5.9 2.6-6 3.3-4-2-2.5 0.8-2.1-1.6-10.7 5.6-3.1 1.2-6.7-0.9-1.6 4-5.8 7.5 1.6 3.7 4.6 1.6-5.2 6.4 0.3 1 1.4z",AU:"M1743 763.6l3.7 2.2 3.3-0.9 4.9-1.2 2.8 0.4-4.5 7.6-3.3 2.1-4 5.2-0.6-1.8-6.6 4.4-0.8-0.3-3-0.2 0.5-5.4 2-4.2 0.6-5.6 2-2.9 3 0.6z m50.5-173.4l1.2 5 4-2.4 1.4 2.7 2.3 2.5-1.1 2.9 0.2 5.5 0.2 3.2 1.3 0.8 0.4 5.5-1.2 3.3 0.8 4.3 5.4 3.4 3.2 3 3.2 2.8-1.1 1.6 2.3 4 0.5 7 2.6-1.4 1.5 2.7 1.6-0.9-0.7 6.8 2.9 3.9 1.9 2.4 2.8 5.2 0 5.2-1 3.7-1.8 3.9 0.7 5.5-2.5 5.7-2.1 3-3.8 5.7-1.5 3.7-3.1 4.6-5 5.8-5.5 3.2-4.4 4.9-3.3 3.2-4.4 5.5-3.7 3.2-3.9 4.8-3.1 4.4-0.8 2.1-4.3 2.2-6.1 0.2-6.3 2.7-3.8 2.4-4.8 2.8-2.7-2.9-2.6-1.1 2.6-3.3-3.5 1.2-7.2 4.6-3.6-1.7-2.4-1-2.8-0.5-4.1-1.8-1.3-4 1.5-4.8 0.4-3.3-1.4-2.6-4.7-0.7 3.2-3.2 0.9-4.7-4.7 4.4-5.3 1.2 4.5-3.5 2.5-3.7 3.5-3.2 1.6-4.7-6.8 5.4-4.3 2.2-4.3 5.1-3-2.6 1.7-3.4-1.4-4.7-1.8-2.4 1.6-1.5-5.3-3.9-3.8-0.1-4-3.2-9.9 0.6-7.9 2.3-6.9 2.2-5-0.4-7 3.3-5.3 1.4-2.3 3.4-3 2.6-4.7 0.2-3.6 0.5-4.2-1.1-4.2 0.7-3.8 0.3-4.5 3.4-1.5-0.3-3.4 1.8-3.4 2-3.8-0.2-3.5 0-4.1-4.1-2.4-1.2 1.5-3.7 2.9-0.9 1.5-1.4 0.6-2.3 2.3-4.5 0.7-3.8-0.7-6.5 0.2-3.7 1.4-3.6-0.9-4.2 0.3-1.9-1.7-2.6 0.7-5-1.9-5.1-0.1-2.7 1.8 2.8-0.5-6 2.3 1.9 1.1 2.5 0.6-3.3-1.6-5.1-0.1-2-0.8-1.9 1.3-3.7 1.5-1.6 1.3-3.2 0.1-3.8 3.1-4.6-0.4 4.9 3.1-4.4 4.9-2.2 3.2-2.7 4.7-2.4 2.6-0.5 1.4 0.8 4.8-2.4 3.5-0.7 1.1-1.4 1.5-0.6 3.1 0.2 6.2-1.9 3.5-2.9 2-3.4 3.9-3.2 0.7-2.6 0.7-3.5 4.9-5.5 1.4 5.6 2.6-1.3-1.5-3 2.3-3.1 2.2 1.4 1.5-4.9 3.5-3.2 1.8-2.5 2.9-1.1 0.4-1.8 2.3 0.7 0.4-1.6 2.6-0.9 2.8-0.9 3.7 3 2.6 3.8 3.5 0 3.5 0.6-0.7-3.5 3.5-5.1 2.7-1.7-0.6-1.6 2.9-3.7 3.7-2.3 2.7 0.8 4.9-1.2 0.3-3.3-3.9-2.1 3.1-0.9 3.6 1.6 2.7 2.6 4.5 1.6 1.7-0.6 3.3 2 3.5-1.9 2 0.6 1.5-1.3 2.2 3.2-2 3.5-2.5 2.6-1.9 0.2 0.3 2.5-2.2 3.2-2.5 3.2 0.2 1.8 3.8 3.6 4 2 2.5 2.2 3.3 3.8 1.6 0 2.6 1.7 0.5 2 4.9 2.1 4.1-2.2 1.8-3.4 1.7-2.9 1.3-3.5 2.6-5.1-0.2-3.1 0.7-1.9-0.1-3.6 1.4-4.9 1.3-1.3-0.6-2.1 1.8-3.4 1.5-3.5 0.4-1.9 2.3-2.4 1.3 3.2-0.1 4 1.3 0.8-0.1 2.7 1.6 3.2-0.1 3.7-0.6 2.3z",AT:"M1070.6 190.8l-0.3 0.8 0.7 2.1-0.2 2.6-2.8 0 1.1 1.4-1.3 4-0.9 1.1-4.4 0.1-2.4 1.5-4.2-0.5-7.3-1.7-1.3-2.1-4.9 1.1-0.5 1.2-3.1-0.9-2.6-0.2-2.3-1.2 0.7-1.5-0.2-1.1 1.4-0.3 2.7 1.7 0.6-1.7 4.4 0.3 3.5-1.1 2.4 0.2 1.7 1.3 0.4-1.1-1-4.1 1.7-0.8 1.6-2.9 3.8 2.1 2.6-2.6 1.7-0.5 4 1.9 2.3-0.3 2.4 1.2z",AW:"M592.9 422l-0.5-0.2-0.5-0.5 0.1-0.6 0.2 0.3 0.4 0.4 0.3 0.5 0 0.1z",IN:"M1427.6 308l-2.8 3-0.9 6 5.8 2.4 5.8 3.1 7.8 3.6 7.7 0.9 3.8 3.2 4.3 0.6 6.9 1.5 4.6-0.1 0.1-2.5-1.5-4.1-0.2-2.7 3.1-1.4 1.5 5.1 0.4 1.2 5.5 2.5 3.2-1 4.7 0.4 4.5-0.2-0.5-3.9-2.6-2.1 4.2-0.8 3.9-4.8 5.4-4 4.9 1.5 3.2-2.7 3.6 4-1.2 2.7 6.1 1 1 2.4-1.7 1.2 1.4 3.9-4.2-1.1-6.2 4.4 0.9 3.7-2 5.4 0.3 3.1-1.6 5.3-4.6-1.5 0.9 6.7-1 2.2 1 2.7-2.5 1.5-4.4-10.2-1.5 0-0.3 4.2-3.6-3.4 1.2-3.6 2.4-0.4 1.6-5.4-3.4-1.1-5.1 0.1-5.4-0.9-1.2-4.5-2.7-0.3-4.9-2.8-1.2 4.4 4.6 3.4-3 2.4-0.9 2.3 3.7 1.7-0.3 3.9 2.6 4.8 1.6 5.3-0.5 2.4-3.8-0.1-6.6 1.3 0.9 4.8-2.4 3.8-7.5 4.4-5.3 7.5-3.8 4.1-5 4.2 0.3 2.9-2.6 1.6-4.8 2.3-2.6 0.3-1.2 4.9 1.9 8.4 0.7 5.3-1.9 6.1 0.7 10.9-2.9 0.3-2.3 4.9 1.9 2.2-5.1 1.8-1.7 4.3-2.2 1.9-5.6-6-3.1-9-2.5-6.5-2.2-3-3.4-6.2-2-8-1.4-4-5.9-8.8-3.5-12.5-2.6-8.2-0.8-7.8-1.7-6-7.7 3.9-4-0.8-8.1-7.8 2.4-2.3-1.9-2.5-7.1-5.5 3.2-4.3 12.1 0-1.8-5.5-3.5-3.2-1.4-5-4-2.8 4.9-6.8 6.5 0.5 4.5-6.7 2.2-6.5 3.9-6.5-1-4.6 3.8-3.7-5.1-3.1-2.9-4.4-3.3-5.6 2-2.8 8.5 1.6 5.7-1 3.8-5.4 7.7 7.6 0.8 5.2 3 3.3 0.6 3.3-4.1-0.9 3.2 7.1 6.2 4 8.6 4.5z",TZ:"M1167 508.4l-0.2 3.9-1.1 4.5 1.6 2.5 2.5-1.5 3.3-0.4 0.7 0.8 3.3-1.6-2.3-2.2 1.9-2.9 2.8-2.9 20.5 13.1 0.3 3.7 8.1 6.4-2.8 8 0.3 3.6 3.5 2.3 0.2 1.7-1.7 3.9 0.3 1.9-0.4 3.1 1.8 4 2.2 6.4 2 1.4-4.6 3.7-6.2 2.5-3.4-0.1-2.1 1.9-3.9 0.2-1.5 0.8-6.7-1.8-2.2 0.2 0.1-0.1-1.8-2.4-0.3-6.8-2.9-3.4-0.4 1.2-1-1.7-5.5-1.2-3.2-1.9-3.6-1.1-2.2-1.1-0.3-0.2-2.7-6.6-0.4-3.9-4.5-4.4 1.4-2.4-1.1-2.6 0.2-2.7-1-0.9 0.3-2.8 0.6-0.1 2-2.3 2.3-3.4 1.4-1.3 0-2.1-1.2-1.5-0.3-2.5 1.6-0.8 0.3-3.8-2.2-3.6 2-0.8 6.2 0.1z",IC:"M888.4 323.4l-0.3 0.5-0.4 0.5-0.3-0.4-0.4 0-0.2-0.2 0.2-0.3 0.4 0.1 0.4-0.4 0.3-0.2 0.2 0.1 0.1 0.3z m13.6-2.3l0 0.5 0.2 0.4-0.2 0.7 0.1 0.3-0.4 0.4-0.5 0.2-0.2 0.2-0.6-0.2-0.5-0.5-0.2-0.4 0-0.6 0.6-0.4 0.1-0.5 0-0.2 0.6 0.1 0.4 0 0.3 0.1 0.3-0.1z m-9.9 0.8l-0.2 0-0.3-0.2-0.2-0.3 0.1-0.4 0.1-0.3 0.3 0 0.3 0 0.5 0.4 0.1 0.3-0.5 0.5-0.2 0z m6.1-3.5l0 0.2-0.6 0.3-0.4 0.5-0.3 0.2 0 0.4-0.4 0.7-0.1 0.4-0.5 0.6-0.1 0.2-0.2 0-0.6 0.2-0.1-0.1-0.1-0.4-0.3-0.4-0.1-0.2-0.2-0.3 0-0.3-0.4-0.6 0.5-0.3 0.3 0.2 0.6-0.2 0.4 0 0.5-0.2 0.5-0.4 0-0.2 0.6-0.3 0.6 0 0.3-0.1 0.1 0.1z m10.2 2.8l-0.3 0.4-0.4 0.1-0.3-0.1-0.4 0 0-0.2 0.3 0 0.6-0.2 0.4-0.3 0.3-0.3 0.1-0.5 0.1-0.3 0.2-0.5 0.3-0.4 0.3-0.6 0.2-0.8 0.2-0.2 0.4-0.1 0.3 0.3 0.1 0.5-0.1 0.5-0.1 0.5 0 0.5-0.1 0.1-0.3 0.7-0.3 0.3-0.6 0.1-0.7 0.3-0.2 0.2z m-19.6-4.5l0.3-0.1 0.2 0.3 0.2 0.5-0.2 0.3 0.1 0.4-0.6 1-0.1-0.1-0.1-0.4-0.4-0.9-0.1-0.3-0.1-0.2 0.2-0.4 0.3-0.2 0.3 0.1z m24.1-2l0 0.4-0.2 0.5-0.7 0.5-0.5 0.1-0.4 0.5-0.5-0.2 0-0.1 0.2-0.4 0-0.4 0.2-0.3 0.3-0.2 0.3 0 0.3-0.3 0.5 0 0.1-0.1 0.2-0.5 0.2-0.1 0.2 0.2-0.2 0.4z",AZ:"M1229 253.2l-3.8-0.9-3.2-2.9-1.2-2.5 1-0.1 1.9 1.7 2.3 0 0.2 1 2.8 3.7z m6.3-17l2.5-2.6 3.5 3.3 3.6 4.6 2.5 0.3 1.9 1.7-4.2 0.5 0.1 5-0.4 2.2-1.7 1.5 0.8 3.1-1.3 0.4-3.9-3.4 1.2-3.1-1.9-1.9-1.9 0.5-5.3 4.7-0.9-4.5-2.5-1-2.6-1.7 1.1-2.1-3.2-2.2 0.7-1.6-2.2-1.1-1.4-1.6 1.1-1.1 4.2 1.9 2.9 0.3 0.6-0.7-3.3-3.5 1.2-0.8 1.5 0.2 4.3 3.8 2.4 0.5 0.6-1.6z",IE:"M956.7 158.2l0.7 4.4-3.9 5.5-8.8 3.6-6.8-0.9 4.3-6.4-2.1-6.2 6.7-4.8 3.7-2.8 0.9 3.2-1.2 3.3 3-0.1 3.5 1.2z",ID:"M1667.5 567.6l-2.4 0.1-7.1-4.5 5.4-1.3 2.8 2 1.8 1.9-0.5 1.8z m24.8-8.7l0.5 1.3-0.1 1.9-4.1 4.8-5 1.4-0.6-0.7 0.7-2.2 2.8-3.9 5.8-2.6z m-39.6-5.1l1.9 1.7 3.6-0.5 1.2 2.7-6.7 1.3-3.9 0.9-3.1-0.1 2.2-3.7 3.2 0 1.6-2.3z m28.3 0l-1.1 3.6-8.6 1.8-7.5-0.8 0.2-2.4 4.6-1.3 3.4 1.9 3.8-0.5 5.2-2.3z m-80.2-8.5l10.8 0.7 1.4-2.7 10.3 3.1 1.8 4.2 8.4 1.2 6.7 3.8-6.6 2.4-6.1-2.6-5.1 0.2-5.8-0.5-5.2-1.1-6.4-2.5-4.1-0.6-2.4 0.8-10.2-2.7-0.8-2.7-5.1-0.5 4.2-6.1 6.8 0.4 4.4 2.5 2.4 0.5 0.6 2.2z m147.9-3.6l-3.2 4.4-0.2-4.8 1.1-2.3 1.3-2.2 1.2 1.9-0.2 3z m-41.4-17.7l-2.2 2.2-3.8-1.2-1-2.8 5.7-0.3 1.3 2.1z m18.4-2.3l1.8 4.9-4.6-2.7-4.7-0.5-3.3 0.4-3.9-0.2 1.5-3.5 7-0.3 6.2 1.9z m59.8-3.2l-1 20.9-1.6 21-4.6-5.3-5.6-1.3-1.5 1.8-7.2 0.2 2.8-5.2 3.7-1.8-1-7-2.3-5.3-10.7-5.5-4.6-0.5-8.3-6-1.8 3.2-2.2 0.5-1.1-2.3 0.1-2.8-4.2-3.2 6.2-2.3 4 0.1-0.4-1.7-8.3 0-2.2-3.8-5-1.2-2.3-3.2 7.6-1.5 2.9-2.1 9.1 2.6 0.9 2.4 1.3 10.4 5.7 3.8 5-6.8 6.6-3.8 5 0 4.8 2.2 4.1 2.3 6.1 1.2z m-89.1-25.8l-4.5 6.4-4.3 1.2-5.4-1.2-9.5 0.3-4.9 0.9-0.8 4.9 5 5.7 3.1-2.9 10.6-2.2-0.5 2.9-2.5-0.9-2.5 3.8-5.1 2.5 5.1 8.2-1.1 2.2 4.8 7.4-0.3 4.2-3.1 1.9-2.1-2.3 3-5.2-5.7 2.5-1.3-1.8 0.8-2.5-3.9-3.8 0.7-6.2-3.9 1.9 0.2 7.5-0.2 9.2-3.7 0.9-2.3-1.8 1.9-5.9-0.6-6.2-2.4-0.1-1.6-4.4 2.5-4.2 0.9-5.1 3-9.7 1.1-2.6 4.8-4.8 4.5 1.9 7.1 0.9 6.5-0.3 5.6-4.6 1 1.4z m19.6 1.9l-0.3 5.6-2.9-0.7-0.9 3.9 2.3 3.4-1.6 0.8-2.2-4.1-1.7-8.2 1-5.1 1.8-2.3 0.5 3.5 3.4 0.5 0.6 2.7z m-108-5.7l1 4.3 3.9 3.7 3.7-1.3 3.6 0.4 3.3-3.2 2.7-0.6 5.4 1.8 4.6-1.4 2.6-8.9 2.1-2.2 1.7-7.3 6.5 0 5 1.1-3 5.8 4.4 6-0.9 3 6.4 5.9-6.7 0.8-1.8 4.4 0.2 5.8-5.5 4.4-0.4 6.4-2.5 9.8-0.7-2.3-6.6 2.9-2.1-3.9-4-0.4-2.8-2.1-6.8 2.4-1.9-3.2-3.8 0.4-4.6-0.7-0.6-8.6-2.8-1.8-2.7-5.5-0.8-5.6 0.6-6 3.3-4.3z m-22.8 50.5l-6.2 0.1-4.5-5.3-7.1-5.3-2.3-3.9-4.1-5.2-2.7-4.8-4.2-9-4.9-5.4-1.7-5.5-2.2-5-5.2-4-3.1-5.5-4.4-3.6-6.2-7.1-0.6-3.3 3.6 0.3 8.9 1.2 5.2 6.3 4.6 4.4 3.2 2.6 5.5 6.9 5.8 0.1 4.8 4.4 3.4 5.4 4.3 3-2.3 5.2 3.3 2.2 2 0.2 0.9 4.5 1.9 3.5 4.1 0.6 2.6 4.1-1.7 8-0.7 9.9z",UA:"M1157.2 174.6l2.3 2.7 0.1 1.2 6.7 2.2 3.6-1 3.6 2.9 2.9-0.1 7.7 2 0.4 1.9-1.3 3.2 1.8 3.5-0.3 2.1-4.8 0.4-2.2 1.8 0.4 2.7-3.9 0.5-3 2.1-4.6 0.3-4 2.4 1 3.9 2.8 1.5 5.1-0.4-0.6 2.3-5.4 1.1-6.3 3.6-3.1-1.3 0.7-2.9-5.9-1.9 0.7-1.2 4.6-2.1-1.7-1.4-8.1-1.6-0.8-2.4-4.5 0.8-1.3 3.5-3.3 4.6-2.4-1.1-2.3 1.1-2.4-1.2 1.2-0.7 0.6-2.1 1.1-2.1-0.6-1.1 1-0.5 0.6 0.9 3 0.2 1.3-0.5-1-0.6 0.2-1-2-1.6-1.1-2.6-2-1 0.1-2.1-2.6-1.7-2-0.3-4-1.9-3.2 0.6-1.1 0.9-2.1 0-1 1.5-3.6 0.6-1.6 1-2.5-1.6-3.2 0-3.2-0.7-2 1.4-0.5-1.7-3-1.7 0.7-2.5 1.2-1.7 1.1 0.4-1.6-2.8 3.8-5.2 2.3-0.7 0.3-1.7-3.2-5.4 2.3-0.3 2.4-1.6 3.8-0.2 4.9 0.5 5.7 1.5 3.9 0.1 1.9 0.9 1.7-1.1 1.5 1.5 4.3-0.3 2.1 0.6-0.3-3.1 1.3-1.4 4.1-0.3 1.8 0.2 1-1.4 1.5 0.3 4.9-0.6 3.8 3.5-0.9 1.3 0.8 1.9 3.9 0.3z",QA:"M1270.1 343.7l-1.5 0.5-1.8-1.3-0.8-4.7 1.1-3.3 1.5-0.7 1.8 2 0.5 3.7-0.8 3.8z",MZ:"m 1166.7,673.5 h -4.1 l -0.3,-2.9 -0.6,-2.9 -0.4,-2.3 1.4,-7.1 -1.1,-4.6 -2.2,-9 6.2,-7.3 1.7,-4.6 0.8,-0.6 0.9,-3.8 -0.8,-1.9 0.4,-4.8 1.3,-4.4 0.4,-8.2 -2.8,-2 -2.7,-0.5 -1.1,-1.6 -2.6,-1.3 -4.7,0.1 -0.2,-2.4 -0.4,-4.6 17.2,-5.3 3.2,3.1 1.5,-0.6 2.2,1.6 0.2,2.6 -1.3,3 0.2,4.5 3.5,4 1.9,-4.5 2.5,-1.3 -0.1,-8.3 -2.2,-4.6 -1.9,-2.1 h -0.4 l -0.6,-7.3 1.5,-6.1 2.2,-0.2 6.7,1.8 1.5,-0.8 3.9,-0.2 2.1,-1.9 3.4,0.1 6.2,-2.5 4.6,-3.7 0.9,2.8 -0.5,6.4 0.5,5.7 -0.2,10 0.8,3.1 -1.9,4.6 -2.4,4.5 -3.7,4 -5.3,2.4 -6.5,3.1 -6.6,6.9 -2.2,1.2 -4.2,4.6 -2.3,1.4 -0.8,4.6 2.4,4.9 0.9,3.7 v 2 l 1,-0.4 -0.5,6.3 -1.1,3 1.2,1.1 -1,2.7 -2.4,2.3 -4.7,2.1 -6.9,3.5 -2.5,2.4 0.3,2.7 1.3,0.4 z"},names:{BD:"Bangladesh",BE:"Belgium",BF:"Burkina Faso",BG:"Bulgaria",BA:"Bosnia and Herzegovina",BB:"Barbados",BL:"Saint-Barthélemy",BM:"Bermuda",BN:"Brunei",BO:"Bolivia",JP:"Japan",BI:"Burundi",BJ:"Benin",BT:"Bhutan",JM:"Jamaica",BW:"Botswana",WS:"Samoa",BR:"Brazil",BS:"Bahamas",BY:"Belarus",BZ:"Belize",TN:"Tunisia",RW:"Rwanda",RS:"Serbia",TL:"Timor-Leste",RE:"Reunion (France)",TM:"Turkmenistan",TJ:"Tajikistan",RO:"Romania",GW:"Guinea-Bissau",GU:"Guam",GT:"Guatemala",GR:"Greece",GQ:"Equatorial Guinea",GP:"Guadeloupe (France)",BH:"Bahrain",GY:"Guyana",GF:"France",GE:"Georgia",GD:"Grenada",GB:"United Kingdom",GA:"Gabon",GN:"Guinea",GM:"The Gambia",GL:"Greenland",GH:"Ghana",OM:"Oman",JO:"Jordan",HR:"Croatia",HT:"Haiti",HU:"Hungary",HN:"Honduras",PR:"Puerto Rico",PS:"Palestine",PW:"Palau",PT:"Portugal",KN:"Saint Kitts and Nevis",PY:"Paraguay",AI:"Anguilla",PA:"Panama",PF:"French Polynesia",PG:"Papua New Guinea",PE:"Peru",PK:"Pakistan",PH:"Philippines",PL:"Poland",ZM:"Zambia",BQSE:"St. Eustatius (Netherlands)",EH:"Western Sahara",RU:"Russian Federation",EE:"Estonia",EG:"Egypt",ZA:"South Africa",EC:"Ecuador",IT:"Italy",VN:"Vietnam",SB:"Solomon Islands",ET:"Ethiopia",SO:"Somalia",ZW:"Zimbabwe",KY:"Cayman Islands",ES:"Spain",ER:"Eritrea",ME:"Montenegro",MD:"Moldova",MG:"Madagascar",MF:"Saint Martin (French)",MA:"Morocco",UZ:"Uzbekistan",MM:"Myanmar",ML:"Mali",MN:"Mongolia",MH:"Marshall Islands",MK:"Macedonia",MU:"Mauritius",MT:"Malta",MW:"Malawi",MV:"Maldives",MQ:"Martinique (France)",MP:"Northern Mariana Islands",MS:"Montserrat",MR:"Mauritania",UG:"Uganda",MY:"Malaysia",MX:"Mexico",IL:"Israel",FR:"France",FI:"Finland",FJ:"Fiji",FK:"Falkland Islands",FM:"Micronesia",FO:"Faeroe Islands",NI:"Nicaragua",NL:"Netherlands",NO:"Norway",NA:"Namibia",VU:"Vanuatu",NC:"New Caledonia",NE:"Niger",NG:"Nigeria",NZ:"New Zealand",BQBO:"Netherlands",NP:"Nepal",NR:"Nauru",XK:"Kosovo",CI:"Côte d'Ivoire",CH:"Switzerland",CO:"Colombia",CN:"China",CM:"Cameroon",CL:"Chile",CA:"Canada",CG:"Republic of Congo",CF:"Central African Republic",CD:"Democratic Republic of the Congo",CZ:"Czech Republic",CY:"Cyprus",CR:"Costa Rica",CW:"Curaco (Netherlands)",CV:"Cape Verde",CU:"Cuba",SZ:"Swaziland",SY:"Syria",SX:"Saint Martin (Dutch)",KG:"Kyrgyzstan",KE:"Kenya",SS:"South Sudan",SR:"Suriname",KH:"Cambodia",SV:"El Salvador",KM:"Comoros",ST:"São Tomé and Principe",SK:"Slovakia",KR:"South Korea",SI:"Slovenia",KP:"North Korea",KW:"Kuwait",SN:"Senegal",SL:"Sierra Leone",SC:"Seychelles",KZ:"Kazakhstan",SA:"Saudi Arabia",SG:"Singapore",SE:"Sweden",SD:"Sudan",DO:"Dominican Republic",DM:"Dominica",DJ:"Djibouti",DK:"Denmark",VG:"British Virgin Islands",DE:"Germany",YE:"Yemen",DZ:"Algeria",US:"United States",UY:"Uruguay",YT:"Mayotte (France)",LB:"Lebanon",LC:"Saint Lucia",LA:"Laos",TV:"Tuvalu",TW:"Taiwan",TT:"Trinidad and Tobago",TR:"Turkey",LK:"Sri Lanka",LV:"Latvia",TO:"Tonga",LT:"Lithuania",LU:"Luxembourg",LR:"Liberia",LS:"Lesotho",TH:"Thailand",TG:"Togo",TD:"Chad",TC:"Turks and Caicos Islands",BQSA:"Saba (Netherlands)",LY:"Libya",VC:"Saint Vincent and the Grenadines",AE:"United Arab Emirates",VE:"Venezuela",AG:"Antigua and Barbuda",AF:"Afghanistan",IQ:"Iraq",VI:"United States Virgin Islands",IS:"Iceland",IR:"Iran",AM:"Armenia",AL:"Albania",AO:"Angola",AS:"American Samoa",AR:"Argentina",AU:"Australia",AT:"Austria",AW:"Aruba",IN:"India",TZ:"Tanzania",IC:"Canary Islands (Spain)",AZ:"Azerbaijan",IE:"Ireland",ID:"Indonesia",UA:"Ukraine",QA:"Qatar",MZ:"Mozambique"},default_regions:{"0":{name:"North America",states:["MX","CA","US","GL","BM"]},"1":{name:"South America",states:["EC","AR","VE","BR","CO","BO","PE","BZ","CL","CR","CU","DO","SV","GT","GY","GF","HN","NI","PA","PY","PR","SR","UY","JM","HT","BS","FK","AI","AG","AW","BB","VG","KY","DM","MQ","LC","VC","GD","GP","MS","TC","SX","MF","KN","CW"]},"2":{name:"Europe",states:["IT","NL","NO","DK","IE","GB","RO","DE","FR","AL","AM","AT","BY","BE","LU","BG","CZ","EE","GE","GR","HU","IS","LV","LT","MD","PL","PT","RS","SI","HR","BA","ME","MK","SK","ES","FI","SE","CH","TR","CY","UA","XK","MT","FO"]},"3":{name:"Africa and the Middle East",states:["QA","BH","SA","AE","SY","OM","KW","PK","AZ","AF","IR","IQ","IL","PS","JO","LB","YE","TJ","TM","UZ","KG","NE","AO","EG","TN","GA","DZ","LY","CG","GQ","BJ","BW","BF","BI","CM","CF","TD","CI","CD","DJ","ET","GM","GH","GN","GW","KE","LS","LR","MG","MW","ML","MA","MR","MZ","NA","NG","ER","RW","SN","SL","SO","ZA","SD","SS","SZ","TZ","TG","UG","EH","ZM","ZW","RE","KM","SC","MU","CV","IC","ST","YT"]},"4":{name:"South Asia",states:["SG","TW","IN","AU","MY","NP","NZ","TH","BN","JP","VN","LK","SB","FJ","BD","BT","KH","LA","MM","KP","PG","PH","KR","ID","CN","MV","NC","VU","NR"]},"5":{name:"North Asia",states:["MN","RU","KZ"]}},proj:"robinson",proj_coordinates:[{lat:48.86666667,x:997.9,lng:2.333333333,y:189.1},{lat:-34.615,x:673.5,lng:-58.446,y:724.1},{lat:-33.874,x:1798.2,lng:151.203,y:719.3}],initial_view:{y:0.99,x:0.99,x2:2018.99,y2:864.5600000000001}}

//Map logic - do not edit
eval((function(x){var d="";var p=0;while(p<x.length){if(x.charAt(p)!="`")d+=x.charAt(p++);else{var l=x.charCodeAt(p+3)-28;if(l>4)d+=d.substr(d.length-x.charCodeAt(p+1)*96-x.charCodeAt(p+2)+3104-l,l);else d+="`";p+=4}}return d})("(function (plugin_name) {` .') ` \"(funcName, baseObj) {` ,$ = ` \"%|| \"docReady\";` E# =` N$ || window;var readyList = []` )&Fired = false` -&EventHandlersInstall` ='`!e%` H!() {if (!` h&) {` r)true;for (var i = 0; i <`!N&.length; i++` V$List[i].fn.call(`\"*\",` K&[i].ctx);}`\"1+}}`!U*StateChange`!f$document.` 7& === \"complete\"`!?$();}}`#P#[`#x$]`$%#`$O\"callback, context` ~#`\"e)setTimeout`%/*` U$(` T$;}, 1);return;} else`\"h'.push({fn:`!,'tx:` X#});}`!{B || !` >%attach`%5! && ` F5interactiv`\"~!`\"+'` E!`!v!`!m#`%L&`%{2`#{,add` D!Listener) {` #5(\"DOMContentLoaded\"`%S#,`' \");`%j\"` G/loa` <.`#`$`\"]0(\"on` I!statec`&$!` Y$`&1'`!)&` M*`!$(`'%$`(j5`(G!}};})(`*7&,`*&#);`%q'console, Object, Array`#N#typeof ` =#`$U\"undefined`%6!` 4*.log` 6,) {` W% {};` >)`'t') {}`&O\"` m#`!K\".create !== \"` F$\") {` 1*` ^(o`-'#` )!F` t!F.prototype = o`(0# new F;`!0#!`\"U!` ?&.forEach) {` #3`!))fn, scope) {`,8*, len = this`,9& < len; ++i) {`,7$` ]!,` F![i], i` &\")`$h!})`,X#.`$M%` )#`$U$` (#`$^\";})()`.k!helper =`04\"`#g%` $%delete_element(e) {e.parentNode.removeChild(e);`-O&clear_sets(arr`\"I-`\"A\"arr`.k+var set = arr[i];set`#M$`!X'`!B\"`!7\"();})` C!splice(0, se`/k$)`/ *placeAll(str, find,` .$`.l!`%8!str.` .#(new RegExp(` H\"\"g\")` J&`\"G'to_float(str`\"##num = parseF` 2%;if (isNaN(num)`!-&`2X\"`.g&`&c\"um`!v(`+f$(obj,`(!, fn`)N#obj`*p() {obj[\"e\" +` F! + fn`0v!n;obj[` &*`%4(` D0`&:$e` w!;};`! +(\"on` J$, ` r*`-6&` P!`-c,`\" $`-d&`\"H&linePath(startX, ` #!Y, endX, endY`%p$tart = {x:` D$y` $\"Y}`'h!end` 9\"` U\"y:endY}`*K$\"M\" +` _\".x + \" ` &&y` +!L\" + end` 4'end.y`'j)one(src`.O!n`&1!`,O'` .' != \"o`)Y!`-k!` /(=== null`&h'` 4&;}`&6!ew` E&` 3(`*]!tructor();`),'i` Z)) {` Y'[i] =`!l.[i]`0m!`-L$`!6*isMobi`/9\"Android:`%z)` Q$avigator.userAgent.match(/` P#/i);}, BlackBerry` 8L` P&` d#iOS` 4LiPhone|iPad|iPo`!M$Opera` <L` P!\\sMini` `#W`(Y!s` 9LIE`#^\"` _#an`\"e2`$#$.`#J#() ||` ,&`\"u&` *+iOS` #+`!u!` %+`!u#(`1(!`!6%isF` $#`.q%ToCheck`)J#getT`3R\"{`(}%` ;+ &&` ?$.toString`2J\"` b-=== \"[`(L\" `!2$]\"`)2'findPos(obj`1x(getStyle`-n\"styleProp`-j'curren` @\"`!u#y = ` +,[` S%]`,h$if `-P$getComputed` Y,` +3`!K\"`*D!` n)`#$#`+L(scrollDist(` q#html = document.getE`4@\"sByTagName(\"html\")[0]`11!html.` e\"Top &&` U&` `$` ]#` <&`&:&[` T'Left,`!?!` @&`\"h)` |+||` NY +` ?<`!=0` 4>`!f,` F%body`!D1` .0`!6\"` E3`$f&[0, 0]`-u\"curtop`1Q!curleft =` .# = 0, scr`&\\\", fixed =`2v\";while ((` ;\"scr.parentNode) &&` U!!`%N'body) {` $-` O\"`#Q'|| 0;`!5#` 1)`$R#0`%w!`(]%scr, \"position\") == \"`!_!\") {`!e$true;}}if (` -\"&& !`(!#opera`4M$crDist`\"%\"`'Z%;`!f$+` 3!Dist[0]`!]$` *'1];}do`\"4&+`)p\"offsetLeft` I'` /&Top;} `#C#obj`*E#` 7\"P`#J!)`,I$[` t#,`$8#]`)K'di`2i\"(xy0, xy1`\".#x0 = xy0.x`$}!y` '$y` *!x1` *!1` 6$` '$` 8\"dx = x1 - x0` +\"y = y1 - y0`!M$Math.sqrt(dy * dy + dx * dx)`!U'rotate(point, transform`!^$ = ` 6![0]`!D\"` (%1` +\"str = Raphael.` W%Path(\"M\" + x + \",\" + y` u(`/C%()` d!re = /M(-?\\d+.?\\d*),(` \"'/` A!m = re.exec(str`#y&m[1], m[2]`#r(bbox_union(arr`\"-$a = [`!y\"x2` \"'y2` /'y` %#for (var i = 0; i < arr.length; i++` q#bb = arr[i];xa.push(bb.x);x2` $'2);y` %&y);y` 3'y2)`*X\"x = helper.min(xa`# \"x2` -'ax(x2` 2#y` B*y` /$` A+y2a`#+%{x:x, x2:x2, y:y, y2:y2, width:x2 - x, height:y2 - y}`#R'mi`#Q!ay`-2&`&P!min.apply(Math, ` >\"` M(ax` C2ax` ;:`''\"_bbox(bbox`&.`$P!bbox.x,`%9!.y`$i\"b` /&2` ,*c` A.2` 4\"d` A/` 4#a2 =`(T$a`!;(`!'\"` 3'b` --c` 3'c` --d` 3'd` --x_`\"p! = [a2[0], b2` \"!c` (\"d2[0]`':#` >)1` E\"1` E\"1` E\"1` D#x_min =`$m!` {#`!'$max =`$M!` ,)y` G'`!\"#` 0%` I%` 0%`&F'_min`&I!` #\"x2:` !`&V\"` $\"`&X#` d!-`!V\"`&\\&` /#` Z!`&a(x_in`!)\"(x, `0S$i = a`)c$`/F#i--) {if (a[i] === x`&_&`1N#` '#`3J\"` &${min:`!I!max:`!g!addEvent:a` \"#, isMobile:i` \"#, linePath:l` \"#, clone:clone, isF`\"!#:i` \"%, findPos:f` \"\", replaceAll:r` \"%,`(+(:`(8'` 1$` -#`'<\"`-H\":`-O&,`1Z%:`1d$,`#C':`#O&, clear_sets:c` \"%, delete_element:d` \"), to_float:t` \"#};})`/o#mapdata = window[plugin_name + \"_` :#\"] ? ` \"=:`$J#` s#info` ]9info` [;` <#` k+` =!= ` E'.subs`2*\"0,` ,)`&X\" - 3).`$O#(\"simplemaps_\", \"\"`*U#emo =`&^\"`+C!randed =`!3'autoload`*4&`)~\"hooks_object = {over_s`%;!` X!, ` -!region` &)loca`&W!` -$ut` J+ut` H,ut` F-click` K*` -\"` M*` .\"` K.ose_popup` +$zoomable_` f/` -+` s+omple` C&refresh_` (,zooming` '-back`!`*xy` *\"}`#e!`$v#`#l!`#[+[]`#Y*` '%`\"G%` -!`#X%` &$` C(`#O([]`#J*` &&` G'`#C+` -\"`#E&[]`#53` &/` j(`\"n$[]`#0/` T$`#3)[]`#6#`!L&xy:[]`#1\"api`&t'`)n#:`)v#,`)h$:`)-#, load:load,`'V\":helper.`-o!(`'c()`)(%` 6/` .(), copy:`0M%(`0A#new_` C\"`!P(` [)this.` 3#)`!e&` /1info)`!%n` z!copy`\"r'};`*c*.push(`\"&&`3t%` )&;}, cre`(j!`\"CB`.Y?`!p)` 0<)`.($`#>&`.b?` \\C` J\"` x'`#%~`#NUmobile_device`!2$isM` 2!.any() ?`0+!`\"'$};`$K%load`$P$`(p)this`3O+` 6&`'<$`2v+` 3*info;if (!` [$|| !`'R$ {console.log(\"The`!!%o` j&`!H#is missing or corrupted.\"`\"k$;}`1l/`!D'` 5!`.E0` 9'` .(`34\"ack_image, ` \"!s_directory, di` \"%`-F!_specific, main_settings, normalizing_factor`#v&pre`#{$` X* =`\"h$.` +*;` q)` <'` +)`!|!scripts = document.getElementsByTagName(\"` C\"\")`$j\"ysrc =` V$[` _#.length - 1].src;`\"u&`!@!`!2'.` 0'!= \"no\" ?` )6`&o#;`#T,` f-` 0-!= \"default` p.` =-`! $` u(` 4-?`$z- :`\"m\".substring(0,` ,#lastIndexOf(\"/worldmap.js\") + 1) + \"map`\"C\"s/\"`'\"\"ignore_pos, rotat`,_!nual_zoom, responsive, div, initi` 6%` \"(_solo, tooltip_` a\", last`4*\"ed` 4&up, `49\"s`&Y&get_`!_!nfo() {div`#i-` /!== undefined ?`\"4!\" :` 8.;`!T(`$S.` 0)` e+-1` `-` D(` m)`\"U!` b9` ;#= \"yes\" &&`#-) != -1`-L+;`#q&` j-width` j!` ;&\"` O-`$k!` K-` 0#`&t,` 0#`'y%f (` Y$= \"0\") {` h%` =\"}zooming_on` t-`#L#`))$` L! :`![!;`&5'` G-` /)`#&$`\"g-`%r!`+j\"info.`)2#_` 2$&& `!Q'?` /5`\"A(`,T$` 5#) {` {)` 0(;}` E(labels) {` #\"` D'` +\";}`($*`#7%`(-(` ,%`(6&` *%`)Z&`#q&var`\">!_time,` %\"`3x\"` &#incr`.8!, custom_shapes, popup_centered` ($orientation, order_number` i#percent`1<\"`'4#back, link_text` D\"`#o\", fade`!W#hide_eastern_`\"o\", `\"w\",`+z$`$3$`#-#var adjacent_opacity` 0!op` \"&`\";%al` +!` W!_size` $'color` %'` U(new_tab` '!`!8%oc`\"a!` <)`3'`\"v\"` |%`#G#` |%` *\"max`)^!` ('` c(` ,\"shadow` Y)rner`!\"\"` ,\"nocs` $(font`-a*refreshable`-n%`!$#`2W1ground_transparent`(|(0 : 1;`#>&` R-` 0'`+1,` 0': 22` _#`\"u!` P3` 6\"` R2` 6\": \"#ffffff\";`$F#` [-url_` 3%`*|3`$e4`!G.` 1,`!M-` 1,: 1;`%Z!` [-js_` 2#`!?3`&!'`$:.` 1'`!4,` 0(: 1.5;`&['` Y-` 0(` Z,` 0(`#`(`*c-` _3` 6(` f2` 6(: \"auto` w$`+{$`!Z4` 7$`!^3` 7$` g+`$b4` 0*`!V3`$k%0.9` i#`)`\"` X3` 6#>`4X\"` '7: 1`$<%`*@!`$.5` 8\"`$14` 8\": `%+$`*|!` U3` 5#`&F3`,;*` L3` 6%`!:2` 6%` g*font` Z3` 6!` W2` 6!: \"12px/1.5 Verdana, Arial, Helvetica, sans-serif\";`/{'`!$-`1v!out`2e&ally`\"o!no\" ?`!q\" :`\"v!;`1!,` ^-` 0-`!z,` 0-: 0.3;`4A%`!K2` 5!` [,` 0&: 0.5` ^\"`!~%` S2` 5&` Y1` 5&: 2` k\"mobil`!@3` 4$`\"w2fade`!~2` 0&`!6,` 0&* 1000 : 200`0D\"`'@\"pdata`/}\"s;custom_shape`'S.` 0*` {,` 0*: {};initial_back` ]-` 0)&&` \"8!`\"h%` *7`(*$hide_eastern_`\">'` M(` /1`)`3link_tex`%0.` 0&`1c-` 1%: \"View Website\";back_imag`1//` 1&`\":4` 8'`!X$`1U\"numbe`19.` 0)` ],` 0)` g$`&d!percent`!Q0` 0,`'c1` 5'`/U!9;}func`1O!is_onclick(`,\"!s) {if ` &#`$\"!on_` <!\") {return`'|\"} else ` ?+detect\" && touch` ?2` ,$`\"@\"}`!B*ff`!26ff`!03` b,var vml;var tough` %!ie` ,!ios` #!`\"3$` (!`2]#ff =` _#var reload` &)`\"3!` I&s;`!u%get_client_info() {vml = Raphael.typ`+L\"VML`'K-ie = document.all` 0-os = helper.isM`,<!.iOS()` ;,`!_!` :/any` ;.`#Y$`1F._up`3x1` 3!:`1t0s;`#M$`#+%`#M(`$r*;`$B!map_outer`#O!map_inn` $&div` 0%hold` ='zoom`#t&create_dom_structure() {` ^\"`#P(getEl`0&!ById(div);` u&` ,: + \"` B#\") ?` Y8` =*`$]%f (`!#&`!W%.removeChild` 4(`&M\"t_to_del`!A7\"tt_sm_\" + `\",!if (` O%) {` #%.parentNode`!()` C&;}}`\"Y2`#]\"`!/#(\"div\"`#4\"`$[!` %Azoom` $A`%@!` sF.id = `#n$` .!\"`!(%` /*zoom` 6\"` !` /*` .!` 8\"`\"Z\"` 1*`$h#` 4(style.posi`&b!= \"relative` y(` &<`!\"` 4.absolut` 3/left = \"3px`\"?'` B<` :'top = \"40` N/` a8zIndex = \"1`!D.` -,div.append`'I.`#:'` 3,zoom` $9`!/!` %9`#s!`+]#transform_rotat`/C\"widt`/C\"height` &!scal` 9\"original_` >&` *%` F'initial_view` ,!normalizing_facto`,K\"ratio`!.&_to` ^$`,Q-imensions(res` d!`*}&`#`\"` ^! = \"`&W/` 1'if (responsive) {` 4$` f#offsetW`\";!if (` 9\"< 1` 9.`+*'` J(`*~'`!0+` \"\"+ \"px\"`4=%` k&`0N(` /#== undefined ? 800`0s-`$%\"`\"V1`!\"*` (** 1`.P$info.calibrate) {`$T( = {};` &(.x = -1 * ` K-.x_adjust` C*y` 76y` <1x2 =`%y).x +` K/`\"V\"` ~*2 = (` ^,-` _+) /` \\/`&j\"`$1$`\"W+` G$`'P)}`($*`!e-`!(.;`(B+` C,y` B-y;`(/+ =`):+ /`)6-` A%` 9$`(r,`)A.` c01000`%b!!`),'`*k!` p'`*g+`#A! =`&9#`+W\") {var bbox_array = [];for (`+-! in`#Y%state_` B&` T%`#{'` 4,[i];` $&.push(bb`-$#path` G! = helper.` G!union(` ~'`,'!center_x = 0.5 * (` W%.x2 +` e&.x) *`-T'` S#y` F0y` L*y` P&`.Y,`2d!\" + ` (#+ \",\" +`!N&` \"+y`.&\"iv`\"4&` T\"`!%!`'~),`/e-)`&f&`':$riv`(r#`&y.riv.`%z#}` a&`%I$\"s\" +`\"0\"`!\\%` $&0,0\"`\"C& =`\"8$?`!J'` F$`\"c-:` 5,;}`$t#pe`0g\"everything` *!all_lines` %%visible_`%v!` 1\"location_label` A&external_border` R.` E'`!O%` @!`!V,`'4!ackground` _%pil`!$'`!G'all_region` &&`!^$` ''`!4'op` '$`3%,canvas() {`#!! = Raphael(map_inner,`)\", `$o\");`!b& =`#W\".rect`.\"+ -`*{,* 2,`,H+` 9(`%z#` A!` I-5` )'` @%5`!@(.`#N%(`#R+` 9)hide();`#I&`!{%set` 2#`%8*` ./`#w#` '/`#%` (0`#~!` *+`&0+`!37` ,1`$q&` G/`&s,` 0/`&@!`!X0in`\"I-`(T&` B+` /&`./\"`#S&,`&o*,`']'` 4#`!w!` &\"`!_,`.s#trial_`){&map_` /! = false`'D-` L\"text() {`1V!demo) {return;}if (`!N$.hostname.match(\"simplemaps.com\")) {demo`!&%` Y(`!D%`1:#parent`1?\"` 3\".` -\"Node;` %\".removeChild` V';`\"'.}` '(document.`\"@\"Element(\"div\"` R'.style.cssText = \"display:inline !important\"` E-posi`#E!= \"absolute\"`4?!branded`\"@#h = 20`$*!w = 140;} else` 5&3` 5&200`\" '`!$#left =`+V\" - w + \"px`!A.top`1p!`*O!- h` 04zIndex = \"1` 7\"`,X!.append`#[-`&*'`-%+` 5!, w, h)`\"G/t`#O\"` U'.`&D!w - 5, h * 0.5, \"S`%w+;text.attr({'text-anchor':\"end\", 'font-size':14` ($w`\"T!':\"bold\", cursor:\"pointer` O%family':\"a`!{!sans-serif\", title:\"Built with `!W\"Maps\"})`$N)`!iS Trial`!vL8`!\\f});}`!7!node.setAttribute(\"href\", \"http://`*0,`!n\"click(`+B%() {window.`*&ref =` P4;}`,J#`#1!_back`'}!back_arrow`,8-` 6!button() {` @&`.2+`(a$35` &!`(z!5`&w\"ack_image`&y#` '!`.I% = directory +`!E\"` =!` c!img = new I` /!img.onload = `\"\\* = img.`)i!;h` '#`)O\";make`!q\"();}` a!src` ?!`!@(`&>%` B*` %` +( {`#;&`)Q+outer`)S)`\"S,`#=&`#?*` i!.` E!(`!M*, 0, 0` m$` P,`'H#`&d,}` 7/reg_num = -1` 3'`2q\"` >,` T(`&X#` ,!` &!_handler`)P*` X\"path = \"m503.7,743.8c190.3-96.6 132.9-417.05-155.6-409.71v-128.7l-228.1,195.0 ` &\"205.8v-131.6c240.9-5.5 229.9,202.8 ` e!,269.3z\"`%|!`!I\"color`2N!in_settings.` 0(? ` \"6: \"#cecece` c-_bord`.|!` F5` ;$` g7` ;$: \"#808080` }(size = 0.0`(v\"`$:'box`%m*rect(`%_'`%R#fill:\"black\", opacity:0`,T.}`*$\"`&b:path(`$~&` }$stroke:`\"&., '` 6\"-`)X!':2` (&`!E#':1, `!a!` T', 'fill` 7)`!_..scale`!I#size,`#-', -2, -6`'_F_box`'jW` 2,box);}if (!initial`#?!`-~).hide();}}`.x!zoom_in`$$!` '!out` #&`/,%` )!ab` 0)in`)U\"` O)` ,#`/J,` h\"`/U!s(`,1#`/0.`!+%`,u+zoom`&?!80`%m\"`!8$`*Q% 64,13.787 0,100.426 m -50.213,` \"\"2001 ` 6#,`'o#` b$`'W\"`!8%`'>:`)Y$`'S'.7`420`&#\"gray\"`&i-1`'z#`!<$v`1T!`!:)`'y!`\"B(`'q+` c33`'Y7`)Q%`'EG0.3, 0.3`'[#2)`$E\"in`4*+` /#`&s\"`#P',`\"5+`\"M'out`$\\'`$^\",64.000999`$:1out`$754`#O~`!;'`$59`\"2$`#Q~`$1>55`$6#out`$@0out`$J'`#Z#`$O#`\"?&)`*.&mov`*0\"ing_dimensions(dire` =!`*:'last_destination.sm.` I..w / ` V%`*n%` 6Ch` L-x` 3Fx + (`!=E- w) / 2`!!!y` \\Fy` [Gh - h` |&r = w / (original_`%q! * `%(!);return {x:x, y:y, w:w, h:h, r:r};}`$Q%`!&$allowed`#dj` =$`&A\"` 3% < 1 ? true : false;if (`1^$zoom != -1 &&`\"o2type == \"manual\" || ` S(_solo)`!}#` 1$`# \"= region_array[` J(]`\"\"4`\")!outside_` K#`$ !>` n+- 1`\"&!`\"N(&&` K,`!O%` a+is_` .\"`\"C-,`!Z7)`#6#` `)`\"[1 {` '!to(` R8`%w#`$2\"}}}`!}/w >`!C*-1`##5 - 1) {if (!`!*E-1]);}`!<*` '#true`'2+_about`'/)`!*!`'C6) {` i\";}var `#_' = {sm:{type:`&7$, zp:1}}`$s%_tween) {`$9,` A:`(DB = current_viewbox` D1b`2P\"{x:` ?+.x /`*e\", y` ,-y` 3&`'/!` 0-w` 7&height` 1-h` :$}`#9\"new`!q*`/n>`#E!!` K*`$*'`\"`@` O*`1t\"to(` P');}` 1!in_click = `%o%(`&S$`%v\"` A#crement);}`2a%` :=1 /`&`\"` M.in.` Y!` w$` f\"`3['` 3'`!\"%` 8#in.touchend` F5` 3*` W'`$2!cattr, la` \"!r` (\"`) #map, label_attributes, loc`#5!s, set_state` %\"` G!;`\"[%set` N'() {` R% = mapdata.` +%;`!G! = [];`!K!` %\"`!C&` 3$`!C*` ?#` G&var`!6!`+%$` >(`$3)`*0\"faul` E$ = {};` &*.color =`+\\#` .+hover_` &9opacity`\"N!in_settings.`!Y#` 7$? ` \"9: 1`!&2` `;` 7*` n3` 7*: 0.6` },url`\")4descrip`,t#` #<_mobile` 24inactiv` '5zoomab` [!`/a!` 0+popup`\"T4` 8!s`\"M4` 7#: ` #\"`%6-ascade` d4` 8#_all == \"yes\" ?`!_! :`!q6_percentage =`*}\"` (&` A,x`\"]4y`\"v4x2` :5` 0&if `3S#s) {for (var`*_# in` ##` 0)i = 0; i <` 6$[` \"\"].`*l!s.length; i++`)%#` 4! =` :3[i`*@([` 0!]` E%;}}}`!3&d`!H*`*X![id] = Object.create(`\"G*`0l\"`!7$id].url` T(`&2(`\"v\"`!+&prop`!,'[id]) {` b+[prop] != \"`!4#\"` t(` 8#`\"Y&` +%;}` T2`&#$` Q1`'|!` >7no` B2`\"4#}}}`-d%`#j!`-Q7` ;%` ,)id`-x+` ;$`-{'` .!`-|%`(N*`!0\"` 7!` A+image_`+o\"` @0` 6&`)~,` 0,`)-,` t(siz`).` U(` <!` f8` <!: \"auto\"` o1posi`-T#` M6` <%` p8` <%: \"center` |2x` n9x` g9x : \"0` b2`0r.` N(`0k.` 0*` ^6`%@8` 6(` d8` <\"`$p8`2>\"`%r<` <&` r8` <&` p>`&#=` <'` r>`&.>` B\"`&1A` <+` |>`&:D` H\"`&J:` <$` t>`&X8` <\"`&\\:` <$` h>`&j8` <\"`&j>` <(` p>`&|8` p>` 7'`,j2ourc`2#&` 5*descrip`%K7` 7'` M+`(M6url`!;,nactiv`'s.all` =\"s_` :&`2I# ?`29!`\"b4id` R9hidden` BG_label`\"r3bord`#p5` 0)`% ,` 0): \"#ffffff`&1,` @#`$t8` 0/` v3`%v>` <)`,*1` 0.` p9`,.#` x0zoom_percentage = ` #+` =/abl`$a9` :&`$[Apopup`&w3` 7!s`)Q3` 6#: ` #\"` h+opacit`+b4` 6$` d2` 6$: 1`*?1` ^:` 6*` l2` 6*` v.`*0'_mobi`#_/`*S-` A$? ` \"5`#z$var region_id =` $$map[id] ?` 5$` )$` P$if (` N&&& rattr[` *%].cascade) {` G!` .-olor) {`\"@*`(l$` 92;}` V1`(^'` `-`)]*` ?8` f3`#1'` l-`.k*` ?8` f3url` d-`/,\"` 70` N3`.k$` Y-`/E'` <5`#;4i`$U!`/81` 81;}}c` 3!id] = Object.create(` Z))`%w!mapnam`*t\"us\" && (id` *!GU\" || ` '#PR` \"(VI` \"(MP` \"(AS\")) {`!>%`!h$`+u!`\"B\"`!#0`0q!eastern`0x\"s)`!C(VT` y(NJ` \"(DE` 0(DC` =)H`!^)A` \"(C` g)R`\"'*D`!t/`2N%`\"&#for (var prop in`*B#specific[id]`)2#` '.[prop] != \"`$2#\"`!$(` 8#=` `/` 5\"`#E\"` _5`/d$` _1tru`&L#` C9no` I2`+v\"}` \\!`-s6`*G#= \"off` \\)`*`+` .&`*[#};`#P%id in mapinfo.paths) {se`'?#(id);}}`-{!set`$:\"_attributes = func`*v!() {var `'$`$d${}`/c%` .!.font_famil`0`.` {\"font`0X-` 0': \"arial,sans-serif\"` v+`\"U$` P0` 6\"` j2` 6\": \"white` f,`#K*` L0` 6(` l2` 6(:`# *`$:#` &*size = ` W\"size`!D,`*Z\"` }*`(d&s`'\"% ?`&t!`24%` Y*line`&S%`!3+ca`4+!` #5_limit`4@.` 1'`\"\\,` 0(: 0.125`!>/`#N9` 6'`#T2` 6': \"#000000`$c,` @!`#]#` L5` ;!` j7` ;!: \"1` i1x`#%3` 8!y` &3parent_typ`/Y!`)J!` n,` =#id` M3anch`#&5` 6#`\" 2` 6#: \"middl`!(.ill` ~3width`!#-pill_` 5\"`!\",` 0'`&e2`#(4`##4`2V\" \"Not Named`!n,displa` G4`#65preset` 4%`,|-s`\"E-impor` 4&`/n\" ? {} :`.M%` V*` r!apply_`-z,`.A&id) {`.],[id] = Object.create(` W));` @0`\".&`1s!`3d-`!R*`3j'` '.`3_2`!!0`2D%` M4`3:\"` j5`3u'` b9`4!&` N9`4%\"` M9`4.$`2h\"`$<\"mapdata`$*4if (!` a0`$'S}`$+-`$#-`#OY`#n2`#9_`#2^`\"Q%id`&b.) {`(?/(id`#'(` S\"` B+`$T)` K\"se`(y5` m4` S5;`%g\"` n!ocation`\"@'` t)) {`+D)` H# = {}`+~&` /#`3.e06`3l(` v$`3_3`!z$`3]3` 1*: \"#FF0067`.S'` y$hover` B#` n5` 9(` t5` 9(`0M-`!'$borde` o7` 9#` l5` 9#: 1.`\"{/` 8\"`!n<` 9)` k;`#6)FFFF`#*5`!z<` 9)`#&;`\"=%2`&=/ize` l6siz`#c/descrip`'?#` D3` :'` E9_mobil`!)7` 9/? ` \"8`%U5url` m6url` @.inactiv`!L.all` =%s_` =&`-z# ?`-_!`!16typ`\"87typ` A/puls` :7` 8#` zD` J!`$|!` T;` >\"`&*5` 9': 4` o5peed` h=` @!` j<` @!: 0.5`.^!` 0\"`)E;` :'`!J4` Z$` \"(&&` \")!= \"auto\" ?` +)`#K5image_sourc`#K7` 9)`\"E5` 9): \"`+&0id`&j<` >!` t,` 0/:`4a!,`1e-.opacity =` *-_` 4#`1}3 =`&l!`,x4` n&`,N9` ?$`,s;` ?$`#{;`*T9` 9&`#u;url`(l7opup`&^7opups`'Y7` :\": ` #\"` n.x =`!#4y ` \"5displa`#S7` 9$`!@5` 9$: \"all`&4Uden`,:5if (`-b3= undefined) {`.&4\"square\";}for (var id in`0U%s) {lattr[id] = Object.create`!+-);` ^%prop` \\)[id]) {if (` 7!== \"overwrite`%a#` E$\"`!*(`&])` b)[prop];continue;}` p)reg` Z-`$l&` 7$` O\"` d/`,g!`\":#` X)` 8#`!62` U5`$[$` U1`*R!` >9no` D2`%B\"}` W!!`\">&`*7)`\"U)`+<,` 0&`,5$` V2`/C!` W/`0-$` .&`0^\"}}};set_`#a\"_attributes()` 3!state` %.label` $/`(m$` -)}var currently_zooming`\"m%var max_width` )!` B&pann` <,` 3'inch` 3(function `'s\"_tooltip() {var find_pos = helper.findPos(map_inner)` ~!x0_page =` J%[0]` 5!y` (.1` 4\"x0 = 0` >#` \"%h` ,%w` +%u` +!l` P\"_mid` O\"_` \"$left = 5` (!tt, h;return {`\"2\":`\"?%() {tt = documen`*K$Element(\"div\");tt.setA`$-$(\"id\", \"tt_sm_\" + div` @\"tyle.posi` z!= \"absolute\"` 5&`)S'none\";`#5%.appendChild(tt)` /'onmousemov`3<!his.pos;tt` \"4}, show`\"A'e`\"/\"`,6$opup_off) {`\"y\";}ignore`$|#`.R&tt == null) {`%M#`-T$);}`\"%0block`\"I'zIndex = 2` *&maxWidth`08!`'9# + \"px` T!`\"S!HTML = `!t#.sm.content;`!@$updat`!o!`\"8%;}, reset_pos`\"V'x, y,` d$`\"`#`\"6\"`0Y(`\"./` +#` k#(y0 + y, x0 + x` h&;}, `!E&`#e1` [,u, l` S*` K+, manual`!h#` $%u`#1!nual.u;l` #&l;} else` <\"ie ? ev`'T!lientY +`'b&`'m$`'g#.scrollTop : e.pageY` v!` U-X` D>Left` ^%X;}u = u -`*Y$` s!l -`+#$`&C!`&j% || `###_`\"2\" || `&s'` 5'up && on_click`'B'`#W0`#]!`%G.`#x*`#d\"!tt || !u || !l` n'`+o! =`%9\"0.5 * `.V\"`+~! = `%[!` 3\"height`\"9!l >`,I\" && u >`,L\") {quad = 4`$R$` G\"<` 093` ?*` m)<` =,2`%K%var ` .#1`0q#entered`({& &&`)$(`#R$ &&`$:$` J&= \"yes\" || ` (/auto\" &&`\"y\" < 401) ? true :`+V'` N$`.i!`*w#top = \"-100`*j#` 2\"`/W#` '.bottom`.O!uto` .'righ` N!` 2\"h = parseInt(tt.offsetH`$D!, 10);w` -1`,G!` ;\"var side =`\"4#- w > 0 ?`%0#(` .%) :`1p#bar`34!`!I!- h` C*` .&` L\"`\"Y+bar`-P'`\"])`!D!` ,,`\"E+`\"l5`%Q$`)4'rienta`1z\"= \"below\"`(.#`%v\"= 3`&3&1;}` .(4`&J)`&z'` h2above` n,1`'[)` .(2`(B)` .)1`%q(`%35`#H\"u + 5`#84l + ` &!` 00`#I+`\"5'`!W(` jZ` |$`%|$l`!R8`!/63`!41`&?%`!&2`)/#`!_+`\"h.3`\"SC` Zd`\"?Q}}, hide`/_'`&C#tt != undefin`+j*display = \"none\";}find`11!= helper.findPos(map_inner)`,b!` A$) {`20# = ` -$[0];`2V#` *(1];}}};}`!Z%getxy(lat, lng`!i#mapinfo.proj`(S!lambert\")`/N\"` 2\" ` /#`%B(` I-xy\") {alert(\"This map only supports x/y loc`)b!s.  These can be added to the mapdata.js file.\")`! 9robinson_pacific`!q+` /,` O9mercator` U+` /$`2G)` q+`2U\"initial = {lat:`#k$:lng};`$'%interse` (!(x0, y0, r0, x1, y1, r1`!:#a, dx, dy, d, h, rx, ry`/x!x2, y2` &!dx = x1 - x`03\"dy = y1 - y` *# = Math.sqrt(dy * dy + dx * dx`1:\"a = (r0 * r0 - r1 * r1 + d * d) / (2` %!`!9# = x0` ]$a / d` 4!y2 = y` 4!y` ,)h`!8)` &a * a` o\"rx = -` S\"(h /`!%$ry =`!\"\"` *(xi = x2 + rx` *#_prime` 1\"-` /$yi = y` C!`#6\"y` >&y` C!y;return {opt1:{x:xi, y:yi}, opt2` -\"` P\"` 3\"` &\"`(|)`(F#(lat`)#\"`!v!adian = 0.017453293`!@!pi`\"h$PI` +\"hi`)-!tlng.lat *` V#` 9!lam` 3'ng` 0*phi0 = 45` D-0 = 9`#g!` A)1 = 33` &-2` R/n`!`$log(` $!cos(phi1) * (1 /` 9\"` /#2)))` +$` I%tan(0.25 * pi + 0.` &!hi2` V*` /51))`%/\"F`!F$`!9(` ,!pow` n;1), n) / `\"3\"rho = F` T(`!'>` ^!` W$0` .N0` [\"`&H$x:rho` Z$sin(n * (lam -`$Y!)), y:`!&!-`!l!` D#cos` 9.`&a(`,2$`&`*earthRadius = 1`\"\"\"`&j4roundToNearest = `2L&` 4#, value) {`\")#`!W!floor(` 5! /` ^$) *` #$;}` z!getSign` k)` `+` ^\"< 0 ? -1 : 1` U#lng` U#` _#`\"@#.lng`$8\"la` v$` 2,at` ;#ng`&$$abs` M0` +0` U%ow`/{!`\"w((5,` R!- 1e-10);` ?\"` e!=`\" !0 : low`-J\"igh =` d!+ 5` m$Index` 0#/` 0#high` 0$` Q!` 1$ratio = (`!0\"low)` 3%AA = [0.8487, 0.84751182` &\"479598` &\"0213` %!3359314` '!257851` &!1475` Q\"0006949, 0.7821619` 3!7606049` T!7365867` l!7086645, 0.67777`!7#6447573` b!609875` 2\"5713448` b!5272973`!<!485626`!R\"45167814]`\"?!BB`\"?!, 0.0838426, 0.16768`!l\"251527`\"C!335370` _\"19`\"L#503055` Q!58689`!Z#718226`\"*\"533663`\"z#51804` j!915371`#U\"99339958, 1.06872269, 1.14066505, 1.2084152` ?!27035062, 1.31998003` '!523`\"3\"adj`$[!(AA[`%5%] - AA[`%]$]`(n!`%=!+` +)` Z$`\"y!(BB` T*BB` L2` ,(`,4'`!D\"*`(I!`0/$` )\"`(z!*`+G(, y:`!2\"*`)7%` 4)`,-0_pacific`,;*`)W\"`2D'- 150;if (lng < -180) {` A#ng + 360;}`!s#`-6%{lat:`)s&, lng:lng})`!A'mercator`!6*y`2H-tan(`*_' / 90 + `2c\"`4e# / 4))`21\"80`0($PI`/h(`\".&, y:y};}var calibrate = mapinfo.proj_coordinates;`!k%find_point(initial, pt1, pt2, pt3`!#` ]!` ;# =` *!` I$`,d\"pt1_proj` 5$pt1` 1$2` *+2` 1$3` *+3` 2#roj_r_pt1 = helper.distance(`!=(`!f!` _!` J+2` 6?`!P\"` S\"dist`!D$` G-` |#` ?1act` =2` I!` C\"scale =` z'/` T%`.Z\"`\"D#`\"M'/` P\"` 6%2` 3(2` 2)opts = interse`$q!(pt1.x`\"y!.y,` z\"`!L!` /\"2` -$`!Z#`!H!third`\"t?`$N\")`!T*emnan`!!`2A%` X,opts.o`!B#3) -`!,'`!A\"` _#2` B@`&t$` X*if (`!C%<` h%) {`(K&`!?%.x, y` $'y};} else` :0`#5!` D&2` I!`)*!rules`({,` 0!`!I\"ules) {for (var i in` O\"`(s#ru`%Y!` .![i`.Q\"condition_stri`,8!rule.` /%;try` Y\"` *% = eval`!-!` ?&);} catch (e) {console`,'!\"The` T'\" +`!&.+ \" is not valid JavaScript\");}if (`!!&`+##oin`&m!`!D!` (\"`,M$`+Z0`,G%[` F\"[0]]` #/1]` \"02]]);}}`/($` b:0` U)1` c)2])`-y\"tt_css_set = false`-c&set_` :\"() {if (` D&`&<%`0&'newStyle(str`\"v$a = document.getElementsByTagName(\"head\")[0`%F\"el` F(create` N#(\"style\");el.type = \"text/css\";el.media = \"screen\"`&o!el.` S!She`!s!` #).cssText = str`(*%el.appendChild(`!A+TextNod`\"C\");}pa` A)el`1n%el`\"u'getsupportedprop(proparray`(4$oot`\"R(`!/$`\"Y#;`(k'= 0; i < ` [%.length; i++`$H#` 3%[i]`)C!oot`\"V\"`!*#answer`.G\"` A%;` -%` #\".replace(\"borderRadius\", ` )#-r` +\")` ?6MozB` M+-moz-` 8EWebkit` S-w` 2!` =FboxShadow`!x\"x-s` (\"`![<` K'`!q#` 7@`!m$` R(`!i%` V'`%e#` Y\";}`.B#ound`\"3\"prop =`%m.[`$ -`#U/`#'/]`1:#cs`,S!` z+?`!*-+ \":`-k!popup_corners + \"px;\" : \"\"` l!min = width / 2 > 250 ?` '': 250;max_` .\"=` l#max` +\"?` }#` (%: min` y!`#)\"`\"P8`#]'`$F,`#{,`\"\\#s`\"]\"` p'?` |(`\"P%3 *`!R#` 8\"`\"V\"`\"o!3 ` \"54` %2rgba(0,0,0,.5)`#9$`)k!` A(< 0.01) {`!^#\"\"`/,\"m` (#.tt_mobile_sm{margin-top: 5px;} .tt_sm{\" +`%$\"+`\"H\"+ \"z-index: 1000000; backg`%2!-color`%!*lor + \"; padding: 7px; opacity:` A&` +#` G\"font` \\(font` 4\"` |#black`!b#nam`\"\"!float: left` \\\"-weight: bold` F\"custom_sm{overflow: hidden;}\";`\"z!+= \".btn_simplemaps{`!))text-decoration: none;`\"]&: #ffffff;display: inline-block;`\"a%5px`#k!`#z\": 0;`'i\"`#X!%; `*D)iz` O!`,H#box; `+E&` */` =4`!D!h`\"r#1.43`\"*\"align: center;white-space: nowrap;vertical` C$middle;-ms-touch-a`1e!: manipul`\"x!;to` \"5cursor: poi`!0!`\"G$user-select`#U#`\"D!` #0s` #/` 6.`\"Y\": 1px solid` +#`/n#: 4`'p!   `%)+:hover{  `%\"-underline;}`,Z\"xml_`&\\! = vml ? \"left`)\\!right`&2(xmark`'('\" +` V'`'c!`)M#left`)O\" `\",`$\\*0px;}\";newStyle(mcss);tt_css_set = true;}fun`$(! get_zooming_dimensions(element) {if ` &$.sm.` :.) {`18#` *9`+u\"bbox = helper.rotate_bbox` x(bbox, transform`.C\"gotoX =` ]!.x` *%Y` +$y` *%W` +$`(b!` .%H` /$`#'\"`1{\"atio` %!zp =`!t)p` 2!paperW`1##original`14#* scale` >&H` t!` =(`!%\"` A%`!|$` \"\"- (`!i\"/ zp -`!v\") * 0.5` F!Y` D#Y` C$H` ?(H` ?(W` D#` g\"` .!H` ,#` O\"`09!` :$` 1\">`+b\"_to`!a#) {`\"l!` a'`\"V&`!K#-= (`\"A(*`#>\"`!J&/ 2`!2)W /` x,;} else`! *H`!&$` s\"`#%#`!&%`#~\"`! *W`!$&`\"X$H *` |.`&x#{x:` p!, y` $!Y, w` $!W, h` $!H, r:` }!}`(8'reset_s`'&!attributes(region`(;#!` %%`!'\";}all` N\"s.stop();for (var i = 0; i < ` Q\".sm.` E#length; ++i) {var ` 4! =` \"\"_array[` G,[i]];` &!.attr(` %\"sm` )!`!j\");highlight_labels` @\", \"`\"<!\", false, \"` 3!\");}`\"T,las`\"b#(`\"Q#` -!destin`-O! && ` $,.sm.type ==` t$` /4`!d'`#Q#` 20ignore_`.r!) {` 2-`\"^!` `;;}`\"^-` B,, \"out`\"T0`#y\"`%\"G`$H$`%C\"` '#`!b!`$`&`!Q(`$k|`%\"E` X\"`\"T,`\"Y!or`(-#by_`(O!() {all`\"w#s.forEach(` P%`\"x*`!O&id == -1`#&'` 3*`0\\..r >`*f\"` P!`)E8`+d&`$>:;}})`*E-`\"&&` F(`!nW!`\"@%`!!Hshow_point(point, display, `'&'`%?#`)&\"` -(`):$`0 !` S#`)I!all\"`4V&true`#&$` :,out`)q!`* %`(6!` 6A`%}#` Q)`&0$`\"|\"Raphael.isPointInsideBBox(`!v+bbox, `\"^!.x,` \"#y)`!8,`!23`+r&`, +`!@#in`,v#`#\\$`#P'` l3`#k\"hreshold = helper.to_float`!8$)`#v!` ?&&&`2##<` T&` r-` '#`)G!;`%C%`!\\&t,`*D\"`%1#x = pt.x;var y` '\"y` (!potential = []` .!`#{\" =`*&$`+'\"`0O%` 0\"]`!q!`'D%`+{7item`,(&i++`!M#curren`)V#`!%%` I\"[i` z\"`%$6` Q)`%6&x, `%/!`\".%.push` <.id);}`$A%` ?+` 8*var `!x\" =`#%&`\")$`2E!` 8\"< `,=&`$3#`,H!` U$=` ;!if (` ]%[0] =`.n#`!*#`%r7pa`!?!mapinfo.paths`$.&id`\"y7Path(path`\"y&`%v7`/G'animate_transform(e, t, `0p$a = {` 4%:t}`!?!!vml && !touch` %!i) {e.` h#(a, zoom_time * 1000`.d&e`2Z\"a`0n)`1I!_corre` ,!`*T(, initial`.W#`1t\".hide(`3%)i` `$`'O!`!|#lbl =` ,(`&V$lbl.sm` b!) {continu`$n#`.L'` @#` (!0, ` (#`.Q1`!(&_set`!-%`0j!`!5!` V#id];` >#.show()`&$#` 9\"scale`&%#` ,#lin`*E$line_`%e#get_` '%(lbl);` F'`#b\"{path:` =%, `$d'` \"$}`(\"#factor =`,<#>`\"F$`!M!_limit ?` 6#:` */`+\\!t = ` .\"`#8!,` n$*` 0\");`&C.` B!t`$~&`$4(pill`\"T#pill =` \"!`#?.` d.pil` h+}}`&G(oc`$R!`&.D` H#`&P%` '*forEach(` v%(lct`$]$ct`&?'`)F\"`&6.` ?\"`&?%` )\"`&04` :!`%o(` I\"`%s$`$W2` 9'`$[.` ,-`$[.ct`$HAct`#f,);`#m&hide_and_`\"W!before`#`4`!=!`4M!`\"T(.sm.type;back_arrow`$,$`$KE;`+KB;`$f&update_`2\"\"s(`$t#helper.x_i`3C#(type, [\"`/5!\", \"` L\"\", \"out\"]`.w\"`*o!ll` h#_attributes`$i$`\"Q\"=` V%` M%` s!` E(`%N(`.=$` U)`!D\"` G6`!<$`(m!`#Y+` 6\"]` c2manual` t\"` ^!or` w#by_`&4!(`%9!(`#5/opacity`#@$` o!!=`\"~\" &&`%1\"!` x)all` y\"s.stop`)<#pill` $)` <#`-7\"'fill-`!&#':adjacent`!7$}` X(` *D`\"v)`!2\"` '(` R11` H,m.`2 #`+&/abel`+3$abel.sm &&`0h\"`-H'` #)`!B$` '*`!35}`!A+`4R${'stroke-width':`!e+border_hover_size * (` F! / original_` ,!) * normalizing_`+0%1.25}, zoom_time * 1000`&:%`$|(`!g5`$7;`\"<#`%C+`\"-3`\".#`!>m`,|3after`)i)`%%\"`+m#_` e!solo &&`,!$` 0!`(*!-1`(:!`-</`*+` g(back) {`-e'`0T$`#l#`1L$`*;'` j4`+H\" ||`!$< ||`!v%`!!<if (`*C\"`\"D\"`\"+3`*y$` U2`#g&zd_to_tween(bb`3k% {x:bb.x, y:bb.y, w:bb.w, h:bb.h};}var end_`!3'`2c!`$y\"wee` )\"current_viewbox;`!0&` C!o`0Y1, call`\"d#`*7\"st_`&o#d`*,!` %'`*'&` *' = false`0>\"`!?#ly_over && !`!-( ==`!a$` <#)) {out.call` O+);}`!%!click` ~'`\"^+`3|*;tooltip`3w$` (#_up` W%` |&zooming = true`->,` 7#_dimensions = get` O$` .'`2D*var to =`%()` \\=` R\"from` K+`\"`!` <?`2:!`\"`*` 52.r;`*R*before`%M1)`%&`3!\"Zoom`$A$`,a\") {`&N+ `$}%` <\";paper.setViewBox` S*.x,` B*.y,` \"+w` 2,h,`$h\"`,i(whenDone() {`,b;`\"5'`#O,`&!+`%Z0`&\"\"on`&u\"` (%`\"{\"`-h\"level();trigg`1[!ok(\"`$$$complete\", []);if (helper.isF`\"(#(`)R%) {` $$`+P!if (!vml && (!touch ||`*C\"mobile)`)5!`\";$ {`&$!able = new T` ($;`+<& = ` >%.`&T\"{from:from, to:to, du`&B!n:`0n,, easing:\"easeOutQuad\", step:`$:%`%i-`%|5;}, finish` S') {`$|%to`3L!`0H%`&R.to`&G.to.x, to.y, to.w` '!`&#&`%y&`/1(create_bbox`!X\"(auto) {var pri`!p!r`%V\"\"\"`*#!` B!` L\"array = {};for (` 6% in mapinfo.paths` j$ath_to_add =` 4*[` T!];` 5*Raphael._pathToAbsolute(` ;'`+X\"bt` C'pathBBox` 5.x = Math.round(bt.x` 3\"y` (-y` 2#2` (.2` f#` ).x2);`#*)+= \"'\" +`\"q#+` '#\":{x: \" + x + \",y:\" + y` '!x2` (!x2` 4\"` (\"y` )!},\";`#p,`#2# = bt;}`$E+` #(.sub` $\"(0,` ,*length - 1`!l/}\"`*\"!!`%S#console.log(\"The`)2!`%D-is: \\n\\n{\" +` ));}return` C-`-%'`&p#content(element`%~#` /# = ` 1#.sm.descrip`,t!var embedded_im`'7!data:image/svg+xml,%3Csvg%20`*t\"-backg`$e!%3D%22new%200%200%20256%20256%22%20height` C\"256px` 1\"i` U#Layer_1` /\"version` 4\"1.` .$`)r\"` 0\"` o3width` t-xml%3Aspace` 6\"preserve` 6%ns` 3\"http%3A%2F%2Fwww.w3.org%2F2000%2F`\"\\!` J'Axlink` =<1999%2F` F\"22%3E%3Cpath%20`\"Z#M137.051%2C128l75.475-7` \"!c2.5-2.5%2C2.5-6.5` C!0-9.051s` +\"-2.5` +\"%2C0L128%2C118.949L52.525%2C43.475%`#;!c` N!` i%` S*s`!*#`!$%%2C` 8!L` m#`!b#`!\\#%2C`!e#`!^'` K3`!7#1.`!M!1.` \"!2.88`!m!.8` i!4`!j#` +!s3.275-0.6`!\".525-` 5!`\"@$`#7\"`#0#`!C&` YG`!P\"` p6`#g:L`$R)z`%_!F`%\"\"`%o#3E`0)\"xmark_modern = \"<img id=\\\"xpic_sm`-*!_\" + div + \"\\\"src=\\\"\" +`)Y*` 7! style=\\\"`'w!: 18px;\\\" alt=\\\"Close\\\" border=\\\"0\\\" />`!?(vml`!C!a` a%line-`)k\": 1.5\\\"`!A<>X</a` j( = vml ?` y':`\"N)` F!url`,(*url ?`,;(url :`3B$url_sub = url` -!js_` ^\"` 4#`/%*11) == \"java`-(\":\" ? true : false` a!tab_click = \"(`.-$(){window.open(`#z\"url`\"L\",\\\"_newtab\\\")})()`!]\"reg` b%`!V#?` _1location.href`$v#` t%` k! :` D1top` :?`\"s'_clean = helper.replaceAll(`#/#, \"'\", \"\\\"\"`4C\"js`\"Z2\" +` g*+ `!$'upon` O%new_tab ?`#S':`\"u&`2}!` e\") {` M)`!5$;}var mobile_par`1g6_` ?\"`%l*` -/: \"<div class=\\\"tt` 3#_sm\\\"><a` 1%btn_simplemaps\\\" onClick='`#f!`!e&+ \"'>\" + link_text + \"</a></div>\"`\"D!!`!M'` S$) {`(B$\"\";`\"9*\"\";}if `4`$`(!$== \"\" && ` d(`\"6.) {` ].var content` /$` (#` p#? (` 1+\"\")`\"0custom`#.!; /`\"X!` b$`\"V!`\"Q\"return` L.`#d\"div>` *,nam`$\"#`-$!`\"8&name` q&` J)`+L\"` L%`#K\"` =*`,m$clear: both;\\\"`$8#`!g#`!y&`\"V\"+`&J)` f&`\"5#}`(+$ is_forgery() {if (map`!k!!= \"continent\") {`\"j#`+p\"`$*!i = 0;for (` *!d in mapinfo.paths) {i++`%F\"i > 8` ^&true;} else` l,`!R)inside(small_`#N#, big` %$) {var ` <! =` \"\"` 4$.sm.zooming_dimensions`'N!` C!.w >` `(` :2.w`\"J1bb =` M,bbox`+G!big = {x:bb.x * scale, y:bb.y` &&x2` <!2` 7'` -!y` )%};`\",\"xbar`\"7$.x +` ##w / 2` <#y` 7(y` :%h` >!`\"K%` d\"`\"R!.x &&`#+#` X!` 0\"y`%D#` D'<` I\"2` A+` 1\"y2`$g,}`# *`$g%create_pattern(e, par`$Z#hovering = par.` *! ? \"_` %!`0i!`/!\"` W#_id =`4Z$` k$_`)6!.sm.id +` k%` S!exist` {\"docu`$N!getE`$X\"ById(` q&)`#,!` K$) {`16#delete`%+$` 7&`%M\"svg = map_inner.firstChild`!=!SVG_NS = svg`*q!spaceURI` 9!defs` 5#querySelector(\"defs`23#`![#`!x(`#O\"`\" #NS(`! \", \"` E#` S#`#*!`\"t#;` 4#.` 0!`\"L&` /%setAttribute(` a$Units\", \"objectBoundingBox` {$mage`!6A` E!` O#rect` 0Arect` M#bg_color`%f#` j!` +#?`%z!` &(:` -!` '!;` 3!`\"2)NS(\"http://www.w3.org/1999/xlink\", \"` $!:href\"`'.!` x#url)`#*%append`%(!(rect` $2` T!);defs` +)` E#);svg` ,)defs`#S'_posi`(g!`\"N(` -$` C!auto` 1)size`3 !auto\" ?`)`! :`)W#`#}\"peat =` s-= \"` 6\"` B1manual` A2` 6\"` B1center` A2` 6\"` B1fi`/\\!`!e#||`!7$||` j$?` H\" :`,!\"`.\"ox = Raphael`1Q!BBox(`1Z)[id]`&1#box_width =` (!.x2 -` ##` 8&height` ;$y` :%y` Z'2` [$` i#/` O(;`!I$_preload(`&)), `-j%(`-X#` 6\"`!S$this.offsetWidth`%V'`!S%` 9'H`!/\"var `!R\"` b(/` L);`!1%get_per`!9$p`$N!`&(*`-`!auto`0K#`$,,` 0#w2h > 1` '#`!0(>`\"w') {` ~\"1;} else` ($`!_*` G&;}` @$` m&`\"W#` q#` (\"` o&` V%2h` o1` W#`$>)` G)`!*$if (`&O\"` t%w2`!T&2h`\"u!`\"R\"` +$` A%per`!/+1 /` 0\"`\"%)per`#0#`#h0 * normalizing_factor`\"p,`4!#` w!`$[&`$q%`%M!new_`&0*`'+'*` W!` 9*`&9%` J,/ w2`&e\"`0k$x = 0` %)y ` \",` W!,` &%`&`#`'G&` Z&` *\"` \\\"`&E&) {` ^)`#[#` b*` /\" *`)S&`!g\"`$e'`'*%` ^,1` Z.1`0c\"`!W!`$-&x` w%`#z!`!e&` 9&y` ;$`\"@#`%{1` rJ0.5 * (`$J'-`$*,)`!A'` C(` i#` D(`(L#;}rect`2|)(\"x\", 0);` '/y\"` \"4`!;!\",`*<(` 30`!1\"` @$`!=$` 6/fill\", bg_color`3\\&`!?.`&J%` -4x` :'x` -Y`\">$`$N)` 54`\"C%`$i*`$>#`!Q/`%%#` +2y` 8%y` +2`!T$`%B.f (rotate`/\\#cx`-A%x +`*.-* 0.5`4\\\"y` @%y` <)`&*#` D\"`!90transform\", \"`!7\"(-\" + ` &\" + \",\" + cx ` \"%y + \")\");}` ^0`#E%`'1/);`-:#\"url(\\\"#\" +`#n$.id + \"\\\")\"`-V\"state_`&[!array = false`\"M!make_` =!`&|#storage,` P#` M!`32&creat` J#s(refresh`2Y#!` %&` ^( = {};` c'` +\"}`!D/mapinfo.` +,`!e!scaled_border`0D\"= ` \"(*` =\"`0J2* 1.25;`\"E& = `\"-%(id`%c#brand_new =`\"U([id] ?`#1\" : tru`#8\"` b$` M&? paper.path(`\"($paths[id]) :` g,`')!` W%) {` ;!.sm = {id:id};}`#Z!vml` 6%nod`%`,class\", \"sm`\"<\"_\" + id)`%@\"attrs = cattr`!4!` .$` [!s = {fill:` D!.`, !, opacity` .#` '#, stroke` /#`#}#` K#cursor:\"pointer\", '` H\"-` W#':1` *&`)<!':`$j.` 9&linejoin':\"round\"}`!u!` E#hov`!4$ =`\"?\"`!I$` /(?`\"Z\"` '0: main_setting`\"!*` x.`&Q#` [/` 5!` s1` 5!:`' (`'>/` r)` \".`'9@var ` M!`$h*`$V*` n\"`$a%`%('`\"j'`$y%`\"~.`$=;`!i&`*M#.sm.`,Y!`+n%if (`!,\"`,p\"url && `'Y#var`.[#paramete`'2!{` t!:` ]!`00$ur`!s$` '%` 3$siz`&w$` '&` 5$position` 4)` -$`1M%` 1)x`1C%` *)y` /$`\"v!` .)` -!`'%\"`2g$url =`-{$` .#(`.U#`\"C,)`#,.`+q!`$q&.fill =` r(`#J-`$(\"ur`#7>tru`#?5` ]%`#E5`%,&`#G9` A\"`#L:` >\"`#Y3` 7\"`#[7`&~'`#@X`(W+`#W0} else {` '@`/;!`!`#nactive) {` J'`-o\" = \"default\"`/n\"`'5)ource`06'.ignor`\"M#`%F6\"url(\" + directory +`,?#` m( + \")`!*)`-Y/||`,h4`!L(emphasizabl`'/%`#'$` -4`*u\"}` 6%`-1A`(=#attr`!o!`\"S\"`(S$transform(t` \"$`(k'`-W)` #&` 7&`-x.` #+` A&description`03%` )'`!*'djacent`.s+'fill`2v&` =%` +#`-'hid`15&hid`\"[$` 4#_label` 4)` -\"`*u!brand_new`$A(reg`!q\"`#r,nam`!!&` (!`25$` (!: mapinfo` *!s[id]`! !!` T)` ~(` j#id` z'`)Q\"` q\"url`-*'`(A#` 5%` )$`$6'n_click = is_on` (!`'5#popup`%#'` +!_off` F#ff` 23`#)!s = []` +&zp`!>%zoom_percentag`!C'zoom`'E#` @&abl` 8'`%H'_mobi` B'` ).` M&type = \"` ,!\"`$}0`';$`%%)` ?%content`-5&` *#`-9\")`-b!sba = ` ,!_bbox_array`$j'ba) {` @\"Raphael.pathBBox(`%A$path`%D!);}var bbox = {x:sba.x, x2` $\"2, y` %!y, y` .\"y2`'['` R#bbox` '*.width` 4#.x2 -` ##` 8+height` @$y` ?%y`\")!`#5)`&y%hide()`,@%`(A,all_visible_` L!s.push`#=$`/^\"`(q-`#>&`#W$;all` K2;for (`4B!d in`(w%`#R!) {mak`!4#(id);}` ^'`!t$func`,!!style_background() {` $&`-l\"{fill:main_settings.` :&`/l\", `,-+` \"#, stroke:\"none\"}`%+#`+G\"`\"O\", last_destination, ` \"';`!c%`&o#` V\"s(refresh)`#}\"!` %&` q( = {}`1g\"` Q#) {`#1+` /&var`(:\" = rattr`'T!`!f'object =` L$` 1+` 4!`!B! ?`\"7)`$t!: paper.set()`(I\"`!e,.sm`!l\"` &%.`$T\"`+c\"`!}&` m&) {console.log(\"Duplicate R`!^\"\");continue`#w\"all_bb` l\"`\"a& = 0; i <`\">*`!9#.length; i++`#\"#`'2\"id`\"e%` D*[i`\"t\"` )!`++%`!q\"` S$`/~(`!y+` u%+ \" does not exist`\"$)`)[)`!B\"` V0`0o%+ \" already assigned to a`!~#` p)`2K.id`$*-`)S'_id`$r#(vml &&`\"P\"`1c!gnore_hover && `0p#`(X! ||`.o$over`(l\"))`%L&`*S)`$Y\"` )'`,f$)`\"h\"` d\"x &&` n#y ` \"%x2` ,'2`,G#`%F\"{x:` =#, y` $#y`.f!` S$`._!` T$}];}`\"`&`.b#helper` )!_union(` z\"`*O#attribu`'I\"{`+\"+` s\"`+.%cursor:\"pointer\"}`&(!`\"z!` D?`#B\"` .#}`$.!`#e') {` Z&.fill`2o%` ;!`#<(`$&( {`!1+` N*` @'` V(inactive`!(*`\":\" = \"default\"`#D(`\"6)` #&`&Q'`'H!`)*,name` 9'descrip`-x!`!]$` )'` 42_mobile` =0` 4#` M'ur`\"U&url` /'labels =`-')` 5&on_click = is_on` (!`#*#popup` A(content =`/{$` *#`-E#` k)`%P-` #+` A'adjacent`%r;` >$`&%%` X&zoomab`#\"'` )$` 9'`\")!_off`\"D#ff`\"/4zp` [)_percentag` c(`&&$` B%` )$` 9'type = \"`,k#` R(d`,^\"`1.+all`3O$`+R\"`#K*`1y'`&J$`'!(zooming_dimensions = get_` '.` p%}`(<!`4>3[-1]`2a\"`*O!u`3g&` 6&;out`3%%` &\"`\"O%out\"` ,$`#P!1` d!`,7*clone(initial_view);bbox.width =` J!.x2 -` ##` 8\"height` 7$y` 6%y`!)$` #bbox`!;%`\"VGout);last_destina`)w#out;}}func` ,!`(\"#borders() {`#K!mapdata.` 1#`#S!turn;}for (i in ` 2.`\"x!` )! =` /,`4_$b`*\"%path(` 8\"` '!);b.transform(t` \"$);b`,b!({stroke:` L#`-[!, fill:\"none\"`/[., '` O\"-dash`%/!':[` Z#dash]` 8&`$J!'` v$size` 2&linejoin':\"round` n'miterlimit':4});b`&$%b`$Z(.getBBox(true);all_external`#u$`(L\"b`43#` --hide();`$Q&get_`-Y!_bbox(e`#y$b = e`!!+if (vml` :%2` ?!_` 7*bb`&j(2` &#`3m\"xa = 0.5 * bb`'U\"`$j!y` +)` K#var pt` ~!`,T!int0` 0!new_`![!{x:pt.x - xa, y:pt.y - ya, x2` 5\"+` 5\"` )!y +` 7!`!3!:`!9$, `!*\":`!1%};`'!\"`!\"#`!u\"`\"~\"`%8!`!@!` *\"set` )'make`#G\"`!w\"il` J$`(:,`1J\"() {`*W%ear_sets([all` :#, ` '!ine` $#pills]);`!C'`%a\"`!\"&` *\"`!P+` )(`0s)` #,.reverse();`\"$& = `!x%(id`%9#`/l!` P/[id`)_\"default_sca`1J'line ? true : false` I!already_rotated =` 5#`+E!`!V-hasOwnProperty(id)`+S'`&y!rand_new`!X&`0N%?` u\" :`!A!`$a*`+P%set()`$b\"`&V!`&J\"`!~\"x * 1, y` (#y * 1`09\"` G!`0D&pare` +!`\">&resize_` ++if `4!$` 4!_`0X\"= \"state\") {` I%` -!`\"(#` G)id];} else ` V6`4 #` e(`2!)` @Nloc`03!` g)` -$` ]5`$s!`#G$&& ` %#y &&`#0#`0`#` '\"`3H&`!#+`%f.`$p!`$$!.x`$j!` X$x` /#y` ))y` 0\"0` ()`+h#`'3,` v!`!G*auto_size`&\\!`$n*` J!}}`\"F\"`\",%console.log(\"The following object does not exist: \" + id)`,1#`#6\"`#)\"nam`\"m\"Not Named\"`#4)` :(`\"4'id` _\"`(=%`4K$`#?+`*D#`#R&`,H#` +\"(`$p#x,`*5#y], `3h'`($%x:` [#[0], y` %%1]}`.3'`)2%text(`$Z#,`(m\".y` |$name`-4)`*.!`*>#`'%$`!q!` m$` =+;}` ,!`3Y%` &$`2w!`,>%hid`$~( &&`%)(hid`-T$||`#M'hide)) {` d,`%C\"` -%`%X%` #\";` W&`/r\"`4_\"` '!`3p\"` 7&`)f\"`&G!`)j'` 0,]` U3`#h!`/t){'stroke-`2h!':0, fill`,s#color, 'font-size'` 2#size` 0$w`31!':\"bold\", cursor:\"`$T!er\"` @$family` Z$font_` -\", 'text-anchor` 9$` (\"`.\"\"over`1k*{`!['h` :!`!g!` H#ut` 46` A#`#I*inactive`(L#`0#`\"$\" = \"`*b#\"`$]$attr`)>!` D\"`&j#`*k!`!,(` #&` 7&`!z.` #+` @'`!x,` #*` ?&`-T\" \"` ,!\"` .&id = id` '&`3~*` (\"`4$%`-!)` E+_limit` L*` .\"` ?&pre`3<\"`/g#` *!` 5&`.h!`*0\"` +&`.g!`*<\"` ^'`.h%`.a\"` 0%line_x` y%` )\"` 0+y` 1*` l'line`3i%` 0\"`,E%(`\",\"t`(D\", ` *!)`)8\"`1T#display`-d#`3><`!-%` P# = \"out\"`3\"J` S/`'+&` -#`-,%` :/main_settings`*k#_` 7$? ` \"9: \"all\";}` e7`\"m)`1M(`#g!||`%C$il`-e!`2t)`\"3(bbox = get`.1\"_bbox`,M%` i*`1N#`$y!path` P$` ($` O$` =$`0o%` 7\"` ?$` 7&`4Z!`%h*` -!* normalizing_factor *`%U\" * 1.25` Z&`+b*`./\"`+k#` ;!`.$#`-J/`.S*`!6%};line`+d\"` s+)` 5\"`1y%` '\"`*M5`#y!`'o%` F#`\">2` 6%`+)-`#C#line;all`#j!`0u$in`4*%set` )(`$V!`'(&`'[%state\" &&`%V'`$p$`%7%`4T$`%Y$`#h!p = 0.1`#t\"calculated_`#:!` J$` Z!.` -\"* (1 + p * 3`%.\"pill` G%`!:\"` (\"?`!G#` (\":` n-` T&h`2O!`!!*` -#`!($`!&\"`!x)x - 0.5 *`!8'` A!y` :(y` 8*` q\"` B!r =`!2)/ 5`2\"\"ill_array[id]`#A#`%/#` .*`+#%` 6(`(8!rect(x, y,`!R'` %#`!)\", r);` a*` y#;}pil`-j?` A!`'.$` &#`-8#`,p%` /%`(d)helper.clone`&%'`('(`&<*image) {` \\..f`\"Q\"cattr[` G'd].`)`!`\"'#`461`!13` :+`!-<` @+`!68h` G!`!I+adjacent`!7A` :/`$##`+V!`\"o.`!x\"` g#x_in`%*\"`$g\"`/t', [`*!#,`0D\"])) {`!/&`)|#` b#bbox_union(`\"8'bbox,`*8*]`+/#` {B`3j#`!9% &&`2w'` 8\"`(*#` '\" =` \"#`'?#` =,];` \"\"`!d:` 6*`!p.` 6&zooming_dimensions`.b%` &;get_` '.(`\"-#;}`/P,`)E!all_pill`.t#`.3!`.k,`.H!`.-`3e#`*y#` '3if `#v- != \"out`/J!` (1all\" ||` 3&hide)` y(hide(`!4&all_visible_` @!`1\"$` I$`!D#!`\"Z&reset) {top` 69` H&`1D.location`\"%!` r&`2&! {` 6$` f5`2r!` &2` \"%`-o)` -%`)-!!vml`\"m$.node.setA`)O$(\"class\", \"sm`!!\"_\" + id`\"2#resize_`\"1\"`/|$adding = main_settings.`\"\"%auto_` >$? 1 +` %A* 2 : 1.3`1R!size`/;!` 5$`(!)`3G#/ normalizing_factor` Y!lc`3?%`$-&` 5!old`#I# = lc`$G!` *\"`!1\"hape` 0&` *!_type`#I!` 7#= \"triangle\") {`!d#` \"!*`!|!`&L#` E*star` =.2;}`!j#_id`!/&id;l`/5!` 3\"].` K';make_`#=$(` <\")`\"L(`#W%`%S!` \\#`%j\"`2q)lct;`\"W) =`\"v'` -*`)g)` 3#`$[!`!S#true;}};for (var id in`&h#`/~' {`!y\"abel(`&D!`'X'`)k$function`,K!line_pat`!6$`&a\"bb`$v(bbox`\"]!w = bb.x2 -` #!` 0!h` /\"y` .#y` 0!r` T(scale ? ratio : 1` >!x_adj = 0.5 * (1 - r) * w` 9!y` '3h` V\"` s(`!~!`!F\"y` '-`!I\"miss`(q\"!x || !` 1\"`$F\"`&^!`'@.`+a$`&x!` =)`+i,` w#) {`!K)` ]&point0.x;`!S)` /-y`'P(`!\",`4S\"`!!*`!!`$>*`4<*;x`#9&p`$T\"+ ` $!)`!G!` 0'y` 3$y)`(P\"cur`!=!`\"C$ = {x:x, y:y}`#9\"ts = [];pt`';#{x:`%X$`$z!, y:` }#bb.y +`%d\")})` C- +` &I` 9&x` =\"x2), y:` M#`%z!` (G2 -` K%`'e!inner = {`)*'k in pts`#s#`\"x%abe`4j!ts[k]` Z!distance_between = helper.` 2$(` Q),`#]-)`&R!k == 0 ||` `.<`!]#` i%) {` +#`!I$` v);` 5$`$e&`!$,` ;$` j$ =`!&-;}}return`\"($linePath(`!0(.x,`!V$` *\"y` &&`!%#` 7(` +$y`,1(`+0!_t(e, s, t, x, y`#r$x = x === undefined ? e.sm.x : `*g\"cy = y` 02y : y`#^!t` 3*) {t = \"0,0\";`\"D$\"t \" + t + \" s\" + s + \",\" + s ` \"$cx` ,%cy`13#`0@)`,%\"`0t(;`\"F%creat` /&s(refresh`\"K#`3&\"paths = {`2z$:\"M -0.57735,.3333 .5` \"'0,-.6666 Z\", diamond:\"M 0,-0.5 -0.4,0 0,0.5 ` &\"Z\", marker:\"m-.015-.997c-.067 0-.13.033-.18.076-.061.054-.099.136-.092.219-.0001.073.034.139.068.201.058.104.122.206.158.32.02` 5\"039.117` A\"75.006.009.011-.004.` \"#9.037-.125.079-.249.144-.362.043-.08.095-.157.124-.244.022-.075.016-.161-.026-.229-.048-.08-.134-`\"*\"227-.146-.013`\"0\"-.027` \"%4` $\"z\", heart`#5!275-.5c-.137.003-.257.089-.3.235-`\"u!379.348.539.58.765.202-.262.596-.33.576-.718-.017-.08`#q!`\"0\"-.13-.206-.087-.066-.20`!!9-.3`#$!5-.055.02-.106.053-.143.098` c#081-.169-.127-.272`#R!\", star:\"m0-.549c-.044.12`!*!4.25` B\".379-.135`\"k#271` ##405.002.108.078.216.155.323.23`#I!2.0`#t!16.057-.023.085-.032.099`\"0!.1` $!97.298.049-.031`$w\"068.143-.101.062`!]$4`\"`!.185-.133.109.077`!=#8.326`!=#4`\"Q#082-.2`\"z!23-`$8!109-.079`'L\"156.3`\"}!36`\"L!`%<$`%H\"2`\"L!-`%)\"04`\"x!`#&!1-`##%-.377\"`0k'id in custom_`*C!s) {`*F'[id]`/)!` 7'[id]`+]\"supported` 3#`4,\"` o+` c') {` C,`2|\"id);}`/O#clear_sets([all`,-&]);`,f*`2}\"`,h) = `,n%(id`,[#placemen`.9!center\"`-L!attrs = lattr`\"3!if (` 1!.type != \"image\"` d#attribut`\"H!{'stroke-width':` S\"border *`0a\" * normalizing_factor, ` S\"` E)_color, fill` 2#` +#opacity` .#` '#`4S!sor:\"poi`\".!}`\"/!over_`!D?h` D!`!\"g` k\"`!^1` 5\"`!^7} else`#O0`!|E` b/`$c&inactive) {` L&.` M\" = \"default\"`'Q#`'.!`%<!`%Q'`%M!`!9!size =`%x\".` (!`#$0`&!'x &&` H#y`&l$oint0`'=\"` &\".x` k%x,` 9#.y` +%y`\"x)` V%getxy`! #lat,` G#lng)`\"'\"rotated = `(z#` +\"([`!.$`!\"&], transform)`\"E!` 5! = {x:` a#[0], y` %%1]}`\"='`\"s\"= \"auto`(K$l = {sm:{}};l.sm.displ`)z!` P\"` )#` 7\"auto_`#Z#true` 0\"`$%#\"`*@$\"` 1\"hide_label = fals` H#` .!`,(#` ,!`#&%` #\"` /\"x` ($.x` +\"y` (%`!F#`%<)` #&` 7\"id = id`,&+`-k#l;return`&_\"` \\(= \"circl`+J%`,Q'pa`-=!` :!(`!T#`$8#.y,`&R\"* 0.5`$8\"bbox`$7\"` E# -` 9' * ratio, y` 9#y` )3x2` X%+` K2` 9$y` ,1`)w%if (`&?#x_i`\"k#`\"S',`0B-)`\"^#cs =` u!`\"4!`&]%`\"q$\"S\" + cs + \",\" ` \"%0,0 T\" +`$`$` 7%`$_$`'@!ath = Raphael.` t%Path`!V#`2G\"`!_&`'~(`!?!).toString() + \"Z\"`'r!`$j+marker\") {`1&)south`+U#`$M#`!E$pathBBox(path`$p\"`%?-path` 9#`#]'`!,+`1Y)` (!`2i(`-?#` 2!url ?`)4#` (&: directory +` 1)sourc`$2\"`!J-` =!(`!!*, 0, 0`4C&.sm`-!\"`\"T'`)f\"`\"]$_preload` X-`4b&`\"1$wh = this.`2(! /` '\"height` }!new_` )\"`&&(new_` L\"=` 6(* iwh` 8%`*f' -` H'/ 2` ;%`*y'` =#` d#/ 2`\"I&attr({` 4\":` ;&, ` l!` -!` %!, x` (!x, y` $!y}`#,).`*9&` 9*`)p!`!n\"+`!`&`)e!`!`\"` 0\"`!1\"};}`%r%`#B!`\"XH`\"4f`%m1rect(`\"5#`\"3#`\"$'` X&`-\",`\">M`\"b%`&g-`#d(attributes`(O#` '\"`$k+` /&`$@*original_`+4%`2!!` %#` =*ver_`!!)` #+` A)`0|,.sm.name`*2%name` 1)scal` 5&` )!` .._limit` 8*` .\"` ?)`,a(` #%` 5)url` ]%url` /)`4I/`!<(`3K4` 9'descrip`,_)` )'` 44_mobi`\"Z'` ).`#V*nactiv` F&` )$`$J*n_click = is_on` (!`%L!s.popup`%D*` .!_off` I#ff` 17uls`!4&` )!` .._speed` 8*` .\"` 80iz` b+` .!` 7/color` <+` .\"`0C$` ((:` -#` )!` a)`*z'` +)`*n'`\"q+int0` 5$0`-(0bbo` ^*labels = []`%}*`\"H\"`\"1-hid`\"\\&hid`&.+isplay`%\\&` *\"` :&`)w%(`(:\"t(`1W&ratio * ` 5!));if ` 5%` p)= \"region\" ||`-=%` 3,state` =!`!c&) {` F%hide()`,V).cont`)U\"create_` *#`!?%);all`3L%s.push` 2'` \"$_array[id] =`!X%`\",!!vml`!:(node.setA`,D$(\"class\", \"sm`!!%_\" + id);}};for (`4\\!d in` v%s) {make` K%(` J\"func`*I!`\"l!_or_`#7\"(` +!)`1\"leve`+q!pi_object.zoom_` 3!`1'\"evel_`-w!` 01_id` C!` w\" =`!)\".sm.` +#?` 4#`\"u#` 0+] : false`#!!` .\") {`%?!`!V\"= \"out\") {return` h#`4$$` <*`%Y#` V(`!w!=` O#`0\"\"` b&`!I!` c$` q,` m1`&9\"`#8#current_` 0!`\"T$`\"B#`!5$]`\"5!` =)`\"s'=` pgmanual`\"U$`(q\">` s'zooming_dimens`'C!r`#?4`!7+`!)$` ',`&<%is_adjacent(el`28!`%Vi`#n3`$y)!= `!4#`#<,tru`#:-`&P\"`#42`%}&`'N)` s'`'G2` 0-`'?<`&`9`!T*`!~*`!v3var upd`&a!ttr = `$9%(e, pos, anim,`-W\"`!D#anim == undefined) {` .\"` q$` A!ttrs` 6-` /\"` <(!` o(po` N!\"over\"` E'`'!over`!c!`-=!s`*'(` P$`&-$` K-` 1$` M/` :+` 5'`!Y#nim ||` 9\"image) {e` D!`\"?\")` a%e.animate` 3\", fade_time`.n!`#d'pill`#b.pill`#p#overr`1F\"if (` '$`#A,` .&`#H(` B(`+u*par`1t\"pill.sm.` +\"`#@(` P)`'M*` U%`':)` ]*`'C'`\"o%` O)`0q,` Q*);`&q!`#;!`(!!`%;4helper.clone(`!1&`%V+`$3%`%F<` X3`%e/`%%&` <<` G(`!;!` 2%`&\"! &&`\"o#.sm.typ`$=/`0<#`!\"#cattr[` \\'`0H#`\"o1.fill`0|&` .!h`\"z!color`!x*` 90` @#}`*w'`&v(`&K!`*r$;};`'>%highlight_labels`/Z$, type, `#L$`'T-!`-F'` U\"`,u%`%~)` 6\"`.4*` .\";}` \"\".forEach(`(r&` 4!`!!$` (!.sm` u'`&!`\"_\"` 6$.pill`#\\!`$4%`#Z$` <#top();`\"u(` 4!,` A$`$8\"ill) {`*J,`#E#` B$`0o(`!&%reset\" ||`#6!`!<\"u`'3!` 1ut` lCut\"`#s'` N!`$.$` <7`(O%` P(}}}});}`#`%`#{\"_inactive`%5$`$!~`$u*` M'`!C$`$N%`\"/!{cursor:\"default\"}`*/&` 30pointer` B!}`,?#inserting`.V%`!D%emphasiz`\"Q,`!M'`$n!!`*F(`\"a$`\"z,` e$able` ;(`!4'true;` K$` 3\"Before(all_visible_`!%!s);setTimeout`#4') {`!.}, 1`\"G#currently_ove`1t!` =!`(T!u` \"$last_`2_#d` 7)`0M#click` D#bel` %'` 0&over` #\"ut` #!background` O-` C%` (#` G'` A'` %&_handler`$A&create_event` 6$s(`%f$`\"D%`\"{)f (this`1m'`4&\".call` +,;}};`!U%` EBut` K;`\"'!` _)`-~$` \\-` F!` [0, `){!;`$Y!` ^*, manua`-h%e` Q!` C!&& !` 4%`-r(`,=\" ` F!shape_type`+X!` /!`'g%location`,k*`2o!` .!ratio < 0.05`.r,`!d#e`3g#);all_` i$s.toFront();` ,$`1>#` /'var mag = 1 *`!i\"` r!_siz`'%\"stroke_width`!(!`2@!['` 2\"-` 3!']` F!anim_to = {` 1*:` X)* 4, ` 9$opacity':0`#u#`+d#'fill` 3',`!B#:`!Y'`40!}`!\"callba`%8+) {` n\"remove();}`)9\"`$2%cale ?`#_#: 1` :!ty = (mag - 1) * 0.5`\"v$size * r * ` X!`*I&_t` n$plac`+x!`$g!south\" ?` J\"_t(e,` U& * mag, \"0,\" + ty) :` .7);`#]#.transform =`!7$`#8$`+k\"(` C#,`$i)peed * 1000, \"ease-`2[\"`#8$);};`*20xy_hook_check(`3!\"!`(^!id`(\"!` '!`'8%se`45!`/9(`.&&|| no_tooltip`2I(var`2=$ = `.|!_or_`-h\"`)l!`!3\"`0$'`-~$) {` #$`/z$}popup_off`3.*` .%` g!`/@&pann`!X#` *'inch` %-zoom` .#`\" #_up && `!D'`\"P(`0@+`#+!` Q#`+0+`1#$`0p-this`#p\"`3;&` {$`*1\"_inactiv`3]&`\"},` 9$` P'`4*.`%:!`%(!` Z*!`\"1'`!v#.show` <&`!!%top`%\\#vml &&`#r(`%Z%`-0&` 3*`-e&`-@'`#A+`\"\\$`\"0!gnore_hover) {update_`+A!` ?#, \"over\");highlight`-C#` 0.`(C!`\")&}} else`/s#`%\"&) {`!L~`!n\\`!|,`\"<>}}`-Z#eset_appearanc`2b+` N$`++%`%G&hide`\"f#is_adjacent`!;% && `(T$== \"`*4\"\"`3Q%`\"/5` 2$`,u$` ?'attributes, fade_time, whenDone);}` A8`![#`#=!` I9`#!8`-}!true);`%d(`$P{ || `#('`%KF`\"pY`\"I;);}`%a%` N$(`$y#helper.isF` ;#(`%y%) {` $$(`&a\"out`&I)force`&G)`.+*`.h'`0g2`)p.`0F'`.>4` 2\"`1J`1^D`$\\(`.g:out`.JC`)!;`\"~4`1.-`+AR`'|(` -2`!J$` ?%`'u~`-Lv`!T(`'u>`0S3 || `'W! ===` h\" {`.P,`.A/;last_`)|#d =`\"G$`(X!`'N%`/+&`(58` +&pann` #.inch`%Z(`':F`'9;`)7&` >'` .$` \\!`\"~'&& e`%!&touchend`$V-` 4:star`)a)if (`(V/`%,#pos();}popup_off`!\\*` .%`\"<,zoomable &&` +)`!C%`2R# || `$c!destin`'7! !` {%`*k+` U%`&-!) {` $`!x\"`*v), e);`*g+`\"o'`,n(`!<!` ]!ed) {out.call` **;setTimeout(`&,&`!=#_to`,1&}`0O'`)o&` 5.` 3#`,Z,`!t3var lin`%\\+url`!}\"ink != \"`*T!!no_urls) {var js_url =` [!.substring(0, 10)`#L!javascript\" ?`)@! :`/p(new_tab ||` g#`(g#` $%window.`+x$.href`!+#`\"Z%` =#top` 13`&k$` B*open`\"<!, \"_newtab\"`$j-` S$`+Y)`$U(`&*(&&`&O\"`%*&`+_-`%.*}`#~3`'q!`#_#coords = get_` (!inates(`&]'pos(e, {l:` H\".x, u` $$y});}` B$show`%b&`',)true`0:9ver\");puls`-o%`!e\"`/ng`/@)`0?6` 2$`0N(`,~!ver`0j!ibutes);}}`$@)`/D&`#V!lose_`!7! = docu` c!getE` @\"ById(\"xpic_sm\" + \"_\" + div`\"S\"` V'`(}!` %%.on`09.`-;'`*oF.sm`+\"6`/](` Y\"trigger`&-\"\"`!L\"`.)!\", []);}`2\"!back` O&`!X%`2[%`#z\"typeof`2q%`3C!\"undefine`0;!` 1&`!8#}`!2*back`!2#if (`!x\"`.h'`.L-`/$0`/K1&& initial`3k!_solo)` *(`\"!#`+)3`,.':\" +` I)`+ $if (incr`%E!a`']!`!>9`4H!`!U!` 30`!s\"`#S#`&g)&& `%>&`$t4,`)Z!,`& *`/t$` v\"_array[`!#6]);}`0G.` 5D,`$}\",`%G%`1!&`,C!inside = is_` &\"`$~-, `! )`$U(]`1D\"` :\" =`#L:manual`%>#`!1!?` _7 :` /*-1]`(u-`#<e`#H7`#,1`)@(_handler`+%,` 8&(`)|!` 4%`0 .`&9\"e.touche`4A$` )!_obj = e.changedT` =\" ?` \"-[0] : ` \\%[0]`2`# {x:` c%.clientX, y` $-Y}`\"[%var y = ie`!$\"` :\" +`.E&`.P$`.M#.scrollTop`!@!pageY`%W!x` V,X` D>Left` ^%X`!|'x, y:y};}`#D&setup_panning`#p$ground`-Y/`#d#`.Y$`/J.`&3,) {reset_appearanc`(:#`/D&`/|/`/P-}};`%3)new_viewbox`%;!`23!oords =`%N/`#E!newX =` @#.x` ,$Y` *&y;dX = (startX -` N!) * ` ,!.r;dY` 6%Y` 9\"Y` 3(var pan_threshold = 5` 5'if (Math.abs(dX) >` @+|| ` 6&Y` /-) {currently`$K$ =`)Z!;}`$|&`!)\"x + d`'#!` *\"y + dY, w` )#w, h` $#h, r` $#r};}var mousedown`#z%var`\"#\"`\"B!`\"}\"` +&Y`$6&` +!_pan`)Q%`+W)`!a#`$q#e.preventDefault`'d!` %*() : (e.` S\"Value`!M$);`!(! = {x:`\"t#`%P$.`'h!` %,y, w` $-`\"f!` %,`\"p!` 9- / original_width / scale};`#'(`$.!`&U<`&@#`&h'`&3#`&f'`(p+` '$pos(e, {l`$Q\"X, u` $\"Y})`-`'during`#)!`!^%`$#%;}`-r) &&`-=&.length > 1` E'var v`\".#`)=*;paper.setViewBox(v.x, v.y, v.w, v.h`!V(finish`!K3 || !`'a-`'o3`&+\"`'&.`!,d`%<+ = v;`4@, = {sm:{}}` *-.sm.zooming_dimensions = ` i+` D1type = \"manual\"`\"3/setTimeout(`/!)`\"p6}, 1)`4\"\"arrow.show();}helper.addEvent(mapdiv, \"`!-%\",`*;&);` 2:move\",`&_'` 1<up\",`%L'` /<lea` |!` -A`'P!`!v!`!\\C` L!`!WH`(Q\"nd`!C+`3_-inch`3j#var `%/\"istanc`,u%`2E*` N!` ;%`2M%xy0`-9\"`)j%[0].page`/w!` %-Y}`,3!xy1` G,1` B1` /#Y}`(b# `\"T#`!@%xy0, xy1`\"9(move`!k\"`*-%`&_&`'#`+LO`'B'`#9#`.?(`#4'`\"{1`3z!`#\\)`#<#diff =` V&-`$!*` x!magnitude =`48'iff` q\"` 5&> 10`\"L#` s!> 0) {zoom_in_click();} else` 1#out` /&`%:,`!=%}` L$` '7`.&.`#{%`&,2`*t?`#`&`+.&00`*l8`(y)`%A&`(`C` 'F`(y*` L\"`\"X&order() {all_states.toBa`#b!background` *&if (all_external_b` ^!s` \\#` ',.toFront();}all_label` *(top` #.` E!oc`/U!` ,(` ,$` @.`+.)_events(refresh`3*$` %&`\"B'hover(over, out`!6\"` 5#`&I\"`&P!`\"`)` 2\"` '&`&r\"` 6)` q\"reset_tooltip, ` \")`0i)` h&` f\"_handler`#j\"responsive) {set_` '&` A$()`*{\"`2V\"`+9!`$e,`&G$`\")$` -,end`\"@(`!S#` 2%`!S&);`.x#`3&\"(` '%`/&%;}`%'\"`$Z%`#Q1` 5&`#\\)`%\\'` Q\"` (!_` W\"` %#` Z%` B\"` Z\"` 6\"` _#`\"W2`&)(`\"E*` +/`\"}-`&M%` T#`!$)}}`-a!etec`$<!iz`1P'`$=4 {` :%` M\"(`/l!size_pap`$r\"var` 7#Timer;` w) = `,&*lear`,F$` K');` \"' = `,i'` 3\", 3`,L!`#5!window`+J%Listener) {` #3(\"` `\"\",`\"U*,`-J\");` ?5orient`#s!chang` I6`/S$` c$ttach`--\"` vD` G(` nGif (vml) {document.body.on`#o2`$d$;};`1#'`$j*`,/\"`/&\".offsetWidth <`4a!return;}create_dimensions(true);` a!.setSize(width, height)`4'!scaled`.a#_`!_#` \"(* ` Q\" / original_` ,!) * normalizing_factor * 1.25`/x%`+5\" && `(N)`+h*forEach`3,'` 4!) {` #!.attr({'stroke-`!9!':`!h.});` J\"sm` N!ibutes[` J*] =`\"B/` O&over_` @:` C$` Y#`+z!`\"PY}`+I,`\"_.` 4$`%J#lattr[` -$.sm.id].type != \"image\") {` 6(`!x9` [2`\"<\"`!_T` b(`#5>`!,2`#W\"` ac`%s1`\"]B});}`$?$in`&1line) {var adj` B!`(i$lin`%m!`%)X` _!`!m1`!!)});})`*h%trial_text(`*J\"min = `!8$2 > 250 ?` '': 250;max`!O\" = popup_max` +\"? ` \"+: min;`,V(veal_map(refresh`\"p#region =` \"#_array[initi`3z#]`+4!!` +$back) {back_arrow.hid`-m!` B!` t&zoom_to(` s\", `-8\"` A!` s(_solo && ` )( != -1`! #ground.show()`!%Dfor (var i = 0; i <`\"<#`%0!`,\"length; i++`\"h#id`\"b%` >&[i]`.s\"tate`+f$`#\"$d`\"y#`+z%hid`-]&`!n#}`!D(in label` \\\"`!2#` -!`).!` 1%`!/$` -\"set` 6%set` 9&`+v\"bel.sm.parent`,)%` )).sm`,)\"== \"`!Z!\"` /6`%Q#!`\"b(id || !` 55) {` 3%hide =`%0!;`!l%`$5%}}}all_external`+F#`)n0` 4\"`!a#Raphael.isPointInsideBBox`&>#`/}!box,`3(#` ($.x,` \",y)) {` ,$`$=$);`4U$`&F.all_visible`2|$` '/`\"F!` -)`\"3-`!&$`)Q'`(N!`)X!) {get_` ,#able_info();`%/!`-C%();`+H#`!F\"(`(x\"` -#`\"h\"` &+`2\"%` (+`!m!();style`(m!`\"S\"();hide_and_show_before(last_destin` k!`*2$`\":!`!b#event`!!$resize_pap` ;!`, '` :\"` x*after` t-);upd`)1!pi();trigger_hook(\"`#9$complete\", []);}var tooltip;`#e%load`#c$mapdata = api_object.` .#;mapinfo` ,-info`)0!map_inn`&y!delete window.`\"F!;}expand`!l#preload();get_client`$t$get_` h\"` *!`-^!s_forgery()) {alert(\"The continent map can't be used with other data.\"`'H&`& 3`%?#dom_structure` ,'imens`%p!` +%canva` %'`2#)`-F!`1F\"nocss) {set_tt_css();}`$)# = ` W$` +\"` f&`/H!button` j#manu`02#) {` @#`0W!` A\"` t!`(/-`'=/`(E*`(>-`!9&`)e\"`'G\"Timeout`,)'`!;&`(j&`(^.re`!N!ll` y#`!P*`(F$` O#`(L!_handl`!0%`(^$`(F));`#*#.` T\"(`'}9`(#+xy` 3!_check();}, 1`(A#getting_xy = false`17!get_xy_from_map = `\"]&log`.q#!` S'|| ` 3\"` e)`0#!} else {`&k$everything.mousedown`#\\'e, a, b`2y$ = ie ? `\"q!.`([\"X + document.d` \"#Ele` +!scrollLeft : e.pageX`\"0!u` W0Y` I>Top` a%Y` e!find_pos = helper.findPos`*r'` D!x0 =` E%[0]` 0!y` ()1` /\"go = `-/,.sm.zooming`)I'` K!this_width = go.r * ` )\"/ scal`$b\"` A!height` =&` )#` <(x` 8\"x` -$ +` u(* (l - x0) /` \"`\"\"\"` O\"y` E,` y#* (u - y` Q!` .\"` R!print_string = \"You clicked on\\nx: \" + x + \",\" + \"\\ny` .\"y` .\"`*y!`&H\"console.log(` m();}`($+lick_xy\", [{x:x, y:y}]);});}`!S!lo`'$`*F(`'{+`1u#}`0o&` z)name, args`'8#fn = hooks`0d#[name]`!}!fn) {fn.apply(null` T#`)U\"plugin_array =` '$` g!` ^#for (var i = 0; i <` I).length; i++`!D(` 8([i`!,=}`!J!`+8)`\"v,if (`\":(.`#g$ ||`!m)` 0%`!U# > 0`#S/`,%!);}`$4\"ov`#Y#`!*)e`)_\"`\"2#type = ` .#.sm.type`\"6!` 5\"= \"`/|!\") {`%M*` z!` 6\", [` W'id]);}` V)`/r$` O3` 6%` H=`07\"` P3` 6#` Q0`\"k#ut`!uxut`\"-_ut`\"+`ut`\"L>`&.\"`\"X4, e`\"1^` }\"`\"_2, e`!gE` b\"`!{3` U0`#G6` e\"`#J5` f#`\"|\"zoomable_`\")~\"` }+`\"R^` b+`\":`.o'` C\"_zoom(id, callback`\"<#` 9\" =` @$`-H#d];zoom_to(` 3\", `,)!` U';}` {%`\"9!` j5` 9! =` ?#` o.` 3!` c9`$w$` y&zp` z)if (!manual` ?!`2e+\"L` Z#`%>! only works when the map is in ` \\' mode.\");return`$@&of zp === \"undefined\") {zp = 4` ;)`!^$` ;/` 1&`\"W\"`2L\"destin`!i\"= {sm:{type:\"`!S\"\", zp:zp`'M#`\"s$ =`\"|&`#Y&var w` 0'.sm.size * scale * zp` A!h = w * original_height /` (&width` H!x` d+x -` X!0.5` ;!y` 1+y - h` 5'r`!(!/ (` n*`!N$);`\"Y'.sm.zooming_dimensions = {x:x, y:y, w:w, h:h, r:r}`&0%` ['`&\"9reset_tooltip`4a$currently_over) {out.call` *,`%(\"!` X#_`$@\") {`%l$ else {` 2*`%)&if (on`*0\"` K'` H#.hide();setTimeout(`,F&) {}, 100`\"!(popup`&X!, id`\"&#`,<.var`,p$`)H/`!n#`,\"3` N*`+(-`\"J$` 9*`&k/`#3\"`4G&`)g(`!2!+ \" \" + id` &!does not exist`)N%`(Q(`#B$`/(*` .$`&X!bo`':!ast_`&&=`/a&!`17+` e!b` y*bbox`(7%(bb.x + bb.x2)`(.+(bb.y` 7\"y` 4%x = x`'|$;y = y` %%`#H(x`!(*`!-\"y` '*y`\"i\"`'$#_`!L!x - box.x) / ratio` U!` <$`!S!y` =#y` :&`(\"'_x > `)c$1.1 ||` T'> `+-#` 7!`.S,Not in this`%l#`$P-`(2-tru`3h\"`(6'` #!`)C\"`%r$`\"u%over` +,`(a$`*?\"pos`\"#&,`!s&,`#5$);ignore_pos`!:$`)d3`!v#` >!`)6&`0X#pulse(`)<!`(\"\"`'m2`'{#`*N'` S\"el,` t!`*)-_`*k\"`+G%`*y$`!\\$up`!W%`#*+`,W%`'v!` 1!ed`#1'`,f5`-](fresh_`+*!`\"7\"set` %&`&+!` )!`+A/`1i!abels` 5$.sm.` ,\";make` ]'for (var i = 0; i <` U#.length; i++`#U#` 4!_id`*1!bels[i].sm.id;set_` /!(` =$)`!%\"` %,}`(J!no`/~$s`#=%`\"T%disable_`$\"!s() {` A*`%_(`$.$` T>en` X9`,p'no_ur`#8!`!B3url` Z$` B#`&u\"` @9`!9#` J.`!3#` @%go_back(`38% {back`&#\"` ,&` G'expand_api() {api_objec`&B!ibr`%`\"create_bbox`%:\";` >'get_xy`(U!g_xy` /(proj = get` **loa`%D!oad_map` /(`1b#zoom`1m&zoom` 9(`'0\"` =#` #&` u*`* #` ?#` #)` =(ba`1D!`#3#`!q)op`)d!` #!` ,)ulse =`+9)` H-`*_!` V$` (!` 7(zoom_lev`+n!\"out\"` (2`(L\"`%!\"` 6'`&\"( =`&0)` ;(`%m' =`%z(` X0`'X\"` c'` +\"` ]/` A%` #)`$X*`+!`$]!`,(\"`$v-`,:'` <'`&8\"`&u&upd`$v$`\"k\"`&w+`#;)`-o!destin`%'!.sm.typ`#b3`,%$` B.id ?` X1id :`)#-`!\\#`(P.`\"'!`-%`.?\"`'Y.s`']&` 1-`'-$s`1G-` 9)`/($`-d\"` /-`,C# = ` ##;}`*J(;`)?$();}window[plugin_name] = (`\"D%(`2\\% ` x&;})();docReady` ?*`010auto`!0!`!f!`0:/`!9\" =` ;+[i`1[\"ready_to_`+A#` H#&&` ##.mapdata &` \",.main_settings.au` ]$!= \"no\" ?`.j!`$}%if (`!!)) {`\"5'` w\"`3[\"Timeout`\"P*`!7#load();}, 1);})` P$;}}});`\"_+push(`#R&` K!\"simplemaps_worldmap\");"))