
<div class="footer">
	<div class="container">
		<div class="footer-row1">
			<div class="f-logo"><a href="<?php echo home_url()?>"><img src="<?php bloginfo('template_url') ?>/images/logo-footer.svg"/></a></div><?php wp_nav_menu(array(
    'theme_location' => 'footer-nav',
    'container' => false,
    'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
    'menu_class' => 'footer-menu',
    'menu_id' => '',
    'depth' => 0
));?>
		</div>
		<div class="footer-row2">
			<div class="copyright">
				Copyright ©  NETOPAY Group LLC, 2018  All rights reserved
				For company information and other legal bits, see our <a href="/imprint">Imprint</a>
			</div>
			<div class="massagers">
			</div>
			<div class="social"><a href="https://www.facebook.com/quantcoin2017" target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a><a href="https://twitter.com/Quant_Coin" target="_blank"> <i aria-hidden="true" class="fa fa-twitter"></i></a>
			</div>
		</div>
	</div>
</div>