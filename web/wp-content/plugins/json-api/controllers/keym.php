<?php
/*
Controller name: KeyM
Controller description: Data manipulation methods for posts
*/
ini_set('memory_limit', '-1');
error_reporting(E_ALL);
include('simple_html_dom.php');
require __DIR__ . '/vendor/autoload.php';
use Google\Cloud\Translate\TranslateClient;

class JSON_API_KeyM_Controller {


  public function get_posts_list() {
    global $json_api;

    $url = 'http://www.globaleventslist.elsevier.com/controls/events_list_control.ashx?a=true&_t=1482507587437';
    $data = array("filterYears" => "2017,2016",
                  "disciplineIds" => "68",
                  "continentCodes" => "EU,NA",
                  "countryCodes" => "ES,US",
                  "sortBy" => "recency",
                  "page" => "5");

    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );


    $translate = new TranslateClient([
        'key' => 'AIzaSyCjQgMyYG0VLqlo3mIXvmq4yI2q0_6VWIo'
    ]);

    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    $json_data = json_decode($result, true);
    $postIds = [];
    foreach ($json_data['results'] as $event) {
      $ret = array();
      $ret['expotrId'] = 'elsevier'.$event['EventId'];
     $the_query = new WP_Query( "post_type=post&meta_key=expotrid&meta_value=".$ret['expotrId']."&meta_compare=LIKE" );
        if (! $the_query->have_posts() ) {
          $ret['titleEn'] = $event['Name'];
          $ret['countryCode'] = $event['CountryCode'];
          $ret['titleEs'] = "";
          $ret['location'] = $event['City'].', '.$event['CountryName'];
          $ret['dateText'] = $event['DateFormatted'];
          $ret['startDate'] = (str_replace('/Date(', '', $event['DateStart'])) ;
          $ret['startDate'] = (str_replace('000)/', '', $ret['startDate'] )) ;
          $ret['startDate'] = gmdate("Ymd", intval($ret['startDate']));
          $html = file_get_html($event['FullUrl']);
          $ret['organization'] = $html->find('span[itemprop=performer]', 0)->plaintext;
          $ret['phone'] = ($html->find('.address', 0)->children(1)->children(0)->plaintext == "Telephone") ? $html->find('.address', 0)->children(1)->children(1)->plaintext : '';
          $ret['content'] = $html->find('.description', 0)->innertext;
          //if ($ret['venue']) {
          $ret['location'] = $html->find('input#txt_map_address', 0)->value;
       
          $text = $ret['titleEn'];
          $target = 'es';
          $translation = $translate->translate($text, [
            'target' => $target
          ]);

          $ret['titleEs'] = $translation['text'];

          $post = new JSON_API_Post();
          $category = 3;
          $post_data = array(
            'post_title'    => '[:en]'.$ret['titleEn'].'[:es]'.$ret['titleEs'].'[:]',
            'post_content'  => $ret['content'],
            'post_status'   => 'publish',
            'post_author'   => 1,
            //'post_date'      => $ret['startDate'],
            'post_category' => array( $category )
          );

          $post_id = wp_insert_post($post_data);
          update_post_meta( $post_id, 'expotrid', $ret['expotrId']);
          update_post_meta( $post_id, 'location', $ret['location']);
          update_post_meta( $post_id, 'datetext', $ret['dateText']);
          update_post_meta( $post_id, 'startdate', $ret['startDate']);
          update_post_meta( $post_id, 'organization', $ret['organization']);
          update_post_meta( $post_id, 'phone', $ret['phone']);
          update_post_meta( $post_id, 'countrycode', $ret['countryCode']);
          $postIds[] = $post_id;
        }
    }
    /*global $json_api;
    if (!current_user_can('edit_posts')) {
      $json_api->error("You need to login with a user that has 'edit_posts' capacity.", 403);
    }
    if (!$json_api->query->nonce) {
      $json_api->error("You must include a 'nonce' value to create posts. Use the `get_nonce` Core API method.", 403);
    }
    $nonce_id = $json_api->get_nonce_id('posts', 'create_post');
    if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
      $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.", 403);
    }
    nocache_headers();
    $post = new JSON_API_Post();
    $id = $post->create($_REQUEST);
    if (empty($id)) {
      $json_api->error("Could not create post.", 500);
    }
    */
    return array(
      'ids' => $postIds,
      'event' => $event,
      'post' => $json_data
    );
  }

/*
  public function create_post() {
    global $json_api;
    if (!current_user_can('edit_posts')) {
      $json_api->error("You need to login with a user that has 'edit_posts' capacity.", 403);
    }
    if (!$json_api->query->nonce) {
      $json_api->error("You must include a 'nonce' value to create posts. Use the `get_nonce` Core API method.", 403);
    }
    $nonce_id = $json_api->get_nonce_id('posts', 'create_post');
    if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
      $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.", 403);
    }
    nocache_headers();
    $post = new JSON_API_Post();
    $id = $post->create($_REQUEST);
    if (empty($id)) {
      $json_api->error("Could not create post.", 500);
    }
    return array(
      'post' => $post
    );
  }
  */
  /*
  public function update_post() {
    global $json_api;
    $post = $json_api->introspector->get_current_post();
    if (empty($post)) {
      $json_api->error("Post not found.");
    }
    if (!current_user_can('edit_post', $post->ID)) {
      $json_api->error("You need to login with a user that has the 'edit_post' capacity for that post.", 403);
    }
    if (!$json_api->query->nonce) {
      $json_api->error("You must include a 'nonce' value to update posts. Use the `get_nonce` Core API method.", 403);
    }
    $nonce_id = $json_api->get_nonce_id('posts', 'update_post');
    if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
      $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.", 403);
    }
    nocache_headers();
    $post = new JSON_API_Post($post);
    $post->update($_REQUEST);
    return array(
      'post' => $post
    );
  }
  
  public function delete_post() {
    global $json_api;
    $post = $json_api->introspector->get_current_post();
    if (empty($post)) {
      $json_api->error("Post not found.");
    }
    if (!current_user_can('edit_post', $post->ID)) {
      $json_api->error("You need to login with a user that has the 'edit_post' capacity for that post.", 403);
    }
    if (!current_user_can('delete_posts')) {
      $json_api->error("You need to login with a user that has the 'delete_posts' capacity.", 403);
    }
    if ($post->post_author != get_current_user_id() && !current_user_can('delete_other_posts')) {
      $json_api->error("You need to login with a user that has the 'delete_other_posts' capacity.", 403);
    }
    if (!$json_api->query->nonce) {
      $json_api->error("You must include a 'nonce' value to update posts. Use the `get_nonce` Core API method.", 403);
    }
    $nonce_id = $json_api->get_nonce_id('posts', 'delete_post');
    if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {
      $json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.", 403);
    }
    nocache_headers();
    wp_delete_post($post->ID);
    return array();
  }
  */
}

?>
